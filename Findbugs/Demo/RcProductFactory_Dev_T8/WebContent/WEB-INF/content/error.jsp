<%@ page language="java" contentType="text/html; charset=Shift-JIS" pageEncoding="Shift-JIS"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<tiles:insertDefinition name="layoutCommon">
<tiles:putAttribute name="css">

</tiles:putAttribute>
<tiles:putAttribute name="javaScript">

</tiles:putAttribute>
<tiles:putAttribute name="body">
<meta http-equiv="Content-Type" content="text/html; charset=Shift-JIS" />
  <div id="contents">
    <div id="content">
        <div id="errorPageContent">
            <s:property value="commonForm.errorMessage"/>
        </div>
    </div>
  </div>
</tiles:putAttribute>
</tiles:insertDefinition>
