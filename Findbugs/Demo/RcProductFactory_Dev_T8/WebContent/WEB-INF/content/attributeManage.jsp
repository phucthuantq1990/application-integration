<%@ page language="java" contentType="text/html; charset=Shift-JIS"
    pageEncoding="Shift-JIS"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<tiles:insertDefinition name="layoutCommon">
<tiles:putAttribute name="css">
    <link rel="stylesheet" type="text/css" href="css/jQuery/kendo.common.min.css?20140804140000" />
    <link rel="stylesheet" type="text/css" href="css/attributeManage.css?20140804140000" />
</tiles:putAttribute>
<tiles:putAttribute name="javaScript">
    <script type="text/javascript" src="js/jQuery/kendo.all.min.js?20140804140000"></script>
    <script type="text/javascript" src="js/jQuery/kendo.culture.ja-JP.min.js?20140804140000"></script>
    <script type="text/javascript" src="js/jQuery/jquery.validate.min.js?20140804140000"></script>
    <script type="text/javascript" src="js/attributeManage.js?20140804140000"></script>
    <script type="text/javascript" src="js/commonJS.js?20140804140000"></script>
</tiles:putAttribute>
<tiles:putAttribute name="body">
<!-- Contents -->
<div id="contents">
    <div id="content">
        <s:if test="%{attributeActionForm.errorMsg != null && attributeActionForm.errorMsg != ''}">
                 <div id="message" class="has-message success">
                     <s:property value="attributeActionForm.errorMsg"/>
                 </div>   
         </s:if>
         <s:else>
            <div id="message">
            </div>
         </s:else>
    <!-- No data message -->
    <input id='noDataMessage'       type="hidden" value='<s:property value="getText('text.error.noData')"/>'>
    <!-- Host Address -->
    <input id='hostAddress'         type="hidden" value='<s:property value='hostAddress'/>'>
    <!-- Header of grid -->
    <input id='titleAttributeCode'           type="hidden" value='<s:property value="getText('text.label.attrEdit.attrCode')"/>'>
    <input id='titleAttributeName'           type="hidden" value='<s:property value="getText('text.label.attrEdit.attrName')"/>'>
    <input id='titleAttributeType'           type="hidden" value='<s:property value="getText('text.label.titleAttributeType')"/>'>
    <input id='titleAttributeLengthLimit'    type="hidden" value='<s:property value="getText('text.label.titleAttributeLengthLimit')"/>'>
    <input id='titleAttributeRegexp'         type="hidden" value='<s:property value="getText('text.label.titleAttributeRegexp')"/>'>
    <input id='titleAttributeShowFlg'        type="hidden" value='<s:property value="getText('text.label.titleAttributeShowFlg')"/>'>
    <input id='titleAttributeDelFlg'         type="hidden" value='<s:property value="getText('text.label.titleDelFlg')"/>'>
    <input id='titleUpdateOn'                type="hidden" value='<s:property value="getText('text.label.category.updatedOn')"/>'>
    <!-- Title for paging -->
    <input id='pagingDisplay'                type="hidden" value='<s:property value="getText('text.paging.display')"/>'>
    <input id='pagingEmpty'                  type="hidden" value='<s:property value="getText('text.paging.empty')"/>'>
    <input id='pagingItemsPerPage'           type="hidden" value='<s:property value="getText('text.paging.itemsPerPage')"/>'>
    <input id='pagingFirst'                  type="hidden" value='<s:property value="getText('text.paging.first')"/>'>
    <input id='pagingPrevious'               type="hidden" value='<s:property value="getText('text.paging.previous')"/>'>
    <input id='pagingNext'                   type="hidden" value='<s:property value="getText('text.paging.next')"/>'>
    <input id='pagingLast'                   type="hidden" value='<s:property value="getText('text.paging.last')"/>'>
    <!-- Filter type for grid -->
    <input id='hiddenTypeCbb'                type="hidden" value='<s:select id="cbbListType" style="width: 95%;" cssClass="tbSearch filter" headerKey=" " headerValue=" " list="attributeActionForm.listType"/>'>

        <!-- start title Management of Brand Master -->
        <div id="title">
            <h1><s:property value="getText('text.label.attributeManageH1')"/></h1>
            <a id="btnAddNewAttribute" class="btn-size-small btn-color-default k-button" href="attributeEdit.html"><s:property value="getText('text.btn.redirectNewAttributeButton')"/></a>
        </div>
        <!-- end title Management of Brand Master -->
        <!-- start contents_inner -->
        <div id="contentsInner">
            <!-- start list button div -->
            <div id="lstButton">
                <input type="button" class="btn-color-default lstButton k-button" id="btnBack" value="<s:property value="getText('text.label.btnBackToPreviousScreen')"/>">
                <input type="button" class="btn-color-default lstButton k-button" id="btnResetFilter" value="<s:property value="getText('text.label.btnResetFilter')"/>">
                <input type="button" class="btn-color-default lstButton k-button" id="btnSearch" value="<s:property value="getText('text.label.btnSearch')"/>">
            </div>
            <!-- end list button div -->
            <!-- start table list master -->
            <div id="table-content">
                <!-- start table list master content -->
                <div id="grid">
                </div>
                <!-- end table list master content -->
            </div>
            <!-- end table list master -->
        </div>
    </div>
</div>    
<!--End Contents -->
</tiles:putAttribute>
</tiles:insertDefinition>