<%@ page language="java" contentType="text/html; charset=Shift-JIS"
    pageEncoding="Shift-JIS"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<tiles:insertDefinition name="layoutCommon">
<tiles:putAttribute name="css">

</tiles:putAttribute>
<tiles:putAttribute name="javaScript">

</tiles:putAttribute>
<tiles:putAttribute name="body">
<!-- Contents -->
<div id="contents">
    <script type="text/javascript" src="js/jQuery/selectable.js?20140804140000"></script>
  <div id="content">
      <div id="title">
          <h1>
            <s:property value="getText('text.label.productDetailH1')"/>
          </h1>
     </div>
     <!-- contents_inner start -->
    <div id="contents_inner" style="border: none; background: none;">
        
    </div><!-- contents_inner -->
    <!-- contents_inner end-->
  </div><!-- vehicle_list -->
</div>
</tiles:putAttribute>
</tiles:insertDefinition>