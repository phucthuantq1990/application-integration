<%@ page language="java" contentType="text/html; charset=Shift-JIS"
    pageEncoding="Shift-JIS"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<tiles:insertDefinition name="layoutCommon">
<tiles:putAttribute name="css">
    <link rel="stylesheet" type="text/css" href="css/jQuery/kendo.common.min.css?20140804140000" />
    <link rel="stylesheet" type="text/css" href="css/commonManage.css?20140804140000" />
    <link rel="stylesheet" type="text/css" href="css/brandMasterManager.css?20140804140000" />
</tiles:putAttribute>
<tiles:putAttribute name="javaScript">
    <script type="text/javascript" src="js/jQuery/kendo.all.min.js?20140804140000"></script>
    <script type="text/javascript" src="js/jQuery/jquery.validate.min.js?20140804140000"></script>
    <script type="text/javascript" src="js/brandMasterManage.js?20140804140000"></script>
</tiles:putAttribute>
<tiles:putAttribute name="body">
<!-- Contents -->
<div id="contents">
    <!-- Start content on each area -->
    <div class="content">
        <!-- Start show message success -->
        <s:if test="msgSuccessWhenEditBrand == 0">
              <div class="has-message success">
                  <s:property value="getText('message.success.insertSuccess')" />
              </div>
        </s:if>
        <s:elseif test="msgSuccessWhenEditBrand == 1">
              <div class="has-message success">
                 <s:property value="getText('message.success.updateSuccess')" />
              </div>
        </s:elseif><!-- End show message success -->
		
        <!-- Start title Management of Brand Master -->
        <div class="title">
            <h1><s:property value="getText('text.label.brandMasterManageH1')"/></h1>
            <a tabindex="13" id="btnAddNewBrand" class="k-button" href="editBrand.html">
                <s:property value="getText('text.label.btnAddNewBrand')"/>
            </a>
        </div><!-- Start title Management of Brand Master -->
        
        <!-- Start content inner -->
        <div id="contentsInner">
            <!-- Start list button div -->
            <div id="lstButton" class="buttonArea">
            <!-- // BOE #6344 No.7 Thai.Son 2014/02/18 Change oder button. -->
                <input type="button" id="btnBack" class="btn-color-default lstButton k-button" tabindex="12"value="<s:property value="getText('text.label.btnBackToPreviousScreen')"/>">
                <input type="button" id="btnResetFilter" class="btn-color-default lstButton k-button" tabindex="11" value="<s:property value="getText('text.label.btnResetFilter')"/>">
                <input type="button" id="btnSearch" class="btn-color-default lstButton k-button" tabindex="10" value="<s:property value="getText('text.label.btnSearch')"/>">
            <!-- // EOE #6344 No.7 Thai.Son 2014/02/18 Change oder button. -->
            </div><!-- End list button div -->            
            <!-- Start table list master -->
            <div id="mainContent">
                <!-- start table list master -->
                <div id="table-content">
                    <!-- start table list master content -->
                    <div id="grid">
                    </div>
                    <!-- end table list master content -->
                </div>
                <!-- end table list master -->
            </div>
            <!-- end table list master -->
        </div><!-- end content inner -->
    </div><!-- End content on each area -->
    <!-- Begin hidden fields -->   
    <div>
	    <!-- No data message -->
	    <input id='noDataMessage'       type="hidden" value='<s:property value="getText('text.error.noData')"/>'>
	    <!-- Host Address -->
	    <input id='hostAddress'         type="hidden" value='<s:property value='hostAddress'/>'>
	    <!-- Header of grid -->
	    <input id='titleBrandCode'      type="hidden" value='<s:property value="getText('text.label.titleBrandCode')"/>'>
	    <input id='titleName'           type="hidden" value='<s:property value="getText('text.label.titleName')"/>'>
	    <input id='titleHyoujiName1'    type="hidden" value='<s:property value="getText('text.label.titleHyoujiName1')"/>'>
	    <input id='titleHyoujiName2'    type="hidden" value='<s:property value="getText('text.label.titleHyoujiName2')"/>'>
	    <input id='titleHyoujiName3'    type="hidden" value='<s:property value="getText('text.label.titleHyoujiName3')"/>'>
	    <input id='titleUser'           type="hidden" value='<s:property value="getText('text.label.titleUser')"/>'>
	    <input id='titleDelFlg'         type="hidden" value='<s:property value="getText('text.label.titleDelFlg')"/>'>
	    <input id='titleKousinDate'     type="hidden" value='<s:property value="getText('text.label.titleKousinDate')"/>'>
	    <!-- Title for paging -->
	    <input id='pagingDisplay'       type="hidden" value='<s:property value="getText('text.paging.display')"/>'>
	    <input id='pagingEmpty'         type="hidden" value='<s:property value="getText('text.paging.empty')"/>'>
	    <input id='pagingItemsPerPage'  type="hidden" value='<s:property value="getText('text.paging.itemsPerPage')"/>'>
	    <input id='pagingFirst'         type="hidden" value='<s:property value="getText('text.paging.first')"/>'>
	    <input id='pagingPrevious'      type="hidden" value='<s:property value="getText('text.paging.previous')"/>'>
	    <input id='pagingNext'          type="hidden" value='<s:property value="getText('text.paging.next')"/>'>
	    <input id='pagingLast'          type="hidden" value='<s:property value="getText('text.paging.last')"/>'> 
	    <!-- Hidden for commbo box filter -->
        <input id='hiddenlistCommonUser' type="hidden" value='<s:if test="null != commonForm.listCommonUser">
                                                        <s:select disabled=""
                                                        id="tbUser"
                                                        list="commonForm.listCommonUser"
                                                        listKey="userId"
                                                        listValue="%{userLastName + ' ' + userFirstName}"
                                                        value=""
                                                        name="lstUser"
                                                        emptyOption="true"/>
                                                    </s:if>'>
    </div><!-- End hidden fields -->
</div>    
<!--End Contents -->
</tiles:putAttribute>
</tiles:insertDefinition>