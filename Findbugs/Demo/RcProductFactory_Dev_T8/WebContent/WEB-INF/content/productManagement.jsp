<%@ page language="java" contentType="text/html; charset=Shift-JIS"
    pageEncoding="Shift-JIS"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<tiles:insertDefinition name="layoutCommon">
<tiles:putAttribute name="css">
    <link rel="stylesheet" type="text/css" href="css/jQuery/kendo.common.min.css?20140804140000" />
    <link rel="stylesheet" type="text/css" href="css/jQuery/kendo.ext.css?20140804140000" />
    <link rel="stylesheet" type="text/css" href="css/jQuery/kendo.default.min.css?20140804140000" />
    <link rel="stylesheet" type="text/css" href="css/common.css?20140804140000" />
    <link rel="stylesheet" type="text/css" href="css/productManagement.css?20140804140000" />
    <link rel="stylesheet" type="text/css" href="css/productManagementDetailSearch.css?20140804140000" />
</tiles:putAttribute>
<tiles:putAttribute name="javaScript">
    <script type="text/javascript" src="js/jQuery/kendo.all.min.js?20140804140000"></script>
    <script type="text/javascript" src="js/jQuery/kendo.web.ext.js?20140804140000" charset="UTF-8"></script>
    <script type="text/javascript" src="js/jQuery/jquery.validate.min.js?20140804140000"></script>
    <script type="text/javascript" src="js/commonJS.js?20140804140000"></script>
    <script type="text/javascript" src="js/productManagementJS.js?20140804140000"></script>
</tiles:putAttribute>
<tiles:putAttribute name="body">
<!-- Contents -->
<div id="contents">
    <script type="text/javascript" src="js/jQuery/selectable.js?20140804140000"></script>
    <div id="content">
      <div id="message">
      </div>
      <!-- Begin hidden fields for page -->

      <input id='pagingDisplay'                type="hidden" value='<s:property value="getText('text.paging.display')"/>'>
      <input id='pagingEmpty'                  type="hidden" value='<s:property value="getText('text.paging.empty')"/>'>
      <input id='pagingItemsPerPage'           type="hidden" value='<s:property value="getText('text.paging.itemsPerPage')"/>'>
      <input id='pagingFirst'                  type="hidden" value='<s:property value="getText('text.paging.first')"/>'>
      <input id='pagingPrevious'               type="hidden" value='<s:property value="getText('text.paging.previous')"/>'>
      <input id='pagingNext'                   type="hidden" value='<s:property value="getText('text.paging.next')"/>'>
      <input id='pagingLast'                   type="hidden" value='<s:property value="getText('text.paging.last')"/>'>

      <input id='titleSyouhinSysCode'                 type="hidden" value='<s:property value="getText('systemProductCode')"/>'>
      <input id='titleBrandName'                      type="hidden" value='<s:property value="getText('brandName')"/>'>
      <input id='titleProductCode'                    type="hidden" value='<s:property value="getText('productCode')"/>'>
      <input id='titleProductName'                    type="hidden" value='<s:property value="getText('productName')"/>'>
      <input id='titleProductGroupCode'               type="hidden" value='<s:property value="getText('groupCode')"/>'>
      <input id='titleSummary'                        type="hidden" value='<s:property value="getText('summary')"/>'>
      <input id='titleHyoujiName'                     type="hidden" value='<s:property value="getText('optionValue')"/>'>
      <input id='titleProductProperPrice'             type="hidden" value='<s:property value="getText('productListPrice')"/>'>
      <input id='titleRate'                           type="hidden" value='<s:property value="getText('divisionRate')"/>'>
      <input id='titleSiireName1'                     type="hidden" value='<s:property value="getText('supplier')"/> 1'>
      <input id='titleSiireName2'                     type="hidden" value='<s:property value="getText('supplier')"/> 2'>
      <input id='titleSupplierStatusName'             type="hidden" value='<s:property value="getText('handlingSituation')"/>'>
      <input id='titleProductMaintenanceFlg'          type="hidden" value='<s:property value="getText('maintainance')"/>'>
      <input id='titleProductStatusName'              type="hidden" value='<s:property value="getText('productStatus')"/>'>
      <input id='titleProductDelFlg'                  type="hidden" value='<s:property value="getText('valid')"/>'>
      <input id='titleFullName'                       type="hidden" value='<s:property value="getText('lastUpdatedPerson')"/>'>
      <input id='titleUpdatedOn'                      type="hidden" value='<s:property value="getText('updatedOn')"/>'>
      <input id='noDataMessage'                       type="hidden" value='<s:property value="getText('text.error.noData')"/>'>
      <input id='noMatterObj'                         type="hidden" value='<s:property value="getText('noMatterObject')"/>'>
      <input id='windowName'                          type="hidden" value='<s:property value="getText('windowName')"/>'>
      <input id='selectWarning'                       type="hidden" value='<s:property value="getText('error')"/>'>
      <input id='csvProcessing'                       type="hidden" value='<s:property value="getText('csvProcessing')"/>'>
      <input id='msgFail'                             type="hidden" value='<s:property value="getText('productManageUpdateFail')"/>'>
      <input id='msgComplete'                         type="hidden" value='<s:property value="getText('productManageUpdateComplete')"/>'>
      <!-- BOE Nguyen.Chuong 2014/03/11: add message warning when choose more than max product of maintenanace action -->
      <input id='confirmYes'                          type="hidden" value='<s:property value="getText('message.confirm.yes')"/>'>
      <input id='maxProductMaintenanceErrorMsg'       type="hidden" value='<s:property value="getText('maxProductMaintenanceActionError')"/>'>
      <input id='maxNumberProductMaintenanceAction'   type="hidden" value='<s:property value="maxNumberProductMaintenanceAction"/>'>
      <!-- Max product export in each time -->
      <input id='maxProductExportCSVErrorMsg'         type="hidden" value='<s:property value="getText('exportCSVOverMaxProductError')"/>'>
      <input id='maxNumberProductExportCSV'           type="hidden" value='<s:property value="maxNumberProductExportCSV"/>'>
      <input id='maintainancedText'                   type="hidden" value='<s:property value="getText('maintainanced')"/>'>
      <input id='hdNoDataMaintenance'                 type="hidden" value='<s:property value="getText('noDataMaintenance')"/>'>
      <input id='hdExecuteOverMaxProductError'        type="hidden" value='<s:property value="getText('executeOverMaxProductError')"/>'>
      <input id="hdDialogTitle"                       type="hidden" value='<s:property value="getText('message.confirm.titleBackToPrevious')"/>'>
      <!-- BOE Nguyen.Chuong 2014/04/04 add message for error list product contain disabled product when maintenace -->
      <input id="enableProductToMaintenanceErrorMsg"  type="hidden" value='<s:property value="getText('enableProductToMaintenance')"/>'>
      <input id="matterOfMaintenanceNotFound"         type="hidden" value='<s:property value="getText('matterOfMaintenanceNotFound')"/>'>
      <input id="matterCodeLabel"                     type="hidden" value='<s:property value="getText('matterNumber')"/>'>
      <input id="matterNameLabel"                          type="hidden" value='<s:property value="getText('matterName')"/>'>
      <input id="matterChangeUserNameLabel"                type="hidden" value='<s:property value="getText('matterChangeUserName')"/>'>
      <input id="productIdLabel"                      type="hidden" value='<s:property value="getText('identificationNumber')"/>'>
      <input id="manyMatterMaintenanceProduct"        type="hidden" value='<s:property value="getText('manyMatterMaintenanceProduct')"/>'>
      <input id="manyProductMaintenanceProduct"       type="hidden" value='<s:property value="getText('manyProductMaintenanceProduct')"/>'>
      <!-- EOE Nguyen.Chuong 2014/04/04 -->
      <!-- EOE Nguyen.Chuong 2014/03/11 -->
      <table id="tblFilterHeader">
        <tr>
          <td></td>
          <!-- BOE by Luong.Dai 2014/04/11 Change maxLength of productSyouhinSysCode from 20 to 18 -->
          <!-- <td><input class="tbSearch k-textbox" type="text" class="filter" maxlength="20" id="tblSyouhinSysCode" /></td> -->
          <td><input class="tbSearch k-textbox" type="text" class="filter" maxlength="18" id="tblSyouhinSysCode" /></td>
          <!-- EOE by Luong.Dai 2014/04/11 Change maxLength of productSyouhinSysCode from 20 to 18 -->
          <td><input class="tbSearch k-textbox" type="text" class="filter" maxlength="50" id="tblBrandName" /></td>
          <td><input class="tbSearch k-textbox" type="text" class="filter" maxlength="100" id="tblProductCode" /></td>
          <td><input class="tbSearch k-textbox" type="text" class="filter" maxlength="512" id="tblProductName" /></td>
          <td><input class="tbSearch k-textbox" type="text" class="filter" maxlength="24" id="tblProductGroupCode" /></td>
          <td><input class="tbSearch k-textbox" type="text" class="filter" maxlength="35010" id="tblDescriptionSummary" /></td>
          <td>
            <label>From</label> <input class="tbSearch k-textbox txt-range" type="text" maxlength="8" id="tblProperPriceFrom" /><br/>
            <label class="lbl-To">To</label> <input class="tbSearch k-textbox txt-range" maxlength="8" type="text" id="tblProperPriceTo" />
          </td>
          <td>
            <label>From</label> <input class="tbSearch k-textbox txt-range" type="text" maxlength="8" id="tblRateFrom" /><br/>
            <label class="lbl-To">To</label> <input class="tbSearch k-textbox txt-range" type="text" maxlength="8" id="tblRateTo" />
          </td>
          <td><input class="tbSearch k-textbox" type="text" class="filter" maxlength="50" id="tblSiireName1" /></td>
          <td><input class="tbSearch k-textbox" type="text" class="filter" maxlength="50" id="tblSiireName2" /></td>
          <td>
            <select class="tbSearch cbbFilter" id="tblSupplierStatusName">
              <option></option>
              <s:iterator value="productManagementActionForm.productSupplierStatusList" status="st">
                <option><s:property value="supplierStatusName"/></option>
              </s:iterator>
            </select>
          </td>
          <td>
            <select class="tbSearch cbbFilter" id="tblProductStatusName" >
              <option></option>
              <s:iterator value="productManagementActionForm.productStatusNameList" status="st">
                <option><s:property/></option>
              </s:iterator>
            </select>
          </td>
          <td>
            <select class="tbSearch cbbFilter" id="tblProductMaintenanceFlg">
              <option value="0"></option>
              <option value="1"><s:property value="getText('maintainanced')"/></option>
              <option value="-1" selected="selected"><s:property value="getText('all')"/></option>
            </select>
          </td>
          <td>
            <select class="tbSearch cbbFilter" id="tblProductDelFlg" >
              <option value="-1"></option>
              <option value="0" selected="selected">Yes</option>
              <option value="1">No</option>
            </select>
          </td>
          <td>
            <select class="tbSearch cbbFilter" id="tblFullName" >
              <option></option>
              <s:iterator value="productManagementActionForm.fullNameList" status="st">
                <option><s:property/></option>
              </s:iterator>
            </select>
          </td>
          <td tabindex="10">
            <div>
              <div>
                <span class="dateTimeLabel">From</span>
                <span>
                  <input class="tbSearch haft-size filter" type="search" id="tblUpdatedFrom" />
                </span>
              </div>
              <div>
                <span class="dateTimeLabel">To</span>
                <span>
                  <input class="tbSearch haft-size filter" type="search" id="tblUpdatedTo" />
                </span>
              </div>
            </div>
          </td>
        </tr>
      </table>
      <!-- End hidden fields for page -->
      <div id="title">
        <h1><s:property value="getText('productManagement')"/></h1>
      </div>
      <div id="contentsInner">
        <!-- start list button div -->
        <div id="lstButton">
          <div id="lstButtonLeft">
<%--             <input type="button" class="btn-color-default lstButton k-button" id="btnPreviewPopup" value="<s:property value="getText('preview')"/>"> --%>
            <input type="button" class="btn-color-default lstButton k-button" id="btnDetailSearch" value="<s:property value="getText('detailSearch')"/>">
            <span id="msAdvanceSearch" style="display:none;"><s:property value="getText('advancedSearchCondition')"/></span>
          </div>
          <div id="lstButtonRight">
            <input type="button" class="btn-color-default lstButton k-button" id="btnReset" value="<s:property value="getText('reset')"/>">
            <input type="button" class="btn-color-default lstButton k-button" id="btnSearch" value="<s:property value="getText('search')"/>">
          </div>
        </div>
        <!-- end list button div -->
        <!-- start action div -->
        <div id="actionDiv">
          <div id="actionDivLeft">
            <label><input type='checkbox' id='selectAllCheckbox'/> <s:property value="getText('selectAll')"/></label>
          </div>
          <div id="actionDivRight">
           <span><s:property value="getText('action')"/></span>
           <select name="statusProcessProduct" class="cbxExcute" id="selectExcute" style="width: 150px;" style="float:right" >
             <option value="-1"></option>
             <option value="0"><s:property value="getText('maintenanceAction')"/></option>
             <option value="1"><s:property value="getText('changeStatusAction')"/></option>
             <option value="2"><s:property value="getText('disableAction')"/></option>
             <option value="3"><s:property value="getText('enableACtion')"/></option>
             <option value="4"><s:property value="getText('csvAction')"/></option>
           </select>
           <input type="button" class="btn-color-default lstButton k-button" id="btnExecute" value="<s:property value="getText('execute')"/>">
          </div>
        </div>
        <!-- end action div -->
        <!-- start table list master -->
        <div id="mainContent" class="">
          <div id="table-content">
            <!-- start table list master content -->
            <div id="grid">
            </div>
            <!-- end table list master content -->
          </div>
        </div>
        <!-- end table list master -->
      </div>
  </div>
    <!-- begin maintenancePopup popup -->
    <div id="maintenancePopup" class="dialog" style="display:none">
      <div class="message">
      <ul>
	      <li>
            <span><s:property value="getText('specifyMatterNoIfYouAddToExistingMatter')"/></span>
          </li>
	      <li>
                <span class="warning"><s:property value="getText('leaveBlankAndPressOkIfWantToCreateMatter')"/></span>
	      </li>
	      <li>
	        <table id="tblMaintenance">
	          <tr>
	            <td class="td-right"><s:property value="getText('matterNumber')"/>:</td>
	            <td class="td-left">
	                 <input id="txtMatterNo" name="txtMatterNo" type="text"
	                      maxLength="16"
	                      data-parameters="getText('matterNumber')"
	                      class="tbSearch k-required k-ajaxCheckExistMatterNo k-textbox" />
	                    <span class="k-widget k-tooltip k-tooltip-validation k-invalid-msg" data-for="txtMatterNo" 
	                              role="alert" style="display: none;"><span class="k-icon k-warning"> </span></span>
	                    <input type="hidden" id="matterNoInvalidFlag" value="0">
	                    <img id="loading-ajax-inline" alt="loading" src="images/loading.gif">
	            </td>
	          </tr>
	          <tr>
	            <td class="td-right"><s:property value="getText('matterName')"/>:</td>
	            <td class="td-left"><span id="spnMatterName"></span></td>
	          </tr>
	          <tr>
	            <td class="td-right"><s:property value="getText('matterChargeUser')"/>:</td>
	            <td class="td-left"><span id="spnMatterUserName"></span></td>
	          </tr>
	        </table>
	      </li>
      </ul>
      </div>
      <div class="buttonArea">
          <input type="button" class="k-button" id="btnMaintenanceOk" value="<s:property value="getText('text.btn.yes')"/>" />
          <input type="button" class="k-button btnCancel" id="btnCancelMaintenance" value="<s:property value="getText('text.btn.cancel')"/>" />
      </div>
    </div>
    <!-- end maintenancePopup popup -->
    <!-- begin confirm maintenance popup -->
    <div id="confirmMaintenancePopup" class="dialog" style="display:none">
      <div class="message">
        <s:property value="getText('confirmMaintenance')"/>
      </div>
      <div class="buttonArea">
        <input type="button" class="k-button" id="btnConfirmMaintenance" value="<s:property value="getText('text.btn.yes')"/>" />
        <input type="button" class="k-button" id="btnCancelConfirmMaintenance" value="<s:property value="getText('text.btn.cancel')"/>" />
      </div>
    </div>
    <!-- end confirm maintenance popup -->
    <!-- begin can not maintenance popup -->
    <div id="cannotMaintenancePopup" class="dialog" style="display:none">
      <div class="message warning">
            <s:property value="getText('cannotMaintenance')"/>
      </div>
      <div class="buttonArea">
        <input type="button" class="k-button" id="btnCannotMaintenance" value='<s:property value="getText('text.btn.yes')"/>' />
      </div>
    </div>
    <!-- end can not maintenance popup -->
    <!-- start CSV popup -->
      <div id="csvPopup" style="display:none">
        <div id="export_header">
            <label><s:property value="getText('selectCsvMode')"/></label>
            <select name="selectCsvMode" id="csvmode" class="cbb-size-normal">
                <!-- <option value="default"></option> -->
                <option value="MODE_1"><s:property value="getText('allData')"/></option>
                <option value="MODE_2"><s:property value="getText('compatibleModel')"/></option>
                <option value="MODE_3"><s:property value="getText('attribute')"/></option>
            </select>
            <input type="button" class="btn-color-default lstButton k-button" id="btnCSVStartExport" 
                value="<s:property value="getText('csvStartExport')"/>"
                style="float: right;width: 100px;height: 25px;">
        </div>
        <div class="pane-content">
            <fieldset id = "exportItems">
              <legend style="text-align: left;"><s:property value="getText('csvOutputItemLabel2')" /></legend>
              <ul id='csv_ul_MODE_1' style="float:left;">
                  <li class="row_legend li_control">
                    <div class="control">
                        <label><input type='checkbox' id='eupdateMode' checked="checked"/>
                        <s:property value="getText('updateMode')" /></label>
                    </div>
                    <div class="control">
                        <label><input type='checkbox' id='eimage1' checked="checked"/>
                        <s:property value="getText('image')" />1</label>
                    </div>
                    <div class="control">
                        <label><input type='checkbox' id='esummary' checked="checked"/>
                        <s:property value="getText('summary')" /></label>
                    </div>
                  </li>
                  <li class="row_legend li_control">
                    <div class="control">
                        <label><input type='checkbox' class="chkDisabled"/>
                        <s:property value="getText('systemProductCode')" /></label>
                    </div>
                    <div class="control">
                        <label><input type='checkbox' id='eimage2' checked="checked"/>
                        <s:property value="getText('image')" />2</label>
                    </div>
                    <div class="control">
                        <label><input type='checkbox' id='enote' checked="checked"/>
                        <s:property value="getText('note')" /></label>
                    </div>
                  </li>
                  <li class="row_legend li_control">
                    <div class="control">
                        <label><input type='checkbox' id='ebrandCode' checked="checked"/>
                        <s:property value="getText('brandCode')" /></label>
                    </div>
                    <div class="control">
                        <label><input type='checkbox' id='eimage3' checked="checked"/>
                        <s:property value="getText('image')" />3</label>
                    </div>
                    <div class="control">
                        <label><input type='checkbox' id='ecaution' checked="checked"/>
                        <s:property value="getText('caution')" /></label>
                    </div>
                  </li>
                  <li class="row_legend li_control">
                    <div class="control">
                        <label><input type='checkbox' id='ebrandName' checked="checked"/>
                        <s:property value="getText('brandName')" /></label>
                    </div>
                    <div class="control">
                        <label><input type='checkbox' id='eimage4' checked="checked"/>
                        <s:property value="getText('image')" />4</label>
                    </div>
                    <div class="control">
                        <label><input type='checkbox' id='eexplanation' checked="checked"/>
                        <s:property value="getText('explanation')" /></label>
                    </div>
                  </li>
                  <li class="row_legend li_control">
                    <div class="control">
                        <label><input type='checkbox' id='eitemCode2' checked="checked"/>
                        <s:property value="getText('itemCode2')" /></label>
                    </div>
                    <div class="control">
                        <label><input type='checkbox' id='eimage5' checked="checked"/>
                        <s:property value="getText('image')" />5</label>
                    </div>
                    <div class="control"></div>
                  </li>
                  <li class="row_legend li_control">
                    <div class="control">
                        <label><input type='checkbox' id='eitemName' checked="checked"/>
                        <s:property value="getText('itemName')" /></label>
                    </div>
                    <div class="control">
                        <label><input type='checkbox' id='eimage6' checked="checked"/>
                        <s:property value="getText('image')" />6</label>
                    </div>
                    <div class="control">
                        <label><input type='checkbox' id='esupplierCode1' checked="checked"/>
                        <s:property value="getText('supplierCode')" />1</label>
                    </div>
                  </li>
                  <li class="row_legend li_control">
                    <div class="control">
                        <label><input type='checkbox' id='eproductName' checked="checked"/>
                        <s:property value="getText('productName')" /></label>
                    </div>
                    <div class="control">
                        <label><input type='checkbox' id='eimage7' checked="checked"/>
                        <s:property value="getText('image')" />7</label>
                    </div>
                    <div class="control">
                        <label><input type='checkbox' id='esupplierName1' checked="checked"/>
                        <s:property value="getText('supplierName')" />1</label>
                    </div>
                  </li>
                  <li class="row_legend li_control">
                    <div class="control">
                        <label><input type='checkbox' id='emakerStockNumber' checked="checked"/>
                        <s:property value="getText('makerStockNumber')" /></label>
                    </div>
                    <div class="control">
                        <label><input type='checkbox' id='eimage8' checked="checked"/>
                        <s:property value="getText('image')" />8</label>
                    </div>
                    <div class="control">
                        <label><input type='checkbox' id='esupplierCode2' checked="checked"/>
                        <s:property value="getText('supplierCode')" />2</label>
                    </div>
                  </li>
                  <li class="row_legend li_control">
                    <div class="control">
                        <label><input type='checkbox' id='enotStockNumber' checked="checked"/>
                        <s:property value="getText('notStockNumber')" /></label>
                    </div>
                    <div class="control">
                        <label><input type='checkbox' id='eimage9' checked="checked"/>
                        <s:property value="getText('image')" />9</label>
                    </div>
                    <div class="control">
                        <label><input type='checkbox' id='esupplierName2' checked="checked"/>
                        <s:property value="getText('supplierName')" />2</label>
                    </div>
                  </li>
                  <li class="row_legend li_control">
                    <div class="control">
                        <label><input type='checkbox' id='ejan' checked="checked"/>
                        <s:property value="getText('jan')" /></label>
                    </div>
                    <div class="control">
                        <label><input type='checkbox' id='eimage10' checked="checked"/>
                        <s:property value="getText('image')" />10</label>
                    </div>
                    <div class="control"></div>
                  </li>
                  <li class="row_legend li_control">
                    <div class="control">
                        <label><input type='checkbox' id='elistPrice' checked="checked"/>
                        <s:property value="getText('listPrice')" /></label>
                    </div>
                    <div class="control"></div>
                    <div class="control">
                        <label><input type='checkbox' id='esupportedBike' checked="checked"/>
                        <s:property value="getText('compatibleModel')" /></label>
                    </div>
                  </li>
                  <li class="row_legend li_control">
                    <div class="control">
                        <label><input type='checkbox' id='edivision' checked="checked"/>
                        <s:property value="getText('division')" /></label>
                    </div>
                    <div class="control">
                        <label><input type='checkbox' id='eoption1' checked="checked"/>
                        <s:property value="getText('option')" />1</label>
                    </div>
                    <div class="control"></div>
                  </li>
                  <li class="row_legend li_control">
                    <div class="control">
                        <label><input type='checkbox' id='egroupCode' checked="checked"/>
                        <s:property value="getText('groupCode')" /></label>
                    </div>
                    <div class="control">
                        <label><input type='checkbox' id='eoption2' checked="checked"/>
                        <s:property value="getText('option')" />2</label>
                    </div>
                    <div class="control">
                        <label><input type='checkbox' id='ecustomerConfirmationItem' checked="checked"/>
                        <s:property value="getText('customerConfirmationItem')" /></label>
                    </div>
                  </li>
                  <li class="row_legend li_control">
                    <div class="control">
                        <label><input type='checkbox' id='edateOfIssue' checked="checked"/>
                        <s:property value="getText('dateOfIssue')" /></label>
                    </div>
                    <div class="control">
                        <label><input type='checkbox' id='eoption3' checked="checked"/>
                        <s:property value="getText('option')" />3</label>
                    </div>
                    <div class="control"></div>
                  </li>
                  <li class="row_legend li_control">
                    <div class="control">
                        <label><input type='checkbox' id='esalePrice' checked="checked"/>
                        <s:property value="getText('salePrice')" /></label>
                    </div>
                    <div class="control">
                        <label><input type='checkbox' id='eoption4' checked="checked"/>
                        <s:property value="getText('option')" />4</label>
                    </div>
                    <div class="control">
                        <label><input type='checkbox' id='elink' checked="checked"/>
                        <s:property value="getText('link')" /></label>
                    </div>
                  </li>
                  <li class="row_legend li_control">
                    <div class="control">
                        <label><input type='checkbox' id='enonReturnable' checked="checked"/>
                        <s:property value="getText('nonReturnable')" /></label>
                    </div>
                    <div class="control">
                        <label><input type='checkbox' id='eoption5' checked="checked"/>
                        <s:property value="getText('option')" />5</label>
                    </div>
                    <div class="control">
                        <label><input type='checkbox' id='evideo' checked="checked"/>
                        <s:property value="getText('video')" /></label>
                    </div>
                  </li>
                  <li class="row_legend li_control">
                    <div class="control">
                        <label><input type='checkbox' id='ebuildToOrderManufacturing' checked="checked"/>
                        <s:property value="getText('buildToOrderManufacturing')" /></label>
                    </div>
                    <div class="control">
                        <label><input type='checkbox' id='eoption6' checked="checked"/>
                        <s:property value="getText('option')" />6</label>
                    </div>
                    <div class="control"></div>
                  </li>
                  <li class="row_legend li_control">
                    <div class="control">
                        <label><input type='checkbox' id='eopenPrice' checked="checked"/>
                        <s:property value="getText('openPrice')" /></label>
                    </div>
                    <div class="control">
                        <label><input type='checkbox' id='eoption7' checked="checked"/>
                        <s:property value="getText('option')" />7</label>
                    </div>
                    <div class="control"></div>
                  </li>
                  <li class="row_legend li_control">
                    <div class="control">
                        <label><input type='checkbox' id='epreferenceImage' checked="checked"/>
                        <s:property value="getText('preferenceImage')" /></label>
                    </div>
                    <div class="control">
                        <label><input type='checkbox' id='eoption8' checked="checked"/>
                        <s:property value="getText('option')" />8</label>
                    </div>
                    <div class="control"></div>
                  </li>
                  <li class="row_legend li_control">
                    <div class="control"></div>
                    <div class="control">
                        <label><input type='checkbox' id='eoption9' checked="checked"/>
                        <s:property value="getText('option')" />9</label>
                    </div>
                    <div class="control"></div>
                  </li>
                  <li class="row_legend li_control">
                    <div class="control"></div>
                    <div class="control">
                        <label><input type='checkbox' id='eoption10' checked="checked"/>
                        <s:property value="getText('option')" />10</label>
                    </div>
                    <div class="control"></div>
                  </li>
              </ul>
              <ul id='csv_ul_MODE_2' style="float:left;display: none;">
                  <li class="row_legend li_control">
                    <div class="control">
                        <label><input type='checkbox' class="mode2CheckBox" />
                        <s:property value="getText('updateMode')" /></label>
                    </div>
                  </li>
                  <li class="row_legend li_control">
                    <div class="control">
                        <label><input type='checkbox' class="mode2CheckBox" />
                        <s:property value="getText('identificationNumber')" /></label>
                    </div>
                  </li>
                  <li class="row_legend li_control">
                    <div class="control">
                        <label><input type='checkbox' class="mode2CheckBox" />
                        <s:property value="getText('systemProductCode')" /></label>
                    </div>
                  </li>
                  <li class="row_legend li_control">
                    <div class="control">
                        <label><input type='checkbox' class="mode2CheckBox" />
                        <s:property value="getText('maker')" /></label>
                    </div>
                  </li>
                  <li class="row_legend li_control">
                    <div class="control">
                        <label><input type='checkbox' class="mode2CheckBox" />
                        <s:property value="getText('carModel')" /></label>
                    </div>
                  </li>
                  <li class="row_legend li_control">
                    <div class="control">
                        <label><input type='checkbox' class="mode2CheckBox" />
                        <s:property value="getText('bikeModel')" /></label>
                    </div>
                  </li>
                </ul>
                <ul id='csv_ul_MODE_3' style="float:left;display: none;">
                  <li class="row_legend li_control">
                    <div class="control">
                        <label><input type='checkbox' class="mode3CheckBox" />
                        <s:property value="getText('updateMode')" /></label>
                    </div>
                  </li>
                  <li class="row_legend li_control">
                    <div class="control">
                        <label><input type='checkbox' class="mode3CheckBox" />
                        <s:property value="getText('identificationNumber')" /></label>
                    </div>
                  </li>
                  <li class="row_legend li_control">
                    <div class="control">
                        <label><input type='checkbox' class="mode3CheckBox" />
                        <s:property value="getText('systemProductCode')" /></label>
                    </div>
                  </li>
                  <li class="row_legend li_control">
                    <div class="control">
                        <label><input type='checkbox' class="mode3CheckBox" />
                        <s:property value="getText('atributeName')" /></label>
                    </div>
                  </li>
                  <li class="row_legend li_control">
                    <div class="control">
                        <label><input type='checkbox' class="mode3CheckBox" />
                        <s:property value="getText('administrativeName')" /></label>
                    </div>
                  </li>
                  <li class="row_legend li_control">
                    <div class="control">
                        <label><input type='checkbox' class="mode3CheckBox" />
                        <s:property value="getText('displayName')" /></label>
                    </div>
                  </li>
                 </ul>
              <fieldset style="width: 225px;float: right;" id='field_csv_export_attr'>
                <legend style="text-align: left;"><s:property value="getText('attribute')" /></legend>
               <s:select list="productManagementActionForm.entMstAttributeList" id="csv_export_attr" emptyOption="true"
                    listKey="attributeCode" listValue="attributeName" cssClass="attrCodeGroupSelect" multiple="multiple" data-placeholder="getText('message.pleaseSelectAttribute')">
               </s:select>
              </fieldset>
            </fieldset>
        </div>
      </div>
    <!-- end CSV popup -->
    <!-- start confirm popup disable -->
    <div id="confirmDisablePopup" class="dialog" style="display:none">
        <div class="message">
            <s:property value="getText('confirmDisableProduct')"/>
        </div>
        <div class="buttonArea">
              <input type="button" class="k-button" id="btnDisable" value="<s:property value="getText('text.btn.yes')"/>" />
              <input type="button" class="k-button btnCancel" value="<s:property value="getText('text.btn.cancel')"/>" />
        </div>
    </div>
    <!-- end confirm popup disable-->
    <!-- start confirm popup enable-->
    <div id="confirmEnablePopup" class="dialog" style="display:none">
       <div class="message">
          <s:property value="getText('confirmEnableProduct')"/>
       </div>
       <div class="buttonArea">
              <input type="button" class="k-button" id="btnEnable" value="<s:property value="getText('text.btn.yes')"/>" />
              <input type="button" class="k-button btnCancel" value="<s:property value="getText('text.btn.cancel')"/>" />
       </div>
    </div>
    <!-- end confirm popup enable-->
    <!-- begin please confirm dialog -->
    <div id="pleaseSelectDialog" class="dialog" style="display:none">
        <div class="message warning">
            <s:property value="getText('nonSelectProductWarning')"/>
        </div>
        <div class="buttonArea">
           <input type="button" class="k-button pleaseSelectDialogOk" value='<s:property value="getText('text.btn.yes')"/>' />
        </div>
    </div>
    <!-- end please confirm dialog -->
    <!-- begin can not enable dialog -->
    <div id="cannotEnableDialog" class="dialog" style="display:none">
        <div class="message warning">
            <s:property value="getText('cannotEnableProduct')"/>
        </div>
        <div class="buttonArea">
            <input type="button" class="k-button btnCancel" value='<s:property value="getText('text.btn.yes')"/>' />
        </div>
    </div>
    <!-- end can not enable dialog -->
    <!-- begin can not disable dialog -->
    <div id="cannotDisableDialog" class="dialog" style="display:none">
        <div class="message warning">
            <s:property value="getText('cannotDisableProduct')"/>
        </div>
        <div class="buttonArea">
            <input type="button" class="k-button btnCancel" value='<s:property value="getText('text.btn.yes')"/>' />
        </div>
    </div>
    <!-- end can not disable dialog -->
    <!-- start warning confirm popup enable -->
    <div id="warningEnablePopup"  class="dialog" style="display:none">
        <div class="message">
          <s:property value="getText('warningEnableProduct')"/>
        </div>
        <div class="buttonArea">
              <input type="button" class="k-button" id="btnWarningEnable" value="<s:property value="getText('text.btn.yes')"/>" />
              <input type="button" class="k-button btnCancel" value="<s:property value="getText('text.btn.cancel')"/>" />
        </div>
    </div>
    <!-- end warning confirm popup enable -->
    <!-- start warning confirm popup disable -->
    <div id="warningDisablePopup"  class="dialog" style="display:none">
        <div class="message">
          <s:property value="getText('warningDisableProduct')"/>
        </div>
        <div class="buttonArea">
           <input type="button" class="k-button" id="btnDisableEnable" value="<s:property value="getText('text.btn.yes')"/>" />
           <input type="button" class="k-button btnCancel" value="<s:property value="getText('text.btn.cancel')"/>" />
        </div>
    </div>
    <!-- end warning confirm popup disable -->
    <!-- start change status popup -->
    <div id="changeStatusPopup" class="dialog" style="display:none">
        <div class="message">
          <s:property value="getText('selectHandlingSituationAfterChange')"/>
          <br/>
          <br/>
          <span id="cbxValidate" class="k-widget k-tooltip k-tooltip-validation k-invalid-msg" 
              data-for="attributeActionForm.attribute.attributeType" style="display:none" role="alert">
          <span class="k-icon k-warning"> </span><s:property value="getText('text.label.fitModelFixPleaseSelectContent')"/></span>
          <br/>
          <select id="cbxPrdSupplierStatus">
            <option value="-1"></option>
             <s:iterator value="productManagementActionForm.productSupplierStatusList" status="st">
                <option value="<s:property value="supplierStatusCode"/>"><s:property value="supplierStatusName"/></option>
              </s:iterator>
          </select>
        </div>
        <div class="buttonArea">
            <input type="button" class="k-button" id="btnUpdateStatus" value="<s:property value="getText('yes')"/>" />
            <input type="button" class="k-button btnCancel" id="" value="<s:property value="getText('text.btn.cancel')"/>" />
        </div>
    </div>
    <!-- end change status popup -->
    <!-- begin can not update status popup-->
    <div id="cannotUpdateStatus" class="dialog" style="display:none">
        <div class="message warning">
            <s:property value="getText('cannotUpdateStatus')"/>
        </div>
        <div class="buttonArea">
            <input type="button" class="k-button pleaseSelectDialogOk" id="btnCannotUpdateStatus" value='<s:property value="getText('text.btn.yes')"/>' />
        </div>
    </div>
    <!-- end can not update status popup -->
    <!-- start warning change status -->
    <div id="warningChangeStatusPopup" class="dialog" style="display:none">
        <div class="message">
          <s:property value="getText('waringUpdateStatus')"/>
        </div>
        <div class="buttonArea">
              <input type="button" class="k-button" id="btnWarningChangeStatus" value="<s:property value="getText('text.btn.yes')"/>" />
              <input type="button" class="k-button btnCancel" value="<s:property value="getText('text.btn.cancel')"/>" />
        </div>
    </div>
    <!-- end warning change status -->
    <!-- begin confirm change status popup-->
    <div id="confirmChangeStatus" class="dialog" style="display:none">
      <div class="message">
        <s:property value="getText('confirmChangingStatusOfSelectedProduct')"/>
      </div>
      <div class="message warning">
            <span id="spnSelectedStatusName"></span>
      </div>
      <div class="buttonArea">
        <input type="button" class="k-button" id="btnConfirmChangeStatus" value="<s:property value="getText('yes')"/>" />
        <input type="button" class="k-button btnCancel" value="<s:property value="getText('message.confirm.no')"/>" />
      </div>
    </div>
    <!-- end confirm maintenance popup -->
    <!-- begin can not update status popup-->
    <div id="allProductsAreImportPopup" class="dialog" style="display:none">
        <div class="message warning">
            <s:property value="getText('allProductsAreImport')"/>
        </div>
        <div class="buttonArea">
            <input type="button" class="k-button" id="btnAllProductsAreImport" value='<s:property value="getText('text.btn.yes')"/>' />
        </div>
    </div>
    <!-- end can not update status popup -->
</div>
<!-- preview popup jsp -->
<jsp:include page="productManagementPreviewPopup.jsp"></jsp:include>

<jsp:include page="productManagementDetailSearch.jsp"></jsp:include>

<!--End Contents -->
</tiles:putAttribute>
</tiles:insertDefinition>