<%@ page language="java" contentType="text/html; charset=Shift-JIS"
    pageEncoding="Shift-JIS"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<tiles:insertDefinition name="layoutCommon">
<tiles:putAttribute name="css">
    <link rel="stylesheet" type="text/css" href="css/jQuery/kendo.ext.css?20140804140000" />
    <link rel="stylesheet" type="text/css" href="css/commonManage.css?20140804140000" />
    <link rel="stylesheet" type="text/css" href="css/productEdit.css?20140804140000" />

<!--     <link rel="stylesheet" href="css/bootstrap.css"> -->
    <link rel="stylesheet" href="css/component/popupUpload.css">

    <link rel="stylesheet" type="text/css" href="css/jQuery/kendo.common-bootstrap.min.css" />
    <link rel="stylesheet" href="css/jquery.fileupload.css">
    <link rel="stylesheet" href="css/jquery.fileupload-ui.css">
    
</tiles:putAttribute>
<tiles:putAttribute name="javaScript">
    <script type="text/javascript" src="js/jQuery/kendo.all.min.js?20140804140000" charset="UTF-8"></script>
    <script type="text/javascript" src="js/jQuery/kendo.web.ext.js?20140804140000" charset="UTF-8"></script>
    <script type="text/javascript" src="js/jQuery/kendo.culture.ja-JP.min.js?20140804140000" charset="UTF-8"></script>
    <script type="text/javascript" src="js/jQuery/jquery.validate.min.js?20140804140000"></script>
    <script type="text/javascript" src="js/jQuery/shortcut.js?20140804140000"></script>
    <script type="text/javascript" src="js/commonJS.js?20140804140000"></script>
    <script type="text/javascript" src="js/productEditJS.js?20140804140000"></script>
</tiles:putAttribute>
<tiles:putAttribute name="body">
<!-- Contents -->
<div id="contents">
    <script type="text/javascript" src="js/jQuery/selectable.js?20140804140000"></script>
    <script type="text/javascript" src="js/jQuery/kendo.custom.upload.js?20140804140000"></script>
    <div id="content" class="content">
    <!-- start of message area -->
     <s:if test="%{form.msgString != null && form.msgString != ''}">
          <div id="message" class="has-message success">
            <s:property value="form.msgString"/>
          </div>
     </s:if>
     <s:else>
        <div id="message">
        </div>
     </s:else>
     <!-- end of message area -->
   
    <!-- end of title -->
    <!-- Start New Block -->
    <div id="contentsInner">
        <!-- start table list master -->
         <!-- start title Product Manage -->
    <div id="title" class="contentInner" style="margin-bottom: 0px;margin-top: 0px;">
        <div id="mainheaderContent" class="headerBlockPE" >
            <h1 class="txtheader">
            <!-- Mode edit --> <!-- button duplicate -->
            <s:set var="flgModeNew" value="%{@net.webike.RcProductFactory.util.Constant@NEW_MODE}"></s:set> 
            <s:if test="#flgModeNew == form.mode">
                <s:property value="getText('aNew')"/>
            </s:if>
            <s:else>
                <s:if test="null != form.product">
                  <s:property value="getText('identificationNumber')"/> / P<span id="productIdH1"><s:property value="form.product.productId"/></span>
                   : 
                   <span id="productSysCodeH1">
                      <s:if test="0 != form.product.productSyouhinSysCode && null != form.product.productSyouhinSysCode ">
                          <s:property value="getText('systemProductCode')"/> / 
                          <a id="viewProductSysCode" style="color: blue;" target="_blank" href='http://www.webike.net/sd/<s:property value="form.product.productSyouhinSysCode"/>/'>
                            <s:property value="form.product.productSyouhinSysCode"/>
                          </a>
                      </s:if>
                      <s:else>
                          <s:property value="getText('text.label.syouhinEqual0')"/>
                      </s:else>
                   </span>
                </s:if>
                <s:else>
                   <s:property value="getText('identificationNumber')"/> / P<span id="productIdH1"><s:property value="form.product.productId"/></span>
                   :  <s:property value="getText('newProduct')"/>
                </s:else>
            </s:else>
            </h1>
            <div class="headerBlockButton">
                <!-- button save -->
                <s:if test="form.product.productRegistrationFlg == 1">
                  <!-- BOE Tran.Thanh 2014/05/09 : remove class  k-state-disabled -->
                  <input type="button" id="btnSave" disabled="disabled" class="k-button btnCrud rightPos" value=" <s:property value="getText('save')"/>">
                  <!-- EOE Tran.Thanh 2014/05/09 : remove class  k-state-disabled -->
                </s:if>
                <s:else>
                  <input type="button" id="btnSave"  class="k-button btnCrud rightPos"     value=" <s:property value="getText('save')"/>">
                </s:else>
                 <!-- Mode edit --> <!-- button duplicate -->
                <s:set var="flgModeEdit" value="%{@net.webike.RcProductFactory.util.Constant@EDIT_MODE}"></s:set> 
                <s:if test="form.mode ==  #flgModeEdit ">
                  <s:if test="form.product.productRegistrationFlg == 1">
                    <!-- BOE Tran.Thanh 2014/05/09 : remove class  k-state-disabled -->
                    <input type="button" id="btnDuplicate" disabled="disabled" class="k-button btnCrud rightPos" value=" <s:property value="getText('replication')"/>">
                    <!-- BOE Tran.Thanh 2014/05/09 : remove class  k-state-disabled -->
                  </s:if>
                  <s:else>
                    <input type="button" id="btnDuplicate" class="k-button btnCrud rightPos" value=" <s:property value="getText('replication')"/>">
                  </s:else>
                </s:if>
                <!-- BOE Tran.Thanh 2014/05/09 : add hidden button delete for Mode New -->
                <s:else>
                    <input type="button" style="display:none;" id="btnDuplicate" class="k-button btnCrud rightPos" value=" <s:property value="getText('replication')"/>">
                </s:else>
                <!-- EOE Tran.Thanh 2014/05/09 : add hidden button delete for Mode New -->
                <!-- button reset -->
                <input type="button" id="btnReset"     class="k-button btnCrud rightPos"   value=" <s:property value="getText('reset')"/>">
                <!-- button delete -->
                <s:if test="form.mode ==  #flgModeEdit ">
                    <s:if test="form.product.productRegistrationFlg == 1">
                      <input type="button" id="btnDelete" disabled="disabled" class="k-button btnCrud rightPos btnRed  k-state-disabled"  value=" <s:property value="getText('delete')"/>">
                    </s:if>
                    <s:else>
                      <input type="button" id="btnDelete"   class="k-button btnCrud rightPos btnRed"  value=" <s:property value="getText('delete')"/>">
                    </s:else>
                </s:if>
                <!-- BOE Tran.Thanh 2014/05/09 : add hidden button delete for Mode New -->
                <s:else>
                    <input type="button" style="display: none;" id="btnDelete" class="k-button btnCrud rightPos btnRed" value=" <s:property value="getText('delete')"/>">
                </s:else>
                <!-- EOE Tran.Thanh 2014/05/09 : add hidden button delete for Mode New -->
                <!-- Button Back -->
                <input type="button" id="btnBack" class="k-button btnCrud rightPos"     value=" <s:property value="getText('back')"/>">
            </div>
            <s:hidden id="hProductGroupCode" value="%{form.factoryProductGeneral.productGroupCode}" ></s:hidden>
            <s:hidden id="hProductMatterNo" value="%{form.productMatterNo}" ></s:hidden>
            <s:hidden id="hTmpImage" value="%{@net.webike.RcProductFactory.util.Constant@FOLDER_TMP_IMAGES_PRODUCT_SENTENCE}" ></s:hidden>
        </div>
    </div>
      
        <!-- start list button div -->
        <div>
        <div style="padding-bottom: 5px;"></div>
            <div id="vertical" style="height:2365px;">
                <div id="top-pane" style="height: 100%; width: 100%;">
                    <div id="horizontal" style="height: 100%; width: 100%;">
                        <div id="left-pane">
                                  <div id="left-pane-vertical" style="height:100%;">
                                      <!-- start block 2 -->
                                      <div id="block2">
                                        <div class="block">
                                          <div class="headerBlock">
                                              <label><s:property value="getText('productImage')"/></label>
                                              <input id="callPopUpBlock2" type="button" class="k-button btnCrud rightPos"
                                                         value="<s:property value="getText('btnEdit')"/>"/>
                                          </div>
                                          <div id="imageBlock">
                                            <input type="hidden" id="noImageURL" value="<s:property value='form.noImageURL'/>">
                                            <input type="hidden" id="listThumbCount" value="<s:property value='form.listImage.size'/>">
                                            <input type="hidden" id="hUrlProductFactoryPath" value="<s:property value='form.urlProductFactoryPath'/>">
                                            <div class="products_main_image_inner">
                                                <div id="imagelistView"></div>
                                                <input type="hidden" id="factoryProductImageDetailJsonStr"
                                                   value="<s:property value="form.factoryProductImageDetailJsonStr"/>">
                                            </div>
                                            <form name="thumb_selector_form">
                                                <div id="listThumb" class="thumb_selector clearfix">
                                                   <div id="imagelistViewThumb"></div>
                                                </div>
                                            </form>
                                        </div>
                                        </div>
                                      </div>
                                      <!-- end block 2 -->
                                      <!-- start block 5 -->
                                      <div id="block5">
                                          <div class="block">
                                            <div class="headerBlock">
                                                <label><s:property value="getText('link')"/></label>
                                                    <input id="callPopUpBlock5" type="button" class="k-button btnCrud rightPos"
                                                           value="<s:property value="getText('addition')"/>"/>
                                                    <input id="deleteListLink" class="btnRed k-button btnCrud rightPos" type="button"
                                                           value="<s:property value="getText('delete')"/>"/>
                                            </div>
                                            <div id="tableListLink" class="gridClass"></div>
                                            <input type="hidden" id="factoryProductLinkJsonStr"
                                                   value="<s:property value="form.factoryProductLinkJsonStr"/>">
                                        </div>
                                      </div>
                                      <!-- end block 5 -->
                                      <!-- start block 6 -->
                                      <div id="block6">
                                          <div class="block">
                                            <div class="headerBlock">
                                                <label><s:property value="getText('video')"/></label>
                                                    <input id="callPopUpBlock6" type="button" class="k-button btnCrud rightPos"
                                                           value="<s:property value="getText('addition')"/>"/>
                                                    <input id="deleteListVideo" class="btnRed k-button btnCrud rightPos" type="button"
                                                           value="<s:property value="getText('delete')"/>"/>
                                            </div>
                                            <div id="tableListVideo" class="gridClass"></div>
                                            <input type="hidden" id="factoryProductVideoJsonStr"
                                                   value="<s:property value="form.factoryProductVideoJsonStr"/>">
                                        </div>
                                      </div>
                                      <!-- end block 6 -->
                                      <!-- start block 7 -->
                                      <div id="block7">
                                          <input id="testData" value="get data" type="button" style="display: none"/>
                                          <div class="block">
                                              <div class="modelDetails">
                                                  <div class="headerBlock" style="display:block;">
                                                      <label><s:property value="getText('modelInfo')"/></label>
                                                  </div>
                                                  <div style="display: flex;">
                                                        <s:textarea id="textAreaDescriptionRemarks"
                                                                    name="form.productDescription.descriptionRemarks"
                                                                    tabindex="11"
                                                                    maxlength="%{@net.webike.RcProductFactory.util.Constant@DESCRIPTION_REMARKS_WARNING_LENGTH}"
                                                                    cssClass="notComma k-input k-length[%{@net.webike.RcProductFactory.util.Constant@DESCRIPTION_REMARKS_LENGTH}] brResolve"
                                                                    data-parameters="getText('productName')">
                                                        </s:textarea>
                                                        <input id="hiddenDescriptionRemarks" type="hidden" value='<s:property value="form.productDescription.descriptionRemarks"/>'/>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
                                      <!-- end block 7 -->
                                      <!-- start block 8 -->
                                      <div id="blockGuestInput">
                                        <div class="block">
                                          <div id='headerBlockGuestInput' class="headerBlock">
                                                <label><s:property value="getText('guestInputInformation')"/></label>
                                                <div class="headerBlockButton">
                                                    <input type="button" id="btnGuestInputNew" class="k-button btnCrud rightPos" value="<s:property value="getText('addition')"/>"/>
                                                    <input type="button" id="btnGuestInputDelete" class="k-button btnCrud btnRed rightPos" value="<s:property value="getText('text.btn.delete')"/>"/>
                                                </div>
                                          </div>
                                          <div id='gridGuestInput' class="gridClass"></div>
                                          <input type="hidden" id="hiddenDataGridGuestInput" value='<s:property value="form.lstProductGuestInputJson"/>'>
                                          <!-- Mode of popup: 0 - New, 1 - Edit -->
                                          <input type="hidden" id="hiddenModeGridGuestInput" value='0'>
                                          <!-- Current selected index row -->
                                          <input type="hidden" id="hiddenRowSelectedGridGuestInput" value='-1'>
                                        </div>
                                      </div>
                                      <!-- end block 8 -->
                                  </div>
                        </div>
                        <div id="right-pane">
                                  <div id="right-pane-vertical" style="height:100%;">
                                      <!-- start block 3 -->
                                      <div id="block3" class="contentInner ">
                                          <ul>
                                              <li class="block3Margin">
                                                  <div style="float:left;">
                                                      <p><s:property value="getText('brand')"/></p>
                                                  </div> 
                                                  <div style="width: 105px;float:left;display: flex;">
                                                      <s:textfield name="form.factoryProductGeneral.productBrandCode" 
                                                               id="infoProductBrandCode" 
                                                               maxLength="%{@net.webike.RcProductFactory.util.Constant@PRODUCT_BRAND_CODE_WARNING_LENGTH}"
                                                               cssClass="k-textbox k-required k-brandCode k-length[%{@net.webike.RcProductFactory.util.Constant@PRODUCT_BRAND_CODE_MAX_LENGTH_INPUT}]"
                                                               data-parameters="getText('brand')">
                                                      </s:textfield>
                                                      <input type="hidden" id="oldProductBrandCode" value='<s:property value="form.factoryProductGeneral.productBrandCode"/>'>
                                                  </div>
                                                  <div id="cbbBrandCodeBlock3" style="width:350px;float:right;display: flex;">
                                                      <s:select
                                                          id="infoBrandList"
                                                          list="form.brandList"
                                                          listKey="brandCode"
                                                          listValue="name"
                                                          value="%{form.factoryProductGeneral.productBrandCode}"
                                                          headerKey="0"
                                                          headerValue=" "
                                                          cssStyle="width:90%;height: 1.35em;"/>
                                                          <div id="errorFilterBrand" class="k-invalid-msg" role="alert" style="display: none;">
                                                          	<a title='<s:property value="getText('filterWrongBrandName')"/>'>
                                                          		<img style="height:16px; width:16px;margin-left:5px;margin-top:5px;"
                                                          			src="./images/agt_action_fail.png" alt="(*)">
                                                         	</a>
                                                         </div>
                                                  </div>
                                              </li>
                                              <li>
                                                   <span style="display:none;"></span>
                                              </li>
                                              <li class="block3Margin" >
                                                <p><s:property value="getText('productName')"/></p>
                                                <s:textfield cssStyle="width:75%; margin-right:4px;"
                                                    name="form.factoryProductGeneral.productName"
                                                    id="infoProductName"
                                                    maxLength="%{@net.webike.RcProductFactory.util.Constant@PRODUCT_NAME_WARNING_LENGTH}"
                                                    cssClass="notComma k-input k-required k-length[%{@net.webike.RcProductFactory.util.Constant@PRODUCT_NAME_MAX_LENGTH_INPUT}]
                                                            nonSpecialChar"
                                                    data-parameters="getText('productName')">
                                                </s:textfield>
                                              </li>
                                              <li class="block3Margin">
                                                    <s:hidden name="form.factoryProductGeneral.productOpenPriceFlg"         id="infoProductOpenPriceFlg"></s:hidden>
                                                    <s:hidden name="form.factoryProductGeneral.productProperSellingFlg"     id="infoProductProperSellingFlg"></s:hidden>
                                                    <s:hidden name="form.factoryProductGeneral.productOrderProductFlg"      id="infoProductOrderProductFlg"></s:hidden>
                                                    <s:hidden name="form.factoryProductGeneral.productNoReturnableFlg"      id="infoProductNoReturnableFlg"></s:hidden>
                                                    <s:hidden name="form.factoryProductGeneral.productAmbiguousImageFlg"    id="infoProductAmbiguousImageFlg"></s:hidden>
                                                    <p><s:property value="getText('variousFlag')"/></p>
                                                    <s:select
                                                      id="infoProductMultiSelectProductFlg"
                                                      list="form.mapProductConditionFlg"
                                                      multiple="multiple"
                                                      cssStyle="width:75%;"
                                                    />
                                              </li>
                                              <li>
                                                  <!-- start block 10 -->
                                                  <div id="category">
                                                      <div class="block" style="display: flex;">
                                                         <div style="float:left;width:110px;">
                                                             <p><s:property value="getText('itemCode2')"/></p> &nbsp;
                                                         </div>
                                                         <div style="width: 105px;float:left;display: flex;">
                                                             <input id="txtCategoryCode"
                                                               name="txtCategoryCode"
                                                               value='<s:property value="form.categoryCode"/>'
                                                               style="width:75px;"
                                                               maxLength="9"
                                                               class="k-required k-textbox k-length[8]"
                                                               data-parameters="<s:property value="getText('itemCode2')"/>"
                                                             />
                                                         </div>
                                                        <!-- Old categoryCode -->
                                                        <input id="oldCategoryCode" type="hidden" value='<s:property value="form.categoryCode"/>'/>
                                                        <div id="categoryNameList" style="width:350px;float:right;display: flex;">
                                                            <s:select id="cbxCategoryName"
                                                                      list="form.listCategory"
                                                                      listKey="categoryCode" 
                                                                      listValue="categoryName"
                                                                      value="%{form.categoryCode}"
                                                                      cssStyle="width:327px;height: 1.35em;border-bottom: 1px solid #DADADA;"/>
                                                           <div id="errorFilterCategory" class="k-invalid-msg" role="alert" style="display: none;">
                                                          		<a title='<s:property value="getText('filterWrongCategoryName')"/>'>
                                                          			<img style="height:16px; width:16px;margin-left:5px;margin-top:5px;"
                                                          				src="./images/agt_action_fail.png" alt="(*)">
                                                         		</a>
                                                         	</div>
                                                        </div>
                                                      </div>
                                                  </div>
                                                  <!-- end block 10 -->
                                              </li>
                                            <li>
                                                <div class="left_content_block">
                                                    <ul>
                                                        <li>
                                                            <span><s:property value="getText('numbers')"/></span>
                                                            <span style="margin-left: 172px;">
                                                                <s:checkbox id="infoProductWebikeCodeFlg" key="form.factoryProductGeneral.productWebikeCodeFlg"
                                                                            cssErrorStyle="margin-right: 2px;margin-top: 2px;"></s:checkbox>
                                                                <s:property value="getText('notStockNumber')"/>
                                                            </span>
                                                        </li>
                                                        <li style="display: flex;">
                                                            <s:textfield cssStyle="width:250px;margin-top:2px;"
                                                                  name="form.factoryProductGeneral.productCode" 
                                                                  id="infoProductCode" 
                                                                  value="%{form.factoryProductGeneral.productCode}"
                                                                  maxLength="%{@net.webike.RcProductFactory.util.Constant@PRODUCT_CODE_WARNING_LENGTH}"
                                                                  cssClass="notComma k-required k-length[%{@net.webike.RcProductFactory.util.Constant@PRODUCT_CODE_LENGTH}]
                                                                            nonSpecialChar"
                                                                  data-parameters="getText('numbers')
                                                                  ">
                                                            </s:textfield>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="right_content_block">
                                                    <ul>
                                                        <li>
                                                            <span><s:property value="getText('ean')"/></span>
                                                        </li>
                                                        <li style="display: flex;">
                                                            <s:textfield cssStyle="width:250px;margin-top:2px;"
                                                                  name="form.factoryProductGeneral.productEanCode"
                                                                  id="infoProductEanCode"
                                                                  maxLength="%{@net.webike.RcProductFactory.util.Constant@PRODUCT_EAN_CODE_WARNING_LENGTH}"
                                                                  cssClass="notComma k-length[%{@net.webike.RcProductFactory.util.Constant@PRODUCT_EAN_CODE_LENGTH}]
                                                                            nonSpecialChar"
                                                                  data-parameters="getText('ean')">
                                                            </s:textfield>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="left_content_block">
                                                    <ul>
                                                        <li>
                                                            <span><s:property value="getText('summary')"/></span>
                                                        </li>
                                                        <li>
                                                            <s:textarea cssStyle="width:250px;height:167px;margin-top:2px;"
                                                                        name="form.factoryProductGeneral.productDescriptionSummary"
                                                                        id="infoProductDescriptionSummary"
                                                                        cols="20" rows="1"
                                                                        maxLength="%{@net.webike.RcProductFactory.util.Constant@DESCRIPTION_SUMMARY_WARNING_LENGTH}"
                                                                        cssClass="notComma k-input k-length[%{@net.webike.RcProductFactory.util.Constant@DESCRIPTION_SUMMARY_LENGTH}] brResolve
                                                                                nonSpecialChar"
                                                                        data-parameters="getText('summary')">
                                                            </s:textarea>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="right_content_block">
                                                    <ul>
                                                        <li>
                                                            <span><s:property value="getText('caution')"/></span>
                                                        </li>
                                                        <li>
                                                            <s:textarea cssStyle="width:250px;height:167px;margin-top:2px;"
                                                                        name="form.factoryProductGeneral.productDescriptionCaution"
                                                                        id="infoproductDescriptionCaution"
                                                                        cols="20" rows="1"
                                                                        maxLength="%{@net.webike.RcProductFactory.util.Constant@DESCRIPTION_CAUTION_WARNING_LENGTH}"
                                                                        cssClass="notComma k-input k-length[%{@net.webike.RcProductFactory.util.Constant@DESCRIPTION_CAUTION_LENGTH}] brResolve 
                                                                                    nonSpecialChar"
                                                                        data-parameters="getText('caution')">
                                                            </s:textarea>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </li>
                                          </ul>
                                      </div>
                                      <!-- end of block 3 -->
                                      <!-- start block 4 -->
                                      <div id="block4">
                                          <div style="padding:5px">
                                            <div id="headerContent" class="headerBlock" >
                                                <label><s:property value="getText('attribute')"/></label>
                                                <div class="headerBlockButton">
                                                    <input type="button" id="btnNewAttribute" value="<s:property value="getText('addition')"/>" class="k-button btnCrud rightPos">
                                                    <input type="button" id="btnDelAttribute" value="<s:property value="getText('delete')"/>" class="k-button btnCrud btnRed rightPos ">
                                                </div>
                                            </div>
                                            <div id="gridAttribute" class="gridClass"></div>
                                          </div>
                                      </div>
                                      <!-- end of block 4 -->
                                      <!-- start block 9 -->
                                      <div id="blockSupplierGroup">
                                          <div id="singleProduct">
                                              <div id="groupProductInfoSimple" style="height: 60px;padding: 5px;">
                                                <ul style="height: 50px; padding-left: 5px;">
                                                    <li style="height: 25px;">
                                                        <p style="width: 100px;"><s:property value="getText('group')"/></p>
                                                        <p style="width: 100px;"><s:property value="getText('listPriceWithTax')"/></p>
                                                        <p style="width: 190px;"><s:property value="getText('partition')"/></p>
                                                        <p style="width: 130px;"><s:property value="getText('partitionRate')"/></p>
                                                    </li>
                                                    <li>
                                                        <p id="groupProductDetails" style="width: 98%">
                                                            <input type="checkbox" id="chkSingleProdFlg" value="Change">
                                                            <span  style="width: 80px;"><s:property value="getText('labelGrouping')"/></span>
                                                            <input style="width: 100px;margin-left:15px;" type="text" maxLength="8" id="tbGroupPriceProperPrice" value='<s:property value="form.productPrice.productProperPrice"/>'>
                                                            <input type="hidden" id="oldGroupPriceProperPrice" value='<s:property value="form.productPrice.productProperPrice"/>'>
                                                            <input type="hidden"    id="hdProperPrice" value='<s:property value="form.productPrice.productProperPrice"/>'>
                                                            <input style="width: 184px;margin-left:6px;" type="text" maxLength="8" id="tbGroupPriceSupplierPrice" value='<s:property value="form.productPrice.supplierPricePrice"/>'>
                                                            <input style="width: 130px;margin-left:6px;" type="text" maxLength="6" id="tbGroupPricePriceRate">
                                                        </p>
                                                    </li>
                                                </ul>
                                               </div>
                                          </div>
                                          <div id="groupProduct">
                                                <div id="groupProductInfo" style="height: 60px;padding: 5px;">
                                                    <ul style="height: 50px; padding-left: 5px;">
                                                        <li style="height: 25px;">
                                                            <p style="width: 100px;"><s:property value="getText('group')"/></p>
                                                            <p style="width: 100px;"><s:property value="getText('groupCode')"/></p>
                                                            <p style="width: 190px;"><s:property value="getText('listPriceWithTax')"/></p>
                                                            <p style="width: 130px;"><s:property value="getText('option')"/></p>
                                                        </li>
                                                        <li style="height: 25px;">
                                                            <p id="groupProductDetails" style="width: 98%">
                                                                <input type="checkbox" id="chkGroupProdFlg" value="Change">
                                                                <span  style="width: 90px;"><s:property value="getText('labelGrouping')"/></span>
                                                                <!-- BOE Luong.Dai 2014/04/11 Add maxLength -->
                                                                <input type="text" id="tbGroupPriceGroupCode" maxlength="18" style="padding: 0px; margin-left: 5px;width: 105px;"/>
                                                                <!-- EOE Luong.Dai 2014/04/11 Add maxLength -->
                                                                <span id="spGroupPriceMinPrice" style="width: 80px;">0</span><span>~</span><span id="spGroupPriceMaxPrice" style="width: 80px;">0</span>
                                                                <input style="height: 25px;margin-left: 38px;width: 130px;" type="button" id="btnGroupPriceAddSelect" value="<s:property value="getText('editChoice')"/>" class="k-button btnCrud"/>
                                                            </p>
                                                        </li>
                                                    </ul>
                                                    
                                                </div>
                                                <!--  button of grid -->
                                                <div class="headerBlock">
                                                    <span id="messageGroupErrorValue" class="error"></span>
                                                    <input type="button" id="btnGroupAdd" class="k-button btnCrud rightPos" value="<s:property value="getText('groupAdd')"/>" />
                                                    <input type="button" id="btnGroupDelete"
                                                           style="margin-right: 5px;"
                                                           class="k-button btnCrud rightPos btnRed" value="<s:property value="getText('deleteGroupBlock9')"/>"/>
                                                </div>
                                                <div id="gridGroupProduct">
                                                </div>
                                          </div>
                                          <!-- Hidden field -->
                                          <input type="hidden" id="defaultFlag"   value='<s:property value="form.defaultFlagString"/>'>
                                          <input type="hidden" id="defaultMarume" value='<s:property value="form.marumeCode"/>'>
                                          <input type="hidden" id="hiddenListSelect" value='<s:property value="form.listSelectJson"/>'>
                                          <input type="hidden" id="hDuplicateMsg" 
                                            value="<s:property value="getText('message.error.duplicateValue', 
                                           {getText('text.grid.maker') + ', ' +getText('text.title.modelDisplacement') + ', ' + getText('text.grid.model')} )"/>"/>
                                          <input type="hidden" id="hdPleaseSelectMsg" value='<s:property value="getText('pleaseSelect')"/>'>
                                          <input type="hidden" id="hdANew" value='<s:property value="getText('aNew')"/>'>
                                          <input type="hidden" id="brandMarumeCode" value='<s:property value="form.brandMarumeCode"/>'>
                                        </div>
                                      <!-- end of block 9 -->
<!--                                       start block 10 -->
<!--                                       <div id="category"> -->
<!--                                           <div class="block" style="display: flex;"> -->
<%--                                             <label style="margin-left: 10px; margin-top:5px;"><s:property value="getText('itemCode2')"/></label> &nbsp; --%>
<!--                                             <input id="txtCategoryCode" -->
<!--                                                    name="txtCategoryCode" -->
<%--                                                    value='<s:property value="form.categoryCode"/>' --%>
<!--                                                    maxLength="9" -->
<!--                                                    class="k-required k-textbox k-length[8]" -->
<%--                                                    data-parameters="<s:property value="getText('itemCode2')"/>" --%>
<!--                                             /> -->
<!--                                             Old categoryCode -->
<%--                                             <input id="oldCategoryCode" type="hidden" value='<s:property value="form.categoryCode"/>'/> --%>
<!--                                             <div style="float:right;margin-left: 29px;"> -->
<%--                                                 <label><s:property value="getText('itemName')"/></label> &nbsp; --%>
<%--                                                 <s:select id="cbxCategoryName" list="form.listCategory" listKey="categoryCode"  --%>
<%--                                                       listValue="categoryName" value="%{form.categoryCode}" cssStyle="width:260px;"/> --%>
<!--                                             </div> -->
<!--                                           </div> -->
<!--                                       </div> -->
<!--                                       end block 10 -->
                                      <!-- start block 11 -->
                                      <div id="block11">
                                        <div class="block">
                                            <!-- Begin hidden fields -->
                                            <div>
                                                <input type="hidden" id="hAllModelMaker"           value="<s:property value="getText('text.grid.maker')" />"/>
                                                <input type="hidden" id="hAllModelDisplacement"    value="<s:property value="getText('text.title.modelDisplacement')" />"/>
                                                <input type="hidden" id="hAllModelName"            value="<s:property value="getText('text.grid.model')" />"/>
                                                <input type="hidden" id="hListFitModelJsonStr"  name="form.listFitModelJsonStr" value='<s:property value="form.listFitModelJsonStr" />' />  
                                           
                                            </div><!-- End hidden fields -->
                                             <div class="headerBlock">
                                                     <label>
                                                         <s:property value="getText('carModel')"/>
                                                     </label>
                                                     <div class="headerBlockButton">
                                                         <form id="" action="" method="POST" >
                                                             <input id="btnNewFitModel" type="button" class="k-button btnCrud rightPos" value="<s:property value="getText('addition')"/>" />
                                                            <input id="btnDeleteFitModel" type="button" class="k-button btnCrud btnRed rightPos" value="<s:property value="getText('text.btn.delete')"/>"/>
                                                         </form>
                                                     </div>
                                             </div>
                                          <div id="gridFitModel" class="gridClass"></div>
                                        </div>
                                      </div>
                                      <!-- end block 11 -->
                                      <!-- start block 12 -->
                                      <div id="block12">
                                        <div class="block">
                                       <input type="hidden" id="factoryProductSupplierJsonStr" value="<s:property value="form.factoryProductSupplierJsonStr"/>">
                                       <input type="hidden" id="hDupSiirOrder" value="<s:property value="getText('message.DupSiireOrder')"/>">
                                       <div class="headerBlock">
                                            <label>
                                                <s:property value="getText('supplier')"/>
                                            </label>
                                            <div class="headerBlockButton">
                                                <form id="" action="" method="POST" >
                                                    <input id="btnNewSupplier" type="button" class="k-button btnCrud rightPos " value="<s:property value="getText('addition')"/>"/>
                                                    <input id="btnDeleteSupplier" type="button" class="k-button btnCrud btnRed rightPos " value="<s:property value="getText('text.btn.delete')"/>"/>
                                                </form>
                                            </div>
                                       </div>
                                       <div id="gridFactoryProductSupplier" class="gridClass"></div>
                                       </div>
                                      </div>
                                      <!-- end block 12 -->
                                      <!-- start block 13 -->
                                      <div id="block13">
                                        <div class="block">
                                          <div id="releaseDateSituation">
                                              <div class="left_content_block" style="border:none;width:210px;display: flex;">
                                                  <s:property value="getText('dateOfIssue')"/>
                                                  <input id="productSupplierReleaseDate"
                                                        maxlength="60"
                                                        class="notComma k-textbox k-length[30] nonSpecialChar"
                                                        name="productSupplierReleaseDate"
                                                        data-parameters='<s:property value="getText('dateOfIssue')"/>'
                                                        value='<s:property value="form.factoryProductGeneral.productSupplierReleaseDate"/>'/>
                                              </div>
                                              <div class="right_content_block_13" style="border:none;">
                                                  <s:property value="getText('productStatus')"/>
                                                  <s:select
                                                      id="productSupplierStatus"
                                                      list="form.listProductSupplierStatus"
                                                      listKey="supplierStatusCode"
                                                      listValue="supplierStatusName"
                                                      value="%{form.factoryProductGeneral.productSupplierStatusCode}"
                                                  />
                                              </div>
                                          </div>
                                        </div>
                                      </div>
                                      <!-- end block 13 -->
                                  </div>
                        </div>
                    </div>
                </div>
                <!-- start block 14 -->
                <div id="middle-pane" style="padding: 5px">
                    <div class="headerBlock14">
                      <s:property value="getText('explanation')"/>
                      <div id="errorCommaIconForBlock14">
                        <a title='<s:property  value="getText('notComma')"/>' style='float:right;'>
                            <img style="background: white;height:16px; width:16px;margin-left:5px;margin-top:5px;" src='./images/agt_action_fail.png'/>
                        </a>
                      </div>
                    </div>
                    <div id="productEditorDiv">
                        <input id='msUploadImageSize' type='hidden' value='<s:property value="getText('message.maximun5MBFile')"/>'/>
                        <input id='msUploadDiskSize' type='hidden' value='<s:property value="getText('message.maximun30MBFileSize')"/>'/>
                        <input id='hMaxUploadFolderMatter' type='hidden' value='<s:property  value="form.maxUploadFolderMatter"/>'/>
                        <input id='hMaxUploadImageSize' type='hidden' value='<s:property  value="form.maxUploadImage"/>'/>
<%--                         <input id='msUpload30MBSize' type='hidden' value='<s:property value="getText('message.maximun30MBFileSize')"/>'/> --%>
                        <textarea id="productEditor" class="notComma" rows="20" cols="20" name="productEditor">
                            <s:property  value="form.productDescription.descriptionSentence"/>
                        </textarea>
                    </div>

                </div>
                 <!-- end block 14 -->
                  <!-- start block 15 -->
                <div id="bottom-pane">
                  <div style="padding:5px;">
                    <div class="pane-content">
                        <div id="headerProduct">
                            <s:property value="getText('managementProducts')"/>
                            <div class="buttonArea">
                                <!-- BOE Nguyen.Chuong 2014/04/22 add button next and previous -->
                                <input type="button" id="btnPrevious"           value="<s:property value="getText('previous')"/>"   class=" btn-color-default lstButton k-button btnCrud">&nbsp;
                                <input type="button" id="btnNext"               value="<s:property value="getText('next')"/>"       class=" btn-color-default lstButton k-button btnCrud">&nbsp;
                                <!-- EOE Nguyen.Chuong 2014/04/22 add button next and previous -->
                                <input type="button" id="btnBlockProductReset"  value="<s:property value="getText('reset')"/>"      class=" btn-color-default lstButton k-button btnCrud">
                                <input type="button" id="btnBlockProductSearch" value="<s:property value="getText('search')"/>"     class=" btn-color-default lstButton k-button btnCrud">&nbsp;
                                <s:property value="getText('action')"/>&nbsp;
                                <select name="statusProcessProduct" id="cbxBlockProductSelectFunction" >
                                    <option value="-1"></option>
                                    <option value="0"><s:property value="getText('delete')"/></option>
                                    <option value="1"><s:property value="getText('permutationColmn')"/></option>
                                    <option value="2"><s:property value="getText('release')"/></option>
                                    <option value="3"><s:property value="getText('activeCanrelease')"/></option>
                                </select>
                                <input type="button" class="btn-color-default lstButton k-button btnCrud " id="btnBlockProductExcute" value="<s:property value="getText('execute')"/>">
                            </div>
                        </div>
                        <div>
                            <div id="gridProduct">
                            </div>
                        </div>
                    </div>
                  </div>
                </div>
                 <!-- end block 15 -->
            </div>
        </div>
        <br/>
    </div>
    <!-- End New Block -->
    </div>
    <!-- Begin hidden fields -->
    <div>
        <!-- Start Common Hidden -->
        <!-- No data message -->
        <input id='noDataMessage'           type="hidden" value='<s:property value="getText('text.error.noData')"/>'>
        <input id='requiredFill'            type="hidden" value='<s:property value="getText('message.error.requiredFill')"/>'>
        <input id='urlFormat'               type="hidden" value='<s:property value="getText('message.error.changeToCorrectUrl')"/>'>
        <input id='maxLengthMsg'            type="hidden" value='<s:property value="getText('warningLength')"/>'>
        <input id='hUnChange'               type="hidden" value='<s:property value="getText('message.error.unChange')"/>'>
        <input id='hDialogTitle'            type="hidden" value='<s:property value="getText('message.confirm.titleBackToPrevious')"/>'>
        <input id='hBrandCodeNotExists'     type="hidden" value='<s:property value="getText('message.error.brandCodeNotExists')"/>'>
        <input id='hCategoryCodeNotExists'  type="hidden" value='<s:property value="getText('message.warning.categoryNotExists')"/>'>
        <input id='noProduct'  				type="hidden" value='<s:property value="getText('noProduct')"/>'>
        <input id='hMax10Image'  type="hidden" value='<s:property value="getText('max10Image')"/>'>
        <!-- End Common Hidden -->
        <!-- jSon product -->
        <input type="hidden" id="hProductJson" value="<s:property value="form.productJson"/>">
        <input type="hidden" id="hMode" value="<s:property value="form.mode"/>">
        <!-- Block 05 -->
        <input type="hidden" id="hCreateLink" value="<s:property value="getText('btnCreateLink')"/>">
        <!-- Block 06 -->
        <input type="hidden" id="hTitleBlock6Dialog" value="<s:property value="getText('video')"/>">
        <input type="hidden" id="hErrorMessageBlock6Dialog" value="<s:property value="getText('message.error.urlYoutube')"/>">
        <input type="hidden" id="hChange" value="<s:property value="getText('change')"/>">
        <!-- Block 10 -->
        <!-- productId -->
        <input type="hidden" id="hiddenProductId" value='<s:property value="form.productId"/>'>
        <input type="hidden" id="hiddenProductIdDuplicate" value='<s:property value="form.productIdDupliCate"/>'>
        <input type="hidden" id="mode" value='<s:property value="form.mode"/>'>
        <!-- Block 10 -->
        <!-- Block 15 -->
        <input id='errorCountGreater0'    type="hidden" value='<s:property value="getText('text.label.errorCountEqual1')"/>'>
        <input id='errorCountEqual0'      type="hidden" value='<s:property value="getText('text.label.errorCountEqual0')"/>'>
        <input id='registrationFlgEqual1' type="hidden" value='<s:property value="getText('text.label.grayRegistrationFlg')"/>'>
        <input id='oneErrorCountGreater0' type="hidden" value='<s:property value="getText('text.label.redRegistrationFlg')"/>'>
        <input id='allErrorCountEqual0'   type="hidden" value='<s:property value="getText('text.label.matterRegistrationFlg3')"/>'>
        <!-- List Registration Flag -->
        <input id='hiddenRegistrationFlg' type="hidden" value='<s:select id="cbbRegistrationFlg"
                                                                cssClass="cbbFilter" 
                                                                headerKey="" 
                                                                headerValue="" 
                                                                list="form.lstRegistrationFlg"/>'>
        <!-- End of Block 15 -->
    </div>
    <!-- End hidden fields -->
</div>
<!--End Contents -->
</tiles:putAttribute>
</tiles:insertDefinition>
<!-- Start Dialog Block 2 -->

<!-- begin display image Product dialog -->

<script type="text/x-kendo-template" id="templatePopupBlock2">
    <div class="rowImagePopup">
        <div class="dragArea" style="background-color: white; height: 119px;"></div>
        <div class="listImagePopup" style="boder: 3px;">
            <div class="imageLeft">
                <div class="imageLeftCoumn1">
                    <h3>#:imageSort#</h3>
                        <img id="imageThumbPopup#:imageId#" class="imageThumbPopup"  alt="#imageId#" src="#:productThumb#">
                </div>
                <div class="imageLeftCoumn2">
                    <img id="imageDetailPopup#:imageId#" class="imageDetailPopup" src="#:productImage#" alt="#:imageId#">
                </div>
            </div>
            <div class="legendMid">
                <fieldset>
                    <legend style="text-align: left;"><s:property value="getText('references')"/></legend>
                    <ul>
                                #if(data.referMode =="real"){#
                                    <li class="row_legend row1">
                                        <input type="radio" class="radTemp" name="radReferModeThumb#:imageId#" value='temp'/>
                                        <label><s:property value="getText('aNew')"/></label>
                                    </li>
                                    <li class="row_legend row2">
                                        <input type="radio" class="radReal" name="radReferModeThumb#:imageId#" checked="checked" value='real'/>
                                        <label><s:property value="getText('production')"/></label>
                                    <li>
                                #} else {#
                                    <li class="row_legend row1">
                                        <input type="radio" class="radTemp" name="radReferModeThumb#:imageId#" checked="checked" value='temp'/>
                                        <label><s:property value="getText('aNew')"/></label>
                                    </li>
                                    <li class="row_legend row2">
                                        <input type="radio" class="radReal" name="radReferModeThumb#:imageId#" value='real'/>
                                        <label><s:property value="getText('production')"/></label>
                                    </li>
                                #}#
                    </ul>
                </fieldset>
            </div>
            <div class="legendRight">
                <fieldset>
                    <legend style="text-align: left;"><s:property value="getText('thumbnails')"/></legend>
                    <ul>
                        <li class="row_legend">
                            <div class="rowImagePopupContent">
                                <input type="hidden" class="imageId" value="#:imageId#">
                                <input id="txtImageThumbPopup#:imageId#"
                                       name="txtImageThumbPopup#:imageId#"
                                       type="text" class="nonSpecialChar k-textbox textboxThumbPopupBlock2 notComma"
                                       value='#:productImageThumbnailPath#'
                                       data-parameters="<s:property value="getText('thumbnails')"/>"/>
                                <a role='button' class='k-tool k-group-end' unselectable='on'>
                                    <span id="TimageBrowser#:imageId#"
                                        class='image-browser borderImageButton' style="margin-left:3px;"
                                        onclick="openDialogUploadImagePopup('TimageBrowser#:imageId#');">Insert Image</span>
                                </a>
                                <input id="btnImageThumbPopup#:imageId#" type="button" style="margin-left:3px;" class="k-button btnCrud btnImageAdaptedThumb" value="<s:property value="getText('change')"/>">
                        </li>
                    </ul>
                </fieldset>
                <fieldset>
                    <legend style="text-align: left;"><s:property value="getText('text.label.matterDetailCase1')" /></legend>
                    <ul>
                        <li class="row_legend">
                            <div class="rowImagePopupContent">
                                <input type="hidden" class="imageId" value="#:imageId#">
                                <input id="txtImageDetailPopup#:imageId#"
                                       name="txtImageDetailPopup#:imageId#"
                                       type="text" class="k-required nonSpecialChar k-textbox textboxDetailPopupBlock2 notComma"
                                       value='#:productImageDetailPath#'
                                       data-parameters="<s:property value="getText('text.label.matterDetailCase1')" />"/>
                                <a role='button' class='k-tool k-group-end' unselectable='on'>
                                    <span id="DimageBrowser#:imageId#"
                                            class='image-browser borderImageButton' style="margin-left:3px;"
                                            onclick="openDialogUploadImagePopup('DimageBrowser#:imageId#');">Insert Image</span>
                                </a>
                                <input id="btnImageDetailPopup#:imageId#" type="button" style="margin-left:3px;" class="k-button btnCrud btnImageAdaptedDetail" value="<s:property value="getText('change')"/>">
                        </li>
                    </ul>
                </fieldset>
            </div>
            <span class="k-link k-icon k-i-close btnDeletePopup forDeleted"><input type="hidden" class="imageId" value="#:imageId#"></span>
        </div>
    </div>
</script>
<div id="imgProductDialog" class="contentInner">
    <div class="headerContent" style="height: 20px;">
        <div class="headerBlockButton">
            <input id="btnInsertNewImageList" type="button" class="k-button btnCrud" value="<s:property value="getText('addition')"/>">
            <input id="btnCancelSaveImageList" type="button" class="k-button btnCrud" value="<s:property value="getText('cancel')"/>">
            <input id="btnSaveImageList" type="button" class="k-button btnCrud" value="<s:property value="getText('decide')"/>">
        </div>
    </div>
    <div id="listView"></div>
</div>
<!-- end display image Product dialog -->
<!-- begin display image ProductImage in ListView -->
<script type="text/x-kendo-template" id="templateImageBlockx">
<div class="imageDetail">
    <a id="imageAhref_#:productImage#" rel="fancy_box" href="#:productImage#" class="fancy_box product">
        <div class="product-description">
            <h3>#:productImage#</h3>
            <p>#:productImage#</p>
        </div>
        <img  id="imageThumb" src="#:productImage#" alt="No Image">
    </a>
</div>
</script>
<script type="text/x-kendo-template" id="templateImageBlock2">
<div class="viewImageContainer">
<div class="viewImage">
    <a id="imageAhref_#:productImage#" alt="No Image" rel="fancy_box" href="#:productImage#"
        title=" #if(referMode =="real"){#
                 <s:property value="getText('references')"/>   ：<s:property value="getText('production')"/>
                #} else {#
                 <s:property value="getText('references')"/>  ：<s:property value="getText('aNew')"/>
                #}#
                </br> <s:property value="getText('thumbnail')"/> ：#:productImageDetailPath# 
                </br> <s:property value="getText('detail')"/>    ：#:productImageThumbnailPath#"
        class="fancy_box product fancy">
        <div class="product-description">
            #if(referMode =="real"){#
                    <h3><s:property value="getText('references')"/>：<s:property value="getText('production')"/></h3>
                    <p> <s:property value="getText('thumbnail')"/> ：#:productImageDetailPath#</p>
            #} else {#
                    <h3> <s:property value="getText('references')"/> ： <s:property value="getText('aNew')"/></h3>
                    <p>  <s:property value="getText('thumbnail')"/>  ：#:productImageDetailPath#</p>
            #}#
            
        </div>
        <img  id="imageThumb" src="#:productImage#" alt="No Image">
    </a>
</div>
</div>
</script>
<!-- end display image ProductImage in ListView -->
<!-- begin display image ProductThumb in ListView -->
<script type="text/x-kendo-template" id="templateImageThumbBlock2">
    <li thumbid="0" class="list_thumb" style="display: list-item;">
        <a href="#:productThumb#"
            class="image_selected" rel="fancy_box"
            alt="#:imageId#">
            <img class="selector_thumb" src="#:productThumb#" alt="#:imageId#">
        </a>
    </li>
</script>
<!-- end display image ProductThumb in ListView -->
<!-- End Dialog Block 2 -->
<!-- Start Dialog Block 5 -->
<!-- begin display Video Product dialog -->
<div id="linkProductDialog" class="contentInner">
    <ul style="padding-left:4px;">
       <li style="display: flex;margin-bottom: 2px;">
           <p><span><s:property value="getText('linkReason')"/></span><span class="asteriskRequire">*</span></p>
           <s:select id="cbbLinkReasonName"
                     name="cbbLinkReasonName"
                                list="form.listMstLinkReason"
                                listKey="linkReasonCode"
                                listValue="linkReasonName"
                                cssClass="cbbFilter k-required"
                                value=""
                                headerKey="" 
                                headerValue=""
                                data-parameters="getText('linkReason')"/>
           <div id='errorBlock5' class="k-invalid-msg" data-for="cbbLinkReasonName"></div>
           <input type="button" id="btnDecide" class="k-button btnPopup" style="float:right;margin-left: 5px !important;width:80px;"
                value="<s:property value="getText('decide')"/>"/>
       </li>
       <li style="display: flex;margin-bottom: 2px;">
           <p><span><s:property value="getText('displayName')"/></span><span class="asteriskRequire">*</span></p>
           <s:textfield id="txtLinkTitlePopup"
                        name="txtLinkTitlePopup"
                        maxLength="%{@net.webike.RcProductFactory.util.Constant@PRODUCT_LINK_TITLE_WARNING_LENGTH}"
                        cssClass="notComma nonSpecialChar k-required txtUrl k-textbox k-required k-length[%{@net.webike.RcProductFactory.util.Constant@PRODUCT_LINK_TITLE_LENGTH}]"
                        data-parameters="getText('displayName')">
           </s:textfield>
       </li>
       <li style="display: flex;margin-bottom: 2px;">
           <p><span><s:property value="getText('url')"/></span><span class="asteriskRequire">*</span></p>
           <s:textfield id="txtUrlPopup"
                        name="txtUrlPopup"
                        maxLength="%{@net.webike.RcProductFactory.util.Constant@PRODUCT_LINK_URL_WARNING_LENGTH}"
                        cssClass="notComma k-textbox k-required txtboxPopup txtUrl k-url
                                  k-length[%{@net.webike.RcProductFactory.util.Constant@PRODUCT_LINK_URL_LENGTH}]"
                        data-parameters="getText('url')">
           </s:textfield>
       </li>
       <li style="display: flex;margin-bottom: 2px;">
           <div class="headerContent" style="width: 328px; padding:5px; text-align:left">
               <label><s:property value="getText('linkGenerators')"/></label>
           </div>
       </li>
       <li style="display: flex;margin-bottom: 2px;">
           <p><span><s:property value="getText('productName')"/></span></p>
           <s:textfield id="txtSyouhinName"
                        name = "txtSyouhinName"
                        cssClass="notComma nonSpecialChar k-textbox txtboxPopup txtUrl"
                        maxlength="255"
                        data-parameters="getText('productName')"></s:textfield>
       </li>
       <li style="display: flex;margin-bottom: 2px;">
           <p><span><s:property value="getText('labelProductsManufacturer')"/></span></p>
           <s:textfield id="txtSyouhinMaker"
                        name = "txtSyouhinMaker"
                        cssClass="notComma nonSpecialChar k-textbox txtboxPopup txtUrl"
                        maxlength="255"
                        data-parameters="getText('labelProductsManufacturer')"></s:textfield>
       </li>
       <li style="display: flex;margin-bottom: 2px;">
           <p><span><s:property value="getText('numbers')"/></span></p>
           <s:textfield id="txtSyouhinCode"
                        name="txtSyouhinCode"
                        cssClass="notComma nonSpecialChar k-textbox txtboxPopup txtUrl"
                        maxlength="50"
                        data-parameters="getText('numbers')"></s:textfield>
       </li>
       <li style="display: flex;margin-bottom: 2px;">
           <p><span><s:property value="getText('labelManufacturerSupport')"/></span></p>
           <s:textfield id="txtSyouhinOp2"
                        name = "txtSyouhinOp2"
                        cssClass="notComma nonSpecialChar k-textbox txtboxPopup txtUrl"
                        data-parameters="getText('labelManufacturerSupport')"></s:textfield>
       </li>
       <li style="display: flex;margin-bottom: 2px;">
           <p><span><s:property value="getText('supportedBike')"/></span></p>
           <s:textfield id="txtSyouhinOp3"
                        name = "txtSyouhinOp3"
                        cssClass="notComma nonSpecialChar k-textbox txtboxPopup txtUrl"
                        data-parameters="getText('supportedBike')"></s:textfield>
       </li>
       <li style="display: flex;margin-bottom: 2px;">
           <p><span><s:property value="getText('summary')"/></span></p>
           <s:textfield id="txtSyouhinOp1"
                        name = "txtSyouhinOp1"
                        cssClass="notComma nonSpecialChar k-textbox txtboxPopup txtUrl"
                        data-parameters="getText('summary')"></s:textfield>
       </li>
       <li style="display: flex;margin-bottom: 2px;">
           <p><span><s:property value="getText('btnClassificationCode')"/></span></p>
           <s:textarea id="txtSyouhinBunruiCode"
                       name = "txtSyouhinBunruiCode"
                       cols="25" rows="3"
                       cssStyle="width:258px; height:88px !important; margin-top: 1px !important;"
                       cssClass="notComma nonSpecialChar k-input">
           </s:textarea>
       </li>
    </ul>
        <div class="modelDetails">
            <div class="btnDialog btnDialogCenter">
                <input type="button" id="btnUpdateUrlProductLink" class="k-button btnCrud btnPopup align-center" value="<s:property value="getText('btnCreateLink')"/>"/>
                <input type="button" id="btnPreViewProductLink" class="k-button btnCrud btnPopup align-center" value="<s:property value="getText('preview')"/>"/>
                <div></div>
            </div>
        </div>
        <hr>
</div>
<!-- End Video Product Dialog -->
<!-- begin display Search Link dialog -->
<div id="linkSearchDialog" class="contentInner">
    <div id="listLinkImage" class="thumb_selector clearfix">
       <div id="imagelistViewLink"></div>
       <!-- BOE Fix ProductEdit bug #16 Correct displaying error message @rcv!nguyen.hieu 2014/03/13. -->
       <div id="emptyVideoListPanel" class="tableListVideo_empty"><s:property value="getText('text.error.noData')"/></div>
       <!-- EOE Fix ProductEdit bug #16 Correct displaying error message. -->
    </div>
</div>
<!-- End Video Product Dialog -->
<!-- begin template Search Link dialog -->
<script type="text/x-kendo-template" id="templateImageBlock5">
<div class="imagePopupBlock5">
    <div class="titlePopup">
        <h3 class="row_table">#:syouhinMaker#</h3>
        <p class="row_table">#:syouhinName#</p>
        <p class="row_table"><s:property value="getText('itemNumbers')"/>: #:syouhinCode#</p>
    </div>
    <img id="imageThumb" src="http://www.webike.net/catalogue/#:syouhinThumbnailUrl#">
    <div class="money"><s:property value="getText('listPrice')"/>: ￥#:syouhinTeika#</div>
    <!-- BOE Fix ProductEdit bug 23 - Show HTML literal format @rcv!nguyen.hieu 2014/03/13. -->
    <!-- <div class="htmlViewPopup">#:syouhinOp1#</div> -->
    <div class="htmlViewPopup">#= syouhinOp1 #</div>
    <!-- EOE Fix ProductEdit bug 23 - Show HTML literal format. -->
</div>
</script>
<!-- End template Search Link dialog -->
<!-- End Dialog Block 5 -->
<!-- Start Dialog Block 6 -->
<!-- begin display Video Product dialog -->
<div id="videoProductDialog" class="contentInner">
    <ul>
       <li style="display: flex;">
           <p><span><s:property value="getText('videoTitle')"/></span></p>
           <s:textfield id="txtProductVideoTitle" name="txtMakerDialog"
                        cssStyle="width:337px"
                        maxLength="%{@net.webike.RcProductFactory.util.Constant@PRODUCT_VIDEO_TITLE_WARNING_LENGTH}"
                        cssClass="notComma nonSpecialChar k-textbox k-required k-length[%{@net.webike.RcProductFactory.util.Constant@PRODUCT_VIDEO_TITLE_LENGTH}]"
                        data-parameters="getText('videoTitle')">
           </s:textfield>
       </li>
       <li style="display: flex;">
           <p><span><s:property value="getText('labelViedeoURL')"/></span></p>
           <s:textfield id="txtProductVideoUrl" name="txtProductVideoUrl"
                        cssStyle="width:335px;height:14px;"
                        maxLength="%{@net.webike.RcProductFactory.util.Constant@PRODUCT_VIDEO_URL_WARNING_LENGTH}"
                        cssClass="notComma k-input k-url k-required k-length[%{@net.webike.RcProductFactory.util.Constant@PRODUCT_VIDEO_URL_LENGTH}]"
                        data-parameters="getText('labelViedeoURL')">
           </s:textfield>
       </li>
       <li style="display: flex;">
           <p style="width: 442px; ; text-align: left;">
               <label><s:property value="getText('labelVideoDescription')"/></label>
           </p>
       </li>
       <li style="display: flex;">
            <s:textarea id="txtProductVideoDescription"
                        name="txtProductVideoDescription" cols="3" rows="2"
                        maxLength="%{@net.webike.RcProductFactory.util.Constant@PRODUCT_VIDEO_DESCRIPTION_WARNING_LENGTH}"
                        cssClass="notComma nonSpecialChar k-input k-required k-length[%{@net.webike.RcProductFactory.util.Constant@PRODUCT_VIDEO_DESCRIPTION_LENGTH}]"
                        data-parameters="getText('labelVideoDescription')"/>
       </li>
    </ul>
        <div class="modelDetails">
            <div class="btnDialog btnDialogCenter">
                <input type="button" id="btnInsertProductVideo" class="k-button btnCrud btnPopup align-center" value="<s:property value="getText('decide')"/>"/>
                <input type="hidden" id="hCreate" value="<s:property value="getText('btnCreating')"/>"/>
                <input type="button" id="btnPreViewProductVideo" class="k-button btnCrud btnPopup align-center" value="<s:property value="getText('preview')"/>"/>
        </div>
    </div>
    <hr>
</div>
<!-- End Video Product Dialog -->
<!-- begin display Video Player dialog -->
<div id="videoPlayerDialog" class="contentInner">
    <ul style="padding-left:4px;">
       <li style="display: flex;margin-bottom: 2px;">
           <span style="height: auto; width: 100%; text-align: left; margin-right:0px;">
               <span id="productVideoTitle"><s:property value="getText('titlePlayVideoPopup')"/></span>
           </span>
       </li>
       <li style="display: flex;margin-bottom: 10px;">
       </li>
       <li style="display: flex;margin-bottom: 2px;">
            <iframe id="popupVideo" width="640" height="390"
                src="">
            </iframe>
       </li>
       <li style="display: flex;margin-bottom: 10px;">
       </li>
       <li style="display: flex;margin-bottom: 2px;">
           <span style="height: auto; width: 100%; text-align: left;margin-right:0px;">
              <span id="productVideoDescriptionDialog"><s:property value="getText('descriptionPlayVideoPopup')"/></span>
           </span>
       </li>
    </ul>
</div>
<!-- End Video Product Dialog -->
<!-- End Dialog Block 6 -->

<!-- begin display Model Product dialog -->
<div id="attProductDialog" class="contentInner">
    <div class="modelDetails">
        <ul>
            <li>
                <p style="height:20px;"><span><s:property value="getText('attribute')"/>(*)</span></p>
                <div id="attributeNameList">
	                <s:select id="cbxAttributeName" list="form.listAllAttribute"
	                          listKey="attributeJSon"
	                          listValue="attributeName"
	                          cssClass="k-required"/>
                </div>
                <input type="button" id="btnUpdateAttrProductDialog" class="k-button btnCrud btnInsertDialog " value="<s:property value="getText('decide')"/>"/>
            </li>
            <li>
                <p><span><s:property value="getText('displayName')"/>(*)</span></p>
                <input type="text" id="txtPopupAttributeDisplayValue"
                    name="supplierSyouhinSiireOrder"
                    class="notComma k-textbox k-required nonSpecialChar"
                    data-parameters='<s:property value="getText('displayName')"/>'>
            </li>
            <li>
                <p><span><s:property value="getText('administrativeName')"/></span></p>
                <input type="text" id="txtPopupAttributeFlagValue"
                            class="notComma k-textbox k-maxlength-regex k-required nonSpecialChar"
                            name="txtPopupAttributeFlagValue"
                            data-parameters='<s:property value="getText('administrativeName')"/>'>
                <input type="checkbox" id="cbPopupAttributeFlagValue" class="">
                <div id="attributeGroupNameArea">
                    <input id="cbxPopupAttributeGroupName">
                </div>
            </li>
            <li>
                <p><span><s:property value="getText('sortOrdering')"/></span></p>
                <input type="text" id="txtSortPopupAttribute"
                       name="txtSortPopupAttribute"
                       class="k-textbox notComma">
            </li>
            <li>
                    <p><span><s:property value="getText('classification')"/></span></p>
                    <s:label id="txtPopupAttributeType"/>
            </li>
            <li>
                    <p><span><s:property value="getText('numberOfCharacters')"/></span></p>
                    <s:label id="txtPopupAttributeLengthLimit"/><s:property value="getText('upToCharacter')"/>
            </li>
            <li>
                <p><span><s:property value="getText('limit')"/></span></p>
                <s:label id="txtPopupAttributeRegexp"/>
            </li>
            </ul>
        <!-- Mode = 0: Insert; Mode = 1: Edit -->
        <input type="hidden" id="popupAttributeMode" value="0"/>
        <input type="hidden" id="popupAttributeType" value="0"/>
        
        <hr>
    </div>
</div>
<!-- begin display addFactorySupplier dialog -->
<div id="addFactorySupplierDialog" class="contentInner">
    <div class="modelDetails">
     <ul style="padding-left:4px;">
        <li style="display: flex;margin-bottom: 2px;">
            <p><span><s:property value="getText('supplier')"/>*</span></p>
            <div id="siireNameList">
	             <s:select id="cbxSiireName"
	                       list="form.listAllEntMstSiire"
	                       listKey="siireCode" listValue="siireName"
	                       value="%{form.listAllEntMstSiire}"
	                       headerKey="0"
	                       headerValue=" "
	                       cssClass="k-required"
	             />
	         </div>
             <input type="button" id="btnEditDialogSupplier" class="k-button btnCrud btnInsertDialog " value="<s:property value="getText('decide')"/>"/>
        </li>
        <li style="display: flex;margin-bottom: 2px;">
           <p><span><s:property value="getText('deliveryTime')"/> *</span></p>
           <div id="noukiNameList">
	           <s:select id="cbxNoukiName1"
	                     list="form.listAllEntMstNouki"
	                     listKey="noukiCode" listValue="noukiName1"
	           />
	       </div>
         </li>
         <li style="display: flex;margin-bottom: 2px;">
            <p><span><s:property value="getText('priority')"/> *</span></p>
            <s:textfield id="txtSupplierSyouhinSiireOrder" name="txtSupplierSyouhinSiireOrder"
                         maxLength="1"
                         cssClass="k-textbox k-required k-length[%{@net.webike.RcProductFactory.util.Constant@SUPPLIER_SYOUHIN_SIIRE_ORDER_LENGTH}]
                                nonSpecialChar is2byteCharacter"
                         data-parameters="getText('priority')">
            </s:textfield>
            </li>
       </ul>
       <hr>
    </div>
</div>
<!-- end display image Product dialog -->

<!-- begin display addFitModelDialog dialog -->
<div id="addFitModelDialog" class="contentInner" style="display:none">
    <div class="modelDetails">
     <ul style="padding-left:4px;">
        <li style="float:right;">
            <span><input type="button" id="btnEditDialogFitModel" class="k-button btnCrud btnInsertDialogMaker " value="<s:property value="getText('decide')"/>"></span>
        </li>
        <li style="margin-bottom: 2px;">
            <span id="summaryFitModelMsg" class="k-widget k-tooltip k-tooltip-validation k-invalid-msg" role="alert"></span>
        </li>
        <!-- Maker Name -->
        <li style="display: flex;margin-bottom: 2px;">
            <div style="display: flex;">
            <input type="hidden" id="txtFitModelSort">
            <p><span><s:property value="getText('maker')"/></span></p>
            <s:textfield id="txtMakerName" name="addFitModelDialog.txtMakerName" 
                 maxLength="%{@net.webike.RcProductFactory.util.Constant@FIT_MODEL_MAKER_WARNING_LENGTH}"
                 cssClass="notComma k-textbox k-required k-length[%{@net.webike.RcProductFactory.util.Constant@FIT_MODEL_MAKER_LENGTH}]
                            nonSpecialChar" 
                 data-parameters="getText('text.grid.maker')"></s:textfield>
            </div>
        </li>
        <!-- Fit Model -->
        <li style="display: flex;margin-bottom: 2px;">
            <p><span><s:property value="getText('bikeModelName')"/></span></p>
            <s:textfield id="txtModelName" name="addFitModelDialog.txtModelName"
                 maxLength="%{@net.webike.RcProductFactory.util.Constant@FIT_MODEL_MODEL_WARNING_LENGTH}"
                 cssClass="notComma k-textbox k-length[%{@net.webike.RcProductFactory.util.Constant@FIT_MODEL_MODEL_LENGTH}]
                            nonSpecialChar" 
                 data-parameters="getText('text.grid.model')"></s:textfield>
        </li>
        <!-- Fit Model Style -->
        <li style="display: flex;margin-bottom: 2px;">
            <p><span><s:property value="getText('bikeModel')"/></span></p>
            <s:textfield id="txtModelStyle"
             maxLength="%{@net.webike.RcProductFactory.util.Constant@FIT_MODEL_STYLE_LENGTH}"
             cssClass="notComma k-textbox k-length[%{@net.webike.RcProductFactory.util.Constant@FIT_MODEL_STYLE_WARNING_LENGTH}]
                        nonSpecialChar"
             data-parameters="getText('text.grid.style')"></s:textfield>
        </li>
       </ul>
    </div>
    <div class="title" style="margin-top: 10px;height: 18px;"><s:property value="getText('modelSearch')"/></div>
    <div class="kendoWrapDiv">
      <div id="gridAllModel"></div>
    </div>
</div>
<!-- end display image Product dialog -->

<!-- begin Matter Product dialog -->
<div id="matterProductDialog" class="contentInner">
    <div class="headerContent" style="height: 20px;width: 98%;margin-left: 4px;">
        <div class="headerBlockButton">
            <input type="button" id="btnCancelBlockProductRun" class="k-button btnCrud btnPopup align-center" value="<s:property value="getText('cancel')"/>"/>
            <input type="button" id="btnBlockProductRun" class="k-button btnCrud btnPopup align-center" value="<s:property value="getText('execute')"/>"/>
        </div>
    </div>
    <div style="margin-top: 3px;margin-bottom: 5px;"></div>
    <div style="background-color:#EEEEEE; overflow-y: scroll;height: 592px;">
    <div class="modelDetails" style="display: flex;">
        <input type="hidden" id="titlePopupBlock15" value='<s:property value="getText('batchEdit')"/>'>
        <!-- Start Column Left -->
        <div class="popup15ColumnLeft">
        <ul style="padding-left:4px;">
            <!-- Start Image1 -->
            <li style="display: flex;margin-bottom: 2px;">
                <div style="width : 112px">
                    <div style="text-align: initial; height: 26px;float:left;margin-left: 4px;">
                        <input type="checkbox" class="replace_checkbox" value="image1" id=""/>&nbsp;
                            <div class="checkColumn1 inputMatterProduct"><s:property value="getText('image1')"/></div>
                    </div>
                    <div style="height: 26px;float:left;margin-left: 20px;">
                        <input type="radio" class="edit replace_all" name="image1" value="0">&nbsp;
                           <div class="inputMatterProduct image1" style="text-align:left;"><s:property value="getText('replacement')"/></div>
                    </div>
                    <div style="height: 26px;float:left;margin-left: 20px;">
                        <input type="radio" class="edit" name="image1" value="1">&nbsp;
                           <div class="inputMatterProduct image1" style="text-align:left;"><s:property value="getText('frontMatter')"/></div>
                    </div>
                    <div style="height: 26px;float:left;margin-left: 20px;">
                        <input type="radio" class="edit" name="image1" value="2">&nbsp;
                           <div class="inputMatterProduct image1" style="text-align:left;"><s:property value="getText('retrofit')"/></div>
                    </div>
                    <div style="height: 26px;float:left;margin-left: 20px;">
                        <input type="radio" class="replace_image" name="image1" value="3">&nbsp;
                           <div class="inputMatterProduct image1" style="text-align:left;"><s:property value="getText('partialReplacement')"/></div>
                    </div>
                </div>
                <div style="width : 355px;">
                    <div style="height: 26px;text-align: right;margin-right: 100px;">
                        <input type="radio" class="" name="image1Mode" value="temporary">&nbsp;
                           <div class="inputMatterProduct" style="text-align:left;"><s:property value="getText('aNew')"/></div>
                        <input type="radio" class="real_mode" name="image1Mode" checked="checked" value="real">&nbsp;
                           <div class="inputMatterProduct" style="text-align:left;"><s:property value="getText('production')"/></div>
                    </div>
                    <div class="textColumn imageFindThumb">
                        <span><s:property value="getText('findThumb')"/></span>&nbsp;
                        <input type="text" class="image1FindThumb k-textbox nonSpecialChar find_text" maxlength="510" name="image1FindThumb" id="" 
                        		value="" style="width:205px;float:right;"
                        		data-parameters='<s:property value="getText('findThumb')"/>'/>
                    </div>
                    <div class="textColumn imageReplaceThumb">
                        <span><s:property value="getText('replaceThumb')"/></span>&nbsp;
                        <input type="text" class="image1ReplaceThumb k-textbox nonSpecialChar" maxlength="510" name="image1ReplaceThumb" id=""
                        		value="" style="width:205px;float:right;"
                        		data-parameters='<s:property value="getText('replaceThumb')"/>'/>
                    </div>
                    <div class="textColumn imageFindDetail">
                        <span><s:property value="getText('findImage')"/></span>&nbsp;
                        <input type="text" class="image1FindImage k-textbox nonSpecialChar find_text" maxlength="510" name="image1FindImage" id="" value="" 
                        		style="width:205px;float:right;"
                        		data-parameters='<s:property value="getText('findImage')"/>'/>
                    </div>
                    <div class="textColumn imageReplaceDetail">
                        <span><s:property value="getText('replaceImage')"/></span>&nbsp;
                        <input type="text" class="image1ReplaceImage k-textbox nonSpecialChar" maxlength="510" name="image1ReplaceImage" id="" 
                        		value="" style="width:205px;float:right;"
                        		data-parameters='<s:property value="getText('replaceImage')"/>'/>
                    </div>
                </div>
            </li>
            <!-- End Image1 -->
            <!-- Start image2 -->
            <li style="display: flex;margin-bottom: 2px;">
                <div style="width : 112px">
                    <div style="text-align: initial; height: 26px;float:left;margin-left: 4px;">
                        <input type="checkbox" class="replace_checkbox" value="image2" id=""/>&nbsp;
                            <div class="checkColumn1 inputMatterProduct"><s:property value="getText('image2')"/></div>
                    </div>
                    <div style="height: 26px;float:left;margin-left: 20px;">
                        <input type="radio" class="edit replace_all" name="image2" value="0">&nbsp;
                           <div class="inputMatterProduct image2" style="text-align:left;"><s:property value="getText('replacement')"/></div>
                    </div>
                    <div style="height: 26px;float:left;margin-left: 20px;">
                        <input type="radio" class="edit" name="image2" value="1">&nbsp;
                           <div class="inputMatterProduct image2" style="text-align:left;"><s:property value="getText('frontMatter')"/></div>
                    </div>
                    <div style="height: 26px;float:left;margin-left: 20px;">
                        <input type="radio" class="edit" name="image2" value="2">&nbsp;
                           <div class="inputMatterProduct image2" style="text-align:left;"><s:property value="getText('retrofit')"/></div>
                    </div>
                    <div style="height: 26px;float:left;margin-left: 20px;">
                        <input type="radio" class="replace_image" name="image2" value="3">&nbsp;
                           <div class="inputMatterProduct image2" style="text-align:left;"><s:property value="getText('partialReplacement')"/></div>
                    </div>
                </div>
                <div style="width : 355px;">
                    <div style="height: 26px;text-align: right;margin-right: 100px;">
                        <input type="radio" class="" name="image2Mode" value="temporary">&nbsp;
                           <div class="inputMatterProduct" style="text-align:left;"><s:property value="getText('aNew')"/></div>
                        <input type="radio" class="real_mode" name="image2Mode" checked="checked" value="real">&nbsp;
                           <div class="inputMatterProduct" style="text-align:left;"><s:property value="getText('production')"/></div>
                    </div>
                    <div class="textColumn imageFindThumb">
                        <span><s:property value="getText('findThumb')"/></span>&nbsp;
                        <input type="text" class="image2FindThumb nonSpecialChar k-textbox find_text" maxlength="510" name="image2FindThumb" id="" 
                        		value="" style="width:205px;float:right;"
                        		data-parameters='<s:property value="getText('findThumb')"/>'/>
                    </div>
                    <div class="textColumn imageReplaceThumb">
                        <span><s:property value="getText('replaceThumb')"/></span>&nbsp;
                        <input type="text" class="image2ReplaceThumb nonSpecialChar k-textbox" maxlength="510" name="image2ReplaceThumb" id="" 
                        		value="" style="width:205px;float:right;"
                        		data-parameters='<s:property value="getText('replaceThumb')"/>'/>
                    </div>
                    <div class="textColumn imageFindDetail">
                        <span><s:property value="getText('findImage')"/></span>&nbsp;
                        <input type="text" class="find_text image2FindImage nonSpecialChar k-textbox find_text" maxlength="510" name="image2FindImage" id="" 
                        		value="" style="width:205px;float:right;"
                        		data-parameters='<s:property value="getText('findImage')"/>'/>
                    </div>
                    <div class="textColumn imageReplaceDetail">
                        <span><s:property value="getText('replaceImage')"/></span>&nbsp;
                        <input type="text" class="image2ReplaceImage k-textbox nonSpecialChar" maxlength="510" name="image2ReplaceImage" id="" 
                        		value="" style="width:205px;float:right;"
                        		data-parameters='<s:property value="getText('replaceImage')"/>'/>
                    </div>
                </div>
            </li>
            <!-- End image2 -->
            <!-- Start image3 -->
            <li style="display: flex;margin-bottom: 2px;">
                <div style="width : 112px">
                    <div style="text-align: initial; height: 26px;float:left;margin-left: 4px;">
                        <input type="checkbox" class="replace_checkbox" value="image3" id=""/>&nbsp;
                            <div class="checkColumn1 inputMatterProduct"><s:property value="getText('image3')"/></div>
                    </div>
                    <div style="height: 26px;float:left;margin-left: 20px;">
                        <input type="radio" class="edit replace_all" name="image3" value="0">&nbsp;
                           <div class="inputMatterProduct image3" style="text-align:left;"><s:property value="getText('replacement')"/></div>
                    </div>
                    <div style="height: 26px;float:left;margin-left: 20px;">
                        <input type="radio" class="edit" name="image3" value="1">&nbsp;
                           <div class="inputMatterProduct image3" style="text-align:left;"><s:property value="getText('frontMatter')"/></div>
                    </div>
                    <div style="height: 26px;float:left;margin-left: 20px;">
                        <input type="radio" class="edit" name="image3" value="2">&nbsp;
                           <div class="inputMatterProduct image3" style="text-align:left;"><s:property value="getText('retrofit')"/></div>
                    </div>
                    <div style="height: 26px;float:left;margin-left: 20px;">
                        <input type="radio" class="replace_image" name="image3" value="3">&nbsp;
                           <div class="inputMatterProduct image3" style="text-align:left;"><s:property value="getText('partialReplacement')"/></div>
                    </div>
                </div>
                <div style="width : 355px;">
                    <div style="height: 26px;text-align: right;margin-right: 100px;">
                        <input type="radio" class="" name="image3Mode" value="temporary">&nbsp;
                           <div class="inputMatterProduct" style="text-align:left;"><s:property value="getText('aNew')"/></div>
                        <input type="radio" class="real_mode" name="image3Mode" checked="checked" value="real">&nbsp;
                           <div class="inputMatterProduct" style="text-align:left;"><s:property value="getText('production')"/></div>
                    </div>
                    <div class="textColumn imageFindThumb">
                        <span><s:property value="getText('findThumb')"/></span>&nbsp;
                        <input type="text" class="image3FindThumb nonSpecialChar k-textbox find_text" maxlength="510" name="image3FindThumb" id="" 
                        		value="" style="width:205px;float:right;"
                        		data-parameters='<s:property value="getText('findThumb')"/>'/>
                    </div>
                    <div class="textColumn imageReplaceThumb">
                        <span><s:property value="getText('replaceThumb')"/></span>&nbsp;
                        <input type="text" class="image3ReplaceThumb nonSpecialChar k-textbox" maxlength="510" name="image3ReplaceThumb" id="" 
		                        value="" style="width:205px;float:right;"
		                        data-parameters='<s:property value="getText('replaceThumb')"/>'/>
                    </div>
                    <div class="textColumn imageFindDetail">
                        <span><s:property value="getText('findImage')"/></span>&nbsp;
                        <input type="text" class="image3FindImage nonSpecialChar k-textbox find_text" maxlength="510" name="image3FindImage" id="" 
		                        value="" style="width:205px;float:right;"
		                        data-parameters='<s:property value="getText('findImage')"/>'/>
                    </div>
                    <div class="textColumn imageReplaceDetail">
                        <span><s:property value="getText('replaceImage')"/></span>&nbsp;
                        <input type="text" class="image3ReplaceImage nonSpecialChar k-textbox" maxlength="510" name="image3ReplaceImage" id="" 
		                        value="" style="width:205px;float:right;"
		                        data-parameters='<s:property value="getText('replaceImage')"/>'/>
                    </div>
                </div>
            </li>
            <!-- End image3 -->
            <!-- Start image4 -->
            <li style="display: flex;margin-bottom: 2px;">
                <div style="width : 112px">
                    <div style="text-align: initial; height: 26px;float:left;margin-left: 4px;">
                        <input type="checkbox" class="replace_checkbox" value="image4" id=""/>&nbsp;
                            <div class="checkColumn1 inputMatterProduct"><s:property value="getText('image4')"/></div>
                    </div>
                    <div style="height: 26px;float:left;margin-left: 20px;">
                        <input type="radio" class="edit replace_all" name="image4" value="0">&nbsp;
                           <div class="inputMatterProduct image4" style="text-align:left;"><s:property value="getText('replacement')"/></div>
                    </div>
                    <div style="height: 26px;float:left;margin-left: 20px;">
                        <input type="radio" class="edit" name="image4" value="1">&nbsp;
                           <div class="inputMatterProduct image4" style="text-align:left;"><s:property value="getText('frontMatter')"/></div>
                    </div>
                    <div style="height: 26px;float:left;margin-left: 20px;">
                        <input type="radio" class="edit" name="image4" value="2">&nbsp;
                           <div class="inputMatterProduct image4" style="text-align:left;"><s:property value="getText('retrofit')"/></div>
                    </div>
                    <div style="height: 26px;float:left;margin-left: 20px;">
                        <input type="radio" class="replace_image" name="image4" value="3">&nbsp;
                           <div class="inputMatterProduct image4" style="text-align:left;"><s:property value="getText('partialReplacement')"/></div>
                    </div>
                </div>
                <div style="width : 355px;">
                    <div style="height: 26px;text-align: right;margin-right: 100px;">
                        <input type="radio" class="" name="image4Mode" value="temporary">&nbsp;
                           <div class="inputMatterProduct" style="text-align:left;"><s:property value="getText('aNew')"/></div>
                        <input type="radio" class="real_mode" name="image4Mode" checked="checked" value="real">&nbsp;
                           <div class="inputMatterProduct" style="text-align:left;"><s:property value="getText('production')"/></div>
                    </div>
                    <div class="textColumn imageFindThumb">
                        <span><s:property value="getText('findThumb')"/></span>&nbsp;
                        <input type="text" class="image4FindThumb nonSpecialChar k-textbox find_text" maxlength="510" name="image4FindThumb" id="" 
		                        value="" style="width:205px;float:right;"
		                        data-parameters='<s:property value="getText('findThumb')"/>'/>
                    </div>
                    <div class="textColumn imageReplaceThumb">
                        <span><s:property value="getText('replaceThumb')"/></span>&nbsp;
                        <input type="text" class="image4ReplaceThumb nonSpecialChar k-textbox" maxlength="510" name="image4ReplaceThumb" id="" 
		                        value="" style="width:205px;float:right;"
		                        data-parameters='<s:property value="getText('replaceThumb')"/>'/>
                    </div>
                    <div class="textColumn imageFindDetail">
                        <span><s:property value="getText('findImage')"/></span>&nbsp;
                        <input type="text" class="image4FindImage nonSpecialChar k-textbox find_text" maxlength="510" name="image4FindImage" id="" 
		                        value="" style="width:205px;float:right;"
		                        data-parameters='<s:property value="getText('findImage')"/>'/>
                    </div>
                    <div class="textColumn imageReplaceDetail">
                        <span><s:property value="getText('replaceImage')"/></span>&nbsp;
                        <input type="text" class="image4ReplaceImage nonSpecialChar k-textbox" maxlength="510" name="image4ReplaceImage" id="" 
		                        value="" style="width:205px;float:right;"
		                        data-parameters='<s:property value="getText('replaceImage')"/>'/>
                    </div>
                </div>
            </li>
            <!-- End image4 -->
            <!-- Start image5 -->
            <li style="display: flex;margin-bottom: 2px;">
                <div style="width : 112px">
                    <div style="text-align: initial; height: 26px;float:left;margin-left: 4px;">
                        <input type="checkbox" class="replace_checkbox" value="image5" id=""/>&nbsp;
                            <div class="checkColumn1 inputMatterProduct"><s:property value="getText('image5')"/></div>
                    </div>
                    <div style="height: 26px;float:left;margin-left: 20px;">
                        <input type="radio" class="edit replace_all" name="image5" value="0">&nbsp;
                           <div class="inputMatterProduct image5" style="text-align:left;"><s:property value="getText('replacement')"/></div>
                    </div>
                    <div style="height: 26px;float:left;margin-left: 20px;">
                        <input type="radio" class="edit" name="image5" value="1">&nbsp;
                           <div class="inputMatterProduct image5" style="text-align:left;"><s:property value="getText('frontMatter')"/></div>
                    </div>
                    <div style="height: 26px;float:left;margin-left: 20px;">
                        <input type="radio" class="edit" name="image5" value="2">&nbsp;
                           <div class="inputMatterProduct image5" style="text-align:left;"><s:property value="getText('retrofit')"/></div>
                    </div>
                    <div style="height: 26px;float:left;margin-left: 20px;">
                        <input type="radio" class="replace_image" name="image5" value="3">&nbsp;
                           <div class="inputMatterProduct image5" style="text-align:left;"><s:property value="getText('partialReplacement')"/></div>
                    </div>
                </div>
                <div style="width : 355px;">
                    <div style="height: 26px;text-align: right;margin-right: 100px;">
                        <input type="radio" class="" name="image5Mode" value="temporary">&nbsp;
                           <div class="inputMatterProduct" style="text-align:left;"><s:property value="getText('aNew')"/></div>
                        <input type="radio" class="real_mode" name="image5Mode" checked="checked" value="real">&nbsp;
                           <div class="inputMatterProduct" style="text-align:left;"><s:property value="getText('production')"/></div>
                    </div>
                    <div class="textColumn imageFindThumb">
                        <span><s:property value="getText('findThumb')"/></span>&nbsp;
                        <input type="text" class="image5FindThumb nonSpecialChar k-textbox find_text" maxlength="510" name="image5FindThumb" id="" 
		                        value="" style="width:205px;float:right;"
		                        data-parameters='<s:property value="getText('findThumb')"/>'/>
                    </div>
                    <div class="textColumn imageReplaceThumb">
                        <span><s:property value="getText('replaceThumb')"/></span>&nbsp;
                        <input type="text" class="image5ReplaceThumb nonSpecialChar k-textbox" maxlength="510" name="image5ReplaceThumb" id="" 
		                        value="" style="width:205px;float:right;"
		                        data-parameters='<s:property value="getText('replaceThumb')"/>'/>
                    </div>
                    <div class="textColumn imageFindDetail">
                        <span><s:property value="getText('findImage')"/></span>&nbsp;
                        <input type="text" class="image5FindImage nonSpecialChar k-textbox find_text" maxlength="510" name="image5FindImage" id="" 
		                        value="" style="width:205px;float:right;"
		                        data-parameters='<s:property value="getText('findImage')"/>'/>
                    </div>
                    <div class="textColumn imageReplaceDetail">
                        <span><s:property value="getText('replaceImage')"/></span>&nbsp;
                        <input type="text" class="image5ReplaceImage nonSpecialChar k-textbox" maxlength="510" name="image5ReplaceImage" id="" 
		                        value="" style="width:205px;float:right;"
		                        data-parameters='<s:property value="getText('replaceImage')"/>'/>
                    </div>
                </div>
            </li>
            <!-- End image5 -->
            <!-- Start combobox Category -->
            <li style="display: flex;margin-bottom: 2px;">
                <div style="width : 272px">
                    <div style="text-align: initial;">
                        <input type="checkbox" class="replace_checkbox" value="category" id="cbxListCategory" style="margin-left: 4px;"/>&nbsp;
                            <div class="checkColumn1 inputMatterProduct"><s:property value="getText('category')"/></div>
                    </div>
                </div>
                <s:select id="cbbCategoryList"
                    list="form.listCategory"
                    listKey="categoryCode" 
                    listValue="categoryName"
                    cssClass="category k-required"
                    cssStyle="width:162px;"
                    data-parameters="getText('category')"/>
            </li>
            <!-- End combobox Category -->
            <!--  Start product Name -->
            <li style="display: flex;margin-bottom: 5px;">
                <div style="width : 223px">
                    <div style="text-align: initial; height: 26px;float:right;">
                        <input type="checkbox" class="replace_checkbox" value="productName" id="cbPopupProductName"/>&nbsp;
                            <div class="checkColumn1 inputMatterProduct"><s:property value="getText('productName2')"/> </div> 
                        <input type="radio"    class="edit replace_all" name="productName" value="0">&nbsp;
                            <div class="inputMatterProduct"><s:property value="getText('replacement')"/></div>
                        <input type="radio"    class="edit" name="productName" value="1">&nbsp;
                            <div class="inputMatterProduct"><s:property value="getText('frontMatter')"/></div>
                    </div>
                    <div style="height: 26px;float:right;">
                        <input type="radio"    class="edit" name="productName" value="2">&nbsp;
                            <div class="inputMatterProduct" style="text-align:left;"><s:property value="getText('retrofit')"/></div>
                        <input type="radio" class="replace" name="productName" value="3">&nbsp;
                           <div class="inputMatterProduct"><s:property value="getText('partialReplacement')"/></div>
                    </div>
                </div>
                <div style="width : 287px;margin-left: 5px;">
                    <div style="height: 26px; text-align:right;display: flex;">
                        <span style="margin-right:4px;"><s:property value="getText('find')"/></span>&nbsp;
                        <input type="text"
                                class="productNameFind nonSpecialChar k-length[255] find_text k-textbox"
                                maxlength="512" 
                        		name="findProductName" id=""
                        		value="" style="width:162px;float:right;"
                        		data-parameters='<s:property value="getText('find')"/>'/>
                    </div>
                    <div style="height: 26px;text-align:right;display: flex;">
                        <span style="margin-right:4px;"><s:property value="getText('replace')"/></span>&nbsp;
                        <input type="text"
                                class="productName nonSpecialChar k-required k-length[255] k-textbox"
                                maxlength="512" 
                        		name="tbproductName" id="tbPopupProductName" value=""
                        		style="width:162px;float:right;" 
                        		data-parameters='<s:property value="getText('replace')"/>'/>
                    </div>
                </div>
            </li>
            <!-- End product Name -->
            <!-- Start JAN -->
            <li style="display: flex;margin-bottom: 2px;">
                <div style="width : 223px">
                    <div style="text-align: initial; height: 26px;float:right;">
                        <input type="checkbox" class="TestTemp replace_checkbox" value="janPopupBlock15" id=""/>&nbsp;
                           <div class="checkColumn1 inputMatterProduct"><s:property value="getText('JAN')"/></div>
                        <input type="radio" class="edit replace_all" name="janPopupBlock15" value="0">&nbsp;
                           <div class="inputMatterProduct"><s:property value="getText('replacement')"/></div>
                        <input type="radio" class="edit" name="janPopupBlock15" value="1">&nbsp;
                           <div class="inputMatterProduct"><s:property value="getText('frontMatter')"/></div>
                    </div>
                    <div style="height: 26px;float:right;">
                        <input type="radio" class="edit" name="janPopupBlock15" value="2">&nbsp;
                           <div class="inputMatterProduct" style="text-align:left;"><s:property value="getText('retrofit')"/></div>
                        <input type="radio" class="replace" name="janPopupBlock15" value="3">&nbsp;
                           <div class="inputMatterProduct"><s:property value="getText('partialReplacement')"/></div>
                    </div>
                </div>
                <div style="width : 287px;margin-left: 5px;">
                    <div style="height: 26px;text-align:right;display: flex;">
                        <span style="margin-right:4px;"><s:property value="getText('find')"/></span>&nbsp;
                        <input type="text" class="janPopupBlock15Find nonSpecialChar k-length[20] find_text k-textbox" maxlength="40" 
                        		name="findJanCode" id="" value=""
                        		style="width:162px;"
                        		data-parameters='<s:property value="getText('find')"/>'/>
                    </div>
                    <div style="height: 26px;text-align:right;display: flex;">
                        <span style="margin-right:4px;"><s:property value="getText('replace')"/></span>&nbsp;
                        <input type="text" class="janPopupBlock15 nonSpecialChar k-required k-length[20] k-textbox" maxlength="40" 
                        		name="replaceJanCode" id="" value=""
                        		style="width:162px;"
                        		data-parameters='<s:property value="getText('replace')"/>'/>
                    </div>
                </div>
            </li>
            <!-- End JAN -->
            <!-- Start productCode -->
            <li style="display: flex;margin-bottom: 2px;height:80px;">
                <div style="margin-left: 4px; width : 223px">
                    <div style="text-align: initial; height: 26px;">
                        <input type="checkbox" class="replace_checkbox" value="productCodeNoFlag" id="cbxProductCode"/>&nbsp;
                           <div class="checkColumn1 inputMatterProduct" style="width: 77px;"><s:property value="getText('productCodeNoFlag')"/></div>
                    </div>
                    <div style="height: 26px;float:left;margin-left: 71px;">
                        <input type="radio" class="edit replace_all" name="productCodeNoFlag" value="0">&nbsp;
                           <div class="inputMatterProduct" style="text-align: left;"><s:property value="getText('replacement')"/></div>
                        <input type="radio" class="edit" name="productCodeNoFlag" value="1">&nbsp;
                           <div class="inputMatterProduct" style="text-align: left;"><s:property value="getText('frontMatter')"/></div>
                    </div>
                    <div style="height: 26px;float:left;margin-left: 71px;">
                        <input type="radio" class="edit" name="productCodeNoFlag" value="2">&nbsp;
                           <div class="inputMatterProduct" style="text-align:left;"><s:property value="getText('retrofit')"/></div>
                        <input type="radio" class="replace" name="productCodeNoFlag" value="3">&nbsp;
                           <div class="inputMatterProduct" style="text-align: left;"><s:property value="getText('partialReplacement')"/></div>
                    </div>
                </div>
                <div style="width : 287px;margin-left: 5px;">
                    <div style="text-align: initial; height: 26px;">
                        <input type="checkbox" class="productCodeNoFlag" value="checkboxProductCodeNoFlag" id="checkboxProductCodeNoFlag"
                               style="margin-left: 52px;"/>&nbsp;
                            <span><s:property value="getText('checked')"/></span>&nbsp;
                    </div>
                    <div style="height: 26px;text-align:right; display: flex;">
                        <span style="margin-right:4px;"><s:property value="getText('find')"/></span>&nbsp;
                        <input type="text"
                                class="productCodeNoFlagFind nonSpecialChar k-length[50] find_text k-textbox"
                                maxlength="100" 
                        		name="findProductCode" id="productCodeNoFlagFind" value=""
                        		style="width:162px;"
                        		data-parameters='<s:property value="getText('find')"/>'/>
                    </div>
                    <div style="height: 26px;text-align:right; display: flex;">
                        <span style="margin-right:4px;"><s:property value="getText('replace')"/></span>&nbsp;
                        <input type="text" 
                                class="productCodeNoFlag nonSpecialChar k-required k-length[50] k-textbox"
                                maxlength="100" 
                        		name="replaceProductCode" id="productCodeNoFlag" value=""
                        		style="width:162px;"
                        		data-parameters='<s:property value="getText('replace')"/>'/>
                    </div>
                </div>
            </li>
            <!-- End productCode -->
        </ul>
        </div>
        <!-- End Column Left -->
        <!-- Start Column Right -->
        <div class="popup15ColumnRight">
        <ul>
            <!-- Start image6 -->
            <li style="display: flex;margin-bottom: 2px;">
                <div style="width : 112px">
                    <div style="text-align: initial; height: 26px;float:left;margin-left: 4px;">
                        <input type="checkbox" class="replace_checkbox" value="image6" id=""/>&nbsp;
                            <div class="checkColumn1 inputMatterProduct"><s:property value="getText('image6')"/></div>
                    </div>
                    <div style="height: 26px;float:left;margin-left: 20px;">
                        <input type="radio" class="edit replace_all" name="image6" value="0">&nbsp;
                           <div class="inputMatterProduct image6" style="text-align:left;"><s:property value="getText('replacement')"/></div>
                    </div>
                    <div style="height: 26px;float:left;margin-left: 20px;">
                        <input type="radio" class="edit" name="image6" value="1">&nbsp;
                           <div class="inputMatterProduct image6" style="text-align:left;"><s:property value="getText('frontMatter')"/></div>
                    </div>
                    <div style="height: 26px;float:left;margin-left: 20px;">
                        <input type="radio" class="edit" name="image6" value="2">&nbsp;
                           <div class="inputMatterProduct image6" style="text-align:left;"><s:property value="getText('retrofit')"/></div>
                    </div>
                    <div style="height: 26px;float:left;margin-left: 20px;">
                        <input type="radio" class="replace_image" name="image6" value="3">&nbsp;
                           <div class="inputMatterProduct image6" style="text-align:left;"><s:property value="getText('partialReplacement')"/></div>
                    </div>
                </div>
                <div style="width : 355px;">
                    <div style="height: 26px;text-align: right;margin-right: 100px;">
                        <input type="radio" class="" name="image6Mode" value="temporary">&nbsp;
                           <div class="inputMatterProduct" style="text-align:left;"><s:property value="getText('aNew')"/></div>
                        <input type="radio" class="real_mode" name="image6Mode" checked="checked" value="real">&nbsp;
                           <div class="inputMatterProduct" style="text-align:left;"><s:property value="getText('production')"/></div>
                    </div>
                    <div class="textColumn imageFindThumb">
                        <span><s:property value="getText('findThumb')"/></span>&nbsp;
                        <input type="text" class="image6FindThumb nonSpecialChar k-textbox find_text" maxlength="510" name="image6FindThumb" id="" 
		                        value="" style="width:205px;float:right;"
		                        data-parameters='<s:property value="getText('findThumb')"/>'/>
                    </div>
                    <div class="textColumn imageReplaceThumb">
                        <span><s:property value="getText('replaceThumb')"/></span>&nbsp;
                        <input type="text" class="image6ReplaceThumb nonSpecialChar k-textbox" maxlength="510" name="image6ReplaceThumb" id="" 
		                        value="" style="width:205px;float:right;"
		                        data-parameters='<s:property value="getText('replaceThumb')"/>'/>
                    </div>
                    <div class="textColumn imageFindDetail">
                        <span><s:property value="getText('findImage')"/></span>&nbsp;
                        <input type="text" class="image6FindImage nonSpecialChar k-textbox find_text" maxlength="510" name="image6FindImage" id="" 
		                        value="" style="width:205px;float:right;"
		                        data-parameters='<s:property value="getText('findImage')"/>'/>
                    </div>
                    <div class="textColumn imageReplaceDetail">
                        <span><s:property value="getText('replaceImage')"/></span>&nbsp;
                        <input type="text" class="image6ReplaceImage nonSpecialChar k-textbox" maxlength="510" name="image6ReplaceImage" id="" 
		                        value="" style="width:205px;float:right;"
		                        data-parameters='<s:property value="getText('replaceImage')"/>'/>
                    </div>
                </div>
            </li>
            <!-- End image6 -->
            <!-- Start image7 -->
            <li style="display: flex;margin-bottom: 2px;">
                <div style="width : 112px">
                    <div style="text-align: initial; height: 26px;float:left;margin-left: 4px;">
                        <input type="checkbox" class="replace_checkbox" value="image7" id=""/>&nbsp;
                            <div class="checkColumn1 inputMatterProduct"><s:property value="getText('image7')"/></div>
                    </div>
                    <div style="height: 26px;float:left;margin-left: 20px;">
                        <input type="radio" class="edit replace_all" name="image7" value="0">&nbsp;
                           <div class="inputMatterProduct image7" style="text-align:left;"><s:property value="getText('replacement')"/></div>
                    </div>
                    <div style="height: 26px;float:left;margin-left: 20px;">
                        <input type="radio" class="edit" name="image7" value="1">&nbsp;
                           <div class="inputMatterProduct image7" style="text-align:left;"><s:property value="getText('frontMatter')"/></div>
                    </div>
                    <div style="height: 26px;float:left;margin-left: 20px;">
                        <input type="radio" class="edit" name="image7" value="2">&nbsp;
                           <div class="inputMatterProduct image7" style="text-align:left;"><s:property value="getText('retrofit')"/></div>
                    </div>
                    <div style="height: 26px;float:left;margin-left: 20px;">
                        <input type="radio" class="replace_image" name="image7" value="3">&nbsp;
                           <div class="inputMatterProduct image7" style="text-align:left;"><s:property value="getText('partialReplacement')"/></div>
                    </div>
                </div>
                <div style="width : 355px;">
                    <div style="height: 26px;text-align: right;margin-right: 100px;">
                        <input type="radio" class="" name="image7Mode" value="temporary">&nbsp;
                           <div class="inputMatterProduct" style="text-align:left;"><s:property value="getText('aNew')"/></div>
                        <input type="radio" class="real_mode" name="image7Mode" checked="checked" value="real">&nbsp;
                           <div class="inputMatterProduct" style="text-align:left;"><s:property value="getText('production')"/></div>
                    </div>
                    <div class="textColumn imageFindThumb">
                        <span><s:property value="getText('findThumb')"/></span>&nbsp;
                        <input type="text" class="image7FindThumb nonSpecialChar k-textbox find_text" maxlength="510" name="image7FindThumb" id="" 
		                        value="" style="width:205px;float:right;"
		                        data-parameters='<s:property value="getText('findThumb')"/>'/>
                    </div>
                    <div class="textColumn imageReplaceThumb">
                        <span><s:property value="getText('replaceThumb')"/></span>&nbsp;
                        <input type="text" class="image7ReplaceThumb nonSpecialChar k-textbox" maxlength="510" name="image7ReplaceThumb" id="" 
		                        value="" style="width:205px;float:right;"
		                        data-parameters='<s:property value="getText('replaceThumb')"/>'/>
                    </div>
                    <div class="textColumn imageFindDetail">
                        <span><s:property value="getText('findImage')"/></span>&nbsp;
                        <input type="text" class="image7FindImage nonSpecialChar k-textbox find_text" maxlength="510" name="image7FindImage" id="" 
		                        value="" style="width:205px;float:right;"
		                        data-parameters='<s:property value="getText('findImage')"/>'/>
                    </div>
                    <div class="textColumn imageReplaceDetail">
                        <span><s:property value="getText('replaceImage')"/></span>&nbsp;
                        <input type="text" class="image7ReplaceImage nonSpecialChar k-textbox" maxlength="510" name="image7ReplaceImage" id="" 
		                        value="" style="width:205px;float:right;"
		                        data-parameters='<s:property value="getText('replaceImage')"/>'/>
                    </div>
                </div>
            </li>
            <!-- End image7 -->
            <!-- Start image8 -->
            <li style="display: flex;margin-bottom: 2px;">
                <div style="width : 112px">
                    <div style="text-align: initial; height: 26px;float:left;margin-left: 4px;">
                        <input type="checkbox" class="replace_checkbox" value="image8" id=""/>&nbsp;
                            <div class="checkColumn1 inputMatterProduct"><s:property value="getText('image8')"/></div>
                    </div>
                    <div style="height: 26px;float:left;margin-left: 20px;">
                        <input type="radio" class="edit replace_all" name="image8" value="0">&nbsp;
                           <div class="inputMatterProduct image8" style="text-align:left;"><s:property value="getText('replacement')"/></div>
                    </div>
                    <div style="height: 26px;float:left;margin-left: 20px;">
                        <input type="radio" class="edit" name="image8" value="1">&nbsp;
                           <div class="inputMatterProduct image8" style="text-align:left;"><s:property value="getText('frontMatter')"/></div>
                    </div>
                    <div style="height: 26px;float:left;margin-left: 20px;">
                        <input type="radio" class="edit" name="image8" value="2">&nbsp;
                           <div class="inputMatterProduct image8" style="text-align:left;"><s:property value="getText('retrofit')"/></div>
                    </div>
                    <div style="height: 26px;float:left;margin-left: 20px;">
                        <input type="radio" class="replace_image" name="image8" value="3">&nbsp;
                           <div class="inputMatterProduct image8" style="text-align:left;"><s:property value="getText('partialReplacement')"/></div>
                    </div>
                </div>
                <div style="width : 355px;">
                    <div style="height: 26px;text-align: right;margin-right: 100px;">
                        <input type="radio" class="" name="image8Mode" value="temporary">&nbsp;
                           <div class="inputMatterProduct" style="text-align:left;"><s:property value="getText('aNew')"/></div>
                        <input type="radio" class="real_mode" name="image8Mode" checked="checked" value="real">&nbsp;
                           <div class="inputMatterProduct" style="text-align:left;"><s:property value="getText('production')"/></div>
                    </div>
                    <div class="textColumn imageFindThumb">
                        <span><s:property value="getText('findThumb')"/></span>&nbsp;
                        <input type="text" class="image8FindThumb nonSpecialChar k-textbox find_text" maxlength="510" name="image8FindThumb" id="" 
		                        value="" style="width:205px;float:right;"
		                        data-parameters='<s:property value="getText('findThumb')"/>'/>
                    </div>
                    <div class="textColumn imageReplaceThumb">
                        <span><s:property value="getText('replaceThumb')"/></span>&nbsp;
                        <input type="text" class="image8ReplaceThumb nonSpecialChar k-textbox" maxlength="510" name="image8ReplaceThumb" id="" 
		                        value="" style="width:205px;float:right;"
		                        data-parameters='<s:property value="getText('replaceThumb')"/>'/>
                    </div>
                    <div class="textColumn imageFindDetail">
                        <span><s:property value="getText('findImage')"/></span>&nbsp;
                        <input type="text" class="image8FindImage nonSpecialChar k-textbox find_text" maxlength="510" name="image8FindImage" id="" 
		                        value="" style="width:205px;float:right;"
		                        data-parameters='<s:property value="getText('findImage')"/>'/>
                    </div>
                    <div class="textColumn imageReplaceDetail">
                        <span><s:property value="getText('replaceImage')"/></span>&nbsp;
                        <input type="text" class="image8ReplaceImage nonSpecialChar k-textbox" maxlength="510" name="image8ReplaceImage" id="" 
		                        value="" style="width:205px;float:right;"
		                        data-parameters='<s:property value="getText('replaceImage')"/>'/>
                    </div>
                </div>
            </li>
            <!-- End image8 -->
            <!-- Start image9 -->
            <li style="display: flex;margin-bottom: 2px;">
                <div style="width : 112px">
                    <div style="text-align: initial; height: 26px;float:left;margin-left: 4px;">
                        <input type="checkbox" class="replace_checkbox" value="image9" id=""/>&nbsp;
                            <div class="checkColumn1 inputMatterProduct"><s:property value="getText('image9')"/></div>
                    </div>
                    <div style="height: 26px;float:left;margin-left: 20px;">
                        <input type="radio" class="edit replace_all" name="image9" value="0">&nbsp;
                           <div class="inputMatterProduct image9" style="text-align:left;"><s:property value="getText('replacement')"/></div>
                    </div>
                    <div style="height: 26px;float:left;margin-left: 20px;">
                        <input type="radio" class="edit" name="image9" value="1">&nbsp;
                           <div class="inputMatterProduct image9" style="text-align:left;"><s:property value="getText('frontMatter')"/></div>
                    </div>
                    <div style="height: 26px;float:left;margin-left: 20px;">
                        <input type="radio" class="edit" name="image9" value="2">&nbsp;
                           <div class="inputMatterProduct image9" style="text-align:left;"><s:property value="getText('retrofit')"/></div>
                    </div>
                    <div style="height: 26px;float:left;margin-left: 20px;">
                        <input type="radio" class="replace_image" name="image9" value="3">&nbsp;
                           <div class="inputMatterProduct image9" style="text-align:left;"><s:property value="getText('partialReplacement')"/></div>
                    </div>
                </div>
                <div style="width : 355px;">
                    <div style="height: 26px;text-align: right;margin-right: 100px;">
                        <input type="radio" class="" name="image9Mode" value="temporary">&nbsp;
                           <div class="inputMatterProduct" style="text-align:left;"><s:property value="getText('aNew')"/></div>
                        <input type="radio" class="real_mode" name="image9Mode" checked="checked" value="real">&nbsp;
                           <div class="inputMatterProduct" style="text-align:left;"><s:property value="getText('production')"/></div>
                    </div>
                    <div class="textColumn imageFindThumb">
                        <span><s:property value="getText('findThumb')"/></span>&nbsp;
                        <input type="text" class="image9FindThumb nonSpecialChar k-textbox find_text" maxlength="510" name="image9FindThumb" id="" 
		                        value="" style="width:205px;float:right;"
		                        data-parameters='<s:property value="getText('findThumb')"/>'/>
                    </div>
                    <div class="textColumn imageReplaceThumb">
                        <span><s:property value="getText('replaceThumb')"/></span>&nbsp;
                        <input type="text" class="image9ReplaceThumb nonSpecialChar k-textbox" maxlength="510" name="image9ReplaceThumb" id="" 
		                        value="" style="width:205px;float:right;"
		                        data-parameters='<s:property value="getText('replaceThumb')"/>'/>
                    </div>
                    <div class="textColumn imageFindDetail">
                        <span><s:property value="getText('findImage')"/></span>&nbsp;
                        <input type="text" class="image9FindImage nonSpecialChar k-textbox find_text" maxlength="510" name="image9FindImage" id="" 
		                        value="" style="width:205px;float:right;"
		                        data-parameters='<s:property value="getText('findImage')"/>'/>
                    </div>
                    <div class="textColumn imageReplaceDetail">
                        <span><s:property value="getText('replaceImage')"/></span>&nbsp;
                        <input type="text" class="image9ReplaceImage nonSpecialChar k-textbox" maxlength="510" name="image9ReplaceImage" id="" 
		                        value="" style="width:205px;float:right;"
		                        data-parameters='<s:property value="getText('replaceImage')"/>'/>
                    </div>
                </div>
            </li>
            <!-- End image9 -->
            <!-- Start image10 -->
            <li style="display: flex;margin-bottom: 2px;">
                <div style="width : 112px">
                    <div style="text-align: initial; height: 26px;float:left;margin-left: 4px;">
                        <input type="checkbox" class="replace_checkbox" value="image10" id=""/>&nbsp;
                            <div class="checkColumn1 inputMatterProduct"><s:property value="getText('image10')"/></div>
                    </div>
                    <div style="height: 26px;float:left;margin-left: 20px;">
                        <input type="radio" class="edit replace_all" name="image10" value="0">&nbsp;
                           <div class="inputMatterProduct image10" style="text-align:left;"><s:property value="getText('replacement')"/></div>
                    </div>
                    <div style="height: 26px;float:left;margin-left: 20px;">
                        <input type="radio" class="edit" name="image10" value="1">&nbsp;
                           <div class="inputMatterProduct image10" style="text-align:left;"><s:property value="getText('frontMatter')"/></div>
                    </div>
                    <div style="height: 26px;float:left;margin-left: 20px;">
                        <input type="radio" class="edit" name="image10" value="2">&nbsp;
                           <div class="inputMatterProduct image10" style="text-align:left;"><s:property value="getText('retrofit')"/></div>
                    </div>
                    <div style="height: 26px;float:left;margin-left: 20px;">
                        <input type="radio" class="replace_image" name="image10" value="3">&nbsp;
                           <div class="inputMatterProduct image10" style="text-align:left;"><s:property value="getText('partialReplacement')"/></div>
                    </div>
                </div>
                <div style="width : 355px;">
                    <div style="height: 26px;text-align: right;margin-right: 100px;">
                        <input type="radio" class="" name="image10Mode" value="temporary">&nbsp;
                           <div class="inputMatterProduct" style="text-align:left;"><s:property value="getText('aNew')"/></div>
                        <input type="radio" class="real_mode" name="image10Mode" checked="checked" value="real">&nbsp;
                           <div class="inputMatterProduct" style="text-align:left;"><s:property value="getText('production')"/></div>
                    </div>
                    <div class="textColumn imageFindThumb">
                        <span><s:property value="getText('findThumb')"/></span>&nbsp;
                        <input type="text" class="image10FindThumb nonSpecialChar k-textbox find_text" maxlength="510" name="image10FindThumb" id="" 
		                        value="" style="width:205px;float:right;"
		                        data-parameters='<s:property value="getText('findThumb')"/>'/>
                    </div>
                    <div class="textColumn imageReplaceThumb">
                        <span><s:property value="getText('replaceThumb')"/></span>&nbsp;
                        <input type="text" class="image10ReplaceThumb nonSpecialChar k-textbox" maxlength="510" name="image10ReplaceThumb" id="" 
		                        value="" style="width:205px;float:right;"
		                        data-parameters='<s:property value="getText('replaceThumb')"/>'/>
                    </div>
                    <div class="textColumn imageFindDetail">
                        <span><s:property value="getText('findImage')"/></span>&nbsp;
                        <input type="text" class="image10FindImage nonSpecialChar k-textbox find_text" maxlength="510" name="image10FindImage" id="" 
		                        value="" style="width:205px;float:right;"
		                        data-parameters='<s:property value="getText('findImage')"/>'/>
                    </div>
                    <div class="textColumn imageReplaceDetail">
                        <span><s:property value="getText('replaceImage')"/></span>&nbsp;
                        <input type="text" class="image10ReplaceImage nonSpecialChar k-textbox" maxlength="510" name="image10ReplaceImage" id="" 
		                        value="" style="width:205px;float:right;"
		                        data-parameters='<s:property value="getText('replaceImage')"/>'/>
                    </div>
                </div>
            </li>
            <!-- End image10 -->
            <!-- Start combobox Brand -->
            <li style="display: flex;margin-bottom: 2px;">
                <div style="width : 272px">
                    <div style="text-align: initial;">
                        <input type="checkbox" class="TestTemp replace_checkbox" value="brandName" id="cbxBrandList" style="margin-left: 4px;"/>&nbsp;
                            <div class="checkColumn1 inputMatterProduct" style="width: 77px;"><s:property value="getText('brandName')"/></div>
                    </div>
                </div>
                <s:select
                    id="cbbBrandList"
                    name=""
                    list="form.brandList"
                    listKey="brandCode"
                    listValue="name"
                    value=""
                    headerKey="0"
                    headerValue=""
                    cssClass="brandName k-required"
                    cssStyle="width:162px;"
                    data-parameters="getText('brandName')"/>
            </li>
            <!-- End combobox Brand -->
            <!-- Start list price -->
            <li style="display: flex;margin-bottom: 2px;">
                <div style="width : 223px">
                    <div style="text-align: initial; height: 26px;float:right;">
                        <input type="checkbox" class="TestTemp replace_checkbox" value="listPrice" id=""/>&nbsp;
                           <div class="checkColumn1 inputMatterProduct"><s:property value="getText('listPrice')"/></div>
                        <input type="radio" class="edit replace_all" name="listPrice" value="0">&nbsp;
                           <div class="inputMatterProduct"><s:property value="getText('replacement')"/></div>
                        <input type="radio" class="edit" name="listPrice" value="1">&nbsp;
                           <div class="inputMatterProduct"><s:property value="getText('frontMatter')"/></div>
                    </div>
                    <div style="height: 26px;float:right;">
                        <input type="radio" class="edit" name="listPrice" value="2">&nbsp;
                           <div class="inputMatterProduct" style="text-align:left;"><s:property value="getText('retrofit')"/></div>
                        <input type="radio" class="replace" name="listPrice" value="3">&nbsp;
                           <div class="inputMatterProduct"><s:property value="getText('partialReplacement')"/></div>
                    </div>
                </div>
                <div style="width : 287px;margin-left: 5px;">
                    <div style="height: 26px;text-align:right;display: flex;">
                        <span style="margin-right:4px;"><s:property value="getText('find')"/></span>&nbsp;
                        <input type="text" class="price listPriceFind nonSpecialChar find_text find_text k-textbox" maxlength="8" 
                        		name="findProperPrice" id="" value="" style="width:162px;float:right;"
                        		data-parameters='<s:property value="getText('find')"/>'/>
                    </div>
                    <div style="height: 26px;text-align:right;display: flex;">
                        <span style="margin-right:4px;"><s:property value="getText('replace')"/></span>&nbsp;
                        <input type="text" class="price listPrice nonSpecialChar k-required k-textbox" maxlength="8" 
                        		name="replaceProperPrice" id="" value="" style="width:162px;float:right;"
                        		data-parameters='<s:property value="getText('replace')"/>'/>
                    </div>
                </div>
            </li>
            <!-- End list price -->
            <!-- Start division -->
            <li style="display: flex;margin-bottom: 2px;">
                <div style="width : 223px">
                    <div style="text-align: initial; height: 26px;float:right;">
                        <input type="checkbox" class="TestTemp replace_checkbox" value="division" id=""/>&nbsp;
                           <div class="checkColumn1 inputMatterProduct"><s:property value="getText('division')"/></div>
                        <input type="radio" class="edit replace_all" name="division" value="0">&nbsp;
                           <div class="inputMatterProduct"><s:property value="getText('replacement')"/></div>
                        <input type="radio" class="edit" name="division" value="1">&nbsp;
                           <div class="inputMatterProduct"><s:property value="getText('frontMatter')"/></div>
                    </div>
                    <div style="height: 26px;float:right;">
                        <input type="radio" class="edit" name="division" value="2">&nbsp;
                           <div class="inputMatterProduct" style="text-align:left;"><s:property value="getText('retrofit')"/></div>
                        <input type="radio" class="replace" name="division" value="3">&nbsp;
                           <div class="inputMatterProduct"><s:property value="getText('partialReplacement')"/></div>
                    </div>
                </div>
                <div style="width : 287px;margin-left: 5px;">
                    <div style="height: 26px; text-align:right;display: flex;">
                        <span style="margin-right:4px;"><s:property value="getText('find')"/></span>&nbsp;
                        <input type="text" class="price divisionFind nonSpecialChar find_text k-textbox" maxlength="8" 
                        		name="findSupplierPrice" id="" value=""
                        		style="width:162px;"
                        		data-parameters='<s:property value="getText('find')"/>'/>
                    </div>
                    <div style="height: 26px;text-align:right;display: flex;">
                        <span style="margin-right:4px;"><s:property value="getText('replace')"/></span>&nbsp;
                        <input type="text" class="price division nonSpecialChar k-required k-textbox" maxlength="8" 
                        		name="replaceSupplierPrice" id="" value=""
                        		style="width:162px;"
                        		data-parameters='<s:property value="getText('replace')"/>'/>
                    </div>
                </div>
            </li>
            <!-- End division -->
            <!-- Start mutil select -->
            <li style="display: flex;margin-bottom: 2px;height:80px;">
                <div style="width : 150px">
                    <div style="text-align: initial; height: 26px;margin-left: 4px;">
                        <input type="checkbox" class="TestTemp replace_checkbox" value="allTypeFlags" id="cbxProductConditionFlg"/>&nbsp;
                           <div class="checkColumn1 inputMatterProduct" style="width:77px;"><s:property value="getText('allTypeFlags')"/></div>
                    </div>
                </div>
                <div style="width : 285px;">
                    <div>
                        <s:select
                          id="block15MultiSelectProductFlg"
                          list="form.mapProductConditionFlg"
                          multiple="multiple"
                          headerKey="" 
                          headerValue=""
                          cssStyle="background-color:none;"
                          data-parameters="getText('replace')"
                          name="listMultiSelectProductFlg"
                        />
                    </div>
                </div>
            </li>
            <!-- End mutil select -->
        </ul>
        </div>
        <!-- End Column Right -->
    </div>
    <!-- Start Colum Middle -->
    <div>
    <ul>
        <!-- Start descriptionSummary -->
        <li style="display: flex;margin-bottom: 2px; width: 1003px;">
            <div style="width : 223px">
                <div style="text-align: initial; height: 26px;margin-left: 4px;">
                    <input type="checkbox" class="replace_checkbox" value="descriptionSummary" id="cbPopupProductSummary"/>&nbsp;
                        <div class="checkColumn1 inputMatterProduct"><s:property value="getText('summary')"/></div>
                    <input type="radio" class="edit replace_all" name="descriptionSummary" value="0">&nbsp;
                       <div class="inputMatterProduct" style="text-align:left;"><s:property value="getText('replacement')"/></div>
                </div>
                <div class="rowCheckboxDescription">
                    <input type="radio" class="edit" name="descriptionSummary" value="1">&nbsp;
                       <div class="inputMatterProduct" style="text-align:left;"><s:property value="getText('frontMatter')"/></div>
                </div>
                <div class="rowCheckboxDescription">
                    <input type="radio" class="edit" name="descriptionSummary" value="2">&nbsp;
                       <div class="inputMatterProduct" style="text-align:left;"><s:property value="getText('retrofit')"/></div>
                </div>
                <div class="rowCheckboxDescription">
                    <input type="radio" class="replace" name="descriptionSummary" value="3">&nbsp;
                       <div class="inputMatterProduct" style="text-align:left;"><s:property value="getText('partialReplacement')"/></div>
                </div>
            </div>
            <div style="width : 777px;">
                <div style="height: 57px;text-align:right; display: flex;">
                    <span style="margin-right:4px;"><s:property value="getText('find')"/></span>&nbsp;
                    <textarea class="descriptionSummaryFind brResolve nonSpecialChar k-length[35000] find_text k-input" 
                            maxlength="35010" rows="3" cols="25" id="tbPopupProductSummary"
                            style="width:665px;"
                            data-parameters='<s:property value="getText('find')"/>'
                            name="findDescriptionSummary"></textarea>
                </div>
                <div style="height: 57px;text-align:right; display: flex;">
                    <span style="margin-right:4px;"><s:property value="getText('replace')"/></span>&nbsp;
                    <textarea class="descriptionSummary brResolve nonSpecialChar k-length[35000] k-input" 
                    	maxlength="35010" rows="3" cols="25" 
                    	id="tbPopupProductSummary" style="width:665px;" 
                    	data-parameters='<s:property value="getText('replace')"/>' name="replaceDescriptionSummary"></textarea>
                </div>
            </div>
        </li>
        <!-- End descriptionSummary -->

        <!-- Start descriptionSentence -->
        <li style="display: flex;margin-bottom: 2px; width: 1003px;">
            <div style="width : 223px">
                <div style="text-align: initial; height: 26px;margin-left: 4px;">
                    <input type="checkbox" class="replace_checkbox" value="descriptionSentence" id="cbPopupProductSentence"/>&nbsp;
                        <div class="checkColumn1 inputMatterProduct"><s:property value="getText('explanation')"/></div>
                    <input type="radio" class="edit replace_all" name="descriptionSentence" value="0">&nbsp;
                       <div class="inputMatterProduct" style="text-align:left;"><s:property value="getText('replacement')"/></div>  
                </div>
                <div class="rowCheckboxDescription">
                    <input type="radio" class="edit" name="descriptionSentence" value="1">&nbsp;
                       <div class="inputMatterProduct" style="text-align:left;"><s:property value="getText('frontMatter')"/></div>
                </div>
                <div class="rowCheckboxDescription">
                    <input type="radio" class="edit" name="descriptionSentence" value="2">&nbsp;
                       <div class="inputMatterProduct" style="text-align:left;"><s:property value="getText('retrofit')"/></div>
                </div>
                <div class="rowCheckboxDescription">
                    <input type="radio" class="replace" name="descriptionSentence" value="3">&nbsp;
                       <div class="inputMatterProduct" style="text-align:left;"><s:property value="getText('partialReplacement')"/></div>
                </div>
            </div>
            <div style="width : 777px;">
                <div style="height: 57px;text-align:right; display: flex;">
                    <span style="margin-right:4px;"><s:property value="getText('find')"/></span>&nbsp;
                    <textarea class="descriptionSentenceFind brResolve nonSpecialChar find_text k-input" rows="3" cols="25" 
                            id="tbPopupProductSentence"
                            style="width:665px;"
                            data-parameters='<s:property value="getText('find')"/>'
                            name="findDescriptionSentence"></textarea>
                </div>
                <div style="height: 57px;text-align:right; display: flex;">
                    <span style="margin-right:4px;"><s:property value="getText('replace')"/></span>&nbsp;
                    <textarea class="descriptionSentence brResolve nonSpecialChar k-input" rows="3" cols="25" 
                            id="tbPopupProductSentence"
                            style="width:665px;"
                            data-parameters='<s:property value="getText('replace')"/>'
                            name="replaceDescriptionSentence"></textarea>
                </div>
            </div>
        </li>
        <!-- End descriptionSentence -->
        
        <!-- Start descriptionCaution -->
        <li style="display: flex;margin-bottom: 2px; width: 1003px;">
            <div style="width : 223px">
                <div style="text-align: initial; height: 26px;margin-left: 4px;">
                    <input type="checkbox" class="replace_checkbox" value="descriptionCaution" id="cbPopupProductCaution"/>&nbsp;
                        <div class="checkColumn1 inputMatterProduct"><s:property value="getText('caution')"/> </div>
                    <input type="radio" class="edit replace_all" name="descriptionCaution" value="0">&nbsp;
                       <div class="inputMatterProduct" style="text-align:left;"><s:property value="getText('replacement')"/></div>
                </div>
                <div class="rowCheckboxDescription">
                    <input type="radio" class="edit" name="descriptionCaution" value="1">&nbsp;
                       <div class="inputMatterProduct" style="text-align:left;"><s:property value="getText('frontMatter')"/></div>
                </div>
                <div class="rowCheckboxDescription">
                    <input type="radio" class="edit" name="descriptionCaution" value="2">&nbsp;
                       <div class="inputMatterProduct" style="text-align:left;"><s:property value="getText('retrofit')"/></div>
                </div>
                <div class="rowCheckboxDescription">
                    <input type="radio" class="replace" name="descriptionCaution" value="3">&nbsp;
                       <div class="inputMatterProduct" style="text-align:left;"><s:property value="getText('partialReplacement')"/></div>
                </div>
            </div>
            <div style="width : 777px;">
                <div style="height: 57px;text-align:right; display: flex;">
                    <span style="margin-right:4px;"><s:property value="getText('find')"/></span>&nbsp;
                    <textarea class="descriptionCautionFind brResolve nonSpecialChar k-length[6000] find_text k-input" 
                            maxlength="6010" rows="3" cols="25" id="tbPopupProductCaution"
                            style="width:665px;"
                            data-parameters='<s:property value="getText('find')"/>'
                            name="findDescriptionCaution"></textarea>
                </div>
                <div style="height: 57px;text-align:right; display: flex;">
                    <span style="margin-right:4px;"><s:property value="getText('replace')"/></span>&nbsp;
                    <textarea class="descriptionCaution brResolve nonSpecialChar k-length[6000] k-input" 
                            maxlength="6010" rows="3" cols="25" id="tbPopupProductCaution"
                            style="width:665px;float:right;"
                            data-parameters='<s:property value="getText('replace')"/>'
                            name="replaceDescriptionCaution"></textarea>
                </div>
            </div>
        </li>
        <!-- End descriptionCaution -->
        <!-- Start descriptionRemarks -->
        <li style="display: flex;margin-bottom: 2px; width: 1003px;">
            <div style="width : 223px">
                <div style="text-align: initial; height: 26px;margin-left: 4px;">
                    <input type="checkbox" class="replace_checkbox" value="descriptionRemarks" id="cbPopupProductRemarks"/>&nbsp;
                        <div class="checkColumn1 inputMatterProduct"><s:property value="getText('note')"/></div>
                    <input type="radio" class="edit replace_all" name="descriptionRemarks" value="0">&nbsp;
                       <div class="inputMatterProduct" style="text-align:left;"><s:property value="getText('replacement')"/></div>
                </div>
                <div class="rowCheckboxDescription">
                    <input type="radio" class="edit" name="descriptionRemarks" value="1">&nbsp;
                       <div class="inputMatterProduct" style="text-align:left;"><s:property value="getText('frontMatter')"/> </div>
                </div>
                <div class="rowCheckboxDescription">
                    <input type="radio" class="edit" name="descriptionRemarks" value="2">&nbsp;
                       <div class="inputMatterProduct" style="text-align:left;"><s:property value="getText('retrofit')"/></div>
                </div>
                <div class="rowCheckboxDescription">
                    <input type="radio" class="replace" name="descriptionRemarks" value="3">&nbsp;
                       <div class="inputMatterProduct" style="text-align:left;"><s:property value="getText('partialReplacement')"/></div>
                </div>
            </div>
            <div style="width : 777px;">
                <div style="height: 57px;text-align:right; display: flex;">
                    <span style="margin-right:4px;"><s:property value="getText('find')"/></span>&nbsp;
                    <textarea class="descriptionRemarksFind brResolve nonSpecialChar k-length[4000] find_text k-input" 
                            maxlength="4020" rows="3" cols="25" id="tbPopupProductRemarksFind"
                            style="width:665px;"
                            data-parameters='<s:property value="getText('find')"/>'
                            name="findDescriptionRemarks"></textarea>
                </div>
                <div style="height: 57px;text-align:right; display: flex;">
                    <span style="margin-right:4px;"><s:property value="getText('replace')"/></span>&nbsp;
                    <textarea class="descriptionRemarks brResolve nonSpecialChar k-length[4000] k-input" 
                            maxlength="4020" rows="3" cols="25" id="tbPopupProductRemarks"
                            style="width:665px;"
                            data-parameters='<s:property value="getText('replace')"/>'
                            name="replaceDescriptionRemarks"></textarea>
                </div>
            </div>
        </li>
        <!-- End descriptionRemarks -->

        <!-- Start dateOfIssue -->
        <li style="display: flex; margin-bottom: 2px; width: 1003px;">
            <div style="width : 223px">
                <div style="text-align: initial; height: 26px;margin-left: 4px;">
                    <input type="checkbox" class="TestTemp replace_checkbox" value="dateOfIssue" id=""/>&nbsp;
                       <div class="checkColumn1 inputMatterProduct"><s:property value="getText('dateOfIssue')"/></div>
                    <input type="radio" class="edit replace_all" name="dateOfIssue" value="0">&nbsp;
                       <div class="inputMatterProduct"><s:property value="getText('replacement')"/></div>
                    <input type="radio" class="edit" name="dateOfIssue" value="1">&nbsp;
                       <div class="inputMatterProduct"><s:property value="getText('frontMatter')"/></div>
                </div>
                <div style="height: 26px;margin-left:64px;">
                    <input type="radio" class="edit" name="dateOfIssue" value="2">&nbsp;
                       <div class="inputMatterProduct" style="text-align:left;"><s:property value="getText('retrofit')"/></div>
                    <input type="radio" class="replace" name="dateOfIssue" value="3">&nbsp;
                       <div class="inputMatterProduct"><s:property value="getText('partialReplacement')"/></div>
                </div>
            </div>
            <div style="width : 777px;">
                <div style="height: 26px;text-align:right;display: flex;">
                    <span style="margin-right:4px;"><s:property value="getText('find')"/></span>&nbsp;
                    <input type="text" class="dateOfIssueFind nonSpecialChar k-length[30] find_text k-textbox" maxlength="60" 
                            name="findReleaseDate" id="" value=""
                            style="width:665px;"
                            data-parameters='<s:property value="getText('find')"/>'/>
                </div>
                <div style="height: 26px;text-align:right;display: flex;">
                    <span style="margin-right:4px;"><s:property value="getText('replace')"/></span>&nbsp;
                    <input type="text" class="dateOfIssue nonSpecialChar k-required  k-length[30] k-textbox" maxlength="60" 
                            name="replaceReleaseDate" id="" value=""
                            style="width:665px;"
                            data-parameters='<s:property value="getText('replace')"/>'/>
                </div>
            </div>
        </li>
        <!-- End dateOfIssue -->
    </ul>
    </div>
    <!-- End Colum Middle -->
    </div>
</div>
<!-- end Matter Product dialog -->

<!-- begin guest input dialog -->
<div id="guestInputDialog" class="contentInner">
    <ul>
        <li>
            <p style="height: 17px;"><span><s:property value="getText('productGuestInputTitle')"/></span></p>
            <s:textfield id="txtTitle" name="productGuestInputTitle"
                         cssStyle="width:265px;"
                         maxLength="%{@net.webike.RcProductFactory.util.Constant@PRODUCT_GUEST_INPUT_TITLE_WARNING_LENGTH}"
                         cssClass="notComma nonSpecialChar k-input k-required k-length[%{@net.webike.RcProductFactory.util.Constant@PRODUCT_GUEST_INPUT_TITLE_LENGTH}]"
                         data-parameters="getText('productGuestInputTitle')">
            </s:textfield>
        </li>
        <li>
            <p><span><s:property value="getText('itemDescription')"/></span></p>
            <s:textarea id="taDescription" name="itemDescription"
                        cols="30" rows="3"
                        maxLength="%{@net.webike.RcProductFactory.util.Constant@PRODUCT_GUEST_INPUT_DESCRIPTION_WARNING_LENGTH}"
                        cssClass="notComma nonSpecialChar k-input k-required k-length[%{@net.webike.RcProductFactory.util.Constant@PRODUCT_GUEST_INPUT_DESCRIPTION_LENGTH}]"
                        data-parameters="getText('itemDescription')">
            </s:textarea>
        </li>
    </ul>
    <div class="btnDialog btnDialogCenter">
        <input type="button" id="btnDecideGuestInput" class="k-button btnCrud btnDialogCenter" value="<s:property value="getText('decide')"/>" />
    </div>
    <hr>
</div>
<!-- end guest input dialog -->

<!-- begin choose select for Product dialog -->
<div id="selectAttProDialog" class="contentInner" style="width: 400px">
    <div class="modelDetails">
        <div class="btnDialog">
            <input type="button" id="btnDecideSelect" class="k-button btnCrud" value="<s:property value="getText('decide')"/>"/>
        </div>
        <ul>
            <li>
                <ul style="width: 100%">
                    <li><p style="width: 100%; margin-right:0px; text-align: left; padding-left: 5px;"><span><s:property value="getText('listOption')"/></span></p></li>
                    <li>
                        <div id="selectNameList">
	                        <select id="cbxSelectName" <%-- list="form.listAllAttribute" --%> cssStyle="width : 250px"
	                            <%-- listKey="attributeJSon" listValue="attributeName" --%> ></select>
	                    </div>
                        <input type="button" id="btnSelectName" style="width:110px;height:26px; margin-left: 5px;" class="k-button btnCrud" value="<s:property value="getText('addition')"/>"/>
                        <input type="button" id="btnDeteleName" style="width:110px;height:26px; margin-left: 5px;" class="k-button btnCrud btnRed rightPos" value="<s:property value="getText('delete')"/>"/>
                    </li>
                    <li style="height: 100%">
                        <div id="gridSelectMaster" style="width: 100%"></div>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
</div>
<!-- End choose select for Product dialog   -->
<!-- Begin alert dialog -->
<div id="alertDialog" class="dialog">
        <div id="alertMsg" class="message">
            <!-- todo message here -->
        </div>
        <div class="buttonArea">
            <input id='btnAlertOK' class='k-button' type='button'
                  value='<s:property value="getText('message.confirm.yes')"/>'>
       </div>
 </div>
<!-- End alert dialog -->
<!-- Begin confirm dialog -->
<div id="confirmDialog" class="dialog">
        <div id="confirmMsg" class="message">
        <!-- todo message here -->
        </div>
        <div class="buttonArea">
            <input id='btnConfirmOK' class='k-button' type='button'
                  value='<s:property value="getText('message.confirm.yes')"/>'>
            <input id='btnConfirmCancel' class='k-button' type='button'
                  value='<s:property value="getText('message.confirm.no')"/>'>
       </div>
 </div>
 <div id="duplicateDataDialog" class="productPleaseSelectDialog">
    <div class="productPleaseSelectDialogContainer">
        <div class="productPleaseSelectDialogContent">
          <!-- BOE #9410 Tran.Thanh 2014/06/30 : Change 型式 to 年式・型式 -->
<%--               <s:property value="getText('message.error.duplicateValue', {'メーカー, 車種名, 型式'} )"/> --%>
              <s:property value="getText('message.error.duplicateValue', {'メーカー, 車種名, 年式・型式'} )"/>
          <!-- EOE #9410 Tran.Thanh 2014/06/30 : Change 型式 to 年式・型式 -->
        </div>
        <input type="button" class="k-button" id="btnDuplicateData" 
            value='<s:property value="getText('text.btn.yes')"/>' />
    </div>
</div>
<!-- End alert dialog -->

<!-- BOE @rcv!Tran.Thanh 2014/08/12 : add popup upload image -->
<jsp:include page="component/popupUpload.jsp"></jsp:include>
<!-- BOE @rcv!Tran.Thanh 2014/08/12 : add popup upload image -->

<!-- From here for hiddent input to store text -->
<input type="hidden" id="lblGuestInputTitle" value="<s:property value="getText('item')"/>" />
<input type="hidden" id="titleBlock4Dialog" value="<s:property value="getText('attribute')"/>">
<input type="hidden" id="titleModelDialog" value="<s:property value="getText('carModel')"/>">
<input type="hidden" id="titleGridAttributeName" value="<s:property value="getText('atributeName')"/>">
<input type="hidden" id="titleGridAttributeValue" value="<s:property value="getText('contentInformation')"/>">
<input type="hidden" id="titleGridAttributeDisplay" value="<s:property value="getText('displayName')"/>">
<input type="hidden" id="titleGridFitModelMaker" value="<s:property value="getText('maker')"/>">
<input type="hidden" id="titleGridFitModelName" value="<s:property value="getText('bikeModelName')"/>">
<input type="hidden" id="titleGridFitModelModel" value="<s:property value="getText('bikeModel')"/>">
<input type="hidden" id="titleGridFactoryProductSuper" value="<s:property value="getText('superiority')"/>">
<input type="hidden" id="titleGridFactoryProductSuperCode" value="<s:property value="getText('supplierCode')"/>">
<input type="hidden" id="titleGridFactoryProductSuperName" value="<s:property value="getText('supplierName')"/>">
<input type="hidden" id="titleGridFactoryProductSuperTimeCode" value="<s:property value="getText('deliveryTimeCode')"/>">
<input type="hidden" id="titleGridFactoryProductSuperTime" value="<s:property value="getText('deliveryTime')"/>">
<input type="hidden" id="titleTableListLinkReason" value="<s:property value="getText('linkReason')"/>">
<input type="hidden" id="titleTableListLinkURL" value="<s:property value="getText('url')"/>">
<input type="hidden" id="titleTableListVideo" value="<s:property value="getText('title')"/>">
<input type="hidden" id="titleTableListLinkURL" value="<s:property value="getText('url')"/>">
<input type="hidden" id="titleTableListVideo" value="<s:property value="getText('title')"/>">
<input type="hidden" id="titleGridProductNumber" value="<s:property value="getText('identificationNumber')"/>">
<input type="hidden" id="titleGridProductProCode" value="<s:property value="getText('systemProductCode')"/>">
<input type="hidden" id="titleGridProductBrandName" value="<s:property value="getText('brandName')"/>">
<input type="hidden" id="titleGridProductProductName" value="<s:property value="getText('productName')"/>">
<input type="hidden" id="titleGridProductItemCode" value="<s:property value="getText('numbers')"/>">
<input type="hidden" id="titleGridProductSumary" value="<s:property value="getText('summary')"/>">
<input type="hidden" id="titleGridProductOptValue" value="<s:property value="getText('optionValue')"/>">
<input type="hidden" id="titleGridProductItemName" value="<s:property value="getText('itemName')"/>">
<input type="hidden" id="titleGridProductStatus" value="<s:property value="getText('status')"/>">
<input type="hidden" id="titleGridProductBaseError" value="<s:property value="getText('basicError')"/>">
<input type="hidden" id="titleGridProductModelError" value="<s:property value="getText('modelError')"/>">
<input type="hidden" id="titleGridProductAttError" value="<s:property value="getText('attributeError')"/>">
<input type="hidden" id="titleGridProductItemError" value="<s:property value="getText('itemError')"/>">
<input type="hidden" id="titleGridProductListPrice" value="<s:property value="getText('productListPrice')"/>">
<!-- BOE 2014/04/16 #7205 Luong.Dai add hidden of header block 1 -->
<input type="hidden" id="titleProductId" 			value="<s:property value="getText('identificationNumber')"/>">
<input type="hidden" id="titleSyouhinSysCode" 		value="<s:property value="getText('systemProductCode')"/>">
<input type="hidden" id="titleNoSyouhinSysCode" 	value="<s:property value="getText('text.label.syouhinEqual0')"/>">
<!-- EOE 2014/04/16 #7205 Luong.Dai add hidden of header block 1 -->
<!-- BOE Nguyen.Chuong #7205 2014/04/17: add column to map filter from matterDetail. -->
<input type="hidden" id='titleGridProductBrandCode'    value='<s:property value="getText('text.label.matterBrandCode')"/>'>
<input type="hidden" id='titleGridProductEanCode'      value='<s:property value="getText('text.label.matterProductEAN')"/>'>
<input type="hidden" id='titleGridCreatedUserId'       value='<s:property value="getText('text.label.matterProductUser')"/>'>
<input type="hidden" id='titleUpdatedOn'               value='<s:property value="getText('text.label.matterUpdateOn')"/>'>
<!-- EOE Nguyen.Chuong #7205 2014/04/17: add column to map filter from matterDetail. -->
<input type="hidden" id="warningSelected" value="<s:property value="getText('message.warning.reminder.select')"/>">
<input type="hidden" id="confirmDelete" value="<s:property value="getText('message.confirm.delete')"/>">
<input type="hidden" id="titleGridBlock2" value="<s:property value="getText('productImage')"/>">
<input type="hidden" id="titleGridBlock4" value="<s:property value="getText('attribute')"/>">
<input type="hidden" id="titleGridBlock5" value="<s:property value="getText('link')"/>">
<input type="hidden" id="titlePopupGridBlock5" value="<s:property value="getText('linkGeneratorSearch')"/>">
<input type="hidden" id="titleGridBlock6" value="<s:property value="getText('video')"/>">
<input type="hidden" id="titleGridBlock8" value="<s:property value="getText('guestInputInformation')"/>">
<input type="hidden" id="titleGridBlock9" value="<s:property value="getText('product')"/>">
<input type="hidden" id="titleGridBlock11" value="<s:property value="getText('carModel')"/>">
<input type="hidden" id="titleGridBlock12" value="<s:property value="getText('supplier')"/>">
<input type="hidden" id="titleGridBlock15" value="<s:property value="getText('managementProducts')"/>">
<input type="hidden" id="duplicateMsg" value="<s:property value="getText('duplicate')"/>">
<input type="hidden" id="dataNotChange" value="<s:property value="getText('dataNotChange')"/>">
<input type="hidden" id="confirmDeleteBlock1" value="<s:property value="getText('confirmDeleteBlock1')"/>">
<input type="hidden" id="questionDuplicateBlock1" value="<s:property value="getText('questionDuplicate')"/>">
<input type="hidden" id="priorityNotNull" value="<s:property value="getText('priorityNotNull')"/>">
<input type="hidden" id="titlePopupBlockSupplier"   value="<s:property value="getText('titlePopupEditSupplier')"/>">
<input type="hidden" id="haveActuallyUsedBlock6"    value="<s:property value="getText('haveActuallyUsed')"/>">

<input type="hidden" id="titleGridProductGroupColor" value="<s:property value="getText('color')"/>">
<input type="hidden" id="titleGridProductGroupSize" value="<s:property value="getText('size')"/>">
<input type="hidden" id="titleGridProductGroupIdentificationNumber" value="<s:property value="getText('identificationNumber')"/>">
<input type="hidden" id="titleGridProductGroupSysProductCode" value="<s:property value="getText('systemProductCode')"/>">
<input type="hidden" id="titleGridProductGroupNumbers" value="<s:property value="getText('numbers')"/>">
<input type="hidden" id="titleGridProductGroupEAN" value="<s:property value="getText('ean')"/>">
<input type="hidden" id="titleGridProductGroupLstPriceTax" value="<s:property value="getText('listPriceWithTax')"/>">
<input type="hidden" id="titleGridProductGroupPartition" value="<s:property value="getText('partition')"/>">
<input type="hidden" id="titleGridProductGroupPartitionRate" value="<s:property value="getText('partitionRate')"/>">
<input type="hidden" id="confirmSaveBlock1" value="<s:property value="getText('confirmSaveBlock1')"/>">
<input type="hidden" id="updateProductBlock15" value="<s:property value="getText('isUpdateProduct')"/>">
<input id='confirmSaveBlock1Group' type="hidden" value='<s:property value="getText('confirmSaveBlock1Group')"/>'>
<input id='msgWarningBlock1Save' type="hidden" value='<s:property value="getText('msgWarningBlock1Save')"/>'>
<input id='validateBlock1SaveFirst' type="hidden" value='<s:property value="getText('validateBlock1SaveFirst')"/>'>
<input id='confirmSaveBlock1GroupCheckBox' type="hidden" value='<s:property value="getText('confirmSaveBlock1GroupCheckBox')"/>'>
<input id='cannotEditReleaseProduct'    type="hidden"   value='<s:property value="getText('cannotEditReleaseProduct')"/>'>
<input id='maxProductOfMatter'          type="hidden"   value='<s:property value="getText('maxProductOfMatter')"/>'>
<!-- BOE Tran.Thanh 2014/04/04 : get Image Name from request -->
<input id='currentImageName'          type="hidden"   value=''>
<!-- EOE Tran.Thanh 2014/04/04 : get Image Name from request -->
<!-- Message -->
<!-- No data message -->
<input id='noDataMessage'                   type="hidden" value='<s:property value="getText('text.error.noData')"/>'>
<input id='categoryNotExists'               type="hidden" value='<s:property value="getText('message.warning.categoryNotExists')"/>'>
<input id='confirmChangeInputValue'         type="hidden" value='<s:property value="getText('confirmSaveChangeInput')"/>'>
<input id='confirmYes'                      type="hidden" value='<s:property value="getText('message.confirm.yes')"/>'>
<input id='confirmCancel'                   type="hidden" value='<s:property value="getText('message.confirm.no')"/>'>
<input id='addYourEntryField'               type="hidden" value='<s:property value="getText('addYourEntryField')"/>'>
<input id='confirmLoseData'                 type="hidden" value='<s:property value="getText('questionDuplicate')"/>'>
<input id='confirmDeleteAndSaveData'        type="hidden" value='<s:property value="getText('confirmDeleteProductAndSaveData')"/>'>
<input id='noProductSelect'                 type="hidden" value='<s:property value="getText('noProductSelect')"/>'>
<input id='attributeValueWrong'             type="hidden" value='<s:property value="getText('attributeValueWrong')"/>'>
<input id='hdTitle'                         type="hidden" value='<s:property value="getText('confirmation')"/>'>
<input id='limitSelectItem'                 type="hidden" value='<s:property value="getText('limitSelectItem')"/>'>
<input id='noSelectedItem'                  type="hidden" value='<s:property value="getText('noSelectedItem')"/>'>
<input id='requiredSiireOrder'              type="hidden" value='<s:property value="getText('requiredSiireOrder')"/>'>
<input id='duplicateOption'                 type="hidden" value='<s:property value="getText('duplicateOption')"/>'>
<input id='invalidInputMsg'                 type="hidden" value='<s:property value="getText('inputInvalidMsg')"/>'>
<input id='blankOption'                     type="hidden" value='<s:property value="getText('blankOption')"/>'>
<input id='releasedProductNotDelete'        type="hidden" value='<s:property value="getText('releasedProductNotDelete')"/>'>
<input id='deleteProductFail'          		type="hidden" value='<s:property value="getText('message.error.deleteFail')"/>'>
<input id='noProductEdit'          			type="hidden" value='<s:property value="getText('noProductEdit')"/>'>
<!-- BOE Tran.Thanh 2014/05/09 : add hidden field -->
<input id='notSaveCurrentData'              type="hidden" value='<s:property value="getText('notSaveCurrentData')"/>'>
<!-- EOE Tran.Thanh 2014/05/09 : add hidden field -->

<!-- Title for paging -->
<input id='pagingDisplay'                       type="hidden" value='<s:property value="getText('text.paging.display')"/>'>
<input id='pagingEmpty'                         type="hidden" value='<s:property value="getText('text.paging.empty')"/>'>
<input id='pagingItemsPerPage'                  type="hidden" value='<s:property value="getText('text.paging.itemsPerPage')"/>'>
<input id='pagingFirst'                         type="hidden" value='<s:property value="getText('text.paging.first')"/>'>
<input id='pagingPrevious'                      type="hidden" value='<s:property value="getText('text.paging.previous')"/>'>
<input id='pagingNext'                          type="hidden" value='<s:property value="getText('text.paging.next')"/>'>
<input id='pagingLast'                          type="hidden" value='<s:property value="getText('text.paging.last')"/>'>
<input id="yenSymbol"                           type="hidden" value="<s:property value="getText('yenSymbol')"/>" />
<input id="changeFlagValueBlock9"               type="hidden" value="<s:property value="getText('changeFlagValue')"/>" />
<input id="upToCharacter"                       type="hidden" value="<s:property value="getText('upToCharacter')"/>" />
<input id="notComma"                            type="hidden" value="<s:property value="getText('notComma')"/>" />
<input id="comma"                               type="hidden" value="<s:property value="getText('comma')"/>" />

<!-- BOE Nguyen.Chuong 2014/03/28: get format for productCode from Constant -->
<input id="productCodeFormat"                   type="hidden" 
                value="<s:property value="%{@net.webike.RcProductFactory.util.Constant@PRODUCT_CODE_FORMAT_JAVASCRIPT}"/>"/>
<!-- EOE Nguyen.Chuong 2014/03/28 -->
<!-- BOE Nguyen.Chuong #7205 2014/04/17 add filter combobox for userId -->
<input id='hiddenListUser'                      type="hidden" value='<s:select id="cbxSearchUpdatedUserId" 
                                                                               cssClass="cbbFilter"
                                                                               headerKey="" headerValue="" 
                                                                               list="commonForm.listCommonUser"
                                                                               listKey="userId"
                                                                               listValue="%{userLastName + ' ' + userFirstName}"/>'>
<!-- EOE Nguyen.Chuong #7205 2014/04/17 add filter combobox for userId -->
<input id="onlyInput1byteNumeric"                       type="hidden" value="<s:property value="getText('onlyInput1byteNumeric')"/>" />
<!-- BOE Nguyen.Chuong #7205 2014/04/21 get filet from matterEdit -->
<input id="productIdFilterMatter"                       type="hidden" value="<s:property value="form.productSearch.productId"/>" />
<input id="syouhinSysCodeFilterMatter"                  type="hidden" value="<s:property value="form.productSearch.productSyouhinSysCode"/>" />
<input id="brandCodeFilterMatter"                       type="hidden" value="<s:property value="form.productSearch.productBrandCode"/>" />
<input id="brandNameFilterMatter"                       type="hidden" value="<s:property value="form.productSearch.brandName"/>" />
<input id="productNameFilterMatter"                     type="hidden" value="<s:property value="form.productSearch.productName"/>" />
<input id="productCodeFilterMatter"                     type="hidden" value="<s:property value="form.productSearch.productCode"/>" />
<input id="eanCodeFilterMatter"                         type="hidden" value="<s:property value="form.productSearch.productEanCode"/>" />
<input id="registrationFlgFilterMatter"                 type="hidden" value="<s:property value="form.productSearch.showProductRegistrationFlg"/>" />
<input id="generalErrorFlgFilterMatter"                 type="hidden" value="<s:property value="form.productSearch.productGeneralErrorFlg"/>" />
<input id="categoryErrorFlgFilterMatter"                type="hidden" value="<s:property value="form.productSearch.productCategoryErrorFlg"/>" />
<input id="natterFactoryErrorCountModelFilterMatter"    type="hidden" value="<s:property value="form.productSearch.productModelErrorFlg"/>" />
<input id="attributeErrorFlgFilterMatter"               type="hidden" value="<s:property value="form.productSearch.productAttributeErrorFlg"/>" />
<input id="updatedUserIdFilterMatter"                   type="hidden" value="<s:property value="form.productSearch.updatedUserId"/>" />
<input id="updatedOnFromFilterMatter"                   type="hidden" value="<s:property value="form.productSearch.updatedOnFrom"/>" />
<input id="updatedOnToFilterMatter"                     type="hidden" value="<s:property value="form.productSearch.updatedOnTo"/>" />
<input id="pageToFilterMatter"                          type="hidden" value="<s:property value="form.page"/>" />
<input id="pageSizeToFilterMatter"                      type="hidden" value="<s:property value="form.pageSize"/>" />
<!-- <input id="sortFieldToFilterMatter"                     type="hidden" value="productBrandCode" /> -->
<!-- <input id="sortDirToFilterMatter"                       type="hidden" value="desc" />  -->
<input id="sortFieldToFilterMatter"                     type="hidden" value="<s:property value="form.productSearch.sortField"/>" />
<input id="sortDirToFilterMatter"                       type="hidden" value="<s:property value="form.productSearch.sortDir"/>" />
<input id="searchFromMatterDetailFlg"                   type="hidden" value="<s:property value="form.searchFromMatterDetailFlg"/>" />
<input id="regexNumber1Byte2Byte"                       type="hidden" value="<s:property value="%{@net.webike.RcProductFactory.util.Constant@REGEX_NUMBER_1BYTE_2BYTE}"/>" />
<input id="regexNumber1Byte2ByteReplace"                type="hidden" value="<s:property value="%{@net.webike.RcProductFactory.util.Constant@REGEX_NUMBER_1BYTE_2BYTE_REPLACE}"/>" />
<input id="regexDecimalPercentNumberReplace"            type="hidden" value="<s:property value="%{@net.webike.RcProductFactory.util.Constant@REGEX_DECIMAL_PERCENT_NUMBER_REPLACE}"/>" />
<input id="regexDecimalPercentNumber"                   type="hidden" value="<s:property value="%{@net.webike.RcProductFactory.util.Constant@REGEX_DECIMAL_PERCENT_NUMBER}"/>" />
<input id="regexNumber2Byte"                            type="hidden" value="<s:property value="%{@net.webike.RcProductFactory.util.Constant@REGEX_NUMBER_2BYTE}"/>" />
<input id="regexNumber2BytePercent"                     type="hidden" value="<s:property value="%{@net.webike.RcProductFactory.util.Constant@REGEX_NUMBER_2BYTE_PERCENT}"/>" />
<input id="regexNumber2ByteSiireOrder"                  type="hidden" value="<s:property value="%{@net.webike.RcProductFactory.util.Constant@REGEX_NUMBER_2BYTE_SIIRE_ORDER}"/>" />
<input id="regexNumber2Byte0"                           type="hidden" value="<s:property value="%{@net.webike.RcProductFactory.util.Constant@REGEX_NUMBER_2BYTE_0}"/>" />
<input id="regexNumber2Byte1"                           type="hidden" value="<s:property value="%{@net.webike.RcProductFactory.util.Constant@REGEX_NUMBER_2BYTE_1}"/>" />
<input id="regexNumber2Byte2"                           type="hidden" value="<s:property value="%{@net.webike.RcProductFactory.util.Constant@REGEX_NUMBER_2BYTE_2}"/>" />
<input id="regexNumber2Byte3"                           type="hidden" value="<s:property value="%{@net.webike.RcProductFactory.util.Constant@REGEX_NUMBER_2BYTE_3}"/>" />
<input id="regexNumber2Byte4"                           type="hidden" value="<s:property value="%{@net.webike.RcProductFactory.util.Constant@REGEX_NUMBER_2BYTE_4}"/>" />
<input id="regexNumber2Byte5"                           type="hidden" value="<s:property value="%{@net.webike.RcProductFactory.util.Constant@REGEX_NUMBER_2BYTE_5}"/>" />
<input id="regexNumber2Byte6"                           type="hidden" value="<s:property value="%{@net.webike.RcProductFactory.util.Constant@REGEX_NUMBER_2BYTE_6}"/>" />
<input id="regexNumber2Byte7"                           type="hidden" value="<s:property value="%{@net.webike.RcProductFactory.util.Constant@REGEX_NUMBER_2BYTE_7}"/>" />
<input id="regexNumber2Byte8"                           type="hidden" value="<s:property value="%{@net.webike.RcProductFactory.util.Constant@REGEX_NUMBER_2BYTE_8}"/>" />
<input id="regexNumber2Byte9"                           type="hidden" value="<s:property value="%{@net.webike.RcProductFactory.util.Constant@REGEX_NUMBER_2BYTE_9}"/>" />
<input id="regexNumber2ByteDotChar"                     type="hidden" value="<s:property value="%{@net.webike.RcProductFactory.util.Constant@REGEX_NUMBER_2BYTE_DOT_CHAR}"/>" />
<input id="regexNumber2BytePercentChar"                 type="hidden" value="<s:property value="%{@net.webike.RcProductFactory.util.Constant@REGEX_NUMBER_2BYTE_PERCENT_CHAR}"/>" />
<input id="hdMatterNo"                   type="hidden" value="<s:property value="%{form.productMatterNo}"/>" />
<!-- EOE Nguyen.Chuong #7205 2014/04/21 get filet from matterEdit -->
<!-- End of message -->
<!-- End here for hiddent input to store text -->
