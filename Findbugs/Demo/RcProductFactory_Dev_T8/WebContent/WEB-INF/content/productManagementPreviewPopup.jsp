<%@ page language="java" contentType="text/html; charset=Shift_JIS"
    pageEncoding="Shift_JIS"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<div id="previewPopupHTML" style="display:none">
<input id='hddProductOpenPriceFlg'          type="hidden" value='<s:property value="getText('openPrice')"/>'>
<input id='hddProductProperSellingFlg'      type="hidden" value='<s:property value="getText('salePrice')"/>'>
<input id='productOrderProductFlg'          type="hidden" value='<s:property value="getText('buildToOrderManufacturing')"/>'>
<input id='hddProductNoReturnableFlg'       type="hidden" value='<s:property value="getText('notReturnedOrExchanged')"/>'>
<input id='hddProductAmbigousImageFlg'      type="hidden" value='<s:property value="getText('preferenceImage')"/>'>
<!-- Preview popup fitModel grid tittle -->
<input id='titleGridMaker'                  type="hidden" value='<s:property value="getText('text.grid.maker')"/>'>
<input id='titleGridModel'                  type="hidden" value='<s:property value="getText('text.grid.model')"/>'>
<input id='titleGridStyle'                  type="hidden" value='<s:property value="getText('text.grid.style')"/>'>
<!-- Preview popup attribute all type grid tittle -->
<input id='titleGridAtributeName'           type="hidden" value='<s:property value="getText('atributeName')"/>'>
<input id='titleGridDisplayName'            type="hidden" value='<s:property value="getText('displayName')"/>'>
<input id='titleGridAdministrativeName'     type="hidden" value='<s:property value="getText('administrativeName')"/>'>
<!-- Preview popup link grid tittle -->
<input id='titleGridReasonLinkTitle'        type="hidden" value='<s:property value="getText('reasonLinkTitle')"/>'>
<!-- Preview popup video grid tittle -->
<input id='titleGridTitle'                  type="hidden" value='<s:property value="getText('title')"/>'>
<!-- Preview popup guest input grid tittle -->
<input id='titleGridGuestInputTitle'        type="hidden" value='<s:property value="getText('confirmationItem')"/>'>
<input id='titleGridGuestInputDescription'  type="hidden" value='<s:property value="getText('itemDescription')"/>'>
<!-- List image dialog -->
<input id='titleGridImageListThumbnail'             type="hidden" value='<s:property value="getText('thumbnail')"/>'>
<input id='titleGridImageListThumbnailPath'         type="hidden" value='<s:property value="getText('thumbnailPath')"/>'>
<input id='titleGridImageListDetailedImage'         type="hidden" value='<s:property value="getText('detailedImage')"/>'>
<input id='titleGridImageListDetailedPath'          type="hidden" value='<s:property value="getText('detailedPath')"/>'>
<input id='titleGridImageListSequentialOrder'       type="hidden" value='<s:property value="getText('sequentialOrder')"/>'>
<!-- Param groupCode from request to search in main grid -->
<input id='groupCodeParam'                          type="hidden" value='<s:property value="groupCodeParam"/>'>
<input id='brandCodeParam'                          type="hidden" value='<s:property value="brandCodeParam"/>'>
<input id='brandNameParam'                          type="hidden" value='<s:property value="brandNameParam"/>'>
    <div id="previewPopupHorizontal">
        <div id="leftPreviewBlock">
            <div id="simpleDataPopup">
                <div id="imagePreviewBlock">
                    <input type="hidden" id="noImageURL" value="<s:property value='productManagementActionForm.noImageURL'/>">
                    <input type="hidden" id="listThumbCount" value="">
                    <div class="products_main_image_inner">
                        <a id="imageAhref" rel="fancy_box"
                           href="" 
                           class="fancy_box">
                            <img id="imageThumb" src=""/>
                            <img id="imageThumbLoading" src="./images/processing.gif" style="display: none;">
                        </a>
                    </div>
                    <form name="thumb_selector_form">
                        <div id="thumb_selector_id" class="thumb_selector clearfix"></div>
                    </form>
                    <div id="thumbHiddenDiv"></div>
                    <input type="button" class="btn-color-default lstButton k-button" id="btnListImage" value='<s:property value="getText('imageList')"/>'>
                </div>
                <div id="productInfoRightPreviewBlock" style="height: 260px;">
                    <table><tbody>
                        <tr>
                            <td class="infoTitle"><s:property value="getText('systemProductCode')"/></td>
                            <td id="productInfoSyouhinSysCode">
                                <span class="productInfoShort">
                                        <a href="" style="color: blue;" target="_blank"></a>
                                </span>
                                <input type="button" id="btnSwitchPrevieMode" class="btn-color-default lstButton k-button" value='<s:property value="getText('convenienceDisplay')"/>'>
                            </td>
                        </tr>
                        <tr>
                            <td class="infoTitle"><s:property value="getText('brand')"/></td>
                            <td id="productInfoBrandName">
                                <span class="productInfoShort"></span>
                            </td>
                        </tr>
                        <tr>
                            <td class="infoTitle"><s:property value="getText('category')"/></td>
                            <td id="productInfoCategoryName">
                                <span class="productInfoShort"></span>
                            </td>
                        </tr>
                        <tr>
                            <td class="infoTitle"><s:property value="getText('productName')"/></td>
                            <td id="productInfoProductName">
                                <span class="productInfoShort"></span>
                            </td>
                        </tr>
                        <tr>
                            <td class="infoTitle"><s:property value="getText('productStockNumber')"/></td>
                            <td id="productInfoProductCode">
                                <span class="productInfoShort"></span>
                            </td>
                        </tr>
                        <tr>
                            <td class="infoTitle"><s:property value="getText('jan')"/></td>
                            <td id="productInfoProductEanCode">
                                <span class="productInfoShort"></span>
                            </td>
                        </tr>
                        <tr>
                            <td class="infoTitle"><s:property value="getText('group')"/></td>
                            <td id="productInfoProductGroupCode">
                                <span class="productInfoShort">
                                        <a href="#" style="color: blue;" target="_blank"></a>
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td class="infoTitle"><s:property value="getText('listPriceWithTax')"/></td>
                            <td id="productInfoProductProperPrice">
                                <span class="productInfoShort"></span>
                            </td>
                        </tr>
                        <tr>
                            <td class="infoTitle"><s:property value="getText('partition')"/></td>
                            <td id="productInfoSikiriPrice">
                                <span class="productInfoShort"></span>
                            </td>
                        </tr>
                        <tr>
                            <td class="infoTitle"><s:property value="getText('dateOfIssue')"/></td>
                            <td id="productInfoProductSupplierReleaseDate">
                                <span class="productInfoShort"></span>
                            </td>
                        </tr>
                    </tbody></table>
                    
                </div>
                <div id="productInfoBottom">
                    <table><tbody>
                        <tr>
                            <td class="infoTitle"><s:property value="getText('variousFlag')"/></td>
                            <td id="productInfoProductCondition">
                                <select id="multiselectProductCondition" multiple="multiple"></select>
                            </td>
                        </tr>
                        <tr>
                            <td class="infoTitle"><s:property value="getText('option')"/></td>
                            <td id="productInfoProductHoyujiName">
                                <span class="productInfoShort"></span>
                            </td>
                        </tr>
                        <tr>
                            <td class="infoTitle"><s:property value="getText('summary')"/></td>
                            <td id="productInfoProductDescriptionSummary">
                                <span class="productInfoShort"></span>
                            </td>
                        </tr>
                        <tr>
                            <td class="infoTitle"><s:property value="getText('supplier')"/></td>
                            <td id="productInfoProductSiireNoukoName">
                                <span class=""></span>
                            </td>
                        </tr>
                    </tbody></table>
                </div>
            </div>
            <div id="fitModelPreviewBlock" style="display:none;" >
                <div class="infoTitle" ><s:property value="getText('supportedBike')"/></div>
                <div class="kendoWrapDiv">
                    <div id="previewPopupFitModelGrid"></div>
                </div>
            </div>
            <div id="guestInputPreviewBlock" style="display:none;">
                <div class="infoTitle" ><s:property value="getText('customerConfirmationItem')"/></div>
                <div class="kendoWrapDiv">
                    <div id="previewPopupGuestInputGrid"></div>
                </div>
            </div>
        </div>
        <div id="rightPreviewBlockTitle" class="infoTitle" style="display:none;"><s:property value="getText('productDetail')"/></div>
        <div id="rightReviewBlock" style="display:none;" >
            <div id="descriptionUpperPreviewBlock">
                <div id="descriptionRemarksPreviewBLock">
                    <div class="title"><s:property value="getText('note')"/></div>
                    <span></span>
                </div>
                <div id="descriptionCautionPreviewBLock">
                    <div class="title"><s:property value="getText('caution')"/></div>
                    <span></span>
                </div>
            </div>
            <div id="descriptionSentencePreviewBLock">
                <div class="title"><s:property value="getText('explanation')"/></div>
                <span></span>
            </div>
            <div id="attributePreviewBlock">
                <div class="title" ><s:property value="getText('attribute')"/></div>
                <div class="kendoWrapDiv">
                    <div id="previewPopupAttributeAllTypeGrid"></div>
                </div>
            </div>
            <div id="linkReasonVideoPreviewBlock">
                <div id="linkReasonPreviewBlock">
                    <div class="title" ><s:property value="getText('link')"/></div>
                    <div id="previewPopuplinkReasonGrid"></div>
                </div>
                <div id="videoPreviewBlock">
                    <div class="title" ><s:property value="getText('video')"/></div>
                    <div id="previewPopupVideoPreviewGrid"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- begin display Video Player dialog -->
<div id="videoPlayerDialogPreviewPopup" class="contentInner">
    <ul style="padding-left:4px;">
       <li style="display: flex;margin-bottom: 2px;">
           <p><span id="productVideoTitlePreviewPopup"></span></p>
       </li>
       <li style="display: flex;margin-bottom: 2px;">
            <iframe id="popupVideoPreviewPopup" width="420" height="320" src="">
            </iframe>
       </li>
       <li style="display: flex;margin-bottom: 2px;">
           <p><span id="productVideoDescriptionPreviewPopup"></span></p>
       </li>
    </ul>
</div>
<div id="imageListDialogPreviewPopup">
    <div class="infoTitle" ><s:property value="getText('imageDetail')"/></div>
    <div id="imageListDialogGridPreviewPopup"></div>
</div>
<!-- End Video Product Dialog -->