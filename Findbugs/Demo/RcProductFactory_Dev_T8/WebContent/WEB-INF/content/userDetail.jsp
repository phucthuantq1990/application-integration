<%@ page language="java" contentType="text/html; charset=Shift-JIS"
    pageEncoding="Shift-JIS"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<tiles:insertDefinition name="layoutCommon">
<tiles:putAttribute name="css">
	<s:if test="%{userDetailActionForm.actionName=='userDetail'}">
		<link rel="stylesheet" type="text/css" href="css/userDetail.css?20140804140000" />
	</s:if>
	<s:else>
		<link rel="stylesheet" type="text/css" href="css/newUser.css?20140804140000" />
	</s:else>
</tiles:putAttribute>
<tiles:putAttribute name="javaScript">
    <script type="text/javascript" src="js/jQuery/jquery.validate.min.js?20140804140000"></script>
    <script type="text/javascript" src="js/localization/messages_ja.js?20140804140000"></script>
	<script type="text/javascript" src="js/userDetailJS.js?20140804140000"></script>
</tiles:putAttribute>
<tiles:putAttribute name="body">
<!-- Contents -->
<div id="contents">
    <div id="content">
    <div id="title">
	<h1><s:property value="%{userDetailActionForm.h1Title}"/></h1>
    </div>
    <!-- contents_inner start -->
    <div id="contents_inner">
        <!--left tab -->
        <s:if test="%{userDetailActionForm.actionName=='userDetail'}">
        	<div id="leftTab" style="float:left;" >
	             <ul>
	                <li id="matterInfo">
	                    <div style="border-bottom-color: #000000;
	                                border-bottom-style: solid;
	                                border-bottom-width: 1px;"><s:property value="getText('text.label.userId')"/>
	                    </div>
	                    <div id="userId"><s:property value="%{userDetailActionForm.entUserTmp.userId}"/></div>
	                    <div>
	                        <s:if test="%{userDetailActionForm.entUserTmp.userDelFlg == 0}">
	                            <div class="lbl-size-medium blueBG" style="margin: 4px;"><s:property value="getText('text.label.userStatus.active')"/></div>
	                        </s:if>
	                        <s:else>
	                            <div class="lbl-size-medium grayBG" style="margin: 4px;"><s:property value="getText('text.label.userStatus.delete')"/></div>
	                        </s:else>
	                    </div>
	                </li>
	                <li>
	                	<s:if test="%{CommonForm.loginAuthority.contains('USER_MANAGER_ROLE_ID')}">
	                    <div class="btn-tab-left">
		                    <s:if test="%{userDetailActionForm.entUserTmp.userDelFlg == 0}">
	                            <input class="btn-size-medium btn-color-red" style="width:127px;"
				                        type="button"
				                        onclick="confirmDeleteUser('<s:property value="userDetailActionForm.entUserTmp.userId"/>','<s:property value="userDetailActionForm.entUserTmp.userDelFlg"/>');" 
				                        value='<s:property value="getText('text.label.btnDeleteUser')"/>'>
	                        </s:if>
	                        <s:else>
	                            <input class="btn-size-medium btn-color-default" style="width:127px;"
				                        type="button"
				                        onclick="confirmUnlockUser('<s:property value="userDetailActionForm.entUserTmp.userId"/>','<s:property value="userDetailActionForm.entUserTmp.userDelFlg"/>');" 
				                        value='<s:property value="getText('text.label.btnUnclockUser')"/>'>
	                        </s:else>
	                    </div>
	                    </s:if>
	                </li>
	            </ul>
	        </div>
        </s:if>
        <!--detail tag-->
        <div  id="<s:property value="%{userDetailActionForm.detailTabId}"/>">
        	<form name="userDetailForm" id="userDetailForm" action='<s:property value="%{userDetailActionForm.submitActionName}"/>' method="post">
                <input type="hidden" name="flgPassChange" id="flgPassChange" value="0"/>
                <ul>
                    <li><p> &nbsp </p>
                        <div>
	                        <span id="successArea" class="successMessage">
                                <s:property value="userDetailActionForm.successMessage"/>
                            </span>
                            <span id="errorArea" class="errorMessage">
                                <s:property value="userDetailActionForm.failMessage"/>
                            </span>
                        </div>
                    </li>
                    <s:if test="%{userDetailActionForm.actionName=='userDetail'}">
                        <input type="hidden" name="userId" id="userIdHidden" value="<s:property value="%{userDetailActionForm.entUserTmp.userId}"/>"/>
                    </s:if>
                    <s:else>
                        <li>
                            <p id="userIdLbl"><s:property value="getText('text.label.userId')"/></p>
                            <div><s:textfield id="userId" name="userDetailActionForm.entUserTmp.userId"
                                     cssClass="txt-size-medium userId"
                                     autocomplete="off"
                                     maxlength="30"
                                     />
                                 <span for="userId" generated="true" class="error"></span>
                            </div>
                                 
                        </li>
                    </s:else>
                    <li><p id="userLastNameLbl"><s:property value="getText('text.label.userLastName')"/></p>
                    	<div>
                            <s:textfield  id="userLastName" name="userDetailActionForm.entUserTmp.userLastName"
                                     	cssClass="txt-size-medium userName userLastName"
                                     	autocomplete="off"
                                     	maxlength="20"/>
                            <span for="userLastName" generated="true" class="error"></span>
                        </div>
                    </li>
                    <li><p id="userFirstNameLbl"><s:property value="getText('text.label.userFirstName')"/></p>
                        <div><s:textfield id="userFirstName" name="userDetailActionForm.entUserTmp.userFirstName"
                                        cssClass="txt-size-medium userName userFirstName"
                                        autocomplete="off"
                                        maxlength="20"/>
                             <span for="userFirstName" generated="true" class="error"></span>
                        </div>
                    </li>
                    <li><p id="userEmail1Lbl"><s:property value="getText('text.label.userEmail1')"/></p>
                        <div><s:textfield id="userEmail1" name="userDetailActionForm.entUserTmp.userEmail"
                                     	cssClass="txt-size-medium userEmail userEmail1"
                                     	autocomplete="off"
                                     	maxlength="160"/>
                        </div>
                    </li>
                    <li><p id="userRedmineApiKeyLabel"><s:property value="getText('text.label.userRedmineApiKey')"/></p>
                        <div>
                                <s:if test="%{userDetailActionForm.actionName=='userDetail'}">
                                    <s:textfield id="userRedmineApiKey" name="userDetailActionForm.entUserTmp.userRedmineApiKey"
                                        cssClass="txt-size-medium userRedmineApiKey"
                                        style="background-color: gainsboro;"
                                        autocomplete="off"
                                        readonly="true"
                                        maxlength="160"/>
                                    <input type="hidden" name="userRedmineApiKeyHidden" id="userRedmineApiKeyHidden" value="<s:property value="%{userDetailActionForm.entUserTmp.userRedmineApiKey}"/>"/>
                                </s:if>
                                <s:else>
	                                <s:textfield id="userRedmineApiKey" name="userDetailActionForm.entUserTmp.userRedmineApiKey"
	                                        cssClass="txt-size-medium userRedmineApiKey"
	                                        style="background-color: gainsboro;"
	                                        autocomplete="off"
                                            readonly="true"
	                                        maxlength="160"/>
	                                <input type="hidden" name="userRedmineApiKeyHidden" id="userRedmineApiKeyHidden" value="<s:property value="%{userDetailActionForm.entUserTmp.userRedmineApiKey}"/>"/>
	                                <input type="button" class="btn-size-small btn-color-default"
                                           id="btn_getApiKey"
                                           style="width:80px;margin-top: 0px;margin-left: 0px;"
                                           value="<s:property value="getText('text.btn.getApiKey')"/>"
                                           onclick="usingRedmineGetApiKey();"/>
                                </s:else>
                                <span for="userRedmineApiKey" id="userRedmineApiKeyError" generated="true" class="error"></span>
                        </div>
                    </li>
                    <s:if test="%{CommonForm.loginAuthority.contains('USER_MANAGER_ROLE_ID') 
                                  || userDetailActionForm.actionName=='newUser'}">
                        <li><p id="userPasswdLbl"><s:property value="getText('text.label.userPasswd')"/></p>
                            <div><s:password id="userPasswd" name="userDetailActionForm.entUserTmp.userPasswd" 
                                    	cssClass="txt-size-medium"
                                    	autocomplete="off"
                                    	maxlength="128"
                                        showPassword="true"/>
                                 <span for="userPasswd" generated="true" class="error"></span>
                            </div>
                        </li>
                        <li><p id="userPasswdConfirmLbl"><s:property value="getText('text.label.userPasswdConfirm')"/></p>
                            <div><s:password id="userPasswdConfirm" name="userDetailActionForm.entUserTmp.userPasswdConfirm"
                                    	cssClass="txt-size-medium"
                                    	autocomplete="off"
                                    	maxlength="128"
                                        showPassword="true"/>
                                 <span for="userPasswdConfirm" generated="true" class="error"></span>
                            </div>
                        </li>
                    </s:if>
                    <s:if test="%{userDetailActionForm.actionName=='newUser'}">
                        <li>
                            <p><s:property value="getText('text.label.status')"/></p>
                            <div><s:select id="userStatus"
                                        name="userDetailActionForm.entUserTmp.userDelFlg"
                                        list="userDetailActionForm.statusTreeMap"
                                        listKey="%{key}"
                                        listValue="%{value}"
                                        required="true"
                                        cssClass="cbb-size-normal"/>
                            </div>
                        </li>
                    </s:if>
                    <li>
                        <s:if test="%{userDetailActionForm.actionName=='userDetail'}">
                            <div>
                                <input type="submit" class="btn-size-medium btn-color-default"
                                	   id="btn_submit"
                                       style="margin-left: 23%; margin-top: 10px; width:90px;"
                                       value='<s:property value="getText('text.btn.update')"/>'/>
                                <s:if test="%{!CommonForm.loginAuthority.contains('USER_MANAGER_ROLE_ID')}">
                                	<input type="button" class="btn-size-medium btn-color-default"
                                	    style="margin-left: 25px; margin-top: 10px; width: 90px; text-decoration: none;"
                                	    value='<s:property value="getText('text.btn.cancel')"/>'
                                	    onclick="window.location='./mainMenu.html';"/>
                                </s:if>
                                <s:else>
	                                <input type="button" class="btn-size-medium btn-color-default"
	                                	    style="margin-left: 25px; margin-top: 10px; width: 90px; text-decoration: none;"
	                                	    value='<s:property value="getText('text.btn.cancel')"/>'
	                                	    onclick="cancelClick();"/>
                                </s:else>
                            </div>
                        </s:if>
                        <s:else>
                            <div style="padding-left: 189px;">
                                <input type="submit" class="btn-size-medium btn-color-default"
                                           id="btn_submit"
                                           style="margin-top: 10px; width:90px;"
                                           value="<s:property value="getText('text.btn.create')"/>"/>
                                <input type="button" class="btn-size-medium btn-color-default"
                                            style="margin-left: 25px; margin-top: 10px; width: 90px; text-decoration: none;"
                                            value='<s:property value="getText('text.btn.cancel')"/>'
                                            onclick="cancelClick();"/>
                            </div>
                        </s:else>
                    </li>
                </ul>
            </form>
        </div>
    </div>
    <!-- contents_inner end-->
    </div>
</div>
<!--End Contents -->
<script type="text/javascript">
/* Error message */
var existedUser = '<s:property value="getText('message.error.existedUser')"/>';
var inputOutOfRange = '<s:property value="getText('message.error.inputOutOfRange')"/>';
var inputLessThanRange = '<s:property value="getText('message.error.inputLessThanRange')"/>';
var IdOrPassInputInvalid = '<s:property value="getText('message.error.IdOrPassInputInvalid')"/>';
var inputInvalid = '<s:property value="getText('message.error.invalidInput')"/>';
var requiredFill = '<s:property value="getText('message.error.requiredFill')"/>';
var inputLengthPasswordInvalid = '<s:property value="getText('message.error.inputLengthPasswordInvalid')"/>';
var inputLengthPasswordConfirmInvalid = '<s:property value="getText('message.error.inputLengthPasswordConfirmInvalid')"/>';
var passConfirmNotEqualPass = '<s:property value="getText('message.error.passConfirmNotEqualPass')"/>';
var canNotGetApiAccessKey = '<s:property value="getText('message.error.canNotGetApiAccessKey')"/>';

jQuery("#btn_submit").click(function() {
	jQuery("#successArea").html("");
	jQuery("#errorArea").html("");
});
function passwordChange() {
    /* change flg to check pass */
    jQuery("#flgPassChange").val("1");
    /* Add class password for pass and passConfirm to validate password format */
    jQuery("#userPasswd").addClass("userPasswd");
    jQuery("#userPasswdConfirm").addClass("userPasswdConfirm");
    /* add class required for password to validate required */
    jQuery("#userPasswd").addClass("userPasswdRequired");
    jQuery("#userPasswdConfirm").addClass("userPasswdConfirmRequired");
}
/*Special modify for userDetail page*/
if("<s:property value='userDetailActionForm.actionName'/>" == 'userDetail') {
    /* Disable loginAuthority if logined as operator */
    /* Show password as 5 * */
    jQuery("#userPasswd").val("*****");

    jQuery("#userPasswd").change(function() {
        passwordChange();
    });
    jQuery("#userPasswdConfirm").change(function() {
        passwordChange();
    });
    /* set password blank if password is default */
    jQuery("#userPasswd").focus(function() {
        if(jQuery("#flgPassChange").val() == 0) {
            jQuery("#userPasswd").val("");
        }
    });
};
/*Special modify for newUser page*/
if("<s:property value='userDetailActionForm.actionName'/>" == 'newUser') {
    passwordChange();
}
</script>
</tiles:putAttribute>
</tiles:insertDefinition>