<%@ page language="java" contentType="text/html; charset=Shift-JIS"
	pageEncoding="Shift-JIS"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<tiles:insertDefinition name="layoutCommon">
	<tiles:putAttribute name="css">
		<link rel="stylesheet" type="text/css" href="css/newMatter.css?20140804140000" />
	</tiles:putAttribute>
	<tiles:putAttribute name="javaScript">
		<script type="text/javascript" src="js/newMatter.js?20140804140000"></script>
		<script type="text/javascript" src="js/jQuery/jquery.validate.min.js?20140804140000"></script>
		<script type="text/javascript" src="js/localization/messages_ja.js?20140804140000"></script>
		<script type="text/javascript">
	          var PJNameNotCorrect = '<s:property value="getText('message.error.nameNotCorrect')"/>';
	          var AdminNotCorrect = '<s:property value="getText('message.error.AdminNotCorrect')"/>';
	          var inputOutOfRange = '<s:property value="getText('message.error.inputOutOfRange')"/>';
	          var matterNameRequired = '<s:property value="getText('message.error.requiredFill')"/>';
	          
		</script>
	</tiles:putAttribute>
	<tiles:putAttribute name="body">
		<!-- Contents -->
		<div id="contents">
			<div id="content">
				<div id="title">
					<h1>
						<s:property value="getText('text.label.newMasterTitel')" />
					</h1>
				</div>
				<!-- contents_inner start -->
				<div id="contents_inner">
					<div id="centerTab">
						<form id='form_new_master' name="matterDetailActionForm"
							id="matterDetailForm" action="addNewMatter.html" method="post">
							<ul>
								<li>
									<p id="kindLabel">
										<s:property value="getText('text.label.kind')" />
									</p>
									<div>
										<span id='kindContext'><s:property
												value="getText('text.label.kindContent')" /></span>
									</div>

								</li>
								<li>
									<p id="pjLabel">
										<s:property value="getText('text.label.projectName')" />
									</p>
									<div>
										<s:textfield id="pjName" name="matterName"
											cssClass="txt-size-medium pjName" autocomplete="off"
											maxlength="510" />
										<span for="pjName" generated="true" class="error"></span>
                                        <span id="errorMessage" class="error" style="display: inline-block;"></span>
									</div>
								</li>
								<li><p>
										<s:property value="getText('text.label.Administrator')" />
									</p>
									<div>
										<s:select id="lstAdmin" name="changeUser"
											list="form.listAdmin" listKey="%{userId}"
											listValue="%{userName}" cssClass="cbb-size-normal" />
									</div></li>
								<li>
									<p id="checkObjLabel">
										<s:property value="getText('text.label.checkObject')" />
									</p>
									<div>
										<s:textfield id="checkObj" name="checkObj"
											cssClass="txt-size-medium userId" autocomplete="off"
											maxlength="24" disabled="true"/>
										<input type="button" value="Browses..." disabled="disabled"/>
									</div>
								</li>
								<li>
									<div style="width: 600px;">
										<input type="button" class="btn-size-medium btn-color-default"
											id="btn_submit"
                                            onclick="insertMatter();"
											style="margin-left: 23%; margin-top: 10px; width: 130px;"
											value='<s:property value="getText('text.label.btnAddProject')"/>' />
										<input type="button" class="btn-size-medium btn-color-default"
										    id="btnCancel"
											style="margin-left: 25px; margin-top: 10px; width: 130px; text-decoration: none;"
											value='<s:property value="getText('text.label.btnCancel')"/>' />
									</div>
								</li>
							</ul>
						</form>
					</div>
				</div>
				<!-- contents_inner end-->
			</div>
		</div>
		<!--End Contents -->
	</tiles:putAttribute>
</tiles:insertDefinition>