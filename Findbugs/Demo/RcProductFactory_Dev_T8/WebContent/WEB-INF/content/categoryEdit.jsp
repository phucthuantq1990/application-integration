<%@ page language="java" contentType="text/html; charset=Shift-JIS"
	pageEncoding="Shift-JIS"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<tiles:insertDefinition name="layoutCommon">
  <tiles:putAttribute name="css">
    <link rel="stylesheet" type="text/css" href="css/categoryEdit.css?20140804140000" />
  </tiles:putAttribute>
  <tiles:putAttribute name="javaScript">
    <script type="text/javascript" src="js/jQuery/jquery.fancybox.pack.js?20140804140000"></script>
    <script type="text/javascript" src="js/jQuery/jquery.fancybox.js?20140804140000"></script>
    <script type="text/javascript" src="js/jQuery/jquery.validate.min.js?20140804140000"></script>
    <script type="text/javascript" src="js/localization/messages_ja.js?20140804140000"></script>
    <script type="text/javascript" src="js/jQuery/jquery-ui.custom.min.js?20140804140000"></script>
    <script src="js/jQuery/jquery.tools.min.js?20140804140000"></script>
    <script type="text/javascript" src="js/categoryEdit.js?20140804140000"></script>
  </tiles:putAttribute>
  <tiles:putAttribute name="body"> 
    <!-- Contents -->
      <div id="contents">
      <div id="content">
            <s:if test="%{form.message != ''}">
                <div id="message" class="has-message msg-error">
                     <!-- BOE hoang.ho 2014/04/11 fix bug message contain <br/> tag -->
                        <s:property value="form.message" escapeHtml="false"/>
                     <!-- EOE hoang.ho 2014/04/11 fix bug message contain <br/> tag -->
                    </div>
            </s:if>
	        <s:else>
	           <div id="message">
               </div>
	        </s:else>
        <!-- start popup search bunrui -->
        <div id="popup_search_bunrui" class="popup-search">
            <input type="text" class="k-input" id="keyword_bunrui">
            <div id="bunrui_search_result">
            </div>
        </div>
        <!-- end popup search bunrui -->
        <!-- start popup search attribute -->
        <div id="popup_search_attribute" class="popup-search">
            <input type="text" class="k-input" id="keyword_attribute">
            <div id="attribute_search_result">
            </div>
        </div>
        <!-- end popup search attribute -->
        <!-- Hidden field -->
        <div>
            <input id='deleteLabel'     type="hidden" value='<s:property value="getText('text.label.btnDelete')"/>'>
            <input id='originalCategoryName'    type="hidden"   value='<s:property value='form.category.categoryName'/>'>
            <input id='originalBunruiCode'      type="hidden"   value='<s:property value='form.category.bunruiCode'/>'>
            <input id='originalDelFlg'          type="hidden"   value='<s:property value='form.category.categoryDelFlg'/>'>
            <input id='title-search-bunrui'     type="hidden"   value='<s:property value="getText('text.label.titleSearchBunrui')"/>'>
            <input id='title-search-attr'       type="hidden"   value='<s:property value="getText('text.label.titleSearchAttr')" />'>
            <!-- Error message -->
            <input id='requiredFill'            type="hidden"   value='<s:property value="getText('message.error.requiredFill')"/>'>
            <input id='only-x-number'           type="hidden"   value='<s:property value="getText('message.error.onlyInputXNumber')"/>'>
            <input id='max-length'              type="hidden"   value='<s:property value="getText('message.error.inputOutOfRange')"/>'>
            <input id='duplicate-value'         type="hidden"   value='<s:property value="getText('message.error.duplicateValue')"/>'>
            <input id='unchange'                type="hidden"   value='<s:property value="getText('message.error.unChange')"/>'>
            <input id='bunrui-not-exists'       type="hidden"   value='<s:property value="getText('message.error.bunruiNotExists')"/>'>
            <input id='category-name-exists'    type="hidden"   value='<s:property value="getText('message.error.existsInDB')"/>'>
            <!-- End error message -->
            <!-- Message confirm -->
            <input id='title-confirm-back'       type="hidden"   value='<s:property value="getText('message.confirm.titleBackToPrevious')"/>'>
            <input id='content-confirm-back'     type="hidden"   value='<s:property value="getText('message.confirm.backToPrevious')"/>'>
            <input id='btn-yes'                  type="hidden"   value='<s:property value="getText('message.confirm.yes')"/>'>
            <input id='btn-no'                  type="hidden"   value='<s:property value="getText('message.confirm.no')"/>'>
            <!-- End of message confirm -->
        </div>
        <!-- end of hidden field -->
          <!-- contents_inner start -->
          <div id="contents_inner">
            <!--left tab -->
            <div id="leftTab">
              <div>
                <h1><s:property value="getText('text.label.titleTab')"/></h1>
                <ul class="tabs">
                  <li id="tabs-info" class="tabStrip"><s:property value="getText('text.label.titleTabInfo')"/></li>
                  <li id="tabs-attr" class="tabStrip"><s:property value="getText('text.label.titleTabAttribute')"/></li>
                </ul>
              </div>
            </div>
              <!--right tab -->
              <div id="rightTab">
                <div>
                  <ul>
                    <li>
                      <s:if test="%{form.insertMode == 0}">
                        <label id="categoryNameLabel">
                          <s:property value="getText('text.label.titleEdit')"/>
                          "<s:property value="%{form.category.categoryName}"/>"
                        </label>
                        <input id='myCategoryNameEdit'       type="hidden"   value='<s:property value="%{form.category.categoryName}"/>'>
                      </s:if>
                      <s:else>
                        <label style="font-size: medium;font-weight: 500;margin-left: 10px;">
                          <s:property value="getText('text.label.titleNew')"/>
                        </label>
                      </s:else>
                        <input type="button" class="btn-color-default lstButton k-button" id="btnSave"
                          value="<s:property value="getText('text.label.btnSave')"/>" tabindex="1"/>
                        <input type="button" class="btn-color-default lstButton k-button"
                          id="btnReload"
                          value="<s:property value="getText('text.label.btnReset')"/>" tabindex="2"/>
                          <!-- BOE THAI.SON -->
                        <%-- <input type="button" class="btn-color-default lstButton k-button"
                          onclick="clickBack();"
                          value="<s:property value="getText('text.label.btnBack')"/>" /> --%>
                          <input type="button" class="btn-color-default lstButton k-button"
                          id="backButton"
                          value="<s:property value="getText('text.label.btnBack')"/>" tabindex="3"/>
                          <!-- EOE THAI.SON -->
                    </li>
                  </ul>
                </div>
                <s:form name="editCategoryActionForm" id="editCategoryActionForm"
                          action="insertOrUpdateBunrui" method="POST">
                  <!-- ######################### Hidden Fields #########################-->
                  <s:hidden name="form.insertMode" id="insertMode"/>
                  <s:hidden name="form.category.categoryCode" id="categoryCode"/>
                  <s:hidden name="form.deletedCodeList" id="deletedCodeList" />
                  <s:hidden name="form.originalAttributeCodeList" id="originalAttributeCodeList" />
                  <!-- ######################### Hidden Fields #########################-->
                  <div class="panes">
                    <!--First content of tab -->
                    <div>
                        <div class="tabTitle">
                            <p><strong><s:property value="getText('text.label.titleTabInfo')"/></strong></p>
                        </div>
                        <ul>
                          <li><p>&nbsp;</p>
                            <div style="padding-top: 5px;"></div>
                          </li>
                          <li>
                            <p>
                              <span id="label-category-name"><s:property value="getText('text.label.categoryName')"/></span>
                              <span class="error">*</span>
                            </p>
                            <div>
                            <!-- --boe thai.son -->
                            <%-- <s:textfield maxlength="20"
                                id="categoryName"
                                name="form.category.categoryName"
                                maxLength="265"
                                data-parameters="getText('text.label.categoryName')"
                                cssClass="k-input txt-size-medium k-required k-ajaxCateNmExist"
                                autocomplete="off" tabindex="4"/> --%>
                              <s:textfield maxlength="20"
                                id="categoryName"
                                name="form.category.categoryName"
                                maxLength="265"
                                data-parameters="getText('text.label.categoryName')"
                                cssClass=" k-textbox txt-size-medium k-required k-ajaxCateNmExist"
                                autocomplete="off" tabindex="4"/>
                               <!--  -- eoe thai.son -->
                              <span class="k-widget k-tooltip k-tooltip-validation k-invalid-msg" data-for="form.category.categoryName" 
                                        role="alert" style="display: none;"><span class="k-icon k-warning"> </span></span>
                              <input type="hidden" id="is-validated-category-name" value="0">
                              <img id="loading-category" alt="loading" src="images/loading.gif">
                              <s:if test="%{form.category.categoryDelFlg == 1}">
                                <div style="display: inline-flex; float: right;">
                                  <input type="checkbox"
                                    name="form.category.categoryDelFlg"
                                    id="delFlag"
                                    value="1"
                                    style="float: right; margin-right: 5px;" checked>
                                  <s:property value="getText('text.label.delFlg')"/>
                                </div>
                              </s:if>
                              <s:else>
                                <div style="display: inline-flex; float: right;  padding-top: 5px; padding-right: 120px;">
                                  <input type="checkbox"
                                    name="form.category.categoryDelFlg"
                                    id="delFlag"
                                    value="0"
                                    style="float: right; margin-right: 5px;" tabindex="7">
                                  <s:property value="getText('text.label.delFlg')"/>
                                </div>
                              </s:else>
                            </div>
                          </li>
                          <li>
                            <p>
                              <span id="label-bunrui-code"><s:property value="getText('text.label.bunruiCode')"/></span>
                              <span class="error">*</span>
                            </p>
                            <div>
                            <!-- --boe thai.son -->
                            <%-- <s:textfield maxlength="20"
                                size="10"
                                id="bunruiCode"
                                name="form.category.bunruiCode"
                                maxLength="4"
                                data-parameters="getText('text.label.bunruiCode')"
                                cssClass="k-input txt-size-medium k-required k-ajaxBunruiCdExist k-bunruiCdLength"
                                autocomplete="off" tabindex="5"/> --%>
                              <s:textfield maxlength="20"
                                size="10"
                                id="bunruiCode"
                                name="form.category.bunruiCode"
                                maxLength="4"
                                data-parameters="getText('text.label.bunruiCode')"
                                cssClass=" k-textbox txt-size-medium k-required k-ajaxBunruiCdExist k-bunruiCdLength"
                                autocomplete="off" tabindex="5"/>
                               <!--  --eoe thai.son -->
                              <span class="k-widget k-tooltip k-tooltip-validation k-invalid-msg" data-for="form.category.bunruiCode" 
                                        role="alert" style="display: none;"><span class="k-icon k-warning"> </span></span>
                              <input type="hidden" id="is-validated-bunrui-code" value="0">
                              <img id="loading-bunrui" alt="loading" src="images/loading.gif">
                              <span id="spnBunruiName"><s:property value="form.category.bunruiName"/></span>
                              <a href="#" id="aBunruiName" tabindex="6"><s:property value="getText('text.label.searchBunrui')"/></a>
                            </div>
                          </li>
                        </ul>
                    </div>
                    <!--Second content of tab -->
                    <div>
                        <div class="tabTitle">
                            <p><strong><s:property value="getText('text.label.titleTabAttribute')"/></strong></p>
                        </div>
                      <ul>
                          <li><p>&nbsp;</p>
                            <div style="padding-top: 5px;"></div>
                          </li>
                          <li>
                            <div style="padding-left: 50px;height: 500px;overflow-y:scroll">
                            <!-- boe thai.son 21-2 bug 73 -->
                              <table id="attrTable">
                              <!-- eoe thai.son 21-2 bug 73 -->
                                <tr>
                                  <th class="column-attr-name"><s:property value="getText('text.label.attrName')"/></th>
                                  <th class="column-attr-type"><s:property value="getText('text.label.attrType')"/></th>
                                  <th class="column-attr-required-flg"><s:property value="getText('text.label.attrRequireFlg')"/></th>
                                  <th class="column-attr-sort">
                                    <span id="header-sort"><s:property value="getText('text.label.attrSort')"/></span>
                                    <span class="error">*</span>
                                  </th>
                                  <th class="column-attr-delete"></th>
                                </tr>
                                <s:iterator value="form.catAttributeList" status="st">
                                  <tr class="tr-catAttribute">
                                    <td class="column-attr-name">
                                      <s:hidden name="form.catAttributeList[%{#st.index}].categoryCode" cssClass="category-code" />
                                      <s:hidden name="form.catAttributeList[%{#st.index}].delFlg" cssClass="del-flg" />
                                      <s:hidden name="form.catAttributeList[%{#st.index}].originalAttribute" cssClass="original-attribute" />
                                      <!-- Start: Long adds 2 line attributeCode and addtribute name. Need to change in JS too.  -->
                                      <s:hidden name="form.catAttributeList[%{#st.index}].attributeCode" cssClass="attribute-code" />
                                      <s:property value="attributeName"/>
                                      <!-- End: Long adds 2 line attributeCode and addtribute name. Need to change in JS too.  -->
                                    </td>
                                    <td class="column-attr-type">
                                      <!-- Start: Long adds 1 line attributeType.-->
                                      <s:property value="attributeType"/>
                                      <!-- End: Long adds 1 line attributeType.-->
                                    </td>
                                    <td  class="column-attr-required-flg">
                                      <s:checkbox cssClass="chkFlg" name="form.catAttributeList[%{#st.index}].chkRequiredFlg" fieldValue="catAttributeList[%{#st.index}].chkRequiredFlg" />
                                      <s:hidden name="form.catAttributeList[%{#st.index}].requiredFlg" cssClass="hdnFlg" />
                                    </td>
                                    <td class="column-attr-sort">
                                      <%-- <s:textfield maxlength="2" name="form.catAttributeList[%{#st.index}].sort" 
                                      cssClass="txtSort k-input" /> --%>
                                     <!--  boe thai.son 21-2 bug 73 -->
                                       <s:textfield maxlength="2" name="form.catAttributeList[%{#st.index}].sort" 
                                      data-parameters="getText('text.label.attrSort')"
                                      cssClass="txtSort k-input k-textbox k-required k-duplicated" /><!-- son -->
                                      <!-- eoe thai.son 21-2 bug 73 -->
                                    </td>
                                    <td class="column-attr-delete">
                                      <input type="button" value="<s:property value="getText('text.label.btnDelete')"/>" class="btnDelete k-button" />
                                    </td>
                                    <td class="error">
                                    </td>
                                  </tr>
                                </s:iterator>
                              </table>
                              <a href="#" id="aAttributeName"><s:property value="getText('text.label.searchAttr')"/></a>
                            </div>
                          </li>
                      </ul>
                    </div>
                  </div>
                  </s:form>
              </div>
            </div>
          </div>
            <!-- Start Kendo Dialog -->
             <div id="confirmBackDialog">
                <div>
                    <p><s:property value="getText('message.confirm.backToPrevious')"/></p>
                    <div style='text-align:center;padding:5px;'>
                        <input class='k-button' type='button'
                              value='<s:property value="getText('message.confirm.yes')"/>' 
                                onclick='backToPreviousPage()' style='margin:7px;padding:5px;width:67px;'>
                        <input id='kendoConfirm_cancel' class='k-button' type='button'
                              value='<s:property value="getText('message.confirm.no')"/>' style='margin:7px;padding:5px;width:67px;'>
                   </div>
                </div>
             </div>
          <!-- contents_inner end-->
        </div>
        <!--End Contents -->
  </tiles:putAttribute>
</tiles:insertDefinition>