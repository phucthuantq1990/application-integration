<%@ page language="java" contentType="text/html; charset=Shift_JIS" pageEncoding="Shift_JIS"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<!-- BOE @rcv!Tran.Thanh 2014/08/13 : popup upload Image  -->
<%-- <script src="http://blueimp.github.io/JavaScript-Load-Image/js/load-image.min.js"></script> --%>
<%-- <script src="http://blueimp.github.io/JavaScript-Canvas-to-Blob/js/canvas-to-blob.min.js"></script> --%>

<script type="text/javascript" src="js/fileUpload/jquery.ui.widget.js"></script>
<script type="text/javascript" src="js/fileUpload/jquery.iframe-transport.js"></script>
<script type="text/javascript" src="js/fileUpload/jquery.fileupload.js"></script>

<script type="text/javascript" src="js/fileUpload/tmpl.js"></script>
<script type="text/javascript" src="js/fileUpload/jquery.fileupload-process.js"></script>
<script type="text/javascript" src="js/fileUpload/jquery.fileupload-image.js"></script>
<script type="text/javascript" src="js/fileUpload/jquery.fileupload-ui.js"></script>

<%-- <script src="http://blueimp.github.io/JavaScript-Canvas-to-Blob/js/canvas-to-blob.min.js"></script> --%>

<%-- <script src="js/vendor/jquery.ui.widget.js"></script> --%>
<script type="text/javascript" src="js/fileUploadImage.js"></script>

<div id="popupUploadImage" class="dialog">
	<input id='mssFileOverMaxSize'               type='hidden' value='<s:property value="getText('message.maximun5MBFile')"/>'/>
	<input id='mssFolderOverMaxSize'             type='hidden' value='<s:property value="getText('message.maximun30MBFileSize')"/>'/>
	<input id='mssFileNameDuplicate'             type='hidden' value='<s:property value="getText('fileNameDuplicate')"/>'/>
	<input id='confirmYesImagePopup'             type="hidden" value='<s:property value="getText('message.confirm.yes')"/>'>
	<input id='confirmCancelImagePopup'          type="hidden" value='<s:property value="getText('message.confirm.no')"/>'>
	<input id='itemImage'                        type="hidden" value='<s:property value="getText('item')"/>'>
    <input id="mssNotInputComma"                 type="hidden" value="<s:property value="getText('notComma')"/>" />
    <input id="comma"                            type="hidden" value="<s:property value="getText('comma')"/>" />
        <!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
        <div class="row fileupload-buttonbar">
            <div class="col-md-7">
               <div class="input-group">
                 <div class="input-group-addon">Search</div>
                 <input id="searchFileName" class="k-textbox" type="" placeholder="example.jpg">
               </div>
            </div>
            <div class="col-md-3">
                <button type="button" id="searchImageName" class="k-button">Search</button>
            </div>
            <form id="upload" method="post" enctype="multipart/form-data" accept-charset="SHIFT_JIS">
            <div class="col-md-7">
                <!-- The fileinput-button span is used to style the file input field as button -->
                <div class="col-md-3" style="padding-left:0px;">
                    <span class="k-button fileinput-button">
                        <i class="glyphicon glyphicon-plus"></i>
                        <span>Add files...</span>
                        <input type="file" name="file" multiple>
                    </span>
                </div>
                <div class="col-md-9">
                    <div class="input-group">
                      <div class="input-group-addon">FileName</div>
                      <input id="fileName" class="k-textbox" type="" placeholder="image name">
                    </div>
                </div>
            </div>
            <div class="col-md-1">
                <input type="button" id="insertImgBtn" value="insert" position="" class="k-button">
            </div>

            <!-- The global progress state -->
            <div class="col-md-4 fileupload-progress fade">
                <!-- The global progress bar -->
                <div id="progress" class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
                    <div class="progress-bar progress-bar-success" style="width:30%;"></div>
                </div>
                <!-- The extended global progress state -->
                <div class="progress-extended">&nbsp;</div>
            </div>
            </form>
        </div>
        <!-- The table listing the files available for upload/download -->
        <div id="drop" style="border : 4px dashed #DADADA; height: 450px;overflow-y: scroll;overflow-x: hidden ">
            <table role="presentation" class="table table-striped">
               <tbody class="files">
               </tbody>
            </table>
        </div>
        <div class="col-md-7 rowTotalSize">
            <span class="label"><s:property value="getText('totalImage')"/> : </span>
            <span id="totalImage"></span>
        </div>
        <div class="col-md-4 rowTotalSize">
            <button type="button" id="refreshPopup" class="k-button" style="float:left;">Refresh</button>
        </div>
        <div class="col-md-7 rowTotalSize">
            <span class="label"><s:property value="getText('totalSizeImage')"/> : </span>
            <span id="totalImageSize"></span>
        </div>
</div>

<input type="hidden" id="uploadImageOnly" type="hidden" value="<s:property value="getText('uploadImageOnly')"/>" />

<!-- The template to display files available for upload -->
<script id="template-upload" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-upload fade">
        <td>
            <span class="preview"></span>
        </td>
        <td>
            <p class="name">{%=file.name%}</p>
            <strong class="error text-danger"></strong>
        </td>
        <td>
            <p class="size">Processing...</p>
            <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="progress-bar progress-bar-success" style="width:0%;"></div></div>
        </td>
        <td>
            {% if (!i && !o.options.autoUpload) { %}
                <button class="btn btn-primary start" disabled>
                    <i class="glyphicon glyphicon-upload"></i>
                    <span>Start</span>
                </button>
            {% } %}
            {% if (!i) { %}
                <button class="btn btn-warning cancel">
                    <i class="glyphicon glyphicon-ban-circle"></i>
                    <span>Cancel</span>
                </button>
            {% } %}
        </td>
    </tr>
{% } %}
</script>
<!-- The template to display files available for download -->
<script id="template-download" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-download fade">
        <td>
            <span class="preview">
                {% if (file.thumbnailUrl) { %}
                    <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" data-gallery><img src="{%=file.thumbnailUrl%}"></a>
                {% } %}
            </span>
        </td>
        <td>
            <p class="name">
                {% if (file.url) { %}
                    <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" {%=file.thumbnailUrl?'data-gallery':''%}>{%=file.name%}</a>
                {% } else { %}
                    <span>{%=file.name%}</span>
                {% } %}
            </p>
            {% if (file.error) { %}
                <div><span class="label label-danger">Error</span> {%=file.error%}</div>
            {% } %}
        </td>
        <td>
            <span class="size">{%=o.formatFileSize(file.size)%}</span>
        </td>
        <td>
            {% if (file.deleteUrl) { %}
                <button class="btn btn-danger delete" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
                    <i class="glyphicon glyphicon-trash"></i>
                    <span>Delete</span>
                </button>
                <input type="checkbox" name="delete" value="1" class="toggle">
            {% } else { %}
                <button class="btn btn-warning cancel">
                    <i class="glyphicon glyphicon-ban-circle"></i>
                    <span>Cancel</span>
                </button>
            {% } %}
        </td>
    </tr>
{% } %}
</script>

<!-- EOE @rcv!Tran.Thanh 2014/08/13 : popup upload Image  -->