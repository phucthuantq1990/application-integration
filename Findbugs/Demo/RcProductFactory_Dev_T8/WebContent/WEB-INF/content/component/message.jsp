<%@ page language="java" contentType="text/html; charset=Shift_JIS"
	pageEncoding="Shift_JIS"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<!-- Begin message -->
	<s:if
		test="%{commonForm.errorMessage != null && commonForm.errorMessage != ''}">
		<div id="message" class="has-message msg-error">
			<s:property value="commonForm.errorMessage" escapeHtml="false"/>
		</div>
	</s:if>
	<s:else>
		<div id="message"></div>
	</s:else>
<!-- end message -->