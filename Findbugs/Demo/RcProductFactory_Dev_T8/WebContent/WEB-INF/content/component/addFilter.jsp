<%@ page language="java" contentType="text/html; charset=Shift_JIS"
    pageEncoding="Shift_JIS"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<!-- Begin add filter -->
    <div class="addedCondition">  
        <fieldset id="filters" class="fieldSet">
            <legend>
                <input id="btnSearchHeader" type="button" class="upDownToggleBtn downToggleBtn floatright"/>
                <strong><span style="display: inline-block;vertical-align: middle;"><s:property value="getText('text.label.category.searchOption')"/></span></strong>
            </legend>
            <div id="searchOptionDiv">
            <!-- begin show hide attributes -->
                <div id="divTemplate" style="display:none">
                    <div id="divContainsId_" class="divContains">
                       <input type="checkbox" name="attrCodeGroup" class="attrCodeGroupChk" checked="checked" />
                       &nbsp;&nbsp;<span class="clsAttrName"></span>
                       &nbsp;&nbsp;
                       
                       <span class="showHidePanel">
                       <s:select list="categoryManageActionForm.entMstAttributeList" 
                            listKey="attributeCode" listValue="attributeName" cssClass="attrCodeGroupSelect" multiple="multiple" data-placeholder="getText('message.pleaseSelectAttribute')">
                       </s:select>
                       &nbsp;&nbsp;
                       
                       <!-- 
                       <a href="#" class="showListbox showHideCls"><img src="./images/bullet_toggle_plus_redmine.png"></a>
                        -->
                       </span>
                    </div>
                </div>
                <div id="divShow"></div>
            <!-- end show hide attributes -->
            <div style="float:right;">
              <s:property value="getText('text.label.category.attribute')"/>
              &nbsp;&nbsp;<select id="cboChooseList">
                   <option value=""></option>
                   <option value="1"><s:property value="getText('text.label.category.belongAttribute')"/></option>
               </select>
           </div>
            </div>
        </fieldset>
    </div>
<!-- end add filter -->