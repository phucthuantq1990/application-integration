<%@ page language="java" contentType="text/html; charset=Shift-JIS"
	pageEncoding="Shift-JIS"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<tiles:insertDefinition name="layoutCommon">
	<tiles:putAttribute name="css">
		<link rel="stylesheet" type="text/css" href="css/mainMenu.css?20140804140000" />
	</tiles:putAttribute>
	<tiles:putAttribute name="javaScript">
		<script type="text/javascript" src="js/jQuery/jquery.freezeheader.js?20140804140000"></script>
		<input type="hidden" id="unnormalProject"
			value="<s:property value="getText('message.error.unnormalProject')"/>" />
		<script type="text/javascript" src="js/mainMenuJS.js?20140804140000"></script>
	</tiles:putAttribute>
	<tiles:putAttribute name="body">
		<!-- Contents -->
		<div id="contents">
			<div id="content">
				<div id="title">
					<h1>
						<s:property value="getText('text.label.mainMenuH1')" />
					</h1>
				</div>
				<!-- contents_inner start -->
				<div id="contents_inner" style="border: none; background: none;">
					<div id="filter">
						<label><s:property value="getText('text.label.status')" /></label>
						<s:if test="null != commonForm.listCommonStatus">
							<s:select headerKey=""
								headerValue="%{getText('text.value.other')}"
								list="commonForm.listCommonStatus" listKey="matterStatusCode"
								listValue="matterStatusName" value="%{status}" name="status"
								cssClass="cbb-size-small" />
						</s:if>
						<s:if test="%{CommonForm.loginAuthority.contains(getText('text.role.userManagerRoleId'))}">
							<label id="userLabel" style="padding-left: 10px;"><s:property
									value="getText('text.label.user')" /></label>
							<s:if test="null != commonForm.listCommonUser">
									<s:select disabled="" headerKey=""
										headerValue="%{getText('text.value.other')}"
										list="commonForm.listCommonUser" listKey="userId"
										listValue="%{userLastName + ' ' + userFirstName}"
										value="%{user}" name="user" cssClass="cbb-size-small" />
							</s:if>
						</s:if>
					</div>
					<div id="mainContent" class="tbl-content">
						<table class="tbl-size-large" id="no4">
							<thead>
								<tr>
									<th width="7%"><span><s:property
												value="getText('text.label.matterNo')" /></span></th>
									<th width="9%"><span><s:property
												value="getText('text.label.tourokuDate')" /></span></th>
									<th width="9%"><span><s:property
												value="getText('text.label.completionDate')" /></span></th>
									<th width="10%"><span><s:property
												value="getText('text.label.matterKindName')" /></span></th>
									<th width="30%"><span><s:property
												value="getText('text.label.matterName')" /></span></th>
									<th width="13%"><span><s:property
												value="getText('text.label.userName')" /></span></th>
									<th width="8%"><span><s:property
												value="getText('text.label.status')" /></span></th>
									<th width="14%"></th>
								</tr>
							</thead>
							<tbody id="mainBody" class="tbl-body">
								<s:if
									test="%{mainMenuActionForm.listMatter!= null && mainMenuActionForm.listMatter.size() >0 }">
									<s:iterator value="%{mainMenuActionForm.listMatter}">
										<tr>
											<td style="text-align: right; width: 7%;"><s:property
													value="matterNo" /></td>
											<td style="text-align: center; width: 9%;"><s:property
													value="convertDateToString(tourokuDate)" /></td>
											<td style="text-align: center; width: 9%;"><s:property
													value="convertDateToString(completionDate)" /></td>
											<td style="text-align: center; width: 10%;"><s:property
													value="matterKindName" /></td>
											<td style="text-align: left; width: 30%;"><s:property
													value="matterName" /></td>
											<td style="text-align: center; width: 13%;"><s:property
													value="userLastName" /> <s:property value="userFirstName" /></td>
											<td style="text-align: center; width: 8%;"><s:property
													value="matterStatusName" /></td>
											<td class="tbl-colum-action" align="center" width="14%">
												<s:if test="%{matterStatusCode=='00'}">
													<a
														href="productInfo.html?matterNo=<s:property value="matterNo"/>"
														onclick="chectButtomClick(<s:property value="matterNo"/>, <s:property value="matterKindCode"/>); return false;"
														class="tbl-control-size-small btn-size-small btn-color-default tipS"
														title="<s:property value="getText('text.label.btnToCheckData')"/>"
														style="color: gray; text-decoration: none;"> <s:property
															value="getText('text.label.btnToCheckData')" /></a>
													<a
														href="matterDetail.html?matterNo=<s:property value="matterNo"/>"
														class="tbl-control-size-small btn-size-small btn-color-default"
														title="<s:property value="getText('text.label.btnToMatterDetail')"/>"
														style="color: gray; text-decoration: none;"> <s:property
															value="getText('text.label.btnToMatterDetail')" /></a>
												</s:if> <s:elseif test="%{matterStatusCode=='10'}">
													<a
														href="productInfo.html?matterNo=<s:property value="matterNo"/>"
														onclick="chectButtomClick(<s:property value="matterNo"/>, <s:property value="matterKindCode"/>); return false;"
														class="tbl-control-size-small btn-size-small btn-color-gold"
														title="<s:property value="getText('text.label.btnToCheckData')"/>"
														style="color: #ffffff; text-decoration: none;"> <s:property
															value="getText('text.label.btnToCheckData')" /></a>
													<a
														href="matter-detail.html?matterNo=<s:property value="matterNo"/>"
														class="tbl-control-size-small btn-size-small btn-color-default"
														title="<s:property value="getText('text.label.btnToMatterDetail')"/>"
														style="color: gray; text-decoration: none;"> <s:property
															value="getText('text.label.btnToMatterDetail')" /></a>
												</s:elseif> <s:elseif test="%{matterStatusCode=='20'">
													<a
														href="productInfo.html?matterNo=<s:property value="matterNo"/>"
														onclick="chectButtomClick(<s:property value="matterNo"/>, <s:property value="matterKindCode"/>); return false;"
														class="tbl-control-size-small btn-size-small btn-color-gold"
														title="<s:property value="getText('text.label.btnToCheckData')"/>"
														style="visibility: hidden;"> <s:property
															value="getText('text.label.btnToCheckData')" /></a>
													<a
														href="matter-detail.html?matterNo=<s:property value="matterNo"/>"
														class="tbl-control-size-small btn-size-small btn-color-default"
														title="<s:property value="getText('text.label.btnToMatterDetail')"/>"
														style="color: gray; text-decoration: none;"> <s:property
															value="getText('text.label.btnToMatterDetail')" /></a>
												</s:elseif>
											</td>
										</tr>
									</s:iterator>
								</s:if>
								<s:else>
									<tr>
										<td colspan="8" class="errorMessage"><s:property
												value="getText('message.error.noData')" /></td>
									</tr>
								</s:else>
							</tbody>
						</table>
					</div>
					<s:if test="%{commonForm.loginAuthority.contains(getText('text.role.masterEditRoleId'))}">
						<div id="btnNewPJ">
							<a href="newMatter.html"
								class="btn-size-medium btn-color-default"
								title="<s:property value="getText('text.label.btnToNewMatter')"/>"
								style="color: gray; text-decoration: none; margin-top: 10px; position: absolute;"><s:property
									value="getText('text.label.btnToNewMatter')" /></a>
						</div>
					</s:if>
				</div>
				<!-- contents_inner -->
				<!-- contents_inner end-->
			</div>
			<!-- vehicle_list -->
		</div>
		<!--End Contents -->
		<script type="text/javascript">
//Load label for button
var checkDataBtnLbl = '<s:property value="getText('text.label.btnToCheckData')"/>';
var ToMatterDetailBtnLbl = '<s:property value="getText('text.label.btnToMatterDetail')"/>';
var nodataMessage = '<s:property value="getText('message.error.noData')"/>';
jQuery(document).ready(function () {
    //Check default for status combobox.
    jQuery('select[name="status"] option[value="00"]').attr('selected','selected');
    
    //Check default for user combobox.
    jQuery('select[name="user"] option[value="<s:property value="commonForm.loginId"/>"]').attr('selected','selected');
    //Showing user combobox by authority.
//     if("<s:property value='commonForm.loginAuthority'/>" != "USER_MANAGER_ROLE_ID" ) {
//         jQuery("#user").hide();
//         jQuery("#userLabel").hide();
//     };
    jQuery("#no4").freezeHeader({ 'height': '546px' });
    //Handler for status combobox change select value.
    jQuery("#status" ).change(function() {
        getMainDataAjax();
    });
    //Handler for user combobox change select value.
    jQuery("#user" ).change(function() {
        getMainDataAjax();
    });
});
</script>

	</tiles:putAttribute>
</tiles:insertDefinition>