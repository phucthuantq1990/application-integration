<%@ page language="java" contentType="text/html; charset=Shift-JIS"
    pageEncoding="Shift-JIS"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<tiles:insertDefinition name="layoutCommon">
<tiles:putAttribute name="css">
    <link rel="stylesheet" type="text/css" href="css/commonManage.css?20140804140000" />
    <link rel="stylesheet" type="text/css" href="css/fitModelCheck.css?20140804140000" />
</tiles:putAttribute>
<tiles:putAttribute name="javaScript">
    <script type="text/javascript" src="js/jQuery/kendo.all.min.js?20140804140000"></script>
    <script type="text/javascript" src="js/jQuery/kendo.culture.ja-JP.min.js?20140804140000"></script>
    <script type="text/javascript" src="js/jQuery/jquery.validate.min.js?20140804140000"></script>
    <script type="text/javascript" src="js/jQuery/shortcut.js?20140804140000"></script>
    <script type="text/javascript" src="js/commonJS.js?20140804140000"></script>
    <script type="text/javascript" src="js/fitModelCheckJS.js?20140804140000"></script>
</tiles:putAttribute>
<tiles:putAttribute name="body">
<!-- Contents -->
<div id="contents">
    <script type="text/javascript" src="js/jQuery/selectable.js?20140804140000"></script>
    <div id="content" class="content">
     <s:if test="%{fitModelCheckActionForm.msgString != null && fitModelCheckActionForm.msgString != ''}">
       <s:if test="%{fitModelCheckActionForm.msgCode == 1}">
          <div id="message" class="has-message success">
            <s:property value="fitModelCheckActionForm.msgString"/>
          </div>
       </s:if>
       <s:else>
          <div id="message" class="has-message msg-error">
            <s:property value="fitModelCheckActionForm.msgString"/>
          </div>
       </s:else>
     </s:if>
     <s:else>
        <div id="message">
        </div>
     </s:else>
    <!-- Link to syouhin page of webike -->
    <input id='linkToSyouhinPage'        type="hidden" value='<s:property value="linkToSyouhinPage"/>'>
    <!--  Param name productId in constant -->
    <input id='productIdParamName'       type="hidden" value='<s:property value="productIdParamName"/>'>
    <!-- No data message -->
    <input id='noDataMessage'            type="hidden" value='<s:property value="getText('text.error.noData')"/>'>
    
    <input id='requiredFill'             type="hidden" value='<s:property value="getText('message.error.requiredFill')"/>'>
    <input id='maxLengthMsg'             type="hidden" value='<s:property value="getText('warningLength')"/>'>
    <input id='invalidInputMsg'          type="hidden" value='<s:property value="getText('inputInvalidMsg')"/>'>
    <!-- Header of grid -->
    <input id='titleGridMaker'           type="hidden" value='<s:property value="getText('text.grid.maker')"/>'>
    <input id='titleGridModel'           type="hidden" value='<s:property value="getText('text.grid.model')"/>'>
    <input id='titleGridStyle'           type="hidden" value='<s:property value="getText('text.grid.style')"/>'>
    <input id='msgSuccess'               type="hidden" value='<s:property value="getText('message.success.updateSuccess')"/>'>
    <input id='msgFail'                  type="hidden" value='<s:property value="getText('message.error.updateFail')"/>'>
    <input id='msgUnchange'              type="hidden" value='<s:property value="getText('message.error.unChange')"/>'>
    <input id='msgProductUnchecked'      type="hidden" value='<s:property value="getText('productUnchecked')"/>'>
    <input id='msgProductCheckCompleted' type="hidden" value='<s:property value="getText('productCheckCompleted')"/>'>
    <input id='msgProductModelErrorFlg'  type="hidden" value='<s:property value="getText('productModelErrorFlg')"/>'>
    <input id='hdTitle'                  type="hidden" value='<s:property value="getText('confirmation')"/>'>
    <input id='hdAddModelDialogTitle'    type="hidden" value='<s:property value="getText('addModelDialogTitle')"/>'>
    <input id='hdDisplayModelProductTitle'  type="hidden" value='<s:property value="getText('displayModelProductTitle')"/>'>
    <input id='prdResgistration'         type="hidden" value='<s:property value="fitModelCheckActionForm.product.productRegistrationFlg"/>'>

    <!-- start title Management of Brand Master -->
    <div id="title">
    	<!-- BOE by Luong.Dai add id for link in header - Fix bug show progress bar when click link -->
        <h1 id="hInfo"><s:property value="getText('matterNumber')"/> / <a id='linkToMatterDetail' style="color: blue;" href="matterDetail.html?matterNo=<s:property value="fitModelCheckActionForm.product.productMatterNo"/>"><s:property value="fitModelCheckActionForm.product.productMatterNo"/></a> : 
            <s:property value="getText('text.label.fitModelCheckH1A')"/> / P<span id="productIdH1"><s:property value="fitModelCheckActionForm.product.productId"/></span>
             : 
             <s:property value="getText('text.label.fitModelCheckH1B')"/> / 
             <span id="productSysCodeH1">
                <s:if test="0 != fitModelCheckActionForm.product.productSyouhinSysCode && null != fitModelCheckActionForm.product.productSyouhinSysCode ">
                    <!-- BOE by Luong.Dai open new tab at 2014/04/01 -->
                    <a id='linkRedirectToSyouhinPage' target="_blank" style="color: blue;" href='<s:property value="linkToSyouhinPage"/><s:property value="fitModelCheckActionForm.product.productSyouhinSysCode"/>/'><s:property value="fitModelCheckActionForm.product.productSyouhinSysCode"/></a>
                    <!-- EOE by Luong.Dai open new tab -->
                </s:if>
                <s:else>
                    <s:property value="getText('text.label.syouhinEqual0')"/>
                </s:else>
             </span>
             <span id="spnProductStatus">
               <s:if test="fitModelCheckActionForm.product.productModelCheckFlg == 0">
                  <s:property value="getText('productUnchecked')"/>
                    <s:if test="fitModelCheckActionForm.product.productModelErrorFlg == 1">
                      <s:property value="getText('productModelErrorFlg')"/>
                    </s:if>
               </s:if>
               <s:else>
                    <s:property value="getText('productCheckCompleted')"/>
                    <s:if test="fitModelCheckActionForm.product.productModelErrorFlg == 1">
                      <s:property value="getText('productModelErrorFlg')"/>
                    </s:if>
               </s:else>
             </span>
        </h1>
        <!-- EOE by Luong.Dai add id for link in header - Fix bug show progress bar when click link -->
    </div>
    <div class="tbl-content">
        <ul style="display: inline-table; padding: 0px;">
            <li>
                <div id="topLeftBlock" class="contentInner">
                    <div class="title"><s:property value="getText('text.label.beforeChange')"/></div>
                    <div class="kendoWrapDiv">
                        <div id="gridModelFit1"></div>
                    </div>
                </div>
                <div id="topRightBlock" class="contentInner">
                    <div class="title">
                        <label>
                            <s:property value="getText('text.label.afterChange')"/>
                        </label>
                        <div class="buttonArea">
                            <form id="fitModelCheckForm" action="processDataFitModelBySingleProduct.html" method="POST" >
                                <input id="btnSave" type="button" class="k-button" value="<s:property value="getText('text.btn.submit')"/>"/>
                                <input id="btnDelete" type="button" class="k-button btnRed" value="<s:property value="getText('text.btn.delete')"/>"/>
                                <input type="hidden" id="productId" name="fitModelCheckActionForm.product.productId" value='<s:property value="fitModelCheckActionForm.product.productId"/>'/>
                                <input type="hidden" id="matterNo" name="fitModelCheckActionForm.factoryMatter.matterNo" value='<s:property value="fitModelCheckActionForm.product.productMatterNo"/>'/>
                                <input type="hidden" id="productErrorFlg" name="fitModelCheckActionForm.product.productModelErrorFlg" value='<s:property value="fitModelCheckActionForm.product.productModelErrorFlg"/>'/>
                                <input type="hidden" id="rqListModelJsonStr" name='<s:property value="fitModelCheckActionForm.listModelJsonStr"/>'/>
                            </form>
                        </div>
                    </div>
                    <div class="kendoWrapDiv">
                        <div id="gridModelFit2"></div>
                    </div>
                </div>
            </li>
            <li>
                <div id="middleLeftBlock" class="contentInner">
                    <div class="title" style="height: 19px;"><s:property value="getText('text.label.productInfoTitle')"/></div>
                    <div id="imageBlock">
                        <input type="hidden" id="noImageURL" value="<s:property value='fitModelCheckActionForm.noImageURL'/>">
                        <input type="hidden" id="listThumbCount" value="<s:property value='fitModelCheckActionForm.listImage.size'/>">
                        <div class="products_main_image_inner">
                            <a id="imageAhref" rel="fancy_box"
                               href="" 
                               class="fancy_box">
                                <img id="imageThumb" src=""/>
                                <img id="imageThumbLoading" src="./images/processing.gif" style="display: none;">
                            </a>
                        </div>
                        <form name="thumb_selector_form">
                            <div class="thumb_selector clearfix"></div>
                        </form>
                        <div id="thumbHiddenDiv">
                            <s:iterator end="3" value="fitModelCheckActionForm.listImage" status="st">
                                <s:if test="fitModelCheckActionForm.listImage[#st.index].productThumb !='' || fitModelCheckActionForm.listImage[#st.index].productImage !=''">
                                    <input type="hidden"
                                        id="thumb_<s:property value="%{#st.index}"/>"
                                        value="<s:property value="%{fitModelCheckActionForm.listImage[#st.index].productThumb}" />" />
                                    <input type="hidden"
                                        id="image_<s:property value="%{#st.index}"/>"
                                        value="<s:property value="%{fitModelCheckActionForm.listImage[#st.index].productImage}" />" />
                                </s:if>
                                <s:else>
                                    <input type="hidden"
                                        id="thumb_<s:property value="%{#st.index}"/>"
                                        value="<s:property value='fitModelCheckActionForm.noImageURL'/>" />
                                    <input type="hidden"
                                        id="image_<s:property value="%{#st.index}"/>"
                                        value="<s:property value='fitModelCheckActionForm.noImageURL'/>" />
                                </s:else>
                            </s:iterator>
                        </div>
                    </div>
                    <div id="productInfo">
                        <table><tbody>
                            <tr>
                                <td class="infoTitle"><s:property value="getText('text.label.productName')"/></td>
                                <td id="productInfoProductName">
<%--                                     <s:if test="fitModelCheckActionForm.product.productName.length() > 8">
                                        <span class="productInfoShort" title="<s:property value='fitModelCheckActionForm.product.productName'/>" >
                                            <s:property value="fitModelCheckActionForm.product.productName"/>
                                        </span>
                                    </s:if>
                                    <s:else> --%>
                                    <span class="productInfoShort">
                                        <s:property value="fitModelCheckActionForm.product.productName"/>
                                    </span>
                                    <%-- </s:else> --%>
                                </td>
                            </tr>
                            <tr>
                                <td class="infoTitle"><s:property value="getText('text.label.makerProductCode')"/></td>
                                <td id="productInfoProductCode">
<%--                                     <s:if test="fitModelCheckActionForm.product.productCode.length() > 8">
                                        <span class="productInfoShort" title="<s:property value='fitModelCheckActionForm.product.productCode'/>" >
                                            <s:property value="fitModelCheckActionForm.product.productCode"/>
                                        </span>
                                    </s:if>
                                    <s:else> --%>
                                    <span class="productInfoShort">
                                        <s:property value="fitModelCheckActionForm.product.productCode"/>
                                    </span>
                                    <%-- </s:else> --%>
                                </td>
                            </tr>
                            <tr class="productSumary">
                                <td class="infoTitle"><s:property value="getText('text.label.productSumary')"/></td>
                                <td id="productInfoDescriptionSummary">
                                    <%-- <s:if test="fitModelCheckActionForm.product.descriptionSummary.length() > 8">
                                        <span class="productInfoShort" title="<s:property value='fitModelCheckActionForm.product.descriptionSummary'/>" >
                                            <s:property value="fitModelCheckActionForm.product.descriptionSummary"/>
                                        </span>
                                    </s:if>
                                    <s:else> --%>
                                    <span class="productInfoShort">
                                        <s:property value="fitModelCheckActionForm.product.descriptionSummary" escapeHtml="false"/>
                                    </span>
                                    <%-- </s:else> --%>
                                </td>
                            </tr>
                            <tr>
                                <td class="infoTitle"><s:property value="getText('text.label.descriptionRemarks')"/></td>
                                <td id="productInfoDescriptionRemarks">
                                    <%-- <s:if test="fitModelCheckActionForm.product.descriptionRemarks.length() > 8">
                                        <span class="productInfoShort" title="<s:property value='fitModelCheckActionForm.product.descriptionRemarks'/>" >
                                            <s:property value="fitModelCheckActionForm.product.descriptionRemarks"/>
                                        </span>
                                    </s:if>
                                    <s:else> --%>
                                    <span class="productInfoShort">
                                        <s:property value="fitModelCheckActionForm.product.descriptionRemarks" escapeHtml="false"/>
                                    </span>
                                    <%-- </s:else> --%>
                                </td>
                            </tr>
                        </tbody></table>
                    </div>
                </div>
                <div id="middleRightBlock" class="contentInner">
                    <div class="modelDetails">
                        <div class="title" style="height: 19px;"><s:property value="getText('text.label.modelInfo')"/></div>
                        <div class="buttonArea">
                            <input type="button" id="btnUpdate2" class="k-button" value="<s:property value="getText('text.label.submit')"/>"/>
                            <input type="button" id="btnInsertMode" class="k-button btnGreen" value="<s:property value="getText('text.label.addNew')"/>"/>
                        </div>
                        <ul style="padding: 0 0 0 3px;">
                            <li style="height: 30px;">
                                <p><span><s:property value="getText('text.label.allModelFitInfo')"/></span></p>
                                <span id="txtAllModelFitInfo" style="margin-top: 5px;float:left"><s:property value="getText('text.label.addNew')"/></span>
                                <input type="hidden" id="hdSort"/>
                            </li>
                            <li style="height: 30px;">
                                <p><span><s:property value="getText('text.grid.maker')"/></span></p>
                                <s:textfield id="txtMaker" name="txtMaker"
	                                cssClass="k-required k-textbox k-length[%{@net.webike.RcProductFactory.util.Constant@FIT_MODEL_MAKER_LENGTH}]
                                              nonSpecialChar"
	                                maxLength="%{@net.webike.RcProductFactory.util.Constant@MODEL_MAKER_LENGTH}"
	                                data-parameters="getText('text.grid.maker')"></s:textfield>
                            </li>
                            <li style="height: 30px;">
                                <p><span><s:property value="getText('text.grid.model')"/></span></p>
                                <s:textfield id="txtModel" name="txtModel"
                                    maxLength="%{@net.webike.RcProductFactory.util.Constant@FIT_MODEL_MODEL_WARNING_LENGTH}"
                                    cssClass="k-textbox k-length[%{@net.webike.RcProductFactory.util.Constant@FIT_MODEL_MODEL_LENGTH}]
                                              nonSpecialChar"
                                    data-parameters="getText('text.grid.model')"></s:textfield>
                            </li>
                            <li>
                            <div style="height: 50px;">
                                <p style="height: 16px;"><span><s:property value="getText('text.grid.style')"/></span></p>
                                <s:textfield id="txtStyle"
                                             maxLength="%{@net.webike.RcProductFactory.util.Constant@MODEL_STYLE_LENGTH}"
                                             cssClass="k-textbox k-length[%{@net.webike.RcProductFactory.util.Constant@FIT_MODEL_STYLE_WARNING_LENGTH}]
                                                       nonSpecialChar"></s:textfield>
                                <div class="candidateArea">
                                 <fieldset id="filters" class="fieldSet">
                                    <legend><s:property value="getText('text.label.setting')"/></legend>
                                    <div><s:property value="getText('text.label.simpleAutoSearch')"/><s:textfield id="candidate" value="3" cssClass="k-textbox"/><s:property value="getText('text.label.charactor')"/></div>
                                    </fieldset>
                                </div>
                            </div>
                            </li>
                        </ul>
                    </div>
                    <div class="title" style="height: 19px;"><s:property value="getText('text.title.allModelGrid')"/></div>
                    <div class="kendoWrapDiv">
                        <div id="gridModelFit3"></div>
                    </div>
                </div>
            </li>
            <li>
                <div id="bottomBlock" class="contentInner">
                    <div class="title">
                        <s:property value="getText('text.title.allProductGrid')"/>
                        <div class="buttonArea">
	                       <input id="btnExcute" type="button" class="btn-color-default lstButton k-button" style="float:right"
	                              value="<s:property value="getText('text.label.excuteProduct')"/>" 
	                              onclick="excuteAction();">
	                        <select name="statusProcessProduct" id="selectExcute" style="width: 200px;" style="float:right" >
	                           <option value="0"><s:property value="getText('text.label.addModel')"/></option>
	                           <option value="1"><s:property value="getText('text.label.displayModelProduct')"/></option>
	                       </select>
	                        <span style="padding:4px 5px 0 5px; float:right"> <s:property value="getText('text.label.action')"/> </span>
	                        <input id="btnSearch" type="button" class="btn-color-default lstButton k-button" value="<s:property value="getText('text.label.btnSearch')"/>" style="float:right"/>
	                        <input id="btnReset" type="button" style="float:right" class="btn-color-default lstButton k-button" value="<s:property value="getText('text.label.btnReset')"/>" style="float:right" />
	                    </div>
                    </div>
                    <div class="kendoWrapDiv">
                        <div id="gridModelFit4"></div>
                    </div>
                </div>
            </li>
        </ul>
      </div>
    </div>
    <!-- Begin hidden fields -->
    <div>
        <!--bottom grid -->
        <input id='textSyouhinEqual0'                             type="hidden" value='<s:property value="getText('text.label.syouhinEqual0')"/>'>
        <input id='textProductModelCheckFlgEqual0'                type="hidden" value='<s:property value="getText('text.label.productModelCheckFlg.equal0')"/>'>
        <input id='textProductModelCheckFlgEqual1'                type="hidden" value='<s:property value="getText('text.label.productModelCheckFlg.equal1')"/>'>
        <input id='textProductModelErrorFlgEqual1'                type="hidden" value='<s:property value="getText('text.label.productModelErrorFlg.equal1')"/>'>
        <input id='textProductProperPrice'                type="hidden" value='<s:property value="getText('text.japan.money')"/>'>
        <input id='textMisMatch'                type="hidden" value='<s:property value="getText('misMatch')"/>'>
        <input id='textNoMisMatch'                type="hidden" value='<s:property value="getText('noMisMatch')"/>'>
        
        <input id='pagingDisplay'                type="hidden" value='<s:property value="getText('text.paging.display')"/>'>
        <input id='pagingEmpty'                  type="hidden" value='<s:property value="getText('text.paging.empty')"/>'>
        <input id='pagingItemsPerPage'           type="hidden" value='<s:property value="getText('text.paging.itemsPerPage')"/>'>
        <input id='pagingFirst'                  type="hidden" value='<s:property value="getText('text.paging.first')"/>'>
        <input id='pagingPrevious'               type="hidden" value='<s:property value="getText('text.paging.previous')"/>'>
        <input id='pagingNext'                   type="hidden" value='<s:property value="getText('text.paging.next')"/>'>
        <input id='pagingLast'                   type="hidden" value='<s:property value="getText('text.paging.last')"/>'>
        
        
        <input id='headerProductId'                     type="hidden" value='<s:property value="getText('text.label.matterProductCode')"/>'>
        <input id='headerProductSyouhinSysCode'         type="hidden" value='<s:property value="getText('text.label.matterModelCode')"/>'>
        <input id='headerBrandName'                     type="hidden" value='<s:property value="getText('text.label.matterBrandName')"/>'>
        <input id='headerProductName'                   type="hidden" value='<s:property value="getText('productName')"/>'>
        <input id='headerProductCode'                   type="hidden" value='<s:property value="getText('productCode')"/>'>
        <input id='headerProductModelCheckFlg'          type="hidden" value='<s:property value="getText('text.label.btnToCheckData')"/>'>
        <input id='headerFitModelMaker'                 type="hidden" value='<s:property value="getText('makerName')"/>'>
        <input id='headerFitModelModel'                 type="hidden" value='<s:property value="getText('bikeModelName')"/>'>
        <input id='headerFitModelMisMatchFlg'           type="hidden" value='<s:property value="getText('masterMisMatch')"/>'>
        <input id='headerProductModelErrorFlg'          type="hidden" value='<s:property value="getText('text.label.matterProductModelError')"/>'>
        <input id='headerDescriptionSummary'            type="hidden" value='<s:property value="getText('text.label.productSumary')"/>'>
        <input id='headerProductSelectDisplay'          type="hidden" value='<s:property value="getText('text.label.productSelectDisplay')"/>'>
        <input id='headerProductProperPrice'            type="hidden" value='<s:property value="getText('text.label.productProperPrice')"/>'>
        <input id='headerCategoryName'                  type="hidden" value='<s:property value="getText('text.label.category.categoryName')"/>'>
        <!-- End Header of grid -->
        <!-- begin label hidden -->
        <input type="hidden" id="hFitModelMaker"        value="<s:property value="getText('text.grid.maker')" />"/>
        <input type="hidden" id="hFitModelModel"        value="<s:property value="getText('text.grid.model')" />"/>
        <input type="hidden" id="hFitModelStyle"        value="<s:property value="getText('text.grid.style')" />"/>
        <input type="hidden" id="hModelMaker"           value="<s:property value="getText('text.grid.maker')" />"/>
        <input type="hidden" id="hModelDisplacement"    value="<s:property value="getText('text.title.modelDisplacement')" />"/>
        <input type="hidden" id="hModelName"            value="<s:property value="getText('text.grid.model')" />"/>
        <!-- End label hidden -->
        <input type="hidden" id="hListFitModelJsonStr"  name="fitModelCheckActionForm.listFitModelJsonStr" value='<s:property value="fitModelCheckActionForm.listFitModelJsonStr" />' />
        <input type="hidden" id="hListModelJsonStr"     name="fitModelCheckActionForm.listModelJsonStr" value='<s:property value="fitModelCheckActionForm.listModelJsonStr" />' />
        <input type="hidden" id="hAddNew"               value="<s:property value="getText('text.label.addNew')"/>"/>
    </div><!-- End hidden fields -->
    <!-- Begin kendogrid template -->
    <div>
    <!-- Start kendo popup, row template -->
          <script id="rowTemplate" type="text/x-kendo-tmpl">
                <tr class="kendoRow kendoRow#: fitModelSort#">  
                    <td class="align-center">
                       <span><input type='checkbox' data-bind='checked: fitModelDelFlg' #= (fitModelDelFlg == 1) ? checked='checked' : '' # class='chkFitModelGroup'/></span>
                    </td>
                    <td class="kendoCell  #= fitModelMakerNoFitFlg == 1 ? (fitModelMaker.length > 50 ? 'red' : 'yellow') : '' # align-center">
                       <span class="kendoRowMaker">#: fitModelMaker#</span>
                    </td>
                    <td class="kendoCell #= (fitModelModelNoFitFlg == 1) ? (fitModelModel.length > 1000 ? 'red' : 'yellow') : '' # align-left">
                      <span class="kendoRowModel">#: fitModelModel #</span>
                    </td>
                    <td class="kendoCell #= (fitModelStyle.length > 255) ? 'red' : '' # align-left">
                      <span class="kendoRowStyle">#: fitModelStyle #</span>
                    </td>
                    <input type="hidden" class="kendoRowSort" value="#: fitModelSort#"/>
                    <input type="hidden" class="kendoRowDelFlg" value="#: fitModelDelFlg#"/>
                    <input type="hidden" class="kendoRowMakerFlg" value="#: fitModelMakerNoFitFlg#"/>
                    <input type="hidden" class="kendoRowModelFlg" value="#: fitModelModelNoFitFlg#"/>
                </tr>
           </script>
          <script id="altRowTemplate" type="text/x-kendo-tmpl">
                <tr class="k-alt kendoRow kendoRow#: fitModelSort#">  
                    <td class="align-center">
                       <span><input type='checkbox' data-bind='checked: fitModelDelFlg' #= (fitModelDelFlg == 1) ? checked='checked' : '' # class='chkFitModelGroup'/></span>
                    </td>
                    <td class="kendoCell  #= fitModelMakerNoFitFlg == 1 ? (fitModelMaker.length > 50 ? 'red' : 'yellow') : '' # align-center">
                       <span class="kendoRowMaker">#: fitModelMaker#</span>
                    </td>
                    <td class="kendoCell #= (fitModelModelNoFitFlg == 1) ? (fitModelModel.length > 1000 ? 'red' : 'yellow') : '' # align-left">
                      <span class="kendoRowModel">#: fitModelModel #</span>
                    </td>
                    <td class="kendoCell #= (fitModelStyle.length > 255) ? 'red' : '' # align-left">
                      <span class="kendoRowStyle">#: fitModelStyle #</span>
                    </td>
                    <input type="hidden" class="kendoRowSort" value="#: fitModelSort#"/>
                    <input type="hidden" class="kendoRowDelFlg" value="#: fitModelDelFlg#"/>
                    <input type="hidden" class="kendoRowMakerFlg" value="#: fitModelMakerNoFitFlg#"/>
                    <input type="hidden" class="kendoRowModelFlg" value="#: fitModelModelNoFitFlg#"/>
                </tr>
          </script>
          <!-- begin submit confirm dialog -->
          <div id="confirmSubmitDialog" class="productPleaseSelectDialog">
            <div class="productPleaseSelectDialogContainer">
              <div class="center" style="margin-bottom: 20px;">
                <s:property value="getText('message.confirm.dataChanged')"/>
              </div>
              <div class="buttonArea">
                  <div class="buttonRight">
                    <input type="button" class="k-button" id="btnSubmit" value="<s:property value="getText('text.btn.yes')"/>" />
                    <input type="button" class="k-button" id="btnCancelSubmit" value="<s:property value="getText('text.btn.cancel')"/>" />
                  </div>
                  <input type="hidden" id="msgConfirmResult" value=""/>
              </div>
              </div>
          </div><!-- end submit confirm dialog -->
          <!-- begin delete confirm dialog -->
          <div id="confirmDeleteDialog" class="productPleaseSelectDialog">
            <div class="productPleaseSelectDialogContainer">
              <div class="center" style="margin-bottom: 20px;">
                <s:property value="getText('confirmDataChanged')"/><br/>
                <s:property value="getText('message.confirm.msgWouldULike')"/>
              </div>
              <div class="buttonArea">
                  <div class="buttonRight">
                    <input type="button" class="k-button" id="btnYes" value="<s:property value="getText('text.btn.yes')"/>" />
                    <input type="button" class="k-button" id="btnNo" value="<s:property value="getText('text.btn.cancel')"/>" />
                  </div>
                  <input type="hidden" id="msgConfirmResult" value=""/>
              </div>
              </div>
          </div><!-- end delete confirm dialog -->
          <!-- begin please confirm dialog -->
          <div id="pleaseSelectDialog" class="productPleaseSelectDialog">
              <div class="productPleaseSelectDialogContainer">
                  <div class="productPleaseSelectDialogContent">
                      <s:property value="getText('text.label.fitModelFixPleaseSelectContent')"/>
                  </div>
                  <input type="button" class="k-button" id="pleaseSelectDialogOk" 
                      value='<s:property value="getText('text.btn.yes')"/>' />
              </div>
          </div>
          <!-- end please confirm dialog -->
          <!-- begin error duplicate value dialog-->
          <div id="duplicateValueDialog" class="productPleaseSelectDialog">
              <div class="productPleaseSelectDialogContainer">
                  <div class="productPleaseSelectDialogContent" style="color:red;">
                      <!-- BOE #9410 Tran.Thanh 2014/06/30 : Change 型式 to 年式・型式 -->
<%--               <s:property value="getText('message.error.duplicateValue', {'メーカー, 車種名, 型式'} )"/> --%>
			              <s:property value="getText('message.error.duplicateValue', {'メーカー, 車種名, 年式・型式'} )"/>
			          <!-- EOE #9410 Tran.Thanh 2014/06/30 : Change 型式 to 年式・型式 -->
                  </div>
                  <input type="button" class="k-button" id="btnDuplicateValueDialog" 
                      value='<s:property value="getText('text.btn.yes')"/>' />
              </div>
          </div>
          <!-- end error duplicate value dialog-->
          <!-- begin confirm dialog -->
          <div id="confirmDialog" class="productPleaseSelectDialog">
              <div class="productPleaseSelectDialogContainer">
                  <div class="productPleaseSelectDialogContent">
                      <s:property value="getText('text.label.confirmSave')"/>
                  </div>
                  <input type="button" class="k-button" id="confirmDialogOk" 
                      value='<s:property value="getText('text.btn.yes')"/>' />
                  <input type="button" class="k-button" id="confirmDialogCancel" 
                      value='<s:property value="getText('text.btn.cancel')"/>' />
              </div>
          </div>
          <!-- end confirm dialog -->
          <!-- begin confirm change grid 4 row -->
          <div id="confirmChangeRowDialog" class="productPleaseSelectDialog">
            <div class="productPleaseSelectDialogContainer">
              <div class="productPleaseSelectDialogContent">
                  <s:property value="getText('text.label.confirmSave')"/>
              </div>
              <input type="button" class="k-button" id="confirmChangeRowDialogOk" 
                  value='<s:property value="getText('text.btn.yes')"/>' />
              <input type="button" class="k-button" id="confirmChangeRowDialogCancel" 
                  value='<s:property value="getText('text.btn.cancel')"/>' />
            </div>
          </div>
          <!-- end confirm change grid 4 row -->
          <!-- begin add Model dialog -->
          <div id="addModelDialog" class="contentInner" style="display:none">
              <div class="modelDetails">
<%--                   <div class="title"><s:property value="getText('text.label.addModelDialogTitle')"/></div> --%>
                  <div class="buttonAreaDialog">
                      <input type="button" id="btnUpdateProductDialog" class="k-button"
                      value="<s:property value="getText('text.label.insert')"/>"/>
                  </div>
                  <ul style="padding-left:4px;">
                      <li style="display: flex;margin-bottom: 2px;">
                          <p><span><s:property value="getText('text.grid.maker')"/></span></p>
                          <s:textfield id="txtMakerDialog" name="txtMakerDialog"
                                       cssClass="k-textbox k-required k-length[%{@net.webike.RcProductFactory.util.Constant@FIT_MODEL_MAKER_LENGTH}]
                                                 nonSpecialChar"
                                       maxLength="%{@net.webike.RcProductFactory.util.Constant@MODEL_MAKER_LENGTH}" 
                                       data-parameters="getText('text.grid.maker')"></s:textfield>
                      </li>
                      <li style="display: flex;margin-bottom: 2px;">
                          <p><span><s:property value="getText('text.grid.model')"/></span></p>
                          <s:textfield id="txtModelDialog" name="txtModelDialog" 
                                       maxLength="%{@net.webike.RcProductFactory.util.Constant@FIT_MODEL_MODEL_WARNING_LENGTH}"
                                       cssClass="k-textbox k-length[%{@net.webike.RcProductFactory.util.Constant@FIT_MODEL_MODEL_LENGTH}]
                                                 nonSpecialChar"
                                       data-parameters="getText('text.grid.model')"></s:textfield>
                      </li>
                      <li style="display: flex;margin-bottom: 2px;">
                          <p><span><s:property value="getText('text.grid.style')"/></span></p>
                          <s:textfield id="txtStyleDialog"
                                       cssClass="k-textbox k-length[%{@net.webike.RcProductFactory.util.Constant@FIT_MODEL_STYLE_WARNING_LENGTH}]
                                                 nonSpecialChar" 
                                       maxLength="%{@net.webike.RcProductFactory.util.Constant@MODEL_STYLE_LENGTH}" ></s:textfield>
                      </li>
                  </ul>
              </div>
              <div class="title" style="margin-top: 10px;height: 18px;"><s:property value="getText('modelSearch')"/></div>
              <div class="kendoWrapDiv">
                  <div id="addModelDialogGrid"></div>
              </div>
          <!-- end add Model dialog -->
          <!-- begin display Model Product dialog -->
	          <div id="displayModelProduct" class="contentInner">
	              <%-- <div class="title"><s:property value="getText('text.header.multiFitModelPopup')"/></div> --%>
	              <div class="modelDetails">
	                  <div class="title">
	                    <span><s:property value="getText('message.header.multiFitModelPopup1')"/></span>
	                    <br>
	                    <span style="color: red;"><s:property value="getText('message.header.multiFitModelPopup2')"/></span>
	                  </div>
	              </div>
	              <div class="kendoWrapDivMulti kendoWrapDiv">
	                  <div id="displayModelProductTable"></div>
	              </div>
	              <div align="center" id="buttonPanel">
	                    <form id="fitModelProductsCheckForm" action="processDataFitModelByMultiProduct.html" method="POST">
	                    <input class="k-button" type="button" id="btnAdaptationModel" value="<s:property value="getText('text.btn.yes')"/>">
	                    <input class="k-button" type="button" id="btnCancelModel" value="<s:property value="getText('text.btn.cancel')"/>">
	                    </form>
	              </div>
	          </div>
	      </div>
          <!-- end display Model Product dialog -->
          <!-- begin can not change product -->
          <div id="cannotChangeProductDialog" class="productPleaseSelectDialog">
              <div class="productPleaseSelectDialogContainer">
                  <div class="productPleaseSelectDialogContent">
                      <s:property value="getText('cannotChangeProduct')"/>
                  </div>
                  <input type="button" class="k-button" id="btnCannotChange" 
                      value='<s:property value="getText('text.btn.yes')"/>' />
              </div>
          </div>
          <!-- end can not change product -->
    </div>
    <!-- End kendoGrid template -->
</div>
<!--End Contents -->
</tiles:putAttribute>
</tiles:insertDefinition>