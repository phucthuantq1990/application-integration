<%@ page language="java" contentType="text/html; charset=Shift-JIS"
    pageEncoding="Shift-JIS"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<tiles:insertDefinition name="layoutCommon">
  <tiles:putAttribute name="css">
    <link rel="stylesheet" type="text/css" href="css/jQuery/kendo.common.min.css?20140804140000" />
    <link rel="stylesheet" type="text/css" href="css/attributeEdit.css?20140804140000" />
  </tiles:putAttribute>
  <tiles:putAttribute name="javaScript">
    <script type="text/javascript" src="js/jQuery/jquery.validate.min.js?20140804140000"></script>
    <script type="text/javascript" src="js/localization/messages_ja.js?20140804140000"></script>
    <script type="text/javascript" src="js/jQuery/jquery-ui.custom.min.js?20140804140000"></script>
    <script src="js/jQuery/jquery.tools.min.js?20140804140000"></script>
    <script src="js/jQuery/kendo.web.min.js?20140804140000"></script>
    <script type="text/javascript" src="js/attributeEdit.js?20140804140000"></script>
  </tiles:putAttribute>
  <tiles:putAttribute name="body">
    <!-- Contents -->
      <div id="contents">
      <div id="content">
      <!-- // BOE #6344 No.64 Thai.Son 2014/02/19 Change confirm pupup to kendo window -->
         <div id="confirmBackDialog">
<%--                 <div id='heading-popup'><s:property value="getText('message.confirm.titleBackToPrevious')"/></div> --%>
                <div>
                    <p><s:property value="getText('message.confirm.backToPrevious')"/></p>
                    <div style='text-align:center;padding:5px;'>
                        <input id='fancyConfirm_ok' class='k-button' type='button'
                              value='<s:property value="getText('message.confirm.yes')"/>' onclick='backToPreviousPage()' style='margin:7px;padding:5px;width:67px;'>
                        <input id='fancyConfirm_cancel' class='k-button' type='button'
                              value='<s:property value="getText('message.confirm.no')"/>' style='margin:7px;padding:5px;width:67px;'>
                   </div>
                </div>
         </div>
        <!-- // EOE #6344 No.64 Thai.Son 2014/02/19 Change to kendo window -->
         <jsp:include page="component/message.jsp"></jsp:include>       
        <!-- Hidden field -->
        <div>
            <!-- End of message confirm -->
        </div>
        <!-- end of hidden field -->
          <!-- contents_inner start -->
          <div id="contents_inner">
            <!--left tab -->
            <div id="leftTab">
              <div>
                <h1><s:property value="getText('text.label.attrEdit.header')"/></h1>
                <ul class="tabs">
                  <li id="tabs-info" class="tabStrip"><s:property value="getText('text.label.attrEdit.attrTitle')"/></li>
                    <!-- BOE ho.hoang 2014/03/10 Fix bug 44 -->
                  <s:if test="%{attributeActionForm.attribute.attributeType == 'Group' && attributeActionForm.insertMode == 0}">
                    <li id="tabs-attr" class="tabStrip"><s:property value="getText('text.label.attrEdit.attrGroupTitle')"/></li>
                  </s:if>
                  <s:if test="%{attributeActionForm.insertMode != 0}">
                    <li id="tabs-attr" class="tabStrip" style="display:none"><s:property value="getText('text.label.attrEdit.attrGroupTitle')"/></li>
                  </s:if>
                  <!-- EOE ho.hoang 2014/03/10 Fix bug 44 -->
                </ul>
              </div>
            </div>
              <!--right tab -->
              <div id="rightTab">
                <div>
                  <ul>
                    <li>
                      <s:if test="%{attributeActionForm.insertMode == 0}">
                      <!-- // BOE #6344 No.41 Thai.Son 2014/02/19 attributeName too long -->
                        <label class="lblMainTitleTabs" id="attributeNameLabel">
                          <s:property value="getText('text.label.attrEdit.attrTitleUpdate', {attributeActionForm.attribute.attributeName})"/>
                        </label>
                        <input id='myAttributeNameEdit'       type="hidden"   value='<s:property value="%{attributeActionForm.attribute.attributeName}"/>'>
                        <!-- // EOE #6344 No.41 Thai.Son 2014/02/19 attributeName too long -->
                      </s:if>
                      <s:else>
                        <label class="lblMainTitleTabs">
                          <s:property value="getText('text.label.attrEdit.attrTitleInsert')"/>
                        </label>
                      </s:else>
                        <input type="button" id="btnSave" class="btn-color-default lstButton k-button" value="<s:property value="getText('text.label.btnSave')"/>" />
                        <input type="button" id="btnReload" class="btn-color-default lstButton k-button" value="<s:property value="getText('text.label.btnReset')"/>" />
                        <input type="button" id="btnBack" class="btn-color-default lstButton k-button" value="<s:property value="getText('text.label.btnBack')"/>" />
                    </li>
                  </ul>
                </div>
                <s:form name="attributeActionForm" id="attributeActionForm" action="insertOrUpdateAttribute" method="POST" >
                  <div class="panes">
                    <!--First content of tab -->
                    <div class="divTabs">
                        <div class="tabTitle">
                            <p><strong><s:property value="getText('text.label.attrEdit.attrTitle')"/></strong></p>
                        </div>
                        <ul>
                          <s:if test="%{attributeActionForm.insertMode == 0}">
	                          <li>
	                            <p>
	                              <span><s:property value="getText('text.label.attrEdit.attrCode')"/></span>
	                            </p>
	                              <span id="attrCode">
	                                   <s:property value="attributeActionForm.attribute.attributeCode"/>
	                                   <input type="hidden" name="attributeActionForm.attribute.attributeCode" value="<s:property value="attributeActionForm.attribute.attributeCode"/>"/>
	                              </span>
	                            
                                   <div class="cssChkDel">
                                    <s:if test="%{attributeActionForm.attribute.attributeDelFlg == 1}">
                                        <input type="checkbox" id="delFlg" name="attributeActionForm.attribute.attributeDelFlg" class="chkFlg" value="1" checked> 
                                    </s:if>
                                    <s:else>
                                        <input type="checkbox" id="delFlg"  name="attributeActionForm.attribute.attributeDelFlg" class="chkFlg" value="0" > 
                                    </s:else>
                                  <s:property value="getText('text.label.delFlg')"/>
                                </div>
	                          </li>
                           </s:if>
                          
                          <li>
                            <p>
                              <span id="labelAttrName"><s:property value="getText('text.label.attrEdit.attrName')"/></span>
                              <span class="error">*</span>
                            </p>
                            <%-- BOE #6634 No.40 Hoang.Ho 2014/02/18 bound length --%>
<%--                              <s:textfield id="attrName" size="40" name="attributeActionForm.attribute.attributeName" maxLength="255" /> --%>
                                                                <s:textfield id="attrName" size="40" 
                                    name="attributeActionForm.attribute.attributeName" maxLength="266"
                                    cssClass="k-required k-ajaxCheckExistAttr k-textbox k-w30" 
                                    data-parameters="getText('text.label.attrEdit.attrName')"/>
                            <%-- EOE #6634 No.40 Hoang.Ho 2014/02/18 bound length --%>
                            <%-- BOE #6634 No.46 Hoang.Ho 2014/02/19 check exist attribute name using ajax --%>
                               <span class="k-widget k-tooltip k-tooltip-validation k-invalid-msg" data-for="attributeActionForm.attribute.attributeName" 
                                     role="alert" style="display: none;"><span class="k-icon k-warning"> </span></span>
                                <input type="hidden" id="validated-attr-name" value="0">
                              <span id="spnAttrNameError" class="error"></span>
                              <div id='loading-attr-name'  style="display:none"><img alt="loading" src="images/loading.gif"></div>
                             <%-- EOE #6634 No.46 Hoang.Ho 2014/02/19 check exist attribute name using ajax --%>
                             <s:if test="%{attributeActionForm.insertMode != 0}">
                                 <div class="cssChkDel">
	                                  <s:if test="%{attributeActionForm.attribute.attributeDelFlg == 1}">
	                                        <input type="checkbox" id="delFlg"  name="attributeActionForm.attribute.attributeDelFlg" class="chkFlg" value="1"  checked> 
	                                    </s:if>
	                                    <s:else>
	                                        <input type="checkbox" id="delFlg" name="attributeActionForm.attribute.attributeDelFlg" class="chkFlg" value="0" > 
	                                    </s:else>
	                                  <s:property value="getText('text.label.delFlg')"/>
                                </div> 
                            </s:if>    
                          </li>
                          <li>
                            <p>
                              <span id="lblAttrType"><s:property value="getText('text.label.attrEdit.attrType')"/></span>
                              <span class="error">*</span>
                            </p>
                            <s:if test="%{attributeActionForm.insertMode == 0}">
                                <s:property value="attributeActionForm.attribute.attributeTypeJapan"/>
                                <input id="attributeType" type="hidden" name="attributeActionForm.attribute.attributeType" value='<s:property value="attributeActionForm.attribute.attributeType"/>' />
                            </s:if>
                            <s:else>
                                <s:select id="attributeTypeList" list="attributeActionForm.listType" emptyOption="true" 
                                name="attributeActionForm.attribute.attributeType"
                                    cssClass="k-select-required"
                                    data-parameters="getText('text.label.attrEdit.attrType')">
                                </s:select>
                                <span class="k-widget k-tooltip k-tooltip-validation k-invalid-msg selectInline" 
                                    data-for="attributeActionForm.attribute.attributeType" style="display:none" role="alert">
                                <span class="k-icon k-warning"> </span></span>
                                <span id="spnAttrType" class="error"></span>
                            </s:else>
                          </li>
                          <li>
                            <p>
                              <span id="lblLengthLimit"><s:property value="getText('text.label.attrEdit.attrLengthLimit')"/></span>
                              <span class="error">*</span>
                            </p>
                              <s:textfield id="attrLengthLimit" cssClass="clsDisable k-textbox k-required k-numberic" 
                             data-parameters="getText('text.label.attrEdit.attrLengthLimit')"
                             maxlength="8" size="14" name="attributeActionForm.attribute.attributeLengthLimit" />
                             <span id="spnAttrLengthError" class="error"></span>
                          </li>
                          <li>
                            <p>
                              <span><s:property value="getText('text.label.attrEdit.attrRegexp')"/></span>
                            </p>
                            <%-- BOE #6634 No.40 Hoang.Ho 2014/02/18 bound length --%>
                             <%--<s:textfield cssClass="clsDisable" id="attrRegexp"   maxlength="255" size="14" name="attributeActionForm.attribute.attributeRegexp" /> --%>
                             <s:textfield cssClass="clsDisable k-textbox" id="attrRegexp"   maxlength="266" size="14" name="attributeActionForm.attribute.attributeRegexp" />
                            <%-- EOE #6634 No.40 Hoang.Ho 2014/02/18 bound length --%>
                          </li>
                          <li>
                            <p>
                              <span><s:property value="getText('text.label.attrEdit.attrShowFlag')"/></span>
                            </p>
                           <s:if test="%{attributeActionForm.attribute.attributeShowFlg == 1}">
                            <input type="checkbox" id="attrShowFlg" class="chkFlg" value="1" name="attributeActionForm.attribute.attributeShowFlg" checked />
                           </s:if>
                           <s:else>
                            <input type="checkbox" id="attrShowFlg" class="chkFlg" value="0" name="attributeActionForm.attribute.attributeShowFlg"/>
                           </s:else>
                              <span><s:property value="getText('text.label.titleDelFlg')"/></span>
                          </li>
                          
                          <s:if test="%{attributeActionForm.insertMode == 0}">
                          <li>
                            <p>
                              <span><s:property value="getText('text.label.attrEdit.attrCreateOn')"/></span>
                            </p>
                             <s:property value="attributeActionForm.attribute.createOn"/>  
                          </li>
                          <li>
                            <p>
                              <span><s:property value="getText('text.label.attrEdit.attrUpdateOn')"/></span>
                            </p>
                             <s:property value="attributeActionForm.attribute.updatedOn"/>  
                          </li>
                          </s:if>
                        </ul>
                    </div>
                    <!--Second content of tab -->
	                <div class="divTabs">
                        <div class="tabTitle">
                              <p><strong><s:property value="getText('text.label.attrEdit.attrGroupTitle')"/></strong></p>
                        </div>
	                     <ul>

	                    </ul>
	                    <div id="attrGroupContains">
		                    <div class="clsBtnArea">
			                   <div id="group-message" class="error"></div>
		                       <input type="button" class="lstButton k-button" id="btnInsert" value="<s:property value="getText('text.label.insert')"/>" />
		                       <input type="button" class="lstButton k-button" id="btnDelete" value="<s:property value="getText('text.label.btnDelete')"/>" />
			               </div>
			               <div id="table-content">
			                    <div id="kendoGrid"></div>
			               </div>
	                    </div>
                    </div>
                    <!-- end Second content of tab-->
                  </div>
                  <s:hidden id='hAttrGroupList' name="attributeActionForm.jsonLstAttributeGroup"/>
                  <s:hidden name="attributeActionForm.insertMode" id="insertMode"/>
                  </s:form>
              </div>
            </div>
          </div>
          <!-- contents_inner end-->
          <!-- hidden fields -->
          <input id='hAttributeName' type="hidden" value='<s:property value='attributeActionForm.attribute.attributeName'/>'>
          <input id='hAttributeLengthLimit' type="hidden" value='<s:property value='attributeActionForm.attribute.attributeLengthLimit'/>'>
          <input id='hAttributeRegexp' type="hidden" value='<s:property value='attributeActionForm.attribute.attributeRegexp'/>'>
          <input id='hAttributeShowFlg' type="hidden" value='<s:property value='attributeActionForm.attribute.attributeShowFlg'/>'>
          <input id='hAttributeDelFlg' type="hidden" value='<s:property value='attributeActionForm.attribute.attributeDelFlg'/>'>
          <input id='hAttributeType' type="hidden" value='<s:property value='attributeActionForm.attribute.attributeType'/>'>
          <input id='hAttrGroupName' type="hidden" value='<s:property value="getText('text.label.attrEdit.attrGroupName')"/>'>
          <input id='hAttrSort' type="hidden" value='<s:property value="getText('text.label.attrEdit.attrSort')"/>'>
          <input id='hPopupTitle' type="hidden" value='<s:property value="getText('text.label.attrEdit.popupTitle')"/>'>
          <!-- boe thai.son -->
          <input id='hPopupTitleDeleteAttributeGroup' type="hidden" value='<s:property value="getText('deleteAttributeGroup')"/>'>
          <!-- eoe thai son -->
          <input id='hMsgUnchange' type="hidden"   value='<s:property value="getText('message.error.unChange')"/>'>
          <!-- Error message -->
          <input id='requiredFill' type="hidden"   value='<s:property value="getText('message.error.requiredFill')"/>'>
          <input id='max-length' type="hidden"   value='<s:property value="getText('message.error.inputOutOfRange')"/>'>
          <input id='duplicate-value' type="hidden"   value='<s:property value="getText('message.error.duplicateGroupValue')"/>'>
          <input id="duplicateTextWithParam" type="hidden" value="<s:property value='getText("message.error.duplicateValue")' />" />
          <input id='no-selected' type="hidden"   value='<s:property value="getText('message.error.noSelected')"/>'>
          <input id='name-exists' type="hidden"   value='<s:property value="getText('message.error.existsInDB')"/>'>
          <input id="onlyNumberMsg" type="hidden" value='<s:property value="getText('message.error.onlyInputXNumber')"/>'>
          <!-- End error message -->
          <!-- Message confirm -->
          <input id='title-confirm-back' type="hidden"   value='<s:property value="getText('message.confirm.titleBackToPrevious')"/>'>
          <input id='content-confirm-back' type="hidden"   value='<s:property value="getText('message.confirm.backToPrevious')"/>'>
          <input id='btn-yes' type="hidden"   value='<s:property value="getText('message.confirm.yes')"/>'>
          <input id='btn-no' type="hidden"   value='<s:property value="getText('message.confirm.no')"/>'>
          <!-- end hidden fields -->
        </div>
        <!--End Contents -->
        <!-- Start kendo popup, row template -->
          <script id="rowTemplate" type="text/x-kendo-tmpl">
 				<tr class="kendoRow kendoRow#: attributeGroupCode#">  
					<td class="align-center">
					   <span><input type='checkbox' data-bind='checked: delFlg' #= (delFlg == 1) ? checked='checked' : '' # class='chkAttrGroup'/></span>
					</td>
					<td class="kendoCell align-left">
					   <span class="kendoRowName">#: attributeGroupName#</span>
					</td>
					<td class="kendoCell align-right">
					  <span class="kendoRowSort">#: attributeSort #</span>
					</td>
					<input type="hidden" class="kendoRowGroupCode" value="#: attributeGroupCode#"/>
					<input type="hidden" class="kendoRowCode" value="#: attributeCode#"/>
					<input type="hidden" class="kendoRowDelFlg" value="#: delFlg#"/>
				</tr>
  		   </script>
          <script id="altRowTemplate" type="text/x-kendo-tmpl">
				 <tr class="k-alt kendoRow kendoRow#: attributeGroupCode#">
					<td class="align-center">
					   <span><input type='checkbox' data-bind='checked: delFlg' #= (delFlg == 1) ? checked='checked' : '' # class='chkAttrGroup'/></span>
					</td>
					<td class="kendoCell align-left">
					   <span class="kendoRowName">#: attributeGroupName#</span>
					</td>
					<td class="kendoCell align-right">
					  <span class="kendoRowSort">#: attributeSort #</span>
					</td>
					<input type="hidden" class="kendoRowGroupCode" value="#: attributeGroupCode#"/>
					<input type="hidden" class="kendoRowCode" value="#: attributeCode#"/>
					<input type="hidden" class="kendoRowDelFlg" value="#: delFlg#"/>
				</tr>
          </script>
          <!-- begin insert or update dialog -->
            <div id="dialog">
			    <div class="divError">
			          <span class="error" id="attributGroupNameRequired"></span><br/>
			          <span class="error" id="attributGroupSortRequired"></span>
			     </div>
		          <ul>
		            <li>   
		               <p>
		                    <span><s:property value="getText('text.label.attrEdit.attrGroupName')"/></span>
		                    <span class="error">*</span>
		                 </p>
		                    <input id="popAttributeGroupName" name="popAttributeGroupName" maxlength="255"
		                    class="k-textbox k-required k-duplicateAttrGroup" data-parameters="<s:property value="getText('text.label.attrEdit.attrGroupName')"/>" />
		            </li>
		            <li>
		               <p>
		                      <span><s:property value="getText('text.label.attrEdit.attrSort')"/></span>
		                      <span class="error">*</span>
		               </p>
		                     <input id="popAttributeSort" name="popAttributeSort" maxlength="2"
		                     class="k-textbox k-required k-numberic k-duplicateAttrGroupSort"
		                      data-parameters="<s:property value="getText('text.label.attrEdit.attrSort')"/>" />
		              </li>
		          </ul>
		          <div class="buttonArea">
		              <div class="buttonRight">
		              <input type="button" class="lstButton k-button" id="btnOk" value="<s:property value="getText('text.btn.yes')"/>" />
		              <input type="button" class="lstButton k-button" id="btnCancel" value="<s:property value="getText('text.btn.cancel')"/>" />
		              </div>
		          </div>
		          <input type="hidden" id="popAttributeGroupCode" value="" />
          </div>
          <!-- end insert or update dialog -->
          <!-- begin delete confirm dialog -->
          <div id="confirmDeleteDialog">
              <div class="center">
                <s:property value="getText('message.confirm.attrGroupDelete')"/><br/>
                <s:property value="getText('message.confirm.msgWouldULike')"/>
              </div>
              <div class="buttonArea">
                  <div class="buttonRight">
                    <input type="button" class="lstButton k-button" id="btnYes" value="<s:property value="getText('text.btn.yes')"/>" />
                    <input type="button" class="lstButton k-button" id="btnNo" value="<s:property value="getText('text.btn.cancel')"/>" />
                  </div>
                  <input type="hidden" id="msgConfirmResult" value=""/>
              </div>
          </div>
          <!-- end delete confirm dialog -->
        <!-- End kendo popup, row template -->
        <!-- Start Kendo Dialog -->
        <!-- boe thai.son no.64 -->
         <%--  <div id="confirmBackDialog">
             <div>
                 <p><s:property value="getText('message.confirm.backToPrevious')"/></p>
                 <div style="text-align:center;padding:5px;height: 55px;">
                    <input id='fancyConfirm_cancel' class='k-button' type='button'
                           value='<s:property value="getText('message.confirm.no')"/>' style='margin:7px;padding:5px;width:67px;margin-right: 68px;height: 31px;'>
                     <input id='fancyConfirm_ok' class='k-button' type='button'
                           value='<s:property value="getText('message.confirm.yes')"/>' onclick='backToPreviousPage()' style='margin:7px;padding:5px;width:67px;margin-right: 10px;height: 31px;'>
                </div>
             </div>
          </div>
          
          <input type="hidden" id="hfAttrName" value='<s:property value="attributeActionForm.attribute.attributeName"/>' >
          <input type="hidden" id="hfAttrLengthLimit" value='<s:property value="attributeActionForm.attribute.attributeLengthLimit"/>' >
          <input type="hidden" id="hfAttrRegexp" value='<s:property value="attributeActionForm.attribute.attributeRegexp"/>' > --%>
          <!-- eoe thai.son no.64-->
  </tiles:putAttribute>
</tiles:insertDefinition>