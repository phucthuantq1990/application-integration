<%@ page language="java" contentType="text/html; charset=Shift-JIS"
	pageEncoding="Shift-JIS"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<tiles:insertDefinition name="layoutCommon">
	<tiles:putAttribute name="css">
		<link rel="stylesheet" type="text/css" href="css/editBrand.css?20140804140000" />
	</tiles:putAttribute>
	<tiles:putAttribute name="javaScript">
		<script type="text/javascript" src="js/jQuery/jquery.validate.min.js?20140804140000"></script>
		<script type="text/javascript" src="js/localization/messages_ja.js?20140804140000"></script>
		<script type="text/javascript" src="js/editBrandJS.js?20140804140000"></script>
		<!-- Js for upload file -->
		<script src="js/upload/jquery.upload.js?20140804140000"></script>
		<script src="js/upload/main.js?20140804140000"></script>
		<!-- Js for upload file -->	
	</tiles:putAttribute>
	<tiles:putAttribute name="body">
		<!-- Contents -->
		<div id="contents">
			<div id="content">
				<!-- BOE add show error Le.Dinh 2014/01/16 -->
				 <s:if test="editBrandActionForm.failMessage != ''">
				 	<div id="message" class="has-message msg-error">
                    	 <s:property value="editBrandActionForm.failMessage" escapeHtml="false"/>
                 	</div>
				 </s:if>
				 <!-- BOE add show error Thai.Son 2014/01/17 -->
                 <s:else>
                    <div id="message">
                    </div>
                 </s:else>
                 <input type="hidden" id="flagCheckImageChange" value="" />
                 <!-- EOE add show error Thai.Son 2014/01/17 -->	
                 <!-- EOE add show error Le.Dinh 2014/01/16 -->
				<!-- contents_inner start -->
				<div id="contents_inner">
					<!--left tab -->
					<div id="leftTab">
						<div>
							<h1><s:property value="getText('text.label.brandInfo')"/></h1>
							<ul class="tabs">
								<li class="tabStrip tabStrip-selected"><s:property value="getText('text.label.brandBasicInfo')"/></li>
							</ul>
						</div>
					</div>
					<!--right tab -->
					<div id="rightTab">
						<div>
							<ul>
								<li>
								    <s:if test="%{editBrandActionForm.mode == 0}">
								        <label class="lblMainTitleTabs">
								            <s:property value="getText('text.label.newBrandInsert')"/>
								        </label>
								    </s:if>
								    <s:else>
								         <label class="lblMainTitleTabs brandNameLabel">
                                            <s:property value="getText('text.label.brandEdit')"/>
                                            "<s:property value="%{editBrandActionForm.entBrand.name}"/>"
                                        </label>
                                        <input id='myBrandName'       type="hidden"   value='<s:property value="%{editBrandActionForm.entBrand.name}"/>'>
								    </s:else>
									 
									<input type="hidden" name="mode" id="mode"
										value="<s:property value="%{editBrandActionForm.mode}"/>" />
									<input type="button" class="btn-color-default lstButton k-button btnTop"
									<%-- BOE #6634 No.15 Hoang.Ho 2014/02/18 Change event onclick to open dialog --%>
									<%-- onclick="saveBrand();" --%>
									id = "btnSave"
                                    <%-- EOE #6634 No.15 Hoang.Ho 2014/02/18 Change event onclick to open dialog  --%>
										value="<s:property value="getText('text.btn.save')"/>" tabindex="14"/> 
									<input type="button" class="btn-color-default lstButton k-button btnTop"
										onclick="reloadPage();"
										value="<s:property value="getText('text.btn.reset')"/>" tabindex="15"/>
									 <!-- BOE: show popup confirm. Thai.Son 2014/01/17 #B07 --> 
									<input id="btnBack" type="button" class="btn-color-default lstButton k-button btnTop"
										onclick="clickBack();"
										value="<s:property value="getText('text.btn.back')"/>" tabindex="16"/>
									<!-- Message confirm -->
						            <input id='title-confirm-back'       type="hidden"   value='<s:property value="getText('message.confirm.titleBackToPrevious')"/>'>
						            <input id='content-confirm-back'     type="hidden"   value='<s:property value="getText('message.confirm.backToPrevious')"/>'>
						            <input id='btn-yes'                  type="hidden"   value='<s:property value="getText('message.confirm.yes')"/>'>
						            <input id='btn-no'                  type="hidden"   value='<s:property value="getText('message.confirm.no')"/>'>
						            <%-- Add text warning length input Bug No.6 - 20140313_not_fix @rcv!cao.lan 2014/03/14 --%>
						            <input id='warningLength'            type="hidden"   value='<s:property value="getText('warningLength')"/>'>
						            <!-- End of message confirm -->
						            <!-- Error message -->
                                    <input id='unchange'                type="hidden"   value='<s:property value="getText('message.error.unChange')"/>'>
                                    <input id="existsInDB" type="hidden" value='<s:property value="getText('message.error.existsInDB')"/>'>
                                    <!-- End error message -->
						            <!-- EOE show popup confirm. Thai.Son 2014/01/17 -->
								</li>
							</ul>
						</div>
						<s:form name="editBrandActionForm" id="editBrandActionForm" action="updateOrNewBrand" method="post">
							<div class="panes">
								<div>
										<div class="tabTitle">
										   <p>
	                                          <strong><s:property value="getText('text.label.basicInfomation')"/></strong>
	                                       </p>
	                                    </div>
										<ul>
	                                        <%-- BOE #6634 No.13 Hoang.Ho 2014/02/19 Upload image when kendo-validate failed--%>
	                                        <li class="kendoValidate">
	                                        <%-- EOE #6634 No.13 Hoang.Ho 2014/02/19 Upload image when kendo-validate failed --%>
												<p>
													<span id="lblBrandName"><s:property value="getText('text.label.brandName')"/></span>
													<span style="color: red;">*</span>
												</p>
												<div>
	                                                <%-- BOE #6634 No.12 Hoang.Ho 2014/02/18 bound length --%>
	                                                 <%-- edit maxLength="80" to "60"--%>
													<s:textfield
													    id = "brandName"
														name="editBrandActionForm.entBrand.name"
	                                                    maxLength="55"
														data-parameters="getText('text.label.brandName')"
														cssClass="txt-size-medium brandName k-required k-ajaxCheckExistBrand k-textbox k-w30"
														autocomplete="off" 
														tabindex="1"/>
	                                                <%-- EOE #6634 No.12 Hoang.Ho 2014/02/18 bound length --%>
	                                                <%-- BOE #6634 No.18 Hoang.Ho 2014/02/18 check exist brand name using ajax --%>
						                              <span class="k-widget k-tooltip k-tooltip-validation k-invalid-msg" data-for="editBrandActionForm.entBrand.name" 
						                                        role="alert" style="display: none;"><span class="k-icon k-warning"> </span></span>
						                              <%-- Add span warning length input Bug No.6 - 20140313_not_fix @rcv!cao.lan 2014/03/14 --%>
	<%-- 					                              <span class="k-widget k-tooltip k-tooltip-validation k-invalid-msg" id="warningBrandName" style="display: none;"></span> --%>
						                              <input type="hidden" id="brandInvalidFlag" value="0">
						                              <img id="loading-ajax-inline" alt="loading" src="images/loading.gif">
	                                                <%-- EOE #6634 No.18 Hoang.Ho 2014/02/18 check exist brand name using ajax --%>
													<s:if test="%{editBrandActionForm.entBrand.delFlg == 1}">
														<div class="cssChkDel">
															<input type="checkbox"
																name="editBrandActionForm.entBrand.delFlg"
																id="delFlag"
																value="1"
																checked
																tabindex="13">
															<s:property value="getText('text.label.defFlg')"/>
															<input type="hidden" name="delflg" id="delflg"
															value="<s:property value="%{editBrandActionForm.entBrand.delFlg}"/>" />
														</div>
													</s:if>
													<s:else>
														<div class="cssChkDel">
															<input type="checkbox"
																name="editBrandActionForm.entBrand.delFlg"
																id="delFlag"
																value="0"
																tabindex="13">
															<s:property value="getText('text.label.defFlg')"/>
															<input type="hidden" name="delflg" id="delflg"
															value="0" />
														</div>
													</s:else>
												</div>
											</li>
											<li>
												<p><s:property value="getText('text.label.hyoujiName1')"/></p>
												<div>
													<s:textfield id="hyoujiName1"
														name="editBrandActionForm.entBrand.hyoujiName1"
														data-parameters="getText('text.label.hyoujiName1')"
														maxLength="55" cssClass="txt-size-medium hyoujiName1 k-textbox k-w30"
														autocomplete="off" tabindex="2"/>
													<span for="hyoujiName1" generated="true" class="error"></span>
													<%-- Add span warning length input Bug No.6 - 20140313_not_fix @rcv!cao.lan 2014/03/14 --%>
													<span class="k-widget k-tooltip k-tooltip-validation k-invalid-msg" id="warningHyoujiName1" style="display: none;"></span>
												</div>
											</li>
											<li>
												<p><s:property value="getText('text.label.hyoujiName2')"/></p>
												<div>
													<s:textfield id="hyoujiName2"
														name="editBrandActionForm.entBrand.hyoujiName2"
	                                                    data-parameters="getText('text.label.hyoujiName2')"
														maxLength="55" cssClass="txt-size-medium hyoujiName2 k-textbox k-w30"
														autocomplete="off" tabindex="3"/>
													<span for="hyoujiName2" generated="true" class="error"></span>
													<%-- Add span warning length input Bug No.6 - 20140313_not_fix @rcv!cao.lan 2014/03/14 --%>
													<span class="k-widget k-tooltip k-tooltip-validation k-invalid-msg" id="warningHyoujiName2" style="display: none;"></span>
												</div>
											</li>
											<li>
												<p><s:property value="getText('text.label.hyoujiName3')"/></p>
												<div>
													<s:textfield id="hyoujiName3"
														name="editBrandActionForm.entBrand.hyoujiName3"
														maxLength="55"
	                                                    data-parameters="getText('text.label.hyoujiName3')"
														cssClass="txt-size-medium hyoujiName3 k-textbox k-w30"
														autocomplete="off" 
														tabindex="4"/>
													<span for="hyoujiName3" generated="true" class="error"></span>
													<%-- Add span warning length input Bug No.6 - 20140313_not_fix @rcv!cao.lan 2014/03/14 --%>
													<span class="k-widget k-tooltip k-tooltip-validation k-invalid-msg" id="warningHyoujiName3" style="display: none;"></span>
												</div>
											</li>
											<%-- BOE #6634 No.13 Hoang.Ho 2014/02/19 Upload image when kendo-validate failed--%>
	                                        <li class="kendoValidate">
											<%-- EOE #6634 No.13 Hoang.Ho 2014/02/19 Upload image when kendo-validate failed --%>
												<p><s:property value="getText('text.label.sortKana')"/></p>
												<div>
													<s:textfield id="sortKanaName"
														name="editBrandActionForm.entBrand.sortKanaName"
	                                                    data-parameters="getText('text.label.sortKana')"
														maxLength="55" cssClass="txt-size-medium sortKana k-katakana k-textbox k-w30"
														autocomplete="off" tabindex="5" />
													<span for="sortKanaName" generated="true" class="error"></span>
													<%-- Add span warning length input Bug No.6 - 20140313_not_fix @rcv!cao.lan 2014/03/14 --%>
													<span class="k-widget k-tooltip k-tooltip-validation k-invalid-msg" id="warningSortKanaName" style="display: none;"></span>
												</div>
											</li>
											<li>
												<p>
													<s:property value="getText('text.label.tantouId1')"/>
													<span style="color: red;">*</span>
												</p>
												<div>
													<s:if test="null != commonForm.listCommonUser">
													<!-- // BOE #6344 No.11 Thai.Son 2014/02/18  -->
														<s:select 
														    id ="listCommonUser"
														    disabled=""
															list="commonForm.listCommonUser"
															listKey="userId"
															listValue="%{userLastName + ' ' + userFirstName}"
															value="%{editBrandActionForm.entBrand.tantouUserId1}"
															name="editBrandActionForm.entBrand.tantouUserId1"
															cssClass="cbb-size-normal tantouId1 k-w50" tabindex="6" />
													<!-- // BOE #6344 No.11 Thai.Son 2014/02/18  -->
													</s:if>
												</div>
											</li>
											<li>
												<p><s:property value="getText('text.label.URL')"/></p>
												<div>
													<s:textfield id="url"
														name="editBrandActionForm.entBrand.url"
														maxLength="265"
	                                                    data-parameters="getText('text.label.URL')"
														cssClass="txt-size-large urlFormat k-textbox k-w50"
														autocomplete="off" tabindex="7" />
													<span for="url" generated="true" class="error"></span>
												</div>
											</li>
											<li>
												<p><s:property value="getText('text.label.RSS')"/></p>
												<div>
													<s:textfield id="rss"
														name="editBrandActionForm.entBrand.rss"
														maxLength="265"
														cssClass="txt-size-large urlFormat k-textbox k-w50"
														autocomplete="off" tabindex="8" />
													<span for="rss" generated="true" class="error"></span>
												</div>
											</li>
											<li>
												<p>
													<s:property value="getText('text.label.marume')"/>
													<span style="color: red;">*</span>
												</p>
												<div>
													<s:if test="null != editBrandActionForm.listMarume">
														<s:select disabled=""
														       id = "listMarume"
															list="editBrandActionForm.listMarume"
															listKey="marumeCode"
															listValue="%{marumeName}"
															value="%{editBrandActionForm.entBrand.brandMarumeCode}"
															name="editBrandActionForm.entBrand.brandMarumeCode"
															cssClass="cbb-size-normal listMarume k-w50" tabindex="9" />
													</s:if>
												</div>
											</li>
											
											<li class="liTextArea">
												<p><s:property value="getText('text.label.sentence')"/></p>
												<div>
													<s:textarea id="sentence"
														name="editBrandActionForm.entBrand.sentence"
														autocomplete="off"
	                                                    data-parameters="getText('text.label.sentence')"
														cssClass="txa-size-medium sentence k-textbox k-w50" tabindex="10">
													</s:textarea>
													<span for="sentence" generated="true" class="error"></span>
												</div>
											</li>
											<li class="liImgUpload">
	                                          <p><s:property value="getText('text.label.image')"/></p>
	                                          <input type="hidden" id="brandLogo" value="<s:property value="editBrandActionForm.entBrand.logo"/>" />
	                                          <div>
	                                                   <s:if test="editBrandActionForm.entBrand.logo != ''">
	                                                       <img id="imageUpload" class="imageUpload k-image" src="brand_image/brand_<s:property value="editBrandActionForm.entBrand.brandCode"/>.gif" onerror="this.onerror=null;this.src='images/noimage_100.gif';" />
	                                                      <input
	                                                            type="button"
	                                                            id="btnDeleteImg"
	                                                            class="btn-color-default k-button"
	                                                            value="<s:property value="getText('text.btn.delete')"/>"
	                                                            onclick="deleteImagePreview();"
	                                                            tabindex="11"
	                                                            />
	                                                   </s:if>
	                                                   <s:else>
	                                                      <img id="imageUpload" class="imageUpload" src="images/noimage_100.gif" />
	                                                      <input
	                                                            type="button"
	                                                            id="btnDeleteImg"
	                                                            class="btn-color-default k-button "
	                                                            value="<s:property value="getText('text.btn.delete')"/>"
	                                                            onclick="deleteImagePreview();"
	                                                            tabindex="11" disabled
	                                                            />
	                                                   </s:else>
											      	<%-- BOE Change design kendo ui for upload file @rcv!cao.lan 2014/02/20 #6634 --%>
											      	<div class="divImgUpload">
	                                                    <div id="imageName">
	                                                        <s:textfield maxlength="265"
	                                                            id="logo"
	                                                            cssClass="k-textbox" autocomplete="off" />
	                                                    </div>  
	                                                    <div class="upload-button btn-color-default k-button">
	                                                    	<span>Browse</span>  
	                                                        <input type="file" id="fileupload"  name="userImage" tabindex="12"/>
	                                                    </div>
	                                                    <span id="errorImage0" class="error" ></span><br/>
	                                                    <span id="errorImage1" class="error" ></span>
	                                          		</div>
	                                          		<%-- EOE @rcv!cao.lan 2014/02/20 #6634 --%>
	                                          </div>
											</li>
										</ul>
										 <s:hidden id="logoHidden" name="editBrandActionForm.entBrand.logo" />
								</div>
							</div>
						</s:form>
					</div>
				</div>
			</div>
			<input type="hidden" id="brandCode" name="editBrandActionForm.entBrand.brandCode" 
			             value="<s:property value="editBrandActionForm.entBrand.brandCode"/>"/>
			<!-- Start Kendo Dialog -->
			 <div id="confirmBackDialog">
<%--                 <div id='heading-popup'><s:property value="getText('message.confirm.titleBackToPrevious')"/></div> --%>
                <div>
                    <p><s:property value="getText('message.confirm.backToPrevious')"/></p>
                    <div style='text-align:center;padding:5px;'>
                        <input id='fancyConfirm_ok' class='k-button' type='button'
                              value='<s:property value="getText('message.confirm.yes')"/>' onclick='backToPreviousPage()' style='margin:7px;padding:5px;width:67px;'>
                        <input id='fancyConfirm_cancel' class='k-button' type='button'
                              value='<s:property value="getText('message.confirm.no')"/>' style='margin:7px;padding:5px;width:67px;'>
                   </div>
                </div>
             </div>
            <%-- BOE #6634 No.15 Hoang.Ho 2014/02/18 Create dialog confirm before save --%>
            <!-- begin submit confirm dialog -->
            <div id="confirmSubmitDialog" class="productPleaseSelectDialog">
                <div class="productPleaseSelectDialogContainer">
                <div class="center" style="margin-bottom: 20px;">
                    <s:property value="getText('message.confirm.dataChanged')"/>
                </div>
                <div class="buttonArea">
                  <div class="buttonRight">
                    <input type="button" class="k-button" id="btnSubmit" value="<s:property value="getText('text.btn.yes')"/>" />
                    <input type="button" class="k-button" id="btnCancelSubmit" value="<s:property value="getText('text.btn.cancel')"/>" />
                  </div>
                </div>
              </div>
            </div><!-- end submit confirm dialog -->
            <%-- EOE #6634 No.15 Hoang.Ho 2014/02/18 Create dialog confirm before save --%>
            <!-- End Kendo Dialog -->
            
            <!-- BOE Tran.Thanh 2014/03/18 : fixbug No.74 -->
            <input id='allFormDataToCheckUnChangeForm' type="hidden" value='<s:property value="getText('message.confirm.yes')"/>
                                                                            <s:property value="editBrandActionForm.entBrand.name"/>
																			<s:property value="editBrandActionForm.entBrand.hyoujiName1"/>
																			<s:property value="editBrandActionForm.entBrand.hyoujiName2"/>
																			<s:property value="editBrandActionForm.entBrand.hyoujiName3"/>
																			<s:property value="editBrandActionForm.entBrand.sortKanaName"/>
																			<s:property value="editBrandActionForm.entBrand.tantouUserId1"/>
																			<s:property value="editBrandActionForm.entBrand.url"/>
																			<s:property value="editBrandActionForm.entBrand.rss"/>
																			<s:property value="editBrandActionForm.entBrand.brandMarumeCode"/>
																			<s:property value="editBrandActionForm.entBrand.sentence"/>
																			<s:property value="editBrandActionForm.entBrand.delFlg"/>'>
			<!-- contents_inner end-->
		</div>
		<!--End Contents -->
<script type="text/javascript">
/* BOE Tran.Thanh : Fixbug No.70 */ 
/*var allFormData_toCheckUnChangeForm = ''
	+ '<s:property value="editBrandActionForm.entBrand.name"/>'
    + '<s:property value="editBrandActionForm.entBrand.hyoujiName1"/>'
    + '<s:property value="editBrandActionForm.entBrand.hyoujiName2"/>'
    + '<s:property value="editBrandActionForm.entBrand.hyoujiName3"/>'
    + '<s:property value="editBrandActionForm.entBrand.sortKanaName"/>'
    + '<s:property value="editBrandActionForm.entBrand.tantouUserId1"/>'
    + '<s:property value="editBrandActionForm.entBrand.url"/>'
    + '<s:property value="editBrandActionForm.entBrand.rss"/>'
    + '<s:property value="editBrandActionForm.entBrand.brandMarumeCode"/>'
    + '<s:property value="editBrandActionForm.entBrand.sentence"/>'
 	// Fix No.5-20140313_not_fix @rcv!cao.lan 2014/03/14
    + '<s:property value="editBrandActionForm.entBrand.delFlg"/>'*/
/* EOE Tran.Thanh : Fixbug No.70 */     
/* Error message */
var inputOutOfRange = '<s:property value="getText('message.error.inputOutOfRange')"/>';
var inputLessThanRange = '<s:property value="getText('message.error.inputLessThanRange')"/>';
var IdOrPassInputInvalid = '<s:property value="getText('message.error.IdOrPassInputInvalid')"/>';
var inputInvalid = '<s:property value="getText('message.error.invalidInput')"/>';
var requiredFill = '<s:property value="getText('message.error.requiredFill')"/>';
var katakanaOnly = '<s:property value="getText('message.error.katakanaOnly')"/>';
var LOGO_DB = $("#brandLogo").val();
var MAX_SIZE_UPLOAD = '<s:property value="getText('message.error.maximumFileLogoUpload')"/>';
</script>
	</tiles:putAttribute>
</tiles:insertDefinition>