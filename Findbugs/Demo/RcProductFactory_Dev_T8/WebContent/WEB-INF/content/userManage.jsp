<%@ page language="java" contentType="text/html; charset=Shift-JIS"
    pageEncoding="Shift-JIS"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<tiles:insertDefinition name="layoutCommon">
<tiles:putAttribute name="css">
    <link rel="stylesheet" type="text/css" href="css/userManager.css?20140804140000" />
</tiles:putAttribute>
<tiles:putAttribute name="javaScript">
    <script type="text/javascript" src="js/jQuery/jquery.freezeheader.js?20140804140000"></script>
    <script type="text/javascript" src="js/jQuery/jquery.fancybox.js?20140804140000"></script>
    <script type="text/javascript" src="js/userManageJS.js?20140804140000"></script>
</tiles:putAttribute>
<tiles:putAttribute name="body">
<!-- Contents -->
<div id="contents">
  <div id="content">
      <div id="title">
          <h1><s:property value="getText('text.label.userManageH1')"/></h1>
     </div>
     <!-- contents_inner start -->
    <div id="contents_inner" style="border: none; background: none;">
        <div id="filter">
            <label><s:property value="getText('text.label.userId')"/></label>
                <s:textfield id="userId" name="userId" cssClass="txt-size-medium" maxlength="12" autocomplete="off" value=""/>
            <label style="padding-left: 10px;"><s:property value="getText('common.label.userName')"/></label>
                <s:textfield id="userName" name="userName" cssClass="txt-size-medium" maxlength="20" autocomplete="off" value=""/>
            <%--  BOE login by redmine without Authentication @rcv!Nguyen.Chuong 2014/01/13 [6115]--%>
            <%-- <label id="author" style="padding-left: 10px;"><s:property value="getText('text.label.userAuthority')"/></label>
                <s:select id="Authority"
                               		name="Authority"
                               		list="UserManageActionForm.authorityTreeMap"
                               		headerKey="" headerValue="すべて"
                               		listKey="%{key}"
                               		listValue="%{value}"
                               		required="true"
                               		cssClass="cbb-size-small"/> --%>
            <%--  EOE login by redmine without Authentication @rcv!Nguyen.Chuong 2014/01/13 [6115]--%>
            <label id="status" style="padding-left: 10px;"><s:property value="getText('text.label.userStatus')"/></label>
                <s:select id="Status"
                               		name="Status"
                               		list="UserManageActionForm.statusTreeMap"
                               		headerKey="" headerValue="すべて"
                               		listKey="%{key}"
                               		listValue="%{value}"
                               		required="true"
                               		cssClass="cbb-size-small"/>
               <div class="btnSearch">
                   <s:submit value="検索" type="button" cssClass="btn-size-medium btn-color-default btnSearch" onclick="getUserDataAjax();"/>
               </div>
        </div>
        <div id="mainContent" class="tbl-content">
        	<input type="hidden" name="lockUserHeader" id="lockUserHeader" value="<s:property value="getText('message.confirm.lockUserHeader')"/>"/>
        	<input type="hidden" name="lockUser" id="lockUser" value="<s:property value="getText('message.confirm.lockUser')"/>"/>
        	<input type="hidden" name="activeUserHeader" id="activeUserHeader" value="<s:property value="getText('message.confirm.activeUserHeader')"/>"/>
        	<input type="hidden" name="activeUser" id="activeUser" value="<s:property value="getText('message.confirm.activeUser')"/>"/>
            <table class="tbl-size-large" id ="no4">
                <thead>
                    <tr>
                        <th width="10%"><span><s:property value="getText('text.label.userId')"/></span></th>
                        <th width="25%"><span><s:property value="getText('text.label.userName')"/></span></th>
                        <th width="27%"><span><s:property value="getText('text.label.userEmail')"/></span></th>
                         <%--BOE login by redmine without Authentication @rcv!Nguyen.Chuong 2014/01/13 [6115]--%>
                        <%-- <th width="12%"><span><s:property value="getText('text.label.userAuthority')"/></span></th> --%>
                        <%-- EOE login by redmine without Authentication @rcv!Nguyen.Chuong 2014/01/13 [6115]--%>
                        <th width="12%"><span><s:property value="getText('text.label.userStatus')"/></span></th>
                        <th width="14%"></th>
                    </tr>
                </thead>
                <tbody id="mainBody" class="tbl-body">
                <s:if test="%{userManageActionForm.listAllUser!= null && userManageActionForm.listAllUser.size() > 0 }">
                    <s:iterator value="%{userManageActionForm.listAllUser}" >
                        <s:if test="%{userDelFlg==0}">
                            <tr>
                        </s:if>
                        <s:else>
                            <tr class="tbl-row-background-gray">
                        </s:else>
                            <td style="text-align:left; width: 10%;"><s:property value="userId"/></td>
                            <td style="text-align:center; width: 25%;"><s:property value="userLastName"/> <s:property value="userFirstName"/></td>
                            <td style="text-align:center; width: 27%;"><s:property value="userEmail"/></td>
                            <%-- BOE login by redmine without Authentication @rcv!Nguyen.Chuong 2014/01/13 [6115]--%>
                            <%-- <s:if test="%{userAuthority==0}">
                                <td style="text-align:center; width: 12%;"><s:property value="getText('text.label.operater')"/></td>
                            </s:if>
                            <s:else>
                                <td style="text-align:center; width: 12%;"><s:property value="getText('text.label.admin')"/></td>
                            </s:else> --%>
                            <%-- EOE login by redmine without Authentication @rcv!Nguyen.Chuong 2014/01/13 [6115]--%>
                            <s:if test="%{userDelFlg==0}">
                                <td style="text-align:center; width: 12%;"><s:property value="getText('text.label.userStatus.active')"/></td>
                            </s:if>
                            <s:else>
                                <td style="text-align:center; width: 12%;"><s:property value="getText('text.label.userStatus.delete')"/></td>
                            </s:else>
                            <td class="tbl-colum-action" align="center" width="14%">
                                <s:if test="%{userDelFlg==0}">
                                  <a  href="changeStatus.html?userId=<s:property value="userId"/>&delFlg=<s:property value="userDelFlg"/>"
                                      onclick="confirmChangeStatusUser('<s:property value="userId"/>',<s:property value="userDelFlg"/>,'<s:property value="getText('message.confirm.lockUserHeader')"/>','<s:property value="getText('message.confirm.lockUser')"/>');return false;"
                                      class="tbl-control-size-small btn-size-small btn-color-gold tipS" 
                                      title="<s:property value="getText('text.label.userStatus.delete')"/>"
                                      style="color: color: #ffffff;text-decoration: none;"><s:property value="getText('text.label.userStatus.delete')"/></a>
                                  <a href="userDetail.html?userId=<s:property value="userId"/>"
                                     class="tbl-control-size-small btn-size-small btn-color-default"
                                     title="<s:property value="getText('text.label.userDetail')"/>"
                                     style="color: gray; text-decoration: none;"><s:property value="getText('text.label.userDetail')"/></a>
                                </s:if>
                                <s:elseif test="%{userDelFlg==1}">
<!--                                   <input class="tbl-control-size-small btn-size-small btn-color-default tipS" style="color: color: #ffffff;" -->
<!--                                         type="button" -->
<%--                                         onclick="confirmChangeStatusUser('<s:property value="userId"/>',<s:property value="delFlg"/>);" --%>
<!--                                         value='アンロック'> -->
                                  <a  href="changeStatus.html?userId=<s:property value="userId"/>&delFlg=<s:property value="userDelFlg"/>"
                                      onclick="confirmChangeStatusUser('<s:property value="userId"/>',<s:property value="userDelFlg"/>,'<s:property value="getText('message.confirm.activeUserHeader')"/>','<s:property value="getText('message.confirm.activeUser')"/>');return false;"
                                      class="tbl-control-size-small btn-size-small btn-color-default tipS"
                                      title="<s:property value="getText('text.label.unlock')"/>"
                                      style="color: color: #ffffff;text-decoration: none;"><s:property value="getText('text.label.unlock')"/></a>
                                  <a href="userDetail.html?userId=<s:property value="userId"/>"
                                     class="tbl-control-size-small btn-size-small btn-color-default"
                                     title="<s:property value="getText('text.label.userDetail')"/>"
                                     style="color: gray; text-decoration: none;"><s:property value="getText('text.label.userDetail')"/></a>
                                </s:elseif>
                              </td>
                        </tr>
                    </s:iterator>
                </s:if>
                <s:else>
                    <tr>
                        <td colspan="8" class="errorMessage">
                            <s:property value="getText('message.error.noData')"/>
                        </td>
                    </tr>
                </s:else>
                </tbody>
            </table>
        </div>
        <div id="btnNewPJ">
            <a href="newUser.html" class="btn-size-medium btn-color-default"
                title="<s:property value="getText('text.label.newUserlH1')"/>"
                style="color:gray; text-decoration: none; margin-top: 10px; position: absolute; width:96px;"><s:property value="getText('text.label.newUserlH1')"/></a>
        </div>
    </div><!-- contents_inner -->
    <!-- contents_inner end-->
  </div><!-- vehicle_list -->
</div>
<!--End Contents -->
<script type="text/javascript">
jQuery(document).ready(function () {
    jQuery("#no4").freezeHeader({ 'height': '546px'});
    
    $("select option").filter(function() {
		// may want to use $.trim in here
		return $(this).text() == "有効";
	}).prop('selected', true);
});

jQuery(document).keypress(function(event) {
    if (event.which == 13) {
        event.preventDefault();
        getUserDataAjax();
    }
});

</script>
</tiles:putAttribute>
</tiles:insertDefinition>