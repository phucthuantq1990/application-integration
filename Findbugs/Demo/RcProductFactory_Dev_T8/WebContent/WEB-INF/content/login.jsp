<%@ page language="java" contentType="text/html; charset=Shift-JIS" pageEncoding="Shift-JIS"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<script type="text/javascript" src="./js/jQuery/jquery-1.8.0.min.js?20140804140000"></script>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=Shift-JIS">
    <title>Login Product Factory</title>
    <link rel="stylesheet" type="text/css" href="css/base.css?20140804140000" />
	<link rel="stylesheet" type="text/css" href="css/layout.css?20140804140000" />
	<link rel="stylesheet" type="text/css" href="css/header.css?20140804140000" />
	<link rel="stylesheet" type="text/css" href="css/footer.css?20140804140000" />	
	<link rel="stylesheet" type="text/css" href="css/login.css?20140804140000"  />
	<!--[if IE]> <link href="css/ie.css" rel="stylesheet" type="text/css"> <![endif] -->
	<script type="text/javascript" src="js/jQuery/jquery-1.8.0.min.js?20140804140000"></script>
	<script type="text/javascript" src="js/jQuery/jquery.validate.min.js?20140804140000"></script>
	<script type="text/javascript" src="js/localization/messages_ja.js?20140804140000"></script>
</head>
<body>
<input type="hidden" id="errorIdOrPassEmpty" value="<s:property value="getText('message.error.IdOrPassEmpty')"/>"/>
<input type="hidden" id="errorIdOrPassWrong" value="<s:property value="getText('message.error.IdOrPassWrong')"/>"/>
<input type="hidden" id="errorIdOrPassInputInvalid" value="<s:property value="getText('message.error.IdOrPassInputInvalid')"/>"/>
<script type="text/javascript" src="js/loginJS.js?20140804140000"></script>
<!-- <!-- Top line begins --> 
<div id="top">
	<div class="wrapper">
    	<a href="#" title="" class="logo"><img src="images/logo.png" alt="" /></a>
        <div class="clear"></div>
    </div>
</div>
<!-- Top line ends -->
    <!-- Login wrapper begins -->
<div class="frm-login">
	<!-- User Login Form -->
    <form name="login_form" id="login" action="j_spring_security_check" method="post">
        <div class="lbl-background-color-blue"> 
           	<span>Webike Product Factory</span>
        </div>
        <div class="frm-login-row">
        	<s:property value="getText('text.label.userId')"/> &nbsp;
        	<input id="j_username" type="text" name="j_username" maxlength="30" class="txt-size-medium lettersonly required" />
        </div>
        <div class="frm-login-row">
        	<s:property value="getText('text.label.userPasswd')"/> &nbsp;&nbsp;
        	<input id="j_password" type="password" name="j_password" maxlength="30" class="txt-size-medium lettersonly required"/>
        </div>
        <span id="errorArea" class="errorMessage">
            <s:property value="commonForm.errorMessage"/>
        </span>
        <div class="btn-login">            
            <input id='btn_login' type="submit" name="submit" value="���O�C��" class="btn-size-large btn-color-default"/>
            <div class="clear"></div>
        </div>
    </form>
    <!-- End User Login Form -->
</div>
<!-- Login wrapper ends -->
<!-- Bottom line begins -->
<div id="bottom" style="position: absolute; ">
	<div id="top_link" class="clearfix"></div>
	<div id="footer" class="clearfix">
    	<div class="copyright">copyright&copy;2012 RiverCrane.All Rights Reserved.</div>
  	</div>
</div>
<!-- Bottom line ends --> 
</body>
</html>