<%@ page language="java" contentType="text/html; charset=Shift_JIS"
    pageEncoding="Shift_JIS"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<div id="detailSearchDialog" style="display:none">
    <input type="hidden" id="detailSearchDialogTitle" value="<s:property value="getText('detailSearch')"/>" />
    <input type="hidden" id="msgCharacterNumber" value="<s:property value="getText('textareaCharacterNumber')"/>" />
	<fieldset id="merchandiseInfo">
		<legend><s:property value="getText('merchandiseInfo')" /></legend>
		<div class="merchandiseInfoRow">
			<label class="merchandiseInfoLabel" for="systemProductCode">
				<s:property value="getText('systemProductCode')"/>
			</label>
			<input type="text" id="systemProductCode" class="k-textbox tb-search" />
		</div>
		<div class="merchandiseInfoRow">
            <label class="merchandiseInfoLabel" for="brand">
                <s:property value="getText('brand')"/>
            </label>
            <input type="text" id="brandCode" maxlength="8" class="k-textbox tb-code-search" />
            <div id="brandArea" class="list-option"><%-- <select id="brandSelect">
            </select> --%>
                <s:select
		           id="brandSelect"
		           list="productManagementActionForm.brandList"
		           listKey="brandCode"
		           listValue="name"
		           value=""
		           headerKey="-1"
		           headerValue=" "/>
            
            </div>
        </div>
		<div class="merchandiseInfoRow">
			<label class="merchandiseInfoLabel">
				<s:property value="getText('productName')"/>
			</label>
			<input type="text" id="productName" class="k-textbox tb-search" />
		</div>
		<div class="merchandiseInfoRow">
			<label class="merchandiseInfoLabel" >
				<s:property value="getText('makerStockNumber')"/>
			</label>
			<input type="text" id="makerStockNumber" class="k-textbox tb-search" />
		</div>
		<!-- <div class="merchandiseInfoRow">
            <label class="merchandiseInfoLabel" >
                Category Code
            </label>
            <input type="text" id="categoryCode" class="k-textbox" />
        </div> -->
		<div class="merchandiseInfoRow">
			<label class="merchandiseInfoLabel">
				<s:property value="getText('category')"/>
			</label>
			<input type="text" id="categoryCode" maxlength="8" class="k-textbox tb-code-search" />
			<div id="categoryArea" class="list-option">
			     <s:select
                   id="categorySelect"
                   list="productManagementActionForm.categoryList"
                   listKey="categoryCode"
                   listValue="categoryName"
                   headerKey="-1"
                   headerValue=" "/>
		     </div>
		</div>
		<!-- <div class="merchandiseInfoRow">
            <label class="merchandiseInfoLabel" >
                Siire Code
            </label>
            <input type="text" id="siireCode" class="k-textbox" />
        </div> -->
		<div class="merchandiseInfoRow">
			<label class="merchandiseInfoLabel">
				<s:property value="getText('supplier')"/>
			</label>
			<input type="text" id="siireCode" maxlength="3" class="k-textbox tb-code-search" />
			<div id="siireArea" class="list-option">
			     <s:select
                   id="supplierSelect"
                   list="productManagementActionForm.siireList"
                   listKey="siireCode"
                   listValue="siireName"
                   headerKey="-1"
                   headerValue=" "/>
			</div>
		</div>
		<div class="merchandiseInfoRow">
			<label class="merchandiseInfoLabel">
				<s:property value="getText('jan')"/>
			</label>
			<input type="text" id="jan" class="k-textbox tb-search" />
		</div>
		<div class="merchandiseInfoRow">
			<label class="merchandiseInfoLabel">
				<s:property value="getText('groupCode')"/>
			</label>
			<input type="text" id="groupCode" class="k-textbox tb-search" />
		</div>
		<div class="merchandiseInfoRow">
			<label class="merchandiseInfoLabel">
				<s:property value="getText('note')"/>
			</label>
			<input type="text" id="note" class="k-textbox tb-search" />
		</div>
		<div class="merchandiseInfoRow">
			<label class="merchandiseInfoLabel">
				<s:property value="getText('summary')"/>
			</label>
			<input type="text" id="summary" class="k-textbox tb-search" />
		</div>
		<div class="merchandiseInfoRow">
			<label class="merchandiseInfoLabel">
				<s:property value="getText('explanation')"/>
			</label>
			<input type="text" id="explanation" class="k-textbox tb-search" />
		</div>
		<div class="merchandiseInfoRow">
			<label class="merchandiseInfoLabel">
				<s:property value="getText('caution')"/>
			</label>
			<input type="text" id="caution" class="k-textbox tb-search" />
		</div>
<!-- 	BOE #12097 2014/09/10 Nguyen.Chuong: add maker filter in detail search. -->
		<div class="merchandiseInfoRow">
		    <label class="merchandiseInfoLabel">
                <s:property value="getText('maker')"/>
            </label>
            <input type="text" id="maker" class="k-textbox tb-search" />
		</div>
<!-- 	EOE #12097 2014/09/10 Nguyen.Chuong: add maker filter in detail search. -->
		<div class="merchandiseInfoRow">
			<label class="merchandiseInfoLabel">
				<s:property value="getText('supportedBike')"/>
			</label>
			<input type="text" id="supportedBike" class="k-textbox tb-search" />
		</div>
		<div class="merchandiseInfoRow">
			<label class="merchandiseInfoLabel">
				<s:property value="getText('bikeModel')"/>
			</label>
			<input type="text" id="bikeModel" class="k-textbox tb-search" />
		</div>
		<div class="merchandiseInfoRow">
			<label class="merchandiseInfoLabel">
				<s:property value="getText('optionType')"/>
			</label>
			<select id="optionTypeSelect" class="tb-search"></select>
		</div>
		<div class="merchandiseInfoRow">
			<label class="merchandiseInfoLabel">
				<s:property value="getText('option')"/>
			</label>
            <input type="text" id="option" class="k-textbox tb-search" />
		</div>
		<div class="merchandiseInfoRow">
			<label class="merchandiseInfoLabel">
				<s:property value="getText('thumbnailImagePath')"/>
			</label>
			<input type="text" id="thumbnailImagePath" class="k-textbox tb-search" />
		</div>
		<div class="merchandiseInfoRow">
			<label class="merchandiseInfoLabel">
				<s:property value="getText('detailedImagePath')"/>
			</label>
			<input type="text" id="detailedImagePath" class="k-textbox tb-search" />
		</div>
		<div class="merchandiseInfoRow">
			<label class="merchandiseInfoLabel">
				<s:property value="getText('deliveryTime')"/>
			</label>
			<select id="deliveryTimeSelect" class="tb-search">
			</select>
		</div>
		<div class="merchandiseInfoRow">
			<label class="merchandiseInfoLabel">
				<s:property value="getText('attribute')"/>
			</label>
			<select id="attributeSelect" class="tb-search">
			</select>
		</div>
		<div class="merchandiseInfoRow">
			<label class="merchandiseInfoLabel">
				<s:property value="getText('attributeDisplayName')"/>
			</label>
			<input type="text" id="attributeDisplayName" class="k-textbox tb-search" />
		</div>
		<div class="merchandiseInfoRow attributeManagementValueRow">
			<label class="merchandiseInfoLabel">
				<s:property value="getText('attributeManagementName')"/>
			</label>
			<input type="text" id="attributeManagementValue" class="k-textbox tb-search" />
			<input type="checkbox" id="attributeManagementValueFlagCheckBox" style="display: none" />
		</div>
		<div class="merchandiseInfoRow">
			<label class="merchandiseInfoLabel">
				<s:property value="getText('linkTitle')"/>
			</label>
			<input type="text" id="linkTitle" class="k-textbox tb-search" />
		</div>
		<div class="merchandiseInfoRow">
			<label class="merchandiseInfoLabel">
				<s:property value="getText('videoTitle')"/>
			</label>
			<input type="text" id="videoTitle" class="k-textbox tb-search" />
		</div>
	</fieldset>
	
	<fieldset id="whetherSearchCondition">
		<legend><s:property value="getText('whetherSearchCondition')" /></legend>
		<div class="whetherSearchConditionRow">
			<input type="button" id="whetherSearchConditionResetButton" class="k-button" value="<s:property value="getText('resetEn')"/>"/>
		</div>
		<div class="whetherSearchConditionRow">
		</div>
		<div class="whetherSearchConditionRow">
		</div>
		<div class="whetherSearchConditionRow">
		</div>
		<div class="whetherSearchConditionRow">
		</div>
		<div class="whetherSearchConditionRow">
		</div>
		<div class="whetherSearchConditionRow">
			<input type="checkbox" id="janCheckBox" class="whetherSearchConditionCheckbox"/>
		</div>
		<div class="whetherSearchConditionRow">
		</div>
		<div class="whetherSearchConditionRow">
			<input type="checkbox" id="noteCheckBox" class="whetherSearchConditionCheckbox" />
		</div>
		<div class="whetherSearchConditionRow">
			<input type="checkbox" id="summaryCheckBox" class="whetherSearchConditionCheckbox" />
		</div>
		<div class="whetherSearchConditionRow">
			<input type="checkbox" id="explanationCheckBox" class="whetherSearchConditionCheckbox" />
		</div>
		<div class="whetherSearchConditionRow">
			<input type="checkbox" id="cautionCheckBox" class="whetherSearchConditionCheckbox" />
		</div>
		<!--  BOE #12097 2014/09/10 Nguyen.Chuong: add maker filter in detail search. -->
	    <div class="whetherSearchConditionRow">
            <input type="checkbox" id="makerCheckBox" class="whetherSearchConditionCheckbox" />
        </div>
        <!--    BOE #12097 2014/09/10 Nguyen.Chuong: add maker filter in detail search. -->
		<div class="whetherSearchConditionRow">
			<input type="checkbox" id="supportedBikeCheckBox" class="whetherSearchConditionCheckbox" />
		</div>
		<div class="whetherSearchConditionRow">
			<input type="checkbox" id="bikeModelCheckBox" class="whetherSearchConditionCheckbox"  />
		</div>
		<div class="whetherSearchConditionRow">
			</div>
		<div class="whetherSearchConditionRow">
			<input type="checkbox" id="optionCheckBox" class="whetherSearchConditionCheckbox" />
		</div>
		<div class="whetherSearchConditionRow">
			<input type="checkbox" id="thumbnailImagePathCheckBox" class="whetherSearchConditionCheckbox" />
		</div>
		<div class="whetherSearchConditionRow">
			<input type="checkbox" id="detailedImagePathCheckBox" class="whetherSearchConditionCheckbox" />
		</div>
		<div class="whetherSearchConditionRow">
		</div>
		<div class="whetherSearchConditionRow">
		</div>
		<div class="whetherSearchConditionRow">
			<input type="checkbox" id="attributeDisplayNameCheckBox" class="whetherSearchConditionCheckbox" />
		</div>
		<div class="whetherSearchConditionRow">
			<input type="checkbox" id="attributeManagementValueCheckBox" class="whetherSearchConditionCheckbox" />
		</div>
		<div class="whetherSearchConditionRow">
			<input type="checkbox" id="linkTitleCheckBox" class="whetherSearchConditionCheckbox" />
		</div>
		<div class="whetherSearchConditionRow">
			<input type="checkbox" id="videoTitleCheckBox" class="whetherSearchConditionCheckbox" />
		</div>
	</fieldset>
	
	<fieldset id="searchOption">
		<legend><s:property value="getText('searchOption')" /></legend>
		<div class="searchOptionRow">
			<input type="button" id="searchOptionResetButton" class="k-button" value="<s:property value="getText('resetEn')"/>"/>
		</div>
		<div class="searchOptionRow"></div>
		<div class="searchOptionRow">
			<input type="radio" id="productNameRadioButton1" class="searchOptionRadioButton" name="productName" value="<s:property value="@net.webike.RcProductFactory.entity.EnumSearchOption@EQUAL.key" />" />
			<label>
				<s:property value="getText('completeness')"/>
			</label>
			<input type="radio" id="productNameRadioButton2" class="searchOptionRadioButton" name="productName" value="<s:property value="@net.webike.RcProductFactory.entity.EnumSearchOption@LIKE.key" />"/>
			<label>
				<s:property value="getText('part')"/>
			</label>
			<input type="radio" id="productNameRadioButton3" class="searchOptionRadioButton" name="productName" value="<s:property value="@net.webike.RcProductFactory.entity.EnumSearchOption@NOT_LIKE.key" />" />
			<label>
				<s:property value="getText('none')"/>
			</label>
		</div>
		<div class="searchOptionRow">
			<input type="radio" id="makerStockNumberRadioButton1" class="searchOptionRadioButton" name="makerStockNumber" value="<s:property value="@net.webike.RcProductFactory.entity.EnumSearchOption@EQUAL.key" />"/>
			<label>
				<s:property value="getText('completeness')"/>
			</label>
			<input type="radio" id="makerStockNumberRadioButton2" class="searchOptionRadioButton" name="makerStockNumber" value="<s:property value="@net.webike.RcProductFactory.entity.EnumSearchOption@LIKE.key" />"/>
			<label>
				<s:property value="getText('part')"/>
			</label>
			<input type="radio" id="makerStockNumberRadioButton3" class="searchOptionRadioButton" name="makerStockNumber" value="<s:property value="@net.webike.RcProductFactory.entity.EnumSearchOption@NOT_LIKE.key" />"/>
			<label>
				<s:property value="getText('none')"/>
			</label>
		</div>
		<div class="searchOptionRow"></div>
		<div class="searchOptionRow"></div>
		<div class="searchOptionRow">
			<input type="radio" id="janRadioButton1" class="searchOptionRadioButton" name="jan" value="<s:property value="@net.webike.RcProductFactory.entity.EnumSearchOption@EQUAL.key" />"/>
			<label>
				<s:property value="getText('completeness')"/>
			</label>
			<input type="radio" id="janRadioButton2" class="searchOptionRadioButton" name="jan" value="<s:property value="@net.webike.RcProductFactory.entity.EnumSearchOption@LIKE.key" />"/>
			<label>
				<s:property value="getText('part')"/>
			</label>
			<input type="radio" id="janRadioButton3" class="searchOptionRadioButton" name="jan" value="<s:property value="@net.webike.RcProductFactory.entity.EnumSearchOption@NOT_LIKE.key" />"/>
			<label>
				<s:property value="getText('none')"/>
			</label>
		</div>
		<div class="searchOptionRow">
			<input type="radio" id="groupCodeRadioButton1" class="searchOptionRadioButton" name="groupCode" value="<s:property value="@net.webike.RcProductFactory.entity.EnumSearchOption@EQUAL.key" />"/>
			<label>
				<s:property value="getText('completeness')"/>
			</label>
			<input type="radio" id="groupCodeRadioButton2" class="searchOptionRadioButton" name="groupCode" value="<s:property value="@net.webike.RcProductFactory.entity.EnumSearchOption@LIKE.key" />"/>
			<label>
				<s:property value="getText('part')"/>
			</label>
			<input type="radio" id="groupCodeRadioButton3" class="searchOptionRadioButton" name="groupCode" value="<s:property value="@net.webike.RcProductFactory.entity.EnumSearchOption@NOT_LIKE.key" />"/>
			<label>
				<s:property value="getText('none')"/>
			</label>
		</div>
		<div class="searchOptionRow">
			<input type="radio" id="noteRadioButton1" class="searchOptionRadioButton" name="note" value="<s:property value="@net.webike.RcProductFactory.entity.EnumSearchOption@EQUAL.key" />"/>
			<label>
				<s:property value="getText('completeness')"/>
			</label>
			<input type="radio" id="noteRadioButton2" class="searchOptionRadioButton" name="note" value="<s:property value="@net.webike.RcProductFactory.entity.EnumSearchOption@LIKE.key" />"/>
			<label>
				<s:property value="getText('part')"/>	
			</label>
			<input type="radio" id="noteRadioButton3" class="searchOptionRadioButton" name="note" value="<s:property value="@net.webike.RcProductFactory.entity.EnumSearchOption@NOT_LIKE.key" />"/>
			<label>
				<s:property value="getText('none')"/>
			</label>
		</div>
		<div class="searchOptionRow">
			<input type="radio" id="summaryRadioButton1" class="searchOptionRadioButton" name="summary" value="<s:property value="@net.webike.RcProductFactory.entity.EnumSearchOption@EQUAL.key" />"/>
			<label>
				<s:property value="getText('completeness')"/>
			</label>
			<input type="radio" id="summaryRadioButton2" class="searchOptionRadioButton" name="summary" value="<s:property value="@net.webike.RcProductFactory.entity.EnumSearchOption@LIKE.key" />"/>
			<label>
				<s:property value="getText('part')"/>
			</label>
			<input type="radio" id="summaryRadioButton3" class="searchOptionRadioButton" name="summary" value="<s:property value="@net.webike.RcProductFactory.entity.EnumSearchOption@NOT_LIKE.key" />"/>
			<label>
				<s:property value="getText('none')"/>
			</label>
		</div>
		<div class="searchOptionRow">
			<input type="radio" id="explanationRadioButton1" class="searchOptionRadioButton" name="explanation" value="<s:property value="@net.webike.RcProductFactory.entity.EnumSearchOption@EQUAL.key" />"/>
			<label>
				<s:property value="getText('completeness')"/>
			</label>
			<input type="radio" id="explanationRadioButton2" class="searchOptionRadioButton" name="explanation" value="<s:property value="@net.webike.RcProductFactory.entity.EnumSearchOption@LIKE.key" />"/>
			<label>
				<s:property value="getText('part')"/>
			</label>
			<input type="radio" id="explanationRadioButton3" class="searchOptionRadioButton" name="explanation" value="<s:property value="@net.webike.RcProductFactory.entity.EnumSearchOption@NOT_LIKE.key" />"/>
			<label>
				<s:property value="getText('none')"/>
			</label>
		</div>
		<div class="searchOptionRow">
			<input type="radio" id="cautionRadioButton1" class="searchOptionRadioButton" name="caution" value="<s:property value="@net.webike.RcProductFactory.entity.EnumSearchOption@EQUAL.key" />"/>
			<label>
				<s:property value="getText('completeness')"/>
			</label>
			<input type="radio" id="cautionRadioButton2" class="searchOptionRadioButton" name="caution" value="<s:property value="@net.webike.RcProductFactory.entity.EnumSearchOption@LIKE.key" />"/>
			<label>
				<s:property value="getText('part')"/>
			</label>
			<input type="radio" id="cautionRadioButton3" class="searchOptionRadioButton" name="caution" value="<s:property value="@net.webike.RcProductFactory.entity.EnumSearchOption@NOT_LIKE.key" />"/>
			<label>
				<s:property value="getText('none')"/>
			</label>
		</div>
		<!--  BOE #12097 2014/09/10 Nguyen.Chuong: add maker filter in detail search. -->
		<div class="searchOptionRow">
            <input type="radio" id="makerRadioButton1" class="searchOptionRadioButton" name="maker" value="<s:property value="@net.webike.RcProductFactory.entity.EnumSearchOption@EQUAL.key" />"/>
            <label>
                <s:property value="getText('completeness')"/>
            </label>
            <input type="radio" id="makerRadioButton2" class="searchOptionRadioButton" name="maker" value="<s:property value="@net.webike.RcProductFactory.entity.EnumSearchOption@LIKE.key" />"/>
            <label>
                <s:property value="getText('part')"/>
            </label>
            <input type="radio" id="makerRadioButton3" class="searchOptionRadioButton" name="maker" value="<s:property value="@net.webike.RcProductFactory.entity.EnumSearchOption@NOT_LIKE.key" />"/>
            <label>
                <s:property value="getText('none')"/>
            </label>
        </div>
        <!--  EOE #12097 2014/09/10 Nguyen.Chuong: add maker filter in detail search. -->
		<div class="searchOptionRow">
			<input type="radio" id="supportedBikeRadioButton1" class="searchOptionRadioButton" name="supportedBike" value="<s:property value="@net.webike.RcProductFactory.entity.EnumSearchOption@EQUAL.key" />"/>
			<label>
				<s:property value="getText('completeness')"/>
			</label>
			<input type="radio" id="supportedBikeRadioButton2" class="searchOptionRadioButton" name="supportedBike" value="<s:property value="@net.webike.RcProductFactory.entity.EnumSearchOption@LIKE.key" />"/>
			<label>
				<s:property value="getText('part')"/>
			</label>
			<input type="radio" id="supportedBikeRadioButton3" class="searchOptionRadioButton" name="supportedBike" value="<s:property value="@net.webike.RcProductFactory.entity.EnumSearchOption@NOT_LIKE.key" />"/>
			<label>
				<s:property value="getText('none')"/>
			</label>
		</div>
		<div class="searchOptionRow">
			<input type="radio" id="bikeModelRadioButton1" class="searchOptionRadioButton" name="bikeModel" value="<s:property value="@net.webike.RcProductFactory.entity.EnumSearchOption@EQUAL.key" />"/>
			<label class="searchOptionLabel">
				<s:property value="getText('completeness')"/>
			</label>
			<input type="radio" id="bikeModelRadioButton2" class="searchOptionRadioButton" name="bikeModel" value="<s:property value="@net.webike.RcProductFactory.entity.EnumSearchOption@LIKE.key" />"/>
			<label class="searchOptionLabel">
				<s:property value="getText('part')"/>
			</label>
			<input type="radio" id="bikeModelRadioButton3" class="searchOptionRadioButton" name="bikeModel" value="<s:property value="@net.webike.RcProductFactory.entity.EnumSearchOption@NOT_LIKE.key" />"/>
			<label class="searchOptionLabel">
				<s:property value="getText('none')"/>
			</label>
		</div>
		<div class="searchOptionRow"></div>
		<div class="searchOptionRow">
			<input type="radio" id="optionRadioButton1" class="searchOptionRadioButton" name="option" value="<s:property value="@net.webike.RcProductFactory.entity.EnumSearchOption@EQUAL.key" />"/>
			<label class="searchOptionLabel">
				<s:property value="getText('completeness')"/>
			</label>
			<input type="radio" id="optionRadioButton2" class="searchOptionRadioButton" name="option" value="<s:property value="@net.webike.RcProductFactory.entity.EnumSearchOption@LIKE.key" />"/>
			<label class="searchOptionLabel">
				<s:property value="getText('part')"/>
			</label>
			<input type="radio" id="optionRadioButton3" class="searchOptionRadioButton" name="option" value="<s:property value="@net.webike.RcProductFactory.entity.EnumSearchOption@NOT_LIKE.key" />"/>
			<label class="searchOptionLabel">
				<s:property value="getText('none')"/>
			</label>
		</div>
		<div class="searchOptionRow">
			<input type="radio" type="radio" id="thumbnailImagePathRadioButton1" class="searchOptionRadioButton" name="thumbnailImagePath" value="<s:property value="@net.webike.RcProductFactory.entity.EnumSearchOption@EQUAL.key" />"/>
			<label>
				<s:property value="getText('completeness')"/>
			</label>
			<input type="radio" type="radio" id="thumbnailImagePathRadioButton2" class="searchOptionRadioButton" name="thumbnailImagePath" value="<s:property value="@net.webike.RcProductFactory.entity.EnumSearchOption@LIKE.key" />"/>
			<label>
				<s:property value="getText('part')"/>
			</label>
			<input type="radio" type="radio" id="thumbnailImagePathRadioButton3" class="searchOptionRadioButton" name="thumbnailImagePath" value="<s:property value="@net.webike.RcProductFactory.entity.EnumSearchOption@NOT_LIKE.key" />"/>
			<label>
				<s:property value="getText('none')"/>
			</label>
		</div>
		<div class="searchOptionRow">
			<input type="radio" id="detailedImagePathRadioButton1" class="searchOptionRadioButton" name="detailedImagePath" value="<s:property value="@net.webike.RcProductFactory.entity.EnumSearchOption@EQUAL.key" />"/>
			<label>
				<s:property value="getText('completeness')"/>
			</label>
			<input type="radio" id="detailedImagePathRadioButton2" class="searchOptionRadioButton" name="detailedImagePath" value="<s:property value="@net.webike.RcProductFactory.entity.EnumSearchOption@LIKE.key" />"/>
			<label>
				<s:property value="getText('part')"/>
			</label>
			<input type="radio" id="detailedImagePathRadioButton3" class="searchOptionRadioButton" name="detailedImagePath" value="<s:property value="@net.webike.RcProductFactory.entity.EnumSearchOption@NOT_LIKE.key" />"/>
			<label>
				<s:property value="getText('none')"/>
			</label>
		</div>
		<div class="searchOptionRow"></div>
		<div class="searchOptionRow"></div>
		<div class="searchOptionRow">
			<input type="radio" id="attributeDisplayNameRadioButton1" class="searchOptionRadioButton" name="attributeDisplayName" value="<s:property value="@net.webike.RcProductFactory.entity.EnumSearchOption@EQUAL.key" />"/>
			<label>
				<s:property value="getText('completeness')"/>
			</label>
			<input type="radio" id="attributeDisplayNameRadioButton2" class="searchOptionRadioButton" name="attributeDisplayName" value="<s:property value="@net.webike.RcProductFactory.entity.EnumSearchOption@LIKE.key" />" />
			<label>
				<s:property value="getText('part')"/>
			</label>
			<input type="radio" id="attributeDisplayNameRadioButton3" class="searchOptionRadioButton" name="attributeDisplayName" value="<s:property value="@net.webike.RcProductFactory.entity.EnumSearchOption@NOT_LIKE.key" />"/>
			<label>
				<s:property value="getText('none')"/>
			</label>
		</div>
		<div class="searchOptionRow">
			<input type="radio" id="attributeManagementValueRadioButton1" class="searchOptionRadioButton" name="attributeManagementValue" value="<s:property value="@net.webike.RcProductFactory.entity.EnumSearchOption@EQUAL.key" />"/>
			<label>
				<s:property value="getText('completeness')"/>
			</label>
			<input type="radio" id="attributeManagementValueRadioButton2" class="searchOptionRadioButton" name="attributeManagementValue" value="<s:property value="@net.webike.RcProductFactory.entity.EnumSearchOption@LIKE.key" />"/>
			<label>
				<s:property value="getText('part')"/>
			</label>
			<input type="radio" id="attributeManagementValueRadioButton3" class="searchOptionRadioButton" name="attributeManagementValue" value="<s:property value="@net.webike.RcProductFactory.entity.EnumSearchOption@NOT_LIKE.key" />"/>
			<label>
				<s:property value="getText('none')"/>
			</label>
		</div>
		<div class="searchOptionRow">
			<input type="radio" id="linkTitleRadioButton1" class="searchOptionRadioButton" name="linkTitle" value="<s:property value="@net.webike.RcProductFactory.entity.EnumSearchOption@EQUAL.key" />"/>
			<label>
				<s:property value="getText('completeness')"/>
			</label>
			<input type="radio" id="linkTitleRadioButton2" class="searchOptionRadioButton" name="linkTitle" value="<s:property value="@net.webike.RcProductFactory.entity.EnumSearchOption@LIKE.key" />"/>
			<label>
				<s:property value="getText('part')"/>
			</label>
			<input type="radio"  id="linkTitleRadioButton3" class="searchOptionRadioButton" name="linkTitle" value="<s:property value="@net.webike.RcProductFactory.entity.EnumSearchOption@NOT_LIKE.key" />"/>
			<label>
				<s:property value="getText('none')"/>
			</label>
		</div>
		<div class="searchOptionRow">
			<input type="radio" id="videoTitleRadioButton1" class="searchOptionRadioButton" name="videoTitle" value="<s:property value="@net.webike.RcProductFactory.entity.EnumSearchOption@EQUAL.key" />"/>
			<label>
				<s:property value="getText('completeness')"/>
			</label>
			<input type="radio" id="videoTitleRadioButton2" class="searchOptionRadioButton" name="videoTitle" value="<s:property value="@net.webike.RcProductFactory.entity.EnumSearchOption@LIKE.key" />"/>
			<label>
				<s:property value="getText('part')"/>
			</label>
			<input type="radio" id="videoTitleRadioButton3" class="searchOptionRadioButton" name="videoTitle" value="<s:property value="@net.webike.RcProductFactory.entity.EnumSearchOption@NOT_LIKE.key" />"/>
			<label>
				<s:property value="getText('none')"/>
			</label>
		</div>
	</fieldset>
	
	<div id="otherSearchOptions">
		<fieldset id="otherSearchCriteria">
			<legend><s:property value="getText('otherSearchCriteria')" /></legend>
			<div class="otherSearchCriteriaVariousFlagRow">
				<label class="otherSearchCriteriaVariousFlagLabel">
					<s:property value="getText('variousFlag')"/>
				</label>
				<select id="variousFlag" class="otherSearchCriteriaWideInput" multiple="multiple"></select>
			</div>
			<div style="clear: both"></div>
			<div class="otherSearchCriteriaRow">
				<label class="otherSearchCriteriaLabel">
					<s:property value="getText('listPrice')"/>
				</label>
				<input type="text" id="fromListPrice" class="k-textbox" />
				&nbsp;~&nbsp; 
				<input type="text" id="toListPrice" class="k-textbox" />
			</div>
			<div class="otherSearchCriteriaRow">
				<label class="otherSearchCriteriaLabel">
					<s:property value="getText('partition')"/>
				</label>
				<input type="text" id="fromPartition" class="k-textbox" />
				&nbsp;~&nbsp; 
				<input type="text" id="toPartition" class="k-textbox" />
			</div>
			<div class="otherSearchCriteriaRow">
				<label class="otherSearchCriteriaLabel">
					<s:property value="getText('makerHandlingSituation')"/>
				</label>
				<select id="makerHandlingSituationSelect" class="otherSearchCriteriaWideInput">
				</select>
			</div>
			<div class="otherSearchCriteriaRow">
				<label class="otherSearchCriteriaLabel">
					<s:property value="getText('dataManagementSituation')"/>
				</label>
				<select id="dataManagementSituationSelect" class="otherSearchCriteriaWideInput">
				</select>
			</div>
		</fieldset>
		
		<fieldset id="managementRecord">
			<legend><s:property value="getText('managementRecord')" /></legend>
			<div class="managementRecordRow">
				<div class="managementRecordColumn1">
					<span class="managementRecordLabel">
						<input type="checkbox" id="productRegistrationDateCheckBox" class="managementRecordCheckBox"/>
						<label><s:property value="getText('productRegistrationDate')"/></label>
					</span>
				</div>
				<div class="managementRecordColumn2">
					<div class="managementRecordRow">
						<input type="text" id="productRegistrationDateFromPicker" class="managementRecordInput"  />&nbsp;
                        <span class="managementRecordPeriod">
                          <s:property value="getText('from')"/>&nbsp;
                        </span>
					</div>
					<div class="managementRecordRow">
						<input type="text" id="productRegistrationDateToPicker" class="managementRecordInput"  />&nbsp;
						<span class="managementRecordPeriod">
						  <s:property value="getText('to')"/>&nbsp;
						</span>
					</div>
				</div>
			</div>
			
			<div style="clear:both"></div>
			
			<div class="managementRecordRow">
				<div class="managementRecordColumn1">
					<span class="managementRecordLabel">
						<input type="checkbox" id="productRenovationDateCheckBox" class="managementRecordCheckBox" />
						<label><s:property value="getText('productRenovationDate')"/></label>
					</span>
				</div>
				<div class="managementRecordColumn2">
					<div class="managementRecordRow">
						<input type="text" id="productRenovationDateFromPicker" class="managementRecordInput"  />&nbsp;
						<span class="managementRecordPeriod">
						  <s:property value="getText('from')"/>&nbsp;
						</span>
					</div>
					<div class="managementRecordRow">
						<input type="text" id="productRenovationDateToPicker" class="managementRecordInput"  />&nbsp;
						<span class="managementRecordPeriod">
						  <s:property value="getText('to')"/>&nbsp;
						</span>
					</div>
				</div>
			</div>
			
			<div style="clear:both"></div>

			<div class="managementRecordRow">
				<div class="managementRecordColumn1">
					<span class="managementRecordLabel">
						<input type="checkbox" id="lastUpdatedPersonCheckBox" class="managementRecordCheckBox" />
						<label><s:property value="getText('lastUpdatedPerson')"/></label>
					</span>
				</div>
				<div class="managementRecordColumn2">
					<select id="lastUpdatedPersonSelect" class="managementRecordInput"></select>&nbsp;
				</div>
			</div>
			
			<div style="clear:both"></div>
			
		</fieldset>
		
		<fieldset id="multiSearch">
			<legend><s:property value="getText('multiSearch')" /></legend>
			<div class="multiSearchRow">
				<span class="multiSearchLabel">
					<input type="radio" id="multiSearchRadioButton1" class="multiSearchRadioButton" name="multiSearch" value="0" />
					<label>
						<s:property value="getText('without')"/>
					</label>
				</span>
				<span class="multiSearchLabel">
					<input type="radio" id="multiSearchRadioButton2" class="multiSearchRadioButton" name="multiSearch" value="1" />
					<label>
						<s:property value="getText('systemProductCode')"/>
					</label>
				</span>
			</div>
			<div style="clear: both"></div>
			<div class="multiSearchRow">
				<span class="multiSearchLabel">
					<input type="radio" id="multiSearchRadioButton3" class="multiSearchRadioButton" name="multiSearch" value="2"/>
					<label>
						<s:property value="getText('jan')"/>
					</label>
				</span>
				<span class="multiSearchLabel">
					<input type="radio" id="multiSearchRadioButton4" class="multiSearchRadioButton" name="multiSearch" value="3"/>
					<label>
						<s:property value="getText('makerStockNumber')"/>
					</label>
				</span>
				<span class="multiSearchItemCounterLabel">
				    <span id="itemCount">
				    0
                    </span>
                    <span id="itemCountLabel">
                        <s:property value="getText('itemCounter')"/>
                    </span>
				</span>
			</div>
			<div style="clear: both"></div>
                <div role="tooltip" class="k-widget k-tooltip k-popup k-group k-reset k-state-border-up" data-role="popup" style="display: none; opacity: 1;" 
                id="multiSearchCriteriaTextArea_tt_active">
                    <div class="k-tooltip-content"><s:property value="getText('textareaCharacterNumber')"/></div>
                    <div class="k-callout k-callout-s" style="left: 9px;"></div>
                </div>
			<textarea id="multiSearchCriteriaTextArea" class="k-textbox" rows="10" cols="10" style="resize:none;" ></textarea>
		</fieldset>
		
		<fieldset id="controlButton">
			<div class="controlButtonRow">
				<input type="button" id="overallResetButton" class="k-button realControlButton" value="<s:property value="getText('reset')"/>" />
				<input type="button" id="searchButton" class="k-button realControlButton" value="<s:property value="getText('search')"/>" />
			</div>
		</fieldset>
	</div>

</div>

<div id="noChangeDialog" style="display:none">
    <input type="hidden" id="noChangeDialogTitle" value="<s:property value="getText('confirmation')"/>" />
    <input type="hidden" id="noChangeDialogText" value="<s:property value="getText('searchCriteriaDoNotChange')"/>" />
    <div id="noChangeDialogContent">
    </div>
    <div id="noChangeDialogButtonPanel">
    <input type="button" id="noChangeDialogOkButton" class="k-button" value="<s:property value="getText('yes')"/>" />
    <input type="button" id="noChangeDialogCancelButton" class="k-button" value="<s:property value="getText('cancel')"/>" />
    </div>
</div>

<div id="noChangeDefaultDialog" style="display:none">
    <input type="hidden" id="noChangeDefaultDialogTitle" value="<s:property value="getText('error')"/>" />
    <input type="hidden" id="noChangeDefaultDialogText" value="<s:property value="getText('searchCriteriaDoNotChangeDefaultText')"/>" />
    <div id="noChangeDefaultDialogContent">
    </div>
    <div id="noChangeDefaultDialogButtonPanel">
        <input type="button" id="noChangeDefaultDialogOkButton" class="k-button" value="<s:property value="getText('ok')"/>" />
    </div>
</div>

<div id="noExistDialog" style="display:none">
    <input type="hidden" id="noExistDialogTitle" value="<s:property value="getText('error')"/>" />
    <input type="hidden" id="brandNoExistMessage" value="<s:property value="getText('brandNoExistMessage')"/>" />
    <input type="hidden" id="categoryNoExistMessage" value="<s:property value="getText('categoryNoExistMessage')"/>" />
    <input type="hidden" id="deliveryTimeNoExistMessage" value="<s:property value="getText('deliveryTimeNoExistMessage')"/>" />
    <input type="hidden" id="supplierNoExistMessage" value="<s:property value="getText('supplierNoExistMessage')"/>" />
    <input type="hidden" id="optionTypeNoExistMessage" value="<s:property value="getText('optionTypeNoExistMessage')"/>" />
    <input type="hidden" id="makerHandlingSituationNoExistMessage" value="<s:property value="getText('makerHandlingSituationNoExistMessage')"/>" />
    <input type="hidden" id="attributeNoExistMessage" value="<s:property value="getText('attributeNoExistMessage')"/>" />
    <input type="hidden" id="lastUpdatedPersonNoExistMessage" value="<s:property value="getText('lastUpdatedPersonNoExistMessage')"/>" />
    <div id="noExistDialogContent">
    </div>
    <div id="noExistDialogButtonPanel">
    <input type="button" id="noExistDialogOkButton" class="k-button" value="<s:property value="getText('ok')"/>" />
    </div>
</div>