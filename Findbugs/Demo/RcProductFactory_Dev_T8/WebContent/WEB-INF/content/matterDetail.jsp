<%@ page language="java" contentType="text/html; charset=Shift-JIS"
    pageEncoding="Shift-JIS"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<tiles:insertDefinition name="layoutCommon">
<tiles:putAttribute name="css">
    <link rel="stylesheet" type="text/css" href="css/jQuery/kendo.ext.css?20140804140000" />
    <link rel="stylesheet" type="text/css" href="css/jQuery/kendo.common.min.css?20140804140000" />
    <link rel="stylesheet" type="text/css" href="css/matterDetail.css?20140804140000" />
    
<!--     <link rel="stylesheet" href="css/bootstrap.css"> -->
    <link rel="stylesheet" href="css/component/popupUpload.css">
    <link rel="stylesheet" href="css/jquery.fileupload.css">
    <link rel="stylesheet" href="css/jquery.fileupload-ui.css">
</tiles:putAttribute>
<tiles:putAttribute name="javaScript">
    <script type="text/javascript" src="js/jQuery/kendo.all.min.js?20140804140000"></script>
    <script type="text/javascript" src="js/jQuery/kendo.web.ext.js?20140804140000" charset="UTF-8"></script>
    <script type="text/javascript" src="js/jQuery/kendo.culture.ja-JP.min.js?20140804140000" charset="UTF-8"></script>
    <script type="text/javascript" src="js/jQuery/jquery.validate.min.js?20140804140000"></script>
    <script type="text/javascript" src="js/matterDetailJS.js?20140804140000"></script>
    <script type="text/javascript" src="js/commonJS.js?20140804140000"></script>

</tiles:putAttribute>

<tiles:putAttribute name="body">
<!-- Contents -->
<div id="contents">
    <script type="text/javascript" src="js/jQuery/selectable.js?20140804140000"></script>
    <script type="text/javascript" src="js/jQuery/kendo.custom.upload.js?20140804140000"></script>
    <div id="content">
        <s:if test="%{matterDetailActionForm.message != null && matterDetailActionForm.message != ''}">
                 <div id="message" class="has-message success" style="margin-bottom: 5px;">
                     <s:property value="matterDetailActionForm.message"/>
                 </div>
         </s:if>
         <s:else>
            <div id="message">
            </div>
         </s:else>
    <!-- message -->
    <input id='noDataMessage'                       type="hidden" value='<s:property value="getText('text.error.noData')"/>'>
    <input id='releasedProductNotDelete'            type="hidden" value='<s:property value="getText('releasedProductNotDelete')"/>'>
    <input id='maximumImportProduct'                type="hidden" value='<s:property value="getText('message.error.maximumImportProduct')"/>'>
    <input id='maximumImportMatter'                 type="hidden" value='<s:property value="getText('message.error.maximumImportMatter')"/>'>
    <input id='invalidFormatCSVMess'                type="hidden" value='<s:property value="getText('csvInvalidFormat')"/>'>
    
    <!-- boe thai.son bug 61 -->
    <%-- <input id='noSelectedProduct'                  type="hidden" value='<s:property value="getText('message.error.noSelectedProduct')"/>'> --%>
    <!-- eoe thai.son bug 61 -->
    <!-- boe thai.son bug 61 -->
    <%-- <input id='content-confirm-deleteProducts'         type="hidden"   value='<s:property value="getText('message.confirm.deleteProducts')"/>'> --%>
    <!-- eoe thai.son -->
    <input id='btn-yes'                             type="hidden"   value='<s:property value="getText('message.confirm.yes')"/>'>
    <input id='btn-no'                              type="hidden"   value='<s:property value="getText('message.confirm.no')"/>'>
    
    <!-- hidden for tooltip  -->
    <input id='matterIssueSubject'                  type="hidden" value='<s:property value="getText('text.label.matterIssueSubject')"/>'>
    <input id='matterAssignedTo'                    type="hidden" value='<s:property value="getText('text.label.matterAssignedTo')"/>'>
    <input id='matterStartDate'                     type="hidden" value='<s:property value="getText('text.label.matterStartDate')"/>'>
    <input id='matterDueDate'                       type="hidden" value='<s:property value="getText('text.label.matterDueDate')"/>'>
    <!----------------------------------------------------------- hidden for gridMode1 --------------------------------------------------------------- -->
    <input id='headerupdateMode'                    type="hidden" value='<s:property value="getText('updateMode')"/>'>
    <input id='headerproductId'                     type="hidden" value='<s:property value="getText('identificationNumber')"/>'>
    <input id='headerproductSyouhinSysCode'         type="hidden" value='<s:property value="getText('systemProductCode')"/>'>
    <input id='headerproductBrandCode'                  type="hidden" value='<s:property value="getText('brandCode')"/>'>
    <input id='headerproductCategoryCode'                  type="hidden" value='<s:property value="getText('itemCode2')"/>'>
    <input id='headerproductName'                  type="hidden" value='<s:property value="getText('productName')"/>'>
    <input id='headerproductCode'                  type="hidden" value='<s:property value="getText('makerStockNumber')"/>'>
    <input id='headerproductWebikeCodeFlg'                  type="hidden" value='<s:property value="getText('partNoFlag')"/>'>
    <input id='headerproductEanCode'                  type="hidden" value='<s:property value="getText('headerproductEanCode')"/>'>
    <input id='headerproductProperPrice'                  type="hidden" value='<s:property value="getText('listPrice')"/>'>
    <input id='headersupplierPricePrice'                  type="hidden" value='<s:property value="getText('division')"/>'>
    <input id='headerproductGroupCode'                  type="hidden" value='<s:property value="getText('groupCode')"/>'>
    <input id='headerproductSupplierReleaseDate'                  type="hidden" value='<s:property value="getText('dateOfIssue')"/>'>
    <input id='headerproductOpenPriceFlg'                  type="hidden" value='<s:property value="getText('openPrice')"/>'>
    <input id='headerproductProperSellingFlg'                  type="hidden" value='<s:property value="getText('salePrice')"/>'>
    <input id='headerproductOrderProductFlg'                  type="hidden" value='<s:property value="getText('buildToOrderManufacturing')"/>'>
    <input id='headerproductNoReturnableFlg'                  type="hidden" value='<s:property value="getText('nonReturnable')"/>'>
    <input id='headerproductAmbiguousImageFlg'                  type="hidden" value='<s:property value="getText('preferenceImage')"/>'>
    <input id='headerdescriptionSummary'                  type="hidden" value='<s:property value="getText('summary')"/>'>
    <input id='headerdescriptionRemarks'                  type="hidden" value='<s:property value="getText('note')"/>'>
    <input id='headerdescriptionCaution'                  type="hidden" value='<s:property value="getText('caution')"/>'>
    <input id='headerdescriptionSentence'                  type="hidden" value='<s:property value="getText('explanation')"/>'>
    <input id='headersiireCode1'                  type="hidden" value='<s:property value="getText('supplierCode')"/>1'>
    <input id='headernoukiCode1'                  type="hidden" value='<s:property value="getText('deliveryTimeCode')"/>1'>
    <input id='headersiireCode2'                  type="hidden" value='<s:property value="getText('supplierCode')"/>2'>
    <input id='headernoukiCode2'                  type="hidden" value='<s:property value="getText('deliveryTimeCode')"/>2'>
    <input id='headercompatibleModel'                  type="hidden" value='<s:property value="getText('compatibleModel')"/>'>
    <input id='headerproductImageThumbnailPath'                  type="hidden" value='<s:property value="getText('thumbnail')"/>'>
    <input id='headerproductImageDetailPath'                  type="hidden" value='<s:property value="getText('detailedImage')"/>'>
    
    <input id='headerselectCode'                  type="hidden" value='<s:property value="getText('lookupCode')"/>'>
    <input id='headerselectName'                  type="hidden" value='<s:property value="getText('optionName')"/>'>
    <input id='headercustomersConfirmationItem'                  type="hidden" value='<s:property value="getText('customerConfirmationItem')"/>'>
    <input id='headerlink'                  type="hidden" value='<s:property value="getText('link')"/>'>
    <input id='headeranimation'                  type="hidden" value='<s:property value="getText('video')"/>'>
    <input id='headerPreviewProduct'              type="hidden" value='<s:property value="getText('previewProduct')"/>'>
    <!--------------------------------- end hidden for gridMode1 ---------------------------------------- -->
    <!-- end hidden for tooltip  -->
    
    <input id='btnSearchIssueHidden'                type="hidden" value='<s:property value="getText('text.label.btnSearchIssue')"/>'>
    <input id='btnChangeIssueHidden'                type="hidden" value='<s:property value="getText('text.label.btnChangeIssue')"/>'>
    <!-- Header of grid -->
    <input id='headerProductId'                     type="hidden" value='<s:property value="getText('text.label.matterProductCode')"/>'>
    <input id='headerProductSyouhinSysCode'         type="hidden" value='<s:property value="getText('text.label.matterModelCode')"/>'>
    <input id='headerProductBrandCode'              type="hidden" value='<s:property value="getText('text.label.matterBrandCode')"/>'>
    <input id='headerBrandName'                     type="hidden" value='<s:property value="getText('text.label.matterBrandName')"/>'>
    <input id='headerProductName'                   type="hidden" value='<s:property value="getText('productName')"/>'>
    <input id='headerProductItem'                   type="hidden" value='<s:property value="getText('text.label.matterProductItem')"/>'>
    <input id='headerProductEanCode'                type="hidden" value='<s:property value="getText('text.label.matterProductEAN')"/>'>
    <input id='headerShowProductRegistrationFlg'    type="hidden" value='<s:property value="getText('text.label.matterProductRegistrationFlg')"/>'>
    <input id='headerProductGeneralErrorFlg'        type="hidden" value='<s:property value="getText('text.label.matterProductBasicError')"/>'>
    <input id='headerProductCategoryErrorFlg'       type="hidden" value='<s:property value="getText('text.label.matterProductItemError')"/>'>
    <input id='headerProductModelErrorFlg'          type="hidden" value='<s:property value="getText('text.label.matterProductModelError')"/>'>
    <input id='headerProductAttributeErrorFlg'      type="hidden" value='<s:property value="getText('text.label.matterProductAttrError')"/>'>
    <input id='headerCreatedUserId'                 type="hidden" value='<s:property value="getText('text.label.matterProductUser')"/>'>
    <input id='headerUpdatedOn'                     type="hidden" value='<s:property value="getText('text.label.matterUpdateOn')"/>'>
    <!-- Title for paging -->
    <input id='pagingDisplay'                       type="hidden" value='<s:property value="getText('text.paging.display')"/>'>
    <input id='pagingEmpty'                         type="hidden" value='<s:property value="getText('text.paging.empty')"/>'>
    <input id='pagingItemsPerPage'                  type="hidden" value='<s:property value="getText('text.paging.itemsPerPage')"/>'>
    <input id='pagingFirst'                         type="hidden" value='<s:property value="getText('text.paging.first')"/>'>
    <input id='pagingPrevious'                      type="hidden" value='<s:property value="getText('text.paging.previous')"/>'>
    <input id='pagingNext'                          type="hidden" value='<s:property value="getText('text.paging.next')"/>'>
    <input id='pagingLast'                          type="hidden" value='<s:property value="getText('text.paging.last')"/>'>
    <!-- Filter type for grid -->
    <input id='hiddenRegistrationFlg'               type="hidden" value='<s:select id="cbbRegistrationFlg"
                                                                cssClass="cbbFilter" 
                                                                headerKey=" " 
                                                                headerValue="" 
                                                                list="matterDetailActionForm.listRegistrationFlg"/>'>
    <input id='hiddenListUser'                      type="hidden" value='<s:select id="tbUpdatedUserId" 
                                                                cssClass="cbbFilter"
                                                                headerKey=" " headerValue="" 
                                                                list="commonForm.listCommonUser"
                                                                listKey="userId"
                                                                listValue="%{userLastName + ' ' + userFirstName}"/>'>
    <input id='errorCountEqual1'                    type="hidden" value='<s:property value="getText('text.label.errorCountEqual1')"/>'>
    <input id='errorCountEqual0'                    type="hidden" value='<s:property value="getText('text.label.errorCountEqual0')"/>'>
    <input id='syouhinCodeEqual0'                   type="hidden" value='<s:property value="getText('text.label.syouhinEqual0')"/>'>
    <input id='redRegistrationFlg'                  type="hidden" value='<s:property value="getText('text.label.redRegistrationFlg')"/>'>
    <input id='grayRegistrationFlg'                 type="hidden" value='<s:property value="getText('text.label.grayRegistrationFlg')"/>'>
    <input id='otherUpdate'                         type="hidden" value='<s:property value="getText('message.error.othersUpdateForJS')"/>'>
    <input id='deleteSuccess'                       type="hidden" value='<s:property value="getText('message.error.deleteSuccess')"/>'>
    <input id='updateSuccess'                       type="hidden" value='<s:property value="getText('message.success.updateSuccess')"/>'>
    <input id='updateFail'                          type="hidden" value='<s:property value="getText('message.error.updateFail')"/>'>
    <input id='noDataMessage'                       type="hidden" value='<s:property value="getText('text.error.noData')"/>'>
    <input id='requiredFill'                        type="hidden"   value='<s:property value="getText('message.error.requiredFill')"/>'>
    <input id='unChange'                            type="hidden"   value='<s:property value="getText('message.error.unChange')"/>'>
    <input id='outOfRange'                          type="hidden"   value='<s:property value="getText('message.error.inputOutOfRange')"/>'>
    <input id='maxProductOfMatter'                  type="hidden"   value='<s:property value="getText('maxProductOfMatter')"/>'>
    <input id='confirmYes'                          type="hidden" value='<s:property value="getText('message.confirm.yes')"/>'>
    <input id='deleteFail'                          type="hidden" value='<s:property value="getText('message.error.deleteFail')"/>'>
    
    <!-- Old data -->
    <input id='oldToolTip'                          type="hidden"   value='<s:property value="matterDetailActionForm.entMstFactoryMatterNew.matterRedmineIssueId"/>'>
    <input id='oldMatterName'                       type="hidden"   value='<s:property value="matterDetailActionForm.entMstFactoryMatterNew.matterName"/>'>
    <input id='oldChangeUser'                       type="hidden"   value='<s:property value="matterDetailActionForm.entMstFactoryMatterNew.matterChargeUserId"/>'>
    <!-- Role -->
    <input id='role'                                type="hidden"   value='<s:property value="commonForm.loginAuthority"/>'>
    <!-- Max product export in each time -->
    <input id='maxProductExportCSVErrorMsg'         type="hidden" value='<s:property value="getText('exportCSVOverMaxProductError')"/>'>
    <input id='maxNumberProductExportCSV'           type="hidden" value='<s:property value="maxNumberProductExportCSV"/>'>
    <!-- CSV data -->
    <input id='csv_popup_title'                     type="hidden"   value='<s:property value="getText('csvImport')"/>'>
    
        <!-- start title Management of Brand Master -->
        <div id="title">
            <h1 class="list-unstyled" style="margin-top: 0px;"><s:property value="getText('text.label.matterDetailH1')"/></h1>
        </div>
        <!-- end title Management of Brand Master -->
        <!-- start list button div -->

        <div id="subTitle">
            <h2 class="list-unstyled"><s:property value="getText('text.label.matterDetailH1')"/></h2>
            <h2 class="list-unstyled" id="headerName" title="<s:property value="matterDetailActionForm.entMstFactoryMatterNew.matterName"/>">"<s:property value="matterDetailActionForm.entMstFactoryMatterNew.matterName"/>"</h2> 
            <button type="button" id="btnReset" class="k-button">
                <s:property value="getText('text.btn.reset')"/>
            </button>
            <button type="button" id="btnAddNew" class="k-button" onclick="clickBack();">
                <s:property value="getText('text.label.btnBackToPreviousScreen')"/>
            </button>
            <button type="button" id="btnimageBrowser" class="k-button">
                <s:property value="getText('imageManagement')"/>
            </button>
            <button type="button" id="btnNewProduct" class="k-button">
               <s:property value="getText('newProduct')"/>
            </button>
        </div>
        <!-- end list button div -->
        <!-- start contents_inner -->
        <div id="contentsInner">
            <!-- start table list master -->
            <div class="headerContent">
                <label><s:property value="getText('text.label.matterDetailCase1')" /></label>
            </div>
            <!-- start list button div -->
            <div>
            <div style="padding-bottom: 5px;"></div>
                <div id="vertical">
                    <div id="top-pane">
                        <div id="horizontal" style="height: 100%; width: 100%;">
                            <div id="left-pane">
                                <div class="pane-content" style="padding:5px;">
                                    <fieldset>
                                      <legend style="text-align: left;"><s:property value="getText('text.label.matterDetailCase1')" /></legend>
                                      <ul>
                                          <li class="row_legend">
                                              <div class="labelColumnLeft"><s:property value="getText('text.label.matterDetailDesign')" /></div>
                                              <div class="content_legend">
                                                    <span id="matterNo"><s:property value="matterDetailActionForm.entMstFactoryMatterNew.matterNo" /></span>
                                                    <div id="delFlgCheckbox">
                                                        <input type="checkbox"
                                                            id="delFlag"
                                                            name="matterDetailActionForm.entMstFactoryMatterNew.matterDelFlg"
                                                            value='<s:property value="matterDetailActionForm.entMstFactoryMatterNew.matterDelFlg"/>'>
                                                        <div><s:property value="getText('text.label.matterDetailDelFlg')" /></div>
                                                    </div>
                                                </div>   
                                          </li>
                                          <li class="row_legend">
                                              <div class="labelColumnLeft"><span id="lblTooltip"><s:property value="getText('text.label.matterDetailIssueNumber')" /></span></div>
                                              <div class="content_legend">
                                                  <a href="linkToDetailIssueRedmine.html?issueId=<s:property value="matterDetailActionForm.entMstFactoryMatterNew.matterRedmineIssueId"></s:property>" target="_blank" id="tooltip" onmouseover="showTooltip()">#<s:property value="matterDetailActionForm.entMstFactoryMatterNew.matterRedmineIssueId"></s:property></a>
                                                  <button type="button" class="k-button" id="btnChangeIssue" onclick="handleBtnGetIssue();">
                                                      <s:property value="getText('text.label.btnChangeIssue')"/>
                                                  </button>
                                                  <span id="spnTooltipError" class="error"></span>
                                              </div>    
                                          </li>
                                          <li class="row_legend">
                                              <div class="labelColumnLeft"><span id="lblMatterName"><s:property value="getText('text.label.projectName')"/></span>
                                                <span class="error">*</span>
                                              </div>
                                              <div class="content_legend">
                                                  <input id="matterName" maxlength="256"
                                                    value='<s:property value="matterDetailActionForm.entMstFactoryMatterNew.matterName"/>' class="k-input k-textbox" type="text" />
                                                  <span id="spnMatterNameError" class="error"></span>
                                              </div>
                                          </li>
                                          <li class="row_legend" id="row-list-user">
                                              <div class="labelColumnLeft"><s:property value="getText('text.label.userAssigne')"/></div>
                                              <!-- start list button div -->
                                              <div class="content_legend">
                                                        <s:select disabled=""
                                                        id="lstUser"
                                                        list="commonForm.listCommonUser"
                                                        listKey="userId"
                                                        listValue="%{userLastName + ' ' + userFirstName}"
                                                        value="%{matterDetailActionForm.entMstFactoryMatterNew.matterChargeUserId}"
                                                        name="matterDetailActionForm.entMstFactoryMatterNew.updatedUserId"
                                                        cssClass="cbb-size-normal tantouId1" />
                                              </div>
                                              <input type="hidden" id="oldUpdatedOn" value='<s:property value="matterDetailActionForm.entMstFactoryMatterNew.updatedOnString"/>' />
                                          </li>
                                          <li class="row_legend">
                                              <button type="button" id="btnUpdate" class="k-button">
                                                  <s:property value="getText('text.label.btnUpdateMatterDetail')"/>
                                              </button>
                                          </li>
                                      </ul>
                                    </fieldset>
                                </div>
                            </div>
                            <div id="right-pane">
                                <div class="pane-content" style="padding:5px;">
                                    <fieldset>
                                      <legend style="text-align: left;"><s:property value="getText('text.label.infomationProduct')"/></legend>
                                      <ul>
                                          <li class="row_legend">
                                              <label class="count-title"><s:property value="getText('text.label.countErrorProduct')"/></label>
                                              <label class="count-value"><s:property value="putComma(matterDetailActionForm.entMstFactoryMatterNew.matterFactoryCount)" /></label>
                                          </li>
                                          <li class="row_legend">
                                              <label style="float:left"><s:property value="getText('text.label.ErrorCountGeneral')"/></label>
                                              <s:set value="matterDetailActionForm.entMstFactoryMatterNew.matterFactoryErrorCountGeneral" var="general"></s:set>
                                              <label class="count-value" <s:if test="#general > 0">style="color: red;"</s:if> ><s:property value="putComma(matterDetailActionForm.entMstFactoryMatterNew.matterFactoryErrorCountGeneral)" /></label>
                                          </li>
                                          <li class="row_legend">
                                              <label class="count-title"><s:property value="getText('text.label.ErrorCountCategory')"/></label>
                                              <s:set value="matterDetailActionForm.entMstFactoryMatterNew.matterFactoryErrorCountCategory" var="category"></s:set>
                                              <label class="count-value" <s:if test="#category > 0">style="color: red;"</s:if> ><s:property value="putComma(matterDetailActionForm.entMstFactoryMatterNew.matterFactoryErrorCountCategory)" /></label>
                                          </li>
                                          <li class="row_legend">
                                              <label class="count-title"><s:property value="getText('text.label.ErrorCountModel')"/></label>
                                              <s:set value="matterDetailActionForm.entMstFactoryMatterNew.matterFactoryErrorCountModel" var="model"></s:set>
                                              <label class="count-value" <s:if test="#model > 0">style="color: red;"</s:if> ><s:property value="putComma(matterDetailActionForm.entMstFactoryMatterNew.matterFactoryErrorCountModel)" /></label>
                                          </li>
                                          <li class="row_legend">
                                              <label class="count-title"><s:property value="getText('text.label.ErrorCountAttribute')"/></label>
                                              <s:set value="matterDetailActionForm.entMstFactoryMatterNew.matterFactoryErrorCountAttribute" var="attribute"></s:set>
                                              <label class="count-value" <s:if test="#attribute > 0">style="color: red;"</s:if> ><s:property value="putComma(matterDetailActionForm.entMstFactoryMatterNew.matterFactoryErrorCountAttribute)" /></label>
                                          </li>
                                      </ul>
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <div style="border-bottom: 1px solid #C9C9C9; padding-top: 5px;"></div>
            </div>
            <br/>
            <!-- start table list master -->
            <div class="headerContent">
                <label><s:property value="getText('text.label.infomationProduct')" /></label>
            </div>
            <!-- start list button div -->
            <div id="lstButton">
                <label id='lblCheckBoxAll'>
                    <input type='checkbox' id='checkBoxAll' class='productCheckbox' onclick='selectAllCheckbox();'/><s:property value="getText('text.label.selectAllCheckBox')" />
                </label>
                
                <label><s:property value="getText('text.label.btnComboxActiveProduct')"/></label>&nbsp;&nbsp;
                <select name="statusProcessProduct" id="size">
                    <option value="default"></option>
                    <option value="0"><s:property value="getText('text.label.btnComboxActiveDelete')"/></option>
                    <option value="1"><s:property value="getText('text.label.btnComboxActiveRelease')"/></option>
                    <option value="2"><s:property value="getText('text.label.btnComboxActiveCanrelease')"/></option>
                    <option value="3"><s:property value="getText('csvProcessing')"/></option>
                    <%-- BOE @rcv!Nguyen.Chuong 2014/08/01: comment last action follow request of Yamashita--%>
<%--                    <option value="4"><s:property value="getText('checkProductInformationError')"/></option> --%>
                    <%-- EOE @rcv!Nguyen.Chuong 2014/08/01: comment last action follow request of Yamashita--%>
                </select>
                <input type="button" class="btn-color-default lstButton k-button" id="btnExcute" value="<s:property value="getText('text.label.excuteProduct')"/>">
            </div>
            <!-- end list button div -->
            <div id="mainContent" class="tbl-content">
                <!-- start table list master -->
                <div id="table-content">
                    <!-- start table list master content -->
                    <div id="grid">
                    </div>
                    <!-- end table list master content -->
                </div>
                <!-- end table list master -->
            </div>
            <!-- end table list master -->
        </div>
        <!-- BOE 2014/01/29 Nguyen.Chuong add form to submit to fitModelCheck page. -->
        <form id="submitForm" action="fitModelCheck.html" method="post">
            <input type="hidden" id="productIdSubmitParam" name="productId">
        </form>
        <!-- EOE 2014/01/29 Nguyen.Chuong add form to submit to fitModelCheck page. -->
    </div>
    
    <!-- BOE 2014/02/18 Triet.Nguyen - Add Confirm Back Dialog -->
    <!-- Start Kendo Dialog -->
     <div id="confirmBackDialog">
        <div>
            <p><s:property value="getText('message.confirm.backToPrevious')"/></p>
            <div style='text-align:center;padding:5px;'>
                <input id='fancyConfirm_ok' class='k-button' type='button'
                      value='<s:property value="getText('message.confirm.yes')"/>' onclick='backToPreviousPage()' style='margin:7px;padding:5px;width:67px;'>
                <input id='fancyConfirm_cancel' class='k-button' type='button'
                      value='<s:property value="getText('message.confirm.no')"/>' style='margin:7px;padding:5px;width:67px;'>
           </div>
        </div>
     </div>
     <!-- boe thai.son -->
     <div id="confirmDeleteDialog">
        <input type="hidden" id="windowNamedeleteDialog" value="<s:property value="getText('windowName')"/>" />
        <div>
            <p><s:property value="getText('message.confirm.deleteProducts')"/></p>
            <div style='text-align:center;padding:5px;'>
                <input id='confirmDelete_ok' class='k-button' type='button'
                      value='<s:property value="getText('message.confirm.yes')"/>'  style='margin:7px;padding:5px;width:67px;'>
                <input id='confirmDelete_cancel' class='k-button' type='button'
                      value='<s:property value="getText('message.confirm.no')"/>' style='margin:7px;padding:5px;width:67px;'>
           </div>
        </div>
     </div>
        <div id="pleaseSelectDialog" >
            <input type="hidden" id="windowNamepleaseSelectDialog" value="<s:property value="getText('windowName')"/>" />
            <div class="matterDetailDialogContainer">
                <div class="matterDetailDialogContent" style="color:red;">
                    <s:property value="getText('message.error.noSelectedProduct')"/>
                </div>
                <input type="button" class="k-button" id="pleaseSelectDialogOk" 
                    value='<s:property value="getText('text.btn.yes')"/>' />
            </div>
        </div>
        <!-- eoe thai.son -->
     <!--EOE 2014/02/18 Triet.Nguyen - Add Confirm Back Dialog -->
     <!-- start popup CSV -->
     <div id="popup_csv" class="popup-csv">
        <div id="tabstrip">
            <ul>
                <li class="k-state-active">
                    <s:property value="getText('csvExport')"/>
                </li>
                <li>
                    <s:property value="getText('csvImport')"/>
                </li>
            </ul>
            <div id="csv_popup_export">
                <div id="export_header">
                    <label><s:property value="getText('selectCsvMode')"/></label>
                    <select name="selectCsvMode" id="csvmode" class="cbb-size-normal">
                        <!-- <option value="default"></option> -->
                        <option value="1"><s:property value="getText('allData')"/></option>
                        <option value="2"><s:property value="getText('compatibleModel')"/></option>
                        <option value="3"><s:property value="getText('attribute')"/></option>
                    </select>
                    <label id="msPleaseSelectDialog" style="display:none;margin-left: 5px;color: red;">
                    <s:property value="getText('csvProductNotSelected')"/></label>
                    <input type="button" class="btn-color-default lstButton k-button" id="btnCSVStartExport" 
                        value="<s:property value="getText('csvStartExport')"/>"
                        style="float: right;width: 100px;height: 25px;">
                </div>
                <div class="pane-content">
                    <fieldset id = "exportItems" >
                      <legend style="text-align: left;"><input type='checkbox' id="exportItemsCheckBoxAll" ><s:property value="getText('csvOutputItemLabel2')" /></legend>
                      <ul id='csv_ul_mod_1' style="float:left;">
                          <li class="row_legend li_control">
                            <div class="control">
                                <label><input type='checkbox' id='updateMode' />
                                <s:property value="getText('updateMode')" /></label>
                            </div>
                            <div class="control">
                                <label><input type='checkbox' id='image1' />
                                <s:property value="getText('image1')" /></label>
                            </div>
                            <div class="control">
                                <label><input type='checkbox' id='summary' />
                                <s:property value="getText('summary')" /></label>
                            </div>
                          </li>
                          <li class="row_legend li_control">
                            <div class="control">
                                <label><input type='checkbox' class="chkDisabled" />
                                <s:property value="getText('identificationNumber')" /></label>
                            </div>
                            <div class="control">
                                <label><input type='checkbox' id='image2' />
                                <s:property value="getText('image2')" /></label>
                            </div>
                            <div class="control">
                                <label><input type='checkbox' id='note' />
                                <s:property value="getText('note')" /></label>
                            </div>
                          </li>
                          <li class="row_legend li_control">
                            <div class="control">
                                <label><input type='checkbox' class="chkDisabled" />
                                <s:property value="getText('systemProductCode')" /></label>
                            </div>
                            <div class="control">
                                <label><input type='checkbox' id='image3' />
                                <s:property value="getText('image3')" /></label>
                            </div>
                            <div class="control">
                                <label><input type='checkbox' id='caution' />
                                <s:property value="getText('caution')" /></label>
                            </div>
                          </li>
                          <li class="row_legend li_control">
                            <div class="control">
                                <label><input type='checkbox' id='brandCode' />
                                <s:property value="getText('brandCode')" /></label>
                            </div>
                            <div class="control">
                                <label><input type='checkbox' id='image4' />
                                <s:property value="getText('image4')" /></label>
                            </div>
                            <div class="control">
                                <label><input type='checkbox' id='explanation' />
                                <s:property value="getText('explanation')" /></label>
                            </div>
                          </li>
                          <li class="row_legend li_control">
                            <div class="control">
                                <label><input type='checkbox' id='brandName' />
                                <s:property value="getText('brandName')" /></label>
                            </div>
                            <div class="control">
                                <label><input type='checkbox' id='image5' />
                                <s:property value="getText('image5')" /></label>
                            </div>
                            <div class="control"></div>
                          </li>
                          <li class="row_legend li_control">
                            <div class="control">
                                <label><input type='checkbox' id='itemCode2' />
                                <s:property value="getText('itemCode2')" /></label>
                            </div>
                            <div class="control">
                                <label><input type='checkbox' id='image6' />
                                <s:property value="getText('image6')" /></label>
                            </div>
                            <div class="control">
                                <label><input type='checkbox' id='supplierCode1' />
                                <s:property value="getText('supplierCode1')" /></label>
                            </div>
                          </li>
                          <li class="row_legend li_control">
                            <div class="control">
                                <label><input type='checkbox' id='itemName' />
                                <s:property value="getText('itemName')" /></label>
                            </div>
                            <div class="control">
                                <label><input type='checkbox' id='image7' />
                                <s:property value="getText('image7')" /></label>
                            </div>
                            <div class="control">
                                <label><input type='checkbox' id='supplierName1' />
                                <s:property value="getText('supplierName1')" /></label>
                            </div>
                          </li>
                          <li class="row_legend li_control">
                            <div class="control">
                                <label><input type='checkbox' id='productName' />
                                <s:property value="getText('productName')" /></label>
                            </div>
                            <div class="control">
                                <label><input type='checkbox' id='image8' />
                                <s:property value="getText('image8')" /></label>
                            </div>
                            <div class="control">
                                <label><input type='checkbox' id='supplierCode2' />
                                <s:property value="getText('supplierCode2')" /></label>
                            </div>
                          </li>
                          <li class="row_legend li_control">
                            <div class="control">
                                <label><input type='checkbox' id='makerStockNumber' />
                                <s:property value="getText('makerStockNumber')" /></label>
                            </div>
                            <div class="control">
                                <label><input type='checkbox' id='image9' />
                                <s:property value="getText('image9')" /></label>
                            </div>
                            <div class="control">
                                <label><input type='checkbox' id='supplierName2' />
                                <s:property value="getText('supplierName2')" /></label>
                            </div>
                          </li>
                          <li class="row_legend li_control">
                            <div class="control">
                                <label><input type='checkbox' id='notStockNumber' />
                                <s:property value="getText('notStockNumber')" /></label>
                            </div>
                            <div class="control">
                                <label><input type='checkbox' id='image10' />
                                <s:property value="getText('image10')" /></label>
                            </div>
                            <div class="control"></div>
                          </li>
                          <li class="row_legend li_control">
                            <div class="control">
                                <label><input type='checkbox' id='jan' />
                                <s:property value="getText('jan')" /></label>
                            </div>
                            <div class="control"></div>
                            <div class="control">
                                <label><input type='checkbox' id='supportedBike' />
                                <s:property value="getText('supportedBike')" /></label>
                            </div>
                          </li>
                          <li class="row_legend li_control">
                            <div class="control">
                                <label><input type='checkbox' id='listPrice' />
                                <s:property value="getText('listPrice')" /></label>
                            </div>
                            <div class="control">
                                <label><input type='checkbox' id='option1' />
                                <s:property value="getText('option1')" /></label>
                            </div>
                            <div class="control"></div>
                          </li>
                          <li class="row_legend li_control">
                            <div class="control">
                                <label><input type='checkbox' id='division' />
                                <s:property value="getText('division')" /></label>
                            </div>
                            <div class="control">
                                <label><input type='checkbox' id='option2' />
                                <s:property value="getText('option2')" /></label>
                            </div>
                            <div class="control">
                                <label><input type='checkbox' id='customerConfirmationItem' />
                                <s:property value="getText('customerConfirmationItem')" /></label>
                            </div>
                          </li>
                          <li class="row_legend li_control">
                            <div class="control">
                                <label><input type='checkbox' id='groupCode' />
                                <s:property value="getText('groupCode')" /></label>
                            </div>
                            <div class="control">
                                <label><input type='checkbox' id='option3' />
                                <s:property value="getText('option3')" /></label>
                            </div>
                            <div class="control"></div>
                          </li>
                          <li class="row_legend li_control">
                            <div class="control">
                                <label><input type='checkbox' id='dateOfIssue' />
                                <s:property value="getText('dateOfIssue')" /></label>
                            </div>
                            <div class="control">
                                <label><input type='checkbox' id='option4' />
                                <s:property value="getText('option4')" /></label>
                            </div>
                            <div class="control">
                                <label><input type='checkbox' id='link' />
                                <s:property value="getText('link')" /></label>
                            </div>
                          </li>
                          <li class="row_legend li_control">
                            <div class="control">
                                <label><input type='checkbox' id='salePrice' />
                                <s:property value="getText('salePrice')" /></label>
                            </div>
                            <div class="control">
                                <label><input type='checkbox' id='option5' />
                                <s:property value="getText('option5')" /></label>
                            </div>
                            <div class="control">
                                <label><input type='checkbox' id='video' />
                                <s:property value="getText('video')" /></label>
                            </div>
                          </li>
                          <li class="row_legend li_control">
                            <div class="control">
                                <label><input type='checkbox' id='nonReturnable' />
                                <s:property value="getText('nonReturnable')" /></label>
                            </div>
                            <div class="control">
                                <label><input type='checkbox' id='option6' />
                                <s:property value="getText('option6')" /></label>
                            </div>
                            <div class="control"></div>
                          </li>
                          <li class="row_legend li_control">
                            <div class="control">
                                <label><input type='checkbox' id='buildToOrderManufacturing' />
                                <s:property value="getText('buildToOrderManufacturing')" /></label>
                            </div>
                            <div class="control">
                                <label><input type='checkbox' id='option7' />
                                <s:property value="getText('option7')" /></label>
                            </div>
                            <div class="control"></div>
                          </li>
                          <li class="row_legend li_control">
                            <div class="control">
                                <label><input type='checkbox' id='openPrice' />
                                <s:property value="getText('openPrice')" /></label>
                            </div>
                            <div class="control">
                                <label><input type='checkbox' id='option8' />
                                <s:property value="getText('option8')" /></label>
                            </div>
                            <div class="control"></div>
                          </li>
                          <li class="row_legend li_control">
                            <div class="control">
                                <label><input type='checkbox' id='preferenceImage' />
                                <s:property value="getText('preferenceImage')" /></label>
                            </div>
                            <div class="control">
                                <label><input type='checkbox' id='option9' />
                                <s:property value="getText('option9')" /></label>
                            </div>
                            <div class="control"></div>
                          </li>
                          <li class="row_legend li_control">
                            <div class="control"></div>
                            <div class="control">
                                <label><input type='checkbox' id='option10' />
                                <s:property value="getText('option10')" /></label>
                            </div>
                            <div class="control"></div>
                          </li>
                      </ul>
                      <ul id='csv_ul_mod_2' style="float:left;display: none;">
                          <li class="row_legend li_control">
                            <div class="control">
                                <label><input type='checkbox' />
                                <s:property value="getText('updateMode')" /></label>
                            </div>
                          </li>
                          <li class="row_legend li_control">
                            <div class="control">
                                <label><input type='checkbox' />
                                <s:property value="getText('identificationNumber')" /></label>
                            </div>
                          </li>
                          <li class="row_legend li_control">
                            <div class="control">
                                <label><input type='checkbox' />
                                <s:property value="getText('systemProductCode')" /></label>
                            </div>
                          </li>
                          <li class="row_legend li_control">
                            <div class="control">
                                <label><input type='checkbox' />
                                <s:property value="getText('maker')" /></label>
                            </div>
                          </li>
                          <li class="row_legend li_control">
                            <div class="control">
                                <label><input type='checkbox' />
                                <s:property value="getText('carModel')" /></label>
                            </div>
                          </li>
                          <li class="row_legend li_control">
                            <div class="control">
                                <label><input type='checkbox' />
                                <s:property value="getText('bikeModel')" /></label>
                            </div>
                          </li>
                        </ul>
                        <ul id='csv_ul_mod_3' style="float:left;display: none;">
                          <li class="row_legend li_control">
                            <div class="control">
                                <label><input type='checkbox' />
                                <s:property value="getText('updateMode')" /></label>
                            </div>
                          </li>
                          <li class="row_legend li_control">
                            <div class="control">
                                <label><input type='checkbox' />
                                <s:property value="getText('identificationNumber')" /></label>
                            </div>
                          </li>
                          <li class="row_legend li_control">
                            <div class="control">
                                <label><input type='checkbox' />
                                <s:property value="getText('systemProductCode')" /></label>
                            </div>
                          </li>
                          <li class="row_legend li_control">
                            <div class="control">
                                <label><input type='checkbox' />
                                <s:property value="getText('atributeName')" /></label>
                            </div>
                          </li>
                          <li class="row_legend li_control">
                            <div class="control">
                                <label><input type='checkbox' />
                                <s:property value="getText('administrativeName')" /></label>
                            </div>
                          </li>
                          <li class="row_legend li_control">
                            <div class="control">
                                <label><input type='checkbox' />
                                <s:property value="getText('displayName')" /></label>
                            </div>
                          </li>
                         </ul>
                      <fieldset style="width: 225px;float: right;" id='field_csv_export_attr'>
                        <legend style="text-align: left;"><s:property value="getText('attribute')" /></legend>
                       <s:select list="matterDetailActionForm.entMstAttributeList" id="csv_export_attr" emptyOption="true"
                            listKey="attributeCode" listValue="attributeName" cssClass="attrCodeGroupSelect" multiple="multiple" 
                            data-placeholder="getText('message.pleaseSelectAttribute')">
                       </s:select>
                      </fieldset>
                    </fieldset>
                </div>
            </div>
            <div class="csv_popup_import">
                   <ul id="importTab" >
                       <li class="importLi" >
                           <label class = "label1Import"  >
                           <s:property value="getText('selectCsvModeImport')"/>
                           </label>
                           <select id="importMode" class="cbb-size-normal" class= "widthInputImport" >
                                <option value="1"><s:property value="getText('allData')"/></option>
                                <option value="2"><s:property value="getText('compatibleModel')"/></option>
                                <option value="3"><s:property value="getText('attribute')"/></option>
                            </select>
                           <input type="button" class="btn-color-default lstButton k-button" id="importBtnCsvAnalyze"  value="<s:property value="getText('csvAnalyze')"/>">
                       </li>
                       <li class="importLi" style="padding:2px;height: 35px;" >
                            <div class="liLeft">
                                <div class="upload-button btn-color-default k-button">
                                     <span><s:property value="getText('selectFiles')"/></span>
                                     <input type="file" id="fileupload"  name="fileCSV" />
                                 </div>
                                <div id="progressBar"></div>
                                <input type="hidden" id="fileuploadName"/>
                            </div>
                            <div class="liRight">
                                <input type="button" class="btn-color-default lstButton k-button" id="exportErrorCSV" value="<s:property value="getText('exportErrorCSV')"/>">
                                <input type="hidden" id="hdPathErrorCSV"/>
                            </div>
                           <%-- <input type="button" class="btn-color-default lstButton k-button" id="importBtnUpload" style="width: 100px;" value="<s:property value="getText('text.label.btnSearch')"/>"> --%>
                       </li>
                       <li class="importLi" style="margin-top: 0px;">
                           <label id="importCountStatus" style="margin-left: 5px; color: red;"></label>
                       </li>
                       <li class="importLi">
                           <fieldset id="csvfieldsetImport">
                               <legend><s:property value="getText('csvFormatSetting')"/></legend>
                               <label class = "label2Import">
                                   <s:property value="getText('fieldDelimiter')"/>
                               </label>
                               <select id="importComboboxFieldDelimiter" class= "widthInputImport">
                                    <option value="\t"><s:property value="getText('tab')"/></option>
                                    <option value=","><s:property value="getText('comma')"/></option>
                               </select>
                               <label class = "label3Import">
                                   <s:property value="getText('fieldEnclose')"/>
                               </label>
                               <input type="text" id="boundedSymbolTxt" class="k-textbox widthInputImport" />
                            </fieldset>
                            <div id="gridMode1"></div>
                            <div id="gridCSVFitModel"></div>
                            <div id="gridAttribute"></div>
                       </li>
                   </ul>
            </div>
        </div>
     </div>
         <!-- Begin alert dialog -->
    <div id="alertDialog" class="dialog">
            <div id="alertMsg" class="message">
                <!-- todo message here -->
            </div>
            <div class="buttonArea">
                <input id='btnAlertOK' class='k-button' type='button'
                      value='<s:property value="getText('message.confirm.yes')"/>'>
           </div>
     </div>
    <!-- End alert dialog -->
    <!-- Begin confirm dialog -->
    <div id="confirmCancelDialog" class="dialog">
            <div class="message">
            <!-- todo message here -->
            </div>
            <div class="buttonArea">
                <input id='btnCancelReflectDataOK' class='k-button' type='button'
                      value='<s:property value="getText('message.confirm.yes')"/>'>
                <input id='btnCancelReflectDataCancel' class='k-button' type='button'
                      value='<s:property value="getText('message.confirm.no')"/>'>
           </div>
     </div>
    <!-- End confirm dialog -->
    <!-- Begin confirm reflect data dialog -->
    <div id="confirmReflectDialog" class="dialog">
            <div class="message">
            <!-- todo message here -->
            </div>
            <div class="buttonArea">
                <input id='btnConfirmReflectOK' class='k-button' type='button'
                      value='<s:property value="getText('message.confirm.yes')"/>'>
                <input id='btnConfirmReflectCancel' class='k-button' type='button'
                      value='<s:property value="getText('message.confirm.no')"/>'>
           </div>
     </div>
    <!-- BOE process upload image @rcv!Le.Dinh 2014/03/11. -->
    
    <!-- BOE @rcv!Tran.Thanh 2014/08/12 : add popup upload image -->
    <jsp:include page="component/popupUpload.jsp"></jsp:include>
    <!-- BOE @rcv!Tran.Thanh 2014/08/12 : add popup upload image -->
    
    <s:hidden id="hTmpImage" value="%{@net.webike.RcProductFactory.util.Constant@FOLDER_TMP_IMAGES_PRODUCT_SENTENCE}" ></s:hidden>
    <div id="dialogImageBrowser" class="dialog">
        <div id="imageBrowser"></div>
    </div>
    <!-- EOE process upload image @rcv!Le.Dinh 2014/03/11. -->
    <!-- End confirm reflect data dialog -->
     <!-- Begin hidden field for fit model list -->
     <input type="hidden" id="hdImportMode" value="<s:property value="getText('updateMode')"/>" />
     <input type="hidden" id="hdIdentificationNumber" value="<s:property value="getText('identificationNumber')"/>" />
     <input type="hidden" id="hdSystemProductCode" value="<s:property value="getText('systemProductCode')"/>" />
     <input type="hidden" id="hdMaker" value="<s:property value="getText('maker')"/>" />
     <input type="hidden" id="hdCarModel" value="<s:property value="getText('carModel')"/>" />
     <input type="hidden" id="hdBikeModel" value="<s:property value="getText('bikeModel')"/>" />
     <input type="hidden" id="hdAtributeName" value="<s:property value="getText('atributeName')"/>" />
     <input type="hidden" id="hdAdministrativeName" value="<s:property value="getText('administrativeName')"/>" />
     <input type="hidden" id="hdDisplayName" value="<s:property value="getText('displayName')"/>" />
     <input type="hidden" id="textSyouhinEqual0" value="0" />
     <!-- End hidden field for fit model list  -->
     
     <!-- BOE Tran.Thanh 2014/04/04 -->
     <input id='currentImageName'          type="hidden"   value=''>
     <input id='msUploadImageSize' type='hidden' value='<s:property value="getText('message.maximun5MBFile')"/>'/>
     <input id='msUploadDiskSize' type='hidden' value='<s:property value="getText('message.maximun30MBFileSize')"/>'/>
     <input id='hMaxUploadFolderMatter' type='hidden' value='<s:property  value="matterDetailActionForm.maxUploadFolderMatter"/>'/>
     <input id='hMaxUploadImageSize' type='hidden' value='<s:property  value="matterDetailActionForm.maxUploadImage"/>'/>
     <!-- EOE Tran.Thanh 2014/04/04 -->

     <!-- EOE #7905 Tran.Thanh 2014/05/16 : add number for count new product in csv file -->
     <input id='hCountNewProductInCSV' type='hidden' value='<s:property  value="matterDetailActionForm.countNewProductInCSVFile"/>'/>
     <!-- EOE #7905 Tran.Thanh 2014/05/16 : add number for count new product in csv file -->

     <!-- Begin hidden field for import csv -->
      <input id="hdMatterNo"                type="hidden" value='<s:property value="matterDetailActionForm.entMstFactoryMatterNew.matterNo" />'/>
      <input id="hdCsvOnWorkingErrorMsg"    type="hidden" value="<s:property value="getText('csvOnWorkingErrorMsg')" />" />
      <input id="hdCsvSizeErrorMsg"         type="hidden" value="<s:property value="getText('csvSizeErrorMsg')" />" />
      <input id="hdCsvFileTypeErrorMsg"     type="hidden" value="<s:property value="getText('csvFileTypeErrorMsg')" />" />
      <input id="hdCsvUploadErrorMsg"       type="hidden" value="<s:property value="getText('csvUploadErrorMsg')" />" />
      <input id="hdCsvHeaderNotExistErrorMsg"   type="hidden" value="<s:property value="getText('csvHeaderNotExistErrorMsg')" />" />
      <input id="hdCsvCancelImportErrorMsg"     type="hidden" value="<s:property value="getText('csvCancelImportErrorMsg')" />" />
      <input id="hdBtnCsvAnalyze"           type="hidden" value="<s:property value="getText('csvAnalyze')"/>">
      <input id="hdBtnCsvReflectedInDatabase"   type="hidden" value="<s:property value="getText('csvAndReflectedInDatabase')"/>">
      <input id='hDialogTitle'              type="hidden" value='<s:property value="getText('message.confirm.titleBackToPrevious')"/>'>
      <input id='titleCSVProcess'           type="hidden" value='<s:property value="getText('csvProcessing')"/>'>
      <input id="hdImportedCSVTableName"   type="hidden" value=""/>
      <input id='hdRealFileName'                type="hidden" value=""/>
      <input id='hdCsvfileName'                 type="hidden" value=""/>
      <input id='hdErrorCSVTableName'           type="hidden" value=""/>
      <input id="hdImportCsvCountMsg"           type="hidden" value='<s:property value="getText('importCsvCountMsg')"/>'>
      <input id="hdImportCsvErrorMsg"           type="hidden" value='<s:property value="getText('importCsvErrorMsg')"/>'>
      <input id="hdImportCsvErrorUnknownMsg"    type="hidden" value='<s:property value="getText('importCsvErrorUnknownMsg')"/>'>
      <input id="hdConfirmReflectCsvMsg"        type="hidden" value='<s:property value="getText('confirmReflectCsvMsg')"/>'>
      <input id="hdConfirmCancelReflectCsvMsg"  type="hidden" value='<s:property value="getText('confirmCancelReflectCsvMsg')"/>'>
      <input id="hdCsvReflectDataSuccess"       type="hidden" value='<s:property value="getText('csvReflectDataSuccess')"/>'>
      <input id="hdCvReflectDataFail"           type="hidden" value='<s:property value="getText('csvReflectDataFail')"/>'>
      <input id="hmDeadlock"                    type="hidden" value='<s:property value="getText('message.error.deadlock')"/>'>
      <input id="csvMode1ValidationSuccess"     type="hidden" value="<s:property value="getText('csvMode1ValidationSuccess')"/>"/>
      <input id="csvMode1ValidationFailure"     type="hidden" value="<s:property value="getText('csvMode1ValidationFailure')"/>"/>
      <input id="csvWrongHeader"                type="hidden" value="<s:property value="getText('csvWrongHeader')"/>"/>
      <input id="newProduct"                    type="hidden" value="<s:property value="getText('aNew')"/>"/>
      <input id="csvCannotCreateTmpTable"       type="hidden" value="<s:property value="getText('csvCannotCreateTmpTable')"/>"/>
      <input id="errorImportCSV"                type="hidden" value="<s:property value="getText('errorImportCSV')"/>"/>
     <!-- Begin hidden field for import csv -->
     
     <!-- end popup search attribute -->
</div>    
<!--End Contents -->
</tiles:putAttribute>
</tiles:insertDefinition>