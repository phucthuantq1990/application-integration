<%@ page language="java" contentType="text/html; charset=Shift-JIS"
    pageEncoding="Shift-JIS"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<script type="text/javascript" src="js/jQuery/jquery.fancybox.js?20140804140000"></script>
<script type="text/javascript" src="js/jQuery/kendo.all.min.js?20140804140000" charset="UTF-8"></script>
<script type="text/javascript" src="js/jQuery/kendo.culture.ja-JP.min.js?20140804140000" ></script>
<script type="text/javascript" src="js/jQuery/kendo.web.ext.js?20140804140000" charset="UTF-8"></script>
<script type="text/javascript">
var defaultRedirectPage = '<s:property value="commonForm.defaultRedirectPage"/>';
var defaultErrorPage = '<s:property value="commonForm.defaultErrorPage"/>';

$(document).ready(function() {
    $("#menu").kendoMenu();
    kendo.culture("ja-JP");
});
</script>
<link type="text/css" rel="stylesheet" href="css/jQuery/jquery.fancybox.css?20140804140000"/>
<link rel="stylesheet" type="text/css" href="css/jQuery/kendo.common.min.css?20140804140000" />
<link rel="stylesheet" type="text/css" href="css/jQuery/kendo.metro.css?20140804140000" />



<!-- TOP - Header -->
<div id="top">
  <div class="wrapper">
    <img class="logo" alt="" src="./images/logo.png">
    <div class="logout">
        <input id='confirmYes'                      type="hidden" value='<s:property value="getText('message.confirm.yes')"/>'>
        <input id='confirmCancel'                   type="hidden" value='<s:property value="getText('message.confirm.no')"/>'>
        <input id='titleLogout'                     type="hidden" value='<s:property value="getText('logout')"/>'>
        <input id='confirmLogout'                   type="hidden" value='<s:property value="getText('message.confirm.logout')"/>'>
        <a href="" onclick="fancyLogout(); return false;" class="btn-size-small btn-color-blue" style="text-decoration: none; width: 60px;padding-left: 10px;padding-right: 10px;padding-top: 2px;padding-bottom: 2px;text-align:center;display: inline;"><s:property value="getText('logout')"/></a>
    </div>
    <s:if test="%{commonForm.loginLastName!=null}">
        <!-- BOE Fix bug #35 Change login link to text form @rcv!nguyen.hieu 2014/03/11. -->
        <!-- <div class="user"><s:property value="getText('logged')"/> ： <a href="userDetail.html?userId=<s:property value="commonForm.loginId" />" class="" style=""><s:property value="commonForm.loginLastName" /> <s:property value="commonForm.loginFirstName" /></a>  <s:property value="getText('san')"/></div> -->
        <div class="user"><s:property value="getText('logged')"/> ： <s:property value="commonForm.loginLastName" /> <s:property value="commonForm.loginFirstName" />  <s:property value="getText('san')"/></div>
        <!-- EOE Fix bug #35 Change login link to text form. -->
        <input type="hidden" id="userLoginId" value="<s:property value="commonForm.loginId"/>"/>
    </s:if>
  </div>
  <div class="menu_bar" style="margin-bottom: -3px;">
      <!--  Menu Kendo -->
      <ul id="menu">
          <%-- BOE hoang.ho 2014/03/10 Remove menu --%>
          <li style="background-color: #8EBC00; width: 190px;">
          <%-- EOE hoang.ho 2014/03/10 Remove menu --%>
              <%-- <span class="h1">Webike Product Checker</span> --%>
              <span id="factoryLabel" class="h1"><s:property value="getText('productFactory')"/></span>
          </li>
          <%-- BOE hoang.ho 2014/03/10 Remove menu --%>
         <%--  <li>
              <a href="mainMenu.html">ダッシュボード</a>
          <li> --%>
          <%-- EOE hoang.ho 2014/03/10 Remove menu --%>
          <li>
              <!-- <a href="">案件管理</a> -->
                <s:property value="getText('matterManagement')"/>
              <ul>
                  <li>
                      <a href="matterManage.html"><s:property value="getText('matterList')"/></a>
                  </li>
              </ul>
          </li>
          <li>
              <!-- <a href="">製品管理</a> -->
              <s:property value="getText('productManagement')"/>
              <ul>
                  <li>
                      <a href="productManagement.html"><s:property value="getText('productSearch')"/></a>
                  </li>
              </ul>
          </li>
          <li>
              <!-- <a href="">マスタ管理</a> -->
              <s:property value="getText('masterManagement')"/>
              <ul>
                  <%-- BOE hoang.ho 2014/03/10 Remove menu --%>
                  <%-- <li>
                      <a href="userManage.html">ユーザー管理</a>
                  </li> --%>
                  <%-- EOE hoang.ho 2014/03/10 Remove menu --%>
                  <li>
                      <a href="brandMasterManager.html"><s:property value="getText('brand')"/></a>
                  </li>
                  <li>
                      <a href="categoryManage.html"><s:property value="getText('category')"/></a>
                  </li>
                  <li>
                      <a href="attributeManage.html"><s:property value="getText('attribute')"/></a>
                  </li>
              </ul>
          </li>
          <%-- BOE hoang.ho 2014/03/10 Remove menu --%>
          <%-- <li>
              <a href="">設定</a>
                                設定
          </li> --%>
          <%-- EOE hoang.ho 2014/03/10 Remove menu --%>
      </ul>
  </div>
</div>
<!-- End TOP - Header -->
  