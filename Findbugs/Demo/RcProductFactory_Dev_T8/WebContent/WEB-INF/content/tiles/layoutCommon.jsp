<%@ page language="java" contentType="text/html; charset=Shift-JIS"
    pageEncoding="Shift-JIS"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>

<?xml version="1.0" encoding="Shift_JIS"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS" />
    <title><tiles:getAsString name="title"/></title>
    <link rel="stylesheet" type="text/css" href="css/layoutBasic.css?20140804140000" />
    <link rel="stylesheet" type="text/css" href="css/base.css?20140804140000" />
	<link rel="stylesheet" type="text/css" href="css/layout.css?20140804140000" />
	<link rel="stylesheet" type="text/css" href="css/header.css?20140804140000" />
	<link rel="stylesheet" type="text/css" href="css/footer.css?20140804140000" />
    <link rel="stylesheet" type="text/css" href="css/redmond/jquery-ui-1.8.23.custom.css?20140804140000" />
<!-- 	<script type="text/javascript" src="js/jQuery/jquery-1.8.0.min.js?20140804140000"></script> -->
<!-- 	<script type="text/javascript" src="js/jQuery/jquery-ui.custom.min.js?20140804140000"></script> -->
	<script type="text/javascript" src="js/jQuery/jquery-1.8.3.js?20140804140000"></script>
    <script type="text/javascript" src="js/commonJS.js?20140804140000"></script>
    <tiles:insertAttribute name="css"/>
    <tiles:insertAttribute name="javaScript"/>
</head>
<body>
<tiles:insertAttribute name="header" />
<tiles:insertAttribute name="body" />
<tiles:insertAttribute name="footer" />
</body>
</html>