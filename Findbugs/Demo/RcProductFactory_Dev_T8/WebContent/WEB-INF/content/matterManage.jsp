<%@ page language="java" contentType="text/html; charset=Shift-JIS"
    pageEncoding="Shift-JIS"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<tiles:insertDefinition name="layoutCommon">
<tiles:putAttribute name="css">
    <link rel="stylesheet" type="text/css" href="css/jQuery/kendo.common.min.css?20140804140000" />
    <link rel="stylesheet" type="text/css" href="css/matterManage.css?20140804140000" />
    <link rel="stylesheet" type="text/css" href="css/common.css?20140804140800" />
    
    <!-- New matter dialog - Begin -->
    <link rel="stylesheet" type="text/css" href="css/newMatterDialog.css?20140804140000" />
    <!-- New matter dialog - End -->
</tiles:putAttribute>
<tiles:putAttribute name="javaScript">
    <script type="text/javascript" src="js/jQuery/kendo.all.min.js?20140804140000"></script>
    <script type="text/javascript" src="js/jQuery/kendo.culture.ja-JP.min.js?20140804140000"></script>
    <script type="text/javascript" src="js/jQuery/jquery.validate.min.js?20140804140000"></script>
    
    <!-- New matter dialog - Begin -->
    <script type="text/javascript" src="js/newMatterDialog.js?20140804140000"></script>
	<!-- New matter dialog - End -->
    
    <script type="text/javascript" src="js/matterManageJS.js?20140804140000"></script>
    <script type="text/javascript" src="js/commonJS.js?20140804140000"></script>
</tiles:putAttribute>
<tiles:putAttribute name="body">
<!-- Contents -->
<div id="contents">
    <script type="text/javascript" src="js/jQuery/selectable.js?20140804140000"></script>
    <div id="content">
	
	<!-- Info Bar -->
	<div id="message" class="has-message" style="display:none"></div>

    <!-- No data message -->
    <input id='noDataMessage'       type="hidden" value='<s:property value="getText('text.error.noData')"/>'>
    <!-- Header of grid -->
    <input id='titleMatterNo'                           type="hidden" value='<s:property value="getText('text.label.matterNo')"/>'>
    <input id='titleMatterRedmineIssueId'               type="hidden" value='<s:property value="getText('text.label.matterRedmineIssueId')"/>'>
    <input id='titleMatterName'                         type="hidden" value='<s:property value="getText('text.label.matterName')"/>'>
    <input id='titleMatterChargeUserId'                 type="hidden" value='<s:property value="getText('text.label.matterChargeUserId')"/>'>
    <input id='titleMatterRegistrationFlg'              type="hidden" value='<s:property value="getText('text.label.matterRegistrationFlg')"/>'>
    <input id='titleMatterFactoryCount'                 type="hidden" value='<s:property value="getText('text.label.matterFactoryCount')"/>'>
    <input id='titleMatterFactoryErrorCountGeneral'     type="hidden" value='<s:property value="getText('text.label.matterFactoryErrorCountGeneral')"/>'>
    <input id='titleMatterFactoryErrorCountCategory'    type="hidden" value='<s:property value="getText('text.label.matterFactoryErrorCountCategory')"/>'>
    <input id='titleMatterFactoryErrorCountModel'       type="hidden" value='<s:property value="getText('text.label.matterFactoryErrorCountModel')"/>'>
    <input id='titleMatterFactoryErrorCountAttribute'   type="hidden" value='<s:property value="getText('text.label.matterFactoryErrorCountAttribute')"/>'>
    <input id='titleUpdateOn'                           type="hidden" value='<s:property value="getText('text.label.updatedOn')"/>'>
    <!-- Hidden for commbo box filter -->
    <input id='hiddenlistCommonUser' type="hidden" value='<s:if test="null != commonForm.listCommonUser">
                                                        <s:select disabled=""
                                                        id="lstUser"
                                                        list="commonForm.listCommonUser"
                                                        listKey="userId"
                                                        listValue="%{userLastName + ' ' + userFirstName}"
                                                        value=""
                                                        name="lstUser"
                                                        headerKey=" "
                                                        headerValue=" "
                                                        cssClass="cbbFilterUser" />
                                                    </s:if>'>
    <input type="hidden" id="hMatterChangeUserId" value="<s:property value="commonForm.loginAuthority"/>">
    <input type="hidden" id="hLoginId" value="<s:property value="commonForm.loginId"/>">
    <input id='hiddenRegistrationFlg' type="hidden" value='<s:if test="null != matterManageActionForm.listRegistrationFlg">
                                                        <s:select id="cbbRegistrationFlg"
                                                        cssClass="cbbFilterUser"
                                                        headerKey=" "
                                                        headerValue=" "
                                                        list="matterManageActionForm.listRegistrationFlg"/>
                                                    </s:if>'>
    <!-- Title for Column Matter Registration Flg -->
    <input id='matterRegistrationFlg1'                type="hidden" value='<s:property value="getText('text.label.matterRegistrationFlg1')"/>'>
    <input id='matterRegistrationFlg2'                type="hidden" value='<s:property value="getText('text.label.matterRegistrationFlg2')"/>'>
    <input id='matterRegistrationFlg3'                type="hidden" value='<s:property value="getText('text.label.matterRegistrationFlg3')"/>'>
    <!-- Title for paging -->
    <input id='pagingDisplay'                type="hidden" value='<s:property value="getText('text.paging.display')"/>'>
    <input id='pagingEmpty'                  type="hidden" value='<s:property value="getText('text.paging.empty')"/>'>
    <input id='pagingItemsPerPage'           type="hidden" value='<s:property value="getText('text.paging.itemsPerPage')"/>'>
    <input id='pagingFirst'                  type="hidden" value='<s:property value="getText('text.paging.first')"/>'>
    <input id='pagingPrevious'               type="hidden" value='<s:property value="getText('text.paging.previous')"/>'>
    <input id='pagingNext'                   type="hidden" value='<s:property value="getText('text.paging.next')"/>'>
    <input id='pagingLast'                   type="hidden" value='<s:property value="getText('text.paging.last')"/>'>
    <!-- Filter type for grid -->

        <!-- start title Management of Brand Master -->
        <div id="title">
            <h1><s:property value="getText('text.label.mainMenuH1')"/></h1>
            <button type="button" id="btnAddNew" class="k-button">
                <s:property value="getText('text.label.btnToNewMatter')"/>
            </button>
        </div>
        <!-- end title Management of Brand Master -->
        <!-- start contents_inner -->
        <div id="contentsInner">
            <!-- start table list master -->
<!--             <div class="headerContent"> -->
<%--                 <label><s:property value="getText('text.label.status')" /></label> --%>
<!--             </div> -->
            <!-- start list button div -->
            <div id="lstButton">
                <input type="button" class="btn-color-default lstButton k-button" id="btnBack" value="<s:property value="getText('text.label.btnBackToPreviousScreen')"/>">
                <input type="button" class="btn-color-default lstButton k-button" id="btnReset" value="<s:property value="getText('text.label.btnReset')"/>">
                <input type="button" class="btn-color-default lstButton k-button" id="btnSearch" value="<s:property value="getText('text.label.btnSearch')"/>">
                <input type="button" class="btn-color-default lstButton k-button btnRed" id="btnDelete" value="<s:property value="getText('text.label.btnDelete')"/>">
            </div>
            <!-- end list button div -->
            <div id="mainContent" class="tbl-content">
                <!-- start table list master -->
                <div id="table-content">
                    <!-- start table list master content -->
                    <div id="grid">
                    </div>
                    <!-- end table list master content -->
                </div>
                <!-- end table list master -->
            </div>
            <!-- end table list master -->
        </div>
    </div>
</div>    

<!-- New matter dialog - Begin -->
<div id="newMatterDialog">
    <input type="hidden" id="hLoginIdPopup" value="<s:property value="commonForm.loginId"/>">
	<div id="contents">
		<div id="contentNewMatterDialog">
			<div id="title">
<!-- 				<h1> -->
<%-- 					<s:property value="getText('text.label.newMasterTitel')" /> --%>
<!-- 				</h1> -->
			</div>
			<div id="contents_inner">
				<div id="centerTab">
		
					
						<ul>
							<li>
								<p id="redmineIssueIdLabel">
									<s:property value="getText('text.label.redmineIssueId')" />
								</p>
								<div id = "divRedmineIssueId">
									<s:textfield id="redmineIssueId" name="redmineIssueId"
										cssClass="txt-size-medium pjName k-input k-textbox" autocomplete="off"
										maxlength="16" />
									<span id="redmineIssueIdMessage" style="display: none"></span>
								</div>
							</li>
							
							<li>
								<p id="pjLabel">
									<s:property value="getText('text.label.projectName')" />
								</p>
								<div id = "divMatterName">
									<s:textfield id="matterName" name="matterName"
										cssClass="txt-size-medium pjName k-input k-textbox" autocomplete="off"
										maxlength="256" />
									<span id="matterNameMessage" style="display: none"></span>
										<!-- 
									<span for="pjName" generated="true" class="error"></span>
				                    <span id="errorMessage" class="error" style="display: inline-block;"></span>
				                     -->
								</div>
							</li>
							
							<li>
								<p>
									<s:property value="getText('text.label.Administrator')" />
								</p>
								<div id = "divChangeUser">
									<s:select id="lstAdmin" name="changeUser"
									    value="commonForm.loginId"
                                        headerKey=" "
                                        headerValue=" "
										list="form.listAdmin"
										listKey="%{userId}"
										listValue="%{userName}" 
										cssClass="cbb-size-normal" />
									<span id="managerNameMessage" style="display: none"></span>
								</div>
							</li>
							
							<li>
								<div>
									<input type="button" 
										class="k-button"
										id="btn_submit"
										style="margin-top: 10px; width: 130px;margin-left: 80px;"
										value='<s:property value="getText('text.label.btnAddProject')"/>' />
									    <input type="button" 
										class="k-button"
									    id="btnCancel"
										style="margin-left: 25px; margin-top: 10px; width: 130px; text-decoration: none;"
										value='<s:property value="getText('text.label.btnCancel')"/>' />
								</div>
							</li>
						</ul>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- New matter dialog - End -->

<!-- Popup Confirm Delete Matter -->
<div id="deleteMatterDialog">
    <!-- boe thai.son -->
    <input type="hidden" id="windowNamedeleteMatterDialog" value="<s:property value="getText('confirmation')"/>" />
    <!-- eoe thai.son -->
	<div class="matterManageDialogContainer">
		<div class="matterManageDialogContent">
			<div id="matterManageDialogContentLine1">
				<s:property value="getText('text.label.matterManageDeleteDialogContent')"/>
			</div>
			<br>
			<div id="matterManageDialogContentLine2">
				<s:property value="getText('text.label.matterManageDeleteDialogContent2')"/>
			</div>
		</div>
		<input type="button" class="k-button" id="btnDeleteMatter" 
			value='<s:property value="getText('text.btn.yes')"/>' />
		<input type="button" class="k-button" id="btnDeleteMatterCancel" 
			value='<s:property value="getText('text.label.btnCancel')"/>' />
	</div>
</div>
<div id="pleaseSelectDialog" class="matterManageDialog">
    <!-- boe thai.son -->
    <input type="hidden" id="windowNamepleaseSelectDialog" value="<s:property value="getText('confirmation')"/>" />
    <!-- eoe thai.son -->
	<div class="matterManageDialogContainer">
		<div class="matterManageDialogContent" style="color:red;">
			<s:property value="getText('text.label.matterManagePleaseSelectContent')"/>
		</div>
		<input type="button" class="k-button" id="pleaseSelectDialogOk" 
			value='<s:property value="getText('text.btn.yes')"/>' />
	</div>
</div>

<input type="hidden" id="deleteDialogTitle" value="<s:property value="getText('text.label.matterManageDeleteDialogTitle')"/>" />
<input type="hidden" id="newMatterTitle" value="<s:property value="getText('text.label.newMatterTitle')" />" />
<input type="hidden" id="newMatterOkMessage" value="<s:property value="getText('message.newMatterOk')"/>" />
<input type="hidden" id="newMatterNgMessage" value="<s:property value="getText('message.newMatterNg')"/>" />
<!-- BOE edit message by Luong.Dai at 2014/03/19 -->
<%-- <input type="hidden" id="newMatterNumberOnlyMessage" value="<s:property value="getText('message.error.onlyInputXNumber')"/>" /> --%>
<input type="hidden" id="newMatterNumberOnlyMessage" value="<s:property value="getText('outOfTicketNumber')"/>" />
<!-- EOE edit message by Luong.Dai at 2014/03/19 -->
<input type="hidden" id="newMatterRequiredMessage" value="<s:property value="getText('message.error.requiredFill')"/>" />
<input type="hidden" id="newMatterMaxLengthMessage" value="<s:property value="getText('message.error.inputOutOfRange')"/>" />
<input type="hidden" id="newMatterInvalidNameFormat" value="<s:property value="getText('message.error.nameNotCorrect')"/>" />
<input type="hidden" id="newMatterRedmineIssueIdLabel" value="<s:property value="getText('text.label.redmineIssueId')"/>" />
<input type="hidden" id="newMatterRedmineMatterLabel" value="<s:property value="getText('text.label.projectName')"/>" />
<input type="hidden" id="newMatterRedmineManagerNameLabel" value="<s:property value="getText('text.label.Administrator')"/>" />
<input type="hidden" id="insertSuccessMessage" value="<s:property value="getText('message.success.insertSuccess')"/>" />
<input type="hidden" id="otherUpdateMessage" value="<s:property value="getText('message.error.othersUpdateForJS')"/>" />
<input type="hidden" id="deleteSuccessMessage" type="hidden" value="<s:property value="getText('message.error.deleteSuccess')"/>" />
<input type="hidden" id="deleteSuccessFail" type="hidden" value="<s:property value="getText('message.error.deleteFail')"/>" />

<!--End Contents -->
</tiles:putAttribute>
</tiles:insertDefinition>