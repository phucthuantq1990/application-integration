<%@ page language="java" contentType="text/html; charset=Shift-JIS"
    pageEncoding="Shift-JIS"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<tiles:insertDefinition name="NSideLayoutBasic">
    <tiles:putAttribute name="main">
    	<meta http-equiv="Refresh" content="1; URL=http://moto.webike.net/">
        <div align="center" style="background:#FCF6C0;padding:30px;font-size:18px">

            <s:if test="msg != null and msg.length() > 0">
                <s:property value="msg" />
            </s:if>
                                      お探しのページは見つかりませんでした。<br>
                                      モトサーチTOPページヘ移動します。
            <br>
            <br>
                                      自動的に遷移しない方はお手数ですが下記をクリックしてください<br />
            <s:a href="http://moto.webike.net/">モトサーチTOPへ</s:a>
        </div>
    </tiles:putAttribute>
</tiles:insertDefinition>