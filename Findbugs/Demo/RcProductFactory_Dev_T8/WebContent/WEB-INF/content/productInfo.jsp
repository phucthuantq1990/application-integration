<%@ page language="java" contentType="text/html; charset=Shift-JIS"
    pageEncoding="Shift-JIS"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<tiles:insertDefinition name="layoutCommon">
<tiles:putAttribute name="css">
    <link rel="stylesheet" type="text/css" href="css/productInfo.css?20140804140000" />
    <link rel="stylesheet" type="text/css" href="css/jQuery/kendo.common.min.css?20140804140000" />
</tiles:putAttribute>
<tiles:putAttribute name="javaScript">
    <script type="text/javascript" src="js/jQuery/jquery.fancybox.pack.js?20140804140000"></script>
    <script type="text/javascript" src="js/jQuery/jquery.fancybox.js?20140804140000"></script>
    <script type="text/javascript" src="js/jQuery/jquery.jstree.js?20140804140000"></script>
    <script type="text/javascript" src="js/jQuery/jquery.validate.min.js?20140804140000"></script>
    <script type="text/javascript" src="js/localization/messages_ja.js?20140804140000"></script>
        <script type="text/javascript" src="js/productInfoJS.js?20140804140000"></script>
    <!-- <script type="text/javascript" src="js/jQuery/kendo.web.min.js?20140804140000"></script> -->
    <!-- 
    * Upgrade version of kendo UI to 2013.3.1119
    * BOE luong.dai 2014/01/07 #6007
    * -->
    <script type="text/javascript" src="js/jQuery/kendo.all.min.js?20140804140000"></script>
    <!-- 
    * EOE luong.dai 2014/01/07 #6007
    * -->
    <script type="text/javascript">
    //original page
    var originalPage = "<s:property value='productInfoActionForm.originalPage'/>";
    //No data message
    var noDataMessage = '<s:property value="getText('text.error.noData')"/>';
    //No image url
    var noImageURL = "<s:property value='productInfoActionForm.noImageURL'/>";
    //Matter no
    var matterNo = "<s:property value='productInfoActionForm.matterNo'/>";
    //Checked status
    var checkedNo = "<s:property value='productInfoActionForm.checkedNo'/>";
    //count of list thumb.
    var listThumbCount = "<s:property value='productInfoActionForm.listThumb.size'/>";
    
    var titleSystemProductCode = "<s:property value="getText('text.label.systemProductCode')"/>";
    var titleBrandName = "<s:property value="getText('text.label.brandName')"/>";
    var titleProductName = "<s:property value="getText('text.label.productName')"/>";
    var titleMakerProductCode = "<s:property value="getText('text.label.makerProductCode')"/>";
    var titleBunruiCode = "<s:property value="getText('text.label.bunruiCode')"/>";
    var titleBunruiName = "<s:property value="getText('text.label.bunruiName')"/>";
    var titleStatus = "<s:property value="getText('text.label.status')"/>";
    var labelCheckStatus = "";
    
    </script>
</tiles:putAttribute>
<tiles:putAttribute name="body">
<!-- Contents -->
<div id="contents">
    <div id="content">
        <div >
            <h1><s:property value="getText('text.label.productInfo')"/></h1>
        </div>
    <!-- contents_inner start -->
    <div id="contents_inner" style="min-height:468px;">
        <!--left tab -->
        <input type="hidden" name="bunruiCode" id="bunruiCode" value=""/>
        <div class="leftTab" style="height:447px;">
            <!-- Image Left  -->
            <div class="products_main_image_inner">
                    <a id="imageAhref" rel="fancy_box"
                       href="" 
                       class="fancy_box">
                        <img id="imageThumb" src=""/>
                        <img id="imageThumbLoading" src="./images/processing.gif" style="display: none;">
                    </a>
            </div>
            <form name="thumb_selector_form">
                <div class="thumb_selector clearfix" style="margin:25px 0 0 30px;"></div>
            </form>
            <!-- products data -->
            <div id="thumbHiddenDiv">
                <s:iterator value="productInfoActionForm.listImage" status="st">
                    <s:if test="productInfoActionForm.listImage[#st.index].productThumb !='' || productInfoActionForm.listImage[#st.index].productImage !=''">
                        <input type="hidden"
                            id="thumb_<s:property value="%{#st.index}"/>"
                            value="<s:property value="%{productInfoActionForm.listImage[#st.index].productThumb}" />" />
                        <input type="hidden"
                            id="image_<s:property value="%{#st.index}"/>"
                            value="<s:property value="%{productInfoActionForm.listImage[#st.index].productImage}" />" />
                    </s:if>
                    <s:else>
                        <input type="hidden"
                            id="thumb_<s:property value="%{#st.index}"/>"
                            value="<s:property value='productInfoActionForm.noImageURL'/>" />
                        <input type="hidden"
                            id="image_<s:property value="%{#st.index}"/>"
                            value="<s:property value='productInfoActionForm.noImageURL'/>" />
                    </s:else>
                </s:iterator>
            </div>
            <!-- End Image Left  -->
        </div>
        <div class="centerTab" style="height:477px;">
            <div id="productCenterTableInfoNodata" class="errorMessage" style="display:none;padding-top: 210px;"><s:property value="getText('text.error.noData')"/></div>
            <ul id="productCenterTableInfo" style="padding:5px;">
                <li>
                <a class="btn-size-small btn-color-default " tabindex="1"
                   style="width:100px; maring-left:10px;float:right;display:inline;maring-bottom:10px;text-decoration: initial;text-align: center;"
                   href="productDetail.html"><s:property value="getText('text.label.checkData')"/></a>
<!--                      <input class="btn-size-small btn-color-default" style="width:100px; maring-left:10px;float:right;display:inline;maring-bottom:10px;" type="button"  -->
<%--                         onclick="/productDetail.html" value='<s:property value="getText('text.label.checkData')"/>'/> --%>
                </li>
                <br/>
                <li style="padding-bottom:5px; border-bottom: 1px solid #C0C0C0;">
                    <div >
                        <h1 style="font-size: 10pt;"><span id="txtProductName" ></span></h1>
                    </div>
                    <span><s:property value="getText('text.label.manufactoryCode')"/> : <span id="txtManufactoryCodeDetail"></span>
                    </span>
                </li>
                <br/>
                <li style="padding-bottom:5px; border-bottom: 1px solid #C0C0C0;">
                    <div >
                        <h1 style="font-size: 10pt;"><s:property value="getText('text.label.brandInfo')"/></h1>
                    </div>
                    <span><span id="txtBrandInfo"></span>
                    </span>
                </li>
                <br/>
                <li>
                    <div >
                        <h1 style="font-size: 10pt;"><s:property value="getText('text.label.productSumary')"/></h1>
                    </div>
                    <span><s:textarea id="txtProductSummary" disabled="true" cssStyle="width:283px;height:80px;resize: none; padding-left: 5px;" value="%{productInfoActionForm.listProduct[0].productSummary}"/></span>
                </li>
                <br/>
                <li>
                    <div >
                        <h1 style="font-size: 10pt;"><s:property value="getText('text.label.relationModel')"/></h1>
                    </div>
                    <div id="tableListModel">
                        <img class='imgLoadingModelTable' src='./images/processing.gif' style="display: none;">
                        <table id="modelListTable" style="margin: 0px; padding:0px;">
                            <tbody>
                                <s:iterator value="%{productInfoActionForm.listModel}" >
                                    <tr>
                                        <td class="clmModelTable1" style="text-align:left; width:;" title="<s:property value="productMaker"/>">
                                            <s:if test="productMaker.length > 8">
                                                <span title="<s:property value='productMaker'/>">
                                                    <s:property value="productMaker"/>
                                                </span>
                                            </s:if>
                                            <s:else>
                                                <s:property value="productMaker"/>
                                            </s:else>
                                        </td>
                                        <td class="clmModelTable2" style="text-align:center; width:;" title="<s:property value="productModel"/>">
                                                <s:property value="productModel"/>
                                        </td>
                                        <td class="clmModelTable3" style="text-align:center; width:;" title="<s:property value="productStyle"/>">
                                                <s:property value="productStyle"/>
                                        </td>
                                    </tr>
                                </s:iterator>
                            </tbody>
                        </table>
                    </div>
                </li>
            </ul>
        </div>
        <!--right tab -->
        <div id="searchTreeView" class="rightTab" style="margin-top:10px;margin-right:10px; height: 70px;">
            <div >
                <h1 style="font-size: 10pt;"><s:property value="getText('text.label.bunruiSearch')"/></h1>
                <div style="float: right; margin-right: 20px;">
                	<a id='hotkey_link' class='fancybox' href="frame/hotkeyFrame.htm" style="background-color: transparent;"><s:property value="getText('text.label.shortcutKey')"/></a>
                </div>
            </div>
            <ul>
                <li>
                    <s:textfield maxlength="20" id="bunruiKeySearch" tabindex="2" name="bunruiKeySearch" cssClass="txt-size-medium tbSearch" onkeypress="HandleBunruiKeySearchPress(event)" cssStyle="width:222px;" autocomplete="off" value=""/>
                </li>
                <li style="margin-left:10px;margin-bottom:5px; margin-top:5px;">
                    <span id="successSearch" style="display:inline;" class="successMessage"></span>
                </li>
            </ul>
        </div>
        <div id="listTreeView" class="rightTab jstree-default">
        </div>
        <form name="categoryTreeView" id="categoryTreeView" onkeypress="return event.keyCode != 13;">
        <div id="detailTreeView" class="rightTab" style="margin-top:10px;margin-right:10px; height: 74px;">
            <ul>
                <li>
                    <div >
                        <h1 style="font-size: 10pt;"><s:property value="getText('text.label.bunruiSearchCode')"/></h1>
                    </div>
                </li>
                <li style="margin-bottom:5px;margin-left:0px;">
                    <s:textfield maxlength="20" id="nodeId"
                                 name="nodeId" cssClass="txt-size-small tbSearch" tabindex="3"
                                 cssStyle="width:50px; text-align:center;"
                                 maxLength="4" autocomplete="off" value="%{productInfoActionForm.listProduct[0].bunruiCode}"/>
                    <div id="nodeDetail" title=""></div>
                </li>
                <li style="padding-bottom:5px;">
                    <span id="errorNode" style="display:none" class="error"><s:property value="getText('message.error.BunruiCodeIncorrect')"/></span>
                </li>
            </ul>
        </div>
        </form>
        <div id="" class="rightTab" style="margin-top:10px;margin-right:10px;">
            <ul>
                <li style="margin-bottom:10px;">
                    <input tabindex="4" class="btn-size-small btn-color-default" style="width:37%; maring-left:10px;display:inline;maring-bottom:10px;" type="button" 
                        id='btn_update_bunrui' value='<s:property value="getText('text.label.updateBunruiCode')"/>'/>
                    <input tabindex="5" class="btn-size-small btn-color-default" style="width:20%; maring-left:10px;display:inline;maring-bottom:10px;" type="button" 
                        id='prev_button' onclick="changeToPrevRow()" value='<s:property value="getText('text.label.forward')"/>'/>
                    <input tabindex="6" class="btn-size-small btn-color-default" style="width:20%; maring-left:10px;display:inline;maring-bottom:10px;" type="button" 
                        id='next_button' onclick="changeToNextRow()" value='<s:property value="getText('text.label.next')"/>'/>
                </li>
            </ul>
        </div>
    </div>
    <!-- contents_inner end-->
    <!-- contents_inner start -->
    <div id="contents_inner" style="border: none; background: none;">
        <div id="filter">
        	<s:property value="getText('text.label.progress')"/> �F
            <span id="totalProduct">
                <s:property value="productInfoActionForm.entMatter.totalCheckedProduct"/>/ <s:property value="productInfoActionForm.entMatter.totalAllProduct"/>
            </span>
            <div class="btnSearch">
                <s:if test="productInfoActionForm.entMatter.totalErrorProduct > 0">
                    <s:submit id = "btn_release" tabindex="7" disabled="true" value="%{getText('text.label.release')}" type="button" cssClass="btn-size-small btn-color-gold" onclick="releaseMatter();"/>
                </s:if>
                <s:else>
                    <s:submit id = "btn_release" value="%{getText('text.label.release')}" type="button" cssClass="btn-size-small btn-color-gold" onclick="releaseMatter();"/>
                </s:else>
            </div>
            <label><s:property value="getText('text.label.systemProductCode')"/></label>
                 <s:textfield id="txtSystemProductCode" tabindex="8" name="systemProductCode" cssClass="txt-size-small tbSearch" onkeypress="HandleKeyPress(event)" cssStyle="width:100px;" maxlength="12" autocomplete="off" value=""/>
            <label style="padding-left: 10px;"><s:property value="getText('text.label.makerProductCode')"/></label>
                 <s:textfield id="txtManufactoryCode" tabindex="9" name="manufactoryCode" cssClass="txt-size-small tbSearch" onkeypress="HandleKeyPress(event)" cssStyle="width:100px;" maxlength="20" autocomplete="off" value=""/>
            <label style="padding-left: 10px;"><s:property value="getText('text.label.bunruiCode')"/></label>
                 <s:textfield id="txtBunruiCode" tabindex="10" name="bunruiCode" cssClass="txt-size-small tbSearch" onkeypress="HandleKeyPress(event)" cssStyle="width:100px;" maxlength="20" autocomplete="off" value=""/>
            <label style="padding-left: 10px;"><s:property value="getText('text.label.status')"/></label>
                 <s:select
                    id="cbbStatusProductCode"
                    tabindex="11"
                    headerKey="" headerValue="%{getText('text.value.other')}"
                    list="productInfoActionForm.ListProductStatus"
                    listKey="productStatusCode"
                    listValue="productStatusName"
                    cssClass="tbSearch"
                    onkeypress="HandleKeyPress(event)"
                />
            <div class='btnSearch'>
                 <input type="button" tabindex="12" class="btn-color-default btn-size-small btnSearch" id='btn_search_product' value='<s:property value="%{getText('text.label.search')}"/>' style="width:55px;" />
            </div>
        </div>
        <div id="mainContent" class="tbl-content">
            <div class="tbl-size-large" id ="no4">
            </div>
        </div>
    </div>
    <!-- contents_inner end-->
    </div>
  </div>
<!--End Contents -->
</tiles:putAttribute>
</tiles:insertDefinition>