<%@ page language="java" contentType="text/html; charset=Shift-JIS"
    pageEncoding="Shift-JIS"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<tiles:insertDefinition name="layoutCommon">
<tiles:putAttribute name="css">
    <link rel="stylesheet" type="text/css" href="css/jQuery/kendo.ext.css?20140804140000" />
    <link rel="stylesheet" type="text/css" href="css/commonManage.css?20140804140000" />
    <link rel="stylesheet" type="text/css" href="css/productPreview.css?20140804140000" />
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css?20140804140000" />
</tiles:putAttribute>
<tiles:putAttribute name="javaScript">
    <script type="text/javascript" src="js/jQuery/kendo.all.min.js?20140804140000"></script>
    <script type="text/javascript" src="js/jQuery/kendo.web.ext.js?20140804140000" charset="UTF-8"></script>
    <script type="text/javascript" src="js/jQuery/kendo.culture.ja-JP.min.js?20140804140000"></script>
    <script type="text/javascript" src="js/jQuery/jquery.validate.min.js?20140804140000"></script>
    <script type="text/javascript" src="js/jQuery/shortcut.js?20140804140000"></script>
    <script type="text/javascript" src="js/commonJS.js?20140804140000"></script>
    <script type="text/javascript" src="js/productPreview.js?20140804140000"></script>
</tiles:putAttribute>
<tiles:putAttribute name="body">
<!-- Contents -->
<div id="contents">
    <!-- No data message -->
    <input id='noDataMessage'       type="hidden" value='<s:property value="getText('text.error.noData')"/>'>
    <!-- Begin Title text of pop up -->
    <input id='titleCheckPerson'             type="hidden" value='<s:property value="getText('text.productPreview.Popup.titleCheckPerson')"/>'>
    <input id='titleCheckPerson1'            type="hidden" value='<s:property value="getText('text.productPreview.Popup.titleCheckPerson1')"/>'>
    <input id='titleCheckPerson2'            type="hidden" value='<s:property value="getText('text.productPreview.Popup.titleCheckPerson2')"/>'>
    <input id='titleTab1'                    type="hidden" value='<s:property value="getText('text.productPreview.Popup.titleTab1')"/>'>
    <input id='titleTab2'                    type="hidden" value='<s:property value="getText('text.productPreview.Popup.titleTab2')"/>'>
    <input id='titleTab3'                    type="hidden" value='<s:property value="getText('text.productPreview.Popup.titleTab3')"/>'>
    <input id='buttonInsertRow'              type="hidden" value='<s:property value="getText('text.productPreview.Popup.buttonInsertRow')"/>'>
    <input id='buttonDelete'                 type="hidden" value='<s:property value="getText('text.productPreview.Popup.buttonDelete')"/>'>
    <input id='tilteCorrespond'              type="hidden" value='<s:property value="getText('text.productPreview.Popup.TilteCorrespond')"/>'>
    <input id='tilteProductId'               type="hidden" value='<s:property value="getText('text.productPreview.Popup.TilteProductId')"/>'>
    <input id='tilteOpenNewTab'              type="hidden" value='<s:property value="getText('text.productPreview.Popup.TilteOpenNewTab')"/>'>
    <input id='tilteRequestType'             type="hidden" value='<s:property value="getText('text.productPreview.Popup.TilteRequestType')"/>'>
    <input id='tilteContent'                 type="hidden" value='<s:property value="getText('text.productPreview.Popup.TilteContent')"/>'>
    <input id='tilteRequestPerson'           type="hidden" value='<s:property value="getText('text.productPreview.Popup.TilteRequestPerson')"/>'>
    <input id='tilteRequestDate'             type="hidden" value='<s:property value="getText('text.productPreview.Popup.TilteRequestDate')"/>'>
    <input id='tilteFinishDate'              type="hidden" value='<s:property value="getText('text.productPreview.Popup.TilteFinishDate')"/>'>
    <input id='correspondTxt'                type="hidden" value='<s:property value="getText('text.productPreview.Popup.buttonCorrespond')"/>'>
    <input id='uncorrespondTxt'              type="hidden" value='<s:property value="getText('text.productPreview.Popup.buttonUncorrespond')"/>'>
    <input id='titleCheckPerson1'            type="hidden" value='<s:property value="getText('text.productPreview.Popup.titleCheckPerson1')"/>'>
    <input id='titleCheckPerson2'            type="hidden" value='<s:property value="getText('text.productPreview.Popup.titleCheckPerson2')"/>'>
    <input id='tooltipCheckPerson'           type="hidden" value='<s:property value="getText('text.productPreview.Popup.TooltipCheckPerson')"/>'>
    <input id='tooltipCheckDate'             type="hidden" value='<s:property value="getText('text.productPreview.Popup.TooltipCheckDate')"/>'>
    <input id='oldStatus1'                   type="hidden" value=''>
    <input id='oldStatus2'                   type="hidden" value=''>
    <input id='confirmYes'                   type="hidden" value='<s:property value="getText('message.confirm.yes')"/>'>
    <input id='confirmCancel'                type="hidden" value='<s:property value="getText('message.confirm.no')"/>'>
    <input id=saveTabAllMessHdd              type="hidden" value='<s:property value="getText('saveCalibrationAll')"/>'>
    <input id='saveTabNoteMessHdd'           type="hidden" value='<s:property value="getText('saveCalibrationNote')"/>'>
    <input id='linkToEditHdd'                type="hidden" value='<s:property value="getText('btnEdit')"/>'>
    <input id='productEditURLHdd'            type="hidden" value='<s:property value="%{@net.webike.RcProductFactory.util.Constant@PRODUCT_EDIT_URL}"/>'>
    <input id='productIdParamNameHdd'        type="hidden" value='<s:property value="%{@net.webike.RcProductFactory.util.Constant@PARAM_PRODUCT_ID}"/>'>
    <input id='roleMasterShowTxt'            type="hidden" value='<s:property value="%{@net.webike.RcProductFactory.util.Constant@MASTER_SHOW_ROLE_ID}"/>'>
    <input id='roleMasterEditTxt'            type="hidden" value='<s:property value="%{@net.webike.RcProductFactory.util.Constant@MASTER_EDIT_ROLE_ID}"/>'>
    <input id='roleManageTxt'                type="hidden" value='<s:property value="%{@net.webike.RcProductFactory.util.Constant@USER_MANAGER_ROLE_ID}"/>'>
<%--     <input id='calibrationRequestTxt'        type="hidden" value='<s:property value="getText('calibrationRequest')"/>'> --%>
   <!--BOE rcv!Tuong.Luong add message confirm delete request-->
    <input type="hidden" id="confirmDeleteRequest" value="<s:property value="getText('confirmDeleteRequest')"/>">
    <input type="hidden" id="notSelectProduct" value="<s:property value="getText('deleteNoneProductSelected')"/>">  
   <!--EOE rcv!Tuong.Luong add message confirm delete request-->
    <!-- End Title text of pop up -->
    <!-- Begin Hidden for commbo box filter -->
    <input id='hiddenListUser' type="hidden" value='<s:if test="null != commonForm.listCommonUser">
                                                        <s:select disabled=""
                                                        id="lstUser"
                                                        list="commonForm.listCommonUser"
                                                        listKey="userId"
                                                        listValue="%{userLastName + ' ' + userFirstName}"
                                                        value=""
                                                        name="lstUser"
                                                        headerKey=" "
                                                        headerValue=" "
                                                        cssClass="cbbFilterUser" />
                                                    </s:if>'>
    <input id='hiddenListCalibrationType' type="hidden" value='<s:if test="null != productPreviewActionForm.listCalibrationType">
                                                        <s:select disabled=""
                                                        id="lstCalibrationType"
                                                        list="productPreviewActionForm.listCalibrationType"
                                                        listKey="calibrationRequestIndividualTypeCode"
                                                        listValue="calibrationRequestIndividualTypeName"
                                                        value=""
                                                        name="lstCalibrationType"
                                                        headerKey=" "
                                                        headerValue=" "
                                                        cssClass="cbbFilterCalibrationType" />
                                                    </s:if>'>
    <input type="hidden" id="jsonCalibrationType" value='<s:property value="productPreviewActionForm.lstCalibrationTypeJson"/>'>
    <!-- End Hidden for commbo box filter -->
    <script type="text/javascript" src="js/jQuery/selectable.js?20140804140000"></script>
    <script type="text/javascript" src="js/jQuery/kendo.custom.upload.js?20140804140000"></script>
    <div id="content" class="content">
    <!-- start of message area -->
    <input id='noDataMessage' type="hidden" value='<s:property value="getText('text.error.noData')"/>'>
     <s:if test="%{form.msgString != null && form.msgString != ''}">
          <div id="message" class="has-message success">
            <s:property value="form.msgString"/>
          </div>
     </s:if>
     <s:else>
        <div id="message">
        </div>
     </s:else>
     <!-- end of message area -->
   
    <!-- end of title -->
    <!-- Start New Block -->
    <div id="contentsInner">
        <!-- start list button div -->
        <div>
        <div style="padding-bottom: 5px;"></div>
            <!-- START BOOTSTRAP -->
		    <div class="row clearfix borderBottomGreen">
                <div class="col-md-12 column">
                    <!-- start block 1,2,3 -->
                    <div id="titleTop" class="contentInner" style="margin-bottom: 0px;margin-top: -18px;margin-left: -22px;margin-right: -22px;">
		                <div id="mainheaderContent" class="headerBlock" >
		                      <div style="padding:5px;height: 35px;">
		                          <!-- Start left header -->
		                          <div id="leftHeader">
		                              <input type="button" id="btnBackTop" class="btn-color-default k-button btnCrud" value="<s:property value='getText("back")'/>"/>
		                              <span> 
		                                  <span id="infoProductId">
		                                      <s:property value="getText('indexProduct')"/> : <span id="indexProductInMatter"></span> / <span id="totalProductInMatter"></span><s:property value='getText("item")'/>
		                                  </span>
		                                  <span id="infoGroup">
		                                      | <s:property value="getText('indexGroupInMatter')"/> : <span id="indexGroupInMatter"></span> / <span id="totalGroupInMatter"></span> <s:property value='getText("item")'/> </span>
		                                  </span>
		                          </div>
		                          <!-- End left header -->
		                          <div id="rightHeader">
		                              <s:property value="getText('changingWay')"/>
		                              <select id="changeProductMode">
		                                <option value="all"><s:property value="getText('all')"/></option>
		                                <option value="productNotInGroup"><s:property value="getText('productNotInGroup')"/></option>
		                                <option value="productInGroup"><s:property value="getText('productInGroup')"/></option>
		                              </select>
		                              <input type="button" id="btnEditTop"    class="btn-color-default k-button btnCrud btnRed" value="<s:property value='getText("btnEdit")' />">
		                              <input type="button" id="btnPrevTop" class="btn-color-default k-button btnCrud"        value="<s:property value='getText("previous")' />">
		                              <input type="button" id="btnNextTop" class="btn-color-default k-button btnCrud"        value="<s:property value='getText("next")' />">
		                          </div>
		                    </div>
		                    <!-- Param and URL for redirect page. -->
		                    <s:hidden id="matterDetailURL" value="%{@net.webike.RcProductFactory.util.Constant@MATTER_DETAIL_URL}"></s:hidden>
		                    <s:hidden id="paramMatterNo" value="%{@net.webike.RcProductFactory.util.Constant@PARAM_MATTER_ID}"></s:hidden>
		                    <s:hidden id="productEdtlURL" value="%{@net.webike.RcProductFactory.util.Constant@PRODUCT_EDIT_URL}"></s:hidden>
		                    <s:hidden id="paramProductId" value="%{@net.webike.RcProductFactory.util.Constant@PARAM_PRODUCT_ID}"></s:hidden>
		                    <!-- Hidden field data -->
		                    <s:hidden id="hProductGroupCode" value="%{productPreviewActionForm.factoryProductGeneral.productGroupCode}" ></s:hidden>
		                    <s:hidden id="hProductMatterNo" value="%{productPreviewActionForm.productMatterNo}" ></s:hidden>
		                    <s:hidden id="hProductId" value="%{productPreviewActionForm.productId}" ></s:hidden>
		                    <s:hidden id="hTmpImage" value="%{@net.webike.RcProductFactory.util.Constant@FOLDER_TMP_IMAGES_PRODUCT_SENTENCE}" ></s:hidden>
		                </div>
		            </div>
                     <!-- end block 1,2,3 -->
                </div>
            </div>
            
            <div class="row clearfix" style="border: 1px solid #DADADA;">
            <!--  BOE @rcv!Luong.Tuong 2014/07/15 #9955 :Add 5 validate label--> 
            <!-- Start verify 5 button block -->   
            <div class = "rowLabelButton">
				<div id="html" class="ItemValidate">
                	<s:property value="getText('text.label.validateLabel.header1')"/>   	
                </div>
             	<div id="thumbnail" class="ItemValidate">
                   	 <s:property value="getText('text.label.block2.window.thumbnails')"/>
                </div>             
                <div id="imageExists" class="ItemValidate">
                   	<s:property value="getText('text.title.block6.header')"/>
                </div>             
                <div id="fitModel" class="ItemValidate">
                   	<s:property value="getText('text.title.block11.header1')"/>
                </div>
                <div id="validateCode" class="ItemValidate">
                    <s:property value="getText('text.label.validateLabel.header5')"/>
                </div>
            </div>
             <!-- EOE @rcv!Luong.Tuong 2014/07/15 #9955 :Add 5 validate label--> 
                <!--BOE @rcv!Luong.Tuong 2014/07/15 #9955 :Add top line--> 
		        <!--<div class="col-md-12 column">  -->
		        <div class="col-md-12 column" style="border-top: solid 1px #ddd;">
		        <!--EOE @rcv!Luong.Tuong 2014/07/15 #9955 :Add top line--> 
		            <div class="row clearfix">
		                <div class="col-md-4 column">
		                    <div class="row clearfix borderBottom">
		                        <div class="col-md-12 column">
		                            <!-------------------------- start block 4 -------------------------->
                                      <div id="block4">
                                        <div id="imagePreviewBlock">
                                            <div id="imageBlock">
                                                <input type="hidden" id="noImageURL" value="<s:property value='form.noImageURL'/>">
                                                <input type="hidden" id="listThumbCount" value="<s:property value='form.listImage.size'/>">
                                                <input type="hidden" id="hUrlProductFactoryPath" value="<s:property value='form.urlProductFactoryPath'/>">
                                                <div class="products_main_image_inner">
                                                    <div id="imagelistView"></div>
                                                </div>
                                                <form name="thumb_selector_form">
                                                    <div id="listThumb" class="thumb_selector clearfix">
                                                       <div id="imagelistViewThumb"></div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                      </div>
                                      <!-------------------------- end block 4 -------------------------->
		                        </div>
		                    </div>
		                    <div class="row clearfix">
		                        <div class="col-md-12 column">
		                            <!-------------------------- start block 14 -------------------------->
                                      <div id="block14">
                                        <input type="hidden" id="titleGridAttributeName" value="<s:property value="getText('atributeName')"/>">
                                        <input type="hidden" id="titleGridAttributeValue" value="<s:property value="getText('contentInformation')"/>">
                                        <input type="hidden" id="titleGridAttributeDisplay" value="<s:property value="getText('displayName')"/>">
                                        <div class="content_block_14">
                                        	<div class="contentTitle"><s:property value="getText('attribute')"/>
	                                        <!-- BOE rcv!Tuong.Luong Add icon edit -->
	                                        	&nbsp;<input type="image" class="editIcon" src="./images/btn_editIcon.png" width="10" height="10" onclick="clickIconInsert(20)"/>
	                                        	<input id="requestToolTip_20" type="image" class="requestToolTip" src="./images/no.png" width="10" height="10" >
	                                   		<!-- EOE rcv!Tuong.Luong Add icon edit -->                                          
	                                   		</div>
                                          <div id="attributeTable"></div>
<%--                                           <div id="attributeTableNodata" class="dataTables_empty" style="display:none;"><s:property value="getText('noAttributeData')"/></div> --%>
                                       </div>
                                      </div>
                                      <!-------------------------- end block 14 -------------------------->
		                        </div>
		                    </div>
		                </div>
		                <div class="col-md-8 column borderLeft">
		                    <div class="row clearfix borderBottom">
		                        <div class="col-md-8 column">
		                            <div class="row clearfix borderBottom borderRight">
		                                <div class="col-md-12 column">
		                                    <!-------------------------- start block 5 -------------------------->
		                                    <div id="block5">
	                                            <div class="block5Content">
	                                                <div class="block5Title">
	                                                    <div class="rowContent">
	                                                        <p>
                                                                <span>
                                                                    <s:property value='getText("identificationNumber")'/> : <span id="viewProductId"></span>
                                                                </span>
                                                                <span style="margin-left: 15px;">
                                                                    <s:property value='getText("systemProductCode")'/> : <span id="viewProductSysCode"></span>
                                                                </span>
	                                                        </p>
	                                                    </div>
	                                                </div>
	                                                <div class="block5ColumnLeft">
	                                                    <div class="rowContent brandNameTooltip">
	                                                        <div class="rowDetail">
                                                                <span id="viewBrandName" class="rowContentSpan"></span>
                                                            </div>
                                                            <div class="rowEdit">
                                                                <input type="image" class="editIcon" src="./images/btn_editIcon.png" width="10" height="10" onclick="clickIconInsert(1)">
                                                                <input id="requestToolTip_1" type="image" class="requestToolTip" src="./images/no.png" width="10" height="10" >
                                                            </div>
                                                        </div>
	                                                    <div class="rowContent categoryNameTooltip">
	                                                        <div class="rowDetail">
	                                                           <span id="viewCategoryName" class="rowContentSpan"></span>
	                                                        </div>
	                                                        <div class="rowEdit">
	                                                           <input type="image" class="editIcon" src="./images/btn_editIcon.png" width="10" height="10" onclick="clickIconInsert(2)">
	                                                           <input id="requestToolTip_2" type="image" class="requestToolTip" src="./images/no.png" width="10" height="10" >
	                                                        </div>
	                                                    </div>
	                                                </div>
	                                                <div class="block5ColumnRight">
	                                                    <div class="rowContent siire1">
	                                                        <div class="rowDetail">
	                                                            <span id="viewSiireName1" class="rowContentSpan"></span>
															</div>
															<div class="rowEdit">
															    <input type="image" class="editIcon" src="./images/btn_editIcon.png" width="10" height="10" onclick="clickIconInsert(10)">
															    <input id="requestToolTip_10" type="image" class="requestToolTip" src="./images/no.png" width="10" height="10" >
															</div>
	                                                    </div>
	                                                    <div class="rowContent siire2">
	                                                        <div class="rowDetail">
	                                                            <span id="viewSiireName2" class="rowContentSpan"></span>
															</div>
<!-- 															<div class="rowEdit"> -->
<!-- 															    <input type="image" class="editIcon" src="./images/btn_editIcon.png" width="10" height="10" onclick="clickIconInsert(10)"> -->
<!-- 															</div> -->
	                                                    </div>
	                                                </div>
	                                            </div>
	                                        </div>
	                                        <!-------------------------- end block 5 -------------------------->
		                                </div>
		                            </div>
		                            <div class="row clearfix borderBottom borderRight">
		                                <div class="col-md-12 column">
		                                    <!-------------------------- start block 6 -------------------------->
		                                    <div id="block6">
	                                            <div class="block5Content">
	                                                <div class="block5Title">
	                                                    <div class="rowContent">
	                                                        <div class="borderLeftGreen tableEdit">
	                                                            <div class="inlineTable">
	                                                               <div id="viewProductName"></div>
	                                                            </div>
	                                                            <div class="pencil">
                                                                    <!-- BOE rcv!Tuong.Luong Add icon edit -->
	                                                                    &nbsp;<input type="image" class="editIcon" src="./images/btn_editIcon.png" onclick="clickIconInsert(3)"/>
	                                                                    <input id="requestToolTip_3" type="image" class="requestToolTip" src="./images/no.png" width="10" height="10" >
	                                                                <!-- EOE rcv!Tuong.Luong Add icon edit -->
                                                                </div>
	                                                        </div>
	                                                        <div class="">
	                                                            <div class="block5ColumnLeftProductCode tableEdit">
	                                                                <div class="borderLeftGreen rowProductCode inlineTable">
	                                                                    <span id="viewProductCode"></span>
	                                                                </div>
	                                                                <div class="pencil">
	                                                                    <!-- BOE rcv!Tuong.Luong Add icon edit -->
	                                                                    &nbsp;<input type="image" class="editIcon" src="./images/btn_editIcon.png" onclick="clickIconInsert(4)"/>
	                                                                    <input id="requestToolTip_4" type="image" class="requestToolTip" src="./images/no.png" width="10" height="10" >
	                                                                    <!-- EOE rcv!Tuong.Luong Add icon edit -->
	                                                                </div>
	                                                            </div>
	                                                            <div class="block5ColumnRightJanCode tableEdit">
	                                                                <div class="inlineTable">
	                                                                   <div id="viewProductJanCode"></div>
	                                                                </div>
	                                                                <div class="pencil">
	                                                                    <!-- BOE rcv!Tuong.Luong Add icon edit -->
	                                                                    &nbsp;<input type="image" class="editIcon" src="./images/btn_editIcon.png" onclick="clickIconInsert(5)"/>
	                                                                    <input id="requestToolTip_5" type="image" class="requestToolTip" src="./images/no.png" width="10" height="10" >
	                                                                    <!-- EOE rcv!Tuong.Luong Add icon edit -->
	                                                                </div>
	                                                            </div>
	                                                        </div>
	                                                    </div>
	                                                </div>
	                                                <div class="selectCode">
	                                                    <div class="block5Column">
	                                                       <p id="viewProductOpenPriceFlg"> <s:property value='getText("openPrice")'/> </p>
	                                                    </div>
	                                                    <div class="block5Column" style="width:85px;">
	                                                       <p id="viewProductProperSellingFlg"> <s:property value='getText("salePrice")'/> </p>
	                                                    </div>
	                                                    <div class="block5Column">
	                                                       <p id="viewProductOrderProductFlg"> <s:property value='getText("buildToOrderManufacturing")'/> </p>
	                                                    </div>
	                                                    <div class="block5Column">
	                                                       <p id="viewProductNoReturnableFlg"> <s:property value='getText("notReturnedOrExchanged")'/> </p>
	                                                    </div>
	                                                    <div class="block5Column" style="width:85px;">
	                                                       <p id="viewProductAmbiguousImageFlg"> <s:property value='getText("preferenceImage")'/> </p>
	                                                    </div>
	                                                    <!-- BOE rcv!Tuong.Luong Add icon edit -->
			                                        	&nbsp;<input type="image" style="margin-left: 5px;" class="editIcon" src="./images/btn_editIcon.png" onclick="clickIconInsert(6)"/>
			                                        	<input id="requestToolTip_6" type="image" class="requestToolTip" src="./images/no.png" width="10" height="10" >
			                                   			<!-- EOE rcv!Tuong.Luong Add icon edit -->
	                                                </div>
	                                                <div class="block5ColumnLeft tableEdit" style="margin-top: 5px;">
	                                                    <div class="price rowDetail">
	                                                        <div class="titlePrice">
	                                                            <p> <s:property value='getText("viewProperPrice")'/>: 
	                                                            </p>
	                                                        </div>
	                                                        <div class="redPrice tableEdit">
	                                                            <p id="viewProperPrice" class="inlineTable"></p>
	                                                        </div>
	                                                    </div>
	                                                    <div class="pencil">
                                                            <!-- BOE rcv!Tuong.Luong Add icon edit -->
                                                            &nbsp;<input type="image" class="editIcon inlineTable pencilPrice" src="./images/btn_editIcon.png" onclick="clickIconInsert(7)"/>
                                                            <input id="requestToolTip_7" type="image" class="requestToolTip" src="./images/no.png" width="10" height="10" >
                                                            <!-- EOE rcv!Tuong.Luong Add icon edit -->
                                                        </div>
	                                                    <div class="price">
	                                                        <div class="titlePrice">
	                                                            <p> <s:property value='getText("viewProperPriceTax")'/>: </p>
	                                                        </div>
	                                                        <div class="redPrice">
	                                                            <p id="viewProperPriceTax"></p>
	                                                        </div>
	                                                    </div>
	                                                </div>
	                                                <div class="block5ColumnRight tableEdit" style="margin-top: 5px;">
	                                                    <div class="price">
	                                                        <div class="titlePrice">
	                                                            <p> | &nbsp; <s:property value='getText("partition")'/> : </p>
	                                                        </div>
	                                                        <div class="redPrice">
	                                                            <p id="viewSupplierPriceRate"></p>
	                                                        </div>
	                                                    </div>
	                                                    <div class="pencil">
                                                            <!-- BOE rcv!Tuong.Luong Add icon edit -->
                                                            &nbsp;<input type="image" class="editIcon inlineTable pencilPrice" src="./images/btn_editIcon.png" onclick="clickIconInsert(8)"/>
                                                            <input id="requestToolTip_8" type="image" class="requestToolTip" src="./images/no.png" width="10" height="10" >
                                                            <!-- EOE rcv!Tuong.Luong Add icon edit -->
                                                        </div>
	                                                </div>
	                                            </div>
	                                        </div>
	                                        <!-------------------------- end block 6 -------------------------->
		                                </div>
		                            </div>
		                            <div class="row clearfix borderBottom borderRight">
		                                <div class="col-md-12 column">
		                                    <!-------------------------- start block 7 -------------------------->
		                                    <div id="block7">
	                                            <div class="block7Content">
	                                                <div class="titleBlock tableEdit">
	                                                   <div class="inlineTable">
	                                                       <p> <s:property value="getText('groupCode')"/> ：
                                                               <span id="groupCodeValue"></span>
                                                           </p>
	                                                   </div>
	                                                   <div class="inlineTable">
	                                                       <!-- BOE rcv!Tuong.Luong Add icon edit -->
	                                                       &nbsp;<input type="image" class="editIcon" src="./images/btn_editIcon.png" onclick="clickIconInsert(9)"/>
	                                                       <input id="requestToolTip_9" type="image" class="requestToolTip" src="./images/no.png" width="10" height="10" >
	                                                       <!-- EOE rcv!Tuong.Luong Add icon edit -->
	                                                   </div>
	                                                </div>
	                                                <div id="blockOptionContent"></div>
	                                            </div>
	                                        </div>
	                                        <!-------------------------- end block 7 -------------------------->
		                                </div>
		                            </div>
		                            <div class="row clearfix borderBottom borderRight">
		                                <div class="col-md-12 column">
		                                    <!-------------------------- start block 8 -------------------------->
		                                    <div id="block8">
	                                            <div class="block5Content">
	                                                <div class="titleBlock8">
	                                                    <span id="guestInputDescription"></span>
                                                         <input type="image" class="editIcon" src="./images/btn_editIcon.png" onclick="clickIconInsert(17)"/>
                                                         <input id="requestToolTip_17" type="image" class="requestToolTip" src="./images/no.png" width="10" height="10" >
	                                                </div>
	                                                <div id="guestInputContent">
	                                                </div>
	                                            </div>
	                                        </div>
	                                        <!-------------------------- end block 8 -------------------------->
		                                </div>
		                            </div>
		                            <div class="row clearfix borderBottom borderRight">
                                        <div class="col-md-12 column">
                                            <!-------------------------- start block 9 -------------------------->
                                            <!-- BOE rcv!Tuong.Luong Add icon edit -->
                                            <div>
												<input type="image" src="./images/btn_editIcon.png" class="editIcon btnIconBlock9" onclick="clickIconInsert(16)"/>
												<input id="requestToolTip_16" type="image" class="requestToolTip" src="./images/no.png" width="10" height="10" >
                                            </div>
                                            <!-- EOE rcv!Tuong.Luong Add icon edit -->
                                            <div id="block9">
		                                        <table id="tblFitModel" cellpadding="0" cellspacing="0" class="tablebase" style="">
		                                           <thead>
		                                                <tr>
		                                                  <th class='left k-header'><s:property value="getText('maker')" /></th>
		                                                  <th class='header-model  k-header'><s:property value="getText('model')" /></th>
		                                                </tr>
		                                           </thead>
		                                           <tbody>
		                                            
		                                            </tbody>
		                                        </table>
		                                        <span id="noFitModelData" style="display: none;"><s:property value="getText('noFitModelData')"/></span>
		                                    </div>
                                            <!-------------------------- end block 9 -------------------------->
                                        </div>
                                    </div>
		                        </div>
		                        <div class="col-md-4 column borderLeft" style="border-bottom: 1px solid #DADADA;">
		                            <!-------------------------- start block 15 -------------------------->
		                            <div id="top-right">
                                        <div class="top-rightHeader"><s:property value="getText('informationOption')"/></div>
                                        <div id="block15Content"></div>
                                    </div>
                                    <!-------------------------- end block 15 -------------------------->
		                        </div>
		                    </div>
		                    <div class="row clearfix">
		                        <div class="col-md-12 column borderBottom">
		                            <!-------------------------- start block 10 -------------------------->
		                            <div id="block10" class="contentInner ">
	                                    <!-- Start content 1 block 10 -->
	                                    <div class="content_block_10">
	                                        <div class="contentTitle">
	                                            <s:property value='getText("summary")'/>
			                                        <!-- BOE rcv!Tuong.Luong Add icon edit -->
			                                        &nbsp;<input type="image" class="editIcon" src="./images/btn_editIcon.png" onclick="clickIconInsert(12)"/>
			                                        <input id="requestToolTip_12" type="image" class="requestToolTip" src="./images/no.png" width="10" height="10" >
			                                   		<!-- EOE rcv!Tuong.Luong Add icon edit -->	                                        </div>
	                                        <div id="viewDescriptionSummary" class="content10">
	                                        </div>
	                                    </div>
	                                    <!-- End content 1 block 10 -->
	                                    <!-- Start content 2 block 10 -->
	                                    <div class="content_block_10">
	                                        <div class="contentTitle">
	                                            <s:property value='getText("caution")'/>
			                                        <!-- BOE rcv!Tuong.Luong Add icon edit -->
			                                        &nbsp;<input type="image" class="editIcon" src="./images/btn_editIcon.png" onclick="clickIconInsert(15)"/>
			                                        <input id="requestToolTip_15" type="image" class="requestToolTip" src="./images/no.png" width="10" height="10" >
			                                   		<!-- EOE rcv!Tuong.Luong Add icon edit -->	                                        </div>
	                                        <div id="viewDescriptionCaution" class="content10">
	                                        </div>
	                                    </div>
	                                    <!-- End content 2 block 10 -->
	                                    <!-- Start content 3 block 10 -->
	                                    <div class="content_block_10">
	                                        <div class="contentTitle">
	                                            <s:property value='getText("note")'/>
			                                        <!-- BOE rcv!Tuong.Luong Add icon edit -->
			                                        &nbsp;<input type="image" class="editIcon" src="./images/btn_editIcon.png" onclick="clickIconInsert(13)"/>
			                                        <input id="requestToolTip_13" type="image" class="requestToolTip" src="./images/no.png" width="10" height="10" >
			                                   		<!-- EOE rcv!Tuong.Luong Add icon edit -->	                                        
			                                   		</div>
	                                        <div id="viewDescriptionRemark" class="content10">
	                                        </div>
	                                    </div>
	                                    <!-- End content 3 block 10 -->
	                                    <!-- Start content 4 block 10 -->
	                                    <div class="content_block_10">
	                                        <div class="contentTitle">
	                                            <s:property value='getText("explanation")'/>
	                                            <!-- BOE rcv!Tuong.Luong Add icon edit -->
	                                             &nbsp;<input type="image" class="editIcon" src="./images/btn_editIcon.png" onclick="clickIconInsert(14)"/>
	                                             <input id="requestToolTip_14" type="image" class="requestToolTip" src="./images/no.png" width="10" height="10" >
	                                        	<!-- EOE rcv!Tuong.Luong Add icon edit -->
	                                        </div>
	                                        <div id="viewDescriptionSentence" class="content10">
	                                        </div>
	                                    </div>
	                                    <!-- End content 4 block 10 -->
	                                </div>
		                            <!-------------------------- end block 10 -------------------------->
		                        </div>
		                    </div>
		                    <div class="row clearfix borderBottom">
		                        <div class="col-md-12 column">
		                            <!-------------------------- start block 11 -------------------------->
	                                <div id="block11" class="contentInner ">
	                                    <div class="contentTitle">
	                                         <s:property value="getText('link')"/>
		                                        <!-- BOE rcv!Tuong.Luong Add icon edit -->
		                                        &nbsp;<input type="image" class="editIcon" src="./images/btn_editIcon.png" onclick="clickIconInsert(19)"/>
		                                        <input id="requestToolTip_19" type="image" class="requestToolTip" src="./images/no.png" width="10" height="10" >
		                                   		<!-- EOE rcv!Tuong.Luong Add icon edit -->	                                    </div>
	                                    <div class="content11">
	                                       <ul></ul>
	                                       <span id="noLinkData" class="dataTables_empty" style="display: none; width: 584px;">
	                                           <s:property value="getText('noLinkData')"/>
	                                       </span>
	                                    </div>
	                                </div>
	                                <!-------------------------- end block 11 -------------------------->
		                        </div>
		                    </div>
		                    <div class="row clearfix">
		                        <div class="col-md-12 column">
		                            <!-------------------------- start block 12 -------------------------->
	                                <div id="block12" class="contentInner">
	                                    <div class="contentTitle" style="margin-left: 9px;"><s:property value="getText('video')"/> 
	                                        <!-- BOE rcv!Tuong.Luong Add icon edit -->
	                                        &nbsp;<input type="image" class="editIcon" src="./images/btn_editIcon.png" onclick="clickIconInsert(18)"/>
	                                        <input id="requestToolTip_18" type="image" class="requestToolTip" src="./images/no.png" width="10" height="10" >
	                                   		<!-- EOE rcv!Tuong.Luong Add icon edit -->
	                                    </div>
	                                    <div id="formatVideo" style="display:none">
                                            <span class="videoTtl"> &bull; videoTitle</span>
                                            <table style="width: 100%;">
                                                <tr>
                                                    <td><iframe class="videoIframe" src="videoURL" frameborder="0" allowfullscreen></iframe>
                                                    <p>videoDescription</p></td>
                                                </tr>
                                            </table>
                                        </div>
	                                    <div id="block12Content">
	                                    </div>
	                                    <span id="noVideoData" class="dataTables_empty" style="display: none;">
                                        	<s:property value="getText('noVideoData')"/>
	                                    </span>
	                                </div>
	                                <!-------------------------- end block 12 -------------------------->
		                        </div>
		                    </div>
		                </div>
		            
		        </div>
		    </div>
        </div>
        <!-- Start panel bottom -->
        <div class="row clearfix borderDefault">
            <div class="col-md-12 column">
                <!-- start block 13 -->
                <div id="bottom-pane">
                  <div style="padding:5px;">
                    <input type="button" id="btnBackBottom" class="btn-color-default k-button btnCrud" value="<s:property value='getText("back")'/>"/>
                    <input type="button" id="btnEditBottom" class="btn-color-default k-button btnRed" style="margin-left: 516px;" value="<s:property value='getText("btnEdit")' />">
                    <input type="button" id="btnPrevBottom" class=" btn-color-default k-button btnCrud" value="<s:property value='getText("previous")'/>">
                    <input type="button" id="btnNextBottom" class=" btn-color-default k-button btnCrud" value="<s:property value='getText("next")' />">
                  </div>
                </div>
                 <!-- end block 13 -->
            </div>
        </div>
        <!-- End panel bottom -->
        <!-- END BOOTSTRAP -->
    </div>
    <!-- End New Block -->
    <!-- BOE @rcv!dau.phuong 20140715 Block: Request Popup -->
    <div id="popUpCheck">
      <div id="divCheck">
            <div id="checkPerson">
                <div class="top-rightHeader headerPopup"><s:property value="getText('text.productPreview.Popup.titleCheckPerson')"/></div>
                    <div id="divCheckbox">
	                    <div class="checkOneTooltip">
	                        <p id="viewCheckOne">
	                           <label for="checkOne" class="lblCheckPerson"><input id="checkOne" type="checkbox" class="checkPerson">
	                               <s:property value="getText('text.productPreview.Popup.titleCheckPerson1')"/>
	                               &nbsp;<span id="numberCheckFirst"></span> / <span class="total_product_matter"></span> 
                               </label>
	                        </p>
	                    </div>
	                    <div class="checkTwoTooltip">
	                        <p id="viewCheckTwo">
	                           <label for="checkTwo" class="lblCheckPerson"><input id="checkTwo" type="checkbox" class="checkPerson">
	                               <s:property value="getText('text.productPreview.Popup.titleCheckPerson2')"/>
	                               &nbsp;<span id="numberCheckSecond"></span> / <span class="total_product_matter"></span>
                               </label>
	                        </p>
	                    </div>
                    </div>
                </div>
            <div id="checkContent">
                <div id="tabstrip">
                    <ul>
                        <li class="k-state-active"><s:property value="getText('text.productPreview.Popup.titleTab1')"/></li>
                        <li id="tabContentIndividual"><s:property value="getText('text.productPreview.Popup.titleTab2')"/></li>
                        <li><s:property value="getText('text.productPreview.Popup.titleTab3')"/></li>
                    </ul>
                    <div id="tabAll">
                        <span style=" float: left; "><s:property value="getText('calibrationAllHeader')"/></span>
                        <div id="allLastUpdateTxt" class="lastUpdateTxt">
                            <s:property value="getText('updatedOn')"/>:&nbsp;<span id="allLastUpdateDateTxt"></span>&nbsp;
                            <s:property value="getText('lastUpdatedPerson')"/>:&nbsp;<span id="allLastUpdateUserTxt"></span>
                        </div>
                        <textarea id="allCommentTta" rows="22" cols="145"></textarea>
                        <input type="button" id="allCancelBtn" class="cancelBtn btn-color-default lstButton k-button" value='<s:property value="getText('cancel')"/>'>
                        <input type="button" id="allSaveBtn" class="saveBtn btn-color-default lstButton k-button" value='<s:property value="getText('save')"/>'>
                    </div>
                    <div>
                        <div id="lstButtonCheckPopup">
                             <label class="lblCheckBoxAll" for="checkBoxAll">
                                <input type="checkbox" id="checkBoxAll" class="checkBoxAll" >&nbsp;<s:property value="getText('all')"/>
                            </label>
                            <input type="button" id="newRowPopup" class="btn-color-default lstButton k-button" id="btnInsert" value="<s:property value="getText('text.productPreview.Popup.buttonInsertRow')"/>">
                            <input type="button" class="btn-color-default lstButton k-button btnRed" id="btnDelete" value="<s:property value="getText('text.productPreview.Popup.buttonDelete')"/>">
                        </div>
                        <div id="mainContent">
                            <!-- start table list master -->
                            <div id="table-content">
                                <!-- start table list master content -->
                                <div id="grid">
                                </div>
                                <!-- end table list master content -->
                            </div>
                            <!-- end table list master -->
                        </div>
                    </div>
                    <div id="tabNote">
                        <span style=" float: left; "><s:property value="getText('calibrationNoteHeader')"/></span>
                        <div id="noteLastUpdateTxt" class="lastUpdateTxt">
                            <s:property value="getText('updatedOn')"/>:<span id="noteLastUpdateDateTxt"></span>&nbsp;
                            <s:property value="getText('lastUpdatedPerson')"/>:<span id="noteLastUpdateUserTxt"></span></div>
                        <textarea id="noteCommentTta" rows="22" cols="145"></textarea>
                        <input type="button" id="noteCancelBtn" class="cancelBtn btn-color-default lstButton k-button" value='<s:property value="getText('cancel')"/>'>
                        <input type="button" id="noteSaveBtn" class="saveBtn btn-color-default lstButton k-button" value='<s:property value="getText('save')"/>'>
                    </div>
                </div>
            </div>
       </div>
     </div>
    <!-- EOE @rcv!dau.phuong 20140715 Block: Request Popup -->
                    </div>
    </div>
</div>
<!--End Contents -->
</tiles:putAttribute>
</tiles:insertDefinition>
<!-- begin display image ProductImage in ListView -->
<script type="text/x-kendo-template" id="templateImageBlockx">
<div class="imageDetail">
    <a id="imageAhref_#:productImage#" rel="fancy_box" href="#:productImage#" class="fancy_box product">
        <div class="product-description">
            <h3>#:productImage#</h3>
            <p>#:productImage#</p>
        </div>
        <img  id="imageThumb" src="#:productImage#" alt="No Image">
    </a>
</div>
</script>
<script type="text/x-kendo-template" id="templateImageBlock2">
<div class="viewImageContainer">
<div class="viewImage">
    <a id="imageAhref_#:productImage#" alt="No Image" rel="fancy_box" onclick="return false;" href="#:productImage#"
        class="fancy_box product fancy">
        <img  id="imageThumb" src="#:productImage#" alt="No Image">
    </a>
    <div id="imageInfo" class="imageInfo">
    <!-- BOE rcv!Tuong.Luong Add icon edit -->
		<div class="rowEditIamge">
			<input type="image" id="editIconBlockImage" src="./images/btn_editIcon.png" class="editIcon btnIcon" onclick="clickIconInsert(11)"/>
            <input id="requestToolTip_11" type="image" class="requestToolTip" src="./images/no.png" width="10" height="10" >
		</div>
	<!-- EOE rcv!Tuong.Luong Add icon edit -->
        <ul>
            <li id='viewDetailPath'>詳細画像パス: #:productImageDetailPath#</li>
            <li id='viewThumbnailPath'>サムネイルパス: #:productImageThumbnailPath#</li>
            <li id='dimensionDetailImage'>詳細画像サイズ: </li>
            <li id='dimensionThumbnailImage'>サムネイル画像サイズ: </li>
        </ul>
    </div>
</div>
</div>
</script>
<!-- end display image ProductImage in ListView -->
<!-- begin display image ProductThumb in ListView -->
<script type="text/x-kendo-template" id="templateImageThumbBlock2">
    <li thumbid="0" class="list_thumb" style="display: list-item;">
        <img class="selector_thumb" src="#:productThumb#">
        #if(productImage != "./images/no-image.jpg") {#
            <a href="#:productImage#" class="fancy_box" rel="fancy_box" onclick="return false;" style="display:none;">
                <img class="selector_thumb" src="#:productImage#">
            </a>
       #}#
    </li>
</script>
<!-- end display image ProductThumb in ListView -->

<!-- ######################### Hidden field ######################### -->
<input id="hdGuestInputDescription"			type="hidden"	value="<s:property value="getText('guestInputDescription')"/>" />
<input id="viewMoreModel"					type="hidden"	value="<s:property value="getText('viewMoreModel')"/>" />
<input id="viewLessModel"					type="hidden"	value="<s:property value="getText('viewLessModel')"/>" />
<input id="pleaseChoiseOption"				type="hidden"	value="<s:property value="getText('pleaseChoiseOption')"/>" />
<input id="viewDimensionThumbnail"			type="hidden"	value="<s:property value="getText('dimensionThumbnail')"/>" />
<input id="viewDimensionDetail"             type="hidden"   value="<s:property value="getText('dimensionDetail')"/>" />
<input id="hdCategory"                      type="hidden"   value="<s:property value="getText('category')"/>" />
<input id="hdBrand"                         type="hidden"   value="<s:property value="getText('brand')"/>" />
<input id="hdSupplier"                      type="hidden"   value="<s:property value="getText('supplier')"/>" />
<input id="hdItemCode2"                     type="hidden"   value="<s:property value="getText('itemCode2')"/>" />
<input id="hdBrandCode"                     type="hidden"   value="<s:property value="getText('brandCode')"/>" />
<input id="yenSymbol"                     	type="hidden"   value="<s:property value="getText('yenSymbol')"/>" />
<input id="hdProductWebikeFlag"             type="hidden"   value="<s:property value="getText('notStockNumber')"/>" />
<input id="hdNoMakerLogo"             		type="hidden"   value="<s:property value="%{@net.webike.RcProductFactory.util.Constant@NO_LOGO_OF_MAKER}"/>" />
<input id="viewSiireCode"                   type="hidden"   value="<s:property value="getText('siireCode')"/>" />
<input id="viewNoukiCode"                   type="hidden"   value="<s:property value="getText('noukiCode')"/>" />
<input id="viewSupplierName"                type="hidden"   value="<s:property value="getText('supplier')"/>" />
<input id="guestInputSeparater"             type="hidden"   value="<s:property value="getText('guestInputSeparater')"/>" />
<input id="viewProductCode2"                type="hidden"   value="<s:property value="getText('productCode2')"/>" />
<input id="detailedImagePath"               type="hidden"   value="<s:property value="getText('detailedImagePath')"/>" />
<input id="thumbnailPath"                   type="hidden"   value="<s:property value="getText('thumbnailPath')"/>" />
<input id="viewJan"                         type="hidden"   value="<s:property value="getText('jan')"/>" />
<!--BOE rcv!Tuong.Luong Popup Confirm Delete Request -->
<div id="deleteRequestDialog">
    <input type="hidden" id="windowNamedeleteRequestDialog" value="<s:property value="getText('confirmation')"/>" />
	<div class="requestManageDialogContainer">
		<div class="requestManageDialogContent">
			<div>
				<s:property value="getText('confirmDeleteRequest')"/>
			</div>
		</div>
		<input type="button" class="k-button" id="btnDeleteRequestOk" 
			value='<s:property value="getText('text.btn.yes')"/>' />
		<input type="button" class="k-button" id="btnDeleteRequestCancel" 
			value='<s:property value="getText('text.label.btnCancel')"/>' />
	</div>
</div>
<div id="pleaseSelectDialog" >
    <input type="hidden" id="windowNamepleaseSelectDialog" value="<s:property value="getText('confirmation')"/>" />
	<div class="requestManageDialogContainer">
		<div class="requestManageDialogContent" style="color:red;">
			<s:property value="getText('deleteNoneProductSelected')"/>
		</div>
		<input type="button" class="k-button" id="pleaseSelectDialogOk" 
			value='<s:property value="getText('text.btn.yes')"/>' />
	</div>
</div>
<!--EOE rcv!Tuong.Luong Popup Confirm Delete Request -->

