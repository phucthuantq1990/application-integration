<%@ page language="java" contentType="text/html; charset=Shift-JIS"
    pageEncoding="Shift-JIS"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<tiles:insertDefinition name="layoutCommon">
<tiles:putAttribute name="css">
    <link rel="stylesheet" type="text/css" href="css/jQuery/kendo.common.min.css?20140804140000" />
    <link rel="stylesheet" type="text/css" href="css/categoryManage.css?20140804140000" />
</tiles:putAttribute>
<tiles:putAttribute name="javaScript">
    <script type="text/javascript" src="js/jQuery/kendo.all.min.js?20140804140000"></script>
    <script type="text/javascript" src="js/jQuery/jquery.validate.min.js?20140804140000"></script>
    <script type="text/javascript" src="js/categoryManage.js?20140804140000"></script>
    <script type="text/javascript" src="js/commonJS.js?20140804140000"></script>
</tiles:putAttribute>
<tiles:putAttribute name="body">
<!-- Contents -->
<div id="contents">
    <div id="content">
        <s:if test="%{categoryManageActionForm.message != null && categoryManageActionForm.message != ''}">
                 <div id="message" class="has-message success">
                     <s:property value="categoryManageActionForm.message"/>
                 </div>   
         </s:if>
         <s:else>
            <div id="message">
            </div>
         </s:else>
        <!-- start title Management of Brand Master -->
        <div id="title">
            <h1><s:property value="getText('text.label.category.categoryManageH1')"/></h1>
            <!-- 
            <a id="btnAddNewBrand" class="btn-size-small btn-color-default k-button" href="categoryEdit.html">
                <s:property value="getText('text.label.category.btnAddNewCategory')"/>
            </a>
             -->
             
             <input type="button" class="btn-color-default lstButton k-button" id="createCategory" 
             value="<s:property value="getText('text.label.category.btnAddNewCategory')"/>">
        </div>
        <!-- end title Management of Brand Master -->
        <!-- start add filter -->
            <jsp:include page="component/addFilter.jsp"></jsp:include>
        <!-- end add filter -->
        <!-- start contents_inner -->
        <div id="contentsInner">
            <!-- start list button div -->
            <div id="lstButton">
                <input type="button" class="btn-color-default lstButton k-button" id="btnBack" value="<s:property value="getText('text.label.btnBackToPreviousScreen')"/>">
                <input type="button" class="btn-color-default lstButton k-button" id="btnResetFilter" value="<s:property value="getText('text.label.btnResetFilter')"/>">
                <input type="button" class="btn-color-default lstButton k-button" id="btnSearch" value="<s:property value="getText('text.label.btnSearch')"/>">
            </div>
            <!-- end list button div -->
            <!-- start table list master -->
            <div id="table-content">
                <!-- start table list master content -->
                <div id="grid">
                </div>
                <!-- end table list master content -->
            </div>
            <!-- end table list master -->
        </div>        
	    <!-- No data message -->
	    <input id='noDataMessage' type="hidden" value='<s:property value="getText('text.error.noData')"/>'>
	    <!-- Host Address -->
	    <input id='hostAddress' type="hidden" value='<s:property value='hostAddress'/>'>
	    <!-- Header of grid -->
	    <input id="tbCategoryCode" type="hidden" value='<s:property value="getText('text.label.category.categoryCode')"/>'>
	    <input id="tbCategoryName" type="hidden" value='<s:property value="getText('text.label.category.categoryName')"/>'>
	    <input id="tbBunruiCode" type="hidden" value='<s:property value="getText('text.label.category.bunruiCode')"/>'>
	    <input id="tbBunruiName" type="hidden" value='<s:property value="getText('text.label.category.bunruiName')"/>'>
	    <input id="tbCategoryAttributeCount" type="hidden" value='<s:property value="getText('text.label.category.categoryAttributeCount')"/>'>
	    <input id="tbCategoryDelFlg" type="hidden" value='<s:property value="getText('text.label.category.categoryDelFlg')"/>'>
	    <input id="tbUpdatedOn" type="hidden" value='<s:property value="getText('text.label.category.updatedOn')"/>'>
	    <!-- Title for paging -->
	    <input id='pagingDisplay'       type="hidden" value='<s:property value="getText('text.paging.display')"/>'>
	    <input id='pagingEmpty'         type="hidden" value='<s:property value="getText('text.paging.empty')"/>'>
	    <input id='pagingItemsPerPage'  type="hidden" value='<s:property value="getText('text.paging.itemsPerPage')"/>'>
	    <input id='pagingFirst'         type="hidden" value='<s:property value="getText('text.paging.first')"/>'>
	    <input id='pagingPrevious'      type="hidden" value='<s:property value="getText('text.paging.previous')"/>'>
	    <input id='pagingNext'          type="hidden" value='<s:property value="getText('text.paging.next')"/>'>
	    <input id='pagingLast'          type="hidden" value='<s:property value="getText('text.paging.last')"/>'>
        <!-- end contentsInner -->        
    </div>
</div>    
<!--End Contents -->
</tiles:putAttribute>
</tiles:insertDefinition>