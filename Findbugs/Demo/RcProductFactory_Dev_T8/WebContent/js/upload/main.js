$(function() {
    previewImage();
    var LOGO_DB = $("#brandLogo").val();
    if(LOGO_DB != '') {
    	enableButton("btnDeleteImg");
    }
    else {
    	disableButton("btnDeleteImg");
	}
    
    // always disable text field image
    $("#logo").prop("disabled", true);
    // first init screen set default logo = blank
    $("#logo").val("");
   
});

// function preview image
function previewImage(){
    $('#fileupload').change(function() {
    	/*BOE Thai.Son 2014/01/17*/
    	$('#flagCheckImageChange').val("true");
    	/*BOE Thai.Son 2014/01/17*/
    	// validate image preview not for IE
    	if (!$.browser.msie) {
	    	var file = $('#fileupload').prop('files')[0];
	       	var size = file.size;
	    	// max Size  = 20kb
	    	var maxSize = 20480 ;
	    	
	    	if (size > maxSize ) {
	    		// clear error span
	    		clearErrorSpan();
	    		$("#errorImage0").text(MAX_SIZE_UPLOAD);
	    		$("#logo").addClass("error");
	    		clearAttr();
	    		return;
	    	}
    	}
    	
        $(this).upload('previewImage.html', function(res) {
        	if(res.imageError != ''){
        		// clear error span
        		clearErrorSpan();
        		var arrayResult = res.imageError.split("-");
        		for(var i = 0; i < arrayResult.length; i++) {
        			$("#errorImage"+i).text(arrayResult[i]);
        		}
        		$("#logo").addClass("error");
        		clearAttr();
        	}
        	else {
        		// clear error span
        		clearErrorSpan();
        		var userLoginId = $("#userLoginId").val();
        		$('#imageUpload').attr("src","tmp_upload/" + userLoginId + "--" + res.imageName);
        		$("#logo").val(res.imageName);
        		$("#logoHidden").val(res.imageName);
        		setAttrPreviewSucces();
        		$("#logo").removeClass("error");
        	}
        	
        }, 'json');
    });
    
}

//function delete image
function deleteImagePreview() {
	
	/*BOE Thai.Son 2014/01/17*/
	$('#flagCheckImageChange').val("true");
	/*BOE Thai.Son 2014/01/17*/
	
	var imageNameHidden  = $("#logoHidden").val();
	$.ajax({
		type : 'POST',
		url : 'deleteImagePreview.html',
		dataType : 'json',
 		 data : {
		'imageName' : imageNameHidden
 		 }, 
		success : function(data) {
			clearAttr();
    		// clear error span
    		clearErrorSpan();
		}
	});
}

// set some attr when preview image success
function setAttrPreviewSucces() {
	$("#fileuploadError").text("");
	$('#fileupload').replaceWith("<input type=\"file\"  name=\"userImage\" id=\"fileupload\" onclick=\"javascript:previewImage();\" />");
	enableButton("btnDeleteImg");
}

//set some attr when preview image Errow
function clearAttr() {
	$('#fileupload').replaceWith("<input type=\"file\" name=\"userImage\" id=\"fileupload\" onclick=\"javascript:previewImage();\" />");
	$("#logo").val("");
	$("#logoHidden").val("");
	$('#imageUpload').attr("src","images/noimage_100.gif");
	disableButton("btnDeleteImg");
	
}

//function disable button
function disableButton(buttonId) {
	$("#"+buttonId).prop("disabled",true);
}	

//function enable button
function enableButton(buttonId) {
	$("#"+buttonId).prop("disabled",false);
}	
// function clearErrorSpan
function clearErrorSpan() {
	$("#errorImage0").text("");
	$("#errorImage1").text("");
}
