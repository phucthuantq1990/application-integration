// BOE #6634 No.46 Hoang.Ho 2014/02/19
var kendoValidatable = null;
var kendoValidatablePopUp = null;
// EOE #6634 No.46 Hoang.Ho 2014/02/19
// BOE #6344 No.64 Thai.Son 2014/02/19 Change confirm pupup to kendo window
//Show confirm message when back previous page
function confirmBack() {
    /*jQuery.fancybox({
        modal : true,
        autoSize: true,
        content : "<div>"
                +"     <div id='heading-popup'>"
                +		jQuery("#title-confirm-back").val()
        		 +"     </div>"
        		 +"     <div id='content-popup'>"
    			 + "    <p>" 
    			 +         jQuery("#content-confirm-back").val()
    			 + "    </p>"
    			 +"     <div style='text-align:center;padding:5px;'>"
    			 + "        <input id='fancyConfirm_ok' class='btn-size-medium btn-color-blue' type='button' "
    			 + "	value='" + jQuery("#btn-yes").val() + "' onclick='backToPreviousPage()' style='margin:7px;padding:5px;width:67px;'>"
		         + "        <input id='fancyConfirm_cancel' class='btn-size-medium btn-color-default' type='button' "
		         + " value='" + jQuery("#btn-no").val() + "' onclick='jQuery.fancybox.close();' style='margin:7px;padding:5px;width:67px;'>"
			     +"     </div>"
		         +"     </div>"
		         + "</div>"
    });*/
	//BOE THAI.SON 21-2
	/*if(checkDataNotModified()){
		backToPreviousPage();
	}else {
    	// open popup
    	var win = $("#confirmBackDialog").data("kendoWindow");
    	win.center();
    	win.open();
    }*/
	if($('#insertMode').val() == 1){
		if (checkValueChangeWhenAddNew()) {
			// open popup confirm:
	    	var win = $("#confirmBackDialog").data("kendoWindow");
	    	win.center();
	    	win.open();	
		}else{
			backToPreviousPage();
		}
	}else{
		if(checkDataNotModified()){
			backToPreviousPage();
		}else {
	    	// open popup
	    	var win = $("#confirmBackDialog").data("kendoWindow");
	    	win.center();
	    	win.open();
	    }
	}
	//EOE THAI.SON 21-2
}

	
//Back to the previous screen
//If doesn't exists, go to Top page
function backToPreviousPage() {
	/*//Show confirm message
	confirmBack();
	jQuery("#fancyConfirm_cancel").focus();
    //Get previous URL
    var previousUrl = document.referrer;
    if ("" != previousUrl) {
        window.history.go(-1);
        return;
    }
    //Go to Top page
    window.location = defaultRedirectPage;*/
	/* BOE by Luong.Dai at 2014/04/08 Show progress bar */
	showProcessBar();
	/* EOE by Luong.Dai at 2014/04/08 Show progress bar */
	window.location = "./attributeManage.html";
}
//EOE #6344 No.64 Thai.Son 2014/02/19 Change confirm pupup to kendo window
//Show message at tab attributeGroup
function showMessage(message) {
    $("#group-message").text(message);
}
//BOE #6634 No.46 Hoang.Ho 2014/02/19 Change to kendo validate
//Validate attributeName: required, length <= 256
function validateAttributeName() {
    var result = false;

//    if ($("#validated-attr-name").val() == 0) {
        var attrName = $("#attrName").val();
//        var labelAttrName = $("#labelAttrName").text();

        if (null == attrName || "" == attrName) {
//            var message = jQuery("#requiredFill").val();
//            var showMessage = message.replace('{0}', labelAttrName);
//
//            $("#spnAttrNameError").text(showMessage);
//            $("#attrName").css("background-color", "#FFD9E6");

            result = false;
//        } else if (attrName.trim().length > 256) {
//            var message = jQuery("#max-length").val();
//            var showMessage = message.replace('{0}', 256);
//
//            $("#spnAttrNameError").text(showMessage);
//            $("#attrName").css("background-color", "#FFD9E6");
//
//            result = false;
        } else {
//            $("#spnAttrNameError").text("");
//            $("#attrName").css("background-color", "#FFFFFF");

            result = true;
        }
//    } else {
//        result = true;
//    }

    return result;

}

//function validateAttributeType() {
//    var result = false;
//    //Get selected attribute Type
//    var attrTypeValue = $('#attributeTypeList :selected').val();
//    // check for mode update.
//    if (attrTypeValue == undefined) {
//    	return true;
//    }
//    var label = $("#lblAttrType").text();
//
//    if (null == attrTypeValue || "" == attrTypeValue) {
//        var message = jQuery("#requiredFill").val();
//        var showMessage = message.replace('{0}', label);
//
//        $("#spnAttrType").text(showMessage);
//        $("#attributeTypeList").css("background-color", "#FFD9E6");
//        $("#attributeTypeList option").css("background-color", "#ffffff");
//
//        result = false;
//    } else {
//        $("#spnAttrType").text("");
//        $("#attributeTypeList").css("background-color", "#FFFFFF");

//        result = true;
//    }
//    return result;
//}
//EOE #6634 No.46 Hoang.Ho 2014/02/19 Change to kendo validate
function showMessage2(message, type) {
    jQuery("#message").text(message);

    if (null == message || "" == message) {
        jQuery("#message").removeClass("has-message");
    } else {
        jQuery("#message").addClass("has-message");
    }

    //Type = 1: Notice
    //Type = 2: Error
    if (type == "1") {
        jQuery("#message").addClass("success");
    } else {
        jQuery("#message").addClass("msg-error");
    }
}

//BOE #6634 No.46 Hoang.Ho 2014/02/19 Change to kendo validate
//function validateAttributeLengthLimit() {
//    var result = false;
//    //Get selected attribute Type
//    var attrValue = $('#attributeTypeList :selected').val();
//
//    if (attrValue == "Flag") {
//        $("#spnAttrLengthError").text("");
//        $("#attrLengthLimit").css("background-color");
//        result = true;
//    } else {
//        var lengthValue = $("#attrLengthLimit").val();
//        var label = $("#lblLengthLimit").text();
//
//        if (null == lengthValue || "" == lengthValue) {
//            var message = jQuery("#requiredFill").val();
//            var showMessage = message.replace('{0}', label);
//
//            $("#spnAttrLengthError").text(showMessage);
//            $("#attrLengthLimit").css("background-color", "#FFD9E6");
//
//            result = false;
//        } else {
//            $("#spnAttrLengthError").text("");
//            $("#attrLengthLimit").css("background-color", "#FFFFFF");

//            result = true;
//        }
//    }
//
//    return result;
//}
//EOE #6634 No.46 Hoang.Ho 2014/02/19 Change to kendo validate

function validateAttributeGroupName() {
    var result = false;
    var attrGroupName = $("#popAttributeGroupName").val();
    var groupNameText = $("#hAttrGroupName").val();
    var requiredText = $("#requiredFill").val();
    var duplicatedText = $("#duplicateTextWithParam").val();
    var msgRequired = requiredText.replace('{0}', groupNameText);
    var msgDuplicated = duplicatedText.replace('{0}', groupNameText);
    if (null == attrGroupName || "" == attrGroupName) {
        $("#popAttributeGroupName").css("background-color", "#FFD9E6");
        $("#attributGroupNameRequired").text(msgRequired);
        result = false;
    } else if (checkDuplicateName(attrGroupName)) {
        $("#popAttributeGroupName").css("background-color", "#FFD9E6");
        $("#attributGroupNameRequired").text(msgDuplicated);
        result = false;
    } else {
        $("#popAttributeGroupName").css("background-color", "#FFFFFF");
        $("#attributGroupNameRequired").text("");
        result = true;
    }

    return result;
}

function validateAttributeSort() {
    var result = false;
    var sort = $("#popAttributeSort").val();
    //boe thai.son: do dung kendoValidate, nen ko xuat msg kieu nay, son comment lai.
    //var sortText = $("#hAttrSort").val();
    //var requiredText = $("#requiredFill").val();
    //var duplicatedText = $("#duplicateTextWithParam").val();
    //var msgRequired = requiredText.replace('{0}', sortText);
    //var msgDuplicated = duplicatedText.replace('{0}', sortText);
    if (!validateNumberic(sort)) {
        $("#popAttributeSort").css("background-color", "#FFD9E6");
        //$("#attributGroupSortRequired").text(msgRequired);
        result = false;
    } else if (checkDuplicateSort(sort)) {
        $("#popAttributeSort").css("background-color", "#FFD9E6");
      //$("#attributGroupSortRequired").text(msgDuplicated);
      //eoe thai.son: do dung kendoValidate, nen ko xuat msg kieu nay, son comment lai.
        return false;
    } else {
        $("#popAttributeSort").css("background-color", "#FFFFFF");
        $("#attributGroupSortRequired").text("");
        result = true;
    }

    return result;
}
//check duplicate attribute group name value
function checkDuplicateName(value) {
    var result = false;
    var currentCode = $("#popAttributeGroupCode").val();

    var grid = $("#kendoGrid").data("kendoGrid");
    var gridData = grid.dataSource.view();

    var length = gridData.length;
    for (var i = 0; i < length; i++) {
        var nameValue = gridData[i].get("attributeGroupName");
        var groupCode = gridData[i].get("attributeGroupCode");
        if (currentCode != null) {
            //Mode insert group
            if (currentCode != groupCode && nameValue == value) {
                result = true;
                break;
            }
        } else {
            //Mode new group
            if (nameValue == value) {
                result = true;
                break;
            }
        }
    }

    return result;
}

//check duplicate sort value
function checkDuplicateSort(value) {
    var result = false;
    var currentCode = $("#popAttributeGroupCode").val();
    //Get grid of attribute group
    var grid = $("#kendoGrid").data("kendoGrid");
    var gridData = grid.dataSource.view();

    var length = gridData.length;
    for (var i = 0; i < length; i++) {
        var sortValue = gridData[i].get("attributeSort");
        var groupCode = gridData[i].get("attributeGroupCode");
        //compare with new attribute sort value 
        if (currentCode != null) {
            //Mode insert group
            if (currentCode != groupCode && parseInt(sortValue) == parseInt(value)) {
                result = true;
                break;
            }
        } else {
            //Mode new group
            if (parseInt(sortValue) == parseInt(value)) {
                result = true;
                break;
            }
        }
    }

    return result;
}

/* 
 * prepare data before transfer to server.
 * 1. Get data from kendogrid data source
 * 2. Iterate in data source and add into JsonString.
 */
function prepareAttrGroupData() {
    var strJson = "";
    //1. Get data from kendogrid data source
    var grid = $("#kendoGrid").data("kendoGrid");
    var dataSource = grid.dataSource;
    var raw = dataSource.data();
    var length = raw.length;
    var groupId = 0;

    //2. Iterate in data source and add into JsonString. 
    var item, i;
    for (i = 0; i < length; i++) {
        item = raw[i];
        // set id = 0, id id < -1 (new insert)
        groupId = (item.attributeGroupCode < 0) ? 0 : item.attributeGroupCode;
        strJson +=
            "{" +
            "\"attributeGroupCode\":" + groupId + "," + "\"attributeCode\":" + item.attributeCode + "," + "\"attributeGroupName\":\"" + item.attributeGroupName + "\"," + "\"attributeSort\":" + item.attributeSort + "," + "\"delFlg\":" + item.delFlg + "},";
    }
    strJson = "[" + strJson.substring(0, strJson.length - 1) + "]";
    return strJson;
}
//boe thai.son 21-2
function checkValueChangeWhenAddNew() {
	var attrName = jQuery("#attrName").val();
	if(attrName.trim() != ""){
		return true;
	}else{
		var attributeTypeList = jQuery("#attributeTypeList").val();
		if(attributeTypeList.trim() != ""){
			return true;
		}else{
			var delFlg = jQuery("#delFlg").val();
			if(delFlg != "0"){
				return true;
			}else{
				var attrLengthLimit = jQuery("#attrLengthLimit").val();
				if(attrLengthLimit.trim() != ""){
					return true;
				}else{
					var attrRegexp = jQuery("#attrRegexp").val();
					if(attrRegexp.trim() != ""){
						return true;
					}else{
						var attrShowFlg = jQuery("#attrShowFlg").val();
						if(attrShowFlg != "0"){
							return true;
						}else{
							return false;
						}
					}
				}
			}
		}
	}
}
//eoe thai.son 21-2
function checkDataNotModified() {
    var result = true;
    // check data of attribute.	
    // attrShowFlg
    if ($("#attrShowFlg").val() != $("#hAttributeShowFlg").val()) {
        result = false;
    }
    // attrRegexp
    if ($("#attrRegexp").val() != $("#hAttributeRegexp").val()) {
        result = false;
    }
    // attrLengthLimit
    if ($("#attrLengthLimit").val() != $("#hAttributeLengthLimit").val()) {
        result = false;
    }
    // delFlg
    if ($("#delFlg").val() != $("#hAttributeDelFlg").val()) {
        result = false;
    }
    // attrName
    if ($("#attrName").val() != $("#hAttributeName").val()) {
        result = false;
    }
    // check data of attribute group.
    var strJson = prepareAttrGroupData();
    var strOldJson = $("#hAttrGroupList").val();
    if (strJson != strOldJson) {
        result = false;
    }
    return result;
}

//Validate list attributeGroup
//Check Duplicate Sort and Duplicate Name
function validateListAttributeGroup() {
    var grid = $("#kendoGrid").data("kendoGrid");
    var gridData = grid.dataSource.view();
    var length = gridData.length;
    var result = true;
    for (var i = 0; i < length; i++) {
        for (var j = i + 1; j < length; j++) {
            //Check duplicate group name and sort value
            if (gridData[i].get("attributeGroupName") == gridData[j].get("attributeGroupName") || gridData[i].get("attributeSort") == gridData[j].get("attributeSort")) {
                result = false;
                //Show message at tab attribute group
                var message = $("#duplicate-value").val();
                showMessage(message);
                break;
            }
        }
    }
    return result;
}

//BOE #6634 No.46 Hoang.Ho 2014/02/19 Change to kendo validate
//Return 0 if validate success
//Return 1 if tab info invalid
//Return 2 if tab group invalid
function validate() {
    var result = 0;
    //Validate attribute name
    if (checkDataNotModified()) {
        showMessage2($("#hMsgUnchange").val(), 2);
        return 1;
    }
//    var attrNameResult = validateAttributeName();
//
//    //Check exists name
//    if (attrNameResult) {
//        var msgAttrName = $("#spnAttrNameError").text();
//
//        if (msgAttrName != null && msgAttrName != "") {
//            attrNameResult = false;
//        }
//    }
//    
//    // check attr type 
//    var attrTypeFlg = validateAttributeType();
//    //Validate attribute length limit
//    var attrLengthResult = validateAttributeLengthLimit();
    //Validate list attributeGroup
//    var groupResult = validateListAttributeGroup();
//    if (!attrLengthResult || !attrNameResult || !attrTypeFlg) {\
    if (!kendoValidatable.validate()) {
        result = 1;
    } /*else if (!groupResult) {
        result = 2;
    }*/

    return result;
}

//Validate value at popup add and edit group attribute
//function validateGroupPopup() {
//    var result = true;
//    var groupNameResult = validateAttributeGroupName();
//    var sortNameResult = validateAttributeSort();
//
//    if (!groupNameResult || !sortNameResult) {
//        result = false;
//    }
//
//    return result;
//}

//EOE #6634 No.46 Hoang.Ho 2014/02/19 Change to kendo validate
$(document).ready(function () {
	// BOE #6634 No.46 Hoang.Ho 2014/02/19 Init kendo validate
	// Init kendo combobox
    /* BOE by Luong.Dai change kendo combobox to kendo dropdown list at 2014/04/11 */
	/*$("#attributeTypeList").kendoComboBox();*/
    $("#attributeTypeList").kendoDropDownList();
    /* EOE by Luong.Dai change kendo combobox to kendo dropdown list */
    /*
     * customer validate of kendo for edit attribute information.
     * rules : define function for validate,
     * 		return true/false -
     * 			if false kendo will be get [messages] correlative
     * messages: define function for get message.
     * 		return String message to show on User Interface.
     * @author hoang.ho
     * @since 2014/02/19
     */
    var kendoValidatorVar = $("#attributeActionForm .divTabs:first").kendoValidator({
        rules: {
            required: function (input) {
                //class-required will be check not blank
                var inputCls = input.attr("class");
                if (inputCls != undefined) {
                    if (inputCls.indexOf("k-required") > -1) {
                    //BOE fix bug 43 thai.son
                    	/*if(inputCls.indexOf("clsDisable") > -1){
                    		if($('#insertMode').val() == "0"){
                    			if($('#hAttributeType').val() == "Flag"){
                    				return true;
                    			}else{
                    				return input.val() != "";
                    			}
                    		}else{
                    			if($("#attributeTypeList").val() == "Flag"){
                    				return true;
                    			}else{
                    				return input.val() != "";
                    			}
                    		}
                    	}else{*/
                    		return input.val() != "";
                    	//}
                    	//EOE fix bug 43 thai.son
                    }
                }
                return true;
            },
            selectRequired: function (input) {
                //class-required will be check not blank
            	if (input.is("select")) {
            		var inputCls = input.attr("class");
            		if (inputCls != undefined) {
            			if (inputCls.indexOf("k-select-required") > -1) {
            				return input.val() != "";
            			}
            		}
            	}
                return true;
            },
            numberic : function (input) {
            	//class-number will be check is numberic
            	if (null == input.val() || "" == input.val()) {
            		return true;
            	}
            	var inputCls = input.attr("class");
            	if (inputCls != undefined) {
            		if (inputCls.indexOf("k-numberic") > -1) {
            			return validateNumberic(input.val());
            		}
            	}
            	return true;
            },
            ajaxCheckExistAttr: function(input) {
            	 //class-ajaxCheckExistAttr will be check attribute name is not exist
                var inputCls = input.attr("class");
                if (inputCls != undefined) {
                    if (inputCls.indexOf("k-ajaxCheckExistAttr") > -1) {
                        return $("#validated-attr-name").val() != 1;
                    }
                }
                return true;
            }
        },
        messages: {
            required: function (input) {
                return getMsgWithParams(input, $("#requiredFill").val());
            },
            selectRequired: function (input) {
                return getMsgWithParams(input, $("#requiredFill").val());
            },
            numberic: function (input) {
                return getMsgWithParams(input, $("#onlyNumberMsg").val());
            },
            ajaxCheckExistAttr: function (input) {
                return getMsgWithParams(input, $("#name-exists").val());
            },
        }
    });
    kendoValidatable = kendoValidatorVar.data("kendoValidator");
    
    /*
     * customer validate of kendo for edit attribute information.
     * rules : define function for validate,
     * 		return true/false -
     * 			if false kendo will be get [messages] correlative
     * messages: define function for get message.
     * 		return String message to show on User Interface.
     * @author hoang.ho
     * @since 2014/02/19
     */
    var kendoValidatorPopUpVar = $("#dialog").kendoValidator({
        rules: {
            required: function (input) {
                //class-required will be check not blank
                var inputCls = input.attr("class");
                if (inputCls != undefined) {
                    if (inputCls.indexOf("k-required") > -1) {
                        return input.val() != "";
                    }
                }
                return true;
            },
            numberic : function (input) {
            	//class-number will be check is numberic
            	if (null == input.val() || "" == input.val()) {
            		return true;
            	}
            	var inputCls = input.attr("class");
            	if (inputCls != undefined) {
            		if (inputCls.indexOf("k-numberic") > -1) {
            			return validateNumberic(input.val());
            		}
            	}
            	return true;
            },
            duplicateAttrGroup: function(input) {
            	 //class-duplicate attribute group will be check not blank
                var inputCls = input.attr("class");
                if (inputCls != undefined) {
                    if (inputCls.indexOf("k-duplicateAttrGroup") > -1) {
                        return !checkDuplicateName(input.val());
                    }
                }
                return true;
            },
            duplicateAttrGroupSort: function(input) {
            	 //class-required will be check not blank
                var inputCls = input.attr("class");
                if (inputCls != undefined) {
                    if (inputCls.indexOf("k-duplicateAttrGroupSort") > -1) {
                        return validateAttributeSort();
                    }
                }
                return true;
            },
        },
        messages: {
            required: function (input) {
                return getMsgWithParams(input, $("#requiredFill").val());
            },
            numberic: function (input) {
                return getMsgWithParams(input, $("#onlyNumberMsg").val());
            },
            duplicateAttrGroup: function (input) {
                return getMsgWithParams(input, $("#duplicateTextWithParam").val());
            },
            duplicateAttrGroupSort: function (input) {
            	//boe thai.son
                return $("#duplicate-value").val();
                //eoe thai.son
            }
        }
    });
    kendoValidatablePopUp = kendoValidatorPopUpVar.data("kendoValidator");
	// add kendovalidate
	// EOE #6634 No.46 Hoang.Ho 2014/02/19 Init kendo validator
	// BOE #6344 No.41 Thai.Son 2014/02/19 attributeName too long
    var MAX_NUM_CHAR_TITLE_ATTRIBUTE_NAME_CAN_SHOW = 10;
	if ($("#insertMode").val() == "0" && $("#myAttributeNameEdit").val().trim().length > MAX_NUM_CHAR_TITLE_ATTRIBUTE_NAME_CAN_SHOW){
		$("#attributeNameLabel").kendoTooltip({
	        content: $("#attributeNameLabel").text(),
	        animation : {
	            close : {
	                effects : "fade:out"
	            },
	            open : {
	                effects : "fade:in",
	                duration : 300
	            }
	        }
	    });
	}
	// eOE #6344 No.41 Thai.Son 2014/02/19 attributeName too long
	
	
    // ***Begin Check condition and set state control
    if ($("#hAttributeType").val() == "Flag") {
        $(".clsDisable").attr("disabled", "disabled");
    }
    // End Check condition and set state control.***
    // ***Begin define label for Screen.
    var hAttrGroupName = $("#hAttrGroupName").val();
    var hAttrSort = $("#hAttrSort").val();
    var hPopupTitle = $("#hPopupTitle").val();
    // End define label for Screen.***

    // ***Begin Define function for screen.
    // End Define function for screen.***

    // Define tab.
    $("ul.tabs").tabs("div.panes > div");

    // ***Begin create grid.

    // Define data get from sever and parse to json
    var products = $("#hAttrGroupList").val();
    var produtsJson = $.parseJSON(products);
    // create filter object.
    var filterRow = $(
        '<tr>' + '<th class="k-header"></th>' + '<th class="k-header" data-field="attributeGroupName"><input type="search" id="attrGroupNameFilter" class="txtFilter k-textbox" style="width: 280px"/></th>' + '<th class="k-header" data-field="attributeSort"><input type="search" id="attrSortFilter" class="txtFilter k-numeric" style="width: 100px"/></th>' + '</tr>');
    // create kendo grid.
    var grid = $("#kendoGrid").kendoGrid({
        dataSource: {
            data: produtsJson,
            schema: {
                model: {
                    fields: {
                        attributeGroupName: {
                            type: "string"
                        },
                        attributeSort: {
                            type: "string"
                        }
                    }
                }
            }
        },
        height: 450,
        maxWidth: 550,
        scrollable: true,
        sortable: {
            mode: "single",
            allowUnsort: false
        },
        pageable: false,
        resizable: true,
        columns: [{
            field: "delFlg",
            title: " ",
            width: "60px",
            sortable: false,
            template: "<input type='checkbox' value=' #= (delFlg == 1)' class='chkAttrGroup' data-bind='checked: delFlg' #= (delFlg == 1) ? checked='checked' : '' #/>"
        }, {
            field: "attributeGroupName",
            title: hAttrGroupName,
            width: "300px"
        }, {
            field: "attributeSort",
            title: hAttrSort,
            width: "120px"
        }, ],

        rowTemplate: kendo.template($("#rowTemplate").html()),
        altRowTemplate: kendo.template($("#altRowTemplate").html())
    });

    // add filter in to grid.
    grid.data("kendoGrid").thead.append(filterRow);

    $(".k-grid-content").css("height", "390px");
    $("#kendoGrid .k-header .k-numeric").kendoNumericTextBox({
		value : "",
		format: "#",
        decimals: 0,
		min : 0,
		step : 1
	});
    // End create grid.***

    // ***Begin create dialog for screen.
    $("#dialog").kendoWindow({
    //boe thai.son 21-2 bug 46
        //maxWidth: 430,
        width: 420,
    //eoe thai.son 21-2 bug 46
        title: hPopupTitle,
        resizable: false,
        modal: true,
        visible: false,
        open: function () {
        	//BOE #6634 No.46 Hoang.Ho 2014/02/19 Change to kendo validate
//            $("#attributGroupNameRequired").text("");
//            $("#popAttributeGroupName").css("background-color", "#FFFFFF");
//            $("#attributGroupSortRequired").text("");
//            $("#popAttributeSort").css("background-color", "#FFFFFF");
        	//BOE #6634 No.46 Hoang.Ho 2014/02/19 Change to kendo validate
        },
        close: function () {
        	disableTxtFilter();
        },
        activate: function () {
            $('#popAttributeGroupName').select();
        }
    });
    // Start comfirm delete dialog.
    $("#confirmDeleteDialog").kendoWindow({
        maxWidth: 400,
        title: $('#hPopupTitleDeleteAttributeGroup').val(),
        resizable: false,
        modal: true,
        visible: false,
        close: function () {
            var cfm = $("#msgConfirmResult").val();
            if (cfm == "1") {

                // Get grid from database.
                var grid = $("#kendoGrid").data("kendoGrid");
                var dataSource = grid.dataSource;
                var raw = dataSource.data();
                var length = raw.length;
                var attrGroupCodeArr = new Array();
                var groupCode = 0;
                var codeLength = 0;

                // get list attribute has checked for delete.
                $(".kendoRow").each(function () {
                    var currObj = $(this);
                    var checkCount = currObj.find(".chkAttrGroup:checked").length;
                    if (checkCount != undefined && checkCount > 0) {
                        groupCode = currObj.find(".kendoRowGroupCode").val();
                        attrGroupCodeArr.push(groupCode);
                    }
                });

                codeLength = attrGroupCodeArr.length;
                // iterate and remove "done" items
                var item, i, j;
                for (i = length - 1; i >= 0; i--) {
                    item = raw[i];
                    for (j = 0; j < codeLength; j++) {
                        if (item.attributeGroupCode == attrGroupCodeArr[j]) {
                            dataSource.remove(item);
                        }
                    }
                }
                if (dataSource.data().length <= 0) {
                	disableTxtFilter();
                	$(".k-grid-header .k-link").each(function () {
                		$(this).find("span").remove();
                	});
                }
            }
        }
    });
 // BOE #6344 No.64 Thai.Son 2014/02/19 Change confirm pupup to kendo window
    $("#confirmBackDialog").kendoWindow({
        maxWidth: 400,
        resizable: false,
        modal: true,
        visible: false,
        title: $("#title-confirm-back").val(),
        activate: function () {
            jQuery("#fancyConfirm_cancel").focus();
        }
	});
	 $("#fancyConfirm_cancel").click(function(e) {
        e.preventDefault();
        var win = $("#confirmBackDialog").data("kendoWindow");
        win.close();
    });
	// EOE #6344 No.64 Thai.Son 2014/02/19 Change confirm pupup to kendo window
    // End create dialog for screen.***	

    // ***Begin define event on scrren.
    $("#btnSave").click(function (e) {
        var validateResult = validate();
        if (validateResult == 0) {
        	var groupShowFlg;
        	if($('#insertMode').val() == 1){
        		groupShowFlg = $("#attributeTypeList").val() == "Group";
        	} else {
        		groupShowFlg = $("#attributeType").val() == "Group";
        	}
        	if (groupShowFlg) {
        		var strJson = prepareAttrGroupData();
        		$("#hAttrGroupList").val(strJson);
        	} else {
        		$("#hAttrGroupList").val("");
        	}
            var from = $("#attributeActionForm")[0];
            from.submit();
        } else if (validateResult == 1) {
            //Validate false at tab Info
            $("#tabs-info").click();
        } else {
            //Validate false at tab Attribute
            $("#tabs-attr").click();
        }
    });
    $(".k-grid-header .k-link").click(function (e) {
        var grid = $("#kendoGrid").data("kendoGrid");
        var dataSource = grid.dataSource;
        if (dataSource.data().length <= 0) {
            $(this).find("span").remove();
            e.preventDefault();
            e.stopPropagation();
        }
    });
    $(".chkFlg").change(function () {
        var chkbox = $(this);
        if (chkbox.is(":checked")) {
            chkbox.val("1");
        } else {
            chkbox.val("0");
        }
    });
    $("#attrName").change(function (e) {
        $("#validated-attr-name").val("0");
        if (validateAttributeName()) {
            var attrName = $("#attrName").val();
            var attrCode = ($("#attrCode").text() + "").trim();
            $.ajax({
                type: "POST",
                url: "loadAjaxAttributeName.html",
                data: {
                    attrName: attrName,
                    attrCode: attrCode
                },
                dataType: "json",
                beforeSend: function () {
                    jQuery("#loading-attr-name").css("display", "inline");
                },
                // BOE #6634 No.46 Hoang.Ho 2014/02/19 Change ajax validate to kendo ajax validate
                success: function (data) {
                    var result = data.existAttrName;
            		var parent = $("#attrName").parent();
            		var errorMsgElem =  parent.find(".k-invalid-msg");
                    if (result == true) {
                    	// Invalid
                        $("#validated-attr-name").val("1");
                        var message = jQuery("#name-exists").val();
                        var label = jQuery("#labelAttrName").text();
                        var showMessage = message.replace('{0}', label);

                		// replace message.
                		errorMsgElem.html('<span class="k-icon k-warning"> </span> ' + showMessage);
                		errorMsgElem.show();
                    } else {
                    	// Valid
                        $("#validated-attr-name").val("0");
                        errorMsgElem.hide();
//                        jQuery("#spnAttrNameError").text("");
//                        jQuery("#attrName").css("background-color", "#FFFFFF");
                    }
//                    jQuery("#loading-attr-name").css("display", "none");
                },
                complete: function () {
                	// hide image loading.
                    jQuery("#loading-attr-name").css("display", "none");
                }
            });
            // EOE #6634 No.46 Hoang.Ho 2014/02/19 Change ajax validate to kendo ajax validate
        }
    });
    $("#btnNo").click(function (e) {
        e.preventDefault();
        var win = $("#confirmDeleteDialog").data("kendoWindow");
        $("#msgConfirmResult").val("0");
        win.close();
    });
    $("#btnYes").click(function (e) {
        e.preventDefault();
        var win = $("#confirmDeleteDialog").data("kendoWindow");
        $("#msgConfirmResult").val("1");
        win.close();
    });
    // End comfirm delete dialog.

    $("#btnOk").click(function (e) {
    	//BOE #6634 No.46 Hoang.Ho 2014/02/19 Change to kendo validate
//        if (validateGroupPopup()) {
		if (kendoValidatablePopUp.validate()) {
		//BOE #6634 No.46 Hoang.Ho 2014/02/19 Change to kendo validate
            e.preventDefault();
            // get data from popup and insert into grid.
            var popAttributeGroupCode = $("#popAttributeGroupCode").val();
            var popAttributeGroupName = $("#popAttributeGroupName").val();
            var popAttributeSort = $("#popAttributeSort").val();

            var grid = $("#kendoGrid").data("kendoGrid");
            var dataSource = grid.dataSource;
            // get mode 

            if (popAttributeGroupCode != undefined && popAttributeGroupCode != "") {
                var rowCls = "kendoRow" + popAttributeGroupCode;
                $("." + rowCls).find(".kendoRowName").text(popAttributeGroupName);
                $("." + rowCls).find(".kendoRowSort").text(popAttributeSort);
                // Get grid from database.
                var raw = dataSource.data();
                var length = raw.length;

                for (var i = length - 1; i >= 0; i--) {
                    if (raw[i].attributeGroupCode == popAttributeGroupCode) {
                        raw[i].attributeGroupName = popAttributeGroupName;
                        raw[i].attributeSort = popAttributeSort;
                        break;
                    }
                }
                // mode update
            } else {
                // create attributeGroupCode
                var attrCodeGen = 0;
                // Get grid from database.
                var raw = dataSource.data();
                var length = raw.length;
                for (var i = length - 1; i >= 0; i--) {
                    if (raw[i].attributeGroupCode < attrCodeGen) {
                        attrCodeGen = raw[i].attributeGroupCode;
                    }
                }
                attrCodeGen = attrCodeGen - 1;

                dataSource.add({
                    "attributeGroupCode": attrCodeGen,
                    "attributeCode": 0,
                    "attributeGroupName": popAttributeGroupName,
                    "attributeSort": popAttributeSort,
                    "delFlg": 0
                });
                // mode insert
            }

            var win = $("#dialog").data("kendoWindow");
            win.close();
        }
    });
    $("#btnCancel").click(function (e) {
        e.preventDefault();
        var win = $("#dialog").data("kendoWindow");
        win.close();
    });


    // create event when text change for filter.
    $("#kendoGrid").on('change', '.txtFilter', function (e) {
        var grid = $("#kendoGrid").data("kendoGrid");
        if (grid.dataSource.data().length > 0) {
            grid.dataSource.filter({
                logic: "and",
                filters: [
                    {
                        operator: "contains",
                        value: jQuery("#attrGroupNameFilter").val(),
                        field: "attributeGroupName"
                    },
                    {
                        operator: "contains",
                        value: jQuery("#attrSortFilter").val() + "",
                        field: "attributeSort"
                    }
                ]
            });
        }
    });

    // Open popup for update row.
    $('#kendoGrid').on('dblclick', '.kendoCell', function (e) {
        e.preventDefault();
        var parentTR = $(this).parent();
        // Get value.
        var kendoRowGroupCode = parentTR.find(".kendoRowGroupCode").val();
        var kendoRowName = parentTR.find(".kendoRowName").text();
        var kendoRowSort = parentTR.find(".kendoRowSort").text();
        //boe thai.son
        // open popup
        var win = $("#dialog").data("kendoWindow");
        win.center();
        win.open();
        // clear old validate, and set value into popup
        $('#dialog li .k-widget.k-tooltip-validation').hide();
        $("#popAttributeGroupCode").val(kendoRowGroupCode);
        $("#popAttributeGroupName").val(kendoRowName);
        $("#popAttributeSort").val(kendoRowSort);
        //eoe thai.son
    });

    $('#kendoGrid').on('change', '.chkAttrGroup', function (e) {
        // Get grid from database.
        var grid = $("#kendoGrid").data("kendoGrid");
        var dataSource = grid.dataSource;
        var raw = dataSource.data();
        var length = raw.length;

        // get attribute group code.
        var parentTR = $(this).parent().parent().parent("tr");
        var state = $(this).is(":checked");
        var kendoRowGroupCode = parentTR.find(".kendoRowGroupCode").val();
        // iterate and remove "done" items
        for (var i = length - 1; i >= 0; i--) {
            if (raw[i].attributeGroupCode == kendoRowGroupCode) {
                raw[i].delFlg = state ? 1 : 0;
            }
        }
    });

    // Open popup for insert row.
    $("#btnInsert").click(function (e) {
        e.preventDefault();
        //BOE THAI.SON 24-2
        // reset kendowindow: set value is blank, hide msg validate.
        $("#popAttributeGroupCode").val("");
        $("#popAttributeGroupName").val("");
        $("#popAttributeSort").val("");
        $('#dialog li .k-widget.k-tooltip-validation').hide();
        //EOE THAI.SON 24-2
        // open popup
        var win = $("#dialog").data("kendoWindow");
        win.center();
        win.open();
    });
    $("#btnDelete").click(function (e) {
        e.preventDefault();
        var checkCount = $(".chkAttrGroup:checked").length;
        if (checkCount == undefined || checkCount <= 0) {
            var message = $("#no-selected").val();
            showMessage(message);
            return;
        } else {
            showMessage("");
        }
        var win = $("#confirmDeleteDialog").data("kendoWindow");
        win.center();
        win.open();
    });

    //Back to the previous screen
    //If doesn't exists, go to Top page
    $("#btnBack").click(function () {
    	// BOE #6344 No.64 Thai.Son 2014/02/19 Change confirm pupup to kendo window
    	/*//Show confirm message
    	confirmBack();
    	jQuery("#fancyConfirm_cancel").focus();*/
    	confirmBack();
    	// // BOE #6344 No.64 Thai.Son 2014/02/19 Change confirm pupup to kendo window
    });

    $("#btnReload").click(function () {
        // use this line of code to prevent bug in firefox.
    	/* BOE by Luong.Dai at 2014/04/08 Show progress bar */
		showProcessBar();
		/* EOE by Luong.Dai at 2014/04/08 Show progress bar */
        window.location.href = window.location.href;
    });

    $("#attributeTypeList").change(function () {
    	// show hide attribute group.
    	var groupShowFlg = $(this).val() == "Group";
    	if (groupShowFlg) {
    		$("#tabs-attr").show();
    	} else {
    		$("#tabs-attr").hide();
    	}
        if ($(this).val() == "Flag") {
            $(".clsDisable").attr("disabled", "disabled");
            //BOE fix bug 43 thai.son
            if($('#attrLengthLimit').next().hasClass("k-invalid-msg")){
            	$('#attrLengthLimit').next().hide();
            }
            //EOE fix bug 43 thai.son
        } else {
            $(".clsDisable").removeAttr("disabled");
        }
    });

    // End define event on screen.***

//BOE #6634 No.46 Hoang.Ho 2014/02/19 Change to kendo validate
//    $("#popAttributeGroupName").on("change focusout", function () {
//        validateAttributeGroupName();
//    });
//BOE #6634 No.46 Hoang.Ho 2014/02/19 Change to kendo validate

    $("#popAttributeSort").keydown(function (event) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(event.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
            // Allow: Ctrl+A
            (event.keyCode == 65 && event.ctrlKey === true) ||
            // Allow: home, end, left, right
            (event.keyCode >= 35 && event.keyCode <= 39)) {
            // let it happen, don't do anything
            return;
        } else {
            // Ensure that it is a number and stop the keypress
            if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105)) {
                event.preventDefault();
            }
        }
    });

    $("#attrLengthLimit").keydown(function (event) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(event.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
            // Allow: Ctrl+A
            (event.keyCode == 65 && event.ctrlKey === true) ||
            // Allow: home, end, left, right
            (event.keyCode >= 35 && event.keyCode <= 39)) {
            // let it happen, don't do anything
            return;
        } else {
            // Ensure that it is a number and stop the keypress
            if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105)) {
                event.preventDefault();
            }
        }
    });
//BOE #6634 No.46 Hoang.Ho 2014/02/19 Change to kendo validate
//    $("#popAttributeSort").on("change focusout", function () {
//        validateAttributeSort();
//    });

//    $("#attrName").focusout(function () {
//        validateAttributeName();
//    });

//    $("#attributeTypeList").on("change focusout", function () {
//        validateAttributeType();
//    });

//    $("#attrLengthLimit").on("change focusout", function () {
//        validateAttributeLengthLimit();
//    });
//EOE #6634 No.46 Hoang.Ho 2014/02/19 Change to kendo validate
    $("#rightTab").click(function (e) {
        if (e.target.id != 'btnDelete') {
            showMessage("");
        }
    });
    
    // click on tab then set tabs color and border.
    $(".tabStrip").click(function () {
    	var id = this.id;
    	if ("tabs-info" == id) {
    		$("#tabs-info").css("background-color","#FFFFFF");
    		$("#tabs-info").css("color","#000000");
        	$("#tabs-info").css("border-color","#7EA700");
        	$("#tabs-attr").css("border-color","#AAAAAA");
    	} else {
    		$("#tabs-attr").css("background-color","#FFFFFF");
    		$("#tabs-attr").css("color","#000000");
        	$("#tabs-attr").css("border-color","#7EA700");
        	$("#tabs-info").css("border-color","#AAAAAA");
    	}
    });
    
   /* // mouse over tab then set color and border.
    $(".tabStrip").bind("mouseover", function () {
    	var cl = $(this).attr("class");
    	var currentCl = cl.replace("tabStrip", "").trim();
    	var id = this.id;
    	if (currentCl == "") {
    		if ("tabs-info" == id) {
        		$("#tabs-info").css("background-color","#7EA700");
            	$("#tabs-info").css("color","#FFFFFF");
            	$("#tabs-info").css("border-color","#7EA700");
        	} else {
        		$("#tabs-attr").css("background-color","#7EA700");
            	$("#tabs-attr").css("color","#FFFFFF");
            	$("#tabs-attr").css("border-color","#7EA700");
        	}
    	}
    });*/
    
    // mouse out tab set tab to original css
    $(".tabStrip").bind("mouseout", function () {
    	var cl = $(this).attr("class");
    	var currentCl = cl.replace("tabStrip", "").trim();
    	var id = this.id;
    	if (currentCl == "") {
    		if ("tabs-info" == id) {
        		$("#tabs-info").css("background-color","#FFFFFF");
            	$("#tabs-info").css("color","#000000");
            	$("#tabs-info").css("border-color","#AAAAAA");
        	} else {
        		$("#tabs-attr").css("background-color","#FFFFFF");
            	$("#tabs-attr").css("color","#000000");
            	$("#tabs-attr").css("border-color","#AAAAAA");
        	}
    	}
    });
    
    function disableTxtFilter() {
    	var grid = $("#kendoGrid").data("kendoGrid");
        var dataSource = grid.dataSource;
        if (dataSource.data().length <= 0) {
        	$(".txtFilter").attr("disabled", "disabled");
        } else {
        	$(".txtFilter").removeAttr("disabled");
        }
    }
    // check data in datasource of grid to disable or enable text filter.
    disableTxtFilter();
    //cycle tabindex: save-->reload-->back--> input...-->save
    $('#btnSave').attr("tabindex",1);
    $('#btnReload').attr("tabindex",2);
    $('#btnBack').attr("tabindex",3);
    var tabIndex = 3;
    $('#attributeActionForm input:visible').each(function(){
    	if($(this).attr("id") != "delFlg"){
    		tabIndex++;
    		$(this).attr("tabindex",tabIndex);
    	}
    });
    $('#delFlg').attr("tabindex",tabIndex+1);
    //keyCode = 9 stands for [Tab] key.
    $("#delFlg").keydown(function (e) {
        if (e.keyCode == 9) {
            e.preventDefault();
            $("#btnSave").focus();
        }
    });
});