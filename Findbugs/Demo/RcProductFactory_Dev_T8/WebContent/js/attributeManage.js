// Max length constants.
var NUMERIC_MIN = 0;
var MAX_INT_MYSQL = 2147483647;
var MAX_LENGTH_INT_MYSQL = 10;

//Filter bar
var filterRow;
//DataPicker kendo object.
var updateOnFromKendo;
var updateOnToKendo;
//if switch page, isNewPage = true
var currentPage = 1;
var dataSource;

//Search condition
var attributeCode = "";
var attributeName = "";
var attributeType = "";
var attributeLengthLimit = "";
var attributeRegexp = "";
var attributeShowFlg = "";
var attributeDelFlg = "Yes";
var updatedOnFrom = "";
var updatedOnTo = "";

var sortResetFlg = 0;

//Load data for grid
function loadGridData() {
	var grid = $("#grid").kendoGrid({
		dataSource: dataSource,
        sortable: {
            mode: "single",
            allowUnsort: false
        },
        resizable: true,
	    pageable: {
	        refresh: true,
	        pageSizes: [50, 100],
	        buttonCount: 5,
	        messages: {
	          display:         jQuery("#pagingDisplay").val(),
	          empty:           jQuery("#pagingEmpty").val(),
	          itemsPerPage:    jQuery("#pagingItemsPerPage").val(),
	          first:           jQuery("#pagingFirst").val(),
	          previous:        jQuery("#pagingPrevious").val(),
	          next:            jQuery("#pagingNext").val(),
	          last:            jQuery("#pagingLast").val()
	        }
	    },
	    columns: [{ 
	  	          field:"attributeCode",
	  	          type: "number",
	  	          title: jQuery("#titleAttributeCode").val(),
	  	          width: "84px",
	  	          attributes: {"class": "align-right"} },
	  	        { 
	  	          field:"attributeName",
	  	          type: "string",
	  	          title: jQuery("#titleAttributeName").val(),
	  	          width: "167px",
	  	          attributes: {"class": "align-left"}
	  	        },
	  	        { 
	  	          field:"attributeTypeJapan",
	  	          type: "string",
	  	          title: jQuery("#titleAttributeType").val(),
	  	          width: "99px",
	  	          attributes: {"class": "align-center"}
	  	        },
	  	        { 
	  	          field: "attributeLengthLimit",
	  	          type: "number",
	  	          title: jQuery("#titleAttributeLengthLimit").val(),
	  	          width: "83px",
	  	          attributes: {"class": "align-right"}
	  	        },
	  	        { 
	  	          field:"attributeRegexp",
	  	          type: "string",
	  	          title: jQuery("#titleAttributeRegexp").val(),
	  	          width: "83px",
	  	          attributes: {"class": "align-left"}
	  	        },
	  	        { 
	  	          field:"attributeShowFlg",
	  	          type: "string",
	  	          title: jQuery("#titleAttributeShowFlg").val(),
	  	          width: "83px",
	  	          attributes: {"class": "align-center"}
	  	        },
	  	        { 
	  	          field: "attributeDelFlg",
	  	          type: "string",
	  	          title: jQuery("#titleAttributeDelFlg").val(),
	  	          width: "83px",
	  	          attributes: {"class": "align-center"}
	  	        },
	  	        { 
	  	          field:"updatedOn",
	  	          type: "date",
	  	          title: jQuery("#titleUpdateOn").val(),
	  	          width: "256px",
	  	          attributes: {"class": "align-center"}
	  	        }],
        height: 557,
        selectable: true,
	    dataBound: function(e){
	    	//Remove noData message
	    	jQuery(".dataTables_empty").remove();
	    	if (this.dataSource.total() > 0) {
	    		setCurrentSearchCondition();
		    	var page = this.dataSource.page();
		    	
				if(page != currentPage){
					currentPage = page;
					this.content.scrollTop(0);
				}
				
				//Add title for row
				var gridData = this.dataSource.view();
				var gridLength = gridData.length;
				for (var i = 0; i < gridLength; i++) {
			    	var currentUid = gridData[i].uid;
			    		
			    	var currenRow = this.table.find("tr[data-uid='" + currentUid + "']");
			    	$(currenRow).attr("title", jQuery("#hostAddress").val() + "/attributeEdit.html?attributeCode=" + gridData[i].attributeCode);
			    }
		    } else {
		    	//Show no data message
		    	jQuery(".k-grid-content").append('<div class="dataTables_empty">' + jQuery("#noDataMessage").val() + '</div>');
		    }
	    },
	    /*change: function(e) {
			var selectedRows = this.select();

	        var attributeCode = this.dataItem(selectedRows).get("attributeCode");
	        
	        // Redirect to edit brand page
	    	window.location = "./attributeEdit.html?attributeCode=" + attributeCode;
	    }*/
    });
	
	return grid;
}

//Set data filter condition to sent to server
function setDataForDataSource(sortField, sortDir) {
    return {
        "attributeActionForm.attribute.attributeCode" :         attributeCode,
        "attributeActionForm.attribute.attributeName" :         attributeName,
        "attributeActionForm.attribute.attributeType" :         attributeType,
        "attributeActionForm.attribute.attributeLengthLimit" :  attributeLengthLimit,
        "attributeActionForm.attribute.attributeRegexp" :       attributeRegexp,
        "attributeActionForm.attribute.attributeShowFlg" :      attributeShowFlg,
        "attributeActionForm.attribute.attributeDelFlg" :       attributeDelFlg,
        "attributeActionForm.attribute.updatedOnFrom" :         updatedOnFrom,
        "attributeActionForm.attribute.updatedOnTo" :           updatedOnTo,
        "attributeActionForm.attribute.sortField" :             sortField,
        "attributeActionForm.attribute.sortDir":                sortDir
    };
}

//Get filter condition from filter bar
function getSearchCondition() {
    attributeCode = jQuery("#tbCode").val();
    attributeName = jQuery("#tbname").val();
    attributeType = jQuery("#cbbListType").val();
    attributeLengthLimit = jQuery("#tbLengthLimit").val();
    attributeRegexp = jQuery("#tbRegexp").val();
    attributeShowFlg = jQuery("#listShowFlg").val();
    attributeDelFlg = jQuery("#listDelFlg").val();
    updatedOnFrom = jQuery("#tbUpdateOnFrom").val();
    updatedOnTo = jQuery("#tbUpdateOnTo").val();
    
    //Validate and trim value
    if (attributeCode) {
        attributeCode = attributeCode.trim();
    }
    if (attributeName) {
        attributeName = attributeName.trim();
    }
    if (attributeType) {
        attributeType = attributeType.trim();
    }
    if (attributeLengthLimit) {
        attributeLengthLimit = attributeLengthLimit.trim();
    }
    if (attributeRegexp) {
        attributeRegexp = attributeRegexp.trim();
    }
    if (attributeShowFlg) {
        attributeShowFlg = attributeShowFlg.trim();
    }
    if (attributeDelFlg) {
        attributeDelFlg = attributeDelFlg.trim();
    }
    if (updatedOnFrom) {
        updatedOnFrom = updatedOnFrom.trim();
    }
    if (updatedOnTo) {
        updatedOnTo = updatedOnTo.trim();
    }
}

//set search condition to filter bar
function setCurrentSearchCondition() {
	jQuery("#tbCode").val(attributeCode);
    jQuery("#tbname").val(attributeName);
    //If attributeType != null
    if (attributeType) {
        jQuery("#cbbListType").val(attributeType);
    } else {
        //attributeType = null: Set value is All
        jQuery("#cbbListType").val(" ");
    }
    jQuery("#tbLengthLimit").val(attributeLengthLimit);
    jQuery("#tbRegexp").val(attributeRegexp);
    //If attributeShowFlg != null
    if (attributeShowFlg) {
        jQuery("#listShowFlg").val(attributeShowFlg);
    } else {
        //listShowFlg = null: Set value is All
        jQuery("#listShowFlg").val(" ");
    }
    //If delFlg != null
    if (attributeDelFlg) {
    	jQuery("#listDelFlg").val(attributeDelFlg);
    } else {
    	//DelFlg = null: Set value is All
    	jQuery("#listDelFlg").val(" ");
    }
    jQuery("#tbUpdateOnFrom").val(updatedOnFrom);
    jQuery("#tbUpdateOnTo").val(updatedOnTo);
}
//Back to the previous screen
//If doesn't exists, go to Top page
function backToPreviousPage() {
	//BOE THAI.SON FIX BUG "BACK TO SIDE MAP"
	/*//Get previous URL
	var previousUrl = document.referrer;
	//Validate previousUrl
    if("" != previousUrl) {
    	//Go to previous page
//        window.location = previousUrl;
    	window.history.go(-1);
        return;
    }*/
    //Go to Top page
	/* BOE by Luong.Dai at 2014/04/08 Show progress bar */
	showProcessBar();
	/* EOE by Luong.Dai at 2014/04/08 Show progress bar */
    window.location = defaultRedirectPage;
  //EOE THAI.SON FIX BUG "BACK TO SIDE MAP"
}

function setInitForFilter() {
    //clear all textbox
    jQuery("input.tbSearch").val('');
    //Set type comboBox: default value is ' ' => All
    jQuery('select[id="cbbListType"] option[value=" "]').attr('selected','selected');
    //Set listShowFlg combobox: default value is ' ' => All
    jQuery('select[id="listShowFlg"] option[value=" "]').attr('selected','selected');
    //Set listDelFlg combobox: default value is Yes => Yes
    jQuery('select[id="listDelFlg"] option[value="Yes"]').attr('selected','selected');
}
//-------------------BOE THAI.SON
//Validate and compare updateOnDate.
/*function validateDateTextBox(textBoxId) {
    //Format date to YYYY/MM/DD
    jQuery("#" + textBoxId).val(formatDateWithSlash(jQuery("#" + textBoxId).val()));
    //Value of validate date
    var validateDateValue = validateDate(jQuery("#" + textBoxId).val());
    //Value of compare fromDate and toDate
    var compareTwoDateValue = compareTwoDate(jQuery("#tbUpdateOnFrom").val(), jQuery("#tbUpdateOnTo").val());
    //If validate true and compare date true
    if(validateDateValue == 1 && compareTwoDateValue != '-1') {
      //Valid date
        jQuery("#" + textBoxId).removeClass("error");
        return true;
    } 
    //Invalid date
    jQuery("#" + textBoxId).addClass("error");
    return false;
};

function validateUpdateOnDate() {
	return validateDateTextBox('tbUpdateOnFrom') && validateDateTextBox('tbUpdateOnTo');
}*/
//Validate date.
function validateTextboxKousinDate(idValue) {
	if ($("#" + idValue).val() == "") {
        jQuery("#" + idValue).removeClass("error");
        return true;
	}
	$("#" + idValue).val(formatDateTimeWithSlash($("#" + idValue).val()));
    if (!validateDateTime(jQuery("#" + idValue).val())) {
        //Invalid date
        jQuery("#" + idValue).addClass("error");
        return false;
    } else {
        //Valid date
        jQuery("#" + idValue).removeClass("error");
        return true;
    }
}
//Validate date range 
function validateKousinDateRange() {
    var result = true;
    var fromDate = $("#tbUpdateOnFrom").val();
    var toDate = $("#tbUpdateOnTo").val();
    if (undefined != fromDate && "" != fromDate && undefined != toDate && "" != toDate) {
        var dateFrom = new Date($("#tbUpdateOnFrom").val());
        var dateTo = new Date($("#tbUpdateOnTo").val());
        if (dateFrom > dateTo) {
            result = false;
            $("#tbUpdateOnFrom, #tbUpdateOnTo").addClass("error");
        }
    }
    return result;
}
//Validate date
function validateKousinDate() {
    var result = true;
    if (!validateTextboxKousinDate("tbUpdateOnFrom")) {
        result = false;
    }
    if (!validateTextboxKousinDate("tbUpdateOnTo")) {
        result = false;
    }
    if (!result) {
        return false;
    }
    result = validateKousinDateRange();
    return result;
}
//------------------------EOE THAI.SON
//Validate date range 
//function validateKousinDateRange() {
//    var result = true;
//    var fromDate = $("#updatedOnFrom").val();
//    var toDate = $("#updatedOnTo").val();
//    if (undefined != fromDate && "" != fromDate && undefined != toDate && "" != toDate) {
//        var dateFrom = new Date($("#updatedOnFrom").val());
//        var dateTo = new Date($("#updatedOnTo").val());
//        if (dateFrom > dateTo) {
//            result = false;
//            $("#updatedOnFrom, #updatedOnTo").addClass("error");
//        }
//    }
//    return result;
//}

jQuery(document).ready(function() {
    //Create filter row in grid
    // BOE #6344 No.3 Thai.Son 2014/02/18 Change to kendo numeric
    filterRow = $(
            '<tr>'
                + '<td><input class="tbSearch" type="search" id="tbCode" maxlength="8" /></td>'
                + '<td><input class="tbSearch k-textbox" type="search" id="tbname" maxlength="256"/></td>'
                + '<td>'
                +    jQuery("#hiddenTypeCbb").val()
                + '</td>'
                + '<td><input class="tbSearch k-numeric" type="search" id="tbLengthLimit" maxlength="8" /></td>'
                + '<td><input class="tbSearch k-textbox" type="search" id="tbRegexp" maxlength="256"/></td>'
                + '<td><select class="tbSearch" id="listShowFlg" style="width: 70px;padding: 0px;">'
                +        '<option value=" "> </option>'
                +        '<option value="Yes">Yes</option>'
                +        '<option value="No">No</option>'
                + '</select></td>'
                + '<td><select class="tbSearch" id="listDelFlg" style="width: 70px;padding: 0px;">'
                +        '<option value=" "> </option>'
                +        '<option value="Yes">Yes</option>'
                +        '<option value="No">No</option>'
                + '</select></td>'
                //BOE THAI.SON
                /*+ '<td><div>'
                +    '<input id="tbUpdateOnFrom" type="search" class="tbSearch haft-size filter"  />'
                +    ' ~ ' 
                +    '<input id="tbUpdateOnTo" type="search" class="tbSearch haft-size filter" style="border-width: 0 0 0px 0px;"/>'
                + '</div></td>'*/
                + "<td>"
				+ "<div>"
				+ 	"<div>"
				+ 		'<span class="dateTimeLabel">From</span>'
				+ 		'<span>'
				+ 			'<input id="tbUpdateOnFrom" type="text" class="tbSearch k-input filterTextBox" style="width: 180px;" />'
				+ 		'</span>'
				+ 	"</div>"
				+ 	"<div>"
				+ 		'<span class="dateTimeLabel">To</span>'
				+ 		'<span>'
				+ 			'<input id="tbUpdateOnTo" type="text" class="tbSearch k-input filterTextBox" style="width: 180px;" />'
				+ 		'</span>'
				+ 	"<div>"
				+ "</div>"
				+ "</td>"
                //EOE THAI.SON
            + '</tr>');
    // EOE #6344 No.3 Thai.Son 2014/02/18 Change to kendo numeric
    //dataSource for attribute grid
    dataSource = new kendo.data.DataSource({
        serverPaging: true,
        autoBind: false,
        // 50 product in 1 page
        pageSize: 50,
        serverSorting: true,
        transport: {
            read: {
                type: "POST",
                dataType: "json",
                url: "attributerManagerFilter.html",
                data:  function (data) {
                    var sortField = "attributeCode";
                    var sortDir = "asc";
                    if (sortResetFlg != 1) {
                        if (data != undefined && data.sort != undefined && data.sort.length > 0) {
                            sortField = data.sort[0].field;
                            sortDir = data.sort[0].dir;
                        }
                    } else {
                        if (data != undefined && data.sort != undefined && data.sort.length > 0) {
                            data.sort[0].field = '';
                            data.sort[0].dir = '';
                        }
                    }
                    return setDataForDataSource(sortField, sortDir);
                },
                beforeSend :function() {
                    //Hide processing of Kendo.
                    jQuery("#grid .k-loading-image").css("background-image", "none");
                    // show process bar before send request.
                    showProcessBar();
                },
                complete: function() {
                    // hide process bar that the request succeeded
                    hideProcessBar();
                    sortResetFlg = 0;
                },
                error: function() {
                    // hide process bar that the request failed
                    hideProcessBar();
                    sortResetFlg = 0;
                },
                cache: false,
            }
        },
        schema: {
            data: "attributeActionForm.listAttribute",
            total: "attributeActionForm.count"
        }
    });
    
    var grid = loadGridData();
    
    //Add filter
    grid.data("kendoGrid").thead.append(filterRow);
    //BOE THAI.SON
    //Set init default value for filter
    setInitForFilter();// can than cho nay.
    $('#cbbListType').kendoDropDownList();
    $('#listShowFlg').kendoDropDownList();
    $('#listDelFlg').kendoDropDownList();
    jQuery("#tbLengthLimit").kendoNumericTextBox({
		value : "",
		format: "#",
        decimals: 0,
		min : NUMERIC_MIN,
		max : MAX_INT_MYSQL,
		step : 1
	});
    jQuery("#tbCode").kendoNumericTextBox({
		value : "",
		format: "#",
        decimals: 0,
		min : NUMERIC_MIN,
		max : MAX_INT_MYSQL,
		step : 1
	});
    //EOE THAI.SON
    //Add datePicker for updateOn textBox.
    kendo.culture("ja-JP");
    //----------BOE THAI.SON
    $("#tbUpdateOnFrom").kendoDateTimePicker({
        interval: 60,
        format: "yyyy/MM/dd HH:mm:ss",
        timeFormat: "HH:mm:ss",
    });
    
    $("#tbUpdateOnTo").kendoDateTimePicker({
        interval: 60,
        format: "yyyy/MM/dd HH:mm:ss",
        timeFormat: "HH:mm:ss",
    });
    /*//kendo for updateOn From textBox.
    jQuery("#tbUpdateOnFrom").kendoDatePicker({
        format: "yyyy/MM/dd"
    });
    
    //Set value kendo object.
    updateOnFromKendo = jQuery("#tbUpdateOnFrom").data("kendoDatePicker");
    
    //kendo for updateOn To textBox.
    jQuery("#tbUpdateOnTo").kendoDatePicker({
        format: "yyyy/MM/dd"
    });
  //Set value kendo object.
    updateOnToKendo = jQuery("#tbUpdateOnTo").data("kendoDatePicker");
    
    //Open datePicker when focus or click
    jQuery("#tbUpdateOnFrom").on( "click focus", function() {
        updateOnFromKendo.open();
    });
    jQuery("#tbUpdateOnTo").on( "click focus", function() {
        updateOnToKendo.open();
    });*/
  //----------EOE THAI.SON
    //Validate From search value of update_on
    jQuery("#tbUpdateOnFrom").focusout(function(){
    	//----------BOE THAI.SON
        /*validateDateTextBox('tbUpdateOnFrom');*/
    	validateDateTimePicker('#tbUpdateOnFrom', '#tbUpdateOnFrom', '#tbUpdateOnTo');
        //----------EOE THAI.SON
    });
    //Validate to search value of update_on
    jQuery("#tbUpdateOnTo").focusout(function(){
    	//----------BOE THAI.SON
        /*validateDateTextBox('tbUpdateOnTo');*/
    	validateDateTimePicker('#tbUpdateOnTo', '#tbUpdateOnFrom', '#tbUpdateOnTo');
      //----------EOE THAI.SON
    });

    //Click button Back
    jQuery("#btnBack").click(function() {
        backToPreviousPage();
    });
    
    //Click button reset
    jQuery("#btnResetFilter").click(function() {
        /* BOE by Luong.Dai 2014/03/17 Change reset event to reload page */
        /*//Set sort reset
        sortResetFlg = 1;
        //Set init default value for filter.
        setInitForFilter();
        //Call search.
        jQuery("#btnSearch").click();*/
    	/* BOE by Luong.Dai at 2014/04/08 Show progress bar */
		showProcessBar();
		/* EOE by Luong.Dai at 2014/04/08 Show progress bar */
        window.location.replace( window.location.href );
    });
    //Button Search click
    jQuery("#btnSearch").click(function() {
        //Validate 2 updateOn
    	//---------------BOE THAI.SON
        /*var validateDate = validateUpdateOnDate();*/
    	var validateDate = validateKousinDate();
    	//-----------------EOE THAI.SON
        //Validate lengthLimit
        var validateLengthLimit = validateNumbericAndBlank(jQuery("#tbLengthLimit").val());
        if (validateDate &&  validateLengthLimit){
            getSearchCondition();
            //Reload data and goto first page
            grid.data("kendoGrid").dataSource.page(1);
            //Go to top of grid
            grid.data("kendoGrid").content.scrollTop(0);
        }
    });
    //Press enter key in textbox filter
    jQuery(document).on("keypress", "input.tbSearch", function(e){ 
    	if (e.keyCode == 13) {
    		jQuery("#btnSearch").click();
        }
    });
    
    //Change ListType, ShowFl, DelFlg value
    jQuery("#cbbListType, #listShowFlg, #listDelFlg").change(function() {
        jQuery("#btnSearch").click();
    });

    //Validate tbLengthLimit when outfocus
    jQuery("#tbLengthLimit").focusout(function(){
        //Valid number
        if(validateNumbericAndBlank(jQuery("#tbLengthLimit").val())) {
            jQuery("#tbLengthLimit").removeClass("error");
        } else {
            //Invalid
            jQuery("#tbLengthLimit").addClass("error");
        }
    });
    
    $("#btnAddNewAttribute").click(function () {
        document.location.href='attributeEdit.html';
    });
 // double click to row in grid then redirect to edit page.
    $("#grid").on("dblclick", "tr.k-state-selected", function () {
        var attributeCode = $(this).find("td:first").text();
        // Redirect to edit brand page
        /* BOE by Luong.Dai at 2014/04/08 Show progress bar */
		showProcessBar();
		/* EOE by Luong.Dai at 2014/04/08 Show progress bar */
        window.location = "./attributeEdit.html?attributeCode=" + attributeCode;
    });
});