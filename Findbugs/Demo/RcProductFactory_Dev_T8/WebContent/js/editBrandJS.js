var errorMessage = "";
// variable of Kendo validator.
var validatable;
var MAX_NUM_CHAR_TITLE_BRAND_NAME_CAN_SHOW = 10;
/** Add max length input Bug No.6 - 20140313_not_fix @rcv!cao.lan 2014/03/14 **/
var MAX_NUM_WAR_NAME = 50;

/** BOE Tran.Thanh : fix bug No.70 **/
var allFormData_toCheckUnChangeForm = $("#allFormDataToCheckUnChangeForm").val();
/** EOE Tran.Thanh : fix bug No.70 **/
jQuery(document).ready(function () {
	if ($("#mode").val() != "0" && $("#myBrandName").val().trim().length > MAX_NUM_CHAR_TITLE_BRAND_NAME_CAN_SHOW){
		$(".brandNameLabel").kendoTooltip({
	        content: $(".brandNameLabel").text(),
	        animation : {
	            close : {
	                effects : "fade:out"
	            },
	            open : {
	                effects : "fade:in",
	                duration : 300
	            }
	        }
	    });
	}
	
	//BOE THAI.SON
	/* BOE by Luong.Dai at 2014/04/11 change kendoComboBox to kendoDropDownList */
    /*$('.cbb-size-normal').kendoComboBox();*/
	$('.cbb-size-normal').kendoDropDownList();
    /* EOE by Luong.Dai at 2014/04/11 change kendoComboBox to kendoDropDownList */
    //EOE THAI.SON.
    // setup ul.tabs to work as tabs for each div directly under div.panes
    //$("ul.tabs").tabs("div.panes > div");
    //Click button Back

    jQuery("#delFlag").change(function () {
        var flg = jQuery("#delFlag").val();
        if (1 == flg) {
            jQuery("#delFlag").val(0);
            jQuery("#delflg").val(0);
        } else {
            jQuery("#delFlag").val(1);
            jQuery("#delflg").val(1);
        }
    });

    /**
     * Get message with parameters. parameters format are: param1,param2
     * @param invalid input item will be get message.
     * @param msgFormat message format
     * 						ex: {0} is required field.
     * @author hoang.ho
     * @since 2014/01/22
     */
    function getMsgWithParams(input, msgFormat) {
        var params = input.data("parameters");
        if (params != undefined && params != "") {
            var paramArr = params.split(",");
            var length = paramArr.length;
            if (length > 0) {
                for (var i = 0; i < length; i++) {
                    msgFormat = msgFormat.replace("{" + i + "}", paramArr[i]);
                }
            }
        }
        return msgFormat;
    }

    // set defalt focus for page.
    $("#brandName").focus();
    // event for tabindex, keyCode = 9 stands for [Tab] key.
    $("#btnBack").keydown(function (e) {
        if (e.keyCode == 9) {
            e.preventDefault();
            $("#brandName").focus();
        }
    });

    /**
     * customer validate of kendo.
     * rules : define function for validate,
     * 		return true/false -
     * 			if false kendo will be get [messages] correlative
     * messages: define function for get message.
     * 		return String message to show on User Interface.
     * @author hoang.ho
     * @since 2014/01/22
     */
    // BOE #6634 No.13 Hoang.Ho 2014/02/19 Upload image when kendo-validate failed
//    var kendoValidatorVar = $("#editBrandActionForm").kendoValidator({
    var kendoValidatorVar = $("#editBrandActionForm .kendoValidate").kendoValidator({
    // EOE #6634 No.13 Hoang.Ho 2014/02/19 Upload image when kendo-validate failed
        rules: {
            required: function (input) {
                //class-required will be check not blank
                var inputCls = input.attr("class");
                if (inputCls != undefined) {
                    if (inputCls.indexOf("k-required") > -1) {
                        return input.val() != "";
                    }
                }
                return true;
            },
            ajaxCheckExistBrand: function(input) {
            	 //class-required will be check not blank
                var inputCls = input.attr("class");
                if (inputCls != undefined) {
                    if (inputCls.indexOf("k-ajaxCheckExistBrand") > -1) {
                        return $("#brandInvalidFlag").val() != 1;
                    }
                }
                return true;
            },
            katakana : function (input) {
               //class-katakana will be check is katakana charactors
               if (null == input.val() || "" == input.val()) {
            	   return true;
               }
               var inputCls = input.attr("class");
               if (inputCls != undefined) {
                   if (inputCls.indexOf("k-katakana") > -1) {
                       return /^[@-@-[§-ŬŜß]+$/i.test(input.val());
                   }
               }
               return true;
           }
        },
        messages: {
            required: function (input) {
                return getMsgWithParams(input, requiredFill);
            },
            katakana: function (input) {
                return katakanaOnly;
            },
            ajaxCheckExistBrand: function (input) {
                return getMsgWithParams(input, $("#existsInDB").val());
            }
        }
    });
    validatable = kendoValidatorVar.data("kendoValidator");
    
    // dialog 
    $("#confirmBackDialog").kendoWindow({
        maxWidth: 400,
        resizable: false,
        modal: true,
        visible: false,
        title: $("#title-confirm-back").val(),
        activate: function () {
            jQuery("#fancyConfirm_cancel").focus();
        }
	});
    $("#fancyConfirm_cancel").click(function(e) {
        e.preventDefault();
        var win = $("#confirmBackDialog").data("kendoWindow");
        win.close();
    });
	//BOE #6634 No.15 Hoang.Ho 2014/02/18 Add confirm dialog when click button save data.
	//Start comfirm submit dialog.
    $("#confirmSubmitDialog").kendoWindow({
    	maxWidth: 400,
    	minWidth: 250,
    	minHeight: 100,
    	resizable: false,
    	modal: true,
    	visible: false,
        title: $("#title-confirm-back").val(),
    });
    $("#btnSave").click(function (e) {
    	e.preventDefault();
    	// show popup confirm
    	if (saveBrand()) {
    		var win  = $("#confirmSubmitDialog").data("kendoWindow");
    		win.center();
    		win.open();
    	}
    });
// End create dialog for screen.***
    $("#btnSubmit").click(function (event, selectedRows, data) {
    	var win  = $("#confirmSubmitDialog").data("kendoWindow");
    	win.close();
    	jQuery("#editBrandActionForm").submit();
    });
    $("#btnCancelSubmit").click(function () {
    	var win  = $("#confirmSubmitDialog").data("kendoWindow");
    	win.close();
    });
    //BOE #6634 No.15 Hoang.Ho 2014/02/18 Add confirm dialog when click button save data.
    //BOE #6634 No.18 Hoang.Ho 2014/02/18 Add validate ajax for brandName
    function loadAjaxCheckBrandName(brandName, brandCode) {
    	$.ajax({
            type: "POST",
            url: "ajaxCheckExistBrandName.html",
            data: {
            	"editBrandActionForm.entBrand.name": brandName,
            	"editBrandActionForm.entBrand.brandCode": brandCode
            },
            dataType: "json",
            beforeSend : function(){
            	// show image loading.
                $("#loading-ajax-inline").css("display", "inline");
            },
            success: function(data) {
            	var result = data.editBrandActionForm.existBrand;
        		var parent = $("#brandName").parent();
        		var errorMsgElem =  parent.find(".k-invalid-msg");
            	if (result == true) {        		
            		//Invalid
            		// set flag
            		$("#brandInvalidFlag").val("1");
            		// get message.
            		var message = $("#existsInDB").val();
            		var label = $("#lblBrandName").text();
            		var showMessage = message.replace('{0}', label);
            		// replace message.
            		errorMsgElem.html('<span class="k-icon k-warning"> </span> ' + showMessage);
            		errorMsgElem.show();
            	} else {
            		// valid
            		$("#brandInvalidFlag").val("0");
            		errorMsgElem.hide();
            	}
            },
            complete : function(){
            	// hide image loading.
                jQuery("#loading-ajax-inline").css("display", "none");
            }
        });
    }
    
    /** Add check length input Bug No.6 - 20140313_not_fix @rcv!cao.lan 2014/03/14 **/
    var txtWarningLength = $.format($("#warningLength").val(),MAX_NUM_WAR_NAME);
    
    $("#brandName").change(function () {
    	//Set is validated bunrui is false
    	$("#brandInvalidFlag").val("0");
    	var name = $("#brandName").val();
        if (name != undefined && name != "") {
        	/** BOE Add check length input Bug No.6 - 20140313_not_fix @rcv!cao.lan 2014/03/14 **/
        	if(name.length > MAX_NUM_WAR_NAME){
        		$("#warningBrandName").show();
        		$("#warningBrandName").html('<span class="k-icon k-warning"></span>' + txtWarningLength);
        	} else {
        		$("#warningBrandName").hide();
        		var code = "";
            	var mode = $("#mode").val();
            	
            	if (mode != "0") {
            		code = $("#brandCode").val();
            	}
            	
            	loadAjaxCheckBrandName(name, code);
        	}
        } else {
        	$("#warningBrandName").hide();
        	/** EOE @rcv!cao.lan 2014/03/14 **/
        }
    });
    
    /** BOE Add check length input Bug No.6 - 20140313_not_fix @rcv!cao.lan 2014/03/14 **/
    $("#hyoujiName1").change(function () {
    	var name = $("#hyoujiName1").val();
        if (name != undefined && name != "") {
        	if(name.length > MAX_NUM_WAR_NAME){
        		$("#warningHyoujiName1").show();
        		$("#warningHyoujiName1").html('<span class="k-icon k-warning"></span>' + txtWarningLength);
        	} else {
        		$("#warningHyoujiName1").hide();
        	}
        } else {
        	$("#warningHyoujiName1").hide();
        }
    });
    
    $("#hyoujiName2").change(function () {
    	var name = $("#hyoujiName2").val();
        if (name != undefined && name != "") {
        	if(name.length > MAX_NUM_WAR_NAME){
        		$("#warningHyoujiName2").show();
        		$("#warningHyoujiName2").html('<span class="k-icon k-warning"></span>' + txtWarningLength);
        	} else {
        		$("#warningHyoujiName2").hide();
        	}
        } else {
        	$("#warningHyoujiName2").hide();
        }
    });
    
    $("#hyoujiName3").change(function () {
    	var name = $("#hyoujiName3").val();
        if (name != undefined && name != "") {
        	if(name.length > MAX_NUM_WAR_NAME){
        		$("#warningHyoujiName3").show();
        		$("#warningHyoujiName3").html('<span class="k-icon k-warning"></span>' + txtWarningLength);
        	} else {
        		$("#warningHyoujiName3").hide();
        	}
        } else {
        	$("#warningHyoujiName3").hide();
        }
    });
    
    $("#sortKanaName").change(function () {
    	var name = $("#sortKanaName").val();
        if (name != undefined && name != "") {
        	if(name.length > MAX_NUM_WAR_NAME){
        		$("#warningSortKanaName").show();
        		$("#warningSortKanaName").html('<span class="k-icon k-warning"></span>' + txtWarningLength);
        	} else {
        		$("#warningSortKanaName").hide();
        	}
        } else {
        	$("#warningSortKanaName").hide();
        }
    });
    /** EOE @rcv!cao.lan 2014/03/14 **/
    
    //EOE #6634 No.18 Hoang.Ho 2014/02/18 Add validate ajax for brandName
});

function reloadPage() {
	/* BOE by Luong.Dai add progress bar at 2014/04/02 */
	showProcessBar();
	/* EOE by Luong.Dai add progress bar */
    location.reload();
}

/*
 * Function validate max length field input is 50
 * Bug No.6 - 20140313_not_fix
 * @author @rcv!cao.lan
 * @since 2014/03/15
 * @return true/false
 * */
function validateMaxLength(){
	var flag = true;
	if($("#brandName").val().length > MAX_NUM_WAR_NAME
			|| $("#hyoujiName1").val().length > MAX_NUM_WAR_NAME
			|| $("#hyoujiName2").val().length > MAX_NUM_WAR_NAME
			|| $("#hyoujiName3").val().length > MAX_NUM_WAR_NAME
			|| $("#sortKanaName").val().length > MAX_NUM_WAR_NAME)
		flag = false;
	return flag;
}

function saveBrand() {
    // call kendo validate form before save.
    // If validate failed. prevent submit.
    if (!validatable.validate()) {
        return false;
    }
    
    // validate max length input Bug No.6 - 20140313_not_fix @rcv!cao.lan 2014/03/15
    if(!validateMaxLength())
    	return false;
    	
    jQuery("#successArea").html("");
    jQuery("#errorArea").html("");
    /*BOE  Thai.Son 2014/01/17*/
    var dataUserInput =
        jQuery("#brandName").val() 
        + jQuery("#hyoujiName1").val() 
        + jQuery("#hyoujiName2").val() 
        + jQuery("#hyoujiName3").val() 
        + jQuery("#sortKanaName").val() 
        + jQuery("#listCommonUser").val() 
        + jQuery("#url").val() 
        + jQuery("#rss").val() 
        + jQuery("#listMarume").val() 
        + jQuery("#sentence").val()
        // Fix No.5-20140313_not_fix @rcv!cao.lan 2014/03/14
        + $("#delFlag").val();
    if ((dataUserInput == allFormData_toCheckUnChangeForm) && ($('#flagCheckImageChange').val() != "true")) {
        var message = $("#unchange").val();
        showMessage(message, "2");
      //BOE #6634 No.15 Hoang.Ho 2014/02/18 Add confirm dialog when click button save data.
        return false;
    } else {
        //showMessage("");
//        jQuery("#editBrandActionForm").submit();
        return true;
      //EOE #6634 No.15 Hoang.Ho 2014/02/18 Add confirm dialog when click button save data.
    }
    /*EOE  Thai.Son 2014/01/17*/
    //jQuery("#editBrandActionForm").submit();
}

function showMessage(message, type) {
    jQuery("#message").text(message);

    if (null == message || "" == message) {
        jQuery("#message").removeClass("has-message");
    } else {
        jQuery("#message").addClass("has-message");
    }

    //Type = 1: Notice
    //Type = 2: Error
    if (type == "1") {
        jQuery("#message").addClass("success");
    } else {
        jQuery("#message").addClass("msg-error");
    }
}
//Show confirm message when back previous page
function confirmBack() {
	  var dataUserInput =
	        jQuery("#brandName").val() 
	        + jQuery("#hyoujiName1").val() 
	        + jQuery("#hyoujiName2").val() 
	        + jQuery("#hyoujiName3").val() 
	        + jQuery("#sortKanaName").val() 
	        + jQuery("#listCommonUser").val() 
	        + jQuery("#url").val() 
	        + jQuery("#rss").val() 
	        + jQuery("#listMarume").val() 
	        + jQuery("#sentence").val();
	  		// Fix No.5-20140313_not_fix @rcv!cao.lan 2014/03/14
	  		+ $("#delFlag").val();
	   // check for insert
	  	var firstSelect = $("#listCommonUser option:first").val() + jQuery("#listMarume option:first").val();
	  	if (firstSelect == dataUserInput) {
	  		backToPreviousPage();
	  	} else if ((dataUserInput == allFormData_toCheckUnChangeForm) && ($('#flagCheckImageChange').val() != "true")) {
	    	backToPreviousPage();
	    } else {
	    	// open popup
	    	var win = $("#confirmBackDialog").data("kendoWindow");
	    	win.center();
	    	win.open();
	    }
	
}

//Show confirm message
function clickBack() {
    //Show confirm message
    confirmBack();
}
//Back to the previous screen
//If doesn't exists, go to Top page
function backToPreviousPage() {
	//BOE THAI.SON FIX BUG "BACK TO SIDE MAP"
    /*//Get previous URL
    var previousUrl = document.referrer;
    //Validate previousUrl
    if ("" != previousUrl) {
        history.back();
        return;
    }
    //Go to Top page
    window.location = defaultRedirectPage;*/
	/* BOE by Luong.Dai add progress bar at 2014/04/01 */
	showProcessBar();
	/* EOE by Luong.Dai add progress bar */
	window.location = "./brandMasterManager.html";
	//EOE THAI.SON FIX BUG "BACK TO SIDE MAP"
}
