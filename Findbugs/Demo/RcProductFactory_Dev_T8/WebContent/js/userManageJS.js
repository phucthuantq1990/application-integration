function getUserDataAjax() {
	var userid = jQuery('#userId').val();
    var username = jQuery('#userName').val();
    /* BOE login by redmine without Authentication @rcv!Nguyen.Chuong 2014/01/13 [6115]*/
    /*var userauthority = jQuery('select[name="Authority"]').find(":selected").val();*/
    /* EOE login by redmine without Authentication @rcv!Nguyen.Chuong 2014/01/13 [6115]*/
    var delflg = jQuery('select[name="Status"]').find(":selected").val();
    var lockUserHeader = jQuery('#lockUserHeader').val();
	var lockUser = jQuery('#lockUser').val();
	var activeUserHeader = jQuery('#activeUserHeader').val();
	var activeUser = jQuery('#activeUser').val();
    jQuery.ajax({
        type: "POST",
        url: "userManageLoadJson.html",
        data: {
            userId: userid,
            userName: username,
            /* BOE login by redmine without Authentication @rcv!Nguyen.Chuong 2014/01/13 [6115]*/
            /*userAuthority : userauthority,*/
            /* EOE login by redmine without Authentication @rcv!Nguyen.Chuong 2014/01/13 [6115]*/
            delFlg: delflg
        },
        dataType: "json",
        success: function(dataResult) {
           var resultHtml = "";
           var data = eval(dataResult["userManageBodyList"]);
           if(data.length > 0) {
               jQuery.each(data, function(i, item) {
            	   if (item.userDelFlg == 0) {
            		   resultHtml += "<tr>";
                   } else {
                	   resultHtml += "<tr class='tbl-row-background-gray'>";
                   }
            	   resultHtml += '<td style="text-align:left; width: 10%;">'   + item.userId + '</td>'
                       + '<td style="text-align:center; width: 25%;">' + item.userLastName + ' ' + item.userFirstName + '</td>'
                       + '<td style="text-align:center; width: 27%;">' + item.userEmail + '</td>';
                       /* BOE login by redmine without Authentication @rcv!Nguyen.Chuong 2014/01/13 [6115]*/
                       /*if (item.userAuthority == 0) {
                    	   resultHtml += '<td style="text-align:center; width: 12%;">オペレーター</td>';
                       } else {
                    	   resultHtml += '<td style="text-align:center; width: 12%;">管理者</td>';
                       }*/
                       /* EOE login by redmine without Authentication @rcv!Nguyen.Chuong 2014/01/13 [6115]*/
                       if (item.userDelFlg == 0) {
                    	   resultHtml += '<td style="text-align:center; width: 12%;">有効</td>';
                       } else {
                    	   resultHtml += '<td style="text-align:center; width: 12%;">ロック</td>';
                       }
                       resultHtml +='<td class="tbl-colum-action" align="center" width="14%">';
                       if (item.userDelFlg == 0 ) {
                           resultHtml += '<a href="changeStatus.html?userId=' + item.userId + '&delFlg=' + item.userDelFlg + '"'
                                      + 'onclick="confirmChangeStatusUser(\''+ item.userId + '\',\'' + item.userDelFlg +'\',\''+ lockUserHeader + '\',\'' + lockUser +'\'); return false;"'
                                      + 'class="tbl-control-size-small btn-size-small btn-color-gold tipS"'
                                      + 'title="' + 'ロック' + '"'
                                      + 'style="color: #ffffff; text-decoration: none;">' + 'ロック' + '</a>';
                           resultHtml += '<a href="userDetail.html?userId='+ item.userId + '"'
                                      + 'class="tbl-control-size-small btn-size-small btn-color-default"'
                                      + 'title="' + '変更' + '"'
                                      + 'style="color: gray; text-decoration: none; ">' + '変更' + '</a>';
                       } else if (item.userDelFlg == 1) {
                    	   resultHtml += '<a href="changeStatus.html?userId=' + item.userId + '&delFlg=' + item.userDelFlg + '"'
			                           + 'onclick="confirmChangeStatusUser(\''+ item.userId + '\',\'' + item.userDelFlg +'\',\''+ activeUserHeader + '\',\'' + activeUser +'\'); return false;"'
			                           + 'class="tbl-control-size-small btn-size-small btn-color-default tipS"'
			                           + 'title="' + 'アンロック' + '"'
			                           + 'style="color: gray; text-decoration: none;">' + 'アンロック' + '</a>';
			                resultHtml += '<a href="userDetail.html?userId='+ item.userId + '"'
			                           + 'class="tbl-control-size-small btn-size-small btn-color-default"'
			                           + 'title="' + '変更' + '"'
			                           + 'style="color: gray; text-decoration: none; ">' + '変更' + '</a>';}
                   resultHtml += '</td>'
                              + "</tr>";
               });
           } else {
               resultHtml += "<tr>" + "<td colspan='8' class='errorMessage'>" + 'データがありません。' + "</td>" + "</tr>";
           };
           //Clear old table
           jQuery("#mainBody").empty();
           //Append new data
           jQuery(resultHtml).appendTo("#mainBody");
           jQuery("#no4").freezeHeader({ 'height': '393px'});
        }
    });
}

function confirmChangeStatusUser(userId, status, header, message) {
    jQuery.fancybox({
        modal : true,
        autoSize : true,
        content : "<div>"
                 +"     <div id='heading-popup'>"
                 +"        " + header + ""
        		 +"     </div>"
        		 +"     <div id='content-popup'>"
    			 + "    <p>" 
    			 +"        " + message +""
    			 + "    </p>"
    			 +"     <div style='text-align:center;padding:5px;'>"
    			 + "        <input id='fancyConfirm_ok' class='btn-size-medium btn-color-blue' type='button' value='はい' onclick='ChangeStatusUser(\""+userId+"\",\""+status+"\");' style='margin:7px;padding:5px;width:67px;'>"
		         + "        <input id='fancyConfirm_cancel' class='btn-size-medium btn-color-default' type='button' value='キャンセル' onclick='jQuery.fancybox.close();' style='margin:7px;padding:5px;width:67px;'>"
			     +"     </div>"
		         +"     </div>"
		         + "</div>"
    });
};

function ChangeStatusUser(userId,status) {
    jQuery.fancybox.close();
    jQuery.ajax({
        type: "POST",
        url: "changeStatus.html",
        data: {            
        	userId : userId,
        	delFlg : status
        },
        success: function getUserDataAjax() {
        	var userid = jQuery('#userId').val();
            var username = jQuery('#userName').val();
           /* BOE login by redmine without Authentication @rcv!Nguyen.Chuong 2014/01/13 [6115]*/
           /* var userauthority = jQuery('select[name="Authority"]').find(":selected").val();*/
           /* EOE login by redmine without Authentication @rcv!Nguyen.Chuong 2014/01/13 [6115]*/
            var delflg = jQuery('select[name="Status"]').find(":selected").val();
            var lockUserHeader = jQuery('#lockUserHeader').val();
        	var lockUser = jQuery('#lockUser').val();
        	var activeUserHeader = jQuery('#activeUserHeader').val();
        	var activeUser = jQuery('#activeUser').val();
            jQuery.ajax({
                type: "POST",
                url: "userManageLoadJson.html",
                data: {
                    userId: userid,
                    userName: username,
                    /* BOE login by redmine without Authentication @rcv!Nguyen.Chuong 2014/01/13 [6115]*/
                    /*userAuthority : userauthority,*/
                    /* EOE login by redmine without Authentication @rcv!Nguyen.Chuong 2014/01/13 [6115]*/
                    delFlg: delflg
                },
                dataType: "json",
                success: function(dataResult) {
                   var resultHtml = "";
                   var data = eval(dataResult["userManageBodyList"]);
                   if(data.length > 0) {
                       jQuery.each(data, function(i, item) {
                    	   if (item.userDelFlg == 0) {
                    		   resultHtml += "<tr>";
                           } else {
                        	   resultHtml += "<tr class='tbl-row-background-gray'>";
                           }
                    	   resultHtml += '<td style="text-align:left; width: 10%;">'   + item.userId + '</td>'
                               + '<td style="text-align:center; width: 25%;">' + item.userLastName + ' ' + item.userFirstName + '</td>'
                               + '<td style="text-align:center; width: 27%;">' + item.userEmail + '</td>';
                               /* BOE login by redmine without Authentication @rcv!Nguyen.Chuong 2014/01/13 [6115]*/
                               /*if (item.userAuthority == 0) {
                            	   resultHtml += '<td style="text-align:center; width: 12%;">オペレーター</td>';
                               } else {
                            	   resultHtml += '<td style="text-align:center; width: 12%;">管理者</td>';
                               }*/
                               /* EOE login by redmine without Authentication @rcv!Nguyen.Chuong 2014/01/13 [6115]*/
                               if (item.userDelFlg == 0) {
                            	   resultHtml += '<td style="text-align:center; width: 12%;">有効</td>';
                               } else {
                            	   resultHtml += '<td style="text-align:center; width: 12%;">ロック</td>';
                               }
                               resultHtml +='<td class="tbl-colum-action" align="center" width="14%">';
                               if (item.userDelFlg == 0 ) {
                                   resultHtml += '<a href="changeStatus.html?userId=' + item.userId + '&delFlg=' + item.userDelFlg + '"'
                                              + 'onclick="confirmChangeStatusUser(\''+ item.userId + '\',\'' + item.userDelFlg +'\',\''+ lockUserHeader + '\',\'' + lockUser +'\'); return false;"'
                                              + 'class="tbl-control-size-small btn-size-small btn-color-gold tipS"'
                                              + 'title="' + 'ロック' + '"'
                                              + 'style="color: #ffffff; text-decoration: none;">' + 'ロック' + '</a>';
                                   resultHtml += '<a href="userDetail.html?userId='+ item.userId + '"'
                                              + 'class="tbl-control-size-small btn-size-small btn-color-default"'
                                              + 'title="' + '変更' + '"'
                                              + 'style="color: gray; text-decoration: none; ">' + '変更' + '</a>';
                               } else if (item.userDelFlg == 1) {
                            	   resultHtml += '<a href="changeStatus.html?userId=' + item.userId + '&delFlg=' + item.userDelFlg + '"'
        			                           + 'onclick="confirmChangeStatusUser(\''+ item.userId + '\',\'' + item.userDelFlg +'\',\''+ activeUserHeader + '\',\'' + activeUser +'\'); return false;"'
        			                           + 'class="tbl-control-size-small btn-size-small btn-color-default tipS"'
        			                           + 'title="' + 'アンロック' + '"'
        			                           + 'style="color: gray; text-decoration: none;">' + 'アンロック' + '</a>';
        			                resultHtml += '<a href="userDetail.html?userId='+ item.userId + '"'
        			                           + 'class="tbl-control-size-small btn-size-small btn-color-default"'
        			                           + 'title="' + '変更' + '"'
        			                           + 'style="color: gray; text-decoration: none; ">' + '変更' + '</a>';}
                           resultHtml += '</td>'
                                      + "</tr>";
                       });
                   } else {
                       resultHtml += "<tr>" + "<td colspan='8' class='errorMessage'>" + 'データがありません。' + "</td>" + "</tr>";
                   };
                   //Clear old table
                   jQuery("#mainBody").empty();
                   //Append new data
                   jQuery(resultHtml).appendTo("#mainBody");
                   jQuery("#no4").freezeHeader({ 'height': '393px'});
                }
            });
        }
        }
    );

};

function fancyAlert(msg) {
    jQuery.fancybox({
        modal : true,
        width :240,
        height:145,
        autoSize: false,
        content : "<div>"
	           +"     <div id='heading-popup'>"
	           +"        チェック"
	   		   +"     </div>"
	   		   +"     <div id='content-popup'>"
			   + "        <p>" 
			   +             msg
			   + "        </p>"
			   +"         <div style='text-align:center;padding:5px;'>"
			   + "    	        <input class='btn-size-medium btn-color-blue' type='button' style='text-align:center;' type=\"button\" onclick=\"jQuery.fancybox.close();\" value=\"はい\">"
			   +"         </div>"
			   +"     </div>"
	           + "</div>"
    });
}