var message = jQuery("#errorIdOrPassInputInvalid").val();

jQuery.validator.addMethod("lettersonly", function(value, element) {
	  return this.optional(element) || /^[a-z0-9_\-\.]*$/i.test(value);
	}, message);

jQuery(document).ready(function(){
	//Set focus textbox
	setFocusTextbox("j_username");
	
    $("#login").validate({
            errorElement: "span",
            onkeyup: function (element, event) {
            	if (event.which === 9 && this.elementValue(element) === "") {
                    return;
                } else {
                    this.element(element);
                } 
            },
            rules: {
            	myField: { lettersonly: true }
            },
            errorPlacement: function(error, element) {
                jQuery("#errorArea").empty();
                error.appendTo('#errorArea');
            }
     });
    
    $("#btn_login").click(function(){
    	if ($('#login').valid() == false) {
    		setFocusTextbox("j_username");
        }
    }); 	
    
});

function setFocusTextbox(idName){
	var txtBox = document.getElementById(idName);
    if (txtBox != null) {
    	txtBox.focus();
    }
}
