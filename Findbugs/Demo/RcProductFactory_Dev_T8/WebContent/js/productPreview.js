/****************************** BOE Variable Global ******************************/
/* BOE global Tran.Thanh */
var TAX = 1.08;
var totalAjaxLoading = 0;

//role of user
var roleUser = "";
/* EOE global Tran.Thanh */

/* BOE global Dai.Luong */
var DOT_SYSBOL = "";
var NO_PRICE = "-----";
var MAX_FIT_MODEL = 5;
var NEXT_PREV_FLG = 0;
/* EOE global Dai.Luong */

/* BOE global Nguyen.Chuong */
var nextProductId;
var previousProductId;
//Current productId
var productId;
var matterNo;
var categoryCode;
var brandCode;
//Current selected image dataUid
var currentImageId;
/* EOE global Nguyen.Chuong */
/* BOE @rcv!dau.phuong */
var dataSourcePopUp;
//search condition
var calibrationRequestIndividualCode = "";
var calibrationRequestIndividualFinishFlg = "";
var calibrationRequestIndividualProductId = "";
var calibrationRequestIndividualTypeCode = "";
var calibrationRequestIndividualValue = "";
var calibrationRequestIndividualRequestUserId = "";
var calibrationRequestIndividualRequestDateFrom = "";
var calibrationRequestIndividualRequestDateTo = "";
var calibrationRequestIndividualFinishDateFrom = "";
var calibrationRequestIndividualFinishDateTo = "";
var individualCode = "";
var editType = "";
var editValue = "";
/* EOE @rcv!dau.phuong */
/* BOE @rcv!Tuong.Luong  Variable Global  */
var updateStatusCheckBoxFlag = false;//true is loadupdatePopup
//declare flag variables of jan and product
var currentFlagProductCode = true;//true is valid flag, false is invalid 
var currentFlagJanCode = true;//true is valid flag, false is invalid 
//declare flag variables fit model
var currentFlagFitModel = true;//true is valid flag, false is invalid 
//store idproduct checkbox
var idArrayCheckBox= new Array();
//using turn on red color label
var currentFlagThumbnailValid = true;//true is valid flag, false is invalid 
var currentFlagImgExists = true;//true is valid flag, false is invalid 
//store image object for check click thumblist
var storeImageObject = new Array();
//List calibration request individual data from DB of current product.
var listCalibrationRequest = "";
/* EOE @rcv!Tuong.Luong  Variable Global  */
/****************************** EOE Variable Global ******************************/

/********************************* BOE Function *********************************/
/**
* Show process bar when sent ajax to server
* @author   Luong.Dai
* @date     2014/06/25
*/
function showProcessBarInAjax() {
    totalAjaxLoading ++ ;
    //Check ajax call by load init or next/prev function
    if (NEXT_PREV_FLG == 0) {
        showProcessBar();
    }
}

/**
* Hide process bar when all ajax done
* @author   Luong.Dai
* @date     2014/06/25
*/
function hideProcessBarInAjax() {
    totalAjaxLoading -- ;
    if (totalAjaxLoading == 0) {
        //Complete all ajax function, set flg next/prev to default
        hideProcessBar();
    }
} 
/* BOE Function Block1 */
/* EOE Function Block1 */

/* BOE Function Block2 */
function loadDataBlock2(productId) {
    $.ajax({
        url: 'loadDataBlock2.html',
        type: 'POST',
        dataType: 'json',
        data: {
            "productId": productId
        },
        beforeSend: function() {
        	showProcessBarInAjax();
        },
        async: false
    })
    .done(function(data) {
        if (data == null) {
        	location.href = "./error.html?error=param";
        } else {
        	//BOE @rcv!Tran.Thanh 2014/07/30 : get role user
        	initRoleUser(data);
        	//EOE @rcv!Tran.Thanh 2014/07/30 : get role user

            initViewBlock2(data);
            initViewBlock5(data);
            initViewBlock6(data);
            //Load data block 7
            if ($('#groupCodeValue').text() != '0') {
            	//group code != 0, product in group
            	selectListSelectOfGroupProduct();
            } else {
            	//product not in group
                $('#blockOptionContent').html("");
            	$('#block15Content').html("");
            }

            //Load list guest input block 8
            loadGuestInputData();
            //Load list fit model block 9
            selectListFitModel();
            //Load link block 11
            loadListLinkOfProduct();
            //Load data block 12: video.
            loadVideoData();
            //Load data block 14: attribute
            loadAttributeData(productId, categoryCode);
            //Load init next, previous productID and status of botton.
            var changeProductMode = $("#changeProductMode").val();
            loadChangeProductIdBtn(productId,matterNo,changeProductMode);
            //BOE @rcv!Nguyen.Chuong Jul 17, 2014 #9955 : Get calibration request whole value and memo
            loadDataPopup(data);
            //EOE @rcv!Nguyen.Chuong Jul 17, 2014 #9811 : Get calibration request whole value and memo
            //count check product
            countProductCheckedInMatter(matterNo);
        }
    })
    .complete(function() {
    	hideProcessBarInAjax();
    });
}

/*
* Tran.Thanh : get role user
*/
function initRoleUser(productPreviewActionForm) {
//	roleUser = "[";
//	if (productPreviewActionForm.roleUser != null) {
//		for (var i=0; i < productPreviewActionForm.roleUser.length; i++) {
//			roleUser += productPreviewActionForm.roleUser[i];
//			roleUser += ",";
//		}
//		roleUser += "]";
//		return;
//	}
	//Default is limit user.
	roleUser = 0;
	//If role user not null.
	if (productPreviewActionForm.roleUser != null) {
		//Loop to check user has edit role.
		for (var i=0; i < productPreviewActionForm.roleUser.length; i++) {
			if (productPreviewActionForm.roleUser[i] == $("#roleMasterShowTxt").val()
					|| productPreviewActionForm.roleUser[i] == $("#roleMasterEditTxt").val()
					|| productPreviewActionForm.roleUser[i] == $("#roleManageTxt").val() ) {
				roleUser = 1; // User content MASTER_SHOW_ROLE_ID, MASTER_EDIT_ROLE_ID, USER_MANAGER_ROLE_ID
				return;
			}
		}
	}
}

//Change status of 2 check box, textArea and 2 button in tab all when user don't have role edit.
function checkRoleForPopup() {
	//limit user
	if (roleUser == 0) {
		$("#checkOne").attr("disabled", true);
		$("#checkTwo").attr("disabled", true);
		$("#allCommentTta").attr("disabled", true);
		$("#allCancelBtn").attr("disabled", true);
		$("#allSaveBtn").attr("disabled", true);
		$($(".editIcon ")).each(function() {
			  $(this).css("display", "none");
		});
		$("#newRowPopup").attr("disabled", true);
		$("#btnDelete").attr("disabled", true);
		return;
	}
	//Not limit user.
	$("#checkOne").attr("disabled", false);
	$("#checkTwo").attr("disabled", false);
	$("#allCommentTta").attr("disabled", false);
	$("#allCancelBtn").attr("disabled", false);
	$("#allSaveBtn").attr("disabled", false);
	$($(".editIcon ")).each(function() {
		  $(this).css("display", "inline");
	});
	$("#newRowPopup").attr("disabled", false);
	$("#btnDelete").attr("disabled", false);
}

function initViewBlock2(productPreviewActionForm) {
    $("#indexProductInMatter").html(productPreviewActionForm.indexProductInMatter);
    $("#totalProductInMatter").html(productPreviewActionForm.totalProductInMatter);
    $("#indexGroupInMatter").html(productPreviewActionForm.indexGroupInMatter);
    $("#totalGroupInMatter").html(productPreviewActionForm.totalGroupInMatter);
    $('#groupCodeValue').text(productPreviewActionForm.entProductGeneral.productGroupCode);
    //Hidden product_group_code when group code is 0
    if (productPreviewActionForm.entProductGeneral.productGroupCode == 0) {
    	$("#infoGroup").hide();
    } else {
    	$("#infoGroup").show();
    }
    productId = productPreviewActionForm.productId;
    matterNo = productPreviewActionForm.matterNo;
    if (productPreviewActionForm.entProductGeneral != null) {
    	categoryCode = productPreviewActionForm.entProductGeneral.productCategoryCode;
    }
    brandCode = productPreviewActionForm.entProductGeneral.productBrandCode;
}
/* EOE Function Block2 */

/* BOE Function Block3 */
function loadChangeProductIdBtn(productId, matterNo, changeProductMode) {
    $.ajax({
        type: "POST",
        url: "loadChangeProductIdBtn.html",
        dataType: "json",
        async: false,
        data: {
            productId : productId,
            matterNo: matterNo,
            changeProductMode : changeProductMode
        },
        beforeSend: function() {
        	showProcessBarInAjax();
        },
        success: function (data) {
            //Set next and previous productID
            nextProductId = data["nextProductId"];
            previousProductId = data["previousProductId"];
            //If next productID is null => no next productID => disable next button.
            if(nextProductId == null) {
                $("#btnNextTop, #btnNextBottom").attr("disabled", "disabled");
            } else {
                //Else enable next button.
                $("#btnNextTop, #btnNextBottom").removeAttr("disabled");
            }
            //If previous productID is null => no previous productID => disable previous button.
            if(previousProductId == null) {
                $("#btnPrevTop, #btnPrevBottom").attr("disabled", "disabled");
            } else {
                //Else enable previous button.
                $("#btnPrevTop, #btnPrevBottom").removeAttr("disabled");
            }
        },
        complete : function() {
        	hideProcessBarInAjax();
        },
        error: function () {
            
        },
    });
}
/**
* Handle hot key for productPrevie:
* - [Previous] Ctrl + Shift + PageUp
* - [Next] Ctrl + Shift + PageDown
* - [Edit] CTRL + E
* @author   Nguyen.Chuong
* @date     2014/06/06
*/
function handleHotKeyPress(e) {
    //Ctrl + shift key
    if (e.ctrlKey && e.shiftKey) {
        if(e.which == 34) {//Page down
            e.preventDefault();
            //If next button is not disabled then go to next product
            if($("#btnNextTop").attr("disabled") != "disabled") {
                //Go to next product
                $("#btnNextTop").click();
            }
        } else if (e.which == 33) {//Page up
            e.preventDefault();
            //If previous button is not disabled then go to previous product
            if($("#btnPrevTop").attr("disabled") != "disabled") {
                $("#btnPrevTop").click();
            }
        } else if (e.which == 69) {//E
            e.preventDefault();
            //Go to productEdit.
            $("#btnEditTop").click();
        }
    }
    
}
/* EOE Function Block3 */

/* BOE Function Block4 */
var isInitImage;
function loadDataBlock4(productId) {
    //Show loading bar
    $.ajax({
        url: 'loadDataBlock4.html',
        type: 'POST',
        dataType: 'json',
        data: {
            "productId": productId
        },
        async: false,
        beforeSend: function() {
        	showProcessBarInAjax();
        	var listView = $("#imagelistViewThumb").data("kendoListView");
        	if (listView == undefined) {
        		isInitImage = false;
        	}
        }
    })
    .done(function(data) {
        if (data == null || data.length == 2) {
            $("#message").addClass("has-message msg-error");
            $("#message").text($('#noProduct').val());
        } else {
            initImageBlock4(data);
            /* BOE @rcv!Tuong.Luong Verify Images thumbnail and exists */
            checkValidateThumbnail(data);
            checkValidateImgExists(data);
            if(currentFlagThumbnailValid == false){
		    	$("#thumbnail").addClass("blockError");
		        $("#thumbnail").addClass("errorFlg");
            }else{
            	$("#thumbnail").removeClass("blockError");
		        $("#thumbnail").removeClass("errorFlg");
            }
            if(currentFlagImgExists == false){
            	$("#imageExists").addClass("blockError");
		        $("#imageExists").addClass("errorFlg");
           }else{
        	   $("#imageExists").removeClass("blockError");
		        $("#imageExists").removeClass("errorFlg");
           }
           resetImageBlockFlag();
           //limit user hide edit icon pencil in block image.
           if (roleUser == 0) {
        	   $("#editIconBlockImage").css("display", "none");
	       }
            /* EOE @rcv!Tuong.Luong Verify Images thumbnail and exists */
        }
        //BOE @rcv!Nguyen.Chuong 2014/08/26 #11468 : load request list value and init all request type tooltip.
        loadDataForRequestTooltip(); //TODO load data and init icon.
        //EOE @rcv!Nguyen.Chuong 2014/08/26 #11468 : load request list value and init all request type tooltip.
    })
    .complete(function() {
    	hideProcessBarInAjax();
    });
}

function initImageBlock4(data) {
    // Define data get from sever and parse to json
    var productImageDetailJsonStr = $.parseJSON(data);

    initImageThumnail(productImageDetailJsonStr);
    var list = [];
    list.push(productImageDetailJsonStr[0]);
    initImageDetail(list);
}

/**
 * Tran.Thanh
 * @param param index to get list item
 */
function loadImage (param) {
	var listImage = $("#listThumb img");
	if (param <= listImage.length && param != 0) {
		var dataSourceThumb = $("#imagelistViewThumb").data('kendoListView').dataSource;
		var dataItem = dataSourceThumb.at(param-1);
		if (dataItem.productImage != "./images/no-image.jpg") {
			var list = [];
		    list.push(dataItem);
		    initImageDetail(list);
		    
		    var listView = $("#imagelistViewThumb").data("kendoListView");
		    listView.select(listView.element.children().get(param-1));
		    
		    //$(listImage[param-1]).click();
		}
	}
}

/**
 * Tran.Thanh 2014/06/17
 * @param ImageThumnailsonStr
 */
function initImageThumnail(ImageThumnailsonStr) {
	if (isInitImage) {
		var dataSource = new kendo.data.DataSource({
		  data:ImageThumnailsonStr
		});
		var listView = $("#imagelistViewThumb").data("kendoListView");
		listView.setDataSource(dataSource);
	} else {
		isInitImage = true;
		$("#imagelistViewThumb").kendoListView({
	        dataSource: ImageThumnailsonStr,
	        template  : kendo.template($("#templateImageThumbBlock2").html()),
	        dataBound : function () {
	        	// Remove noData message
				var gridData = this.dataSource.view();
				//Number record of grid
				var length = gridData.length;
				if (length > 0) {
					var count = 0;
					for ( var i = 0; i < length; i++) {
						var productImage = gridData[i].productImage;
						if (productImage != "./images/no-image.jpg") {
						//handleHotKeyImage(i);
							count ++;
						}
					}
					handleHotKeyImage(count);
				}
	        },
	        selectable: true,
	        change: function(e){
	        	var index = this.select().index();
	            var dataItem = this.dataSource.at(index);
	            if (dataItem.productImage != "./images/no-image.jpg") {
	            	//if new selected DataUid different with current then load new. else do nothing.
	                if (currentImageId != dataItem.uid) {
	                    //remove class fancy_box of current and add class fancy_box for new click image
	                    $("li[data-uid='" + currentImageId + "'] a").addClass("fancy_box");
	                    $("li[data-uid='" + dataItem.uid + "'] a").removeClass("fancy_box");
	                    currentImageId = dataItem.uid;
	                    var list = [];
	                    list.push(dataItem);
	                    initImageDetail(list);
	                }
	            } else {
	            	//if click to noImage then don't select.
	                jQuery(".list_thumb.k-state-selected").removeClass("k-state-selected");
	                jQuery("li[data-uid='" + currentImageId + "']").addClass("k-state-selected");
	            }
	            //limit user hide edit icon pencil in block image.
	        	if (roleUser == 0) {
	        		$("#editIconBlockImage").css("display", "none");
	        	}
	        	//If listImageRequest.length > 0
	        	// => image have individual data => show (!) icon, init kendoTooltip again and bind click event to new icon.
	        	if (listImageRequest.length > 0) {
	        		$("#requestToolTip_11").css("display","inline");
	        		initTooltipRequest(11, listImageRequest);
	        		$( "#requestToolTip_11" ).bind("click",function(e) {
	        			//Call function to toogle tooltip.
	        			toogleRequestTooltip();
	        			//Call function javaScript stopPropagation() and return false for not call default html click.
	        			e.stopPropagation();
	        		    return false;
	        		});
	        	} else {
	        		$("#requestToolTip_11").css("display","none");
	        	}
	        }
	    });
	}
	$("#listThumb img").on('load', function() {
    	$("#listThumb img").on('click', function() {
    		if (this.src.indexOf("/images/no-image.jpg") < 0) {
    			$("#dimensionThumbnailImage").empty();
    			$("#dimensionThumbnailImage").html($("#viewDimensionThumbnail").val() + ": " + this.naturalWidth + "x" + this.naturalHeight);
    			
    		} else {
    			if ($("#dimensionThumbnailImage").html() == ($("#viewDimensionThumbnail").val() + " : ")) {
    				$("#dimensionThumbnailImage").html($("#viewDimensionThumbnail").val() + " : ");
    			}
        	}
    		var storeImageObjectLength = storeImageObject.length;
	    	/*BOE rcv!Tuong.Luong Hanle hight light Image and thumbnail */
	    	for (var i = 0; i < storeImageObjectLength; i++) {
	    		if(storeImageObject[i].productThumb == this.src){
	    			if(storeImageObject[i].isExistsImage == false ){
	    				$("#viewDetailPath").addClass("blockError");
	    			}
	    			if(storeImageObject[i].isValidThumbnailSize == false ){
	    				$("#dimensionThumbnailImage").addClass("blockError");
	    			}
	    		}
	    	}
	    	/*EOE rcv!Tuong.Luong Hanle hight light Image and thumbnail */
    	});
    	//Load size of thumbnail in detail.
    	var firstImage = $(".selector_thumb")[0];
        //Remove fancy_box class of first image.
        $("li.list_thumb.k-state-selected a").removeClass("fancy_box");
    	if (firstImage.src.indexOf("/images/no-image.jpg") < 0) {
    	    //Add class fancy_box and change to cursor pointer when first image is different with no-image
    	    $("#imagelistView a").addClass("fancy_box").css('cursor',"pointer");
    		$("#dimensionThumbnailImage").html($("#viewDimensionThumbnail").val() + ": " + firstImage.naturalWidth + "x" + firstImage.naturalHeight);
    	} else {
    	    //Remove class fancy_box and change to cursor default when first image is different with no-image
    	    $("#imagelistView a").removeClass("fancy_box").css('cursor',"default");
    		$("#dimensionThumbnailImage").html($("#viewDimensionThumbnail").val() + " : ");
    	}
    });
    var listView = $("#imagelistViewThumb").data("kendoListView");
    listView.select(listView.element.children().first());
}

/**
 * Tran.Thanh 2014/06/17
 * @param jSonImage
 */
function initImageDetail(jSonImage) {
    $("#imagelistView").kendoListView({
        dataSource: jSonImage,
        template  : kendo.template($("#templateImageBlock2").html()),
        dataBound : function () {
            //init click image show fancybox
            initClickShowImage();
            var dataItem = this.dataSource.at(0);
            if (dataItem.productImage == "./images/no-image.jpg") {
            	$("#viewDetailPath").html($("#detailedImagePath").val() + " : ");
            	$("#viewThumbnailPath").html($("#thumbnailPath").val() + " : ");
            	$("#dimensionThumbnailImage").html($("#viewDimensionThumbnail").val() + " : ");
            }
        },
        selectable: false,
    });

    $('#imageThumb').on('load', function() {
    	if (this.src.indexOf("/images/no-image.jpg") < 0) {
    		$("#dimensionDetailImage").html($("#viewDimensionDetail").val() + ": " + this.naturalWidth + "x" + this.naturalHeight);
    	} else {
    		$("#dimensionDetailImage").html($("#viewDimensionDetail").val() + " : ");
    	}
    });

}

/**
 * Tran.Thanh 2014/06/13
 * Load fancybox when click image thumb.
 */
function initClickShowImage() {
    $(".fancy").click(function() {
        $(".fancy_box").fancybox({
            'showCloseButton' : false,
            'enableEscapeButton' : true,
            'padding' : 10,
            'transitionIn' : 'elastic',
            'transitionOut' : 'fade',
            beforeShow: function() {
                // Thumb image is Loading
                //isImageThumbLoading = true;
            },
            beforeClose: function() {
                // Thumb image is closed
                //isImageThumbLoading = false;
            }
        });
    });
}

/**
 * Tran.Thanh
 * Handle init image
 * @param i
 */
function handleHotKeyImage(count) {
	for(var j = 0; j < count; j ++) {
		initKeypressImage(j+1);
	}
}

function initKeypressImage(index) {
	switch(index) {
	    case 1:
	    	$(document).keydown(function(e) {
	    		if (e.ctrlKey && e.shiftKey) {
	    	    	if (e.which == 49) {
	    	            e.preventDefault();
	    	            loadImage(index);
	    	        }
	    		}
	    	});
	        break;
	    case 2:
	    	$(document).keydown(function(e) {
	    		if (e.ctrlKey && e.shiftKey) {
	    	    	if (e.which == 50) {
	    	            e.preventDefault();
	    	            loadImage(index);
	    	        }
	    		}
	    	});
	        break;
	    case 3:
	    	$(document).keydown(function(e) {
	    		if (e.ctrlKey && e.shiftKey) {
	    	    	if (e.which == 51) {
	    	            e.preventDefault();
	    	            loadImage(index);
	    	        }
	    		}
	    	});
	        break;
	    case 4:
	    	$(document).keydown(function(e) {
	    		if (e.ctrlKey && e.shiftKey) {
	    	    	if (e.which == 52) {
	    	            e.preventDefault();
	    	            loadImage(index);
	    	        }
	    		}
	    	});
	        break;
	    case 5:
	    	$(document).keydown(function(e) {
	    		if (e.ctrlKey && e.shiftKey) {
	    	    	if (e.which == 53) {
	    	            e.preventDefault();
	    	            loadImage(index);
	    	        }
	    		}
	    	});
	        break;
	    case 6:
	    	$(document).keydown(function(e) {
	    		if (e.ctrlKey && e.shiftKey) {
	    	    	if (e.which == 54) {
	    	            e.preventDefault();
	    	            loadImage(index);
	    	        }
	    		}
	    	});
	        break;
	    case 7:
	    	$(document).keydown(function(e) {
	    		if (e.ctrlKey && e.shiftKey) {
	    	    	if (e.which == 55) {
	    	            e.preventDefault();
	    	            loadImage(index);
	    	        }
	    		}
	    	});
	        break;
	    case 8:
	    	$(document).keydown(function(e) {
	    		if (e.ctrlKey && e.shiftKey) {
	    	    	if (e.which == 56) {
	    	            e.preventDefault();
	    	            loadImage(index);
	    	        }
	    		}
	    	});
	        break;
	    case 9:
	    	$(document).keydown(function(e) {
	    		if (e.ctrlKey && e.shiftKey) {
	    	    	if (e.which == 57) {
	    	            e.preventDefault();
	    	            loadImage(index);
	    	        }
	    		}
	    	});
	        break;
	    case 10:
	    	$(document).keydown(function(e) {
	    		if (e.ctrlKey && e.shiftKey) {
	    	    	if (e.which == 48) {
	    	            e.preventDefault();
	    	            loadImage(index);
	    	        }
	    		}
	    	});
	        break;
	    default:
	        e.preventDefault();
	}
}
/* EOE Function Block4 */

/* BOE Function Block5 */
/**
 * Tran.Thanh 2014/06/13
 * @param productPreviewActionForm
 */
function initViewBlock5(productPreviewActionForm) {
    if (productPreviewActionForm.entProductGeneral != null) {
        $("#viewProductId").html(" P" + productPreviewActionForm.entProductGeneral.productId);
        $("#viewProductSysCode").html(productPreviewActionForm.entProductGeneral.productSyouhinSysCode);
        $("#viewCategoryName").html( $("#hdCategory").val() + " : " + productPreviewActionForm.entProductGeneral.productCategoryName);
        $("#viewBrandName").html($("#hdBrand").val() + " : " + productPreviewActionForm.entProductGeneral.productBrandName);

		//Add tooltip for Category Name and Brand Name
        destroyTooltip(".brandNameTooltip");
        $(".brandNameTooltip").kendoTooltip({
            content: $("#hdBrandCode").val() + " : " + productPreviewActionForm.entProductGeneral.productBrandCode,
            animation : {
                close : {
                    effects : "fade:out"
                },
                open : {
                    effects : "fade:in",
                    duration : 300
                }
            }
        });

        destroyTooltip(".categoryNameTooltip");
        $(".categoryNameTooltip").kendoTooltip({
            content: $("#hdItemCode2").val() + " : " + productPreviewActionForm.entProductGeneral.productCategoryCode,
            animation : {
                close : {
                    effects : "fade:out"
                },
                open : {
                    effects : "fade:in",
                    duration : 300
                }
            }
        });
    }
    if (productPreviewActionForm.listSupplier != null) {
    	if (productPreviewActionForm.listSupplier[0] != null) {
//    		$("#viewSiire1").html(productPreviewActionForm.listSupplier[0].siireName);
//    		$("#viewNouki1").html(productPreviewActionForm.listSupplier[0].noukiName1);
    		$("#viewSiireName1").html($("#viewSupplierName").val() + "1 : "
    				+ "<span id='viewSiire1'>" + productPreviewActionForm.listSupplier[0].siireName + "</span>"
    				+ " / "
    				+ "<span id='viewNouki1'>" + productPreviewActionForm.listSupplier[0].noukiName1) + "</span>";
    		//Add tooltip for Siire And Nouki
            destroyTooltip("#viewSiire1");
            $("#viewSiire1").kendoTooltip({
                content: $("#viewSiireCode").val() + " : " +  productPreviewActionForm.listSupplier[0].siireCode,
                animation : {
                    close : {
                        effects : "fade:out"
                    },
                    open : {
                        effects : "fade:in",
                        duration : 300
                    }
                }
            });
          //Add tooltip for Siire And Nouki
            destroyTooltip("#viewNouki1");
            $("#viewNouki1").kendoTooltip({
                content: $("#viewNoukiCode").val() + " : " + productPreviewActionForm.listSupplier[0].noukiCode,
                animation : {
                    close : {
                        effects : "fade:out"
                    },
                    open : {
                        effects : "fade:in",
                        duration : 300
                    }
                }
            });
            $(".siire1").show();
    	} else {
    		$(".siire1").hide();
    	}
    	if (productPreviewActionForm.listSupplier[1] != null) {
//    		$("#viewSiire2").html(productPreviewActionForm.listSupplier[1].siireName);
//    		$("#viewNouki2").html(productPreviewActionForm.listSupplier[1].noukiName1);
    		$("#viewSiireName2").html($("#viewSupplierName").val() + "2 : "
    				+ "<span id='viewSiire2'>" + productPreviewActionForm.listSupplier[1].siireName + "</span>"
    				+ " / "
    				+ "<span id='viewNouki2'>" + productPreviewActionForm.listSupplier[1].noukiName1) + "</span>";
    		//Add tooltip for Siire And Nouki
            destroyTooltip("#viewSiire2");
            $("#viewSiire2").kendoTooltip({
                content: $("#viewSiireCode").val() + " : " + productPreviewActionForm.listSupplier[1].siireCode,
                animation : {
                    close : {
                        effects : "fade:out"
                    },
                    open : {
                        effects : "fade:in",
                        duration : 300
                    }
                }
            });
          //Add tooltip for Siire And Nouki
            destroyTooltip("#viewNouki2");
            $("#viewNouki2").kendoTooltip({
                content: $("#viewNoukiCode").val() + " : " + productPreviewActionForm.listSupplier[1].noukiCode,
                animation : {
                    close : {
                        effects : "fade:out"
                    },
                    open : {
                        effects : "fade:in",
                        duration : 300
                    }
                }
            });
            $(".siire2").show();
    	} else {
    		$(".siire2").hide();
    	}
    }

}

/**
* destroy tooltip before create
* @author   Luong.Dai
* @date     2014/08/11
*/
function destroyTooltip(selector) {
    //Add tooltip for Category Name and Brand Name
    var tooltip = $(selector).data("kendoTooltip");
    if (tooltip != undefined) {
        tooltip.destroy();
    }
}
/* EOE Function Block5 */

/* BOE Function Block6 */
function roundSupplierPriceWithMarumeCode(value, marumeCode) {
	var result = value;
	switch(marumeCode) {
		case '1':
			result = truncateDecimal(value);
			break;
		case '2':
			result = roundUpDecimal(value);
			break;
		case '3':
			result = truncateDecimal(value);
			break;
		case '4':
			result = roundingDecimal(value);
			break;
		case '5':
			result = roundUpSingleDigit(value);
			break;
		case '6':
			result = truncateSingleDigit(value);
			break;
		case '7':
			result = roundingSingleDigit(value);
			break;
		case '8':
			break;
		case '9':
			break;
		case '10':
			break;
	};
	return result;
}
/**
 * Tran.Thanh 2014/06/16
 * init view info general product
 * @param productPreviewActionForm data
 */
function initViewBlock6(productPreviewActionForm) {
    if (productPreviewActionForm.entProductGeneral != null) {
    	//Init product Name
        $("#viewProductName").html(productPreviewActionForm.entProductGeneral.productName);
        /* BOE rcv!Tuong.Luong Hanle hightlight jan and product code */
        if(productPreviewActionForm.entProductGeneral.validProductCode == false
        		 || productPreviewActionForm.entProductGeneral.validJanCode == false){
        	if (productPreviewActionForm.entProductGeneral.validProductCode == false){
        		$("#viewProductCode").addClass("blockError");
        		$("#validateCode").addClass("errorFlg");
        	}
        	if (productPreviewActionForm.entProductGeneral.validJanCode == false) {
        		$("#viewProductJanCode").addClass("blockError");
                $("#validateCode").addClass("errorFlg");
        	}
        }else{
        	$("#viewProductJanCode").removeClass("blockError");
        	$("#viewProductCode").removeClass("blockError");
    		$("#validateCode").removeClass("errorFlg");
        }

        /* EOE rcv!Tuong.Luong Hanle hightlight jan and product code */
        //Init productCode
        $("#viewProductCode").empty();
        $("#viewProductCode").html($("#viewProductCode2").val() + " : " + productPreviewActionForm.entProductGeneral.productCode);
        if (productPreviewActionForm.entProductGeneral.productWebikeCodeFlg == 1) {
        	$("#viewProductCode").append(" ( "+$("#hdProductWebikeFlag").val()+")");
        }
        //Init Jan code
        $("#viewProductJanCode").html($("#viewJan").val() + " : " + productPreviewActionForm.entProductGeneral.productEanCode);
        //Init Supplier Price
        initViewSupplier(productPreviewActionForm);
    	
    	//init view Select
    	initViewSelect(productPreviewActionForm);
    }
}

function initViewSupplier(productPreviewActionForm) {
	$("#viewProperPrice").html("&#165;" + productPreviewActionForm.entProductGeneral.productProperPrice);
    var productProperPriceTax = removeDecimal(productPreviewActionForm.entProductGeneral.productProperPrice * TAX);
    //remove decimal
    productProperPriceTax = removeDecimal(productProperPriceTax);
    $("#viewProperPriceTax").html("&#165;" + productProperPriceTax);

    productProperPrice = productPreviewActionForm.entProductGeneral.productProperPrice;
	productSupplierPrice = productPreviewActionForm.entProductGeneral.supplierPricePrice;
	if (productProperPrice == 0) {
		$("#viewSupplierPriceRate").html("&#165; "+ productPreviewActionForm.entProductGeneral.supplierPricePrice +"(0%)");
	} else {
		var supplierPriceRate = (parseFloat(productSupplierPrice) / parseFloat(productProperPrice)) * 100.00;
		//remove decimal
		supplierPriceRate = removeDecimal(supplierPriceRate);
		$("#viewSupplierPriceRate").html("&#165;" +productPreviewActionForm.entProductGeneral.supplierPricePrice
				+ " (" + supplierPriceRate + "%)");
	}
}

function initViewSelect(productPreviewActionForm) {
	//Init select Code
	$("#viewProductOpenPriceFlg").parent().css("background-color", "white");
	$("#viewProductProperSellingFlg").parent().css("background-color", "white");
	$("#viewProductOrderProductFlg").parent().css("background-color", "white");
	$("#viewProductNoReturnableFlg").parent().css("background-color", "white");
	$("#viewProductAmbiguousImageFlg").parent().css("background-color", "white");

    if (productPreviewActionForm.entProductGeneral.productOpenPriceFlg != '0' ) {
    	$("#viewProductOpenPriceFlg").parent().css("background-color", "antiquewhite");
    }
    if (productPreviewActionForm.entProductGeneral.productProperSellingFlg != '0' ) {
    	$("#viewProductProperSellingFlg").parent().css("background-color", "antiquewhite");
    }
    if (productPreviewActionForm.entProductGeneral.productOrderProductFlg != '0' ) {
    	$("#viewProductOrderProductFlg").parent().css("background-color", "antiquewhite");
    }
    if (productPreviewActionForm.entProductGeneral.productNoReturnableFlg != '0' ) {
    	$("#viewProductNoReturnableFlg").parent().css("background-color", "antiquewhite");
    }
    if (productPreviewActionForm.entProductGeneral.productAmbiguousImageFlg != '0' ) {
    	$("#viewProductAmbiguousImageFlg").parent().css("background-color", "antiquewhite");
    }
    if ($(".selectCode").html().length == 0) {
    	$(".selectCode").hide();
    }
}
/* EOE Function Block6 */

/* BOE Function Block7 */
/**
* Read list select of group product
* @author 	Luong.Dai
* @param 	productId 	Id of product
* @param 	brandCode 	brandCode
* @param 	matterNo	matterNo
* @date 	2014/06/14
*/
function selectListSelectOfGroupProduct() {
	$('#groupCodeValue').val();
	$('#blockOptionContent').html("");
	$.ajax({
		url: 'loadListSelectOfProduct.html',
		type: 'POST',
		dataType: 'json',
		data: {
			productId: productId
			, groupCode : $('#groupCodeValue').text()
			, brandCode: brandCode
			, matterNo: matterNo
		},
        beforeSend: function() {
        	showProcessBarInAjax();
        }
	})
	.done(function(data) {
		if (typeof data != undefined && data != null) {
			$('#blockOptionContent').html("");
			var html = "";
			//Index of maker
			var idxTbl = 0;
			for (key in data) {
				html += '<div class="rowContent" style="margin-top: 5px;">'
					+ 		'<select name="' + key + '" id="cbxSelect' + idxTbl + '" class="cbbBlock7">';
				//Set select name to top combobox
				//Select name and list select display
				var selectValue = data[key];
				for (selectFullName in selectValue) {
					//Set default selected value if full name of select
					html += '<option value=" ">' + selectFullName + '</option>';
					//List select display of select name
					var listSelectDisplay = selectValue[selectFullName];
					//Replace string "please choise"
					/*var selectName = selectFullName.replace($('#pleaseChoiseOption').val(), "");*/
					//Add option to list combobox
					for (display in listSelectDisplay) {
						//display value
						var value = listSelectDisplay[display];
						if (value.productSelectSort != null && value.productSelectSort == 0) {
							//Set selected display value of current product
							html += '<option selected="" value="' + value.productSelectDisplay + '">' /*+ selectName + ' : '*/ + value.productSelectDisplay + '</option>';	
						} else {
							html += '<option value="' + value.productSelectDisplay + '">' /*+ selectName + ' : '*/ + value.productSelectDisplay + '</option>';
						}
					}
				}
				//Close tag
				html += '</select></div>';

				idxTbl++;
			}
			$('#blockOptionContent').html(html);
			//Set list display for block 15
			viewListDisplay(data);
			//init kendo combobox to list attribute
			handleOptionKendoCombobox();
		}
	})
	.complete(function() {
    	hideProcessBarInAjax();
    });
}

/**
* init kendo combobox for list option and handle change event
* @author 	Luong.Dai
* @date 	2014/06/16
* If change data of combobox, reload product data
*/
function handleOptionKendoCombobox() {
	$('.cbbBlock7').kendoDropDownList({
		width: 400,
		change : function() {
            //Set flg next/prev to false
            NEXT_PREV_FLG = 0;
			//Reload proper price, supplier price and attribute
			handleReloadProductDataWhenChangeOption();
		}
	});
}

/**
* Reload data of product when change option data
* @author 	Luong.Dai
* @date 	2014/06/16
*/
function handleReloadProductDataWhenChangeOption() {
	//format : select code _ select display : select code _ select display....
	var lstSelectValue = "";
	//Get list option
	var lstOption = $('select.cbbBlock7');
	//Loop to get list option value
	for (var i = 0; i < lstOption.length; i++) {
		var option = lstOption[i];
		var selectCode = $(option).attr('name');
		var selectValue = $(option).val();
		if (i > 0) {
			//split symbol
			lstSelectValue += "|";
		}
		lstSelectValue += selectCode + "_" + selectValue;
	}
	if (lstSelectValue != "") {
		$.ajax({
			url: 'loadProductDataWhenChangeOption.html',
			type: 'POST',
			dataType: 'json',
			data: {
				selectValue : lstSelectValue
				, productId : productId
				, groupCode : $('#groupCodeValue').text()
				, brandCode : brandCode
				, matterNo 	: matterNo
			},
	        beforeSend: function() {
	        	showProcessBarInAjax();
	        }
		})
		.done(function(data) {
			var productGeneral = data.entProductGeneral;
			var properPrice 	= NO_PRICE;
			var supplierPrice 	= NO_PRICE;
			var properPriceTax 	= NO_PRICE;
			var rate 			= 0;
			if (productGeneral != null) {
                $('.cbbBlock7 .k-dropdown-wrap .k-input').removeClass('error-option');
				//cannot get product data
				properPrice = productGeneral.productProperPrice;
				supplierPrice = productGeneral.supplierPricePrice;
				properPriceTax = TAX * parseInt(properPrice);
				properPriceTax = removeDecimal(properPriceTax);

				//Calc supplier rate
				if (properPrice == 0) {
					rate = 0;
				} else {
					rate = (parseFloat(supplierPrice) / parseFloat(properPrice)) * 100.00;
					//Remove decimal
					rate = removeDecimal(rate);
				}
				//BOE @rcv! Luong.Dai 2014/08/12: reload product data when change option
				//Load attribute data
				//loadAttributeData(productGeneral.productId, productGeneral.productCategoryCode);
                 //Change flag: next/prev function
                NEXT_PREV_FLG = 1;
                //Change current productId.
                productId = productGeneral.productId;
                //Load new status of next/previous button and next/previous productId
                var changeProductMode = $("#changeProductMode").val();
                loadChangeProductIdBtn(productId,matterNo,changeProductMode);
                ////Load all data of next productId
                loadDataByProductId(productId);
                initRequestPopUp();
                loadCheckPopUp(productId);
                hideBackgroundPopUp();
                //EOE @rcv! Luong.Dai 2014/08/12: reload product data when change option
			} else {
				//No attribute chuongggg
//				//Hide table
//				$('#attributeTable').css("display", "none");
//				//Show no data
//				$('#attributeTableNodata').css("display", "block");
                //BOE @rcv! Luong.Dai 2014/08/12: If no product, don't reload attribute
			    // Show no data message
                /*$("#attributeTable").find("tbody").html(
                        '<td id="attributeTableNodata" colspan="3"><div class="dataTables_empty" >'
                                + $("#noDataMessage").val() + '</div></td>');*/
                //Change background color of combobox option to pink
                $('.cbbBlock7 .k-dropdown-wrap .k-input').addClass('error-option');
                //EOE @rcv! Luong.Dai 2014/08/12: If no product, don't reload attribute
			}
			//Set data
			$('#viewProperPrice').html("&#165;" + properPrice);
			$('#viewProperPriceTax').html("&#165;" + properPriceTax);
			$('#viewSupplierPriceRate').html("&#165;" + supplierPrice + " (" + rate + "%)");
		})
		.complete(function() {
	    	hideProcessBarInAjax();
	    });
	}
}

/**
* Remove decimal: example: 20.5 = 20
* @author 	Luong.Dai
*/
function removeDecimal(number) {
	if (typeof number != undefined && number != null) {
        number = parseInt(number);
		//Check number < 0
		if ((number - 0.5) >= 0) {
			number = number - 0.5;
		}
		number = number.toFixed(0);
		return number;
	}
	return 0;
}
/* EOE Function Block7 */

/* BOE Function Block8 */
/**
* Load list guest input of product by ajax
* @author   Luong.Dai
* @date     2014/06/13
*/
function loadGuestInputData() {
    //Remove current description
    $('#guestInputDescription').val("");
    //Call ajax to get guest input string
    $.ajax({
        url: 'selectGuestInputDescription.html',
        type: 'POST',
        dataType: 'json',
        data: {
            productId: productId
        },
        beforeSend: function() {
        	showProcessBarInAjax();
        }
    })
    .done(function(data) {
        if (typeof data != undefined && data != null) {
            if (data.length > 0) {
                //Has guest input data
                $('#block8').closest("div.row")
            				.css("display", "block");
                //Remove content
                $('#guestInputContent').html("");
                var description = "";
                var title = "";
                for (var i = 0; i < data.length; i++) {
                    if (i > 0) {
                        //Appent "." symbol
                        description += DOT_SYSBOL;
                    }
                    //Add to string description
                    description += data[i].productGuestInputDescription;
                    //Set guest input title to input
                    title   += "<div class='rowContent'>"
                            + "<input type='text' name='' maxlength='100' value='"
                                    + data[i].productGuestInputTitle + ":' id='' class='tbBlock8 k-textbox'>"
                            + "</div>";
                }
                //Set data to view
                $('#guestInputContent').html(title);
                //Description
                var descriptionValue = $('#hdGuestInputDescription').val();
                descriptionValue = descriptionValue.replace(/{[0]}/g, description);
                $('#guestInputDescription').html(descriptionValue);
            } else {
            	$('#block8').closest("div.row")
            				.css("display", "none");
            }
        }
    })
    .complete(function() {
    	hideProcessBarInAjax();
    });
}
/* EOE Function Block8 */

/* BOE Function Block9 */
/**
* Load list fit model of product by ajax
* @author   Luong.Dai
* @date     2014/06/13
*/
function selectListFitModel() {
    $('#block9 tbody').htm;
    $.ajax({
        url: 'selectFitModelOfProduct.html',
        type: 'POST',
        dataType: 'json',
        data: {
            productId: productId
        },
        beforeSend: function() {
        	showProcessBarInAjax();
        }
    })
    .done(function(data) {
        if (typeof data != undefined && data != null) {
            $('#block9 tbody').html("");
            var html = "";
            //Index of maker
            var idxTbl = 0;
            for (key in data) {
                //Add model image
                html += "<tr>";
                   // +   "<td class='left'>";
                if(data[key][0].validMakerName == false){
                	html  +=  "<td class='leftInvalid'>";
                	currentFlagFitModel = false;
                }else{
                	html += "<td class='left'>";
                }
                if (key == $('#hdNoMakerLogo').val()) {
                	//No maker logo
                	html += '<span align="center">' + key + '</span>';
                } else {
                	//Show logo image
                	html += "<img src='http://img.webike.net" + key + "'>";
                }
                html +=   "</td>"
					+   "<td class='right'>"
                    +       "<table class='innertbl' id='innertbl" + idxTbl + "'>"
                    +           "<tbody>";
                //List model of maker
                var lstModel = data[key];
                var i;
                var lstModelLength = lstModel.length;
                for (i = 0; i < lstModelLength; i++) {
                	/* BOE Rcv!Tuong.Luong verify Fit Model block */
                	if (lstModel[i].validModelName == false || lstModel[i].validMakerName == false){
                		currentFlagFitModel = false;
                	}
                	/* EOE Rcv!Tuong.Luong verify Fit Model block */
                    //Show only 5 record
                    if (i >= MAX_FIT_MODEL) {
                        //Hide record has index >= 5
                        html += "<tr class='innertbl_tr' style='display: none;'>";
                    } else {
                        html += "<tr class='innertbl_tr'>"; 
                    }
                    //Show model of maker
                    /* BOE Rcv!Tuong.Luong highlight verify Fit Model block */
                    if (lstModel[i].validModelName == false){
                    	html += "<td class='innertbl_td_invalid'>"
                    		+           lstModel[i].fitModelModel
                    		+			" "
                    		+			lstModel[i].fitModelStyle
                    		+       "</td>"
                    		+   "</tr>";
                    }else{
                    	 html += "<td class='innertbl_td'>"
                             +           lstModel[i].fitModelModel
                             +			 " "
                     		 +			 lstModel[i].fitModelStyle
                             +       "</td>"
                             +   "</tr>";
                    }
                    /* EOE Rcv!Tuong.Luong highlight verify Fit Model block */
                }
                html += "</tbody></table>";
                if (i > MAX_FIT_MODEL) {
                    //If maker has more than 5 model, show link show more model
                    html += "<div style='padding: 5px 0px 0px 0px;'>"
                        + "<a href='javascript:void(0)' onclick='viewMoreFitModel(" + idxTbl + "); return false;' id='viewMoreLink" + idxTbl + "'>"
                        +   $('#viewMoreModel').val() + "</a></div>"
                        + "<div style='padding: 0px 0px 5px 0px;'>"
                        + "<a style='display: none;' href='javascript:void(0)' onclick='viewLessFitModel(" + idxTbl + "); return false;' id='viewLessLink" + idxTbl + "'>"
                        +   $('#viewLessModel').val() + "</a></div>";
                }
                    
                html += "</td></tr>";
                idxTbl++;
            }
            //Check no data
            if (html != "") {
            	$('#block9 tbody').html(html);
            	//Show table data
            	$('#block9').closest("div.row")
            				.css("display", "block");
            } else {
            	//No fitmodel data
            	//Hide table data
            	$('#block9').closest("div.row")
            				.css("display", "none");
            }
            /* BOE rcv!Tuong.Luong Turn On White Flag Fit Model */
        	//Add class to label
        	if (currentFlagFitModel == false){
        		$("#fitModel").addClass("blockError");
                $("#fitModel").addClass("errorFlg");
        		//$("#fitModel").attr("style","background-color:red");
        	}else{
        		$("#fitModel").removeClass("errorFlg");
        		$("#fitModel").removeClass("blockError");
        		//$("#fitModel").addClass("ItemValidate");
        	}
        	//reset
        	currentFlagFitModel = true;
        	/* EOE rcv!Tuong.Luong Turn On White Flag Fit Model */
        }
    })
    .complete(function() {
    	hideProcessBarInAjax();
    });
}

/**
* view all model of maker
* @author   Luong.Dai
* @param    index of maker
* @date     2014/06/14
*/
function viewMoreFitModel(index) {
    //id of table in html is innertbl + index of maker
    var idTable = '#innertbl' + index;

    //View all model of maker
    $(idTable + " tr").css("display", "block");
    //Hide link show more model
    $('#viewMoreLink' + index).css("display", "none");
    //Show link show less model
    $('#viewLessLink' + index).css("display", "block");
}

/**
* view 5 model of maker
* @author   Luong.Dai
* @param    index of maker
* @date     2014/06/14
*/
function viewLessFitModel(index) {
    //id of table in html is innertbl + index of maker
    var idTable = '#innertbl' + index;

    //View all model of maker
    var tags = $(idTable + " tr");
    if (tags != null) {
        for (var i = MAX_FIT_MODEL; i < tags.length; i++) {
            //Show only 5 record
            $(tags[i]).css("display", "none");
        }
    }
    //Hide link show less model
    $('#viewLessLink' + index).css("display", "none");
    //Show link show more model
    $('#viewMoreLink' + index).css("display", "block");
}
/* EOE Function Block9 */

/* BOE Function Block10 */
function loadDataBlock10(productId) {
    //Show loading bar
    $.ajax({
        url: 'loadDataBlock10.html',
        type: 'POST',
        dataType: 'json',
        data: {
            "productId": productId
        }/*,
        async: false,*/
	    , beforeSend: function() {
	    	showProcessBarInAjax();
	    }
    })
    .done(function(data) {
        // BOE @rcv!Nguyen.Chuong Jul 23, 2014 #9955 : default remove error class.
        $("#viewDescriptionSummary").removeClass("blockError");
        $("#viewDescriptionCaution").removeClass("blockError");
        $("#viewDescriptionRemark").removeClass("blockError");
        $("#viewDescriptionSentence").removeClass("blockError");
        $("#html").removeClass("errorFlg");
        // EOE @rcv!Nguyen.Chuong Jul 23, 2014 #9955 : default remove error class.
        if (data == null) {
        	$("#viewDescriptionSummary").html("");
        	$("#viewDescriptionCaution").html("");
        	$("#viewDescriptionRemark").html("");
        	$("#viewDescriptionSentence").html("");
        } else {
            initDescriptionBlock10(data);
        }
    })
    .complete(function() {
    	hideProcessBarInAjax();
    });
}

function initDescriptionBlock10(entDescription) {
	/*$("#viewDescriptionSummary").html(entDescription.descriptionSummary);
	$("#viewDescriptionCaution").html(entDescription.descriptionCaution);
	$("#viewDescriptionRemark").html(entDescription.descriptionRemarks);
	$("#viewDescriptionSentence").html(entDescription.descriptionSentence);*/
    viewDescriptionValue("viewDescriptionSummary",  entDescription.descriptionSummary,  "summaryFrame");
    viewDescriptionValue("viewDescriptionCaution",  entDescription.descriptionCaution,  "cautionFrame");
    viewDescriptionValue("viewDescriptionRemark",   entDescription.descriptionRemarks,  "remarksFrame");
    viewDescriptionValue("viewDescriptionSentence", entDescription.descriptionSentence, "sentenceFrame");
    // BOE @rcv!Nguyen.Chuong Jul 23, 2014 #9955 : check and add class error validate when invalid HTML 
    if (!entDescription.descriptionSummaryErrorFlg) {
        $("#viewDescriptionSummary").addClass("blockError");
        $("#html").addClass("errorFlg");
    }
    if (!entDescription.descriptionCautionErrorFlg) {
        $("#viewDescriptionCaution").addClass("blockError");
        $("#html").addClass("errorFlg");
    }
    if (!entDescription.descriptionRemarksErrorFlg) {
        $("#viewDescriptionRemark").addClass("blockError");
        $("#html").addClass("errorFlg");
    }
    if (!entDescription.descriptionSentenceErrorFlg) {
        $("#viewDescriptionSentence").addClass("blockError");
        $("#html").addClass("errorFlg");
    }
    // EOE @rcv!Nguyen.Chuong Jul 23, 2014 #9955 : check and add class error validate when invalid HTML
    /* BOE #7206 Luong.Dai 2014/06/24 Change color of caution to Mallon */
    $("#cautionFrame").contents().find('body').css('color', 'maroon');
    /* EOE #7206 Luong.Dai 2014/06/24 Change color of caution to Mallon */
}

/**
* view discription with html format
* @author   Luong.Dai
* @date     2014/6/19
*/
function viewDescriptionValue(id, value, idOfFrame) {
    $("#" + id).html("<iframe id='" + idOfFrame +"' frameborder='0' style='overflow-y:hidden; overflow-x:scroll; width: 100%;height: 0px;' verticalscrolling='no'></iframe>");
    $("#" + idOfFrame).get()[0].contentWindow.document.write(value);
    /* BOE #7206 Luong.Dai 2014/06/24 Change font font of content same with title */
    $("#" + idOfFrame).contents().find('body').css('font','12px "Helvetica Neue", Helvetica, Arial, sans-serif');
    /* BOE Luong.Dai 2014/07/04 Go to new tab when click link on iframe */
    $("#" + idOfFrame).contents().find('a').attr('target', idOfFrame);
    /* EOE Luong.Dai 2014/07/04 Go to new tab when click link on iframe */
    /* EOE #7206 Luong.Dai 2014/06/24 Change font font of content same with title */
    if (value != "") {
        $('#' + idOfFrame).on('load', function() {
            setTimeout('resizeIframe(\"' + this.id + '\")', 1000);
            /*BOE rcv!Nguyen.Chuong #11468 2014/08/29 call event click in iframe.*/
        	document.getElementById(idOfFrame).contentWindow.document.onclick = 
        		function() {
	    			var tooltipList = $(".requestToolTip:visible");
	    			var kendoTooltip;
	        		//Check if status tooltip is show => hide all tooltip.
	        		tooltipStatus = "hide";
	        		$.each(tooltipList, function(index, tooltip){
	        			kendoTooltip = $(tooltip).data("kendoTooltip");
	        			kendoTooltip.hide();
	        		});
        	};
        	/*EOE rcv!Nguyen.Chuong #11468 2014/08/29 call event click in iframe.*/
        });
    }
    $("#" + idOfFrame).get()[0].contentWindow.document.close();
}

/**
* Resize height of frame to full height
* @author   Luong.Dai
* @date     2014/6/19
*/
function resizeIframe(frameId) {
    var height = $(document.getElementById(frameId).contentWindow.document).height() + 20;
    document.getElementById(frameId).style.height = height +"px";
    //Remove scroll
    $("#" + frameId).contents().find("head")
                                .append($("<style type='text/css'>  body{overflow-y:hidden;}  </style>"));
};
/* EOE Function Block10 */

/* BOE Function Block11 */
/**
* Load list link of product by ajax
* @author   Luong.Dai
* @param    productId   Id of product
* @date     2014/06/13
*/
function loadListLinkOfProduct() {
    $('.content11 ul').html("");
    $.ajax({
        url: 'selectLinkOfProduct.html',
        type: 'POST',
        dataType: 'json',
        data: {
            productId: productId
        },
        beforeSend: function() {
        	showProcessBarInAjax();
        }
    })
    .done(function(data) {
        if (typeof data != undefined && data != null) {
            if (data.length > 0) {
                //Hide no data message
                $('#noLinkData').css("display", "none");
                $('.content11 ul').css("display", "block");

                var html = "";
                linkReason = "";
                for (var i = 0; i < data.length; i++) {
                    if (linkReason != data[i].linkReasonName) {
                        if (linkReason == "") {
                            html += "<li>";
                        } else {
                            html += "</li><li>";
                        }
                        linkReason = data[i].linkReasonName;
                        html += data[i].linkReasonName + "<br>"
                             +  "<a target='_blank' href='" + data[i].productLinkUrl + "'>"
                             + data[i].productLinkTitle + "</a>"
                             + "<br>";
                    } else {
                      //Show link reason and link title
                        html    += "<a target='_blank' href='" + data[i].productLinkUrl + "'>"
                                    + data[i].productLinkTitle + "</a>"+ "<br>";
                    }
                }
                html += "</li>";
                $('.content11 ul').html(html);
            } else {
                //No link data
                $('#noLinkData').css("display", "block");
                $('.content11 ul').css("display", "none");
            }
        }
    })
    .complete(function() {
    	hideProcessBarInAjax();
    });
}
/* EOE Function Block11 */

/* BOE Function Block12 */
/**
* Load list video of product by ajax from DB
* @author   Nguyen.Chuong
* @date     2014/06/16
*/
function loadVideoData() {
    $.ajax({
    	url: 'loadVideoData.html',
    	type: 'POST',
    	dataType: 'json',
    	data: {
    		productId: productId},
        beforeSend: function() {
        	showProcessBarInAjax();
        }
    })
    .success(function(data) {
        //Reset html
        $('#block12Content').html("");
        //Check no data
        if (data.length > 0) {
            //Show video content, hide no data message
            $('#block12Content').css("display", "block");
            $('#noVideoData').css("display", "none");

            var formatVideo;
            $(data).each(function() {
                //Get default format of video block.
                formatVideo = $("#formatVideo").html();
                //Add video title to block.
                formatVideo = formatVideo.replace("videoTitle", this.productVideoTitle);
                //Add video link and replace watch in URL = embed to display in iFrame.
                formatVideo = formatVideo.replace("videoURL", this.productVideoUrl.replace("watch?v=", "embed/"));
                //Add video description.
                formatVideo = formatVideo.replace("videoDescription", this.productVideoDescription);
                //Append to block.
                $("#block12Content").append(formatVideo);
            });
        } else {
            //No video
            //Hide video content, show no data message
            $('#block12Content').css("display", "none");
            $('#noVideoData').css("display", "block");
        }
    })
    .fail(function() {
    	console.log("error");
    })
    .complete(function() {
    	hideProcessBarInAjax();
    });
}
/* EOE Function Block12 */

/* BOE Function Block13 */
/* EOE Function Block13 */

/* BOE Function Block14 */
/**
* Load list attribute of product by ajax from DB by productId and categoryCode
* @author   Nguyen.Chuong
* @date     2014/06/16
*/
function loadAttributeData(productIdParam, categoryParam) {  
    $.ajax({
        url: 'loadDataAttribute.html',
        type: 'POST',
        dataType: 'json',
        data: {
            productId: productIdParam
          , categoryCode: categoryParam},
        beforeSend: function() {
        	showProcessBarInAjax();
        }
    })
    .done(function(data) {
        var titleGridAttributeName = $("#titleGridAttributeName").val();
        var titleGridAttributeValue = $("#titleGridAttributeValue").val();
        var titleGridAttributeDisplay = $("#titleGridAttributeDisplay").val();
        $("#attributeTable").kendoGrid({
            dataSource : data,
            selectable: "false",
            resizable: false,
            scrollable: false,
            columns : [
                        {
                            title : titleGridAttributeName,
                            field : "attributeName",
                            width : "30px",
                            attributes:{ class:"text-left" }
                        },
                        {
                            title : titleGridAttributeValue,
                            template : function(e) {
                                //[Type is Flag] : tbl_factory_product_attribute_flag.attribute_flag_values : if value is 1, display "True" else display "False".
                                return changeAttributeValueToShow(e.attributeType, e.attributeValues);
                            },
                            width : "25px",
                            attributes:{ class:"text-left" }
                        },
                        {
                            title : titleGridAttributeDisplay,
                            field : "attributeDisplay",
                            width : "30px",
                            attributes:{ class:"text-left" }
                        }
            ],
            dataBound: function(data) {
//                //If data is null then show nodata message.
//                if ( $("#attributeTable").data('kendoGrid').dataSource != null
//                        && $("#attributeTable").data('kendoGrid').dataSource.total() != 0) {
//                    //Hide nodata message.
//                    $("#attributeTableNodata").css("display","none");
//                    //Show attribute table
//                    $("#attributeTable").css("display","block");
//                } else {
//                    //Show nodata message.
//                    $("#attributeTableNodata").css("display","block");
//                    //Hide attribute table.
//                    $("#attributeTable").css("display","none");
//                }
                //If data is null then show nodata message.
                var gridData = this.dataSource.view();
                //Number record of grid
                var length = gridData.length;
                if (length == 0) {
                    // Show no data message
                    $("#attributeTable").find("tbody").append(
                            '<td id="attributeTableNodata" colspan="3"><div class="dataTables_empty" >'
                                    + $("#noDataMessage").val() + '</div></td>');
                } else {
                    $("#attributeTableNodata").remove();
                }
                
            }
        });
    })
    .complete(function() {
    	hideProcessBarInAjax();
    });
}
/* EOE Function Block14 */

/* BOE Function Block15 */
/**
* View list display to block 15
* @author 	Luong.Dai
* @date 	2014/06/16
* @param 	displayData
* format params: [selectCode : [Select name : list select display]]
*/
function viewListDisplay(displayData) {
	$('#block15Content').html("");
	var html = "";
	//Loop in list select code
	for (selectCode in displayData) {
		//Get list [select name : list display]
		var displayInfo = displayData[selectCode];
		//Loop to get display infor
		for (selectFullName in displayInfo) {
			html += '<div class="top-rightInfo">';
			html += '<span>&bull;' + selectFullName + '</span>';
			/*var selectName = selectFullName.replace($('#pleaseChoiseOption').val(), "");*/
			//List display
			html += "<ul>";
			//List display
			var listDisplay = displayInfo[selectFullName];
			for (display in listDisplay) {
				html += '<li>' + /*selectName + ' : ' +*/ listDisplay[display].productSelectDisplay + '</li>';
			}
			//Close ul and div tag
			html += '</ul></div>';
		}
	}
	//Appent to content
	$('#block15Content').html(html);
	//Change css of last part in block 15 to no border bottom.
	$(".top-rightInfo").last().css("border-bottom","0px");
}
/* EOE Function Block15 */

/* BOE Function Popup */
/**
* Remove data of textArea, update user span, update date span of tab all and tab note in popup.
* @author   Nguyen.Chuong
* @date     2014/07/17
*/
function resetAllFieldPopup() {
    //Tab all
    $("#allCommentTta").val("");
    $("#allLastUpdateDateTxt").html("");
    $("#allLastUpdateUserTxt").html("");
    //Tab note
    $("#noteCommentTta").val("");
    $("#noteLastUpdateDateTxt").html("");
    $("#noteLastUpdateUserTxt").html("");
}
//old value to store origin value of all comment and note commnet.
// => when click cancel load old value. 
var oldAllCommentValue = "";
var oldNoteCommentValue = "";
//Load data for popup tab note and tab comment.
function loadDataPopup(data) {
    var entCalibration = data.entTblCalibrationRequestWhole;
    resetAllFieldPopup();
    if (entCalibration != null) {
        var tmpTime;
        //Tab all
        oldAllCommentValue = entCalibration.calibrationRequestWholeValue;
        $("#allCommentTta").val(oldAllCommentValue);

        tmpTime = entCalibration.calibrationRequestWholeValueDate;
        if (tmpTime != null) {
            //replace "T" charater and change "-" to "/" to create correct date for all browser.
            tmpTime = tmpTime.replace("T", " ").replace(/-/g, "/");
            $("#allLastUpdateDateTxt").html(getDateTimeWithFormat(tmpTime
                                                                , "{Y}/{M}/{D} {H}:{Mi}:{S}"));
        } else {
            $("#allLastUpdateDateTxt").html("");
        }
        $("#allLastUpdateUserTxt").html(entCalibration.calibrationRequestWholeValueUserName);

        //Tab note
        oldNoteCommentValue = entCalibration.calibrationRequestWholeMemo;
        $("#noteCommentTta").val(oldNoteCommentValue);
        tmpTime = entCalibration.calibrationRequestWholeMemoDate;
        if (tmpTime != null) {
            //replace "T" charater and change "-" to "/" to create correct date for all browser.
            tmpTime = tmpTime.replace("T", " ").replace(/-/g, "/");
            $("#noteLastUpdateDateTxt").html(getDateTimeWithFormat(tmpTime
                                                                 , "{Y}/{M}/{D} {H}:{Mi}:{S}"));
        } else {
            $("#noteLastUpdateDateTxt").html("");
        }
        $("#noteLastUpdateUserTxt").html(entCalibration.calibrationRequestWholeMemoUserName);
    }
}

function saveTabAllOrNotePopup(tabNo) {
    $.ajax({
        url: 'saveTabAllOrNotePopup.html',
        type: 'POST',
        dataType: 'json',
        data: {
              "productPreviewActionForm.entTblCalibrationRequestWhole.calibrationRequestWholeMatterNo"          : matterNo
            , "productPreviewActionForm.entTblCalibrationRequestWhole.calibrationRequestWholeValue"             : $("#allCommentTta").val()
            , "productPreviewActionForm.entTblCalibrationRequestWhole.calibrationRequestWholeMemo"              : $("#noteCommentTta").val()
            , "productPreviewActionForm.entTblCalibrationRequestWhole.calibrationRequestWholeValueUserId"       : $("#userLoginId").val()
            , "productPreviewActionForm.entTblCalibrationRequestWhole.calibrationRequestWholeMemoUserId"        : $("#userLoginId").val()
            , "productPreviewActionForm.tabNo"                                                                  : tabNo}
        , beforeSend: function() {
            showProcessBarInAjax();
        }
        , success: function(data) {
            var insertTime;
            //Tab all
            if (tabNo == 1) {
                insertTime = data.entTblCalibrationRequestWhole.calibrationRequestWholeValueDate;
                //replace "T" charater and change "-" to "/" to create correct date for all browser.
                insertTime = insertTime.replace("T", " ").replace(/-/g, "/");
                $("#allLastUpdateDateTxt").html(getDateTimeWithFormat(insertTime,"{Y}/{M}/{D} {H}:{Mi}:{S}"));
                $("#allLastUpdateUserTxt").html($("#lstUser").children("option[value='" + $("#userLoginId").val() + "']").html());
            } else {
                //Tab memo
                insertTime = data.entTblCalibrationRequestWhole.calibrationRequestWholeMemoDate;
                //replace "T" charater and change "-" to "/" to create correct date for all browser.
                insertTime = insertTime.replace("T", " ").replace(/-/g, "/");
                $("#noteLastUpdateDateTxt").html(getDateTimeWithFormat(insertTime,"{Y}/{M}/{D} {H}:{Mi}:{S}"));
                $("#noteLastUpdateUserTxt").html($("#lstUser").children("option[value='" + $("#userLoginId").val() + "']").html());
            }
        }
    })
    .done(function(data) {
        hideProcessBarInAjax();
    });
}

/**
 * Create entity to add to popup grid:<br>
 * finishFlg = false<br>
 * productId = current productId<br>
 * editURL = ./productEdit.html?productId=[productId in productId column]<br>
 * typeCode = param type code<br>
 * value = ""<br>
 * requestUserId = current login<br>
 * requestDate = current date.<br>
 * finishDate = ""<br>
 * @author Nguyen.Chuong
 * @param typeCode: type code of added row. If empty mean add row empty type
 */
function createRowInPopupGrid(typeCode) {
    //Get current login user name
    var loginName = $("#lstUser").children("option[value='" + $("#userLoginId").val() + "']").html();
    //Current time
    var currentTime = getDateTimeWithFormat("","{Y}/{M}/{D} {H}:{Mi}:{S}");
    //Get type name
    var typeName = $("#lstCalibrationType").children("option[value=" + typeCode + "]").html();
    return returnObj = {
            "calibrationRequestIndividualFinishFlg":            false,
            "calibrationRequestIndividualProductId":            productId,
            "calibrationRequestIndividualTypeCode":             typeCode,
            "calibrationRequestIndividualTypeName":             typeName,
            "calibrationRequestIndividualValue":                "",
            "calibrationRequestIndividualRequestUserId":        $("#userLoginId").val(),
            "calibrationRequestIndividualRequestUserName":      loginName,
            "calibrationRequestIndividualRequestDate":          currentTime,
            "calibrationRequestIndividualFinishDate":           ""
        };
}

function insertNewRowPopupIntoDB(tmpObj) {
    var resultObj = null;
    var finishFlg = tmpObj.calibrationRequestIndividualFinishFlg;
    if (finishFlg == true) {
        finishFlg = 1;
    } else {
        finishFlg = 0;
    }
    var productId = tmpObj.calibrationRequestIndividualProductId;
    var typeCode = tmpObj.calibrationRequestIndividualTypeCode;
    var typeName = '';
    if (typeCode != '') {
        typeName = $("#lstCalibrationType").children("option[value=" + typeCode + "]").html();
    }
    var userId = tmpObj.calibrationRequestIndividualRequestUserId;
    var userName = $("#lstUser").children("option[value='" + userId + "']").html();
    var requestDate = tmpObj.calibrationRequestIndividualRequestDate;
    $.ajax({
        url: 'insertNewRowPopupToDB.html',
        type: 'POST',
        dataType: 'json',
        async: false,
        data: {
            "productPreviewActionForm.entTblFactoryCalibrationRequestIndividual.calibrationRequestIndividualFinishFlg":              finishFlg
            , "productPreviewActionForm.entTblFactoryCalibrationRequestIndividual.calibrationRequestIndividualProductId":            productId
            , "productPreviewActionForm.entTblFactoryCalibrationRequestIndividual.calibrationRequestIndividualTypeCode":             typeCode
            , "productPreviewActionForm.entTblFactoryCalibrationRequestIndividual.calibrationRequestIndividualTypeName":             typeName
            , "productPreviewActionForm.entTblFactoryCalibrationRequestIndividual.calibrationRequestIndividualValue":                ""
            , "productPreviewActionForm.entTblFactoryCalibrationRequestIndividual.calibrationRequestIndividualRequestUserId":        userId
            , "productPreviewActionForm.entTblFactoryCalibrationRequestIndividual.calibrationRequestIndividualRequestUserName":      userName
            , "productPreviewActionForm.entTblFactoryCalibrationRequestIndividual.calibrationRequestIndividualRequestDateStr":       requestDate
        },
        beforeSend: function() {
            showProcessBarInAjax();
        },
        success: function(data) {
            resultObj = data.entTblFactoryCalibrationRequestIndividual;
            if(requestDate != '') {
                resultObj.calibrationRequestIndividualRequestDate = requestDate;
            }
        }
    })
    .done(function(data) {
        //initCheckPersonInPopUp(data);
    })
    .complete(function() {
    	//BOE @rcv!Nguyen.Chuong 2014/08/26 #11468 : load request list value and init all request type tooltip.
        loadDataForRequestTooltip(); //TODO load data and init icon.
        //EOE @rcv!Nguyen.Chuong 2014/08/26 #11468 : load request list value and init all request type tooltip.
        hideProcessBarInAjax();
    });
    return resultObj;
}

function clickIconInsert(typeCode) {
    callPopup();
    //BOE @rcv!dau.phuong set tab when click icon pencil
    $("#tabContentIndividual").trigger("click");
    //EOE @rcv!dau.phuong set tab when click icon pencil
    var tmpObj = createRowInPopupGrid(typeCode);
    //add to grid
    var grid = $("#grid").data("kendoGrid");
    tmpObj = insertNewRowPopupIntoDB(tmpObj);
    var dataSourcePopup = grid.dataSource;
    dataSourcePopup.insert(0,tmpObj);
    //Set time out to wait popup finish draw.
    setTimeout('autoFocusValueNewRow()', 0);
};
/* EOE Function Popup */

/* BOE Function Other */
/**
 * init Kendo Spliter for Layout.
 * @author Tran.Thanh
 * @param
 */
function initLayout() {
    //Set height content
//  var heightContent = $("#right-pane").height();
//  $("#vertical").css("height",$("#right-pane").height()+40);
    //Spliter for content and footer.
    $("#vertical").kendoSplitter({
        orientation : "vertical",
        panes : [
            {
                collapsible : false,
                resizable: false,
                size: 2250 + "px"
            },
            {
                collapsible: false,
                resizable: true, 
                size: "40px"
            }
        ]
    });
    //Spliter to splilt UI be 2 part: 
    //- Left block: Block 4 image, block 14 attribute
    //- Right block: All other.
    $("#horizontal").kendoSplitter({
        orientation : "horizontal",
        panes : [ {
            collapsible : false,
            resizable : false,
            size : "360px"
        }, {
            collapsible : false,
            resizable : false,
            size : "590px"
        } ]
    });
    //Spliter to splilt UI be 2 part: 
    //- Left block: block 5,6 general,7 option,8 guestInput,9 fitModel,
    //- Right block: 15 option.
    $("#blockTopRight").kendoSplitter({
        orientation : "horizontal",
        panes : [ {
            collapsible : false,
            resizable : false,
            size : "420px"
        }, {
            collapsible : false,
            resizable : false
        } ]
    });

    //Spliter to splilt UI be 2 part: 
    //- Top block: Block 4 image, 
    //- bottom block: Block 14 attribute
    var heightBlock14 = $("#block14").height();
    $("#left-pane-vertical").kendoSplitter({
        orientation : "vertical",
        panes : [ 
            { collapsible: false, resizable: false, size: "633px" },
            { collapsible: false, resizable: false, size: heightBlock14 + "px" }
        ]
    });

    //Right Pane
    //Spliter to splilt UI be 4 part from up to bottom: 
    //- Block 1: block 5,6 general,7 option,8 guestInput,9 fitModel, 15 option
    //- Block 2: block 10 description
    //- Block 3: block 11 link
    //- Block 4: block 12 video
    var heightTable = $("#block9 table").height();
    var heighTopLeft = heightTable + 490;
    var heightDescription = $("#block10").height();
    var heightLink = $("#block11").height();
//  var heightVideo = $("#block12Content").height();
    $("#right-pane-vertical").kendoSplitter({
        orientation : "vertical",
        panes : [ 
            { collapsible: false, resizable: true, size: heighTopLeft + "px" },
            { collapsible: false, resizable: true, size: heightDescription + "px" },
            { collapsible: false, resizable: true, size: heightLink + "px" },
            { collapsible: false, resizable: true, size: 840 + "px" }
       ]
    });

    //Spliter to splilt UI be 4 part from up to bottom: 
    //- Block 1: block 5
    //- Block 2: block 6 general
    //- Block 3: block 7 option
    //- Block 4: 8 guestInput
    //- Block 5: block 9 fitModel
    $("#top-left").kendoSplitter({
        orientation : "vertical",
        panes : [ 
            { collapsible: false, resizable: false, size: "100px" },
            { collapsible: false, resizable: false, size: "140px" },
            { collapsible: false, resizable: false, size: "120px" },
            { collapsible: false, resizable: false, size: "130px" },
            { collapsible: false, resizable: false, size: heightTable + "px" }
       ]
    });
    
}
/* EOE Function Other */
/********************************* EOE Function *********************************/

/****************************** BOE Document Ready ******************************/

function loadDataByProductId(productId) {
	//load Ajax block 2 : index of product in matter, index of product group
    loadDataBlock2(productId);
    // Load Ajax block 4 : load image of product
    loadDataBlock4(productId);
    // Load Ajax block 10 : Load description  of product
    loadDataBlock10(productId);
    //Change productId in URL.
    window.history.replaceState("", "", "./productPreview.html?productId=" + productId);
}

/**
* count product checked in matter
* @author 	Luong.Dai
* @date 	2014/07/30
* @param 	matterNo
*/
function countProductCheckedInMatter(matterNo) {
	$.ajax({
		url: 'countCheckedProductInMatter.html'
		, type: 'POST'
		, dataType: 'json'
		, data: {
			'productPreviewActionForm.matterNo': matterNo
		},
	})
	.done(function(data) {
		var entity = data.entStatusPopUpCheck;
		//Set value to view
		$('#numberCheckFirst').text(entity.firstCheckCount);
		$('#numberCheckSecond').text(entity.secondCheckCount);
		$('.total_product_matter').text(entity.allProductCount);
	})
	.fail(function() {
		//Go to error page
		window.location.href = "./error.html?error=param";
	});
	
}

$(document).ready(function(){
	DOT_SYSBOL = $('#guestInputSeparater').val();
	//Change combobox to kendo style
	$('#changeProductMode').width(130).kendoDropDownList({});
    /******************************* BOE Document Ready Tran.Thanh *******************************/
    //Get productId param
    productId = $("#hProductId").val();
    //Load data by productId
    loadDataByProductId(productId);
    //init layout
    initLayout();
    /******************************* EOE Document Ready Tran.Thanh *******************************/
    
    /******************************* BOE Document Ready Dai.Luong *******************************/
    /******************************* EOE Document Ready Dai.Luong *******************************/
    
    /******************************* BOE Document Ready Nguyen.Chuong *******************************/
    /**
     * Handle back button click.
     * @author Nguyen.Chuong
     */
    $("#btnBackTop, #btnBackBottom").click(function(){
        location.href = $("#matterDetailURL").val() + "?" + $("#paramMatterNo").val() + "=" + matterNo;
    });
    /**
     * Handle change value of changeMode combobox.
     * @author Nguyen.Chuong
     */
    $("#changeProductMode").change(function(){
        var changeProductMode = $("#changeProductMode").val();
        loadChangeProductIdBtn(productId,matterNo,changeProductMode);
    });
    //Hanle edit product botton
    $("#btnEditTop, #btnEditBottom").click(function(){
        location.href = $("#productEdtlURL").val() + "?" + $("#paramProductId").val() + "=" + productId;
    });

    /**
     * Hanle next product button click.
     * @author Nguyen.Chuong
     */
    $("#btnNextTop, #btnNextBottom").click(function(){
        //Change flag: next/prev function
        NEXT_PREV_FLG = 1;
        //Change current productId.
        productId = nextProductId;
        //Load new status of next/previous button and next/previous productId
        var changeProductMode = $("#changeProductMode").val();
        loadChangeProductIdBtn(productId,matterNo,changeProductMode);
        ////Load all data of next productId
        loadDataByProductId(productId);
        //BOE @rcv!dau.phuong : again loading popup request
        initRequestPopUp();
        loadCheckPopUp(productId);
        hideBackgroundPopUp();
        //EOE @rcv!dau.phuong : again loading popup request
    });

    /**
     * Hanle previous product button click.
     * @author Nguyen.Chuong
     */
    $("#btnPrevTop, #btnPrevBottom").click(function(){
        //Change flag: next/prev function
        NEXT_PREV_FLG = 1;
        //Change current productId.
        productId = previousProductId;
        //Load new status of next/previous button and next/previous productId
        var changeProductMode = $("#changeProductMode").val();
        loadChangeProductIdBtn(productId,matterNo,changeProductMode);
        //Load all data of previous productId
        loadDataByProductId(productId);
        //BOE @rcv!dau.phuong : again loading popup request
        initRequestPopUp();
        loadCheckPopUp(productId);
        hideBackgroundPopUp();
        //EOE @rcv!dau.phuong : again loading popup request
    });

    /**
     * Handler hotkey
     * @author Nguyen.Chuong
     * @param
     */
    $(document).keydown(function(e) {
        handleHotKeyPress(e);
    });
    
    /* BOE Function Popup */
    //Handler when cancel button in tab all and tab note.
    $("#allCancelBtn").click(function(){
        $("#allCommentTta").val(oldAllCommentValue);
        initRequestPopUp();
        hideBackgroundPopUp();
    });
    $("#noteCancelBtn").click(function(){
        $("#noteCommentTta").val(oldNoteCommentValue);
        initRequestPopUp();
    });
    //Handler when click save button in tab all.
    $("#allSaveBtn").click(function(){
        var lblOk = $("#confirmYes").val();
        var labelCancel = $("#confirmCancel").val();
        $.when(kendo.ui.ExtOkCancelDialog.show({ 
            title: "", 
            message: $("#saveTabAllMessHdd").val(),
            labelOK: lblOk,
            labelCancel: labelCancel,
            icon: "k-ext-warning" })
        ).done(function (response) {
            if (response.button == "OK") {
                //Save data to olde hidden value.
                oldAllCommentValue = $("#allCommentTta").val();
                //Ajax to save data to DB.
                saveTabAllOrNotePopup(1);
            }
        });
    });
    //Handler when click save button in tab note.
    $("#noteSaveBtn").click(function(){
        var lblOk = $("#confirmYes").val();
        var labelCancel = $("#confirmCancel").val();
        $.when(kendo.ui.ExtOkCancelDialog.show({ 
            title: "", 
            message: $("#saveTabNoteMessHdd").val(),
            labelOK: lblOk,
            labelCancel: labelCancel,
            icon: "k-ext-warning" })
        ).done(function (response) {
            if (response.button == "OK") {
                //Save data to olde hidden value.
                oldNoteCommentValue = $("#noteCommentTta").val();
                //Ajax to save data to DB.
                saveTabAllOrNotePopup(2);
            }
        });
    });


    //Handler when click new button in tab Individual.
    $("#newRowPopup").click(function(){ 
        var grid = $("#grid").data("kendoGrid");
        var dataSourcePopup = grid.dataSource;
        //Create entity to insert
        var tmpObj = createRowInPopupGrid("");
        tmpObj = insertNewRowPopupIntoDB(tmpObj);
        //add to grid
        dataSourcePopup.insert(0,tmpObj);
        //Set time out to wait popup finish draw.
        autoFocusValueNewRow();
    });

    /* EOE Function Popup */
    /* BOE rcv!Tuong.Luong function delete */
   // handleButtonDelete();
    initDeleteRequestDialog();
    initPleaseSelectDialog();

    /* EOE rcv!Tuong.Luong function delete */

    /******************************* EOE Document Ready Nguyen.Chuong *******************************/

/******************************* BOE Document Ready Dau.Phuong ****************************/
    //set row for textarea of tab 1,3 when load page in IE
    setRowForTextareaInTab13();
    //init request popup
    initRequestPopUp();
    loadCheckPopUp(productId);
    //case to close request popup
    //press ESC
    $(document).keyup(function (e) {
        var code = e.keyCode || e.which;
        if (code == 27) { //Enter keycode
            initRequestPopUp();
            hideBackgroundPopUp();
        }
    });
    //click outside popup
    $("#backgroundPopUp").click(function(){
        initRequestPopUp();
        hideBackgroundPopUp();
    });
    //init tabtrip in request popup
    $("#tabstrip").kendoTabStrip({
        animation:  {
            open: {
                effects: "fadeIn"
              , duration: 0
            },
            close: {
                duration: 0,
                effects: "fadeOut"
            },
        }
    });
    //init grid in request popup
    loadGridPopUp();
    $("#filterCorrespond").kendoDropDownList();
    $("#lstUser").kendoDropDownList();
    $("#lstCalibrationType").kendoDropDownList();
//    kendo.culture("ja-JP");
    $("#filterFromRequestDate").kendoDateTimePicker({
        interval: 60,
        format: "yyyy/MM/dd HH:mm:ss",
        timeFormat: "HH:mm:ss",
    });
    $("#filterToRequestDate").kendoDateTimePicker({
        interval: 60,
        format: "yyyy/MM/dd HH:mm:ss",
        timeFormat: "HH:mm:ss",
        width: 130
    });
    $("#filterFromFinishDate").kendoDateTimePicker({
        interval: 60,
        format: "yyyy/MM/dd HH:mm:ss",
        timeFormat: "HH:mm:ss",
    });
    $("#filterToFinishDate").kendoDateTimePicker({
    interval: 60,
    format: "yyyy/MM/dd HH:mm:ss",
    timeFormat: "HH:mm:ss",
    });
    $("#editRequestDate").kendoDateTimePicker({
        interval: 60,
        format: "yyyy/MM/dd HH:mm:ss",
        timeFormat: "HH:mm:ss",
    });
    $("#editFinishDate").kendoDateTimePicker({
        interval: 60,
        format: "yyyy/MM/dd HH:mm:ss",
        timeFormat: "HH:mm:ss",
    });
    // Handle datetime picker validation.
    handleDateTimePicker();
    //BOE function check or uncheck for check pop up
    $(document).on('click', '#checkOne', function (e) {
    	if ($("#checkOne").prop("checked") == false){
			updateStatusCheckBoxFlag = true;
		}
        if ($("#checkTwo").prop("checked") == true) {
             e.preventDefault();
        } else {
            updateStatusPopUpCheck('#checkOne', '#checkTwo', productId);
            //BOE Tuong.Luong: Check Auto next
            if ($("#checkOne").prop("checked") == true && jQuery("#btnNextTop").attr("disabled") != "disabled") {
                $("#btnNextTop").click();
            }
            //EOE Tuong.Luong: Check Auto next
        }
    });
    $(document).on('click', '#checkTwo', function (e) {
    	if ($("#checkTwo").prop("checked") == false){
			updateStatusCheckBoxFlag = true;
		}
        if ($("#checkOne").prop("checked") == false) {
            e.preventDefault();
        } else {
            updateStatusPopUpCheck('#checkOne', '#checkTwo', productId);
            //BOE Tuong.Luong: Check Auto next
            if (jQuery("#checkTwo").prop("checked") == true && jQuery("#btnNextTop").attr("disabled") != "disabled") {
                $("#btnNextTop").click();
            }
            //EOE Tuong.Luong: Check Auto next
        }
    });
    //EOE function check or uncheck for check pop up
    //BOE function for request pop up TAB 2
    //check all check box
    handleSelectAll("#checkBoxAll",  "#grid table tbody tr", ".matterCheckbox", selectedKendoClass, selectedRowClass);
//    gridSingleClick("#grid", "#selectAllCheckbox", ".matterCheckbox");
    //change status of request
    $(document).on('click', '.respond', function (e) {
    	changeCorrespondRequest($(this).val(), $(this).attr("id"));
    });
    // Change dropdownlist user, calibration type
	$("#lstUser, #lstCalibrationType, #filterCorrespond").change(function() {
		getSearchCondition();
		doFilterRequestGrid();
	});
	// Press enter key in textbox filter
	$(document).on("keypress", "input.tbSearch",
			function(e) {
				if (e.keyCode == 13) {
					/*BOE rcv!Tuong.Luong validate product filter*/
					var validateResult = validateSearchFilter();
					if (validateResult) {
						getSearchCondition();
						doFilterRequestGrid();
					}
					/*EOE rcv!Tuong.Luong validate product filter*/
			}
	});
    //Check role of popup tab all and note.
    checkRoleForPopup();
    //EOE function for request pop up TAB 2
	/*BOE rcv!Nguyen.Chuong #11468 2014/08/28 Handle click event of (!) tooltip request.*/
	//Handle click event of (!) tooltip request
	$(".requestToolTip").click(function(e) { //TODO
		//Call function to toogle tooltip.
		toogleRequestTooltip();
		//Call function javaScript stopPropagation() and return false for not call default html click.
		e.stopPropagation();
	    return false; 
	});
	//Event to always set status of show tooltip to hide.
	$("html").click(function(e) {
		tooltipStatus = "hide";
	});
	//Handle scroll then show tooltip request if current status is show.
	$(window).scroll(function(e) {
		//Set time out for wait until scroll complete.
		setTimeout(function(){
			var tooltipList = $(".requestToolTip:visible");
			var kendoTooltip;
			//Check if status tooltip is hide => show all tooltip.
			if (tooltipStatus == "show") {
				$.each(tooltipList, function(index, tooltip){
					kendoTooltip = $(tooltip).data("kendoTooltip");
					if (kendoTooltip != undefined) {
						kendoTooltip.show();
					}
				});
				return;
			}
		}, 250);
	});
	/*EOE rcv!Nguyen.Chuong #11468 2014/08/28 Handle click event of (!) tooltip request.*/
/******************************* EOE Document Ready Dau.Phuong ****************************/
});

/******************************* BOE Function By Dau.Phuong *******************************/
function initRequestPopUp() {
    if (jQuery.browser.msie || (!!navigator.userAgent.match(/Trident.*rv\:11\./))) { //detect IE (especial IE 11)
        $('#divCheck').css({'width': '116px'});
    } else {
        $('#divCheck').css({'width': '101px'});
    }
    $('#checkContent').css({'marginLeft': '97px'});
    $('#checkContent').css({'display': 'none'});
    $('.headerPopup').click(function() {
        showBackgroundPopUp();
        if (jQuery.browser.msie || (!!navigator.userAgent.match(/Trident.*rv\:11\./))) {
            $('#divCheck').css({'width': '921px'});
        } else {
            $('#divCheck').css({'width': '902px'});
        }
        $('#checkContent').css({'marginLeft': '0'});
        $('#checkContent').css({'display': 'block'});
    });
}
function createComboboxCorrespond(id) {
    var html = '<td><select id="' + id + '" class="cbbFilterCorrespond"">' + '<option value=" "></option>' + '<option value="1">' + $("#correspondTxt").val() + '</option>' + '<option value="0">' + $("#uncorrespondTxt").val() + '</option>' + '</select></td>';
    return html;
}
//Auto focus into value textBox of new added row.
function autoFocusValueNewRow() {
    var grid = $("#grid").data("kendoGrid");
    gridData = grid.dataSource.view();
    newRowObj = grid.table.find("tr:first-child");
    newCellObj = newRowObj.find("td[class='individualValue']");
    grid.editCell(newCellObj);
}

function loadGridPopUp() {
    //get data for grid calibration request
    datasourcePopUp = new kendo.data.DataSource({
        serverPaging: true,
        autoBind: false,
        // 50 product in 1 page
        pageSize: 50,
        serverSorting: true,
        transport: {
            read: {
                type: "POST",
                dataType: "json",
                url: "loadDataForGridPopUp.html",
                data: function (data) {
                    return setDataForDataSource();
                },
                beforeSend: function () {
                    // show process bar before send 
                	// request
                    //$("#selectAllCheckbox").prop("checked", false);
                    showProcessBar();
                },
                complete: function () {
                    // hide process bar that the request
                    // succeeded
                    hideProcessBar();
                },
                error: function () {
                    // hide process bar that the request
                    // failed
                    hideProcessBar();
                    // sortResetFlg = 0;
                },
                cache: false,
            }
        }
    });
    var modeEditGrid = {
            mode: "incell",
            confirmation: false
        };
    if (roleUser == 0) {
    	modeEditGrid = false;
    }
    
    $("#grid").kendoGrid({ 
        dataSource: datasourcePopUp,
        editable: modeEditGrid,
        selectable: "multiple row",
        width: 1190,
        resizable: true,
        height: 368,
        columns: [
            //1
            {
                title: "",
                /*BOE rcv!Tuong.Luong add event for checkbox on Grid */
                template: "<input type='checkbox' name='matterCheckbox' class='matterCheckbox'" +
                    " id='#= calibrationRequestIndividualProductId #'  value='#=calibrationRequestIndividualCode#'/>",
                /* EOE rcv!Tuong.Luong add event for checkbox on Grid */
                attributes: {
                    "class": "align-center"
                },
                width: "30px"
            },
            //2
            {
                template: function(data) {
                    return checkCorrespond(data.calibrationRequestIndividualFinishFlg, data.calibrationRequestIndividualCode);
                },
                field: "",
                type: "number",
                title: $("#tilteCorrespond").val(),
                width: "90px"
            },
            //3
            {
                template: function(data) {
                    return "P" + data.calibrationRequestIndividualProductId;
                },
                //                field: "calibrationRequestIndividualProductId",
                type: "number",
                title: $("#tilteProductId").val(),
                width: "120px",
                attributes: {
                    "class": "align-right"
                }
            },
            //4
            {
                template: function(data) {
                    //Link to productEdit: ./productEdit.html?productId=xxx
                    var productEditURL = $("#productEditURLHdd").val() + "?" + $("#productIdParamNameHdd").val() + "=" + data.calibrationRequestIndividualProductId;
                    var urlFormat = '<a class="linkToProductReview" href="{0}" target="_blank">' + $("#linkToEditHdd").val() + '</a>';
                    var aTag = $.format(urlFormat, productEditURL);
                    return aTag;
                },
                type: "string",
                title: $("#tilteOpenNewTab").val(),
                width: "70px",
                attributes: {
                    "class": "align-center"
                }
            },
            //5
            {
                field: "calibrationRequestIndividualTypeName",
                type: "number",
                title: $("#tilteRequestType").val(),
                width: "130px",
                editor: listCalibrationType,
                attributes: {
                    "class": "align-center individualTypeName"
                }
            },
            //6
            {
                template: function(data) {
                    var html = "<pre class='preCalibrationData'>" + data.calibrationRequestIndividualValue + "</pre>";
                    return html;
                },
                field: "calibrationRequestIndividualValue",
                type: "string",
                title: $("#tilteContent").val(),
                width: "200px",
                editor: editorContent,
                attributes: {
                    "class": "individualValue"
                }
            },
            //7
            {
                template: function(data) {
                    return data.calibrationRequestIndividualRequestUserName;
                },
                type: "string",
                title: $("#tilteRequestPerson").val(),
                width: "150px",
                attributes: {
                    "class": "align-center"
                }
            },
            //8
            {
                template: function(data) {
                    if (data.calibrationRequestIndividualRequestDate != "" && data.calibrationRequestIndividualRequestDate != null) {
                        //replace "T" charater and change "-" to "/" to create correct date for all browser.
                        var date = new Date(data.calibrationRequestIndividualRequestDate.replace("T", " ").replace(/-/g, "/"));
                        return "<span title='" + getDateTimeWithFormat(date, "{Y}/{M}/{D} {H}:{Mi}:{S}") + "'>" + getDateTimeWithFormat(date, "{Y}/{M}/{D}") + "</span>";
                    }
                    return "";
                },
                type: "date",
                title: $("#tilteRequestDate").val(),
                width: "200px",
                attributes: {
                    "class": "align-center"
                }
            },
            //9
            {
                template: function(data) {
                    if (data.calibrationRequestIndividualFinishDate != "" && data.calibrationRequestIndividualFinishDate != null) {
                        //replace "T" charater and change "-" to "/" to create correct date for all browser.
                        var date = new Date(data.calibrationRequestIndividualFinishDate.replace("T", " ").replace(/-/g, "/"));
                        return "<span title='" + getDateTimeWithFormat(date, "{Y}/{M}/{D} {H}:{Mi}:{S}") + "'>" + getDateTimeWithFormat(date, "{Y}/{M}/{D}") + "</span>";
                    }
                    return "";
                },
                type: "date",
                title: $("#tilteFinishDate").val(),
                width: "200px",
                attributes: {
                    "class": "align-center"
                }
            }
        ],
        edit: function(a) {
            var calibrationTypeCode = a.model.calibrationRequestIndividualTypeCode;
            if (calibrationTypeCode != null && calibrationTypeCode != "") {
                if ($(a.container).hasClass('individualTypeName')) {
                    $("#editCalibrationType").data("kendoDropDownList").value(a.model.calibrationRequestIndividualTypeCode);
                }
            }
            individualCode = a.model.calibrationRequestIndividualCode;
            $(".editorContentRequest").keypress(function(e) {
                if (e.keyCode == 13) {
                    if (e.shiftKey) {
                        return true;
                    }
                    if ($(this).val() != null && $(this).val() != "") {
                        updateContentRequest(individualCode, "", $(this).val());
                    }
                    return false;
                }
            });
            $(".editorContentRequest").addClass("tbSearch filter k-textbox");
            $(".editorContentRequest").change(function() {
                updateContentRequest(individualCode, "", $(this).val());
            });
        },
        dataBound: function(e) {
            var grid = $("#grid").data("kendoGrid");
            // Remove noData message
            $("#grid .dataTables_empty").remove();
            $("#checkBoxAll").attr("checked", false);
            gridSingleClick("#grid", "#checkBoxAll", ".matterCheckbox");
            // Remove noData message
            $("#grid .dataTables_empty").remove();
            $("#checkBoxAll").attr("checked", false);
            if (this.dataSource.total() > 0) {
                var currentUid = "";
                var currenRow = "";
                // Add title for row
                var gridData = this.dataSource.view();
                var gridLength = gridData.length;
                for (var i = 0; i < gridLength; i++) {
                    currentUid = gridData[i].uid;
                    currenRow = grid.table.find("tr[data-uid='" + currentUid + "']");
                    if (gridData[i].calibrationRequestIndividualFinishFlg == 1) {
                        // Row checked
                        $(currenRow).removeClass("k-alt");
                        $(currenRow).addClass("rowGridChecked");
                    }
                }
                // BOE @rcv!Nguyen.Chuong Jul 11, 2014 #9955 : add tooltip kendo for update date
                $("#grid .k-grid-content tr td:nth-child(8n), #grid .k-grid-content tr td:nth-child(8n + 1)").kendoTooltip({
                    filter: "span",
                    showAfter: 1000
                });
                // EOE @rcv!Nguyen.Chuong Jul 11, 2014 #9955 : add tooltip kendo for update date
            } else {
                // Show no data message
                $("#grid .k-grid-content").append(
                    '<div class="dataTables_empty" id="grid4EmptyElement">' + $("#noDataMessage")
                    .val() + '</div>');
            }
        }
    });
    // Add filter to create
    $("#grid").data("kendoGrid").thead.append(createFilterRow());
}

function listUser(container, options) {
	var tmp = $("#jsonUser").val();
	var json = $.parseJSON(tmp);
	var dtSource = new kendo.data.DataSource({ 
    	data: json
	});
	$('<input data-text-field="" data-value-field="userId" data-bind="value:' + options.field + '"/>')
    .appendTo(container)
    .kendoDropDownList({
        autoBind: false,
        dataSource: dtSource,
        change: function(a) {
        	alert(a);
        }
    });
}

function listCalibrationType(container, options) {
	var tmp = $("#jsonCalibrationType").val();
	var json = $.parseJSON(tmp);
	var dtSource = new kendo.data.DataSource({ 
    	data: json
	});
	$('<input id="editCalibrationType" data-text-field="calibrationRequestIndividualTypeName" data-value-field="calibrationRequestIndividualTypeCode" data-bind="value:' + options.field + '"/>')
    .appendTo(container)
    .kendoDropDownList({
        autoBind: false,
        dataSource: dtSource,
        dataTextField:"calibrationRequestIndividualTypeName",
        dataValueField:"calibrationRequestIndividualTypeCode",
        change: function(a) {
            var value = this.value();
            if (value != null && value != "") {
                updateContentRequest(individualCode, value, "");
            }
        }
    });
}

// Create filter row in grid
function createFilterRow() {
    var filterRow = $('<tr>' 
        + '<td></td>' 
        + createComboboxCorrespond("filterCorrespond") 
        + '<td><input class="tbSearch filter k-textbox" maxLength="19" 	type="text" id="filterProductId"/></td>' 
        + '<td></td>' 
        + '<td>' 
        + $("#hiddenListCalibrationType").val() 
        + '</td>' 
        + '<td><input class="tbSearch filter k-textbox" maxLength="19" 	type="text" id="filterContent"/></td>' 
        + '<td>' 
        + $("#hiddenListUser").val() 
        + '</td>' 
        + '<td><div>' 
        + '<span class="dateTimeLabel"> From </span>' 
        + '<input class="tbSearch k-input filterTextBox" type="search" id="filterFromRequestDate" style="width: 130px;"/>' 
        + '</span></br>' 
        + '<span class="dateTimeLabel"> To </span>' 
        + '<span>' 
        + '<input class="tbSearch k-input filterTextBox" type="search" id="filterToRequestDate" style="width: 130px;"/>' 
        + '</span>' + '</div></td>' 
        + '<td><div>' 
        + '<span class="dateTimeLabel"> From </span>' 
        + '<input class="tbSearch k-input filterTextBox" type="search" id="filterFromFinishDate" style="width: 130px;"/>' 
        + '</span></br>' 
        + '<span class="dateTimeLabel"> To </span>' 
        + '<span>' 
        + '<input class="tbSearch k-input filterTextBox" type="search" id="filterToFinishDate" style="width: 130px;"/>' 
        + '</span>' 
        + '</div></td>' 
        + '</tr>');
	return filterRow;
}
//Set data filter condition to sent to serverR
function setDataForDataSource() {
//	getSearchCondition();
    return {
    	//productId
    	"productId" : productId,
        // 2
        "productPreviewActionForm.entTblFactoryCalibrationRequestIndividual.calibrationRequestIndividualFinishFlg": calibrationRequestIndividualFinishFlg,
        // 3
        "productPreviewActionForm.entTblFactoryCalibrationRequestIndividual.calibrationRequestIndividualProductId": calibrationRequestIndividualProductId.trim(),
        // 5
        "productPreviewActionForm.entTblFactoryCalibrationRequestIndividual.calibrationRequestIndividualTypeCode": calibrationRequestIndividualTypeCode,
        // 6
        "productPreviewActionForm.entTblFactoryCalibrationRequestIndividual.calibrationRequestIndividualValue": calibrationRequestIndividualValue,
        // 7
        "productPreviewActionForm.entTblFactoryCalibrationRequestIndividual.calibrationRequestIndividualRequestUserId": calibrationRequestIndividualRequestUserId,
        // 8
        "productPreviewActionForm.entTblFactoryCalibrationRequestIndividual.calibrationRequestIndividualRequestDateFrom": calibrationRequestIndividualRequestDateFrom,
        "productPreviewActionForm.entTblFactoryCalibrationRequestIndividual.calibrationRequestIndividualRequestDateTo": calibrationRequestIndividualRequestDateTo,
        // 9
        "productPreviewActionForm.entTblFactoryCalibrationRequestIndividual.calibrationRequestIndividualFinishDateFrom": calibrationRequestIndividualFinishDateFrom,
        "productPreviewActionForm.entTblFactoryCalibrationRequestIndividual.calibrationRequestIndividualFinishDateTo": calibrationRequestIndividualFinishDateTo,
    	"productPreviewActionForm.entTblFactoryCalibrationRequestIndividual.sortField": "calibration_request_individual_code",
    	"productPreviewActionForm.entTblFactoryCalibrationRequestIndividual.sortDir": "DESC"
    };
}
function checkCorrespond(value, individualCode) {
    var result = "";
    if (value != null) {
        if (value == 1) {
            result = '<input type="button" id="' + individualCode + '" class="correspond respond" value="' + $("#correspondTxt").val() + '"/>';
        } else {
            result = '<input type="button" id="' + individualCode + '" class="unCorrespond respond" value="' + $("#uncorrespondTxt").val() + '"/>';
        }
    }
    return result;
}
function initCheckPersonInPopUp(entStatusPopUpCheck) {
//    var htmlCheckOne = '<label for="checkOne" class="lblCheckPerson"><input id="checkOne" type="checkbox" class="checkPerson">'+$("#titleCheckPerson1").val()+'</label>';
//    var htmlCheckTwo = '<label for="checkTwo" class="lblCheckPerson"><input id="checkTwo" type="checkbox" class="checkPerson">'+$("#titleCheckPerson2").val()+'</label>';
//    $("#viewCheckOne").html(htmlCheckOne);
//    $("#viewCheckTwo").html(htmlCheckTwo);
    if (entStatusPopUpCheck != null) {
        //set temporary variable for old status
        $("#oldStatus1").val(entStatusPopUpCheck.productCheck1Flg);
        $("#oldStatus2").val(entStatusPopUpCheck.productCheck2Flg);
        //Add tooltip for popup check
        if (entStatusPopUpCheck.productCheck1Flg == 1) {
            $("#checkOne").prop("checked", true);
            var html = "<div class='tooltipRespond'>" + $("#tooltipCheckPerson").val() + " : " + entStatusPopUpCheck.productCheck1UserLastName + entStatusPopUpCheck.productCheck1UserFirstName;
            //replace "T" charater and change "-" to "/" to create correct date for all browser.
            html += "</br>" + $("#tooltipCheckDate").val() + " : " + getDateTimeWithFormat(entStatusPopUpCheck.productCheck1Date.replace("T", " ").replace(/-/g, "/")
                                                                                         , "{Y}/{M}/{D} {H}:{Mi}:{S}") + "</div>";
            $(".checkOneTooltip").kendoTooltip({
                content: html,
                animation : {
                    close : {
                        effects : "fade:out"
                    },
                    open : {
                        effects : "fade:in",
                        duration : 300
                    }
                }
            });
            //Check check box one
            $("#checkOne").attr("checked", true);
        } else {
        	$(".checkOneTooltip").kendoTooltip({});
            var tootltip = $(".checkOneTooltip").data("kendoTooltip");
            tootltip.destroy();
            //Uncheck check box one
            $("#checkOne").attr("checked", false);
        }
        if (entStatusPopUpCheck.productCheck2Flg == 1) {
            $("#checkTwo").prop("checked", true);
            var html = "<div class='tooltipRespond'>" + $("#tooltipCheckPerson").val() + " : " + entStatusPopUpCheck.productCheck2UserLastName + entStatusPopUpCheck.productCheck2UserFirstName;
            //replace "T" charater and change "-" to "/" to create correct date for all browser.
            html += "</br>" + $("#tooltipCheckDate").val() + " : " + getDateTimeWithFormat(entStatusPopUpCheck.productCheck2Date.replace("T", " ").replace(/-/g, "/")
                                                                                         , "{Y}/{M}/{D} {H}:{Mi}:{S}") + "</div>";
            $(".checkTwoTooltip").kendoTooltip({
                content: html,
                animation : {
                    close : {
                        effects : "fade:out"
                    },
                    open : {
                        effects : "fade:in",
                        duration : 300
                    }
                }
            });
            //Check check box one
            $("#checkTwo").attr("checked", true);
        } else {
        	$(".checkTwoTooltip").kendoTooltip({});
            var tootltip = $(".checkTwoTooltip").data("kendoTooltip");
            tootltip.destroy();
            //Uncheck check box two
            $("#checkTwo").attr("checked", false);
        }
    }
}
function loadCheckPopUp(productId){
	$.ajax({
        url: 'loadCheckPopUp.html',
        type: 'POST',
        dataType: 'json',
        data: {
            "productId": productId
        },
        beforeSend: function() {
        	showProcessBarInAjax();
        }
    })
    .done(function(data) {
        initCheckPersonInPopUp(data);
    })
    .complete(function() {
    	hideProcessBarInAjax();
    });
}
function updateStatusPopUpCheck(value1, value2, productId) {
    var status1 = 0;
    var status2 = 0;
    var loginId = $("#userLoginId").val();
    if ($(value1).prop("checked") == true) {
        status1 = 1;
    }
    if ($(value2).prop("checked") == true) {
        status2 = 1;
    }
    var oldStatus1 = $("#oldStatus1").val();
    var oldStatus2 = $("#oldStatus2").val();
    $.ajax({
        url: 'updateStatusPopUpCheck.html',
        type: 'POST',
        dataType: 'json',
        data: {
            "productId": productId
            , "status1": status1
            , "status2": status2
            , "loginId": loginId
            , "oldStatus1": oldStatus1
            , "oldStatus2": oldStatus2
        },
        beforeSend: function() {
        	showProcessBarInAjax();
        }
    })
    .done(function(data) {
    	//BOE Tuong.Luong avoid call load update productid 
    	if(updateStatusCheckBoxFlag){
    		loadCheckPopUp(data);
    	}
    	//EOE Tuong.Luong avoid call load update productid
    	//count checked product
        countProductCheckedInMatter(matterNo);
    })
    .complete(function() {
    	updateStatusCheckBoxFlag = false;
    	hideProcessBarInAjax();
    });
}
function validateStatusPopUpCheck(value1, value2) {
    var check = false;
    if (($(value1).prop("checked") == false && $(value2).prop("checked") == false) 
       || ($(value1).prop("checked") == true && $(value2).prop("checked") == true) 
       || ($(value1).prop("checked") == true && $(value2).prop("checked") == false)) {
        check = true;
    }
    return check;
}
//function change corresponding request
function changeCorrespondRequest(value, individualCode) {
    var loginId = $("#userLoginId").val();
    var statusRequest;
    if (value != null) {
        if (value == $("#correspondTxt").val()) {
            statusRequest = 0; //change to uncorresponding
        } else {
            statusRequest = 1; //change to corresponding
        }
        $.ajax({
                url: 'changeCorrespondRequest.html',
                type: 'POST',
                dataType: 'json',
                data: {
                    "individualCode": individualCode,
                    "statusRequest": statusRequest,
                    "loginId": loginId
                },
                beforeSend: function () {
                    showProcessBarInAjax();
                }
                })
            .done(function () {
            	//BOE @rcv!Nguyen.Chuong 2014/07/29 #9955 : change button, backround of row and remove if grid is filter.
                //doFilterRequestGrid();
            	var row = $("#" + individualCode).closest("tr");
            	if($("#filterCorrespond").find(":selected").text() != "") {
            	    var grid = $("#grid").data("kendoGrid");
            	    grid.removeRow(row);
            	    return;
            	}
            	if (value != null) {
            		//Change row and button to correspond
                    if (value == $("#correspondTxt").val()) {
                    	$(row).removeClass("rowGridChecked");
                    	$("#" + individualCode).removeClass("correspond");
                    	$("#" + individualCode).addClass("unCorrespond");
                    	$("#" + individualCode).val($("#uncorrespondTxt").val());
                    	return;
                    }
                	//Change row and button to unCorrespond
                	$(row).addClass("rowGridChecked");
                	$("#" + individualCode).removeClass("unCorrespond");
                	$("#" + individualCode).addClass("correspond");
                	$("#" + individualCode).val($("#correspondTxt").val());
                }
                //EOE @rcv!Nguyen.Chuong 2014/07/29 #9955 : change button, backround of row and remove if grid is filter.
            })
            .complete(function () {
                hideProcessBarInAjax();
            });
    }
}
function doFilterRequestGrid() {
    $("#grid").data('kendoGrid').dataSource.page(0);
    $("#grid").data('kendoGrid').content.scrollTop(0);
}

function getSearchCondition() {
    // 2
    calibrationRequestIndividualFinishFlg = $("#filterCorrespond").val();
    // 3
    calibrationRequestIndividualProductId = $("#filterProductId").val().replace(/[^0-9]/g, '');
    // 5
    calibrationRequestIndividualTypeCode = $("#lstCalibrationType").val();
    // 6
    calibrationRequestIndividualValue = $("#filterContent").val();
    // 7
    calibrationRequestIndividualRequestUserId = $("#lstUser").val();
    // 8
    calibrationRequestIndividualRequestDateFrom = $("#filterFromRequestDate").val();
    calibrationRequestIndividualRequestDateTo = $("#filterToRequestDate").val();
    // 9
    calibrationRequestIndividualFinishDateFrom = $("#filterFromFinishDate").val();
    calibrationRequestIndividualFinishDateTo = $("#filterToFinishDate").val();
    //trim value
    // 2
    if (calibrationRequestIndividualFinishFlg) {
        calibrationRequestIndividualFinishFlg = calibrationRequestIndividualFinishFlg.trim();
    }
    // 3
    if (calibrationRequestIndividualProductId) {
        calibrationRequestIndividualProductId = calibrationRequestIndividualProductId.trim();
    }
    // 5
    if (calibrationRequestIndividualTypeCode) {
        calibrationRequestIndividualTypeCode = calibrationRequestIndividualTypeCode.trim();
    }
    // 6
    if (calibrationRequestIndividualValue) {
        calibrationRequestIndividualValue = calibrationRequestIndividualValue.trim();
    }
    // 7
    if (calibrationRequestIndividualRequestUserId) {
        calibrationRequestIndividualRequestUserId = calibrationRequestIndividualRequestUserId.trim();
    }
    // 8
    if (calibrationRequestIndividualRequestDateFrom) {
        calibrationRequestIndividualRequestDateFrom = calibrationRequestIndividualRequestDateFrom.trim();
    }
    if (calibrationRequestIndividualRequestDateTo) {
        calibrationRequestIndividualRequestDateTo = calibrationRequestIndividualRequestDateTo.trim();
    }
    // 9
    if (calibrationRequestIndividualFinishDateFrom) {
        calibrationRequestIndividualFinishDateFrom = calibrationRequestIndividualFinishDateFrom.trim();
    }
    if (calibrationRequestIndividualFinishDateTo) {
        calibrationRequestIndividualFinishDateTo = calibrationRequestIndividualFinishDateTo.trim();
    }
}
function updateContentRequest(individualCode, editType, editValue) {
    var loginId = $("#userLoginId").val();
    $.ajax({
        url: 'updateContentRequest.html',
        type: 'POST',
        dataType: 'json',
        data: {
            "individualCode": individualCode,
            "loginId": loginId,
            "editType" : editType,
            "editValue" : editValue
        },
        beforeSend: function () {
            showProcessBarInAjax();
        }
        })
    .done(function () {
    	doFilterRequestGrid();
    	//Load data of request tooltip after change row data.
    	loadDataForRequestTooltip();
    })
    .complete(function () {
        hideProcessBarInAjax();
    });
}
function handleDateTimePicker() {
    // Handle focusout event.
    $("#filterFromRequestDate").focusout(function() {
        validateDateTimePicker('#filterFromRequestDate', '#filterFromRequestDate', '#filterToRequestDate');
    });
    $("#filterFromRequestDate").focusout(function() {
        validateDateTimePicker('#filterFromRequestDate', '#filterFromRequestDate', '#filterFromRequestDate');
    });
    $("#filterFromFinishDate").focusout(function() {
        validateDateTimePicker('#filterFromFinishDate', '#filterFromFinishDate', '#filterFromFinishDate');
    });
    $("#filterFromFinishDate").focusout(function() {
        validateDateTimePicker('#filterFromFinishDate', '#filterFromFinishDate', '#filterToFinishDate');
    });
}
function showBackgroundPopUp() {
    $('#backgroundPopUp').fadeIn('fast');
}
function hideBackgroundPopUp() {
    $('#backgroundPopUp').fadeOut('fast');
}
var editorContent = function (container, options) {
    $('<textarea data-bind="value: ' + options.field + '" class="editorContentRequest" rows="2"></textarea>').appendTo(container);
};
function setRowForTextareaInTab13() {
    if ((/msie/.test(navigator.userAgent.toLowerCase())) || (/mozilla/.test(navigator.userAgent.toLowerCase()) && !/webkit/.test(navigator.userAgent.toLowerCase()))) {
        $("#noteCommentTta").attr("cols", "140");
        $("#allCommentTta").attr("cols", "140");
    }
}
/******************************* EOE Function By Dau.Phuong *******************************/
/******************************* BOE rcv!Tuong.Luong Function *******************************/
//Turn on flag thumbnail size invalid
function checkValidateThumbnail(data){
	if(data != ""){
		var jsonData = $.parseJSON(data);
		var jsonDataLength = jsonData.length;
	    for (var i = 0; i < jsonDataLength; i++) {
	    	if(jsonData[i].isValidThumbnailSize == false){
	    		//invalid data, turn on red color
	    		currentFlagThumbnailValid = false;
	    		break;
	    	}
	    }
	}
}
//Check Exists image
function checkValidateImgExists(data){
    storeImageObject = new Array();
	var imageObject;
	var jsonData = $.parseJSON(data);
	var jsonDataLength = jsonData.length;
    for (var i = 0; i < jsonDataLength; i++) {
    	if(jsonData[i].isExistsImage == false){
    		//flag turn on red flag
    		currentFlagImgExists = false;	
    	}
    	imageObject = new Object();
    	//create new image Object with 4 properties
    	imageObject.productImage = jsonData[i].productImage;
		imageObject.isExistsImage = jsonData[i].isExistsImage;	
		imageObject.productThumb = jsonData[i].productThumb;
		imageObject.isValidThumbnailSize = jsonData[i].isValidThumbnailSize;	
    	storeImageObject.push(imageObject);
    }
    //Check first path image,if invalid turn on 
    if(storeImageObject[0].isExistsImage == false){
    	$("#viewDetailPath").addClass("blockError");
    }
    //Check invalid thumbnail size, if invalid turn on 
    if(storeImageObject[0].isValidThumbnailSize == false){
    	$("#dimensionThumbnailImage").addClass("blockError");
    }
}
function callPopup(){
	$(".headerPopup").click();
}
function resetImageBlockFlag(){
	 currentFlagImgExists = true;
	 currentFlagThumbnailValid = true;
}
//Validate search filter
function validateSearchFilter() {
	var productIdResult = validateNumbericAndCharacter($("#filterProductId").val());
	if (!productIdResult){
		return false;
	}
	return true;
}

function deleteProductAjax(idparams) {
	$.ajax({
        url: 'deleteValuePopUpCheck.html',
        type: 'POST',
        dataType: 'json',
        data: {
            "idCalibrationArray": idparams
        },
        beforeSend: function() {
        	showProcessBarInAjax();
        }
    })
    .done(function(data) {
    	//clear  without creating a new array by setting its length to zero
    	idArrayCheckBox.length = 0;
    	//BOE @rcv!Nguyen.Chuong 2014/08/26 #11468 : load request list value and init all request type tooltip.
        loadDataForRequestTooltip(); //TODO load data and init icon.
        //EOE @rcv!Nguyen.Chuong 2014/08/26 #11468 : load request list value and init all request type tooltip.
    })
    .complete(function() {
    	doFilterRequestGrid();
    	hideProcessBarInAjax();
    });
}

function initDeleteRequestDialog() {
	$("#deleteRequestDialog").kendoWindow({
		minWidth : 400,
		minWeight : 300,
		title : $("#windowNamedeleteRequestDialog").val(),
		resizable : false,
		modal : true,
		visible : false,
		draggable : false,
		open : function() {
			$("#matterName").text("");
			$("#lstAdmin").val($("#lstAdmin option:first").val());
		}
	});

	// Get delete dialog.Center its title.
	var deleteRequestDialog = $("#deleteRequestDialog").data(
			"kendoWindow");

	// Add handlers.
	$("#btnDelete").click(function() {
		var checkedVals = $('.matterCheckbox:checkbox:checked').map(function() {
		    return this.value;
		}).get();
		checkedVals.join(",");
		if (checkedVals.length  > 0) {
			deleteRequestDialog.center().open();
		} else {
			// Show PleaseSelect dialog.
			var pleaseSelectDialog = $("#pleaseSelectDialog").data("kendoWindow");
			pleaseSelectDialog.center().open();
		}
	});
	$("#btnDeleteRequestCancel").click(function(e) {
		deleteRequestDialog.close();
	});
	$("#btnDeleteRequestOk").click(function() {
		// Reset error message panel first.
		resetErrorMssagePanels();
		var checkedVals = $('.matterCheckbox:checkbox:checked').map(function() {
		    return this.value;
		}).get();
		checkedVals.join(",");
		// Call API to delete selected row.
		deleteProductAjax(String(checkedVals));
		// Close dialog.
		deleteRequestDialog.close();
	});
}

function initPleaseSelectDialog() {
	// Init pleaseSelect dialog.
	$("#pleaseSelectDialog").kendoWindow({
		minWidth : 300,
		minWeight : 200,
		title : $("#windowNamepleaseSelectDialog").val(),
		resizable : false,
		modal : true,
		visible : false,
		draggable : false
	});
	// Get pleaseSelect dialog.
	var pleaseSelectDialog = $("#pleaseSelectDialog").data("kendoWindow");
	$("#pleaseSelectDialogOk").click(function(e) {
		pleaseSelectDialog.close();
	});
}
//message bar
function resetErrorMssagePanels() {
	// remove all class of msg div and clear msg
	$("#message").removeClass("has-message success");
	$("#message").removeClass("has-message msg-error");
	$("#message").text("");
	$("#message").hide();
}
/**EOE rcv!Tuong.Luong Function****/

/*BOE rcv!Nguyen.Chuong add icon show tooltip detail of request popup.*/
//List of each request type.
var listBrandRequest = [];
var listCategoryRequest = [];
var listProductNameRequest = [];
var listProductCodeRequest = [];
var listJANRequest = [];
var listAllTypeFlagsRequest = [];
var listProperPriceRequest = [];
var listSupplierPricePriceRequest = [];
var listGroupRequest = [];
var listShiireRequest = [];
var listImageRequest = [];
var listSummaryRequest = [];
var listRemarkRequest = [];
var listSentenceRequest = [];
var listCautionRequest = [];
var listCheckFitModelRequest = [];
var listGestInputRequest = [];
var listVideoRequest = [];
var listLinkRequest = [];
var listAttributeRequest = [];
//Current satus(hide/show) of tooltip request.
var tooltipStatus = "hide";
/**
 * rcv!Nguyen.Chuong #11468 2014/08/26<br>
 * toogle to hide or show all tooltip in UI follow visible (!) icon.
 */
function toogleRequestTooltip() {
	var tooltipList = $(".requestToolTip:visible");
	var kendoTooltip;
	//Check if status tooltip is hide => show all tooltip.
	if (tooltipStatus == "hide") {
		tooltipStatus = "show";
		$.each(tooltipList, function(index, tooltip){
			kendoTooltip = $(tooltip).data("kendoTooltip");
			if (kendoTooltip != undefined) {
				kendoTooltip.show();
			}
		});
		return;
	}
	//Check if status tooltip is show => hide all tooltip.
	tooltipStatus = "hide";
	$.each(tooltipList, function(index, tooltip){
		kendoTooltip = $(tooltip).data("kendoTooltip");
		kendoTooltip.hide();
	});
}

/**
 * rcv!Nguyen.Chuong #11468 2014/08/26<br>
 * function to do 2 process:<br>
 * - Load data list request of all type for current product(call function load data of grid in request popup with filter productID)<br>
 * - Call function loadRequestTooltipIcon() to get value and init all type request tooltip.<br>
 */
function loadDataForRequestTooltip() {  //TODO load all data tooltip
	//Reset all tooltip value
	resetAllRequestTooltip();
	$.ajax({
        url: 'loadDataForGridPopUp.html',
        type: 'POST',
        dataType: 'json',
        data: {
        	"productId" : productId,
        	//Sent productId param to get only individual request of current product.
        	"productPreviewActionForm.entTblFactoryCalibrationRequestIndividual.calibrationRequestIndividualProductId": productId,
        	//Sent finish flg = 0 to get only unfinish request.
        	"productPreviewActionForm.entTblFactoryCalibrationRequestIndividual.calibrationRequestIndividualFinishFlg": 0,
        	"productPreviewActionForm.entTblFactoryCalibrationRequestIndividual.sortField": "calibration_request_individual_code",
        	"productPreviewActionForm.entTblFactoryCalibrationRequestIndividual.sortDir": "ASC"
        }
    })
    .complete(function(data) {
        listCalibrationRequest = $.parseJSON(data.responseText);
        loadRequestTooltipIcon();
    });
}

/**
 * rcv!Nguyen.Chuong #11468 2014/08/26<br>
 * function to do 2 process:<br>
 * - Process to get value of request:<br>
 *   + Show 3 first request(event it empty).<br>
 *   + Each request value only show the first 40 character(if more than 40 replace with "...").<br>
 *   + If request has more than 1 row, only show first row.<br>
 * - Create tooltip of each request has request value.<br>
 * @param type number of request type
 * @param listRequest list<string> content value of this request type.
 */
function initTooltipRequest(type, listRequest) {
	//Final value of tooltip
    var tooltipValue = "";
    //Loop list request to get top 3 value not empty.
    for (var i = 0; i < listRequest.length; i++) {
    	//If index still less than 3 then continue to get value.
        if (i < 3) {
//            tooltipValue += $("#calibrationRequestTxt").val() + (i + 1) + ":<br>";
            var subStringFlg = false;
            var tmpString = listRequest[i];
            //Check value item is content enter character.
            if (tmpString.indexOf("\n") >= 0) {
            	//SubString to get first line
                tmpString = tmpString.substring(0, tmpString.indexOf("\n"));
                //Change subStringFlg = true to add "..."
                subStringFlg = true;
            }
            //Check the first line or value item has length more then 40
            if (tmpString.length > 40) {
            	//If more then 40 => subString and show value follow format: "abc..."
                tooltipValue += tmpString.substring(0, 40);
                //Change subStringFlg = true to add "..."
                subStringFlg = true;
            } else {
                tooltipValue += tmpString;
            }
            //Add "..." if current request value content substring of orginal value.
            if(subStringFlg) {
            	tooltipValue += "...";
            }
        	//Add new line after each value.
            tooltipValue += "<br>";
        }
    }
    //Remove last "<br>" character.
    tooltipValue = tooltipValue.substring(0, tooltipValue.length - 4);
    //Create tooltip kendo for each request type.
    if (listRequest.length > 0) {
    	//Show (!) icon of this type if value is not empty.
        $("#requestToolTip_" + type).css("display", "inline");
        //Create kendo tooltip.
        $("#requestToolTip_" + type).kendoTooltip({
            autoHide: false,
            showOn: "none",
            content: tooltipValue,
            position: "top",
        });
    } else {
    	//Hide (!) icon of this type if value is empty.
        $("#requestToolTip_" + type).css("display", "none");
    }
}

/**
 * rcv!Nguyen.Chuong #11468 2014/08/26<br>
 * Reset all value of tooltip request and detroy all tooltip kendo.<br>
 */
function resetAllRequestTooltip() {
	//Set list calibration request to null for new data load.
	listCalibrationRequest = "";
	//Reset each request value list.
	listBrandRequest 				= [];
	listCategoryRequest 			= [];
	listProductNameRequest 			= [];
	listProductCodeRequest 			= [];
	listJANRequest 					= [];
	listAllTypeFlagsRequest 		= [];
	listProperPriceRequest 			= [];
	listSupplierPricePriceRequest 	= [];
	listGroupRequest 				= [];
	listShiireRequest 				= [];
	listImageRequest 				= [];
	listSummaryRequest 				= [];
	listRemarkRequest 				= [];
	listSentenceRequest 			= [];
	listCautionRequest 				= [];
	listCheckFitModelRequest 		= [];
	listGestInputRequest 			= [];
	listVideoRequest 				= [];
	listLinkRequest 				= [];
	listAttributeRequest 			= [];
	//Get all tooltip to detroy.
	var tooltipList = $(".requestToolTip:visible");
	var kendoTooltip;
	//Loop to detroy all kendo tooltip.
	$.each(tooltipList, function(index, tooltip){
		kendoTooltip = $(tooltip).data("kendoTooltip");
		if (kendoTooltip != undefined) {
			kendoTooltip.destroy();
		}
	});
}
/**
 * rcv!Nguyen.Chuong #11468 2014/08/26<br>
 * function to do 2 process:<br>
 * - Reset all value of tooltip request, loop list request of product to get list value of each request type.<br>
 * - Call function initTooltipRequest() to create data and kendo tooltip object of each request type.<br>
 */
function loadRequestTooltipIcon() {
	//Loop list request value to get each request type list value
	$.each(listCalibrationRequest, function(index, requestItem){
		switch(requestItem.calibrationRequestIndividualTypeCode) {
		    case 1: //Brand
		    	listBrandRequest.push(requestItem.calibrationRequestIndividualValue);
		        break;
		    case 2: //Category
		    	listCategoryRequest.push(requestItem.calibrationRequestIndividualValue);
		        break;
		    case 3: //ProductName
		    	listProductNameRequest.push(requestItem.calibrationRequestIndividualValue);
		        break;
		    case 4: //productCodeNoFlag
		    	listProductCodeRequest.push(requestItem.calibrationRequestIndividualValue);
		        break;
		    case 5: //JAN
		    	listJANRequest.push(requestItem.calibrationRequestIndividualValue);
		        break;
		    case 6: //allTypeFlags
		    	listAllTypeFlagsRequest.push(requestItem.calibrationRequestIndividualValue);
		        break;
		    case 7: //Proper price 
		    	listProperPriceRequest.push(requestItem.calibrationRequestIndividualValue);
		        break;
		    case 8: //Supplier price price 
		    	listSupplierPricePriceRequest.push(requestItem.calibrationRequestIndividualValue);
		        break;
		    case 9: //Group 
		    	listGroupRequest.push(requestItem.calibrationRequestIndividualValue);
		        break;
		    case 10: //Shiire
		    	listShiireRequest.push(requestItem.calibrationRequestIndividualValue);
		        break;
		    case 11: //Image 
		    	listImageRequest.push(requestItem.calibrationRequestIndividualValue);
		        break;
		    case 12: //Summary
		    	listSummaryRequest.push(requestItem.calibrationRequestIndividualValue);
		        break;
		    case 13: //Remark
		    	listRemarkRequest.push(requestItem.calibrationRequestIndividualValue);
		        break;
		    case 14: //Sentence
		    	listSentenceRequest.push(requestItem.calibrationRequestIndividualValue);
		        break;
		    case 15: //Caution
		    	listCautionRequest.push(requestItem.calibrationRequestIndividualValue);
		        break;
		    case 16: //Check fit model
		    	listCheckFitModelRequest.push(requestItem.calibrationRequestIndividualValue);
		        break;
		    case 17: //GestInput
		    	listGestInputRequest.push(requestItem.calibrationRequestIndividualValue);
		        break;
		    case 18: //Video
		    	listVideoRequest.push(requestItem.calibrationRequestIndividualValue);
		        break;
		    case 19: //Link 
		    	listLinkRequest.push(requestItem.calibrationRequestIndividualValue);
		        break;
		    case 20: //Attribute
		    	listAttributeRequest.push(requestItem.calibrationRequestIndividualValue);
		        break;
		    default:
		        break;
		}
	});
	//Call funtion to init tooltip request.
	initTooltipRequest(1, listBrandRequest);
	initTooltipRequest(2, listCategoryRequest);
	initTooltipRequest(3, listProductNameRequest);
	initTooltipRequest(4, listProductCodeRequest);
	initTooltipRequest(5, listJANRequest);
	initTooltipRequest(6, listAllTypeFlagsRequest);
	initTooltipRequest(7, listProperPriceRequest);
	initTooltipRequest(8, listSupplierPricePriceRequest);
	initTooltipRequest(9, listGroupRequest);
	initTooltipRequest(10, listShiireRequest);
	initTooltipRequest(11, listImageRequest);
	initTooltipRequest(12, listSummaryRequest);
	initTooltipRequest(13, listRemarkRequest);
	initTooltipRequest(14, listSentenceRequest);
	initTooltipRequest(15, listCautionRequest);
	initTooltipRequest(16, listCheckFitModelRequest);
	initTooltipRequest(17, listGestInputRequest);
	initTooltipRequest(18, listVideoRequest);
	initTooltipRequest(19, listLinkRequest);
	initTooltipRequest(20, listAttributeRequest);
}

/*EOE rcv!Nguyen.Chuong add icon show tooltip detail of request popup.*/
