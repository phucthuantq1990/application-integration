jQuery(document).ready(function(){
    jQuery.validator.addMethod("lettersonly", function(value, element) {
        return this.optional(element) || /^[a-zA-Z0-9]+$/i.test(value.trim());
    }, IdOrPassInputInvalid);
    
    jQuery.validator.addMethod("alphaEmail", function(value, element) {
        return this.optional(element) || /^(([A-Za-z0-9]+_)|([A-Za-z0-9]+\-)|([A-Za-z0-9]+\.))*[A-Za-z0-9]+@(([A-Za-z0-9]+\-)|([A-Za-z0-9]+\.)|([A-Za-z0-9]+_))*[A-Za-z0-9]{1,}\.[a-zA-Z]{2,}$/i.test(value);
    }, jQuery.format(inputInvalid));
    
    jQuery.validator.addMethod("maxLength",
          jQuery.validator.methods.maxlength,
          jQuery.format(inputOutOfRange));

    jQuery.validator.addMethod("checkExistedUserId", function(value, element) {
        return !checkExistedUserId(value.trim());
    }, existedUser);
    
    function checkExistedUserId(userId) {
        var checkResult = true;
        jQuery.ajax({
            type: "POST",
            url: "checkExistedUserId.html",
            data: {
                userId: userId
            },
            async: false,
            dataType: "json",
            success: function(dataResult) {
                checkResult = eval(dataResult["validUserId"]);
            }
        });
        return checkResult;
    };
    
    jQuery.validator.addClassRules("userId", { 
                  lettersonly: true
                  , maxLength: 12
                  , required: true
                  , checkExistedUserId: true
                  });

    jQuery.validator.addClassRules("userPasswd", { 
        lettersonly: true
        , maxLength: 8
        , required: true
        });

    jQuery.validator.addClassRules("userPasswdConfirm", { 
        lettersonly: true
        , maxLength: 8
        , required: true
        , equalTo: "#userPasswd"
        });
    
    jQuery.validator.addClassRules("userName", { 
        maxLength: 10
        , required: true
        });

    jQuery.validator.addClassRules("userEmail", { 
        lengthMax: 80
        });
    jQuery.validator.addClassRules("userEmail1", { 
        alphaEmail: jQuery("#userEmail1Lbl").html()
        });

    jQuery.validator.addClassRules("userEmail2", { 
        alphaEmail: jQuery("#userEmail2Lbl").html()
        });

    jQuery("#userDetailForm").validate({
            errorElement: "span",
            onkeyup: false,
            onclick: false
     });
});
