//tab index: btnSave:1, btnReload:2, backButton:3. So, the max current tabindex is:
var maxCurrentTabIndex = 3;
var kendoValidatorVar;

function setTagIndexForListAttribute() {
	var lstRequireFlg = jQuery("input.chkFlg");
	var lstSort = jQuery("input.txtSort");
	
	var length = lstRequireFlg.length;
	
	for (var i = 0; i < length; i++) {
		jQuery(lstRequireFlg[i]).attr("tabindex", i + 1);
		jQuery(lstSort[i]).attr("tabindex", length + i + 1);
	}
}

//Show confirm message when back previous page
function confirmBack() {
//    jQuery.fancybox({
//        modal : true,
//        autoSize: true,
//        content : "<div>"
//                +"     <div id='heading-popup'>"
//                +		jQuery("#title-confirm-back").val()
//        		 +"     </div>"
//        		 +"     <div id='content-popup'>"
//    			 + "    <p>" 
//    			 +         jQuery("#content-confirm-back").val()
//    			 + "    </p>"
//    			 +"     <div style='text-align:center;padding:5px;'>"
//    			 + "        <input id='fancyConfirm_ok' class='btn-size-medium btn-color-blue k-button' type='button' "
//    			 + "	value='" + jQuery("#btn-yes").val() + "' onclick='backToPreviousPage()' style='margin:7px;padding:5px;width:67px;'>"
//		         + "        <input id='fancyConfirm_cancel' class='btn-size-medium btn-color-default k-button' type='button' "
//		         + " value='" + jQuery("#btn-no").val() + "' onclick='jQuery.fancybox.close();' style='margin:7px;padding:5px;width:67px;'>"
//			     +"     </div>"
//		         +"     </div>"
//		         + "</div>"
//    });
	// open popup
	var win = $("#confirmBackDialog").data("kendoWindow");
	win.center();
	win.open();
}

//Show confirm message
function clickBack() {
	//Show confirm message
	confirmBack();
}

//Back to the previous screen
//If doesn't exists, go to Top page
function backToPreviousPage() {
	//BOE THAI.SON FIX BUG "BACK TO SIDE MAP"
  /*//Get previous URL
  var previousUrl = document.referrer;
  //Validate previousUrl
  if ("" != previousUrl) {
      //Go to previous page
      window.location = previousUrl;
      return;
  }
  //Go to Top page
  window.location = defaultRedirectPage;*/
	/* BOE by Luong.Dai at 2014/04/11 Show progress bar */
	showProcessBar();
	/* EOE by Luong.Dai at 2014/04/11 Show progress bar */
	window.location = "./categoryManage.html";
	//EOE THAI.SON FIX BUG "BACK TO SIDE MAP"
}
function loadAjaxBunruiName(bunruiCode) {
	$.ajax({
		type: "POST",
        url: "loadAjaxBunruiName.html",
        data: {
        	"form.bunrui.bunruiCode": bunruiCode
        },
        dataType: "json",
        beforeSend : function(){
        	$("#loading-bunrui").css("display", "inline");
        },
        success: function (dataResult) {
        	// BOE Hoang.Ho 2014/02/17 Change to kendo validate
//        	$("#loading-bunrui").css("display", "none");

    		var parentBunrui = $("#bunruiCode").parent();
    		var errorMsgElem =  parentBunrui.find(".k-invalid-msg");
//        	$("#spnBunruiName").removeClass("loading-ajax");
        	if (dataResult.bunrui == null) {
        		// Invalid
            	$("#is-validated-bunrui-code").val("1");
        		var message = 	jQuery("#bunrui-not-exists").val();
        		errorMsgElem.html('<span class="k-icon k-warning"> </span> ' + message);
        		errorMsgElem.show();
//        		$("#spnBunruiCodeError").text(message);
//                $("#bunruiCode").css("background-color", "#FFD9E6");
                $("#spnBunruiName").text("");
        	} else {
        		// Valid
        		$("#is-validated-bunrui-code").val("0");
        		errorMsgElem.hide();
        		$("#spnBunruiName").text(dataResult.bunrui.bunruiName);
        	}
        	// EOE Hoang.Ho 2014/02/17 Change to kendo validate
        },
        complete : function(){
        	$("#loading-bunrui").css("display", "none");
        }
	});
}

function validateBunruiCode() {
	// BOE Hoang.Ho 2014/02/17 Change to kendo validate
	if ($("#is-validated-bunrui-code").val() == 0) {
		var bunruiCode = $("#bunruiCode").val();
//	    var labelBunruiCode = $("#label-bunrui-code").text();
	
	    if (null == bunruiCode || "" == bunruiCode.trim()) {
//	        var message = 	jQuery("#requiredFill").val();
//	    	var showMessage = message.replace('{0}', labelBunruiCode);
//	    	
//	        $("#spnBunruiCodeError").text(showMessage);
//	        $("#bunruiCode").css("background-color", "#FFD9E6");
//	        $("#spnBunruiName").text("");
	        
	        return false;
	    } else if (bunruiCode.trim().length != 4) {
//	    	var message = 	jQuery("#only-x-number").val();
//	    	var showMessage = message.replace('{0}', 4);
//	    	
//	        $("#spnBunruiCodeError").text(showMessage);
//	        $("#bunruiCode").css("background-color", "#FFD9E6");
//	        $("#spnBunruiName").text("");
	        
	        return false;
	    } 
	    return true;
	} 

	return false;
	// EOE Hoang.Ho 2014/02/17 Change to kendo validate
}

/*
 * Validate blank or length before send ajax.
 */
function validateCategoryName() {
	// valid
	if ($("#is-validated-category-name").val() == 0){
		var categoryName = $("#categoryName").val();
	    if (null == categoryName || "" == categoryName) {
	    	// check empty
	        return false;
	    }
	    return true;
	} 
	// in valid
	return false;
}

function validateTextboxSort(element) {
	var sort = $(element).val();
//    var label = $("#header-sort").text();
    //Sort = -1: attribute is deleted
    if (sort == -1) {
    	return true;
    }
    //Validate null
    if (null == sort || "" == sort) {
//        var message = 	jQuery("#requiredFill").val();
//    	var showMessage = message.replace('{0}', label);
//    	
//    	$(element.parentNode.parentNode).find('.error').text(showMessage);
//        $(element).css("background-color", "#FFD9E6");
        return false;
    } else if (sort.trim().length > 2) {
//    	var message = 	jQuery("#max-length").val();
//    	var showMessage = message.replace('{0}', 2);
//    	
//    	$(element.parentNode.parentNode).find('.error').text(showMessage);
//        $(element).css("background-color", "#FFD9E6");
        return false;
    } else if (checkDuplicateSort(element)) {
//    	var message = 	jQuery("#duplicate-value").val();
//    	var showMessage = message.replace('{0}', label);
//    	
//    	$(element.parentNode.parentNode).find('.error').text(showMessage);    	
//    	$(element).css("background-color", "#FFD9E6");
        return false;
    }
    else {
    	$(element.parentNode.parentNode).find('.error').text('');
        $(element).css("background-color", "#FFFFFF");
    }
    
    return true;
}

function checkDuplicateSort(element) {
	var result = false;
	//If value = -1: attribute has been delete
	if (parseInt($(element).val()) >= 0) {
		var lstSort = $("input.txtSort");
		
		var length = lstSort.length;
		
		for (var i = 0; i < length; i++) {
			if (element != lstSort[i]
				&& parseInt($(element).val()) == parseInt($(lstSort[i]).val())) {
				result = true;
				break;
			}
		}
	}
	
	return result;
}

function validateSortValue() {
	var result = true;
	var lstSort = $("input.txtSort");
	
	var length = lstSort.length;
	
	for (var i = 0; i < length; i++) {
		if (!validateTextboxSort(lstSort[i])) {
			result = false;
		}
	}
	
	return result;
}

//Validate value before insert or update category
//Return 0 if validate success
//Return 1 if validate false at tag Information
//Retrun 2 if validate false at tag Attribute
function validate() {

	// BOE Hoang.Ho 2014/02/17 Change to kendo validate
	var validateCatName = validateCategoryName();
//	if (validateCatName) {	
//		if (jQuery("#spnCategoryNameError").html() != "") {
//			validateCatName = false;
//		}
//	}
	// EOE Hoang.Ho 2014/02/17 Change to kendo validate
	var validateCode = validateBunruiCode();
//	if (validateCode) {	
//		if (jQuery("#spnBunruiCodeError").html() != "") {
//			validateCode = false;
//		}
//	}
	
	var validateSort = validateSortValue();//son comment
//	
	// 
	if ( validateCatName && validateCode && validateSort){
		return 0;
	}
	//boe thai.son 20-3
	if (!validateCode || !validateCatName) {
		return 1;
	} else if(!validateSort){
		return 2;
	}
	//eoe thai.son 20-3
}

function getOriginalAttribute() {
	var lstAttr = new Array();
	
	var lstOriginalAttr = jQuery(".original-attribute");
	
	var length = lstOriginalAttr.length;
	
	for (var i = 0; i < length; i++) {
		lstAttr[i] = jQuery(lstOriginalAttr[i]).val();
	}
	
	return lstAttr; 
}

function compareWithOriginalAttr(stringAttr, lstAttr) {
	var length = lstAttr.length;
	
	for (var i = 0; i < length; i++) {
		if (lstAttr[i] == stringAttr) {
			return true;
		}
	}
	
	return false;
}
function checkValueChangeWhenAddNew() {
	var currentCatName = jQuery("#categoryName").val();
	if(currentCatName.trim() != ""){
		return true;
	}else{
		var currentBunruiCode = jQuery("#bunruiCode").val();
		if(currentBunruiCode.trim() != ""){
			return true;
		}else{
			var currentDelFlg = jQuery("#delFlag").val();
			if(currentDelFlg != "0"){
				return true;
			}else{
				var lstCurrentAttr = jQuery(".tr-catAttribute").not(".delete-row");
				if(lstCurrentAttr != null && lstCurrentAttr.length != 0){
					return true;
				}else{
					return false;
				}
			}
		}
	}
}
function checkValueChangeWhenUpdate(lstAttr) {
	if (null == lstAttr) {
		return true;
	} else {
		var oldCatName = jQuery("#originalCategoryName").val();
		var oldBunruiCode = jQuery("#originalBunruiCode").val();
		var oldDelFlg = jQuery("#originalDelFlg").val();
		
		var currentCatName = jQuery("#categoryName").val();
		var currentBunruiCode = jQuery("#bunruiCode").val();
		var currentDelFlg = jQuery("#delFlag").val();
		
		if (oldCatName != currentCatName
			|| oldBunruiCode != currentBunruiCode
			|| oldDelFlg != currentDelFlg) {
			return true;
		}
		
		var lstCurrentAttr = jQuery(".tr-catAttribute").not(".delete-row");
		
		var length = lstCurrentAttr.length;
		
		if (length != lstAttr.length) {
			return true;
		}
		
		for (var i = 0; i < length; i++) {
			var stringAttr = createStringFromAttribute(lstCurrentAttr[i]);
			
			if (!compareWithOriginalAttr(stringAttr, lstAttr)) {
				return true;
			}
		}
	}
	
	return false;
}

function createStringFromAttribute(element) {
	var attrCode = jQuery(element).find(".attribute-code").val();
	var attrRequireFlg = jQuery(element).find(".hdnFlg").val();
	var attrSort = jQuery(element).find(".txtSort").val();
	
	return attrCode + ";" + attrRequireFlg + ";" + attrSort;
}

function showMessage(message, type) {
	jQuery("#message").text(message);
	
	if (null == message || "" == message) {
		jQuery("#message").removeClass("has-message");
	} else {
		jQuery("#message").addClass("has-message");
	}
	
	//Type = 1: Notice
	//Type = 2: Error
	if (type == "1") {
		jQuery("#message").addClass("success");
	} else {
		jQuery("#message").addClass("msg-error");
	}
}

function getListAttribute() {
	jQuery.ajax({
        type: "POST",
        url: "searchAjaxAttributeName.html",
        data: {
        	attrName: $("#keyword_attribute").val()
        },
        dataType: "json",
        beforeSend : function(){
            jQuery("#attribute_search_result").css("background", "47% 45% url('images/loading.gif') no-repeat");
        },
        success: function(dataResult) {
        	//Get list current attribute code
        	var lstRowAttr = jQuery(".tr-catAttribute").not(".delete-row");
        	
        	var currentLstAttrCode = jQuery(lstRowAttr).find(".attribute-code");
        	
        	var lstAttribute = dataResult.lstAttribute;
        	
        	var ulTag = "<ul>";
        	
        	var resultLength = lstAttribute.length;
        	var currentLength = currentLstAttrCode.length;
        	
        	for (var i = 0; i < resultLength; i++) {
        		var attrExists = false;
        		for (var j = 0; j < currentLength; j++) {
        			if (lstAttribute[i].attributeCode == currentLstAttrCode[j].value) {
        				attrExists = true;
        				break;
        			}
        		}
        		
        		if (attrExists) {
        			ulTag += "<li class='attr-disable'>"; 
        		} else {
        			ulTag += "<li>";
        		}
        		
        		ulTag += "<span class='attr-name'>" + lstAttribute[i].attributeName + "</span>"
        				+ "<span class='attr-code'>" + lstAttribute[i].attributeCode + "</span>"
        				+ "<span class='attr-type'>" + lstAttribute[i].attributeType + "</span>"
        				+ "</li>";
        	}
        	
        	ulTag += "</ul>";
        	
        	jQuery("#attribute_search_result").empty();
        	jQuery("#attribute_search_result").append(ulTag);
        },
        complete: function(){
        	jQuery("#attribute_search_result").css("background", "");
        }
    });
}

function getBunruiList() {
	jQuery.ajax({
        type: "POST",
        url: "searchAjaxBunruiName.html",
        data: {
        	bunruiName: $("#keyword_bunrui").val()
        },
        dataType: "json",
        beforeSend : function(){
            jQuery("#bunrui_search_result").css("background", "47% 45% url('images/loading.gif') no-repeat");
        },
        success: function(dataResult) {
        	var lstBunrui = dataResult.lstBunrui;
        	
        	var ulTag = "<ul>";
        	
        	var length = lstBunrui.length;
        	
        	for (var i = 0; i < length; i++) {
        		ulTag += "<li>" 
        				+ "<span class='bunrui-name'>" + lstBunrui[i].bunruiName + "</span>"
        				+ "<span class='bunrui-code'>" + lstBunrui[i].bunruiCode + "</span>"
        				+ "</li>";
        	}
        	
        	ulTag += "</ul>";
        	
        	jQuery("#bunrui_search_result").empty();
        	jQuery("#bunrui_search_result").append(ulTag);
        },
        complete: function(){
        	jQuery("#bunrui_search_result").css("background", "");
        }
    });
}

function loadAjaxCheckCategoryName(categoryName, categoryCode) {
	jQuery.ajax({
        type: "POST",
        url: "loadAjaxCategoryName.html",
        data: {
        	categoryName: categoryName,
        	categoryCode: categoryCode
        },
        dataType: "json",
        beforeSend : function(){
            jQuery("#loading-category").css("display", "inline");
        },
        success: function(dataResult) {
        	var result = dataResult.existsCategory;
    		var parentCate = $("#categoryName").parent();
    		var errorMsgElem =  parentCate.find(".k-invalid-msg");
        	// BOE Hoang.Ho 2014/02/17 Change to kendo validate
//        	if (result == true) {
//        		
//        		jQuery("#spnCategoryNameError").text(showMessage);
//        		jQuery("#categoryName").css("background-color", "#FFD9E6");
//        	} else {
//        		jQuery("#spnCategoryNameError").text("");
//        		jQuery("#categoryName").css("background-color", "#FFFFFF");
//        	}
//        	
        	if (result == true) {        		
        		//Invalid
        		// set flag
        		$("#is-validated-category-name").val("1");
        		// get message.
        		var message = jQuery("#category-name-exists").val();
        		var label = jQuery("#label-category-name").text();
        		var showMessage = message.replace('{0}', label);
        		// replace message.
        		errorMsgElem.html('<span class="k-icon k-warning"> </span> ' + showMessage);
        		errorMsgElem.show();
        	} else {
        		// valid
        		$("#is-validated-category-name").val("0");
        		errorMsgElem.hide();
        	}
        	// EOE Hoang.Ho 2014/02/17 Change to kendo validate
        },
        complete : function(){
            jQuery("#loading-category").css("display", "none");
        }
    });
}
var MAX_NUM_CHAR_TITLE_CATEGORY_NAME_CAN_SHOW = 10;
$(document).ready(function () {
	//BOE THAI.SON
	if ($("#insertMode").val() == "0" && $("#myCategoryNameEdit").val().trim().length > MAX_NUM_CHAR_TITLE_CATEGORY_NAME_CAN_SHOW){
		$("#categoryNameLabel").kendoTooltip({
	        content: $("#categoryNameLabel").text(),
	        animation : {
	            close : {
	                effects : "fade:out"
	            },
	            open : {
	                effects : "fade:in",
	                duration : 300
	            }
	        }
	    });
	}
	//EOE THAI.SON
	var lstAttr = getOriginalAttribute();
	
    $("ul.tabs").tabs("div.panes > div");
    
    $("#bunruiCode").keydown(function (event) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(event.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
            // Allow: Ctrl+A
            (event.keyCode == 65 && event.ctrlKey === true) ||
            // Allow: home, end, left, right
            (event.keyCode >= 35 && event.keyCode <= 39)) {
            // let it happen, don't do anything
            return;
        } else {
            // Ensure that it is a number and stop the keypress
            if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105)) {
                event.preventDefault();
            }
        }
    });
    
    $(".txtSort").keydown(function (event) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(event.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
            // Allow: Ctrl+A
            (event.keyCode == 65 && event.ctrlKey === true) ||
            // Allow: home, end, left, right
            (event.keyCode >= 35 && event.keyCode <= 39)) {
            // let it happen, don't do anything
            return;
        } else {
            // Ensure that it is a number and stop the keypress
            if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105)) {
                event.preventDefault();
            }
        }
    });

    $("#bunruiCode").change(function () {
    	//Set is validated bunrui is false
    	$("#is-validated-bunrui-code").val("0");
    	
        if (validateBunruiCode()) {
        	var bunruiCode = $("#bunruiCode").val();
        	loadAjaxBunruiName(bunruiCode);
        }
    });
    
    $("#categoryName").change(function () {
    	//Set is validated bunrui is false
    	$("#is-validated-category-name").val("0");
    	
        if (validateCategoryName()) {
        	var categoryName = $("#categoryName").val();
        	var categoryCode = "";
        	var mode = $("#insertMode").val();
        	
        	if (mode == "0") {
        		categoryCode = $("#categoryCode").val();
        	}
        	
        	loadAjaxCheckCategoryName(categoryName, categoryCode);
        }
    });
    
    $("#delFlag").change(function () {
    	if ($("#delFlag").prop("checked")) {
    		$(this).val("1");
    	} else {
    		$(this).val("0");
    	}
    });
    
    $("#btnReload").click(function () {
    	/* BOE by Luong.Dai at 2014/04/11 Show progress bar */
    	showProcessBar();
    	/* EOE by Luong.Dai at 2014/04/11 Show progress bar */
    	location.reload();
    });
	// BOE Hoang.Ho 2014/02/17 Change to kendo validate
//    $("#categoryName").focusout(function () {
//    	validateCategoryName();
//    });
    
//    $("#bunruiCode").focusout(function () {
//    	validateBunruiCode();
//    });
	// EOE Hoang.Ho 2014/02/17 Change to kendo validate
    
//    $(".txtSort").focusout(function() {
//    	//validateTextboxSort(this);//son comment
//    });

    $(".btnDelete").click(function () {
    	var tr = $(this).closest('tr');
        $(tr).find("td.column-attr-name input.del-flg").val("1");
        $(tr).find(".txtSort").attr("value", "-1");
        tr.addClass("delete-row");
    });
    
    $(".chkFlg").change(function () {
    	var tr = $(this).closest('td');
    	if ($(this).prop("checked")) {
    		tr.find(".hdnFlg").val("1");
    	} else {
    		tr.children(".hdnFlg").val("0");
    	}
    });

    $("#btnSave").click(function () {
    	// BOE Hoang.Ho 2014/02/17 Change to kendo validate
    	// validate kendo
    	//boe thai.son 20-3
    	/*if (!kendoValidatorVar.validate()) {
    		return false;
    	}*/
    	var kendoValueValidate = kendoValidatorVar.data("kendoValidator").validate();
    	//eoe thai.son 20-3
    	// EOE Hoang.Ho 2014/02/17 Change to kendo validate
    	var validateResult = validate();
    	if (validateResult == 0) {
    		//Validate success
    		if (checkValueChangeWhenUpdate(lstAttr)) {
    			//boe thai.son 20-3
    			//BOE THAI.SON
    			//showMessage("");
    			//EOE THAI.SON
    			/* BOE by Luong.Dai 2014/03/18 Fix cannot submit form when delete attribute */
    			/*$("#editCategoryActionForm").submit();*/
    			if(kendoValueValidate == true){
    				document.getElementById("editCategoryActionForm").submit();
    			}
    			/* EOE by Luong.Dai 2014/03/18 Fix cannot submit form when delete attribute */
    			//eoe thai.son 20-3
    		} else {
    			var message = $("#unchange").val();
    			
    			showMessage(message, "2");
    		}
    	} else if (validateResult == 1){
    		//Validate false at tab Info
    		$("#tabs-info").click();
    	} else {
    		//Validate false at tab Attribute
    		$("#tabs-attr").click();
    	}
    });
	//BOE THAI.SON
    $("#backButton").click(function () {
    	if($('#insertMode').val() == 1){
    		if (checkValueChangeWhenAddNew()) {
    			clickBack();	
    		}else{
    			backToPreviousPage();
    		}
    	}else{
    		if (checkValueChangeWhenUpdate(lstAttr)) {
    			clickBack();	
    		}else{
    			backToPreviousPage();
    		}
    	}
    	
    	
    });
    //EOE THAI.SON
    //BOE THAI.SON 13-2
	//Show popup search bunrui
	/*$("#popup_search_bunrui").dialog({
		autoOpen: false,
		title: $("#title-search-bunrui").val(),
		position: {
			my: 'left top',
		    at: 'right bottom',
		    of: $('#aBunruiName')
		},
		width: 250
	});*/
    $("#popup_search_bunrui").kendoWindow({
		width : 300,
		height : 300,
		title : $("#title-search-bunrui").val(),
		resizable : false,
		//modal : true,
		visible : false,
		actions: [
	        "Close"
	    ],
	});
    
    /*$("#aBunruiName").click(function(e) {
		e.stopPropagation();
		$("#popup_search_bunrui, #popup_search_attribute").dialog('close');
		//Reset popup value
		jQuery("#keyword_bunrui").val("");
		jQuery("#bunrui_search_result").empty();
		$("#popup_search_bunrui").dialog("open");
		getBunruiList();
	});*/
	$("#aBunruiName").click(function(e) {
		e.stopPropagation();
		//$("#popup_search_bunrui, #popup_search_attribute").dialog('close');
		//Need replace this statement for fitting.
		$("#popup_search_bunrui").data("kendoWindow").close();
		$("#popup_search_attribute").data("kendoWindow").close();
		//
		//Reset popup value
		jQuery("#keyword_bunrui").val("");
		jQuery("#bunrui_search_result").empty();
		//$("#popup_search_bunrui").dialog("open");
		$("#popup_search_bunrui").data("kendoWindow").open();
		$(".k-window").css("left",((screen.availWidth)/2-150)+"px");
		$(".k-window").css("top",((screen.availHeight)/2-100)+"px");
		getBunruiList();
	});
	//EOE THAI.SON 13-2
	
	//Press enter in textbox bunrui search
	jQuery(document).on("keypress", "#keyword_bunrui", function(e){ 
    	if (e.keyCode == 13) {
    		getBunruiList();
        }
    });
	//BOE THAI.SON 15-2
	//Click to selected bunrui to fill in textbox bunrui code 
	/*jQuery(document).on("click", "#bunrui_search_result ul li", function(){ 
		jQuery("#spnBunruiName").text(jQuery(this).find(".bunrui-name").text());
		jQuery("#bunruiCode").val(jQuery(this).find(".bunrui-code").text());
		//Set is validated bunrui is false
    	$("#is-validated-bunrui-code").val("0");
		validateBunruiCode();
		//BOE THAI.SON
		$("#popup_search_bunrui").data("kendoWindow").close();
		//EOE THAI.SON
    });*/
	var alreadyclicked=false;
	jQuery(document).on("click", "#bunrui_search_result ul li", function(){
        var el=$(this);
        if (alreadyclicked)
        {
            alreadyclicked=false; // reset
            clearTimeout(alreadyclickedTimeout); // prevent this from happening
            // do what needs to happen on double click. 
            jQuery("#spnBunruiName").text(jQuery(this).find(".bunrui-name").text());
    		jQuery("#bunruiCode").val(jQuery(this).find(".bunrui-code").text());
    		//Set is validated bunrui is false
        	$("#is-validated-bunrui-code").val("0"); 
//    		validateBunruiCode();
    		//BOE THAI.SON
    		$("#popup_search_bunrui").data("kendoWindow").close();
        }
        else
        {
            alreadyclicked=true;
            alreadyclickedTimeout=setTimeout(function(){
                alreadyclicked=false; // reset when it happens
                // do what needs to happen on single click. 
                // use el instead of $(this) because $(this) is 
                // no longer the element
                jQuery("#spnBunruiName").text(el.find(".bunrui-name").text());
        		jQuery("#bunruiCode").val(el.find(".bunrui-code").text());
        		//Set is validated bunrui is false
            	$("#is-validated-bunrui-code").val("0");
//        		validateBunruiCode();
        		//BOE THAI.SON
        		$("#popup_search_bunrui").data("kendoWindow").close();
            },300); // <-- dblclick tolerance here
        }
    	// BOE Hoang.Ho 2014/02/17 Change to kendo validate
		// Remove message 
		var parentBunrui = $("#bunruiCode").parent();
		var errorMsgElem =  parentBunrui.find(".k-invalid-msg");
		errorMsgElem.hide();
	    // EOE Hoang.Ho 2014/02/17 Change to kendo validate
        return false;
    });
	//EOE THAI.SON 15-2
	//BOE THAI.SON
	//Show popup search attribute
	/*$("#popup_search_attribute").dialog({
		autoOpen: false,
		title: $("#title-search-attr").val(),
		position: {
			my: 'left top',
		    at: 'right bottom',
		    of: $('#aAttributeName')
		},
		width: 250,
		closeOnEscape: true
	});*/
	$("#popup_search_attribute").kendoWindow({
		width : 300,
		height : 300,
		title : $("#title-search-attr").val(),
		resizable : false,
		//modal : true,
		visible : false,
		actions: [
	        "Close"
	    ],
	});
	//Click link to show popup search attribute
	/*$("#aAttributeName").click(function(e) {
		e.stopPropagation();
		$("#popup_search_bunrui, #popup_search_attribute").dialog('close');   
		//Reset popup value
		jQuery("#keyword_attribute").val("");
		jQuery("#attribute_search_result").empty();
		$("#popup_search_attribute").dialog("open");
		getListAttribute();
	});*/
	$("#aAttributeName").click(function(e) {
		e.stopPropagation();
		//$("#popup_search_bunrui, #popup_search_attribute").dialog('close');
		$("#popup_search_bunrui").data("kendoWindow").close();
		$("#popup_search_attribute").data("kendoWindow").close();
		//Reset popup value
		jQuery("#keyword_attribute").val("");
		jQuery("#attribute_search_result").empty();
		//$("#popup_search_attribute").dialog("open");
		$("#popup_search_attribute").data("kendoWindow").open();
		$(".k-window").css("left",((screen.availWidth)/2-500)+"px");
		$(".k-window").css("top",((screen.availHeight)/2-100)+"px");
		getListAttribute();
	});
	
	//Press enter in textbox attribute search
	jQuery(document).on("keypress", "#keyword_attribute", function(e){ 
    	if (e.keyCode == 13) {
    		getListAttribute();
        }
    });
	
	//Click to selected attribute 
	jQuery(document).on("click", "#attribute_search_result ul li", function(){ 
		//Check disable
		if (jQuery(this).attr("class") == "attr-disable") {
			return;
		}
		// define max value sort.
		var valuesSortMax = -1;
		// loop and get max value.
		$(".tr-catAttribute").not(".delete-row").find('.txtSort').each(function(){
			var valueSort = $(this).val();
			var valueSortInt = 0;
			if (valueSort != undefined && valueSort != "") {
				valueSortInt = parseInt(valueSort);
				if (valueSortInt > valuesSortMax) {
					valuesSortMax = valueSortInt;
				}
			}
		});
		
		
		var index = jQuery("table tr.tr-catAttribute").length;
		var html = '<tr class="tr-catAttribute">'
				+ 		'<td class="column-attr-name">'
				+			'<input type="hidden" class="attribute-code"'
				+ 					'id="editCategoryActionForm_form_catAttributeList_' + index + '__categoryCode" value="' 
				+ 					jQuery("#categoryCode").val() + '" name="form.catAttributeList[' + index + '].categoryCode">'
				+			'<input type="hidden" class="del-flg"'
				+ 					'id="editCategoryActionForm_form_catAttributeList_' + index + '__delFlg" value="' 
				+ 					0 + '" name="form.catAttributeList[' + index + '].delFlg">'
				+			'<input type="hidden" class="attribute-code"'
				+					'id="editCategoryActionForm_form_catAttributeList_' + index + '__attributeCode" value="'
				+					jQuery(this).find(".attr-code").text() + '" name="form.catAttributeList[' + index + '].attributeCode">'
				+					jQuery(this).find(".attr-name").text()
				+ 		'</td>'
				+		'<td class="column-attr-type">' + jQuery(this).find(".attr-type").text() + '</td>'
				+		'<td class="column-attr-required-flg"> <input type="checkbox" class="chkFlg"'
				+				'id="editCategoryActionForm_form_catAttributeList_' + index + '__chkRequiredFlg" '
				+				'value="catAttributeList[' + index + '].chkRequiredFlg" name="form.catAttributeList[' + index + '].chkRequiredFlg">'
				+			'<input type="hidden" value="0" name="form.catAttributeList['
				+				index + '].requiredFlg" id="editCategoryActionForm_form_catAttributeList_' + index + '__requiredFlg" class="hdnFlg">'
				+		'</td>'
				//BOE THAI.SON 21-2 bug 73
				/*
				////BOE THAI.SON
				+ 		'<td class="column-attr-sort"> <input type="text" maxlength="2" data-parameters="'+$("#header-sort").text()+'" class="txtSort k-input k-required k-duplicated" id="editCategoryActionForm_form_catAttributeList_' + index + '__sort"'
				+										'value="' + (parseInt(valuesSortMax) + 1) + '" '
				////EOE THAI.SON
				*/
				////BOE THAI.SON
				+ 		'<td class="column-attr-sort"> <input type="text" maxlength="2" data-parameters="'+$("#header-sort").text()+'" class="txtSort k-input k-textbox k-required k-duplicated" id="editCategoryActionForm_form_catAttributeList_' + index + '__sort"'
				+										'value="' + (parseInt(valuesSortMax) + 1) + '" '
				////EOE THAI.SON
				//eoe thai.son thai.son bug 73
				+ 			'name="form.catAttributeList[' + index + '].sort"> </td>'
				+		'<td class="column-attr-delete"> <input type="button" value="' + jQuery("#deleteLabel").val() + '" class="btnDelete btn-color-default k-button"> </td>'
				+		'<td class="error"></td>'
				+	'</tr>';
		
		jQuery("table tbody").append(html);
		//BOE THAI.SON
		var valueSortMaxCurrent = jQuery(".tr-catAttribute").not(".delete-row").last().find($('.txtSort')).val();
		if(!validateNumberic(valueSortMaxCurrent)){
			jQuery(".tr-catAttribute").not(".delete-row").last().find($('.txtSort')).val(0);
		}
		//EOE THAI.SON
		//Disable attribute
		jQuery(this).attr("class", "attr-disable");
		
		//Bind click function for delete row
		jQuery("table tbody tr:last-child .btnDelete").bind( "click", function( event ) {
			var tr = $(this).closest('tr');
	        $(tr).find("td.column-attr-name input.del-flg").val("1");
	        $(tr).find(".txtSort").attr("value", "-1");
	        tr.addClass("delete-row");
		});
		
		//Check in require flg
		jQuery("table tbody tr:last-child .chkFlg").bind("change", function() {
			var tr = $(this).closest('td');
	    	if ($(this).prop("checked")) {
	    		tr.find(".hdnFlg").val("1");
	    	} else {
	    		tr.children(".hdnFlg").val("0");
	    	}
		});
		
		//Focus out textbox sort
		jQuery("table tbody tr:last-child .txtSort").bind("focusout", function() {
			//validateTextboxSort(this);//son comment
		});
		
		//Input number only in textbox sort
		jQuery("table tbody tr:last-child .txtSort").bind("keydown", function(event) {
			// Allow: backspace, delete, tab, escape, enter and .
	        if ($.inArray(event.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
	            // Allow: Ctrl+A
	            (event.keyCode == 65 && event.ctrlKey === true) ||
	            // Allow: home, end, left, right
	            (event.keyCode >= 35 && event.keyCode <= 39)) {
	            // let it happen, don't do anything
	            return;
	        } else {
	            // Ensure that it is a number and stop the keypress
	            if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105)) {
	                event.preventDefault();
	            }
	        }
		});
		
		//Set tag index for list attribute
		setTagIndexForListAttribute();
    });
	
	$(document).click(function(e) {
	     if (!($(e.target).closest('#popup_search_bunrui').length || $(e.target).closest('#popup_search_attribute').length)) {
	    	 //BOE THAI.SON
	         //$("#popup_search_bunrui, #popup_search_attribute").dialog('close');
	         $("#popup_search_bunrui").data("kendoWindow").close();
	         $("#popup_search_attribute").data("kendoWindow").close();
	         //EOE THAI.SON
	     }
	});
	
	// Set tag index for list attribute
	setTagIndexForListAttribute();
    
	//BOE THAI.SON: TAB INDEX:
	// set defalt focus for page.
    $("#categoryName").focus();
    // event for tabindex, keyCode = 9 stands for [Tab] key.
    $("#delFlag").keydown(function (e) {
        if (e.keyCode == 9) {
            e.preventDefault();
            //$("#categoryName").focus();
            $("[tabindex='1']").focus();
        }
    });
    $("#aAttributeName").keydown(function (e) {
        if (e.keyCode == 9) {
            e.preventDefault();
            //$("#categoryName").focus();
            $("[tabindex='1']").focus();
        }
    });
    $("[tabindex='"+ maxCurrentTabIndex + "']").keydown(function (e) {
        if (e.keyCode == 9) {
            e.preventDefault();
            $("#aAttributeName").focus();
        }
    });
	//EOE THAI.SON
    //BOE THAI.SON KENDO VALIDATE:
    kendoValidatorVar = $("#editCategoryActionForm").kendoValidator({
        rules: {
            required: function (input) {
                //class-required will be check not blank
                var inputCls = input.attr("class");
                if (inputCls != undefined) {
                    if (inputCls.indexOf("k-required") > -1) {
                        return input.val() != "";
                    }
                }
                return true;
            },
            duplicated: function(input){
            	//class-required will be check not blank
                var inputCls = input.attr("class");
                if (inputCls != undefined) {
                    if (inputCls.indexOf("k-duplicated") > -1) {
                        //return input.val() != "";//checkDuplicateSort
                    	return checkDuplicateSort(input[0]) == false;
                    }
                }
                return true;
            },
            ajaxCateNmExist: function (input) {
                //class-required will be check not blank
                var inputCls = input.attr("class");
                if (inputCls != undefined) {
                    if (inputCls.indexOf("k-ajaxCateNmExist") > -1) {
                        return $("#is-validated-category-name").val() != 1;
                    }
                }
                return true;
            },
            ajaxBunruiCdExist: function (input) {
                //class-required will be check not blank
                var inputCls = input.attr("class");
                if (inputCls != undefined) {
                    if (inputCls.indexOf("k-ajaxBunruiCdExist") > -1) {
                        return $("#is-validated-bunrui-code").val() != 1;
                    }
                }
                return true;
            },
            bunruiCdLength : function (input) {
                //class-required will be check not blank
                var inputCls = input.attr("class");
                if (inputCls != undefined) {
                    if (inputCls.indexOf("k-bunruiCdLength") > -1) {
                		var bunruiCode = $("#bunruiCode").val();
                        return bunruiCode.trim().length == 4;
                    }
                }
                return true;
            }
            
        },
        messages: {
            required: function (input) {
                return getMsgWithParams(input, $("#requiredFill").val());
            },
            ajaxCateNmExist : function(input) {
                return getMsgWithParams(input, $("#category-name-exists").val());
            },
            ajaxBunruiCdExist : function(input) {
                return getMsgWithParams(input, $("#bunrui-not-exists").val());
            },
            bunruiCdLength : function(input) {
    	    	var message = 	jQuery("#only-x-number").val() + "";
    	    	return message.replace('{0}', 4);
            },
            duplicated: function (input) {
            	var message = 	jQuery("#duplicate-value").val();
            	var showMessage = message.replace('{0}', $("#header-sort").text());
                return showMessage;
            }
        }
    });
    // click on tab then set tabs color and border.
    $(".tabStrip").click(function () {
    	var id = this.id;
    	if ("tabs-info" == id) {
    		$("#categoryName").focus();
    		$("#tabs-info").css("background-color","#FFFFFF");
    		$("#tabs-info").css("color","#000000");
        	$("#tabs-info").css("border-color","#7EA700");
        	$("#tabs-attr").css("border-color","#AAAAAA");
    	} else {
    		$("#tabs-attr").css("background-color","#FFFFFF");
    		$("#tabs-attr").css("color","#000000");
        	$("#tabs-attr").css("border-color","#7EA700");
        	$("#tabs-info").css("border-color","#AAAAAA");
    	}
    });
    
    // mouse over tab then set color and border.
    $(".tabStrip").bind("mouseover", function () {
    	var cl = $(this).attr("class");
    	var currentCl = cl.replace("tabStrip", "").trim();
    	var id = this.id;
    	if (currentCl == "") {
    		if ("tabs-info" == id) {
        		$("#tabs-info").css("background-color","#7EA700");
            	$("#tabs-info").css("color","#FFFFFF");
            	$("#tabs-info").css("border-color","#7EA700");
        	} else {
        		$("#tabs-attr").css("background-color","#7EA700");
            	$("#tabs-attr").css("color","#FFFFFF");
            	$("#tabs-attr").css("border-color","#7EA700");
        	}
    	}
    });
    
    // mouse out tab set tab to original css
    $(".tabStrip").bind("mouseout", function () {
    	var cl = $(this).attr("class");
    	var currentCl = cl.replace("tabStrip", "").trim();
    	var id = this.id;
    	if (currentCl == "") {
    		if ("tabs-info" == id) {
        		$("#tabs-info").css("background-color","#FFFFFF");
            	$("#tabs-info").css("color","#000000");
            	$("#tabs-info").css("border-color","#AAAAAA");
        	} else {
        		$("#tabs-attr").css("background-color","#FFFFFF");
            	$("#tabs-attr").css("color","#000000");
            	$("#tabs-attr").css("border-color","#AAAAAA");
        	}
    	}
    });
    // dialog 
    $("#confirmBackDialog").kendoWindow({
        maxWidth: 400,
        resizable: false,
        modal: true,
        visible: false,
        title: $("#title-confirm-back").val(),
        activate: function () {
            jQuery("#fancyConfirm_cancel").focus();
        }
	});
    $("#kendoConfirm_cancel").click(function(e) {
        e.preventDefault();
        var win = $("#confirmBackDialog").data("kendoWindow");
        win.close();
        return false;
    });
    $("#backButton").keydown(function (e) {
        if (e.keyCode == 9) {
            e.preventDefault();
            $("#categoryName").focus();
        }
    });
});
function getMsgWithParams(input, msgFormat) {
    var params = input.data("parameters");
    if (params != undefined && params != "") {
        var paramArr = params.split(",");
        var length = paramArr.length;
        if (length > 0) {
            for (var i = 0; i < length; i++) {
                msgFormat = msgFormat.replace("{" + i + "}", paramArr[i]);
            }
        }
    }
    return msgFormat;
}  