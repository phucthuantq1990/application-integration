var errorMessage = "";
jQuery(document).ready(function() {
	// Set selected Administrator
	SetDefaultValue();
	jQuery("#pjName").focusout(function() {
	    validate(jQuery("#pjName").val().trim()); 
	});
	//Cancel button click
	jQuery("#btnCancel").click(function() {
	    var previousUrl = document.referrer;
	    if("" != previousUrl) {
	        window.location = previousUrl;
	        return;
	    }
	    window.location = defaultRedirectPage;
	    return;
	});
});

function SetDefaultValue() {
	var userName = jQuery('#userLoginId').val();

	$("select option").filter(function() {
		// may want to use $.trim in here
		return $(this).val() == userName;
	}).prop('selected', true);

	setFocusTextbox("pjName");
}

function setFocusTextbox(idName){
	var txtBox = document.getElementById(idName);
    if (txtBox != null) {
    	txtBox.focus();
    }
}

function validate(matterName) {
    if ("" == matterName) {
        errorMessage = jQuery.validator.format(matterNameRequired,jQuery("#pjLabel").html());;
    } else if (matterName.length > 255) {
        errorMessage = jQuery.validator.format(inputOutOfRange, "255");
    } else if (!/^[a-zA-Z0-9O-X-a-yĤ-ß@-§-ŬŜß\u3040-\u309f\u30a0-\u30ff\u4e00-\u9faf\s]+$/i.test(matterName)) {
        errorMessage = PJNameNotCorrect;
    } else {
        errorMessage = "";
    }
    if ("" != errorMessage) {
        jQuery("#pjName").addClass("error");
        jQuery("#errorMessage").html(errorMessage);
        jQuery("#errorMessage").css("display", "inline-block");
        return;
    }
    jQuery("#pjName").removeClass("error");
    jQuery("#errorMessage").html("");
    jQuery("#errorMessage").css("display", "none");
    return;
}

function insertMatter() {
    var matterName = jQuery("#pjName").val().trim();
    jQuery("#pjName").val(matterName);
    validate(matterName);
    if ("" == errorMessage) {
        jQuery("#form_new_master").submit();
        return;
    }
}

