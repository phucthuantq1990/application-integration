
function getMainDataAjax() {
    var status = jQuery('select[name="status"]').find(":selected").val();
    var userId = jQuery('select[name="user"]').find(":selected").val();

    jQuery.ajax({
        type: "POST",
        url: "mainMenuLoadJson.html",
        data: {
            status: status,
            userId: userId
        },
        dataType: "json",
        success: function(dataResult) {
           var resultHtml = "";
           var data = eval(dataResult["mainBodyList"]);
           if(data.length > 0) {
               jQuery.each(data, function(i, item) {
                   resultHtml += "<tr>"
                       + '<td style="text-align:right; width: 7%;">'   + item.matterNo                         + '</td>'
                       + '<td style="text-align:center; width: 9%;">' + getFormattedDate(item.tourokuDate)     + '</td>'
                       + '<td style="text-align:center; width: 9%;">' + getFormattedDate(item.completionDate)  + '</td>'
                       + '<td style="text-align:center; width: 10%;">' + item.matterKindName                    + '</td>'
                       + '<td style="text-align:left; width: 30%;">'   + item.matterName                        + '</td>'
                       + '<td style="text-align:center; width: 13%;">' + item.userLastName + ' ' + item.userFirstName + '</td>'
                       + '<td style="text-align:center; width: 8%;">' + item.matterStatusName                  + '</td>'
                       + '<td class="tbl-colum-action" align="center" width="14%">';
                       if (item.matterStatusCode == '00') {
                           resultHtml += '<a href="productInfo.html?matterNo=' + item.matterNo + '"'
                                      + 'onclick="chectButtomClick(' + item.matterNo + ',' + item.matterKindCode + '); return false;"'
                                      + 'class="tbl-control-size-small btn-size-small btn-color-default tipS"'
                                      + 'title="' + checkDataBtnLbl + '"'
                                      + 'style="color: gray; text-decoration: none;">' + checkDataBtnLbl + '</a>';
                           resultHtml += '<a href="matterDetail.html?matterNo='+ item.matterNo + '"'
                                      + 'class="tbl-control-size-small btn-size-small btn-color-default"'
                                      + 'title="' + ToMatterDetailBtnLbl + '"'
                                      + 'style="color: gray; text-decoration: none; ">' + ToMatterDetailBtnLbl + '</a>';
                       } else if (item.matterStatusCode == '10') {
                           resultHtml += '<a href="productInfo.html?matterNo=' + item.matterNo + '"'
                                      + 'onclick="chectButtomClick(' + item.matterNo + ',' + item.matterKindCode + '); return false;"'
                                      + 'class="tbl-control-size-small btn-size-small btn-color-gold"'
                                      + 'title="' + checkDataBtnLbl + '"'
                                      + ' style="color: #ffffff; text-decoration: none;">' + checkDataBtnLbl + '</a>';
                           resultHtml += '<a href="matterDetail.html?matterNo='+ item.matterNo + '"'
                                      + ' class="tbl-control-size-small btn-size-small btn-color-default"'
                                      + ' title="' + ToMatterDetailBtnLbl + '"'
                                      + ' style="color: gray; text-decoration: none; ">' + ToMatterDetailBtnLbl + '</a>';
                       } else if (item.matterStatusCode == '20'){
                           resultHtml += '<a href="productInfo.html?matterNo=' + item.matterNo + '"'
                                      + ' onclick="chectButtomClick(' + item.matterNo + ',' + item.matterKindCode + '); return false;"'
                                      + 'class="tbl-control-size-small btn-size-small"'
                                      + 'title="' + checkDataBtnLbl + '"'
                                      + 'style="visibility: hidden;">' + checkDataBtnLbl + '</a>';
                           resultHtml += '<a href="matterDetail.html?matterNo='+ item.matterNo + '"'
                                      + 'class="tbl-control-size-small btn-size-small btn-color-default"'
                                      + 'title="' + ToMatterDetailBtnLbl + '"'
                                      + 'style="color: gray; text-decoration: none; ">' + ToMatterDetailBtnLbl + '</a>';
                       }
                       
                   resultHtml += '</td>'
                              + "</tr>";
               });
           } else {
               resultHtml += "<tr>" + "<td colspan='8' class='errorMessage'>" + nodataMessage + "</td>" + "</tr>";
           };
           //Clear old table
           jQuery("#mainBody").empty();
           //Append new data
           jQuery(resultHtml).appendTo("#mainBody");
           jQuery("#no4").freezeHeader({ 'height': '390px'});
        }
    });
}

function setHeightBtnNewPJ(){
	var tableHeight = jQuery("#mainContent").height();
	$('#btnNewPJ').css('margin-top',tableHeight);
}