/*
 * jQuery TableFix plugin ver 1.0.1
 * Copyright (c) 2010 Otchy
 * This source file is subject to the MIT license.
 * http://www.otchy.net
 */
(function(jQuery){
	jQuery.fn.tablefix = function(options) {
		return this.each(function(index){
			// 陷�ｽｦ騾�ｿｽ�ｶ蜥擾ｽｶ螢ｹ�ｽ陋ｻ�､陞ｳ�ｽ
			var opts = jQuery.extend({}, options);
			var baseTable = jQuery(this);
			var withWidth = (opts.width > 0);
			var withHeight = (opts.height > 0);
			if (withWidth) {
				withWidth = (opts.width < baseTable.width());
			} else {
				opts.width = baseTable.width();
			}
			if (withHeight) {
				withHeight = (opts.height < baseTable.height());
			} else {
				opts.height = baseTable.height();
			}
			if (withWidth || withHeight) {
				if (withWidth && withHeight) {
					opts.width -= 40;
					opts.height -= 40;
				} else if (withWidth) {
					opts.width -= 20;
				} else {
					opts.height -= 20;
				}
			} else {
				return;
			}
			// 陞溷､慚�div 邵ｺ�ｮ髫ｪ�ｭ陞ｳ�ｽ
			baseTable.wrap("<div></div>");
			var div = baseTable.parent();
			div.css({position: "relative"});
			// 郢ｧ�ｹ郢ｧ�ｯ郢晢ｽｭ郢晢ｽｼ郢晢ｽｫ鬩幢ｽｨ郢ｧ�ｪ郢晁ｼ斐◎郢晢ｿｽ繝ｨ邵ｺ�ｮ陷ｿ髢�ｽｾ�ｽ
			var fixRows = (opts.fixRows > 0) ? opts.fixRows : 0;
			var fixCols = (opts.fixCols > 0) ? opts.fixCols : 0;
			var offsetX = 0;
			var offsetY = 0;
			baseTable.find('tr').each(function(indexY) {
				jQuery(this).find('td,th').each(function(indexX){
					if (indexY == fixRows && indexX == fixCols) {
						var cell = jQuery(this);
						offsetX = cell.position().left;
						offsetY = cell.parent('tr').position().top;
						return false;
					}
				});
				if (indexY == fixRows) {
					return false;
				}
			});
			// 郢晢ｿｽ�ｽ郢晄じﾎ晉ｸｺ�ｮ陋ｻ�ｽ迚｡邵ｺ�ｨ陋ｻ譎�ｄ陋ｹ�ｽ
			var crossTable = baseTable.wrap('<div></div>');
			var rowTable = baseTable.clone().wrap('<div></div>');
			var colTable = baseTable.clone().wrap('<div></div>');
			var bodyTable = baseTable.clone().wrap('<div></div>');
			var crossDiv = crossTable.parent().css({position: "absolute", overflow: "hidden"});
			var rowDiv = rowTable.parent().css({position: "absolute", overflow: "hidden"});
			var colDiv = colTable.parent().css({position: "absolute", overflow: "hidden"});
			var bodyDiv = bodyTable.parent().css({position: "absolute", overflow: "auto"});
			div.append(rowDiv).append(colDiv).append(bodyDiv);
			// 郢ｧ�ｯ郢晢ｽｪ郢晢ｿｽ�ｽ鬯�ｼ懈ｲｺ邵ｺ�ｮ髫ｪ�ｭ陞ｳ�ｽ
			var bodyWidth = opts.width - offsetX;
			var bodyHeight = opts.height - offsetY;
			crossDiv.width(offsetX);
			crossDiv.css('height',offsetY);
			rowDiv
				.width(bodyWidth + (withWidth ? 20 : 0) + (withHeight ? 20 : 0));
			rowDiv.css({left: offsetX + 'px'});
			rowDiv.height(offsetY)
			rowDiv.css('height',offsetY);
			rowTable.css({
				marginLeft: -offsetX + 'px',
				marginRight: (withWidth ? 20 : 0) + (withHeight ? 20 : 0) + 'px'
			});
			colDiv.width(offsetX);
			colDiv.css({top: offsetY + 'px'});
			rowDiv.css('height',bodyHeight + (withWidth ? 20 : 0) + (withHeight ? 20 : 0));
			colTable.css({
				marginTop: -offsetY + 'px',
				marginBottom: (withWidth ? 20 : 0) + (withHeight ? 20 : 0) + 'px'
			});
			bodyDiv.width(bodyWidth + (withWidth ? 20 : 0) + (withHeight ? 20 : 0));
			bodyDiv.css({left: offsetX + 'px', top: offsetY + 'px'});
			bodyDiv.css('height',bodyHeight + (withWidth ? 20 : 0) + (withHeight ? 20 : 0));
			bodyTable.css({
				marginLeft: -offsetX + 'px',
				marginTop: -offsetY + 'px',
				marginRight: (withWidth ? 20 : 0) + 'px',
				marginBottom: (withHeight ? 20 : 0) + 'px'
			});
			if (withHeight) {
				rowTable.width(bodyTable.width());
			}
			// 郢ｧ�ｹ郢ｧ�ｯ郢晢ｽｭ郢晢ｽｼ郢晢ｽｫ鬨ｾ�｣陷搾ｿｽ
			bodyDiv.scroll(function() {
				rowDiv.scrollLeft(bodyDiv.scrollLeft());
				colDiv.scrollTop(bodyDiv.scrollTop());
			});
			// 陞溷､慚�div 邵ｺ�ｮ髫ｪ�ｭ陞ｳ�ｽ
			div
				.width(opts.width + (withWidth ? 20 : 0) + (withHeight ? 20 : 0));
			div.css('height',opts.height + (withWidth ? 20 : 0) + (withHeight ? 20 : 0));
		});
	}
})(jQuery);