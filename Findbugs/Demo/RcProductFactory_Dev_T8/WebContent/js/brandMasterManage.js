// BOE #6344 No.3 Thai.Son 2014/02/18 Change to kendo 
// Max length constants.
var NUMERIC_MIN = 0;
var MAX_INT_MYSQL = 2147483647;
var MAX_LENGTH_INT_MYSQL = 10;
//Filter bar
/* BOE add style="border-width: 0 0 0px 0px;" to fix layout input date Le.Dinh 2014/01/16 #B07 */
// BOE hoang.ho 2014/04/11 fix change person in charge to combobox
var filterRow;
function initFilterRow() {
	filterRow = $(
        '<tr>'
            + '<td><input class="tbSearch k-input" type="text" class="filter" id="tbBrandCode" tabindex="1" maxlength="8" /></td>'
            + '<td><input class="tbSearch k-textbox" type="text" class="filter" id="tbName" tabindex="2" maxlength="50"/></td>'
            + '<td><input class="tbSearch k-textbox" type="text" class="filter" id="tbHyoujiName1" tabindex="3" maxlength="50"/></td>'
            + '<td><input class="tbSearch k-textbox" type="text" class="filter" id="tbHyoujiName2" tabindex="4" maxlength="50"/></td>'
            + '<td><input class="tbSearch k-textbox" type="text" class="filter" id="tbHyoujiName3" tabindex="5" maxlength="50"/></td>'
            + '<td>'
            +       $("#hiddenlistCommonUser").val()
            + '</td>'
            + '<td><select class="tbSearch cbbYesNo" id="listDelFlg" tabindex="7" style="width: 70px;padding: 0px;">'
            // BOE hoang.ho 2014/04/11 fix bug change option All to Blank
            + '<option value="-1"></option>'
            // EOE hoang.ho 2014/04/11 fix bug change option All to Blank
            + '<option value="0">Yes</option>'
            + '<option value="1">No</option>'
            + '</select></td>'
            
            + "<td>"
            + "<div>"
            +   "<div>"
            +       '<span class="dateTimeLabel">From</span>'
            +       '<span>'
            +           '<input class="tbSearch haft-size filter" type="search" id="tbKousinDateFrom" tabindex="8"/>'
            +       '</span>'
            +   "</div>"
            +   "<div>"
            +       '<span class="dateTimeLabel">To</span>'
            +       '<span>'
            +           '<input class="tbSearch haft-size filter" type="search" id="tbKousinDateTo" tabindex="9"/>'
            +       '</span>'
            +   "</div>"
            + "</div>"
            + "</td>"
            
        + '</tr>');
}
// EOE hoang.ho 2014/04/11 fix change person in charge to combobox
// EOE #6344 No.3 Thai.Son 2014/02/18 Change to kendo 
/* EOE add style="border-width: 0 0 0px 0px;" to fix layout input date Le.Dinh 2014/01/16 #B07 */
//if switch page, isNewPage = true
var currentPage = 1;
var dataSource;

//Search condition
var brandCode = "";
var name = "";
var hyoujiName1 = "";
var hyoujiName2 = "";
var hyoujiName3 = "";
var kousinUserName = "";
var delFlg = "0";
var kousinDateFrom = "";
var kousinDateTo = "";
var sortResetFlg = 0;

//Load data for grid
/* BOE change width to % to fix layout Le.Dinh 2014/01/16 #B07 */
function loadGridData() {
	var grid = $("#grid").kendoGrid({
		dataSource: dataSource,
        sortable: {
            mode: "single",
            allowUnsort: false
        },
	    pageable: {
	        refresh: true,
	        pageSizes: [50, 100],
	        buttonCount: 5,
	        messages: {
	          display:         jQuery("#pagingDisplay").val(),
	          empty:           jQuery("#pagingEmpty").val(),
	          itemsPerPage:    jQuery("#pagingItemsPerPage").val(),
	          first:           jQuery("#pagingFirst").val(),
	          previous:        jQuery("#pagingPrevious").val(),
	          next:            jQuery("#pagingNext").val(),
	          last:            jQuery("#pagingLast").val()
	        }
	    },
	    columns: [{ 
	  	          field:"brandCode",
	  	          type: "number",
	  	          title: jQuery("#titleBrandCode").val(),
	  	          width: "95px", 
	  	          attributes: {"class": "align-right"} },
	  	        { 
	  	          field:"name",
	  	          type: "string",
	  	          title: jQuery("#titleName").val(),
	  	          width: "145px",
	  	          attributes: {"class": "align-left"}
	  	        },
	  	        { 
	  	          field:"hyoujiName1",
	  	          type: "string",
	  	          title: jQuery("#titleHyoujiName1").val(),
	  	          width: "95px",
	  	          attributes: {"class": "align-left"}
	  	        },
	  	        { 
	  	          field:"hyoujiName2",
	  	          type: "string",
	  	          title: jQuery("#titleHyoujiName2").val(),
	  	          width: "95px",
	  	          attributes: {"class": "align-left"}
	  	        },
	  	        { 
	  	          field:"hyoujiName3",
	  	          type: "string",
	  	          title: jQuery("#titleHyoujiName3").val(),
	  	          width: "95px",
	  	          attributes: {"class": "align-left"}
	  	        },
	  	        { 
	  	          field:"kousinUserName",
	  	          type: "string",
	  	          title: jQuery("#titleUser").val(),
	  	          width: "160px",
	  	          attributes: {"class": "align-center"}
	  	        },
	  	        { 
	  	          field: "delFlgString",
	  	          type: "string",
	  	          title: jQuery("#titleDelFlg").val(),
	  	          width: "83px",
	  	          attributes: {"class": "align-center"}
	  	        },
	  	        { 
	  	          field:"kousinDateString",
	  	          type: "date",
	  	          title: jQuery("#titleKousinDate").val(),
	  	          // BOE #6634 No.21 Hoang.Ho 2014/02/18 enlarge with
	  	          width: "250px",
	  	          // EOE #6634 No.21 Hoang.Ho 2014/02/18 enlarge with
	  	          attributes: {"class": "align-center"}
	  	        }],
        height: 540,
        selectable: true,
	    resizable : true,
	    dataBound: function(e){
	    	//Remove noData message
	    	jQuery(".dataTables_empty").remove();
	    	if (this.dataSource.total() > 0) {
	    		setCurrentSearchCondition();
		    	var page = this.dataSource.page();
		    	
				if(page != currentPage){
					currentPage = page;
					this.content.scrollTop(0);
				}
				
				//Add title for row
				var gridData = this.dataSource.view();
				var gridLength = gridData.length;
				for (var i = 0; i < gridLength; i++) {
			    	var currentUid = gridData[i].uid;
			    		
			    	var currenRow = this.table.find("tr[data-uid='" + currentUid + "']");
			    	$(currenRow).attr("title", jQuery("#hostAddress").val() + "/editBrand?brandCode=" + gridData[i].brandCode);
			    }
		    } else {
		    	//Show no data message
		    	jQuery(".k-grid-content").append('<div class="dataTables_empty">' + jQuery("#noDataMessage").val() + '</div>');
		    }
	    },
    });
	
	return grid;
}

//Set data filter condition to sent to server
function setDataForDataSource(sortField, sortDir) {
	var brandCodeInt = 0;
	if (validateNumberic(brandCode)){
		brandCodeInt = parseInt(brandCode);
	}
	
    return {
        "brandMasterManagerActionForm.entMstBrand.brandCode" :      brandCodeInt,
        "brandMasterManagerActionForm.entMstBrand.name" :           name,
        "brandMasterManagerActionForm.entMstBrand.hyoujiName1" :    hyoujiName1,
        "brandMasterManagerActionForm.entMstBrand.hyoujiName2" :    hyoujiName2,
        "brandMasterManagerActionForm.entMstBrand.hyoujiName3" :    hyoujiName3,
        "brandMasterManagerActionForm.entMstBrand.kousinUserName" : kousinUserName,
        "brandMasterManagerActionForm.entMstBrand.delFlg" :         delFlg,
        "brandMasterManagerActionForm.entMstBrand.kousinDateFrom" : kousinDateFrom,
        "brandMasterManagerActionForm.entMstBrand.kousinDateTo" :   kousinDateTo,
        "brandMasterManagerActionForm.entMstBrand.sortField"    :   sortField,
        "brandMasterManagerActionForm.entMstBrand.sortDir"      :   sortDir,
    };
}

//Get filter condition from filter bar
function getSearchCondition() {
	brandCode = jQuery("#tbBrandCode").val();
    name = jQuery("#tbName").val();
    hyoujiName1 = jQuery("#tbHyoujiName1").val();
    hyoujiName2 = jQuery("#tbHyoujiName2").val();
    hyoujiName3 = jQuery("#tbHyoujiName3").val();
    kousinUserName = jQuery("#tbUser").val();
    delFlg = jQuery("#listDelFlg").val();
    kousinDateFrom = jQuery("#tbKousinDateFrom").val();
    kousinDateTo = jQuery("#tbKousinDateTo").val();
    
    //Validate and trim value
    if (brandCode) {
    	brandCode = brandCode.trim();
    }
    if (name) {
    	name = name.trim();
    }
    if (hyoujiName1) {
    	hyoujiName1 = hyoujiName1.trim();
    }
    if (hyoujiName2) {
    	hyoujiName2 = hyoujiName2.trim();
    }
    if (hyoujiName3) {
    	hyoujiName3 = hyoujiName3.trim();
    }
    if (kousinUserName) {
    	kousinUserName = kousinUserName.trim();
    }
    if (delFlg) {
    	delFlg = delFlg.trim();
    }
    if (kousinDateFrom) {
    	kousinDateFrom = kousinDateFrom.trim();
    }
    if (kousinDateTo) {
    	kousinDateTo = kousinDateTo.trim();
    }
}

//set search condition to filter bar
function setCurrentSearchCondition() {
	jQuery("#tbBrandCode").val(brandCode);
    jQuery("#tbName").val(name);
    jQuery("#tbHyoujiName1").val(hyoujiName1);
    jQuery("#tbHyoujiName2").val(hyoujiName2);
    jQuery("#tbHyoujiName3").val(hyoujiName3);
    jQuery("#tbUser").val(kousinUserName);
    //If delFlg != null
    if (delFlg) {
    	jQuery("#listDelFlg").val(delFlg);
    } else {
    	//DelFlg = null: Set value is All
    	jQuery("#listDelFlg").val(" ");
    }
    jQuery("#tbKousinDateFrom").val(kousinDateFrom);
    jQuery("#tbKousinDateTo").val(kousinDateTo);
}
//Back to the previous screen
//If doesn't exists, go to Top page
function backToPreviousPage() {
	//BOE THAI.SON : CLICK BACK --> DASHBOARD
	/*//Get previous URL
	var previousUrl = document.referrer;
	//Validate previousUrl
    if("" != previousUrl) {
    	//Go to previous page
        window.location = previousUrl;
        return;
    }*/
    //EOE THAI.SON.
    //Go to Top page
	/* BOE by Luong.Dai show progress bar at 2014/04/01 */
	showProcessBar();
	/* EOE by Luong.Dai show progress bar at 2014/04/01 */
    window.location = defaultRedirectPage;
}

//Reset all filter
function resetSearchCondition() {
	//clear all textbox
	jQuery("input.tbSearch").val('');
	
	//reset status list
	setSelectedValue("Yes");
}

//Set selected for list status
//Value = 0: Yes
//Value = 1: No
function setSelectedValue(value) {
	$("select option").filter(function() {
	    //may want to use $.trim in here
	    return $(this).text() == value; 
	}).prop('selected', true);
}

/* BOE add auto format yyyy/mm/dd and compare two date for input date Le.Dinh 2014/01/16 #B07 */
//Validate and compare and auto format date
function validateTextboxKousinDate(textBoxId){
    //Format date to YYYY/MM/DD
    jQuery("#" + textBoxId).val(formatDateWithSlash(jQuery("#" + textBoxId).val()));
    //Value of validate date
    var validateDateValue = validateDate(jQuery("#" + textBoxId).val());
    //Value of compare fromDate and toDate
    var compareTwoDateValue = compareTwoDate(jQuery("#tbKousinDateFrom").val(), jQuery("#tbKousinDateTo").val());
    //If validate true and compare date true
    if(validateDateValue == 1 && compareTwoDateValue != '-1') {
      //Valid date
        jQuery("#" + textBoxId).removeClass("error");
        return true;
    } 
    //Invalid date
    jQuery("#" + textBoxId).addClass("error");
    return false;
}
/* EOE add auto format yyyy/mm/dd and compare two date for input date Le.Dinh 2014/01/16 #B07 */

function validateKousinDate() {
	return validateTextboxKousinDate("tbKousinDateFrom") && validateTextboxKousinDate("tbKousinDateTo");
}

jQuery(document).ready(function() {
	initFilterRow();
    //dataSource for brand master grid
    dataSource = new kendo.data.DataSource({
        serverPaging: true,
        autoBind: false,
        serverSorting: true,
        // 50 product in 1 page
        pageSize: 50,
        transport: {
            read: {
                type: "POST",
                dataType: "json",
                url: "brandMasterManagerFilter.html",
                data:  function (data) {
                    var sortField = "brandCode";
                    var sortDir = "asc";
                    if (sortResetFlg != 1) {
                        if (data != undefined && data.sort != undefined && data.sort.length > 0) {
                            sortField = data.sort[0].field;
                            sortDir = data.sort[0].dir;
                        }
                    } else {
                        if (data != undefined && data.sort != undefined && data.sort.length > 0) {
                            data.sort[0].field = '';
                            data.sort[0].dir = '';
                        }
                    }
                    return setDataForDataSource(sortField, sortDir);
                },
                beforeSend :function() {
                	//Hide processing of Kendo.
                    jQuery("#grid .k-loading-image").css("background-image", "none");
                    // show process bar before send request.
                    showProcessBar();
                },
                complete: function() {
                    // hide process bar that the request succeeded
                    hideProcessBar();
                    sortResetFlg = 0;
                },
                error: function() {
                    // hide process bar that the request failed
                    hideProcessBar();
                    sortResetFlg = 0;
                },
                cache: false,
            }
        },
        schema: {
            data: "listBrand",
            total: "count",
        }
    });
    
    var grid = loadGridData();
    
    //Add filter
    grid.data("kendoGrid").thead.append(filterRow);
    //Set default DelFlg = Yes
    setSelectedValue("Yes");
    /*BOE  Thai.Son 2014/01/17*/
    // BOE #6344 No.5 Thai.Son 2014/02/18 Change to kendoNumeric 
    $('#listDelFlg').kendoDropDownList();
    // BOE hoang.ho 2014/04/11 Change persion in charge to kendo combobox
    $("#tbUser").kendoComboBox();
    // EOE hoang.ho 2014/04/11 Change persion in charge to kendo combobo
    jQuery("#tbBrandCode").kendoNumericTextBox({
		value : "",
		format: "#",
        decimals: 0,
		min : NUMERIC_MIN,
		max : MAX_INT_MYSQL,
		step : 1
	});
	// BOE #6344 No.5 Thai.Son 2014/02/18 Change to kendoNumeric 
    /*EOE  Thai.Son 2014/01/17*/
    /* BOE add datepicker for input date Le.Dinh 2014/01/16 #B07 */
    //Add datePicker for updateOn textBox.
    kendo.culture("ja-JP");
    //kendo for updateOn From textBox.
    jQuery("#tbKousinDateFrom").kendoDateTimePicker({
        interval: 60,
        format: "yyyy/MM/dd HH:mm:ss",
        timeFormat: "HH:mm:ss",
    });
    //Set value kendo object.
    updateOnFromKendo = jQuery("#tbKousinDateFrom").data("kendoDateTimePicker");
    
    //kendo for updateOn To textBox.
    jQuery("#tbKousinDateTo").kendoDateTimePicker({
        interval: 60,
        format: "yyyy/MM/dd HH:mm:ss",
        timeFormat: "HH:mm:ss",
    });
  //Set value kendo object.
    updateOnToKendo = jQuery("#tbKousinDateTo").data("kendoDateTimePicker");
    
    // Handle datetime picker validation.
    handleDateTimePicker();
    
    // Validate and compare updateOnDate.
    function validateDateTextBox(textBoxId) {
    	// Format date to YYYY/MM/DD
    	jQuery("#" + textBoxId).val(
    			formatDateWithSlash(jQuery("#" + textBoxId).val()));
    	// Value of validate date
    	var validateDateValue = validateDate(jQuery("#" + textBoxId).val());
    	// Value of compare fromDate and toDate
    	var compareTwoDateValue = compareTwoDate(jQuery("#tbKousinDateFrom").val(),
    			jQuery("#tbKousinDateTo").val());
    	// If validate true and compare date true
    	if (validateDateValue == 1 && compareTwoDateValue != '-1') {
    		// Valid date
    		jQuery("#" + textBoxId).removeClass("error");
    		return true;
    	}
    	// Invalid date
    	jQuery("#" + textBoxId).addClass("error");
    	return false;
    };
    
    /* EOE add datepicker for input date Le.Dinh 2014/01/16 #B07 */
    
        
    //Click button Back
    jQuery("#btnBack").click(function() {
        backToPreviousPage();
    });
    
    //Click button reset
    jQuery("#btnResetFilter").click(function() {
        //Set sort reset
        sortResetFlg = 1;
        resetSearchCondition();
        jQuery("#btnSearch").click();
    });
    //Button Search click
    jQuery("#btnSearch").click(function() {
        /*var valid = validateKousinDate();*/
        var valid = true;
        if (valid == true){
            getSearchCondition();
            //Reload data and goto first page
            grid.data("kendoGrid").dataSource.page(1);

            //Go to top of grid
            grid.data("kendoGrid").content.scrollTop(0);
        }
    });
    //Press enter key in textbox filter
    jQuery(document).on("keypress", "input.tbSearch", function(e){ 
    	if (e.keyCode == 13) {
    		jQuery("#btnSearch").click();
        }
    });
    // BOE hoang.ho 2014/04/11 fix bug change person in charge to combobox
    //Change DelFlg value
    jQuery("#listDelFlg, #tbUser").change(function() {
        jQuery("#btnSearch").click();
    });
    // BOE hoang.ho 2014/04/11 fix bug change person in charge to combobox
    
    //Validate From search value of kousinDate
    /*jQuery("#tbKousinDateFrom").focusout(function(){
        validateTextboxKousinDate("tbKousinDateFrom");
    });
    
    //Validate To search value of kousinDate
    jQuery("#tbKousinDateTo").focusout(function(){
        validateTextboxKousinDate("tbKousinDateTo");
    });*/

    // set defalt focus for page.
    // It have to put to the end of the document ready.
    $("#tbBrandCode").focus();
    // event for tabindex
    $("#btnAddNewBrand").keydown(function(e) {
        if (e.keyCode == 9) {
            e.preventDefault();
                $("#tbBrandCode").focus();
    	}
    });
    
    // double click to row in grid then redirect to edit page.
    $("#grid").on("dblclick", "tr.k-state-selected", function () {
    	/* BOE by Luong.Dai show progress bar at 2014/04/02 */
    	showProcessBar();
    	/* EOE by Luong.Dai show progress bar */
        var brandCode = $(this).find("td:first").text();
        // Redirect to edit brand page
        window.location = "./editBrand.html?brandCode=" + brandCode;
    });
    
    /**
     * Handle datetime picker.
     * @param selector string
     * @string boolean
     */
    function handleDateTimePicker() {
        // Handle focusout event.
        $("#tbKousinDateFrom").focusout(function() {
        	validateDateTimePicker('#tbKousinDateFrom', '#tbKousinDateFrom', '#tbKousinDateTo');
        });
        $("#tbKousinDateTo").focusout(function() {
        	validateDateTimePicker('#tbKousinDateTo', '#tbKousinDateFrom', '#tbKousinDateTo');
        });
    }
});