$(window).load(function(){
	/**
	 * BOF Handle keyup tab-key on field filter
	 * @rcv!cao.lan 2014/03/10
	 * **/
	$('.k-grid-header-wrap').keyup(function(e) {
	    var code = e.keyCode || e.which;
	    // code = 9 is tab-key
	    if (code == '9') {
	    	// get scroll left in grid header 
	    	var left = $('.k-grid-header-wrap').scrollLeft();
	    	// if tab-key and scroll left change then scroll left k-grid-content
    		$(".k-grid-content").scrollLeft(left);
	    }
	 });
	
	/** EOF @rcv!cao.lan 2014/03/10 **/
});
//Filter bar
var filterRow;
// DataPicker kendo object.
var updateOnFromKendo;
var updateOnToKendo;
// if switch page, isNewPage = true
var currentPage = 1;
var dataSource;

//Max,min length search condition
var maxLengthNumeric = 99999999;
var minLengthNumeric = 0;

// Search condition
var matterNo = "";
var matterRedmineIssueId = "";
var matterName = "";
var matterChargeUserId = "";
var matterRegistrationFlg = "";
var matterFactoryCountFrom = "";
var matterFactoryCountTo = "";
var matterFactoryErrorCountGeneralFrom = "";
var matterFactoryErrorCountGeneralTo = "";
var matterFactoryErrorCountCategoryFrom = "";
var matterFactoryErrorCountCategoryTo = "";
var matterFactoryErrorCountModelFrom = "";
var matterFactoryErrorCountModelTo = "";
var matterFactoryErrorCountAttributeFrom = "";
var matterFactoryErrorCountAttributeTo = "";
var updatedOnFrom = "";
var updatedOnTo = "";

var sortResetFlg = 0;

// Load data for grid
function loadGridData() {
	var matterRegistrationFlg1 = jQuery("#matterRegistrationFlg1").val();
	var matterRegistrationFlg2 = jQuery("#matterRegistrationFlg2").val();
	var matterRegistrationFlg3 = jQuery("#matterRegistrationFlg3").val();
//	var selectedKendoClass = "k-alt k-state-selected";
//	var selectedRowClass = "selectedGridRow";
	function handleClickingRow() {
	    gridSingleClick("#grid", "#selectAllCheckbox", ".matterCheckbox");
		$("#grid .k-grid-content tr").on({
//			click: function(e) {
//				// BOE #6634 No.50 Hoang.Ho 2014/02/19 Change selected row fllow by kendo-UI
//				var clsClick = e.target.className;
//				// flag true stand for check box has clicked.
//				var chkClickFlag = (clsClick != undefined) && (clsClick.indexOf("matterCheckbox") > -1);				
////			    handleSelectMultiRow(this, ".matterCheckbox", selectedKendoClass, selectedRowClass);
//				if (!chkClickFlag) {					
//					var currentChkItem = $(this).find(".matterCheckbox");
//					var state =currentChkItem.prop("checked");
//					currentChkItem.prop("checked", !state);
//				}
//				// EOE #6634 No.50 Hoang.Ho 2014/02/19  Change selected row fllow by kendo-UI
//			},
			dblclick: function() {
				/* BOE by Luong.Dai show progress bar when redirect page at 2014/04/01 */
				showProcessBar();
				/* EOE by Luong.Dai show progress bar when redirect page */
//				var matterId = $(this).find("td:nth-child(2)").text();
				var matterId = $(this).find(".matterCheckbox").attr("id");
				window.location = "./matterDetail.html?matterNo=" + matterId;
			}
		});
	}

	var grid = $("#grid")
			.kendoGrid(
					{
						dataSource : dataSource,
						sortable : {
							mode : "single",
							allowUnsort : false
						},
						// BOE #6634 No.50 Hoang.Ho 2014/02/19 Change selected row fllow by kendo-UI
                        selectable: "multiple row",
        				// EOE #6634 No.50 Hoang.Ho 2014/02/19 Change selected row fllow by kendo-UI
						change: function(e) {
							// Empty.
						},
						pageable : {
							refresh : true,
							pageSizes : [ 50, 100 ],
							buttonCount : 5,
							messages : {
								display : jQuery("#pagingDisplay").val(),
								empty : jQuery("#pagingEmpty").val(),
								itemsPerPage : jQuery("#pagingItemsPerPage")
										.val(),
								first : jQuery("#pagingFirst").val(),
								previous : jQuery("#pagingPrevious").val(),
								next : jQuery("#pagingNext").val(),
								last : jQuery("#pagingLast").val()
							}
						},
						resizable: true,
						columns : [
								{
									/*BOE dang.nam 16/6/2014 change class name*/
									/*title : "<input type='checkbox' id='selectAllCheckbox'/>",*/
									title : "<input type='checkbox' class='matterCheckbox' id='selectAllCheckbox'/>",
									/*EOE dang.nam 16/6/2014 change class name*/
									template : "<input type='checkbox' name='matterCheckbox' class='matterCheckbox' id='#= matterNo #'  value='#= updatedOn #'/>",
									attributes : {
										"class" : "align-center"
									},
									width : "30px"
								},
								// 1
								{
									field : "matterNo",
									type : "number",
									title : jQuery("#titleMatterNo").val(),
									width : "100px",
									attributes : {"class" : "align-right"},
									template : "#= kendo.toString(matterNo, 'n0' ) #"
								},
								// 2
								{
									field : "matterRedmineIssueId",
									type : "string",
									title : jQuery("#titleMatterRedmineIssueId")
											.val(),
									width : "100px",
									attributes : {"class" : "align-right"},
									//template : "#= kendo.toString(matterRedmineIssueId, 'n0' ) #"
								},
								// 3
								{
									field : "matterName",
									type : "string",
									title : jQuery("#titleMatterName").val(),
									width : "250px",
									attributes : {
										"class" : "align-left"
									}
								},
								// 4
								{
									field : "managerName",
									type : "number",
									title : jQuery("#titleMatterChargeUserId")
											.val(),
									width : "180px",
									attributes : {
										"class" : "align-center"
									}
								},
								// 5
								{
									template : "#if(matterRegistrationFlg == 1){# <span class='rowGray'>"
											+ matterRegistrationFlg1
											+ "</span> #} "
											+ "else if (matterRegistrationFlg == 0 && "
											+ "(matterFactoryErrorCountGeneral > 0 ||"
											+ "matterFactoryErrorCountCategory > 0 ||"
											+ "matterFactoryErrorCountModel > 0 ||"
											+ "matterFactoryErrorCountAttribute > 0)) {# <span class='error'>"
											+ matterRegistrationFlg2
											+ "</span> #}"
											+ "else {# <span>"
											+ matterRegistrationFlg3
											+ "</span> #}#",
									field : "matterRegistrationFlg",
									type : "number",
									title : jQuery(
											"#titleMatterRegistrationFlg")
											.val(),
									width : "180px",
									attributes : {
										"class" : "align-center"
									}
								},
								// 6
								{
									field : "matterFactoryCount",
									type : "string",
									title : jQuery("#titleMatterFactoryCount")
											.val(),
									width : "150px",
									attributes : {"class" : "align-right"},
									template : "#= kendo.toString(matterFactoryCount, 'n0' ) #"
								},
								// 7
								{
									template : "#if(matterFactoryErrorCountGeneral > 0){# <span class='error'> #= matterFactoryErrorCountGeneral #</span> #} else{# #= matterFactoryErrorCountGeneral # #}#",
									field:"matterFactoryErrorCountGeneral",
									type : "string",
									title : jQuery(
											"#titleMatterFactoryErrorCountGeneral")
											.val(),
									width : "150px",
									attributes : {"class" : "align-right"},
									template : "#= kendo.toString(matterFactoryErrorCountGeneral, 'n0' ) #"
								},
								// 8
								{
									template : "#if(matterFactoryErrorCountCategory > 0){# <span class='error'> #= matterFactoryErrorCountCategory #</span> #} else{# #= matterFactoryErrorCountCategory # #}#",
									field:"matterFactoryErrorCountCategory",
									type : "string",
									title : jQuery(
											"#titleMatterFactoryErrorCountCategory")
											.val(),
									width : "150px",
									attributes : {"class" : "align-right"},
									template : "#= kendo.toString(matterFactoryErrorCountCategory, 'n0' ) #"
								},
								// 9
								{
									template : "#if(matterFactoryErrorCountModel > 0){# <span class='error'> #= matterFactoryErrorCountModel #</span> #} else{# #= matterFactoryErrorCountModel # #}#",
									field: "matterFactoryErrorCountModel",
									type : "string",
									title : jQuery(
											"#titleMatterFactoryErrorCountModel")
											.val(),
									width : "150px",
									attributes : {"class" : "align-right"},
									template : "#= kendo.toString(matterFactoryErrorCountModel, 'n0' ) #"
								},
								// 10
								{
									template : "#if(matterFactoryErrorCountAttribute > 0){# <span class='error'> #= matterFactoryErrorCountAttribute #</span> #} else{# #= matterFactoryErrorCountAttribute # #}#",
									field:"matterFactoryErrorCountAttribute",
									type : "string",
									title : jQuery(
											"#titleMatterFactoryErrorCountAttribute")
											.val(),
									width : "150px",
									attributes : {"class" : "align-right"},
									template : "#= kendo.toString(matterFactoryErrorCountAttribute, 'n0' ) #"
								},
								// 11
								{
									field : "updatedOn",
									type : "date",
									title : jQuery("#titleUpdateOn").val(),
									width : "250px",
									attributes : {
										"class" : "align-center"
									},
									// format:"{0:yyyy/MM/dd HH:mm:ss}",
									// template: "#=
									// kendo.toString(kendo.parseDate(updatedOn,
									// 'yyyy-MM-dd'), 'yyyy/MM/dd HH:mm:ss') #"
									//template : "#= kendo.toString(new Date(updatedOn), 'yyyy/MM/dd HH:mm:ss') #"
									template : function(a) {
										return a.updatedOnToString;
									}
								} ],
						height : 557,
						dataBound : function(e) {
							// Remove noData message
							jQuery(".dataTables_empty").remove();
							if (this.dataSource.total() > 0) {
								var grid = $("#grid").data("kendoGrid");
								// setCurrentSearchCondition();
								var page = this.dataSource.page();

								if (page != currentPage) {
									currentPage = page;
									this.content.scrollTop(0);
								}

								// Add title for row
								var gridData = this.dataSource.view();
								for ( var i = 0; i < gridData.length; i++) {
									var currentUid = gridData[i].uid;

									var currenRow = grid.table
											.find("tr[data-uid='" + currentUid
													+ "']");
									// $(currenRow).removeClass("k-alt");

									if (gridData[i].matterRegistrationFlg == "1") {
										// Row error
										$(currenRow).addClass("rowGray");
									}
								}
							} else {
								// Show no data message
								jQuery(".k-grid-content").append(
										'<div class="dataTables_empty">'
												+ jQuery("#noDataMessage")
														.val() + '</div>');
							}

							// Select all processing.
							handleSelectAll("#selectAllCheckbox", "#grid table tbody tr", ".matterCheckbox", selectedKendoClass, selectedRowClass);
							// 
//							$("#selectAllCheckbox").click(function () {
//								var currentState = $(this).prop("checked");
//								$("#grid table tbody tr").find(".matterCheckbox").prop("checked", currentState);
//							});
							
							// Handle select multi rows.
							handleClickingRow();
						},
					});

	return grid;
}

// Set data filter condition to sent to server
function setDataForDataSource(sortField, sortDir) {
	return {
		// 1
		"matterManageActionForm.entMstFactoryMatterNew.matterNo" : matterNo,
		// 2
		"matterManageActionForm.entMstFactoryMatterNew.matterRedmineIssueId" : matterRedmineIssueId,
		// 3
		"matterManageActionForm.entMstFactoryMatterNew.matterName" : matterName,
		// 4
		"matterManageActionForm.entMstFactoryMatterNew.matterChargeUserId" : matterChargeUserId,
		// 5
		"matterManageActionForm.entMstFactoryMatterNew.matterRegistrationFlg" : matterRegistrationFlg,
		// 6
		"matterManageActionForm.entMstFactoryMatterNew.matterFactoryCountFrom" : matterFactoryCountFrom,
		"matterManageActionForm.entMstFactoryMatterNew.matterFactoryCountTo" : matterFactoryCountTo,
		// 7
		"matterManageActionForm.entMstFactoryMatterNew.matterFactoryErrorCountGeneralFrom" : matterFactoryErrorCountGeneralFrom,
		"matterManageActionForm.entMstFactoryMatterNew.matterFactoryErrorCountGeneralTo" : matterFactoryErrorCountGeneralTo,
		// 8
		"matterManageActionForm.entMstFactoryMatterNew.matterFactoryErrorCountCategoryFrom" : matterFactoryErrorCountCategoryFrom,
		"matterManageActionForm.entMstFactoryMatterNew.matterFactoryErrorCountCategoryTo" : matterFactoryErrorCountCategoryTo,
		// 9
		"matterManageActionForm.entMstFactoryMatterNew.matterFactoryErrorCountModelFrom" : matterFactoryErrorCountModelFrom,
		"matterManageActionForm.entMstFactoryMatterNew.matterFactoryErrorCountModelTo" : matterFactoryErrorCountModelTo,
		// 10
		"matterManageActionForm.entMstFactoryMatterNew.matterFactoryErrorCountAttributeFrom" : matterFactoryErrorCountAttributeFrom,
		"matterManageActionForm.entMstFactoryMatterNew.matterFactoryErrorCountAttributeTo" : matterFactoryErrorCountAttributeTo,
		// 11
		"matterManageActionForm.entMstFactoryMatterNew.updatedOnFrom" : updatedOnFrom,
		"matterManageActionForm.entMstFactoryMatterNew.updatedOnTo" : updatedOnTo,
		// more
		"offset" : 1,
		"numberOfRow" : 10,
		"matterManageActionForm.entMstFactoryMatterNew.sortField" : sortField,
		"matterManageActionForm.entMstFactoryMatterNew.sortDir" : sortDir
	};
}

// Get filter condition from filter bar
function getSearchCondition() {
	// 1
	matterNo = jQuery("#matterNoSearch").val();
	// 2
	matterRedmineIssueId = jQuery("#matterRedmineIssueIdSearch").val();
	// 3
	matterName = jQuery("#matterNameSearch").val();
	// 4
	matterChargeUserId = jQuery("#lstUser").val();
	// 5
	// matterRegistrationFlg = jQuery("#cbbRegistrationFlg").val();
	var tmpRegisFlg = jQuery("#cbbRegistrationFlg").val();
	if (tmpRegisFlg == "FlgEqual1") {
		tmpRegisFlg = "1";
	} else if (tmpRegisFlg == "Larger0") {
		tmpRegisFlg = "2";
	} else if (tmpRegisFlg == "FlgEqual0") {
		tmpRegisFlg = "3";
	} else {
		tmpRegisFlg = "";
	}
	matterRegistrationFlg = tmpRegisFlg;
	//BOE Nguyen.Chuong 2014/03/11: fix bugs get wrong data.
	// 6
	matterFactoryCountFrom = jQuery("#matterFactoryCountSearchFrom").val();
//	if (matterFactoryCountFrom == "") {
//		matterFactoryCountFrom = minLengthNumeric;
//	}
	matterFactoryCountTo = jQuery("#matterFactoryCountSearchTo").val();
//	if (matterFactoryCountTo == "") {
//		matterFactoryCountTo = maxLengthNumeric;
//	}
	// 7
	matterFactoryErrorCountGeneralFrom = jQuery("#matterFactoryErrorCountGeneralSearchFrom").val();
//	if (matterFactoryErrorCountGeneralFrom == "") {
//		matterFactoryErrorCountGeneralFrom = minLengthNumeric;
//	}
	matterFactoryErrorCountGeneralTo = jQuery("#matterFactoryErrorCountGeneralSearchTo").val();
//	if (matterFactoryErrorCountGeneralTo == "") {
//		matterFactoryErrorCountGeneralTo = maxLengthNumeric;
//	}
	// 8
	matterFactoryErrorCountCategoryFrom = jQuery("#matterFactoryErrorCountCategorySearchFrom").val();
//	if (matterFactoryErrorCountCategoryFrom == "") {
//		matterFactoryErrorCountCategoryFrom = minLengthNumeric;
//	}
	matterFactoryErrorCountCategoryTo = jQuery("#matterFactoryErrorCountCategorySearchTo").val();
//	if (matterFactoryErrorCountCategoryTo == "") {
//		matterFactoryErrorCountCategoryTo = maxLengthNumeric;
//	}
	// 9
	matterFactoryErrorCountModelFrom = jQuery("#matterFactoryErrorCountModelSearchFrom").val();
//	if (matterFactoryErrorCountModelFrom == "") {
//		matterFactoryErrorCountModelFrom = minLengthNumeric;
//	}
	matterFactoryErrorCountModelTo = jQuery("#matterFactoryErrorCountModelSearchTo").val();
//	if (matterFactoryErrorCountModelTo == "") {
//		matterFactoryErrorCountModelTo = maxLengthNumeric;
//	}
	// 10
	matterFactoryErrorCountAttributeFrom = jQuery("#matterFactoryErrorCountAttributeSearchFrom").val();
//	if (matterFactoryErrorCountAttributeFrom == "") {
//		matterFactoryErrorCountAttributeFrom = minLengthNumeric;
//	}
	matterFactoryErrorCountAttributeTo = jQuery("#matterFactoryErrorCountAttributeSearchTo").val();
//	if (matterFactoryErrorCountAttributeTo == "") {
//		matterFactoryErrorCountAttributeTo = maxLengthNumeric;
//	}
	// 11
	updatedOnFrom = jQuery("#updatedOnFormSearch").val();
	updatedOnTo = jQuery("#updatedOnFormToSearch").val();

	// Validate and trim value
	// 3
	if (matterName) {
		matterName = matterName.trim();
	}
	// 4
	if (matterChargeUserId) {
		matterChargeUserId = matterChargeUserId.trim();
	}
	// 5
	if (matterRegistrationFlg) {
		matterRegistrationFlg = matterRegistrationFlg.trim();
	}
	// 11
	if (updatedOnFrom) {
		updatedOnFrom = updatedOnFrom.trim();
	}
	if (updatedOnTo) {
		updatedOnTo = updatedOnTo.trim();
	}

	return true;
}

// set search condition to filter bar
function setCurrentSearchCondition() {
	jQuery("#tbCode").val(attributeCode);
	jQuery("#tbname").val(attributeName);
	// If attributeType != null
	if (attributeType) {
		jQuery("#cbbListType").val(attributeType);
	} else {
		// attributeType = null: Set value is All
		jQuery("#cbbListType").val(" ");
	}
	jQuery("#tbLengthLimit").val(attributeLengthLimit);
	jQuery("#tbRegexp").val(attributeRegexp);
	// If attributeShowFlg != null
	if (attributeShowFlg) {
		jQuery("#listShowFlg").val(attributeShowFlg);
	} else {
		// listShowFlg = null: Set value is All
		jQuery("#listShowFlg").val(" ");
	}
	// If delFlg != null
	if (attributeDelFlg) {
		jQuery("#listDelFlg").val(attributeDelFlg);
	} else {
		// DelFlg = null: Set value is All
		jQuery("#listDelFlg").val(" ");
	}
	jQuery("#tbUpdateOnFrom").val(updatedOnFrom);
	jQuery("#tbUpdateOnTo").val(updatedOnTo);
}
// Back to the previous screen
// If doesn't exists, go to Top page
function backToPreviousPage() {
	//BOE THAI.SON FIX BUG "BACK TO SIDE MAP"
	/*// Get previous URL
	var previousUrl = document.referrer;
	// Validate previousUrl
	if ("" != previousUrl) {
		// Go to previous page
		// window.location = previousUrl;
		window.history.go(-1);
		return;
	}*/
	// Go to Top page
	window.location = defaultRedirectPage;
	//eOE THAI.SON FIX BUG "BACK TO SIDE MAP"
}

// Validate and compare updateOnDate.
function validateDateTextBox(textBoxId) {
	// Format date to YYYY/MM/DD
	jQuery("#" + textBoxId).val(
			formatDateWithSlash(jQuery("#" + textBoxId).val()));
	// Value of validate date
	var validateDateValue = validateDate(jQuery("#" + textBoxId).val());
	// Value of compare fromDate and toDate
	var compareTwoDateValue = compareTwoDate(jQuery("#tbUpdateOnFrom").val(),
			jQuery("#tbUpdateOnTo").val());
	// If validate true and compare date true
	if (validateDateValue == 1 && compareTwoDateValue != '-1') {
		// Valid date
		jQuery("#" + textBoxId).removeClass("error");
		return true;
	}
	// Invalid date
	jQuery("#" + textBoxId).addClass("error");
	return false;
};

function validateUpdateOnDate() {
	return validateDateTextBox('updatedOnFormSearch')
			&& validateDateTextBox('updatedOnFormToSearch');
}

// Validate date range
 function validateKousinDateRange() {
	var result = true;
	var fromDate = $("#updatedOnFormSearch").val();
	var toDate = $("#updatedOnFormToSearch").val();
	if (undefined != fromDate && "" != fromDate && undefined != toDate
			&& "" != toDate) {
		var dateFrom = new Date($("#updatedOnFormSearch").val());
		var dateTo = new Date($("#updatedOnFormToSearch").val());
		if (dateFrom > dateTo) {			
			result = false;
			$("#updatedOnFormSearch, #updatedOnFormToSearch").addClass("error");
		}
	}
	return result;
}
 
 function validateTextboxKousinDate(idValue) {
	 if ($("#" + idValue).val() == "") {
        jQuery("#" + idValue).removeClass("error");
        return true;
	}
	$("#" + idValue).val(formatDateTimeWithSlash($("#" + idValue).val()));
    if (!validateDateTime(jQuery("#" + idValue).val())) {
        //Invalid date
        jQuery("#" + idValue).addClass("error");
        return false;
    } else {
        //Valid date
        jQuery("#" + idValue).removeClass("error");
        return true;
    }
}
 
//Validate date
function validateKousinDate() {
     var result = true;
     if (!validateTextboxKousinDate("updatedOnFormSearch")) {
         result = false;
     }
     if (!validateTextboxKousinDate("updatedOnFormToSearch")) {
         result = false;
     }
     if (!result) {
         return false;
     }
     result = validateKousinDateRange();
     return result;
}

function deleteMatterByListMatterNo() {
	var arrayMatterId = "";
	var arrayUpdateOn = "";
	$('.matterCheckbox:checked').each(function() {
		var UpdateOn = $(this).val();

		var matterId = $(this).attr('id');
		arrayMatterId += matterId + "_";
		arrayUpdateOn += UpdateOn + "_";
		// $('.matterCheckbox:checked').
	});
	
	var sortField = "matterNo";
	var sortDir = "DESC";
	var dateTimeFilter = $(".dateTimeFilter").first().val();

	// load ajax delete recode
	$.ajax({
		type : 'POST',
		url : 'deleteMatterList.html',
		dataType : 'json',
		data : {
			'popupConfirm' : false,
			'arrayMatterId' : arrayMatterId,
			'arrayUpdateOn' : arrayUpdateOn,
			// 1
			"matterManageActionForm.entMstFactoryMatterNew.matterNo" : matterNo,
			// 2
			"matterManageActionForm.entMstFactoryMatterNew.matterRedmineIssueId" : matterRedmineIssueId,
			// 3
			"matterManageActionForm.entMstFactoryMatterNew.matterName" : matterName,
			// 4
			"matterManageActionForm.entMstFactoryMatterNew.matterChargeUserId" : matterChargeUserId,
			// 5
			"matterManageActionForm.entMstFactoryMatterNew.matterRegistrationFlg" : matterRegistrationFlg,
			// 6
			"matterManageActionForm.entMstFactoryMatterNew.matterFactoryCountFrom" : matterFactoryCountFrom,
			"matterManageActionForm.entMstFactoryMatterNew.matterFactoryCountTo" : matterFactoryCountTo,
			// 7
			"matterManageActionForm.entMstFactoryMatterNew.matterFactoryErrorCountGeneralFrom" : matterFactoryErrorCountGeneralFrom,
			"matterManageActionForm.entMstFactoryMatterNew.matterFactoryErrorCountGeneralTo" : matterFactoryErrorCountGeneralTo,
			// 8
			"matterManageActionForm.entMstFactoryMatterNew.matterFactoryErrorCountCategoryFrom" : matterFactoryErrorCountCategoryFrom,
			"matterManageActionForm.entMstFactoryMatterNew.matterFactoryErrorCountCategoryTo" : matterFactoryErrorCountCategoryTo,
			// 9
			"matterManageActionForm.entMstFactoryMatterNew.matterFactoryErrorCountModelFrom" : matterFactoryErrorCountModelFrom,
			"matterManageActionForm.entMstFactoryMatterNew.matterFactoryErrorCountModelTo" : matterFactoryErrorCountModelTo,
			// 10
			"matterManageActionForm.entMstFactoryMatterNew.matterFactoryErrorCountAttributeFrom" : matterFactoryErrorCountAttributeFrom,
			"matterManageActionForm.entMstFactoryMatterNew.matterFactoryErrorCountAttributeTo" : matterFactoryErrorCountAttributeTo,
			// 11
			"matterManageActionForm.entMstFactoryMatterNew.updatedOnFrom" : updatedOnFrom,
			"matterManageActionForm.entMstFactoryMatterNew.updatedOnTo" : updatedOnTo,
			// more
			"offset" : 1,
			"numberOfRow" : 10,
			"matterManageActionForm.entMstFactoryMatterNew.sortField" : sortField,
			"matterManageActionForm.entMstFactoryMatterNew.sortDir" : sortDir
		},
		 beforeSend : function(){
			 showProcessBar();
	    },
		success : function(data) {
			if (data == null) {
				// BOE hoang.ho 2014/04/12 fix bug message bar
				var message = $("#deleteSuccessFail").val();
				viewMessage(message, 1);
//				alert("Not Delete");
				// EOE hoang.ho 2014/04/12 fix bug message bar
				return;
			}
			
			/*console.log(data);*/

			// Exclusive error.
			var lastUpdatedUser = data.lastUpdatedUser;
			if (lastUpdatedUser != null) {
			    /*BOE #7331 Nguyen.Chuong 2014/07/01 change logic to not show message into message bar*/
				if (lastUpdatedUser.errorMessage != null) {
//					viewMessage(lastUpdatedUser.errorMessage, 1);
					/*BOE dang.nam 16/6/2014 show popupConfirm*/
	                $.when(kendo.ui.ExtOkCancelDialog.show({
	                    title: "",
	                    labelOK:"Ok",
	                    labelCancel:"Cancel",
	                    message: lastUpdatedUser.errorMessage,
	                    }).done(function (response) 
	                    {
	                        if (response.button == "OK") 
	                        {
	                            $.ajax({
	                                type : 'POST',
	                                url : 'deleteMatterList.html',
	                                dataType : 'json',
	                                data : {
	                                    'popupConfirm' : true,
	                                    'arrayMatterId' : arrayMatterId,
	                                    'arrayUpdateOn' : arrayUpdateOn,
	                                    // 1
	                                    "matterManageActionForm.entMstFactoryMatterNew.matterNo" : matterNo,
	                                    // 2
	                                    "matterManageActionForm.entMstFactoryMatterNew.matterRedmineIssueId" : matterRedmineIssueId,
	                                    // 3
	                                    "matterManageActionForm.entMstFactoryMatterNew.matterName" : matterName,
	                                    // 4
	                                    "matterManageActionForm.entMstFactoryMatterNew.matterChargeUserId" : matterChargeUserId,
	                                    // 5
	                                    "matterManageActionForm.entMstFactoryMatterNew.matterRegistrationFlg" : matterRegistrationFlg,
	                                    // 6
	                                    "matterManageActionForm.entMstFactoryMatterNew.matterFactoryCountFrom" : matterFactoryCountFrom,
	                                    "matterManageActionForm.entMstFactoryMatterNew.matterFactoryCountTo" : matterFactoryCountTo,
	                                    // 7
	                                    "matterManageActionForm.entMstFactoryMatterNew.matterFactoryErrorCountGeneralFrom" : matterFactoryErrorCountGeneralFrom,
	                                    "matterManageActionForm.entMstFactoryMatterNew.matterFactoryErrorCountGeneralTo" : matterFactoryErrorCountGeneralTo,
	                                    // 8
	                                    "matterManageActionForm.entMstFactoryMatterNew.matterFactoryErrorCountCategoryFrom" : matterFactoryErrorCountCategoryFrom,
	                                    "matterManageActionForm.entMstFactoryMatterNew.matterFactoryErrorCountCategoryTo" : matterFactoryErrorCountCategoryTo,
	                                    // 9
	                                    "matterManageActionForm.entMstFactoryMatterNew.matterFactoryErrorCountModelFrom" : matterFactoryErrorCountModelFrom,
	                                    "matterManageActionForm.entMstFactoryMatterNew.matterFactoryErrorCountModelTo" : matterFactoryErrorCountModelTo,
	                                    // 10
	                                    "matterManageActionForm.entMstFactoryMatterNew.matterFactoryErrorCountAttributeFrom" : matterFactoryErrorCountAttributeFrom,
	                                    "matterManageActionForm.entMstFactoryMatterNew.matterFactoryErrorCountAttributeTo" : matterFactoryErrorCountAttributeTo,
	                                    // 11
	                                    "matterManageActionForm.entMstFactoryMatterNew.updatedOnFrom" : updatedOnFrom,
	                                    "matterManageActionForm.entMstFactoryMatterNew.updatedOnTo" : updatedOnTo,
	                                    // more
	                                    "offset" : 1,
	                                    "numberOfRow" : 10,
	                                    "matterManageActionForm.entMstFactoryMatterNew.sortField" : sortField,
	                                    "matterManageActionForm.entMstFactoryMatterNew.sortDir" : sortDir
	                                },
	                                success : function(data){
	                                    if (data == null) {
	                                        var message = $("#deleteSuccessFail").val();
	                                        viewMessage(message, 1);
	                                        return;
	                                    }
	                                    var lastUpdatedUser = data.lastUpdatedUser;
	                                    if (lastUpdatedUser != null) {
	                                        if (lastUpdatedUser.errorMessage != null) {
	                                            viewMessage(lastUpdatedUser.errorMessage, 1);
	                                        } else {
	                                            var message = $("#otherUpdateMessage").val();
	                                            message = message.replace("{0}",
	                                                    lastUpdatedUser.managerName);
	                                            message = message.replace("{1}",
	                                                    lastUpdatedUser.updatedOnToString);
	                                            viewMessage(message, 1);
	                                        }
	                                    } else {
	                                        var message = $("#deleteSuccessMessage").val();
	                                        viewMessage(message, 0);
	                                    }
	                                    //Refresh Grid.
	                                    $("#btnSearch").click();
	                                }
	                            });
	                        }
	                    })
	                );
	                /*EOE dang.nam 16/6/2014 show popupConfirm*/
				} else {
					// invalid
					var message = $("#otherUpdateMessage").val();
					message = message.replace("{0}",
							lastUpdatedUser.managerName);
					message = message.replace("{1}",
							lastUpdatedUser.updatedOnToString);
					// BOE hoang.ho 2014/04/11 fix bug message bar
					/*$("#matterManageMessage").removeClass().addClass(
							"k-block k-error-colored").text(message).show();*/
					viewMessage(message, 1);
					// EOE hoang.ho 2014/04/11 fix bug message bar
				}
				/*EOE #7331 Nguyen.Chuong 2014/07/01 change logic to not show message into message bar*/
			} else {
				// valid
				var message = $("#deleteSuccessMessage").val();
				/*$("#matterManageMessage").removeClass().addClass(
						"k-block k-success-colored").text(message)
						.show();*/
				// BOE hoang.ho 2014/04/12 fix bug message bar
				viewMessage(message, 0);
				// EOE hoang.ho 2014/04/12 fix bug message bar
			}
			//Refresh Grid.
			$("#btnSearch").click();
		},
        complete : function(){
        	hideProcessBar();
        }
	});
}
// BOE hoang.ho 2014/04/11 fix bug message bar
function resetErrorMssagePanels() {
	// remove all class of msg div and clear msg
	$("#message").removeClass("has-message success");
	$("#message").removeClass("has-message msg-error");
	$("#message").text("");
	$("#message").hide();
}
//View message
function viewMessage(msg, msgCode) {
	// Remove message
	if (null == msg || "" == msg) {
		$("#message").text("");
		$("#message").removeAttr('class');
		$("#message").hide();
	} else {
		$("#message").text(msg);
		$("#message").addClass("has-message");
		// Message success
		if (msgCode == 0) {
			$("#message").addClass("success");
		} else {
			$("#message").addClass("msg-error");
		}
		$("#message").show();
	}
}

function Hello($scope, $http) {
    $http.get('http://localhost:8080/RcProductFactory/selectMatterManageList.html').
       success(function(data) {
           $scope.user = data.form.listAdmin;
       });
}

//EOE hoang.ho 2014/04/11 fix bug message bar
jQuery(document)
		.ready(
				function() {
					var phonecatApp = angular.module('phonecatApp', []);
					phonecatApp.controller('PhoneListCtrl', function () {
						Hello();
					});
					
					// Init something.
					resetErrorMssagePanels();
					initPleaseSelectDialog();
					initDeleteMatterDialog();

					// Create filter row in grid
					filterRow = $('<tr>'
							+ '<td></td>'
							/* BOE Luong.Dai 2014/04/11 chagne maxLength from 20 to 18*/
							+ '<td><input class="tbSearch k-input" id="matterNoSearch" maxLength="18"/></td>'
							/* EOE Luong.Dai 2014/04/11 chagne maxLength from 20 to 18*/
							+ '<td><input class="tbSearch k-input" id="matterRedmineIssueIdSearch" maxLength="8"/></td>'
							+ '<td><input class="tbSearch k-textbox" type="text" id="matterNameSearch"	maxLength="128"/></td>'
							+ '<td>'
							+    jQuery("#hiddenlistCommonUser").val()
							+ '</td>'
							+ '<td>'
							+    jQuery("#hiddenRegistrationFlg").val()
							+ '</td>'

							+ '<td><div>'
							+    '<span class="dateTimeLabel"> From </span>'
							+    '<input class="tbSearch k-input filterTextBox" type="search" id="matterFactoryCountSearchFrom" maxlength="8"/>'
							+    '</span></br>'
							+    '<span class="dateTimeLabel"> To </span>'
							+    '<span>'
							+    '<input class="tbSearch k-input filterTextBox" type="search" id="matterFactoryCountSearchTo" maxlength="8"/>'
							+    '</span>'
							+ '</div></td>'

							+ '<td><div>'
							+ '<span class="dateTimeLabel"> From </span>'
							+ '<input class="tbSearch  k-input filterTextBox" type="search" id="matterFactoryErrorCountGeneralSearchFrom" maxlength="8"/>'
							+ '</span></br>'
							+ '<span class="dateTimeLabel"> To </span>'
							+ '<span>'
							+ '<input class="tbSearch  k-input filterTextBox" type="search" id="matterFactoryErrorCountGeneralSearchTo" maxlength="8"/>'
							+ '</span>'
							+ '</div></td>'

							+ '<td><div>'
							+ '<span class="dateTimeLabel"> From </span>'
							+ '<input class="tbSearch k-input filterTextBox" type="search" id="matterFactoryErrorCountCategorySearchFrom" maxlength="8"/>'
							+ '</span></br>'
							+ '<span class="dateTimeLabel"> To </span>'
							+ '<span>'
							+ '<input class="tbSearch  k-input filterTextBox" type="search" id="matterFactoryErrorCountCategorySearchTo" maxlength="8"/>'
							+ '</span>'
							+ '</div></td>'

							+ '<td><div>'
							+ '<span class="dateTimeLabel"> From </span>'
							+ '<input class="tbSearch  k-input filterTextBox" type="search" id="matterFactoryErrorCountModelSearchFrom" maxlength="8"/>'
							+ '</span></br>'
							+ '<span class="dateTimeLabel"> To </span>'
							+ '<span>'
							+ '<input class="tbSearch  k-input filterTextBox" type="search" id="matterFactoryErrorCountModelSearchTo" maxlength="8"/>'
							+ '</span>'
							+ '</div></td>'

							+ '<td><div>'
							+ '<span class="dateTimeLabel"> From </span>'
							+ '<input class="tbSearch  k-input filterTextBox" type="search" id="matterFactoryErrorCountAttributeSearchFrom" maxlength="8"/>'
							+ '</span></br>'
							+ '<span class="dateTimeLabel"> To </span>'
							+ '<span>'
							+ '<input class="tbSearch  k-input filterTextBox" type="search" id="matterFactoryErrorCountAttributeSearchTo" maxlength="8"/>'
							+ '</span>'
							+ '</div></td>'

							+ '<td>'							
							+ "<div>"
							+ 	"<div>"
							+ 		'<span class="dateTimeLabel">From</span>'
							+ 		'<span>'
							+ 			'<input id="updatedOnFormSearch" type="text" class="tbSearch k-input filterTextBox" style="width: 180px;" />'
							+ 		'</span>'
							+ 	"</div>"
							+ 	"<div>"
							+ 		'<span class="dateTimeLabel">To</span>'
							+ 		'<span>'
							+ 			'<input id="updatedOnFormToSearch" type="text" class="tbSearch k-input filterTextBox" style="width: 180px;" />'
							+ 		'</span>'
							+ 	"<div>"
							+ "</div>"							
							+ '</td>' 
							
							+ '</tr>');
					// dataSource for attribute grid
					dataSource = new kendo.data.DataSource(
							{
								serverPaging : true,
								autoBind : false,
								// 50 product in 1 page
								pageSize : 50,
								serverSorting : true,
								transport : {
									read : {
										type : "POST",
										dataType : "json",
										url : "selectFilteredMatterManageList.html",
										data : function(data) {
											var sortField = "matterNo";
											/* BOE Fix bug #12 Changing initial sorting order @rcv!nguyen.hieu 2014/03/11. */
											//var sortDir = "asc";
											var sortDir = "DESC";
											/* EOE Fix bug #12 Changing initial sorting order. */
											if (sortResetFlg != 1) {
												if (data != undefined
														&& data.sort != undefined
														&& data.sort.length > 0) {
													sortField = data.sort[0].field;
													sortDir = data.sort[0].dir;
												}
											} else {
												if (data != undefined
														&& data.sort != undefined
														&& data.sort.length > 0) {
													data.sort[0].field = '';
													data.sort[0].dir = '';
												}
											}
											return setDataForDataSource(
													sortField, sortDir);
										},
										beforeSend : function() {
											// show process bar before send
											// request.
											jQuery("#selectAllCheckbox").prop("checked", false);
											showProcessBar();
										},
										complete : function() {
											// hide process bar that the request
											// succeeded
											hideProcessBar();
											sortResetFlg = 0;
										},
										error : function() {
											// hide process bar that the request
											// failed
											hideProcessBar();
											sortResetFlg = 0;
										},
										cache : false,
									}
								},
								schema : {
									data : "matterManageActionForm.entMstFactoryMatterList",
									total : "matterManageActionForm.entMstFactoryMatterCount"
								}
							});

					var grid = loadGridData();
					// Add filter
					grid.data("kendoGrid").thead.append(filterRow);
					// Set init default value for filter
					setInitForFilter();

					// Add datetimePicker for updateOn textBox.
					kendo.culture("ja-JP");
					// kendo for updateOn From textBox.
					jQuery("#updatedOnFormSearch").kendoDateTimePicker({
						//BOE: TRIET_NGUYEN
						interval: 60,
				        format: "yyyy/MM/dd HH:mm:ss",
				        timeFormat: "HH:mm:ss",
				        //EOE: TRIET_NGUYEN
					});
					// Set value kendo object.
					updateOnFromKendo = jQuery("#updatedOnFormSearch").data(
							"kendoDateTimePicker");

					// kendo for updateOn To textBox.
					jQuery("#updatedOnFormToSearch").kendoDateTimePicker({
						//BOE: TRIET_NGUYEN
						interval: 60,
				        format: "yyyy/MM/dd HH:mm:ss",
				        timeFormat: "HH:mm:ss",
				        //EOE: TRIET_NGUYEN
					});
					// Set value kendo object.
					updateOnToKendo = jQuery("#updatedOnFormToSearch").data(
							"kendoDateTimePicker");

					// Handle datetime picker validation.
				    handleDateTimePicker();
				    
					// Open datetimePicker when focus or click
				    //BOE: TRIET_NGUYEN
					//jQuery("#updatedOnFormSearch").on("click focus",
					//		function() {
					//			updateOnFromKendo.open();
					//		});
					//jQuery("#updatedOnFormToSearch").on("click focus",
					//		function() {
					//			updateOnToKendo.open();
					//		});
				    //BOE: TRIET_NGUYEN
				    
					jQuery("#lstUser").kendoDropDownList();
					
					//BOE: TRAN_THANH 24/03/2014
					if ($("#hMatterChangeUserId").val() != null) {
						//console.log($("#hMatterChangeUserId").val());
						if ($("#hMatterChangeUserId").val().indexOf("LIMITED_USER_ID") >= 0) {
							var dropdownlist = $("#lstUser").data("kendoDropDownList");
							dropdownlist.value($("#hLoginId").val());
							dropdownlist.enable(false);
						}
					}
					//EOE: TRAN_THANH 24/03/2014

					jQuery("#cbbRegistrationFlg").kendoDropDownList();
					// 1
					jQuery("#matterNoSearch").kendoNumericTextBox({
						format: "#",
			            decimals: 0,
						min : minLengthNumeric,
						max : maxLengthNumeric,
						step : 1
					});
					// 2
					jQuery("#matterRedmineIssueIdSearch").kendoNumericTextBox({
						format : "#",
						decimals: 0,
						min : minLengthNumeric,
						max : maxLengthNumeric,
						step : 1
					});
					// 6
					jQuery("#matterFactoryCountSearchFrom")
							.kendoNumericTextBox({
								format : "#",
								decimals: 0,
								min : minLengthNumeric,
								max : maxLengthNumeric,
								step : 1
							});
					jQuery("#matterFactoryCountSearchTo").kendoNumericTextBox({
						format : "#",
						decimals: 0,
						min : minLengthNumeric,
						max : maxLengthNumeric,
						step : 1
					});
					// 7
					jQuery("#matterFactoryErrorCountGeneralSearchFrom")
							.kendoNumericTextBox({
								format : "#",
								decimals: 0,
								min : minLengthNumeric,
								max : maxLengthNumeric,
								step : 1
							});
					jQuery("#matterFactoryErrorCountGeneralSearchTo")
							.kendoNumericTextBox({
								format : "#",
								decimals: 0,
								min : minLengthNumeric,
								max : maxLengthNumeric,
								step : 1
							});
					// 8
					jQuery("#matterFactoryErrorCountCategorySearchFrom")
							.kendoNumericTextBox({
								format : "#",
								decimals: 0,
								min : minLengthNumeric,
								max : maxLengthNumeric,
								step : 1
							});
					jQuery("#matterFactoryErrorCountCategorySearchTo")
							.kendoNumericTextBox({
								format : "#",
								decimals: 0,
								min : minLengthNumeric,
								max : maxLengthNumeric,
								step : 1
							});
					// 9
					jQuery("#matterFactoryErrorCountModelSearchFrom")
							.kendoNumericTextBox({
								format : "#",
								decimals: 0,
								min : minLengthNumeric,
								max : maxLengthNumeric,
								step : 1
							});
					jQuery("#matterFactoryErrorCountModelSearchTo")
							.kendoNumericTextBox({
								format : "#",
								decimals: 0,
								min : minLengthNumeric,
								max : maxLengthNumeric,
								step : 1
							});
					// 10
					jQuery("#matterFactoryErrorCountAttributeSearchFrom")
							.kendoNumericTextBox({
								format : "#",
								decimals: 0,
								min : minLengthNumeric,
								max : maxLengthNumeric,
								step : 1
							});
					jQuery("#matterFactoryErrorCountAttributeSearchTo")
							.kendoNumericTextBox({
								format : "#",
								decimals: 0,
								min : minLengthNumeric,
								max : maxLengthNumeric,
								step : 1
							});

					// Validate From search value of update_on
					//BOE: TRIET_NGUYEN
					//jQuery("#updatedOnFormSearch").focusout(function() {
					//	validateDateTextBox('updatedOnFormSearch');
					//});
					// Validate to search value of update_on
					//jQuery("#updatedOnFormToSearch").focusout(function() {
					//	validateDateTextBox('updatedOnFormToSearch');
					//});
					//EOE: TRIET_NGUYEN

					// Click button Back
					jQuery("#btnBack").click(function() {
						backToPreviousPage();
					});

					// Click button reset
					jQuery("#btnReset").click(function() {
						// Set sort reset
						sortResetFlg = 1;
						// Set init default value for filter.
						setInitForFilter();
						// Call search.
						jQuery("#btnSearch").click();
					});
					// Button Search click
					jQuery("#btnSearch").click(function() {
						// Validate 2 updateOn
						//var validateDate = validateUpdateOnDate();
						var validateDate = true;
				        var valid = true;
				        if (!validateKousinDate()) {
				            valid = false;
				        }
						// Validate lengthLimit
						// var validateLengthLimit =
						// validateNumbericAndBlank(jQuery("#tbLengthLimit").val());
						var checkCondition = getSearchCondition();
						if (validateDate && checkCondition && valid) {
							// getSearchCondition();
							// Reload data and goto first page
							grid.data("kendoGrid").dataSource.page(1);
							// Go to top of grid
							grid.data("kendoGrid").content.scrollTop(0);
						}
					});
					// Press enter key in textbox filter
					jQuery(document).on("keypress", "input.tbSearch",
							function(e) {
								if (e.keyCode == 13) {
									jQuery("#btnSearch").click();
								}
							});

					// Change ListType, ShowFl, DelFlg value
					jQuery("#lstUser, #cbbRegistrationFlg").change(function() {
						jQuery("#btnSearch").click();
					});

					// Validate tbLengthLimit when outfocus
					jQuery("#tbLengthLimit").focusout(
							function() {
								// Valid number
								if (validateNumbericAndBlank(jQuery(
										"#tbLengthLimit").val())) {
									jQuery("#tbLengthLimit").removeClass(
											"error");
								} else {
									// Invalid
									jQuery("#tbLengthLimit").addClass("error");
								}
							});
					
				    // validate Date
				    $("#updatedOnFormSearch, #updatedOnFormToSearch").focusout(function () {
				    	//validateKousinDate();
				    });

					function initDeleteMatterDialog() {
						// Start Delete Matter
						//var deleteDialogTitle = $("#deleteDialogTitle").val();
						$("#deleteMatterDialog").kendoWindow({
							minWidth : 400,
							minWeight : 300,
							//boe thai.son
							title : $("#windowNamedeleteMatterDialog").val(),
							//eoe thai.son
							resizable : false,
							modal : true,
							visible : false,
							draggable : false,
							open : function() {
								$("#matterName").text("");
								$("#lstAdmin").val($("#lstAdmin option:first").val());
							}
						});
						//boe thai.son
						/*// Center its title.
						$("#deleteMatterDialog_wnd_title").css("text-align",
								"center");*/
						//eoe thai.son
						// Get delete dialog.
						var deleteMatterDialog = $("#deleteMatterDialog").data(
								"kendoWindow");

						// Add handlers.
						$("#btnDelete").click(function() {
							var checkedCount = $(".matterCheckbox:checked").length;
							if (checkedCount > 0) {
								deleteMatterDialog.center().open();
							} else {
								// Show PleaseSelect dialog.
								var pleaseSelectDialog = $("#pleaseSelectDialog").data("kendoWindow");
								pleaseSelectDialog.center().open();
							}
						});
						$("#btnDeleteMatterCancel").click(function(e) {
							deleteMatterDialog.close();
						});
						$("#btnDeleteMatter").click(function() {
							// Reset error message panel first.
							resetErrorMssagePanels();

							// Call API to delete selected row.
							deleteMatterByListMatterNo();

							// Close dialog.
							deleteMatterDialog.close();
						});
					}

					function initPleaseSelectDialog() {
						// Init pleaseSelect dialog.
						$("#pleaseSelectDialog").kendoWindow({
							minWidth : 300,
							minWeight : 200,
							//boe thai.son
							title : $("#windowNamepleaseSelectDialog").val(),
							//eoe thai.son
							resizable : false,
							modal : true,
							visible : false,
							draggable : false
						});
						// Get pleaseSelect dialog.
						var pleaseSelectDialog = $("#pleaseSelectDialog").data("kendoWindow");
						$("#pleaseSelectDialogOk").click(function(e) {
							pleaseSelectDialog.close();
						});
					}

					// BOE hoang.ho 2014/04/11 fix bug message bar
					/*function resetErrorMssagePanels() {
						// Init matter manage panel.
						$("#matterManageMessage").removeClass().text("").hide();
					}*/
					// EOE hoang.ho 2014/04/11 fix bug message bar
					
					//BOE: TRIET_NGUYEN
				    function handleDateTimePicker() {
				        // Handle focusout event.
				        $("#updatedOnFormSearch").focusout(function() {
				        	validateDateTimePicker('#updatedOnFormSearch', '#updatedOnFormSearch', '#updatedOnFormToSearch');
				        });
				        $("#updatedOnFormToSearch").focusout(function() {
				        	validateDateTimePicker('#updatedOnFormToSearch', '#updatedOnFormSearch', '#updatedOnFormToSearch');
				        });
				    }
				    //EOE: TRIET_NGUYEN
				    
				    function setInitForFilter() {
				    	// clear all textbox
				    	// 1
				    	jQuery("#matterNoSearch").kendoNumericTextBox({
				    		value : "",
				    		format: "#",
				            decimals: 0,
				    		min : minLengthNumeric,
				    		max : maxLengthNumeric,
				    		step : 1
				    	});
				    	// 2
				    	jQuery("#matterRedmineIssueIdSearch").kendoNumericTextBox({
				    		value : "",
				    		format: "#",
				            decimals: 0,
				    		min : minLengthNumeric,
				    		max : maxLengthNumeric,
				    		step : 1
				    	});
				    	// 3
				    	jQuery("#matterNameSearch").val('');
				    	
				    	// 4 Set type comboBox: default value is ' ' => All
				    	$("#lstUser").kendoDropDownList();
				    	$("#lstUser").data('kendoDropDownList').value(" ");
				    	
				    	// 5 Set type comboBox: default value is ' ' => All
				    	$("#cbbRegistrationFlg").kendoDropDownList();
				    	$("#cbbRegistrationFlg").data('kendoDropDownList').value(" ");
				    	
				    	// 6
				    	jQuery("#matterFactoryCountSearchFrom").kendoNumericTextBox({
				    		value : "",
				    		format: "#",
				            decimals: 0,
				    		min : minLengthNumeric,
				    		max : maxLengthNumeric,
				    		step : 1
				    	});
				    	jQuery("#matterFactoryCountSearchTo").kendoNumericTextBox({
				    		value : "",
				    		format: "#",
				            decimals: 0,
				    		min : minLengthNumeric,
				    		max : maxLengthNumeric,
				    		step : 1
				    	});
				    	// 7
				    	jQuery("#matterFactoryErrorCountGeneralSearchFrom").kendoNumericTextBox({
				    		value : "",
				    		format: "#",
				            decimals: 0,
				    		min : minLengthNumeric,
				    		max : maxLengthNumeric,
				    		step : 1
				    	});
				    	jQuery("#matterFactoryErrorCountGeneralSearchTo").kendoNumericTextBox({
				    		value : "",
				    		format: "#",
				            decimals: 0,
				    		min : minLengthNumeric,
				    		max : maxLengthNumeric,
				    		step : 1
				    	});
				    	// 8
				    	jQuery("#matterFactoryErrorCountCategorySearchFrom").kendoNumericTextBox({
				    		value : "",
				    		format: "#",
				            decimals: 0,
				    		min : minLengthNumeric,
				    		max : maxLengthNumeric,
				    		step : 1
				    	});
				    	jQuery("#matterFactoryErrorCountCategorySearchTo").kendoNumericTextBox({
				    		value : "",
				    		format: "#",
				            decimals: 0,
				    		min : minLengthNumeric,
				    		max : maxLengthNumeric,
				    		step : 1
				    	});
				    	// 9
				    	jQuery("#matterFactoryErrorCountModelSearchFrom").kendoNumericTextBox({
				    		value : "",
				    		format: "#",
				            decimals: 0,
				    		min : minLengthNumeric,
				    		max : maxLengthNumeric,
				    		step : 1
				    	});
				    	jQuery("#matterFactoryErrorCountModelSearchTo").kendoNumericTextBox({
				    		value : "",
				    		format: "#",
				            decimals: 0,
				    		min : minLengthNumeric,
				    		max : maxLengthNumeric,
				    		step : 1
				    	});
				    	// 10
				    	jQuery("#matterFactoryErrorCountAttributeSearchFrom").kendoNumericTextBox({
				    		value : "",
				    		format: "#",
				            decimals: 0,
				    		min : minLengthNumeric,
				    		max : maxLengthNumeric,
				    		step : 1
				    	});
				    	jQuery("#matterFactoryErrorCountAttributeSearchTo").kendoNumericTextBox({
				    		value : "",
				    		format: "#",
				            decimals: 0,
				    		min : minLengthNumeric,
				    		max : maxLengthNumeric,
				    		step : 1
				    	});
				    	// 11
				    	// Set listShowFlg combobox: default value is ' ' => All
				    	// jQuery('select[id="updatedOnFormSearch"] option[value="
				    	// "]').attr('selected','selected');
				    	jQuery("#updatedOnFormSearch").val("");
				    	// Set listDelFlg combobox: default value is Yes => Yes
				    	// jQuery('select[id="updatedOnFormToSearch"] option[value="
				    	// "]').attr('selected','selected');
				    	jQuery("#updatedOnFormToSearch").val("");
				    }
				});