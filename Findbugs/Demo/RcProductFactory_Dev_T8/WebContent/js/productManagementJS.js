/* BOE globel variable*/
var syouhinSysCode = "";
var syouhinSysCodeToPreview = "105";
var brandName = "";
var productCode = "";
var productName = "";
var productGroupCode = "";
var descriptionSummary = "";
var hyoujiName = "";
var productProperPriceFrom = "";
var productProperPriceTo = "";
var rateFrom = "";
var rateTo = "";
var siireName1 = "";
var siireName2 = "";
var supplierStatusName = "";
var productMaintenanceFlg = -1;
var productStatusName = "";
var productDelFlg = 0;
var fullName = "";
var updatedOnFrom = "";
var updatedOnTo = "";
var sortField = "";
var sortDir = "";
var sortResetFlg = 0;
var grid;
var selectedKendoClass = "k-alt k-state-selected";
var selectedRowClass = "selectedGridRow";
var arraySyouhinSysCode = "";
var popup;
var supplierStatusCode = "";
var isLoadedFromSv = 0;
var validMatterNo = 0;
var matterNo = "";
var defaultTokenStr = "";
var tokenStr = "";
var checkStatus = 0;
var productDelFlgStatus = 0;

// Indicate filtering is used (THIS IS A MUST).
var detailSearchFlag = 1;

var numericMax = 2147483647;
var numericMin = 0;

var isImageThumbLoading = false;
var systemProductCodeMaxLen = 20;
var brandMaxLen = 60;
var productNameMaxLen = 255;
var makerStockNumberMaxLen = 50;
var itemMaxLen = 266;
var supplierMaxLen = 60;
var janMaxLen = 40;
var groupCodeMaxLen = 12;
var noteMaxLen = 4000;
var summaryMaxLen = 35000;
/*var explanationMaxLen = 100;*/
var cautionMaxLen = 6000;
var supportedBikeMaxLen = 1000;
var bikeModelMaxLen = 255;
var optionTypeMaxLen = 150;
var optionMaxLen = 150;
var thumbnailImagePathMaxLen = 255;
var detailedImagePathMaxLen = 255;
var deliveryTimeMaxLen = 30;
var attributeMaxLen = 266;
var attributeDisplayNameMaxLen = 128;
/*var attributeManagementValueMaxLen = 266;*/
var linkTitleMaxLen = 128;
var videoTitleMaxLen = 128;
var listPriceMaxLen = 8;
var partitionMaxLen = 8;

var listThumbCount = 0;
var listSyouhinImage;
var noImageURL;
var filterHeader;
//Status of detail load data: true: loaded, false: not loaded
var previewPopupLoadStatus = false;
//Value mode of preview popup: simple mode and detail mode.
var previewPopupMode = "simple";
var previewPopupFitModelGrid;
var previewPopupAttributeAllTypeGrid;
var previewPopupLinkReasonGrid;
var previewPopupVideoGrid;
var windowName;
//var kendoValidatorVar;

var ALL_EXPORTMODE = "MODE_1";
var COMPATIBLE_MODEL_EXPORTMODE = "MODE_2";
var ATTRIBUTE_EXPORTMODE = "MODE_3";

var originalSyouhinSysCodeToPreview = "";
var buttonClicked = false;
var isClickBtnMaintenance = false;
var productValidateMode = 0;
var msgFail = "";
var msgComplete = "";

var lines = 20000;
var chars = 40;
var isValideTextArea = true;

/* Error flg in popup search*/
var errorCategoryCode = false;
var errorBrandCode = false;
var errorSiireCode = false;

/* EOE globel variable*/
$(window).load(function(){
	/**
	 * BOF Handle keyup tab-key on field filter
	 * @rcv!cao.lan 2014/03/10
	 * **/
	$('.k-grid-header-wrap').keyup(function(e) {
	    var code = e.keyCode || e.which;
	    // code = 9 is tab-key
	    if (code == '9') {
	    	// get scroll left in grid header 
	    	var left = $('.k-grid-header-wrap').scrollLeft();
	    	// if tab-key and scroll left change then scroll left k-grid-content
    		$(".k-grid-content").scrollLeft(left);
	    }
	 });
	
	/** EOF @rcv!cao.lan 2014/03/10 **/
});


// function check number record length can process
// process 1,2,3,4.
// max record 50.000
// @author hoang.ho
// @since 2014/03/19
function checkRecordLengthIsValid() {
	 var allCheckBoxValue = $("#selectAllCheckbox").prop("checked");
	 if (allCheckBoxValue) {
         //Check number of product to export is over max.
         var maxNumberExport = jQuery("#maxNumberProductMaintenanceAction").val();
         if ($("#grid").data("kendoGrid").dataSource.total() > maxNumberExport) {
             //Show error message
             $.when(kendo.ui.ExtAlertDialog.show({
                 title: $("#hdDialogTitle").val(), 
                 label: jQuery("#confirmYes").val(),
                 message: $.format(jQuery("#hdExecuteOverMaxProductError").val(), maxNumberExport),
                 width: 393,
                 icon: "k-ext-information" })
             ).done();
             return false;
         }
     }
     return true;
	 
}

function getSearchOptionRadioButtonValue(nameAttribute) {
    return parseInt($("input[type=radio][name=" + nameAttribute + "]:checked").val());
}

    
/**
 * Get multi search textArea trimmed lines without empty lines.
 * @param lines Text of lines.
 * @return JSON array of lines
 */
function getMultiSearchTextAreaLines(textOfLines) {
    var lines = textOfLines.split("\n");

    // Store only non-empty lines.
    var withoutEmptyLines = [];
    $.each(lines, function(key, value){
        var trimmed = value.trim();
        if(trimmed != "") {
          withoutEmptyLines.push(trimmed);
        }
    });
    return withoutEmptyLines;
}

 function getDetailSearchParams() {
        var systemProductCode = $("#systemProductCode").val();
        if(systemProductCode == "") {
            systemProductCode = null;
        }
        
        var brandCode = $("#brandSelect").data("kendoComboBox").value();
        if(brandCode == "" || brandCode == undefined || parseInt(brandCode) < 0) {
            brandCode = null;
        }
        
        var productName = $("#productName").val();
        
        var makerStockNumber = $("#makerStockNumber").val();
        
        var categoryCode = $("#categorySelect").data("kendoComboBox").value();
        if(categoryCode == "" || categoryCode == undefined || parseInt(categoryCode) < 0) {
            categoryCode = null;
        }
        
        var siireCode = $("#supplierSelect").data("kendoComboBox").value();
        if(siireCode == "" || siireCode == undefined || siireCode == "-1") {
            siireCode = null;
        }
        
        var jan = $("#jan").val();
        
        var groupCode = $("#groupCode").val();
        if(groupCode == "") {
            groupCode = null;
        }
        
        var note = $("#note").val();
        
        var summary = $("#summary").val();
        
        var explanation = $("#explanation").val();
        
        var caution = $("#caution").val();
        
        //BOE #12097 2014/09/10 Nguyen.Chuong: add maker filter in detail search.
        var maker = $("#maker").val();
        //EOE #12097 2014/09/10 Nguyen.Chuong: add maker filter in detail search.
        var supportedBike = $("#supportedBike").val();//TODO
        
        var bikeModel = $("#bikeModel").val();
        
        var selectCode = $("#optionTypeSelect").data("kendoComboBox").value();
        if(selectCode == "" || selectCode == undefined) {
            selectCode = null;
        }
        
        var option = $("#option").val();
        
        var thumbnailImagePath = $("#thumbnailImagePath").val();
        
        var detailedImagePath = $("#detailedImagePath").val();
        
        var deliveryTime = $("#deliveryTimeSelect").data("kendoComboBox").value();
        if(deliveryTime == "" || deliveryTime == undefined) {
            deliveryTime = "";
        }
        
        var attribute = $("#attributeSelect").data("kendoComboBox").value();
        if(attribute == "" || attribute == undefined) {
            attribute = null;
        } else {
            var index = attribute.indexOf("_");
            if(index != -1) {
                var attributeCode = attribute.substring(0, index);
                attribute = attributeCode;
            } else {
                // It should never go here.
            }
        }
        
        var attributeDisplayName = $("#attributeDisplayName").val();
        
        var attributeManagementValueDisabled = $("#attributeManagementValue").prop("disabled");
        var attributeManagementValue = "";
        var attributeManagementBooleanValue = false;
        var attributeManagementValueType = 1;
        if(attributeManagementValueDisabled) {
            attributeManagementValueType = 2;
            attributeManagementBooleanValue = $("#attributeManagementValueFlagCheckBox").prop("checked");
        } else {
            attributeManagementValueType = 1;
            attributeManagementValue = $("#attributeManagementValue").val();
        }
        
        var linkTitle = $("#linkTitle").val();
        
        var videoTitle = $("#videoTitle").val();
        
        var variousFlagStr = "";
        var variousFlag = $("#variousFlag").data("kendoMultiSelect").value();
        if(variousFlag == "" || variousFlag == undefined) {
            variousFlagStr = "";
        } else {
            $.each(variousFlag, function(key, value){
                variousFlagStr += value + ",";
            });
            variousFlagStr = variousFlagStr.substring(variousFlagStr, variousFlagStr.length - 1);
        }
        
        var fromListPrice = $("#fromListPrice").val();
        if(fromListPrice == "") {
            fromListPrice = null;
        }
        
        var toListPrice = $("#toListPrice").val();
        if(toListPrice == "") {
            toListPrice = null;
        }
        
        var fromPartition = $("#fromPartition").val();
        if(fromPartition == "") {
            fromPartition = null;
        }
        
        var toPartition = $("#toPartition").val();
        if(toPartition == "") {
            toPartition = null;
        }
        
        var makerHandlingSituation = $("#makerHandlingSituationSelect").data("kendoComboBox").value();
        if(makerHandlingSituation == "" || makerHandlingSituation == undefined) {
            makerHandlingSituation = "";
        }
        
        var dataManagementSituation = $("#dataManagementSituationSelect").data("kendoDropDownList").value();
        //BOE Nguyen.Chuong 2014/03/25 change default value.
        if (dataManagementSituation == undefined) {
            dataManagementSituation = 0;
        }
        else if(dataManagementSituation == "") {
            dataManagementSituation = -1;
        } else {
            dataManagementSituation = parseInt(dataManagementSituation);
        }
        //EOE Nguyen.Chuong 2014/03/25
        var createdOnFrom = $("#productRegistrationDateFromPicker").data("kendoDateTimePicker").value();
        var createdOnFromStr = kendo.toString(createdOnFrom, "yyyy-MM-dd HH:mm:ss");
        if(createdOnFromStr == null) {
            createdOnFromStr = "";
        }
        
        var createdOnTo = $("#productRegistrationDateToPicker").data("kendoDateTimePicker").value();
        var createdOnToStr = kendo.toString(createdOnTo, "yyyy-MM-dd HH:mm:ss");
        if(createdOnToStr == null) {
            createdOnToStr = "";
        }
        
        var updatedOnFrom = $("#productRenovationDateFromPicker").data("kendoDateTimePicker").value();
        var updatedOnFromStr = kendo.toString(updatedOnFrom, "yyyy-MM-dd HH:mm:ss");
        if(updatedOnFromStr == null) {
            updatedOnFromStr = "";
        }
        
        var updatedOnTo = $("#productRenovationDateToPicker").data("kendoDateTimePicker").value();
        var updatedOnToStr = kendo.toString(updatedOnTo, "yyyy-MM-dd HH:mm:ss");
        if(updatedOnToStr == null) {
            updatedOnToStr = "";
        }
        
        var lastUpdatedPerson = $("#lastUpdatedPersonSelect").data("kendoComboBox").value();
        if(lastUpdatedPerson == "" || lastUpdatedPerson == undefined) {
            lastUpdatedPerson = "";
        }
        
        // Get multiSearch option.
        multiSearchWhich = getSearchOptionRadioButtonValue("multiSearch");
        var textAreaContent = $("#multiSearchCriteriaTextArea").val();
        var contentLines = getMultiSearchTextAreaLines(textAreaContent);
        // Prepare criteria string to pass to server.
        var criteriaStr = "";
        $.each(contentLines, function(key, value) {
            criteriaStr += value.trim() + ",";
        });
        criteriaStr = criteriaStr.substring(criteriaStr, criteriaStr.length - 1);

        // Get search option values.
        var productNameSearchOption = getSearchOptionRadioButtonValue("productName");
        var makerStockNumberSearchOption = getSearchOptionRadioButtonValue("makerStockNumber");
        var janSearchOption = getSearchOptionRadioButtonValue("jan");
        var groupCodeSearchOption = getSearchOptionRadioButtonValue("groupCode");
        var noteSearchOption = getSearchOptionRadioButtonValue("note");
        var summarySearchOption = getSearchOptionRadioButtonValue("summary");
        var explanationSearchOption = getSearchOptionRadioButtonValue("explanation");
        var cautionSearchOption = getSearchOptionRadioButtonValue("caution");
        //BOE #12097 2014/09/10 Nguyen.Chuong: add maker filter in detail search.
        var makerSearchOption = getSearchOptionRadioButtonValue("maker");
        //EOE #12097 2014/09/10 Nguyen.Chuong: add maker filter in detail search.
        var supportedBikeSearchOption = getSearchOptionRadioButtonValue("supportedBike");
        var bikeModelSearchOption = getSearchOptionRadioButtonValue("bikeModel");
        var optionSearchOption = getSearchOptionRadioButtonValue("option");
        var thumbnailImagePathSearchOption = getSearchOptionRadioButtonValue("thumbnailImagePath");
        var detailedImagePathSearchOption = getSearchOptionRadioButtonValue("detailedImagePath");
        var attributeDisplayNameSearchOption = getSearchOptionRadioButtonValue("attributeDisplayName");
        var attributeManagementValueSearchOption = getSearchOptionRadioButtonValue("attributeManagementValue");
        var linkTitleSearchOption = getSearchOptionRadioButtonValue("linkTitle");
        var videoTitleSearchOption = getSearchOptionRadioButtonValue("videoTitle");

        // Whether condition.
        var janWhether = $("#janCheckBox").prop("checked");
        var noteWhether = $("#noteCheckBox").prop("checked");
        var summaryWhether = $("#summaryCheckBox").prop("checked");
        var explanationWhether = $("#explanationCheckBox").prop("checked");
        var cautionWhether = $("#cautionCheckBox").prop("checked");
        //BOE #12097 2014/09/10 Nguyen.Chuong: add maker filter in detail search.
        var makerWhether = $("#makerCheckBox").prop("checked");
        //EOE #12097 2014/09/10 Nguyen.Chuong: add maker filter in detail search.
        var supportedBikeWhether = $("#supportedBikeCheckBox").prop("checked");
        var bikeModelWhether = $("#bikeModelCheckBox").prop("checked");
        var optionWhether = $("#optionCheckBox").prop("checked");
        var thumbnailImagePathWhether = $("#thumbnailImagePathCheckBox").prop("checked");
        var detailedImagePathWhether = $("#detailedImagePathCheckBox").prop("checked");
        var attributeDisplayNameWhether = $("#attributeDisplayNameCheckBox").prop("checked");
        var attributeManagementValueWhether = $("#attributeManagementValueCheckBox").prop("checked");
        var linkTitleWhether = $("#linkTitleCheckBox").prop("checked");
        var videoTitleWhether = $("#videoTitleCheckBox").prop("checked");
        
        // DateTime checkboxes.
        var productRegistrationDateIncluded = $("#productRegistrationDateCheckBox").prop("checked");
        var productRenovationDateIncluded = $("#productRenovationDateCheckBox").prop("checked");
        var lastUpdatedPersonIncluded = $("#lastUpdatedPersonCheckBox").prop("checked");
        //BOE Nguyen.Chuong 2014/03/15: add default of maintenance combobox.
        var productMaintenanceFlg = -1;
        //EOE Nguyen.Chuong 2014/03/15
        
        var params = {
            "productManagementActionForm.entMstProduct.syouhinSysCode": systemProductCode,
            "productManagementActionForm.entMstProduct.brandCode": brandCode,
            "productManagementActionForm.entMstProduct.productName": productName,
            "productManagementActionForm.entMstProduct.productCode": makerStockNumber,
            "productManagementActionForm.entMstProduct.productCategoryCode": categoryCode,
            "productManagementActionForm.entMstProduct.siireCode": siireCode,
            "productManagementActionForm.entMstProduct.productEanCode": jan,
            "productManagementActionForm.entMstProduct.productGroupCode": groupCode,
            "productManagementActionForm.entMstProduct.descriptionRemarks": note,
            "productManagementActionForm.entMstProduct.descriptionSummary": summary,
            "productManagementActionForm.entMstProduct.descriptionSentence": explanation,
            "productManagementActionForm.entMstProduct.descriptionCaution": caution,
            //BOE #12097 2014/09/10 Nguyen.Chuong: add maker filter in detail search.
            "productManagementActionForm.entMstProduct.maker": maker,
            //EOE #12097 2014/09/10 Nguyen.Chuong: add maker filter in detail search.
            "productManagementActionForm.entMstProduct.model": supportedBike,
            "productManagementActionForm.entMstProduct.style": bikeModel,
            "productManagementActionForm.entMstProduct.selectCode": selectCode,
            "productManagementActionForm.entMstProduct.hyoujiName": option,
            "productManagementActionForm.entMstProduct.thumbnail": thumbnailImagePath,
            "productManagementActionForm.entMstProduct.image": detailedImagePath,
            "productManagementActionForm.entMstProduct.noukiCode": deliveryTime,
            "productManagementActionForm.entMstProduct.attributeCode": attribute,
            "productManagementActionForm.entMstProduct.attributeDisplayName": attributeDisplayName,
            "productManagementActionForm.entMstProduct.attributeManagementValue": attributeManagementValue,
            "productManagementActionForm.entMstProduct.attributeManagementBooleanValue": attributeManagementBooleanValue,
            "productManagementActionForm.entMstProduct.attributeManagementValueType": attributeManagementValueType,
            "productManagementActionForm.entMstProduct.linkTitle": linkTitle,
            "productManagementActionForm.entMstProduct.videoTitle": videoTitle,
            "variousFlag": variousFlagStr,
            "productManagementActionForm.entMstProduct.productProperPriceFrom": fromListPrice,
            "productManagementActionForm.entMstProduct.productProperPriceTo": toListPrice,
            "productManagementActionForm.entMstProduct.fromPartition": fromPartition,
            "productManagementActionForm.entMstProduct.toPartition": toPartition,
            "productManagementActionForm.entMstProduct.supplierStatusCode": makerHandlingSituation,
            "productManagementActionForm.entMstProduct.productDelFlg": dataManagementSituation,
            
            "productManagementActionForm.entMstProduct.createdOnFrom": createdOnFromStr,
            "productManagementActionForm.entMstProduct.createdOnTo": createdOnToStr,
            "productManagementActionForm.entMstProduct.updatedOnFrom": updatedOnFromStr,
            "productManagementActionForm.entMstProduct.updatedOnTo": updatedOnToStr,
            "productManagementActionForm.entMstProduct.updatedUserId": lastUpdatedPerson,
            "productManagementActionForm.entMstProduct.multiSearchWhich": multiSearchWhich,
            "productManagementActionForm.entMstProduct.detailSearchFlag": detailSearchFlag,
            "multiSearchCriteria": criteriaStr,
            
            // CheckBox flags.
            "productManagementActionForm.entMstProduct.productNameSearchOption": productNameSearchOption,
            "productManagementActionForm.entMstProduct.makerStockNumberSearchOption": makerStockNumberSearchOption,
            
            "productManagementActionForm.entMstProduct.janSearchOption": janSearchOption,
            "productManagementActionForm.entMstProduct.groupCodeSearchOption": groupCodeSearchOption,
            "productManagementActionForm.entMstProduct.noteSearchOption": noteSearchOption,
            "productManagementActionForm.entMstProduct.summarySearchOption": summarySearchOption,
            "productManagementActionForm.entMstProduct.explanationSearchOption": explanationSearchOption,
            "productManagementActionForm.entMstProduct.cautionSearchOption": cautionSearchOption,
            //BOE #12097 2014/09/10 Nguyen.Chuong: add maker filter in detail search.
            "productManagementActionForm.entMstProduct.makerSearchOption": makerSearchOption,
            //EOE #12097 2014/09/10 Nguyen.Chuong: add maker filter in detail search.
            "productManagementActionForm.entMstProduct.supportedBikeSearchOption": supportedBikeSearchOption,
            "productManagementActionForm.entMstProduct.bikeModelSearchOption": bikeModelSearchOption,
            
            "productManagementActionForm.entMstProduct.optionSearchOption": optionSearchOption,
            "productManagementActionForm.entMstProduct.thumbnailImagePathSearchOption": thumbnailImagePathSearchOption,
            "productManagementActionForm.entMstProduct.detailedImagePathSearchOption": detailedImagePathSearchOption,

            "productManagementActionForm.entMstProduct.attributeDisplayNameSearchOption": attributeDisplayNameSearchOption,
            "productManagementActionForm.entMstProduct.attributeManagementValueSearchOption": attributeManagementValueSearchOption,
            "productManagementActionForm.entMstProduct.linkTitleSearchOption": linkTitleSearchOption,
            "productManagementActionForm.entMstProduct.videoTitleSearchOption": videoTitleSearchOption,
            
            // Whether search condition.
            "productManagementActionForm.entMstProduct.janWhether": janWhether,
            "productManagementActionForm.entMstProduct.noteWhether": noteWhether,
            "productManagementActionForm.entMstProduct.summaryWhether": summaryWhether,
            "productManagementActionForm.entMstProduct.explanationWhether": explanationWhether,
            "productManagementActionForm.entMstProduct.cautionWhether": cautionWhether,
            //BOE #12097 2014/09/10 Nguyen.Chuong: add maker filter in detail search.
            "productManagementActionForm.entMstProduct.makerWhether": makerWhether,
            //EOE #12097 2014/09/10 Nguyen.Chuong: add maker filter in detail search.
            "productManagementActionForm.entMstProduct.supportedBikeWhether": supportedBikeWhether,
            "productManagementActionForm.entMstProduct.bikeModelWhether": bikeModelWhether,
            "productManagementActionForm.entMstProduct.optionWhether": optionWhether,
            "productManagementActionForm.entMstProduct.thumbnailImagePathWhether": thumbnailImagePathWhether,
            "productManagementActionForm.entMstProduct.detailedImagePathWhether": detailedImagePathWhether,
            "productManagementActionForm.entMstProduct.attributeDisplayNameWhether": attributeDisplayNameWhether,
            "productManagementActionForm.entMstProduct.attributeManagementValueWhether": attributeManagementValueWhether,
            "productManagementActionForm.entMstProduct.linkTitleWhether": linkTitleWhether,
            "productManagementActionForm.entMstProduct.videoTitleWhether": videoTitleWhether,
            
            // DateTime checkboxes.
            "productManagementActionForm.entMstProduct.productRegistrationDateIncluded": productRegistrationDateIncluded,
            "productManagementActionForm.entMstProduct.productRenovationDateIncluded": productRenovationDateIncluded,
            "productManagementActionForm.entMstProduct.lastUpdatedPersonIncluded": lastUpdatedPersonIncluded,
            
            //BOE Nguyen.Chuong 2014/03/15: add default of maintenance combobox.
            "productManagementActionForm.entMstProduct.productMaintenanceFlg": productMaintenanceFlg,
            //EOE Nguyen.Chuong 2014/03/15
            // BOE hoang.ho 2014/03/24: add field when apply tuning sql
            "productManagementActionForm.allCheckBoxValue" 						  :      $("#selectAllCheckbox").prop("checked"),
            "productManagementActionForm.entMstProduct.productDelFlgStatus"		  : 	 productDelFlgStatus,
            "productManagementActionForm.entMstProduct.supplierStatusCode"        : 	 supplierStatusCode,
            "productManagementActionForm.checkStatus"        				      : 	 checkStatus,
            "arraySyouhinSysCode"                                                 :      arraySyouhinSysCode,
        };

        return params;
    }

    function createTooltipContentMatter(matterInfo) {
        var html = "<div id='tooltip-content'>";
        //matterCode
        html += $('#matterCodeLabel').val() + ": "  + matterInfo.productMatterNo + "<br/>";
        if (matterInfo.allMatterCount > 1) {
            html += $('#manyMatterMaintenanceProduct').val() + "<br/>";
        }
        html += $('#matterNameLabel').val() + ": " + matterInfo.matterName + "<br/>"
            + $('#matterChangeUserNameLabel').val() + ": " + matterInfo.userLastName + " " + matterInfo.userFirstName + "<br/>"
            + $('#productIdLabel').val() + ": P" + matterInfo.productId + "<br/>";
        if (matterInfo.allProductCount > 1) {
            html += $('#manyProductMaintenanceProduct').val() + "<br/>";
        }
        html += "</div>";
        return html;
    }
/************************************************ BOE Document ready ************************************************/ 
jQuery(document).ready(function () {
    msgFail = $("#msgFail").val();
    msgComplete = $("#msgComplete").val();
    // add title common here
    windowName = $("#windowName").val();
    noImageURL = jQuery("#noImageURL").val();
    filterHeader = $("#tblFilterHeader").find("tbody").html();
    $("#cbxPrdSupplierStatus").kendoDropDownList({});
    $("#tblFilterHeader").remove();
    /*BOE productMangegement ready*/
    $("#selectExcute").kendoDropDownList();
    $("#btnExecute").click(function () {
      clearMsgText();
      $("#actionDivRight .k-dropdown-wrap").css("background-color", "#FFFFFF");
      var actionMode = $("#selectExcute").val();
      if (actionMode == -1) {
          // change combobox action to red when no choose any action.
          $("#actionDivRight .k-dropdown-wrap").css("background-color", "#FFD9E6");
          return;
      } else if (actionMode == 0) { //maintenance action
          if (isChecked()) {
              openPopup("maintenancePopup");
          } else {
              initPleaseSelectDialog();
          }
      } else if (actionMode == 1) {
        if (isChecked()) {
        	// check total record valid
        	if (checkRecordLengthIsValid()) {
        		// case valid
        		openPopup("changeStatusPopup");
        	}
        } else {
          initPleaseSelectDialog();
        }
      } else if (actionMode == 2) {
        if (isChecked()) {
        	// check total record valid
        	if (checkRecordLengthIsValid()) {
        		// case valid
        		openPopup("confirmDisablePopup");
        	}
        } else {
          initPleaseSelectDialog();
        }
        
      } else if (actionMode == 3) {
        if (isChecked()) {
        	// check total record valid
        	if (checkRecordLengthIsValid()) {
        		// case valid 
        		openPopup("confirmEnablePopup");
        	}
        } else {
          initPleaseSelectDialog();
        }
      } else if (actionMode == 4) {
        if (isChecked()) {
          checkProductCanEdit();
          var win = $("#csvPopup").data("kendoWindow");
          win.center();
          win.open();
        } else {
          initPleaseSelectDialog();
        }
      }
    });
    /*BOE init popup*/
    $("#maintenancePopup").kendoWindow({
        minWidth: 270,
    	width: 400,
        resizable: false,
        title: windowName,
        modal: true,
        visible: false,
        close: function () {
          validMatterNo = 0;
          isLoadedFromSv = 0;
          // set buttonClicked to true to prevent focustout event
          buttonClicked = true;
          clearMatterText();
        },
        open: function () {
          // when open popup set buttonClicked to default value
          buttonClicked = false;
        },
    });

    // start CSV popup
    $("#csvPopup").kendoWindow({
        width : 900,
        height : 560,
        resizable: false,
        title: $('#csvProcessing').val(),
        modal: true,
        visible: false,
    });
    
    // Disable mode's checkboxes.
    $(".mode2CheckBox,.mode3CheckBox").prop("checked", true).prop("disabled", true);
    // end CSV popup
    
    // start export CSV
    //BOE Nguyen.Chuong 2014/03/11: modify to add process with all rows when check all checkBox.
    
    function createHiddenInputParam(name, value){
        var hiddenField = document.createElement("input");
        hiddenField.setAttribute("name", name);
        hiddenField.setAttribute("value", value);
        return hiddenField;
    };
    
    function setParamForExportCSV(form, syouhinSysCodes, allCheckBoxValue, headers, attributeCodes, sortField, sortDir) {
        //Filter of grid
        form.appendChild(createHiddenInputParam("productManagementActionForm.entMstProduct.syouhinSysCode", syouhinSysCode));
        form.appendChild(createHiddenInputParam("productManagementActionForm.entMstProduct.brandName", brandName));
        form.appendChild(createHiddenInputParam("productManagementActionForm.entMstProduct.productCode", productCode));
        form.appendChild(createHiddenInputParam("productManagementActionForm.entMstProduct.productName", productName));
        form.appendChild(createHiddenInputParam("productManagementActionForm.entMstProduct.productGroupCode", productGroupCode));
        form.appendChild(createHiddenInputParam("productManagementActionForm.entMstProduct.descriptionSummary", descriptionSummary));
//        form.appendChild(createHiddenInputParam("productManagementActionForm.entMstProduct.hyoujiName", hyoujiName));
        form.appendChild(createHiddenInputParam("productManagementActionForm.entMstProduct.productProperPriceFrom", productProperPriceFrom));
        form.appendChild(createHiddenInputParam("productManagementActionForm.entMstProduct.productProperPriceTo", productProperPriceTo));
        form.appendChild(createHiddenInputParam("productManagementActionForm.entMstProduct.rateFrom", rateFrom));
        form.appendChild(createHiddenInputParam("productManagementActionForm.entMstProduct.rateTo", rateTo));
        form.appendChild(createHiddenInputParam("productManagementActionForm.entMstProduct.siireName1", siireName1));
        form.appendChild(createHiddenInputParam("productManagementActionForm.entMstProduct.siireName2", siireName2));
        form.appendChild(createHiddenInputParam("productManagementActionForm.entMstProduct.supplierStatusName", supplierStatusName));
        form.appendChild(createHiddenInputParam("productManagementActionForm.entMstProduct.productMaintenanceFlg", productMaintenanceFlg));
        form.appendChild(createHiddenInputParam("productManagementActionForm.entMstProduct.productStatusName", productStatusName));
        form.appendChild(createHiddenInputParam("productManagementActionForm.entMstProduct.productDelFlg", productDelFlg));
        form.appendChild(createHiddenInputParam("productManagementActionForm.entMstProduct.fullName", fullName));
        form.appendChild(createHiddenInputParam("productManagementActionForm.entMstProduct.updatedOnFrom", updatedOnFrom));
        form.appendChild(createHiddenInputParam("productManagementActionForm.entMstProduct.updatedOnTo", updatedOnTo));
        form.appendChild(createHiddenInputParam("productManagementActionForm.entMstProduct.sortField", sortField));
        form.appendChild(createHiddenInputParam("productManagementActionForm.entMstProduct.sortDir", sortDir));
        form.appendChild(createHiddenInputParam("productManagementActionForm.entMstProduct.detailSearchFlag", detailSearchFlag));
        //Add list syouhinSysCodes.
        form.appendChild(createHiddenInputParam("productManagementActionForm.syouhinSysCodes", syouhinSysCodes));
        form.appendChild(createHiddenInputParam("productManagementActionForm.allCheckBoxValue", allCheckBoxValue));
        form.appendChild(createHiddenInputParam("productManagementActionForm.headers", headers));
        form.appendChild(createHiddenInputParam("productManagementActionForm.attributeCodes", attributeCodes));
    };
    
    function setParamForExportCSVDetails(form, syouhinSysCodes, allCheckBoxValue, headers, attributeCodes, sortField, sortDir) {
        //Filter of grid
    	var params = getDetailSearchParams();
    	for (var prop in params) {
    		if (params[prop] != null && params[prop] != undefined) {
    			form.appendChild(createHiddenInputParam(prop, params[prop]));
    		}
    	}
        //Add list syouhinSysCodes.
        form.appendChild(createHiddenInputParam("productManagementActionForm.syouhinSysCodes", syouhinSysCodes));
        form.appendChild(createHiddenInputParam("productManagementActionForm.allCheckBoxValue", allCheckBoxValue));
        form.appendChild(createHiddenInputParam("productManagementActionForm.headers", headers));
        form.appendChild(createHiddenInputParam("productManagementActionForm.attributeCodes", attributeCodes));
    };
    
    $("#btnCSVStartExport").on("click", function() {
        var exportMode = $("#csvmode").val();
        var requestURL = "";
        var allCheckBoxValue = $("#selectAllCheckbox").prop("checked");
        var syouhinSysCodes = "";
        var headers = "";
        //If all product checkBox is checked
        if (allCheckBoxValue) {
            //Check number of product to export is over max.
            var maxNumberExport = jQuery("#maxNumberProductExportCSV").val();
            if($("#grid").data("kendoGrid").dataSource.total() > maxNumberExport) {
                //Show error message
                $.when(kendo.ui.ExtAlertDialog.show({
                    title: "", 
                    label: jQuery("#confirmYes").val(),
                    message: $.format(jQuery("#maxProductExportCSVErrorMsg").val(), maxNumberExport),
                    width: 393,
                    icon: "k-ext-information" })
                ).done();
                return;
            }
        } else {
            //All product checkBox not check
            //Get list checked check box in grid.
            $("input:checkbox:checked.productCheckbox").each(function() {
                syouhinSysCodes += "," + $(this).attr("id");
            });
            if (syouhinSysCodes.length > 0) {
                // remove first ","
                syouhinSysCodes = syouhinSysCodes.replace(",", "");
            }
        }
        var attributeCodes = $("#csv_export_attr").val();
        if (attributeCodes != null) {
            attributeCodes = attributeCodes.join();
        } else {
            attributeCodes = "";
        }
        //Check exportMode
        if (exportMode == ALL_EXPORTMODE) {
            $("#exportItems input:checked").each(function() {
                headers += "," + $(this).attr("id");
            });
            if (headers.length > 0) {
                // remove the first ","
                headers = headers.replace(",", "");
            }
            requestURL = "prdMngExportCSVMode1";
        } else if (exportMode == COMPATIBLE_MODEL_EXPORTMODE) {
            requestURL = "prdMngExportCSVMode2";
        } else if (exportMode == ATTRIBUTE_EXPORTMODE) {
            requestURL = "prdMngExportCSVMode3";
        }

        var form  =  document.createElement("form");
        form.setAttribute("method", "post");
        form.setAttribute("action", "./" + requestURL + ".html");
        if (detailSearchFlag == 2) {
        	setParamForExportCSVDetails(form, syouhinSysCodes, allCheckBoxValue, headers, attributeCodes, sortField, sortDir);
        } else {
        	//Create filter param
        	setParamForExportCSV(form, syouhinSysCodes, allCheckBoxValue, headers, attributeCodes, sortField, sortDir);
        }

        document.body.appendChild(form);
        form.submit();

        document.body.removeChild(form);
    });
    //EOE Nguyen.Chuong 2014/03/11
    // end export CSV
    
    // start confirm popup
    $("#confirmDisablePopup").kendoWindow({
        minWidth: 270,
        resizable: false,
        title: windowName,
        modal: true,
        visible: false,
    });
    $("#confirmEnablePopup").kendoWindow({
        minWidth: 270,
        resizable: false,
        title: windowName,
        modal: true,
        visible: false,
    });
    // end confirm popup
    $("#pleaseSelectDialog").kendoWindow({
        minWidth: 270,
        title : $("#selectWarning").val(),
        resizable : false,
        modal : true,
        visible : false,
        draggable : false
      });
    $("#cannotEnableDialog").kendoWindow({
        minWidth: 270,
        title : windowName,
        resizable : false,
        modal : true,
        visible : false,
        draggable : false
    });
    $("#cannotDisableDialog").kendoWindow({
        minWidth: 270,
        title : windowName,
        resizable : false,
        modal : true,
        visible : false,
        draggable : false
    });
    $("#warningEnablePopup").kendoWindow({
        minWidth: 270,
        title : windowName,
        resizable : false,
        modal : true,
        visible : false,
        draggable : false
    });
    $("#warningDisablePopup").kendoWindow({
        minWidth: 270,
        title : windowName,
        resizable : false,
        modal : true,
        visible : false,
        draggable : false
    });
    $("#changeStatusPopup").kendoWindow({
    	minWidth: 350,
        resizable: false,
        title: windowName,
        modal: true,
        visible: false,
        close: function () {
          $("#cbxValidate").css("display", "none");
          // reset kendo dropdown list
          $("#cbxPrdSupplierStatus").data("kendoDropDownList").select($("#cbxPrdSupplierStatus").data("kendoDropDownList").ul.children().eq(0));
        },
    });
    $("#confirmMaintenancePopup").kendoWindow({
        minWidth: 270,
        title : windowName,
        resizable : false,
        modal : true,
        visible : false,
        draggable : false
    });
    $("#cannotMaintenancePopup").kendoWindow({
        minWidth: 270,
        title : windowName,
        resizable : false,
        modal : true,
        visible : false,
        draggable : false
    });
    $("#warningChangeStatusPopup").kendoWindow({
        minWidth: 270,
        title : windowName,
        resizable : false,
        modal : true,
        visible : false,
        draggable : false
    });
    $("#cannotUpdateStatus").kendoWindow({
        minWidth: 270,
        title : windowName,
        resizable : false,
        modal : true,
        visible : false,
        draggable : false
    });
    $("#allProductsAreImportPopup").kendoWindow({
        minWidth: 270,
        title : windowName,
        resizable : false,
        modal : true,
        visible : false,
        draggable : false
    });
    $("#confirmChangeStatus").kendoWindow({
        minWidth: 270,
        resizable: false,
        title: windowName,
        modal: true,
        visible: false,
    });
    /*EOE init popup*/
    $("#txtMatterNo").on({
      keypress: function(e) {
        if (e.keyCode == 13) {
          processMaintenanceProduct();
        }
      },
      focusout: function(e) {
        if (!buttonClicked) {
          processMaintenanceProduct();
        } else {
          buttonClicked = false;
        }
      }
    });
    // set buttonClicked to true to prevent focustout event
    $("#btnCancelMaintenance").mousedown(function () {
      buttonClicked = true;
    });
    $(document).on("keypress", "#grid input.tbSearch", function(e) {
        if (e.keyCode == 13) {
            clickButtonSearch();
        }
    });
    //BOE Nguyen.Chuong 2014/03/27 move code change to kendo combobox.
//    $("#tblProductDelFlg").change(function () {
//        clickButtonSearch();
//    });
//    $("#tblSupplierStatusName").change(function () {
//        clickButtonSearch();
//    });
//    $("#tblProductMaintenanceFlg").change(function () {
//        clickButtonSearch();
//    });
//    $("#tblProductStatusName").change(function () {
//        clickButtonSearch();
//    });
//    $("#tblFullName").change(function () {
//        clickButtonSearch();
//    });
    //EOE Nguyen.Chuong 2014/03/27
    $("#btnSearch").click(function () {
        clearMsgText();
        clickButtonSearch();
    });
    $("#btnReset").click(function () {
      // use this line of code to prevent bug in firefox.
      window.location = window.location.pathname;
    });
    // handle check all
    handleSelectAll("#selectAllCheckbox", "#grid table tbody tr", ".productCheckbox", selectedKendoClass, selectedRowClass);
    // process syouhinSysCode when check all and uncheck all
    $("#selectAllCheckbox").change(function() {
      var checked = $(this).prop("checked");
      if(checked) {
        var mainGrid = $("#grid").data("kendoGrid");
        syouhinSysCodeToPreview = mainGrid._data[0].syouhinSysCode;
      } else {
        syouhinSysCodeToPreview = "";
      }
    });
    $(".btnCancel").click(function () {
//      productDelFlg = $("#tblProductDelFlg").val().trim();
      closePopup();
    });
    $("#btnDisable").click(function () {

    	productDelFlgStatus = 1;
      if (!$("#selectAllCheckbox").prop("checked")) {
	      var canDelete = checkProductCanDelete();
	      if (canDelete == 1) {
	        closePopup();
	        openPopup("cannotEnableDialog");
//	        productDelFlg = $("#tblProductDelFlg").val().trim();
	      } else if (canDelete == 2) {
	        closePopup();
	        openPopup("warningDisablePopup");
	      } else {
	        closePopup();
	        updateProductDelFlg();
	      }
	      return;
       }
      var params;
	  	if (detailSearchFlag == 2) {
	    params = getDetailSearchParams();
	  	} else {
		  params = passParamsToGrid(undefined);
	  	}
//       var params = passParamsToGrid(undefined);
      checkStatus = 0;
      $.ajax({
  	    type: "POST",
  	    url: "ajaxUpdateProductDelFlg.html",
  	    dataType: "JSON",
  	    data: params,
  	    beforeSend: function () {
  	      showProcessBar();
  	    },
  	    success: function (data) {
  	      if (data != null) {
  	          /* Check valid or not get status of request
  	           * -1 : ignore check
  	    	   * 0 : all products can be deleted 
  	    	   * 1 : some products can be deleted 
  	    	   * 2 : no product can be deleted. 
  	    	   */
  	    	   hideProcessBar();
  	    	   checkStatus = data.productManagementActionForm.checkStatus;
	  	       if (checkStatus == 2) {
	  	         closePopup();
	  	         openPopup("cannotEnableDialog");
//	  	         productDelFlg = $("#tblProductDelFlg").val().trim();
	  	       } else if (checkStatus == 1) {
	  	         closePopup();
	  	         // set check status -1 when click OK on dialog
	  	         openPopup("warningDisablePopup");
	  	       } else if (checkStatus == 0){
	  	         closePopup();
//	  	         productDelFlg = $("#tblProductDelFlg").val().trim();
	  	         reloadMainGrid();
	  	       }
  	      }
  	    },
  	    error: function () {
//  	      productDelFlg = $("#tblProductDelFlg").val().trim();
  	      hideProcessBar();
  	    },
  	    complete: function () {

  	    },
     });
    });
    $("#btnEnable").click(function () {
    	checkStatus = 0;
    	productDelFlgStatus = 0;
        if (!$("#selectAllCheckbox").prop("checked")) {
          var canDelete = checkProductCanDelete();
	      if (canDelete == 1) {
	        closePopup();
	        openPopup("cannotDisableDialog");
//	        productDelFlg = $("#tblProductDelFlg").val().trim();
	      } else if (canDelete == 2) {
	        closePopup();
	        openPopup("warningEnablePopup");
	      } else {
	        closePopup();
	        updateProductDelFlg();
	      }
	      return;
        }
        var params;
  	  	if (detailSearchFlag == 2) {
  	    params = getDetailSearchParams();
  	  	} else {
  		  params = passParamsToGrid(undefined);
  	  	}
//        var params = passParamsToGrid(undefined);
    	$.ajax({
    	    type: "POST",
    	    url: "ajaxUpdateProductDelFlg.html",
    	    dataType: "JSON",
    	    data: params,
    	    beforeSend: function () {
    	      showProcessBar();
    	    },
    	    success: function (data) {
    	      if (data != null) {
    	          /* Check valid or not get status of request
    	           * -1 : ignore check
    	    	   * 0 : all products can be deleted 
    	    	   * 1 : some products can be deleted 
    	    	   * 2 : no product can be deleted. 
    	    	   */
    	    	  hideProcessBar();
    	    	  checkStatus = data.productManagementActionForm.checkStatus;
    	          if (checkStatus == 2) {
    	            closePopup();
    	            openPopup("cannotDisableDialog");
//    	            productDelFlg = $("#tblProductDelFlg").val().trim();
    	          } else if (checkStatus == 1) {
    	            closePopup();
    	            // Set checkStatus = -1 when click ok on dialog
    	            openPopup("warningEnablePopup");
    	          } else if (checkStatus == 0) {
    	            closePopup();
//    	            productDelFlg = $("#tblProductDelFlg").val().trim();
                    reloadMainGrid();
    	          }
    	      }
    	    },
    	    error: function () {
//    	      productDelFlg = $("#tblProductDelFlg").val().trim();
    	      hideProcessBar();
    	    },
    	    complete: function () {

    	    },
       });
    });
    $("#btnUpdateStatus").click(function () {
      supplierStatusCode = $("#cbxPrdSupplierStatus").val();
      if (supplierStatusCode == -1) {
    	  // data not select
    	  $("#cbxValidate").show();
        return;
      } else {
        $("#cbxValidate").hide();
        var selectedStatusName = $("#cbxPrdSupplierStatus option:selected").text();
        closePopup();
        $("#spnSelectedStatusName").text(selectedStatusName);
        openPopup("confirmChangeStatus");
      }
    });
    $("#btnMaintenanceOk").mousedown(function () {
      buttonClicked = true;
    });
    $("#btnMaintenanceOk").click(function () {
      matterNo = $("#txtMatterNo").val();
      if (matterNo == "") {
        matterNo = -1;
        // check product can maintenance or not
        // if ok then process copy data
        processPreCopyData();
      } else {
        // set button flg to true
        // mean btnMaintenanceOk is clicked
        isClickBtnMaintenance = true;
        // re-check matter no
        processMaintenanceProduct();
      }
      //jQuery("#btnSearch").click();
    });
    $("#btnConfirmMaintenance").click(function () {
      closePopup();
      productValidateMode = 2;
      copyData();
    });
    $("#btnCannotMaintenance").click(function () {
      closePopup();
      // set popup data for maintenance popup
      popup = $("#maintenancePopup").data("kendoWindow");
    });
    $("#btnCancelConfirmMaintenance").click(function () {
      closePopup();
      // set validate mode to default mode
      productValidateMode = 0;
      // set popup data for maintenance popup
      popup = $("#maintenancePopup").data("kendoWindow");
    });
    $("#btnAllProductsAreImport").click(function () {
      closePopup();
      // set popup data for maintenance popup
      popup = $("#maintenancePopup").data("kendoWindow");
    });
    $("#btnCannotUpdateStatus").click(function () {
      closePopup();
    });
    $("#btnWarningChangeStatus").click(function () {
      closePopup();
      checkStatus = -1;
      updateProductSupplierStatusCode();
    });
    $("#btnWarningEnable").click(function () {
      closePopup();
      checkStatus = -1;
      updateProductDelFlg();
    });
    $("#btnDisableEnable").click(function () {
      closePopup();
      checkStatus = -1;
      updateProductDelFlg();
    });
    $("#btnConfirmChangeStatus").click(function () {
	    if (!$("#selectAllCheckbox").prop("checked")) {
		    var canEdit = checkProductCanEdit();
		    if (canEdit == 1) {
		        closePopup();
		        openPopup("cannotUpdateStatus");
		    } else if (canEdit == 2) {
		        closePopup();
		        openPopup("warningChangeStatusPopup");
		    } else {
		        closePopup();
		        updateProductSupplierStatusCode();
		    }
		    return;
	    }
    	checkStatus = 0;
    	var params;
  	  	if (detailSearchFlag == 2) {
  	    params = getDetailSearchParams();
  	  	} else {
  		  params = passParamsToGrid(undefined);
  	  	}
//    	var params = passParamsToGrid(undefined);
    	
    	$.ajax({
    	    type: "POST",
    	    url: "ajaxUpdateProductSupplierStatusCode.html",
    	    dataType: "JSON",
    	    data: params,
    	    beforeSend: function () {
    	      showProcessBar();
    	    },
    	    success: function (data) {
    	      if (data != null) {
    	    	  /* Check valid or not get status of request
      	           * -1 : ignore check
      	    	   * 0 : all products can be deleted 
      	    	   * 1 : some products can be deleted 
      	    	   * 2 : no product can be deleted. 
      	    	   */
    	    	  hideProcessBar();
    	    	  checkStatus = data.productManagementActionForm.checkStatus;
    	    	  if (checkStatus == 2) {
    	    	     closePopup();
    	    	     openPopup("cannotUpdateStatus");
	    	      } else if (checkStatus == 1) {
		    	     closePopup();
		    	     // Set checkStatus = -1 when click OK on dialog
		    	     openPopup("warningChangeStatusPopup");
	    	      } else if (checkStatus == 0){
	    	        closePopup(); 
	    	        reloadMainGrid();
	    	      }
    	      }
    	    },
    	    error: function () {
    	      hideProcessBar();
    	    },
    	  });
    });
    /*EOE productMangegement ready*/
    
    /*BOE productReview ready*/
    /*************** BOE Spliter park ***************/
    jQuery("#rightReviewBlock").kendoSplitter({
        orientation: "vertical",
        panes: [
            { //descriptionRemarksPreviewBLock
              size: "70px"
            , collapsible : false
            , resizable: true }
          , { //descriptionSentencePreviewBLock
              size: "210px"
            , collapsible : false
            , resizable: true }
          , { //attributePreviewBlock
              size: "146px"
            , collapsible : false
            , resizable: true }
          , { //linkReasonVideoPreviewBlock
              size: "160px"
            , collapsible : false
            , resizable: true }
        ],
        resize: function(e) {
            //resize grid follow the size of block
            resizeGridByBlockId("attributePreviewBlock");
        }
    });
    // init spliter for top description in right block
    jQuery("#descriptionUpperPreviewBlock").kendoSplitter({
        orientation: "horizontal",
        panes: [
            { size: "282px"
            , collapsible : false
            , resizable: true }
          , { 
             collapsible : false
            , resizable: true }
        ]
    });
    // init spliter for bottom link and video in right block
    jQuery("#linkReasonVideoPreviewBlock").kendoSplitter({
        orientation: "horizontal",
        panes: [
            { size: "282px"
            , collapsible : false
            , resizable: true }
          , { 
             collapsible : false
            , resizable: true }
        ],
        resize: function(e) {
            //resize grid follow the size of block
            resizeGridByBlockId("linkReasonPreviewBlock");
            resizeGridByBlockId("videoPreviewBlock");
            
        }
    });

    /*************** EOE Spliter park ***************/
    
    /*************** BOE Init window ***************/
    //Init preview popup kendo window
    jQuery("#previewPopupHTML").kendoWindow({
        width : "auto"
      , height : "auto"
      , minWidth: "570"
      , modal: true
      , visible: false
      , resizable: false
      , open: function ( e) {
          this.wrapper.css({ left: 53 });
          this.wrapper.css({ top: 40 });
        }
      , close: function ( e) {
          //Status of detail load data.
          previewPopupLoadStatus = false;
          //Value mode of preview popup.
          previewPopupMode = "simple";
          toggleToShowDetailBlockPreviewPopup();
      }
    });
    
 // Init image list dialog.
    $("#imageListDialogPreviewPopup").kendoWindow({
        height:420,
        width : 760,
        maxWidth : 800,
        maxWeight : 800,
        title : $("#btnListImage").val(),
        resizable : false,
        modal : true,
        visible : false,
        draggable : true
    });

    //Init video dialog to load video
    $("#videoPlayerDialogPreviewPopup").kendoWindow({
        height:390,
        width : 350,
        maxHeight : 390,
        maxWidth : 350,
        title : "Video Player",
        resizable : false,
        modal : true,
        visible : false,
        draggable : true
    });
    /*************** EOE init window ***************/
    
    /*************** BOE handler for mouse over and click ***************/
    //BOE Nguyen.Chuong: remove preview button
//    //Click btn to call previewPopup.
//    jQuery("#btnPreviewPopup").on("click", function(e){
//        clearMsgText();
//        if ("" !== syouhinSysCodeToPreview) {
//            e.preventDefault();
//            //Load simple data
//            getInitPreviewPopupData(syouhinSysCodeToPreview);
//        } else {
//            //Warning please select popup
//            initPleaseSelectDialog();
//        }
//        
//    });
    //EOE Nguyen.Chuong: remove preview button
    
    // Mouseover event for auto load thumb into image thumb.
    jQuery(document).on("mouseover", ".list_thumb", function() {
        var thumbId = jQuery(this).attr('thumbId');
        if (thumbId === undefined)
            return false;
        // Switch thumb to image thumb
        switchThumb(thumbId);
    });
    
    // Load fancybox when click image thumb.
    jQuery(".fancy_box").fancybox({
        'showCloseButton' : false,
        'padding' : 10,
        'transitionIn' : 'elastic',
        'transitionOut' : 'fade',
        beforeShow: function(){
            // Thumb image is Loading
            isImageThumbLoading = true;
        },
        beforeClose: function() {
            // Thumb image is closed
            isImageThumbLoading = false;
        }
    });
    
    //list image buttom click btnListImage
    jQuery("#btnListImage").click(function() {
        initImageListDiaglog();
    });

    // Switch mode buttom click
    jQuery("#btnSwitchPrevieMode").click(function() {
        // If simple mode then show detail block
        if (previewPopupMode == "simple") {
            //Detail data will auto load ajax.
            //if user click buttom before data loaded then show processing image.
            //Image will be hide when data loaded at end of funtion loadAjaxForPreviewPopup();
            if(!previewPopupLoadStatus) {
                showProcessBar();
                return;
            }
            hideProcessBar();
            // set current mode to detail
            previewPopupMode = "detail";
            // Show detail block
            toggleToShowDetailBlockPreviewPopup();
        } else {
            // if detail mode then hide detail block and set mode to simple
            // set current mode to simple
            previewPopupMode = "simple";
            // Toggle to hide block spliter.
            toggleToShowDetailBlockPreviewPopup();
        }
    });
    /*************** EOE handler for mouse over and click ***************/
    /*EOE productReview ready*/
    
    /* BOE productDetailSearch ready */
	function initDetailSearchDialog() {
        // Get dialog title form hidden.
        var dialogTitle = $("#detailSearchDialogTitle").val();
        
        $("#detailSearchDialog").kendoWindow({
            title: dialogTitle,
            minWidth : 800,
            minHeight : 600,
            resizable : false,
            modal : true,
            visible : false,
            draggable : false,
            open : function() {
                
            }
        });
        
		// Init controls.
		initDetailSearchControls();
    }
    
    var isDetailSearchDialogOpen = false;
    function handleDetailSearchButton() {
      $("#btnDetailSearch").click(function() {
        clearMsgText();
  			if(!isDetailSearchDialogOpen) {
  				showDetailSearchDialog();
  				isDetailSearchDialogOpen = true;
  			} else {
  				showDetailSearchDialog();
  			}
      });
    }
    
    function initDetailSearchCheckBox(checkBoxSelector, inputSelector) {
        $(checkBoxSelector).change(function() {
        	var checked = $(this).prop("checked");
        	if(checked) {
        		$(inputSelector).prop("disabled", checked).addClass("k-state-disabled");	
        	} else {
        		$(inputSelector).prop("disabled", checked).removeClass("k-state-disabled");
        	}
        });
    }
    
    function turnOnDisability(flag) {
		// MultiSearch textArea.
		$("#multiSearchCriteriaTextArea").prop("disabled", !flag);
		
    	/*
    	// Improve performance. It's too slow.
		$("#merchandiseInfo *").prop("disabled", flag);
		$("#whetherSearchCondition *").prop("disabled", flag);
		$("#searchOption *").prop("disabled", flag);
		$("#otherSearchCriteria *").prop("disabled", flag);
		$("#managementRecord *").prop("disabled", flag);

		$("#optionTypeSelect, #option").prop("disabled", true);
		$("#optionTypeSelect").kendoDropDownList().data("kendoDropDownList").enable(false);
		
		// Kendo controls.
		$("#brandSelect").data("kendoComboBox").enable(!flag);
		$("#categorySelect").data("kendoComboBox").enable(!flag);
		$("#supplierSelect").data("kendoComboBox").enable(!flag);
		$("#optionTypeSelect").data("kendoComboBox").enable(!flag);
		$("#deliveryTimeSelect").data("kendoComboBox").enable(!flag);
		$("#attributeSelect").data("kendoComboBox").enable(!flag);
		$("#makerHandlingSituationSelect").data("kendoComboBox").enable(!flag);
		$("#dataManagementSituationSelect").data("kendoDropDownList").enable(!flag);
		$("#variousFlag").data("kendoMultiSelect").enable(!flag);
		
		// MultiSearch textArea.
		$("#multiSearchCriteriaTextArea").prop("disabled", !flag);
		
		// Reset buttons. 
		$("#whetherSearchConditionResetButton").prop("disabled", flag);
		$("#searchOptionResetButton").prop("disabled", flag);
		if(flag) {
			$("#whetherSearchConditionResetButton").addClass("k-state-disabled");
			$("#searchOptionResetButton").addClass("k-state-disabled");
		} else {
			$("#whetherSearchConditionResetButton").removeClass("k-state-disabled");
			$("#searchOptionResetButton").removeClass("k-state-disabled");
		}
		
		if(!flag) {
			var productRegistrationDateChecked = $("#productRegistrationDateCheckBox").prop("checked");
			var productRenovationDateChecked = $("#productRenovationDateCheckBox").prop("checked");
			var lastUpdatedPersonChecked = $("#lastUpdatedPersonCheckBox").prop("checked");
			
			$("#productRegistrationDateFromPicker").data("kendoDateTimePicker").enable(productRegistrationDateChecked);
			$("#productRegistrationDateToPicker").data("kendoDateTimePicker").enable(productRegistrationDateChecked);	
			$("#productRenovationDateFromPicker").data("kendoDateTimePicker").enable(productRenovationDateChecked);
			$("#productRenovationDateToPicker").data("kendoDateTimePicker").enable(productRenovationDateChecked);
			$("#lastUpdatedPersonSelect").data("kendoComboBox").enable(lastUpdatedPersonChecked);
		} else {
			$("#productRegistrationDateFromPicker").data("kendoDateTimePicker").enable(false);
			$("#productRegistrationDateToPicker").data("kendoDateTimePicker").enable(false);	
			$("#productRenovationDateFromPicker").data("kendoDateTimePicker").enable(false);
			$("#productRenovationDateToPicker").data("kendoDateTimePicker").enable(false);
			$("#lastUpdatedPersonSelect").data("kendoComboBox").enable(false);
		}
		*/
    }
    
    function handleManagementRecordCheckBoxes() {
    	$("#productRegistrationDateCheckBox").change(function(){
    		var checked = $(this).prop("checked");
    		$("#productRegistrationDateFromPicker").data("kendoDateTimePicker").enable(checked);
    		$("#productRegistrationDateToPicker").data("kendoDateTimePicker").enable(checked);
    	});
    	
    	$("#productRenovationDateCheckBox").change(function(){
    		var checked = $(this).prop("checked");
    		$("#productRenovationDateFromPicker").data("kendoDateTimePicker").enable(checked);
    		$("#productRenovationDateToPicker").data("kendoDateTimePicker").enable(checked);
    	});
    	
    	$("#lastUpdatedPersonCheckBox").change(function(){
    		var checked = $(this).prop("checked");
    		$("#lastUpdatedPersonSelect").data("kendoComboBox").enable(checked);
    	});
    }
    
    function initDetailSearchControls() {
    	// Init textBox controls.
    	/* BOE : Tran.Thanh : apply style format numeric for all textbox */
    	initNumberTextBox("systemProductCode");
    	//numericTextBox("#systemProductCode");
    	//Utils.bindFormatOnTextfield("groupCode", Utils._TF_NUMBER);
    	initNumberTextBox("groupCode");
    	
    	initNumberTextBox("fromListPrice");
    	initNumberTextBox("toListPrice");
    	initNumberTextBox("fromPartition");
    	initNumberTextBox("toPartition");
        //BOE @rcv! Luong.Dai 2014/08/07 10740: add number textbox for code
        initNumberTextBox("brandCode");
        initNumberTextBox("categoryCode");
        //EOE @rcv! Luong.Dai 2014/08/07 10740: add number textbox for code
    	
    	//numericTextBox("#groupCode");
    	//Utils.bindFormatOnTextfield("makerStockNumber", Utils._TF_NUMBER);
    	//numericTextBox("#makerStockNumber");
    	/* EOE : Tran.Thanh : apply style format numeric for all textbox */
        
        // Set message here.
        var brandNoExistMessage = $("#brandNoExistMessage").val();
        var categoryNoExistMessage = $("#categoryNoExistMessage").val();
        var deliveryTimeNoExistMessage = $("#deliveryTimeNoExistMessage").val();
        var supplierNoExistMessage = $("#supplierNoExistMessage").val();
        var optionTypeNoExistMessage = $("#optionTypeNoExistMessage").val();
        var makerHandlingSituationNoExistMessage = $("#makerHandlingSituationNoExistMessage").val();
        var lastUpdatedPersonMessage = $("#lastUpdatedPersonMessage").val();

        // Init select controls.
        // initDetailSearchComboBox("#brandSelect", "loadBrandList.html", "brandList", "brandCode", "name", brandNoExistMessage);
        initComboBoxBrandName();
        // initDetailSearchComboBox("#categorySelect", "loadCategoryList.html", "categoryList", "categoryCode", "categoryName", categoryNoExistMessage);
        initComboBoxCategoryName();
        initDetailSearchComboBox("#deliveryTimeSelect", "loadNoukiList.html", "noukiList", "noukiCode", "noukiName1", deliveryTimeNoExistMessage);
        // initDetailSearchComboBox("#supplierSelect", "loadSiireList.html", "siireList", "siireCode", "siireName", supplierNoExistMessage);
        initComboBoxSiireName();
        initDetailSearchComboBox("#optionTypeSelect", "loadSyouhinSelectList.html", "syouhinSelectList", "selectCode", "name", optionTypeNoExistMessage);
        initDetailSearchComboBox("#makerHandlingSituationSelect", "loadProductSupplierStatusList.html", "productSupplierStatusList", "supplierStatusCode", "supplierStatusName", makerHandlingSituationNoExistMessage);
        initDetailSearchComboBox("#lastUpdatedPersonSelect", "loadUserList.html", "userList", "userId", "fullName", lastUpdatedPersonMessage);
        initDetailSearchDropDownList("#dataManagementSituationSelect", "loadProductDeleteFlagList.html", "productDeleteFlagList", "value", "name");
        initDetailSearchMultiSelect("#variousFlag", "loadVariousFlagList.html", "variousFlagList", "key", "displayName");
        initAttributeComboBox();
        
        // Init numeric controls.
        initDetailSearchNumericTextBox("#fromListPrice");
        initDetailSearchNumericTextBox("#toListPrice");
        initDetailSearchNumericTextBox("#fromPartition");
        initDetailSearchNumericTextBox("#toPartition");
        
        //Init and handle date time picker.
        initDateTimePicker("#productRegistrationDateFromPicker");
        initDateTimePicker("#productRegistrationDateToPicker");
        initDateTimePicker("#productRenovationDateFromPicker");
        initDateTimePicker("#productRenovationDateToPicker");
        handleRegistrationDateTimePicker();
        handleRenovationDateTimePicker();
        handleManagementRecordCheckBoxes();
        
        // Init checkBoxes.
        initDetailSearchCheckBox("#janCheckBox", "#jan");
        initDetailSearchCheckBox("#noteCheckBox", "#note");
        initDetailSearchCheckBox("#summaryCheckBox", "#summary");
        initDetailSearchCheckBox("#explanationCheckBox", "#explanation");
        initDetailSearchCheckBox("#cautionCheckBox", "#caution");
        initDetailSearchCheckBox("#supportedBikeCheckBox", "#supportedBike");
        initDetailSearchCheckBox("#bikeModelCheckBox", "#bikeModel");
        initDetailSearchCheckBox("#optionCheckBox", "#option");
        initDetailSearchCheckBox("#thumbnailImagePathCheckBox", "#thumbnailImagePath");
        initDetailSearchCheckBox("#detailedImagePathCheckBox", "#detailedImagePath");
        initDetailSearchCheckBox("#attributeDisplayNameCheckBox", "#attributeDisplayName");
        initDetailSearchCheckBox("#attributeManagementValueCheckBox", "#attributeManagementValue");
        initDetailSearchCheckBox("#linkTitleCheckBox", "#linkTitle");
        initDetailSearchCheckBox("#videoTitleCheckBox", "#videoTitle");
        
        // Init multiCheck radio buttons.
        initMultiCheckRadioButtons();
        
        // Handle reset buttons.
        handleWhetherSearchConditionResetButton();
        handleSearchOptionResetButton();
        handleOverallResetButton();
        
        // Handle detail search button.
        handleDetailSearchSearchButton();
        
        // Handl textArea.
        handleMultiSearchCriteriaTextArea();
        
        // Reset everything.
        resetControlState();
        
        // Set maxlength.
        setMaxLenForAllInput();
        
        // Get detailSearch params.
        var detailSearchParams = getDetailSearchParams();
        // Get params to compare later.
        var paramsToCompare = getParamsToCompare(detailSearchParams);
    	// Get its token.
        defaultTokenStr = tokenStr = JSON.stringify(paramsToCompare);
    }
    
    /**
     * Clone param object and remove the elements shouldn't be compared.
     * @params params Detail search params.
     * @return newParams Params to compare.
     */
    function getParamsToCompare(params) {
    	var newParams = jQuery.extend({}, params);
    	
    	delete newParams["productManagementActionForm.entMstProduct.productNameSearchOption"];
    	delete newParams["productManagementActionForm.entMstProduct.makerStockNumberSearchOption"];
    	delete newParams["productManagementActionForm.entMstProduct.janSearchOption"];
    	delete newParams["productManagementActionForm.entMstProduct.groupCodeSearchOption"];
    	delete newParams["productManagementActionForm.entMstProduct.noteSearchOption"];
    	delete newParams["productManagementActionForm.entMstProduct.summarySearchOption"];
    	delete newParams["productManagementActionForm.entMstProduct.explanationSearchOption"];
    	delete newParams["productManagementActionForm.entMstProduct.cautionSearchOption"];
    	delete newParams["productManagementActionForm.entMstProduct.supportedBikeSearchOption"];
    	delete newParams["productManagementActionForm.entMstProduct.bikeModelSearchOption"];
    	delete newParams["productManagementActionForm.entMstProduct.optionSearchOption"];
    	delete newParams["productManagementActionForm.entMstProduct.thumbnailImagePathSearchOption"];
    	delete newParams["productManagementActionForm.entMstProduct.detailedImagePathSearchOption"];
    	delete newParams["productManagementActionForm.entMstProduct.attributeDisplayNameSearchOption"];
    	delete newParams["productManagementActionForm.entMstProduct.attributeManagementValueSearchOption"];
    	delete newParams["productManagementActionForm.entMstProduct.linkTitleSearchOption"];
    	delete newParams["productManagementActionForm.entMstProduct.videoTitleSearchOption"];
    	delete newParams["productManagementActionForm.entMstProduct.productRegistrationDateIncluded"];
    	delete newParams["productManagementActionForm.entMstProduct.productRenovationDateIncluded"];
    	delete newParams["productManagementActionForm.entMstProduct.lastUpdatedPersonIncluded"];
    	delete newParams["productManagementActionForm.entMstProduct.multiSearchWhich"];
    	delete newParams["productManagementActionForm.entMstProduct.detailSearchFlag"];
    	delete newParams["productManagementActionForm.entMstProduct.attributeManagementValueType"];
    	return newParams;
    }
    
    function initMultiCheckRadioButtons() {
        $("input[type=radio][name=multiSearch]").change(function(){
        	var id = $(this).attr("id");
        	
        	// Other than "nashi", disable everything except multiSearch.
        	if(id != "multiSearchRadioButton1") {
        		turnOnDisability(true);
        	} else {
        		turnOnDisability(false);
        	}
        });
    }
    
	/**
	 * NOTE: This is a temporary fix for the bug comboBox arrow sign moving up.
	 */
	function fixKendoComboBoxArrowSign(selectSelector) {
		$(selectSelector).parent(".k-widget.k-combobox.k-header").find(".k-icon.k-i-arrow-s").css("margin", "4px 0 0 0");
	}
	
	/**
	 * NOTE: This is a temporary fix for the bug dropDownList arrow sign moving up.
	 */
	function fixKendoDropDownListArrowSign(selectSelector) {
		$(selectSelector).parent(".k-widget.k-dropdown.k-header").find(".k-icon.k-i-arrow-s").css("margin", "4px 0 0 0");
	}
	
	/**
	 * NOTE: This is a temporary fix for the bug loading sign moving up.
	 */
	function fixKendoLoadingSign(selectSelector) {
		$(selectSelector).parent(".k-widget.k-combobox.k-header").find(".k-icon.k-i-arrow-s.k-loading").css("margin", "!important 4px 0 0 0");
	}
	
	/**
	 * Get JSON list for Kendo select.
	 * For example:
	 * <select>
	 * 	<option value="someValue">SomeText</option>
	 *  <option value="someValue2">SomeText2</option>
	 * </select>
	 * @return Json array.
	 */
	function getJsonList(response, listProperty, valueProperty, nameProperty) {
        var newList = [];

        // Copy from old list to new list.
        var list = response.productManagementActionForm[listProperty];
        $.each(list, function(key, value) {
            var newItem = {};
            newItem[valueProperty] = value[valueProperty];
            newItem[nameProperty] = value[nameProperty];
            newList.push(newItem);
        });
        
        return newList;
	}
	
	/**
	 * Get JSON list for Kendo select with blank items.
	 * For example:
	 * <select>
	 * 	<option value=""></option>
	 * 	<option value="someValue">SomeText</option>
	 * </select>
	 * @return Json array.
	 */
	function getJsonListWithBlankItem(response, listProperty, valueProperty, nameProperty) {
        var newList = [];
        
        // Add blank item.
        var blankItem = {};
        blankItem[valueProperty] = "";
        blankItem[nameProperty] = "";
        newList.push(blankItem);

        // Copy from old list to new list.
        var list = response.productManagementActionForm[listProperty];
        $.each(list, function(key, value) {
            var newItem = {};
            newItem[valueProperty] = value[valueProperty];
            newItem[nameProperty] = value[nameProperty];
            newList.push(newItem);
        });
        
        return newList;
	}
    
    function initDetailSearchComboBox(selector, url, listProperty, valueProperty, nameProperty, content) {
        $(selector).kendoComboBox({
            dataTextField: nameProperty,
            dataValueField: valueProperty,
            dataSource: {
                transport: {
                    read: {
                        url: url,
                        dataType: "json",
                        type: "POST",
                    }
                },
				requestStart: function(e) {
					fixKendoLoadingSign(selector);
				},
				requestEnd: function(e) {
				},
                schema: {
                    parse: function(response) {
                    	return getJsonListWithBlankItem(response, listProperty, valueProperty, nameProperty);
                    }
                },
            },
            change: function(e) {
            	if(this.value() && this.selectedIndex == -1) {
            		// Select default.
            		this.value("");
            		
            		initNoExistDialog(content);
            		openNoExistDialog();
            	}
            },
            dataBound: function(e) {
                //fixKendoComboBoxArrowSign(selector);
            }
        });
    }
    
    function initComboBoxBrandName() {
        $("#brandSelect").kendoComboBox({
            filter: "contains",
            change: function(e) {
                if (this.selectedIndex == -1) {
                    //Input value not exits in list
                    errorBrandCode = true;
                    //Change color to pink
                    this.input.addClass('error');
                    $("#brandCode").val("");
                    $("#brandCode").addClass('error');
                } else {
                    errorBrandCode = false;
                    this.input.removeClass('error');
                    $("#brandCode").removeClass('error');
                    // set value again for brandCode
                    if (parseInt(this.value()) == -1) {
                        $("#brandCode").val("");
                    } else {
                        $("#brandCode").val(this.value());
                        var infoBrandList = $('#brandSelect').data("kendoComboBox");
                        infoBrandList.select($(brandCode).val());
//                        infoBrandList.input.focusout();
                        
                    }
                }
            }
        });

        handleInputValueToFilterAtComboBox("#brandArea input", "#brandSelect");
    }

    function initComboBoxCategoryName() {
        $("#categorySelect").kendoComboBox({
            filter: "contains",
            change: function(e) {
                if (this.selectedIndex == -1) {
                    //Input value not exits in list
                    errorCategoryCode = true;
                    //Change color to pink
                    this.input.addClass('error');
                    $("#categoryCode").val("");
                    $("#categoryCode").addClass('error');
                } else {
                    errorCategoryCode = false;
                    this.input.removeClass('error');
                    $("#categoryCode").removeClass('error');
                    // set value again for brandCode
                    if (parseInt(this.value()) == -1) {
                        $("#categoryCode").val("");
                    } else {
                        $("#categoryCode").val(this.value());
                    }
                }
            }
        });

        handleInputValueToFilterAtComboBox("#categoryArea input", "#categorySelect");
    }

    function initComboBoxSiireName() {
        $("#supplierSelect").kendoComboBox({
            filter: "contains",
            change: function(e) {
                if (this.selectedIndex == -1) {
                    //Input value not exits in list
                    errorSiireCode = true;
                    //Change color to pink
                    this.input.addClass('error');
                    $("#siireCode").val("");
                    $("#siireCode").addClass('error');
                } else {
                    errorSiireCode = false;
                    this.input.removeClass('error');
                    $("#siireCode").removeClass('error');
                    // set value again for brandCode
                    if (parseInt(this.value()) == -1) {
                        $("#siireCode").val("");    
                    } else {
                        $("#siireCode").val(this.value());
                    }
                }
            }
        });

        handleInputValueToFilterAtComboBox("#siireArea input", "#supplierSelect");
    }

    /**
    * Handle input value to filter in kendo combobox
    * @author   Luong.Dai
    * @date     2014/08/07
    * @param    selector    selector of input
    * @param    combobox    id of combobox to filter
    */
    function handleInputValueToFilterAtComboBox(selector, combobox) {
        $(document).on('input', selector, function(event) {
            var cbx = $(combobox).data("kendoComboBox");
            cbx.search($(this).val());
        });
    }


    /**
     * Init no exist dialog.
     */
    function initNoExistDialog(content) {
    	var title = $("#noExistDialogTitle").val();
	    $("#noExistDialog").kendoWindow({
	        minWidth: 300,
	        minHeight: 70,
	        title : title,
	        resizable : false,
	        modal : true,
	        visible : false,
	        draggable : false
	    });
	    
	    // Change no exist content.
	    $("#noExistDialogContent").text(content);
	    
	    $("#noExistDialogOkButton").click(function(){
	    	closeNoExistDialog();
	    });
    }
    
    function openNoExistDialog() {
		var noExistDialog = $("#noExistDialog").data("kendoWindow");
		noExistDialog.center().open();
    }
    
    function closeNoExistDialog() {
    	var noExistDialog = $("#noExistDialog").data("kendoWindow");
    	noExistDialog.close();
    }
    
    function initDetailSearchDropDownList(selector, url, listProperty, valueProperty, nameProperty) {
        $(selector).kendoDropDownList({
            dataTextField: nameProperty,
            dataValueField: valueProperty,
            dataSource: {
                transport: {
                    read: {
                        url: url,
                        dataType: "json",
                        type: "POST",
                    }
                },
                schema: {
                    parse: function(response) {
                    	return getJsonListWithBlankItem(response, listProperty, valueProperty, nameProperty);
                    }
                },
            },
            dataBound: function(e) {
                //fixKendoDropDownListArrowSign(selector);
            }
        });
    }
    
    function initDetailSearchMultiSelect(selector, url, listProperty, valueProperty, nameProperty) {
        $(selector).kendoMultiSelect({
            dataTextField: nameProperty,
            dataValueField: valueProperty,
            dataSource: {
                transport: {
                    read: {
                        url: url,
                        dataType: "json",
                        type: "POST",
                    }
                },
                schema: {
                    parse: function(response) {
                        return response.productManagementActionForm[listProperty];
                    }
                },
            },
            dataBound: function(e) {
            }
        });
    }
    
    function initAttributeComboBox() {
    	var attributeSelector = "#attributeSelect";
    	var valuePropperty = "attributeCode";
    	var nameProperty = "attributeName";
    	var typeProperty = "attributeType";
    	var listProperty = "attributeList";
    	
        $(attributeSelector).kendoComboBox({
            dataTextField: nameProperty,
            dataValueField: valuePropperty,
            dataSource: {
                transport: {
                    read: {
                        url: "loadAttributeList.html",
                        dataType: "json",
                        type: "POST",
                    }
                },
                schema: {
                    parse: function(response) {
                        var newList = [];
                        
                        // Add blank item.
                        var blankItem = {};
                        blankItem[valuePropperty] = "";
                        blankItem[nameProperty] = "";
                        newList.push(blankItem);
                        
                        var list = response.productManagementActionForm[listProperty];
                        $.each(list, function(key, value) {
                            var newItem = {};
                            newItem[valuePropperty] = value[valuePropperty] + "_" + value[typeProperty];
                            newItem[nameProperty] = value[nameProperty];
                            newList.push(newItem);
                        });
                        return newList;
                    }
                },
            },
            change: function(e) {
            	if(this.value() && this.selectedIndex == -1) {
            		// Select default value.
            		this.value("");
            		
            		var attributeNoExistMessage = $("#attributeNoExistMessage").val();
            		initNoExistDialog(attributeNoExistMessage);
            		openNoExistDialog();
            		return;
            	}
            	
                // Get selectedItem.
                var selectedIndex = this.select();
                var comboBox = $(attributeSelector).data("kendoComboBox");
                var selectedItem = comboBox.dataItem(selectedIndex);
                
                if(selectedItem != undefined) {
                    var value = selectedItem.attributeCode;
                    var index = value.indexOf("_");
                    if(value != "" && index != -1) {
                    	// Get attribute code and attribute type from the compound string "A_B".
                        var attributeCode = value.substring(0, index);
                        var attributeType = value.substring(index + 1, value.length);
                        
                        // Check attribute type.
                        if(attributeType == "Group") {
                            $("#attributeManagementValueFlagCheckBox").hide();
                            $("#attributeManagementValue").prop("disabled", false).val("").show();
                            
                            // Call API to get attribute group name.
                            loadAttributeGroup(attributeCode);
                            
                        } else if(attributeType == "Flag") {
                            $("#attributeManagementValueFlagCheckBox").show();
                            $("#attributeManagementValue").prop("disabled", true).val("").hide();
                            
                        } else {
                            $("#attributeManagementValueFlagCheckBox").hide();
                            $("#attributeManagementValue").prop("disabled", false).val("").show();
                            
                        }
                    }
                }
            },
            dataBound: function(e) {
            	//fixKendoComboBoxArrowSign(attributeSelector);
            }
        });
    }
    
    function loadAttributeGroup(attributeCode) {
        var url = "loadAttributeGroup.html";
        var data = {"attributeCode": attributeCode};
        
        $.ajax({
            url: url,
            data: data,
            dataType: "json",
            type: "POST",
            beforeSend: function(jqXHR, settings) {
                // Show progress bar.
                showProcessBar();
            },
            success: function(data, textStatus, jqXHR) {
                if(data == null) {
                    hideProcessBar();
                    return;
                }
                
                // Get useful list and init components based on them.
                var dataForm = data.productManagementActionForm;
                var attributeGroupList = dataForm.attributeGroupList;
                
                // Build attributeGroupList value.
                var attributeGroupListValue = "";
                $.each(attributeGroupList, function(key, value) {
                    attributeGroupListValue += value["attributeGroupName"] + ", ";
                });
                attributeGroupListValue = attributeGroupListValue.substring(attributeGroupListValue, attributeGroupListValue.length - 2);
                // Set value.
                $("#attributeManagementValue").val(attributeGroupListValue);
                hideProcessBar();
            },
            error: function(jqXHR, textStatus, errorThrown) {
                hideProcessBar();
            }
        });
    }
    
    function showDetailSearchDialog() {
        // Show detail search dialog.
        var detailSearchDialog = $("#detailSearchDialog").data("kendoWindow");
        detailSearchDialog.center().open();
    }
    
    function resetControlState() {
        // Reset whetherSearchCondition.
        $(".whetherSearchConditionCheckbox").prop("checked", false);
        
        // Reset searchOption.
        resetSearchOptionRadioButtons();
        
        // Reset various flag multiSelect.
        $("#variousFlag").data("kendoMultiSelect").value("");
        
        // Reset managementRecord.
        $(".managementRecordCheckBox").prop("checked", false);
        // Remove error.
        $("#productRegistrationDateFromPicker, #productRegistrationDateToPicker, " +
                "#productRenovationDateFromPicker, #productRenovationDateToPicker").removeClass("error");
        // Reset dateTimePicker checkBoxes.
        $(".managementRecordCheckBox").prop("checked", false);
        
        // Reset multiSearch.
        $("#multiSearchRadioButton1").prop("checked", true);
        
        // Reset textBoxes.
        $(".merchandiseInfoRow input[type=text], .otherSearchCriteriaRow input[type=text]").val("").removeClass("error");
        // Exceptional case (dropDownList).
        var dropDownList = $("#dataManagementSituationSelect").data("kendoDropDownList");
        dropDownList.value(0);
        // Exceptional case (hidden checkBox).
        $("#attributeManagementValueFlagCheckBox").prop("checked", false);
        
        // Reset Kendo controls.
        resetKendoControls();
        
        // Reset textarea.
        $("#multiSearchCriteriaTextArea").val("");
        $("#itemCount").text("0");
        isValideTextArea = true;
        $('#multiSearchCriteriaTextArea_tt_active').css('display', 'none');
        
        // Enable/disable everything.
        turnOnDisability(false);
    }
    
    function resetKendoControls() {
    	$("#brandSelect").data("kendoComboBox").dataSource.filter({});
        $("#brandSelect").data("kendoComboBox").select(0);
        $("#categorySelect").data("kendoComboBox").dataSource.filter({});
        $("#categorySelect").data("kendoComboBox").select(0);
        $("#supplierSelect").data("kendoComboBox").dataSource.filter({});
        $("#supplierSelect").data("kendoComboBox").select(0);
        $("#optionTypeSelect").data("kendoComboBox").dataSource.filter({});
        $("#optionTypeSelect").data("kendoComboBox").select(0);
        $("#deliveryTimeSelect").data("kendoComboBox").dataSource.filter({});
        $("#deliveryTimeSelect").data("kendoComboBox").select(0);
        $("#attributeSelect").data("kendoComboBox").dataSource.filter({});
        $("#attributeSelect").data("kendoComboBox").select(0);
        $("#makerHandlingSituationSelect").data("kendoComboBox").dataSource.filter({});
        $("#makerHandlingSituationSelect").data("kendoComboBox").select(0);
        $("#dataManagementSituationSelect").data("kendoDropDownList").select(1);
        $("#lastUpdatedPersonSelect").data("kendoComboBox").dataSource.filter({});
        $("#lastUpdatedPersonSelect").data("kendoComboBox").select(0);
        $("#productRegistrationDateFromPicker").data("kendoDateTimePicker").value(null);
        $("#productRegistrationDateToPicker").data("kendoDateTimePicker").value(null);
        $("#productRenovationDateFromPicker").data("kendoDateTimePicker").value(null);
        $("#productRenovationDateToPicker").data("kendoDateTimePicker").value(null);
    }
    
    function resetSearchOptionRadioButtons() {
        $("#productNameRadioButton2").prop("checked", true);
        $("#makerStockNumberRadioButton1").prop("checked", true);
        
        $("#janRadioButton2").prop("checked", true);
        $("#groupCodeRadioButton2").prop("checked", true);
        $("#noteRadioButton2").prop("checked", true);
        $("#summaryRadioButton2").prop("checked", true);
        $("#explanationRadioButton2").prop("checked", true);
        $("#cautionRadioButton2").prop("checked", true);
        //BOE #12097 2014/09/10 Nguyen.Chuong: add maker filter in detail search.
        $("#makerRadioButton2").prop("checked", true);
        //EOE #12097 2014/09/10 Nguyen.Chuong: add maker filter in detail search.
        $("#supportedBikeRadioButton2").prop("checked", true);
        $("#bikeModelRadioButton2").prop("checked", true);
        
        $("#optionRadioButton2").prop("checked", true);
        $("#thumbnailImagePathRadioButton2").prop("checked", true);
        $("#detailedImagePathRadioButton2").prop("checked", true);
        
        $("#attributeDisplayNameRadioButton2").prop("checked", true);
        $("#attributeManagementValueRadioButton2").prop("checked", true);
        $("#linkTitleRadioButton2").prop("checked", true);
        $("#videoTitleRadioButton2").prop("checked", true);
    }
    
    function resetMerchandiseInfoTextBoxes() {
        $("#jan").prop("disabled", false).removeClass("k-state-disabled");
        $("#note").prop("disabled", false).removeClass("k-state-disabled");
        $("#summary").prop("disabled", false).removeClass("k-state-disabled");
        $("#explanation").prop("disabled", false).removeClass("k-state-disabled");
        $("#caution").prop("disabled", false).removeClass("k-state-disabled");
        $("#jan").prop("disabled", false).removeClass("k-state-disabled");
        $("#caution").prop("disabled", false).removeClass("k-state-disabled");
        $("#supportedBike").prop("disabled", false).removeClass("k-state-disabled");
        $("#bikeModel").prop("disabled", false).removeClass("k-state-disabled");
        $("#option").prop("disabled", false).removeClass("k-state-disabled");
        $("#thumbnailImagePath").prop("disabled", false).removeClass("k-state-disabled");
        $("#detailedImagePath").prop("disabled", false).removeClass("k-state-disabled");
        $("#attributeDisplayName").prop("disabled", false).removeClass("k-state-disabled");
        $("#attributeManagementValue").prop("disabled", false).removeClass("k-state-disabled");
        $("#linkTitle").prop("disabled", false).removeClass("k-state-disabled");
        $("#videoTitle").prop("disabled", false).removeClass("k-state-disabled");
    }
    
    function handleWhetherSearchConditionResetButton() {
        $("#whetherSearchConditionResetButton").click(function(){
            $(".whetherSearchConditionCheckbox").prop("checked", false);
            
            resetMerchandiseInfoTextBoxes();
        });
    }
    
    function handleSearchOptionResetButton() {
        $("#searchOptionResetButton").click(function(){
            // Reset radioButtons.
            resetSearchOptionRadioButtons();
        });
    }
    
    function handleOverallResetButton() {
        $("#overallResetButton").click(function(){
            // Reset everything.
            resetControlState();
        });
    }

    
    /**
     * Handle multiSearch textArea.
     */
    function handleMultiSearchCriteriaTextArea() {
    	$("#multiSearchCriteriaTextArea").keyup(function(){
    		var value = $(this).val();

    		// Get textArea lines.
    		var lines = getMultiSearchTextAreaLines(value);
    		
    		// Count lines.
    		var lineCount = lines.length;
    		$("#itemCount").text(lineCount);
    	});
    }
    
    function initDetailSearchNumericTextBox(selector) {
    	/* DEPRECATED because it doesn't have k-textbox style.
        $(selector).kendoNumericTextBox({
            format: "#",
            min: numericMin,
            max: numericMax,
            step: 1,
            spinners: false
        });
        */

    	//Utils.bindFormatOnTextfield("selector", Utils._TF_NUMBER);
    	numericTextBox(selector);
    }
    
    /**
     * Init Kendo dateTimePicker.
     * @param selector Selector to init dateTimePicker.
     */
    function initDateTimePicker(selector) {
        $(selector).kendoDateTimePicker({
            interval: 60,
            format: "yyyy/MM/dd HH:mm:ss",
            timeFormat: "HH:mm:ss",
        });
    }
    
    /**
     * Handle registration datetime picker.
     * @param selector string
     * @string boolean
     */
    function handleRegistrationDateTimePicker() {
        // Handle focusout event.
        $("#productRegistrationDateFromPicker").focusout(function() {
            validateDateTimePicker('#productRegistrationDateFromPicker', '#productRegistrationDateFromPicker', '#productRegistrationDateToPicker');
        });
        $("#productRegistrationDateToPicker").focusout(function() {
            validateDateTimePicker('#productRegistrationDateToPicker', '#productRegistrationDateFromPicker', '#productRegistrationDateToPicker');
        });
    }
    
    /**
     * Handle registration datetime picker.
     * @param selector string
     * @string boolean
     */
    function handleRenovationDateTimePicker() {
        // Handle focusout event.
        $("#productRenovationDateFromPicker").focusout(function() {
            validateDateTimePicker('#productRenovationDateFromPicker', '#productRegistrationDateFromPicker', '#productRenovationDateToPicker');
        });
        $("#productRenovationDateToPicker").focusout(function() {
            validateDateTimePicker('#productRenovationDateToPicker', '#productRenovationDateFromPicker', '#productRenovationDateToPicker');
        });
    }
    
    function setMaxLenForAllInput() {
        setInputMaxLen("#systemProductCode", systemProductCodeMaxLen);
//      setInputMaxLenForKendoSelect("#brandSelect", brandMaxLen);
        setInputMaxLen("#productName", productNameMaxLen);
        setInputMaxLen("#makerStockNumber", makerStockNumberMaxLen);
//      setInputMaxLenForKendoSelect("#categorySelect", itemMaxLen);
//      setInputMaxLenForKendoSelect("#supplierSelect", supplierMaxLen);
        setInputMaxLen("#jan", janMaxLen);
        setInputMaxLen("#groupCode", groupCodeMaxLen);
        setInputMaxLen("#note", noteMaxLen);
        setInputMaxLen("#summary", summaryMaxLen);
        /*setInputMaxLen("#explanation", explanationMaxLen);*/
        setInputMaxLen("#caution", cautionMaxLen);
        setInputMaxLen("#supportedBike", supportedBikeMaxLen);
        setInputMaxLen("#bikeModel", bikeModelMaxLen);
//      setInputMaxLenForKendoSelect("#optionTypeSelect", optionTypeMaxLen);
        setInputMaxLen("#option", optionMaxLen);
        setInputMaxLen("#thumbnailImagePath", thumbnailImagePathMaxLen);
        setInputMaxLen("#detailedImagePath", detailedImagePathMaxLen);
//      setInputMaxLenForKendoSelect("#deliveryTimeSelect", deliveryTimeMaxLen);
//      setInputMaxLenForKendoSelect("#attributeSelect", attributeMaxLen);
        setInputMaxLen("#attributeDisplayName", attributeDisplayNameMaxLen);
        /*setInputMaxLen("#attributeManagementValue", attributeManagementValueMaxLen);*/
        setInputMaxLen("#linkTitle", linkTitleMaxLen);
        setInputMaxLen("#videoTitle", videoTitleMaxLen);
        setInputMaxLen("#fromListPrice", listPriceMaxLen);
        setInputMaxLen("#toListPrice", listPriceMaxLen);
        setInputMaxLen("#fromPartition", partitionMaxLen);
        setInputMaxLen("#toPartition", partitionMaxLen);
    }
    
    function setInputMaxLen(selector, maxLen) {
        $(selector).attr("maxlength", maxLen);
    }
    
    function setInputMaxLenForKendoSelect(selector, maxLen) {
        var comboBox = $(selector).data("kendoComboBox");
        comboBox.input.attr("maxlength", maxLen);
    }
    
    function handleDetailSearchSearchButton() {
    	$("#searchButton").click(function(){
    	    if(isValideTextArea
                && !errorCategoryCode
                && !errorBrandCode
                && !errorSiireCode){
    	        loadDetailSearchProductList();
    	    } else {
    	        //alert($('#msgCharacterNumber').val());
    	    	//console.log($('#msgCharacterNumber').val());
    	    }
    	});
    }


    
   
    
    function initGrid() {
    	  var dataSource = new kendo.data.DataSource({
    	      serverPaging: true,
    	      autoBind: false,
    	      serverSorting: true,
    	      // 50 product in 1 page
    	      pageSize: 50,
    	      requestEnd: function(e) {
    	    	  var data = e.response;
    	    	  if(data == null) {
    	    		  return;
    	    	  }
    	    	  
    	    	  // Check if detail search has been done before.
    	    	  detailSearchFlag = data.productManagementActionForm.entMstProduct.detailSearchFlag;
    	    	  if(detailSearchFlag == 2) {
    	    		  // Indicate detail search and filtering are combined (THIS IS A MUST).
//    	    		  detailSearchFlag = 3;
//    	    		  
    	    		  // Reset and disable every filters on main grid.
    	    		  resetAndDisableFiltersOnGrid();

    	    		  // Update grid URI.
//    		    	  var gridSearch = $("#grid").data("kendoGrid");
//    		    	  gridSearch.dataSource.options.transport.read.url = "loadDetailSearchProductList.html";
//    		    	  gridSearch.dataSource.options.transport.read.data = passParamsToGrid;
    	    	  }
    	      },
    	      transport: {
    	          read: {
    	              type: "POST",
    	              dataType: "json",
    	              url: "loadDetailSearchProductList.html",
    	              data:  function(data) {
    	            	  var params;
    	            	  if (detailSearchFlag == 2) {
    	            		  // get sort field 
    	        			var sortField = "syouhinSysCode";
    	        			var sortDir = "ASC";
    	        			if (sortResetFlg != 1) {
    	        				if (data != undefined && data.sort != undefined && data.sort.length > 0) {
    	        					sortField = data.sort[0].field;
    	        					sortDir = data.sort[0].dir;
    	        				}
    	        			} else {
    	        				if (data != undefined && data.sort != undefined && data.sort.length > 0) {
    	        					data.sort[0].field = '';
    	        					data.sort[0].dir = '';
    	        				}
    	        			}
    	        			if (sortField == "updatedOnString") {
    	        				sortField = "updatedOn";
    	        			} else if (sortField == "delFlgString") {
    	        				sortField = "productDelFlg";
    	        			}
    	            	    params = getDetailSearchParams();
    	            	    params["productManagementActionForm.entMstProduct.sortField"] = sortField;
    	            	    params["productManagementActionForm.entMstProduct.sortDir"] = sortDir;
    	            	  } else {
    	            		  params = passParamsToGrid(data);
    	            	  }
    	            	  return params;
    	              },
    	              beforeSend :function() {
    	                showProcessBar();
    	              },
    	              complete: function(data) {
    	                  //Set groupCode param search value to groupCodeSearch text box.
    	                  jQuery("#tblProductGroupCode").val(productGroupCode);
    	                  //Set brandName param search value to brandNameSearch text box.
    	                  jQuery("#tblBrandName").val(brandName);
    	                  var checked = $("#selectAllCheckbox").prop("checked");
    	                  if (checked) {
    	                    $("#selectAllCheckbox").prop("checked", false);
    	                  }
    	                  hideProcessBar();
    	              },
    	              cache: false,
    	          }
    	      },
    	      schema: {
    	          data: "productManagementActionForm.prdList",
    	          total: "productManagementActionForm.totalRecord"
    	      },
    	      error: function(e) {
    	    	//alert(e.errors);
    		    console.log(e.errors); // displays "Invalid query"
    		  }
    	  });

    	  grid = $("#grid").kendoGrid({
    	    dataSource: dataSource,
    	    sortable: {
    	        mode: "single",
    	        allowUnsort: false
    	    },
    	    pageable: {
    	        refresh: true,
    	        pageSizes: [50, 100],
    	        buttonCount: 5,
    	        messages: {
    	            display: jQuery("#pagingDisplay").val(),
    	            empty: jQuery("#pagingEmpty").val(),
    	            itemsPerPage: jQuery("#pagingItemsPerPage").val(),
    	            first: jQuery("#pagingFirst").val(),
    	            previous: jQuery("#pagingPrevious").val(),
    	            next: jQuery("#pagingNext").val(),
    	            last: jQuery("#pagingLast").val()
    	        }
    	    },
    	    columns: [{
    	        title: "",
    	        template: "<input type='checkbox' name='' class='productCheckbox' id='#= syouhinSysCode#' value='#= productStatusCanEditFlg#, #= productStatusCanDeleteFlg#'/>",
    	        attributes: {
    	            "class": "align-center"
    	        },
    	        width: "30px"
    	    }, {
    	        field: "syouhinSysCode",
    	        type: "number",
    	        title: jQuery("#titleSyouhinSysCode").val(),
    	        width: "95px",
    	        attributes: {
    	            "class": "align-right"
    	        }
    	    }, {
    	        field: "brandName",
    	        type: "string",
    	        title: jQuery("#titleBrandName").val(),
    	        width: "150px",
    	        attributes: {
    	            "class": "align-left"
    	        }
    	    }, {
    	        field: "productCode",
    	        type: "string",
    	        title: jQuery("#titleProductCode").val(),
    	        width: "150px",
    	        attributes: {
    	            "class": "align-left, row_table"
    	        }
    	    }, {
    	        field: "productName",
    	        type: "string",
    	        title: jQuery("#titleProductName").val(),
    	        width: "360px",
    	        attributes: {
    	            "class": "align-left, row_table"
    	        }
    	    }, {
    	        field: "productGroupCode",
    	        type: "number",
    	        title: jQuery("#titleProductGroupCode").val(),
    	        width: "100px",
    	        attributes: {
    	            "class": "align-center, row_table"
    	        }
    	    }, {
    	        field: "descriptionSummary",
    	        template: function(data) {
    	                        var descriptionSummaryTxt = data.descriptionSummary;
    	                        if ("" != descriptionSummaryTxt && null != descriptionSummaryTxt) {
    	                            var elementFormat = "<div title='{0}'>{1}</div>";
    	                            descriptionSummaryTxt = descriptionSummaryTxt.replace(/</g, "&lt;");
    	                            descriptionSummaryTxt = descriptionSummaryTxt.replace(/>/g, "&gt;");
    	                            var descriptionSummaryShortTxt = shortenText(descriptionSummaryTxt == null ? '' : descriptionSummaryTxt, 14);
    	                            return jQuery.format(elementFormat, descriptionSummaryTxt,descriptionSummaryShortTxt);
    	                        }
    	                        return "";
    	                  },
    	        type: "string",
    	        title: jQuery("#titleSummary").val(),
    	        width: "190px",
    	        attributes: {
    	            "class": "align-left, row_table"
    	        }
    	    }, /*{
    	        field: "hyoujiName",
    	        template: "<div title='#= hyoujiName #'>#= shortenText(hyoujiName == null ? '' : hyoujiName, 8) #</div>",
    	        type: "string",
    	        title: jQuery("#titleHyoujiName").val(),
    	        width: "120px",
    	        attributes: {
    	            "class": "align-left"
    	        }
    	    }, */{
    	        field: "productProperPrice",
    	        type: "number",
    	        title: jQuery("#titleProductProperPrice").val(),
    	        width: "100px",
    	        attributes: {
    	            "class": "align-right"
    	        },
    	        template: function (myData) {
    	          return kendo.toString(myData.productProperPrice, "n0");
    	        },
    	    }, {
    	        field: "rate",
    	        type: "number",
    	        title: jQuery("#titleRate").val(),
    	        width: "100px",
    	        attributes: {
    	            "class": "align-right"
    	        },
    	        template: function (myData) {
    	        	if (myData.rate != null) {
    	        		return myData.rate + "%";
    	        	}
    	        	return "0%";
    	        },
    	    }, {
    	        field: "siireName1",
    	        type: "string",
    	        title: jQuery("#titleSiireName1").val(),
    	        width: "225px",
    	        attributes: {
    	            "class": "align-center"
    	        }
    	    }, {
    	        field: "siireName2",
    	        type: "string",
    	        title: jQuery("#titleSiireName2").val(),
    	        width: "150px",
    	        attributes: {
    	            "class": "align-center"
    	        }
    	    }, {
    	        field: "supplierStatusName",
    	        type: "string",
    	        title: jQuery("#titleSupplierStatusName").val(),
    	        width: "125px",
    	        attributes: {
    	            "class": "align-center"
    	        }
    	    }, {
    	        field: "productStatusName",
    	        type: "string",
    	        title: jQuery("#titleProductStatusName").val(),
    	        width: "125px",
    	        attributes: {
    	            "class": "align-center"
    	        }
    	    }, {
    	        field: "productMaintenanceFlg",
    	        type: "string",
    	        //BOE: Nguyen.Chuong 2014/03/15: Show メンテナンス when productMaintenanceFlg = 1
    	        template: function(myData){
    	            if(myData.productMaintenanceFlg == 1) {
                        return '<span class="maintainancedText">' + jQuery("#maintainancedText").val() + '</span>';
    	            }
    	            return "";
    	        },
    	        //EOE: Nguyen.Chuong 2014/03/15: Show メンテナンス when productMaintenanceFlg = 1
    	        title: jQuery("#titleProductMaintenanceFlg").val(),
    	        width: "125px",
    	        attributes: {
    	            "class": "align-center"
    	        }
    	    }, {
    	        field: "delFlgString",
    	        type: "number",
    	        title: jQuery("#titleProductDelFlg").val(),
    	        width: "70px",
    	        attributes: {
    	            "class": "align-center"
    	        }
    	    }, {
    	        field: "fullName",
    	        type: "string",
    	        title: jQuery("#titleFullName").val(),
    	        width: "150px",
    	        attributes: {
    	            "class": "align-center"
    	        }
    	    }, {
    	        field: "updatedOnString",
    	        type: "date",
    	        title: jQuery("#titleUpdatedOn").val(),
    	        width: "250px",
    	        attributes: {
    	            "class": "align-center"
    	        }
    	    }, ],
    	    height: 600,
    	    selectable: "multiple, row",
    	    resizable: true,
    	    scrollable: true,
    	    dataBound: function (e) {
    	        // Remove noData message
    	        jQuery("#grid .dataTables_empty").remove();
                var currentRow = "";
                var maintenanceCell;
                var matterInfo;
                var tooltipContent = "";
    	        if (this.dataSource.total() > 0) {
                    var gridData = this.dataSource.view();
                    var gridLength = gridData.length;
                    //add tooltip for product maintenance
                    for (var i = 0; i < gridLength; i++) {
                        if (gridData[i].productMaintenanceFlg == 1) {
                            tooltipContent = "";
                            //Get matter info
                            matterInfo = gridData[i].matterInfo;
                            if (gridData[i].hasMaintenanceProduct == false) {
                                tooltipContent = $('#matterOfMaintenanceNotFound').val();
                            } else if (null != matterInfo) {
                                //Show tooltip for product not release
                                tooltipContent = createTooltipContentMatter(matterInfo);
                            }

                            if (tooltipContent != "") {
                                //add tooltip
                                currentRow = this.table.find("tr[data-uid='" + gridData[i].uid + "']");
                                maintenanceCell = $(currentRow).find(".maintainancedText");
                                $(maintenanceCell).kendoTooltip({
                                    content: tooltipContent
                                });
                            }
                        }
                    }
    	        } else {
    	            // Show no data message
    	            jQuery("#grid .k-grid-content").append(
    	                    '<div class="dataTables_empty" id="gridEmptyElement">'
    	                        + jQuery("#noDataMessage").val() 
    	                        + '</div>');
    	        }
    	        //Set syouhin_sys_code for preview popup
    	        setSyouhinSysCodeForPreviewPopup();
    	        
    	        // Add Kendo tooltip for summary.
    	        var listSummary = $("#grid .k-grid-content tr td:nth-child(7)");
    	        var divChild;
    	        var valueChild;
    	        //Loop to get summary data to show title.
    	        for (var i = 0; i < listSummary.length; i++) {
    	            divChild = jQuery(listSummary[i]).find("div");
    	            if(divChild.length > 0) {
    	                valueChild = jQuery(divChild).attr("title");
    	                valueChild = valueChild.replace(/</g, "&lt;");
    	                valueChild = valueChild.replace(/>/g, "&gt;");
    	                //Create kendo tooltip
    	                jQuery(divChild).kendoTooltip({
    	                    content: valueChild
    	                });
    	            }
    	            
    	        }
    	        // Handle selecting rows with checkBoxes.
    	        handleSelectingRow();
    	    },
    	    change: function (data) {
    	      //Set syouhin_sys_code for preview popup
    	      setSyouhinSysCodeForPreviewPopup();
    	    },
    	  });
    	  // add filter
    	  grid.data("kendoGrid").thead.append(filterHeader);
    	  // set textbox for only input number
    	  setNumbericInput();
    	  $('#tblSupplierStatusName').kendoComboBox({
              change: function(e) {
                  clickButtonSearch();
              }
          });
    	  $('#tblProductMaintenanceFlg').kendoDropDownList({
              change: function(e) {
                  clickButtonSearch();
              }
          });
    	  $('#tblProductStatusName').kendoComboBox({
              change: function(e) {
                  clickButtonSearch();
              }
          });
    	  $('#tblProductDelFlg').kendoDropDownList({
    	      change: function(e) {
    	          clickButtonSearch();
    	      }
    	  });
    	  $('#tblFullName').kendoComboBox({
              change: function(e) {
                  clickButtonSearch();
              }
          });
    	  $("#tblUpdatedFrom").kendoDateTimePicker({
    	      interval: 60,
    	      format: "yyyy/MM/dd HH:mm:ss",
    	  });
    	  $("#tblUpdatedTo").kendoDateTimePicker({
    	      interval: 60,
    	      format: "yyyy/MM/dd HH:mm:ss",
    	  });
    	    
  /* BOE TRAN.THANH 2014/03/24 
   * Add validate date format.
   */
  function handleDateTimePicker() {
      // Handle focusout event.
      $("#tblUpdatedFrom").focusout(function() {
      	validateDateTimePicker('#tblUpdatedFrom', '#tblUpdatedFrom', '#tblUpdatedFrom');
      });
      $("#tblUpdatedTo").focusout(function() {
      	validateDateTimePicker('#tblUpdatedTo', '#tblUpdatedTo', '#tblUpdatedTo');
      });
  }
  /* EOE TRAN.THANH 2014/03/24 */
  handleDateTimePicker();
    	};

    
    function loadDetailSearchProductListHelper(detailSearchParams, newTokenStr) {
		// Indiate detail search is used. (THIS IS A MUST).
		detailSearchParams["productManagementActionForm.entMstProduct.detailSearchFlag"] = detailSearchFlag = 2;
		
		// Reload grid with detail search params.
		updateGridDataSource("loadDetailSearchProductList.html", detailSearchParams);
		
		// Store new token string for later checking.
		tokenStr = newTokenStr;
    }
    
    function loadDetailSearchProductList() {
    	// Get detailSearch params.
    	var detailSearchParams = getDetailSearchParams();
    	// Get params to compare.
    	var paramsToCompare = getParamsToCompare(detailSearchParams);
    	// Get its token.
    	var newTokenStr = JSON.stringify(paramsToCompare);
    	
    	// If no modification compared to default state.
//    	if(newTokenStr == defaultTokenStr) {
//    		initNoChangeDefaultDialog();
//    		showNoChangeDefaultDialog();
//    	} 
    	
    	// If no modification compared to previous state.
//    	else 
    	    if(newTokenStr == tokenStr) {
    		initNoChangeDialog();
    		showNoChangeDialog();
    	}
    	
    	// If search fields are modified.
    	else {
    		loadDetailSearchProductListHelper(detailSearchParams, newTokenStr);
    		closeDetailSearchDialog();
    		
    		// Show info bar.
    		$('#msAdvanceSearch').show();
    		
    	}
    }
    
    /**
     * Close no change default dialog.
     */
    function closeNoChangeDefaultDialog() {
        var dialog = $("#noChangeDefaultDialog").data("kendoWindow");
        dialog.close();
    }
    
    /**
     * Show no change default dialog.
     */
    function showNoChangeDefaultDialog() {
        var dialog = $("#noChangeDefaultDialog").data("kendoWindow");
        dialog.center().open();
    }
    
    /**
     * Init no change default dialog. This dialog appears when dialog inputs doesn't change.
     */
    function initNoChangeDefaultDialog() {
    	var noChangeDefaultDialogTitle = $("#noChangeDefaultDialogTitle").val();
		var noChangeDefaultDialogText = $("#noChangeDefaultDialogText").val();
		
	    $("#noChangeDefaultDialog").kendoWindow({
	        minWidth: 300,
	        minHeight: 70,
	        title : noChangeDefaultDialogTitle,
	        resizable : false,
	        modal : true,
	        visible : false,
	        draggable : false
	    });
	    $("#noChangeDefaultDialogContent").text(noChangeDefaultDialogText);
	    $("#noChangeDefaultDialogOkButton").click(function(){
	    	closeNoChangeDefaultDialog();
	    });
    }
    
    /**
     * Close detail search dialog.
     */
    function closeDetailSearchDialog() {
        var dialog = $("#detailSearchDialog").data("kendoWindow");
        dialog.close();
    }
    
    /**
     * Close no change dialog.
     */
    function closeNoChangeDialog() {
        var dialog = $("#noChangeDialog").data("kendoWindow");
        dialog.close();
    }
    
    /**
     * Show no change dialog.
     */
    function showNoChangeDialog() {
        var dialog = $("#noChangeDialog").data("kendoWindow");
        dialog.center().open();
    }
    
    /**
     * Init no change dialog. This dialog appears when user doesn't change anything at all in detail search dialog.
     */
    function initNoChangeDialog() {
    	var noChangeDialogTitle = $("#noChangeDialogTitle").val();
		var noChangeDialogText = $("#noChangeDialogText").val();
		
	    $("#noChangeDialog").kendoWindow({
	        minWidth: 300,
	        minHeight: 70,
	        title : noChangeDialogTitle,
	        resizable : false,
	        modal : true,
	        visible : false,
	        draggable : false
	    });
	    $("#noChangeDialogContent").text(noChangeDialogText);
	    $("#noChangeDialogOkButton").click(function(){
	    	closeNoChangeDialog();
	    	closeDetailSearchDialog();
            // Show info bar.
            $('#msAdvanceSearch').show();
	    });
    	$("#noChangeDialogCancelButton").click(function(){
    		closeNoChangeDialog();
	    });
    }
    
    /**
     * Update grid dataSource.
     * @param uri URI.
     * @param params Param.
     */
    function updateGridDataSource(uri, params) {
    	/*var gridSearch = $("#grid").data("kendoGrid");
    	gridSearch.dataSource.options.transport.read.url = uri;
    	if (params["productManagementActionForm.entMstProduct.sortField"] == null || params["productManagementActionForm.entMstProduct.sortField"] == '') {
    		params["productManagementActionForm.entMstProduct.sortField"] = 'syouhinSysCode';
    	}
    	gridSearch.dataSource.options.transport.read.data = params;
    	gridSearch.dataSource.read();
    	gridSearch.refresh();    */

    	  grid.data("kendoGrid").dataSource.page(1);
    	  grid.data("kendoGrid").content.scrollTop(0);
    }
    
    initDetailSearchDialog();
    handleDetailSearchButton();
    /* EOE productDetailSearch ready */
    
    $("#csvmode").kendoDropDownList({
        change: function() {
            $("#lstButton .k-dropdown-wrap").css("background-color", "none");
            $("#exportItems ul").css('display','none');
            $("#field_csv_export_attr").css('display','none');
            
            var id_ul_mod = "#csv_ul_" + $("#csvmode").val();
            $(id_ul_mod).css('display','');
            switch ($("#csvmode").val()) {
                case ALL_EXPORTMODE:
                    $(id_ul_mod + ' :checkbox').attr({"disabled": false});
                    $(id_ul_mod + ' .chkDisabled').attr({"disabled": true, "checked": true});
                    $("#field_csv_export_attr").css('display','');
                    $("#csv_export_attr_taglist").css('display','');                    
                    $("#csv_export_attr").data("kendoMultiSelect").enable(true);
                    break;
                case COMPATIBLE_MODEL_EXPORTMODE:
                    $(id_ul_mod + ' :checkbox').attr({"disabled": true,"checked": true});
                    $("#field_csv_export_attr").css('display','none');
                    $("#csv_export_attr_taglist").css('display','none');
                    $("#csv_export_attr").data("kendoMultiSelect").enable(false);
                    break;
                case ATTRIBUTE_EXPORTMODE:
                    $(id_ul_mod + ' :checkbox').attr({"disabled": true,"checked": true});
                    $("#field_csv_export_attr").css('display','');
                    $("#csv_export_attr_taglist").css('display','');
                    $("#csv_export_attr").data("kendoMultiSelect").enable(true);
                    break;
            }
            return;
        }
    });
    $('#csv_export_attr option[value=""]').remove();
    $("#csv_export_attr").kendoMultiSelect({});
    $("#csv_export_attr").data("kendoMultiSelect").value("");

    $('#multiSearchCriteriaTextArea_tt_active').css('display', 'none');
    
    function checkRowTextArea(){
        row = $('#multiSearchCriteriaTextArea').val().split("\n");
        newLines = row.length;
        newChars = 0;
        
        for(var i=0; i<newLines; i++){
            charLength = row[i].length;
            if(newChars < charLength){
                newChars = charLength;
            }
        }
        
        if(newChars > chars) {
            $('#multiSearchCriteriaTextArea_tt_active').css('display','block');
            isValideTextArea = false;
        } else {
            $('#multiSearchCriteriaTextArea_tt_active').css('display','none');
            isValideTextArea = true;
        }
    }
    
    function checkLineTextArea(){
        row = $('#multiSearchCriteriaTextArea').val().split("\n");
        newLines = row.length;
        
        if(newLines > lines){
            newrow = row.splice(0, lines);
            value = newrow.join("\n");
            $('#multiSearchCriteriaTextArea').val(value);
            return false;
        }
    }
    $('#multiSearchCriteriaTextArea').bind({
        keyup : function(e) {
            checkRowTextArea();
        },
        keydown : function(e) {
            checkLineTextArea();
        },
        paste : function() {
            setTimeout(function() {
                checkLineTextArea();
                checkRowTextArea();
            }, 0);
        }
    });

    initGrid();

    //BOE @rcv! Luong.Dai 2014/08/07: Add event when type in brand, category and siire code
    $('#brandCode').on("focusout keydown", function (e) {
        if(e.type == "focusout" || e.keyCode == 13) {
            var brandCode = $("#brandCode").val();
            var infoBrandList = $('#brandSelect').data("kendoComboBox");
            infoBrandList.dataSource.filter({});
            infoBrandList.input.removeClass('error');
            $("#brandCode").removeClass("error");
            if (brandCode.length > 0) {
                $("#brandCode").removeClass("error");
                var hasBrandCodeInCbx =$("select#brandSelect option[value='" + brandCode + "']").length; 
                if (hasBrandCodeInCbx > 0) {
                    errorBrandCode = false;
                    //Set select cbxCat
                    infoBrandList.value(brandCode);
                } else {
                    errorBrandCode = true;
                    $("#brandCode").addClass("error");
                    infoBrandList.select(0);
                }
            } else {
                errorBrandCode = false;
                infoBrandList.select(0);
            }
        }
    });

    $('#categoryCode').on("focusout keydown", function (e) {
        if(e.type == "focusout" || e.keyCode == 13) {
            var categoryCode = $("#categoryCode").val();
            var infoCatList = $('#categorySelect').data("kendoComboBox");
            infoCatList.dataSource.filter({});
            infoCatList.input.removeClass('error');
            $("#categoryCode").removeClass("error");
            if (categoryCode.length > 0) {
                $("#categoryCode").removeClass("error");
                var hasCatCodeInCbx =$("select#categorySelect option[value='" + categoryCode + "']").length; 
                if (hasCatCodeInCbx > 0) {
                    errorCategoryCode = false;
                    //Set select cbxCat
                    infoCatList.value(categoryCode);
                } else {
                    errorCategoryCode = true;
                    $("#categoryCode").addClass("error");
                    infoCatList.select(0);
                }
            } else {
                errorCategoryCode = false;
                infoCatList.select(0);
            }
        }
    });

    $('#siireCode').on("focusout keydown", function (e) {
        if(e.type == "focusout" || e.keyCode == 13) {
            var siireCode = $("#siireCode").val();
            var infoSiireList = $('#supplierSelect').data("kendoComboBox");
            infoSiireList.dataSource.filter({});
            infoSiireList.input.removeClass('error');
            $("#siireCode").removeClass("error");

            if (siireCode.length > 0) {
                $("#siireCode").removeClass("error");
                var hasSiireCodeInCbx =$("select#supplierSelect option[value='" + siireCode + "']").length; 
                if (hasSiireCodeInCbx > 0) {
                    errorSiireCode = false;
                    //Set select cbxCat
                    infoSiireList.value(siireCode);
                } else {
                    errorSiireCode = true;
                    $("#siireCode").addClass("error");
                    infoSiireList.select(0);
                }
            } else {
                errorSiireCode = false;
                infoSiireList.select(0);
            }
        }
    });
    //EOE @rcv! Luong.Dai 2014/08/07: Add event when type in brand, category and siire code
});
/************************************************ EOE Document ready ************************************************/ 

/************************************************ BOE Function ************************************************/ 
function passParamsToGrid(data) {
	var sortField = "syouhinSysCode";
	var sortDir = "ASC";
	if (sortResetFlg != 1) {
		if (data != undefined && data.sort != undefined && data.sort.length > 0) {
			sortField = data.sort[0].field;
			sortDir = data.sort[0].dir;
		}
	} else {
		if (data != undefined && data.sort != undefined && data.sort.length > 0) {
			data.sort[0].field = '';
			data.sort[0].dir = '';
		}
	}
	//BOE Nguyen.Chuong 2014/03/19 get and set param from request to search condition.
	//Set group code to search on server if request contain groupCode param
	if(jQuery("#groupCodeParam").val() != "") {
		productGroupCode = jQuery("#groupCodeParam").val();
		//Set param hidden to empty.
		jQuery("#groupCodeParam").val("");
	}
	//Set brand name to search on server if request contain brandCode param
	if(jQuery("#brandCodeParam").val() != "") {
	    brandName = jQuery("#brandNameParam").val();
	    //Set param hidden to empty.
        jQuery("#brandCodeParam").val("");
        jQuery("#brandNameParam").val("");
    }
	//Modify URL to remove groupCode param and brandCode param when exist
	if (window.location.href.indexOf("groupCodeParam") > 0 || window.location.href.indexOf("brandCodeParam") > 0) {
	    //Modify URL to remove groupCode param
	    window.history.pushState({},"","productManagement.html");
	}
	//EOE Nguyen.Chuong 2014/03/19
	if (detailSearchFlag == 3) {
		// get search condition for grid
		getSearchCondition();
	}
	if (sortField == "updatedOnString") {
		sortField = "updatedOn";
	} else if (sortField == "delFlgString") {
		sortField = "productDelFlg";
	}
	// Prepare params.
	var params = setParamToServer(sortField, sortDir);
	return params;
}

/*BOE productMangegement function*/
function resetAndDisableFiltersOnGrid() {
	$("#tblSyouhinSysCode").val("").prop("disabled", true).addClass("k-state-disabled");
	$("#tblBrandName").val("").prop("disabled", true).addClass("k-state-disabled");
	$("#tblProductCode").val("").prop("disabled", true).addClass("k-state-disabled");
	$("#tblProductName").val("").prop("disabled", true).addClass("k-state-disabled");
	$("#tblProductGroupCode").val("").prop("disabled", true).addClass("k-state-disabled");
	$("#tblDescriptionSummary").val("").prop("disabled", true).addClass("k-state-disabled");
	$("#tblProperPriceFrom").val("").prop("disabled", true).addClass("k-state-disabled");
	$("#tblProperPriceTo").val("").prop("disabled", true).addClass("k-state-disabled");
	$("#tblRateFrom").val("").prop("disabled", true).addClass("k-state-disabled");
	$("#tblRateTo").val("").prop("disabled", true).addClass("k-state-disabled");
	$("#tblSiireName1").val("").prop("disabled", true).addClass("k-state-disabled");
	$("#tblSiireName2").val("").prop("disabled", true).addClass("k-state-disabled");
	
	var supplierStatusNameComboBox = $("#tblSupplierStatusName").data("kendoComboBox");
	supplierStatusNameComboBox.select(0);
	supplierStatusNameComboBox.enable(false);
	
	var productMaintenanceFlgDdl = $("#tblProductMaintenanceFlg").data("kendoDropDownList");
	productMaintenanceFlgDdl.select(0);
	productMaintenanceFlgDdl.enable(false);
	
	var productStatusNameComboBox = $("#tblProductStatusName").data("kendoComboBox");
	productStatusNameComboBox.select(0);
	productStatusNameComboBox.enable(false);
	
	var deleteFlagDropDown = $("#tblProductDelFlg").data("kendoDropDownList");
	deleteFlagDropDown.select(0);
	deleteFlagDropDown.enable(false);
	
	var fullNameComboBox = $("#tblFullName").data("kendoComboBox");
	fullNameComboBox.select(0);
	fullNameComboBox.enable(false);
	
	var updateFromDateTimePicker = $("#tblUpdatedFrom").data("kendoDateTimePicker");
	updateFromDateTimePicker.value("");
	updateFromDateTimePicker.enable(false);
	
	var updatToDateTimePicker = $("#tblUpdatedTo").data("kendoDateTimePicker");
	updatToDateTimePicker.value("");
	updatToDateTimePicker.enable(false);
	
	syouhinSysCode = "";
	brandName = "";
	productCode = "";
	productName = "";
	productGroupCode = "";
	descriptionSummary = "";
	productProperPriceFrom = "";
	productProperPriceTo = "";
	rateFrom = "";
	rateTo = "";
	siireName1 = "";
	siireName2 = "";
	supplierStatusName = "";
	productMaintenanceFlg = -1;
	productStatusName = "";
	productDelFlg = 0;
	fullName = "";
	updatedOnFrom = "";
	updatedOnTo = "";
}
//BOE Nguyen.Chuong 2014/03/11: modify to add new param size of text
/**
 * Make text short.
 * @param text full text want to shorten
 * @param size size of short text
 * @return shortened text.
 */
function shortenText(text, size) {
	if(text != "") {
		var num = Math.min(text.length, size);
		return text.substring(0, num) + "...";
	}
	return "";
}
//EOE Nguyen.Chuong 2014/03/11


/**
 * Select rows with checkBoxes.
 */
function handleSelectingRow() {
	$("#grid .k-grid-content tr").on({
		click: function(e) {
			// Get clicked row.
			var row = this;
			var checkedAll = $("#selectAllCheckbox").prop("checked");
			// Check if the clicked element is checkBox or row.
			if($(e.target).is("input[type=checkbox]")) {
				var checkBox = e.target;
				if($(checkBox).prop("checked")) {
					var grid = $("#grid").data("kendoGrid");
					// set to original param to get it when uncheck
					originalSyouhinSysCodeToPreview = syouhinSysCodeToPreview;
					grid.select(row);
					// get id value of checkbox
			        syouhinSysCodeToPreview = checkBox.id;
				} else {
					$(row).removeClass("k-state-selected").removeAttr("aria-selected");
					// set it back to orginal data
					syouhinSysCodeToPreview = originalSyouhinSysCodeToPreview;
				}
			} else {
				// Store selected rows.
				var grid = $("#grid").data("kendoGrid");
				var selected = grid.select();
				// Clear old selected rows and its checkBoxes.
				grid.clearSelection();
				if (checkedAll) {
					$("#selectAllCheckbox").prop("checked", false);
					deselectRow("#grid table tbody tr", ".productCheckbox", selectedKendoClass, selectedRowClass);
					$("#grid .k-grid-content .productCheckbox:checked").prop("checked", true);
				} else {
					$("#grid .k-grid-content .productCheckbox:checked").prop("checked", false);
				}
				
				// Reselect new rows and check new checkBoxes.
				grid.select(selected);
				selected.find(".productCheckbox").prop("checked", true);
			}
		},
		//BOE Nguyen.Chuong2014/03/11: change event show preview popup from click preview button to dbClick row
		//Db click to grid row then show preview popup.
		dblclick: function(e) {
		    clearMsgText();
		    showProcessBar();
	        if ("" !== syouhinSysCodeToPreview) {
	            e.preventDefault();
	            //Load simple data
	            getInitPreviewPopupData(syouhinSysCodeToPreview);
	        } else {
	            //Warning please select popup
	            initPleaseSelectDialog();
	        }
		}
		//EOE Nguyen.Chuong2014/03/11
	});
}

function setParamToServer(sortField, sortDir) {
    return {
        "productManagementActionForm.entMstProduct.syouhinSysCode"            :      syouhinSysCode,
        "productManagementActionForm.entMstProduct.brandName"                 :      brandName,
        "productManagementActionForm.entMstProduct.productCode"               :      productCode,
        "productManagementActionForm.entMstProduct.productName"               :      productName,
        "productManagementActionForm.entMstProduct.productGroupCode"          :      productGroupCode,
        "productManagementActionForm.entMstProduct.descriptionSummary"        :      descriptionSummary,
//        "productManagementActionForm.entMstProduct.hyoujiName"                :      hyoujiName,
        "productManagementActionForm.entMstProduct.productProperPriceFrom"    :      productProperPriceFrom,
        "productManagementActionForm.entMstProduct.productProperPriceTo"      :      productProperPriceTo,
        "productManagementActionForm.entMstProduct.rateFrom"                  :      rateFrom,
        "productManagementActionForm.entMstProduct.rateTo"                    :      rateTo,
        "productManagementActionForm.entMstProduct.siireName1"                :      siireName1,
        "productManagementActionForm.entMstProduct.siireName2"                :      siireName2,
        "productManagementActionForm.entMstProduct.supplierStatusName"        :      supplierStatusName,
        "productManagementActionForm.entMstProduct.productMaintenanceFlg"     :      productMaintenanceFlg,
        "productManagementActionForm.entMstProduct.productStatusName"         :      productStatusName,
        "productManagementActionForm.entMstProduct.productDelFlg"             :      productDelFlg,
        "productManagementActionForm.entMstProduct.fullName"                  :      fullName,
        "productManagementActionForm.entMstProduct.updatedOnFrom"             :      updatedOnFrom,
        "productManagementActionForm.entMstProduct.updatedOnTo"               :      updatedOnTo,
        "productManagementActionForm.entMstProduct.sortField"                 :      sortField,
        "productManagementActionForm.entMstProduct.sortDir"                   :      sortDir,
        "productManagementActionForm.entMstProduct.detailSearchFlag"          :      detailSearchFlag,
        "productManagementActionForm.allCheckBoxValue" 						  :      $("#selectAllCheckbox").prop("checked"),
        "productManagementActionForm.entMstProduct.productDelFlgStatus"		  : 	 productDelFlgStatus,
        "productManagementActionForm.entMstProduct.supplierStatusCode"        : 	 supplierStatusCode,
        "productManagementActionForm.checkStatus"        				      : 	 checkStatus,
        "arraySyouhinSysCode"                                                 :      arraySyouhinSysCode,
    };
};

//Validate date range
function validateKousinDateRange() {
   var result = true;
   var fromDate = $("#tblUpdatedFrom").val();
   var toDate = $("#tblUpdatedTo").val();
   if (undefined != fromDate && "" != fromDate && undefined != toDate
           && "" != toDate) {
       var dateFrom = new Date($("#tblUpdatedFrom").val());
       var dateTo = new Date($("#tblUpdatedTo").val());
       if (dateFrom > dateTo) {            
           result = false;
           $("#tblUpdatedFrom, #tblUpdatedTo").addClass("error");
       }
   }
   return result;
}

function validateTextboxKousinDate(idValue) {
    if ($("#" + idValue).val() == "") {
       jQuery("#" + idValue).removeClass("error");
       return true;
   }
   $("#" + idValue).val(formatDateTimeWithSlash($("#" + idValue).val()));
   if (!validateDateTime(jQuery("#" + idValue).val())) {
       //Invalid date
       jQuery("#" + idValue).addClass("error");
       return false;
   } else {
       //Valid date
       jQuery("#" + idValue).removeClass("error");
       return true;
   }
}

//Validate date
function validateKousinDate() {
     var result = true;
     if (!validateTextboxKousinDate("tblUpdatedFrom")) {
         result = false;
     }
     if (!validateTextboxKousinDate("tblUpdatedTo")) {
         result = false;
     }
     if (!result) {
         return false;
     }
     result = validateKousinDateRange();
     return result;
}

function clickButtonSearch() {
  // validate data
	var valid = true;
    if (!validateKousinDate()) {
        valid = false;
    }
    if (valid) {
    	getSearchCondition();
    	grid.data("kendoGrid").dataSource.page(1);
    	grid.data("kendoGrid").content.scrollTop(0);
    }
};

function getSearchCondition() {
  syouhinSysCode = $("#tblSyouhinSysCode").val().trim();
  brandName = $("#tblBrandName").val().trim();
  productCode = $("#tblProductCode").val().trim();
  productName = $("#tblProductName").val().trim();
  productGroupCode = $("#tblProductGroupCode").val().trim();
  descriptionSummary = $("#tblDescriptionSummary").val().trim();
//  hyoujiName = $("#tblHyoujiName").val().trim();
  productProperPriceFrom = $("#tblProperPriceFrom").val().trim();
  productProperPriceTo = $("#tblProperPriceTo").val().trim();
  rateFrom = $("#tblRateFrom").val().trim();
  rateTo = $("#tblRateTo").val().trim();
  siireName1 = $("#tblSiireName1").val().trim();
  siireName2 = $("#tblSiireName2").val().trim();
  supplierStatusName = $("#tblSupplierStatusName option:selected").text().trim();
  productMaintenanceFlg = $("#tblProductMaintenanceFlg option:selected").val().trim();
  productStatusName = $("#tblProductStatusName option:selected").text().trim();
  fullName = $("#tblFullName option:selected").text().trim();
  updatedOnFrom = $("#tblUpdatedFrom").val().trim();
  updatedOnTo = $("#tblUpdatedTo").val().trim();
  productDelFlg = $("#tblProductDelFlg").val().trim();
};
function checkProductCanEdit() {
  arraySyouhinSysCode = "";
  var errorCount = 0;
  var productId = "";
  var productChecked = 0;
  $('.productCheckbox:checked').each(function() {
    productChecked++;
    var productCanEdit = $(this).val().split(",")[0];
    if (productCanEdit == 0) {
      errorCount++;
    } else {
      productId = $(this).attr('id');
      arraySyouhinSysCode += productId + "_";
    }
  });
  $('#csv_ul_MODE_1 .chkDisabled').attr({"disabled": true, "checked": true});
  if (errorCount == productChecked) {
    // return 1 mean all product are can't edit
    return 1;
  } else if (errorCount != 0) {
    // return 2 mean some products are can edit
    return 2;
  } else {
    // return 3 mean all products are can edit
    return 3;
  }
};
//return 1 mean all product are can't detete
//return 2 mean some products are can detete
//return 3 mean all products are can detete
function checkProductCanDelete() {
  arraySyouhinSysCode = "";
  var errorCount = 0;
  var productId = "";
  var productChecked = 0;
  $('.productCheckbox:checked').each(function() {
    productChecked++;
    var productCanEdit = $(this).val().split(",")[1];
    if (productCanEdit == 0) {
      errorCount++;
    } else {
      productId = $(this).attr('id');
      arraySyouhinSysCode += productId + "_";
    }
  });
  if (errorCount == productChecked) {
    // return 1 mean all product are can't detete
    return 1;
  } else if (errorCount != 0) {
    // return 2 mean some products are can detete
    return 2;
  } else {
    // return 3 mean all products are can detete
    return 3;
  }
};
function isChecked() {
    var checkedCount = $(".productCheckbox:checked").length;
    if (checkedCount > 0) {
        return true;
    }
    return false;
};
function initPleaseSelectDialog() {
    var pleaseSelectDialog = $("#pleaseSelectDialog").data("kendoWindow");
    pleaseSelectDialog.center().open();
    $(".pleaseSelectDialogOk").click(function(e) {
        pleaseSelectDialog.close();
    });
};
function openPopup(id) {
    popup = $("#" + id).data("kendoWindow");
    popup.center();
    popup.open();
};
function closePopup() {
    popup.close();
};
function updateProductDelFlg() {
	var params;
  	if (detailSearchFlag == 2) {
    params = getDetailSearchParams();
  	} else {
	  params = passParamsToGrid(undefined);
  	}
//  var params = passParamsToGrid(undefined);
  $.ajax({
    type: "POST",
    url: "ajaxUpdateProductDelFlg.html",
    dataType: "JSON",
    data: params,
    beforeSend: function () {
      showProcessBar();
    },
    success: function (data) {
      if (data != null) {
//        productDelFlg = $("#tblProductDelFlg").val().trim();
        hideProcessBar();
        reloadMainGrid();
      }
    },
    error: function () {
//      productDelFlg = $("#tblProductDelFlg").val().trim();
      hideProcessBar();
    },
    complete: function () {

    },
  });
};
function updateProductSupplierStatusCode() {
	var params;
	  	if (detailSearchFlag == 2) {
	    params = getDetailSearchParams();
	  	} else {
		  params = passParamsToGrid(undefined);
	  	}
//  var params = passParamsToGrid(undefined);
  $.ajax({
    type: "POST",
    url: "ajaxUpdateProductSupplierStatusCode.html",
    dataType: "JSON",
    data: params,
    beforeSend: function () {
      showProcessBar();
    },
    success: function (data) {
      if (data != null) {
        hideProcessBar();
        reloadMainGrid();
      }
    },
    error: function () {
      hideProcessBar();
    },
  });
};
function addMatterText(matterName, matterFullName) {
    $("#spnErrorMsg").text("");
    $("#spnMatterName").text(matterName);
    $("#spnMatterUserName").text(matterFullName);
};
function clearMatterText() {
    $("#spnErrorMsg").text("");
    $("#txtMatterNo").val("");
    $("#spnMatterName").text("");
    $("#spnMatterUserName").text("");
    var parent = $("#txtMatterNo").parent();
    var errorMsgElem =  parent.find(".k-invalid-msg");
    errorMsgElem.hide();
};

function setParamToServerForMaintenance(){
    if($("#selectAllCheckbox").prop("checked")){
    	var params;
  	  	if (detailSearchFlag == 2) {
  	    	params = getDetailSearchParams();
  	  	} else {
  		  params = passParamsToGrid(undefined);
  	  	}
  	  	//BOE Nguyen.Chuong 2014/03/28 add param matterNo.
  	  	params["productManagementActionForm.matterNo"] = matterNo;
  	  	/* BOE #9414 Luong.Dai 2014/07/02 Add mode to param sent to server */
  	  	params["productManagementActionForm.productValidateMode"] = productValidateMode;
  	  	/* EOE #9414 Luong.Dai 2014/07/02 Add mode to param sent to server */
  	  	//EOE Nguyen.Chuong 2014/03/28
  	  	return params;
        //Nguyen.Chuong add param filter to get list
/*        return {
            "productManagementActionForm.allCheckBoxValue"                        :      $("#selectAllCheckbox").prop("checked"),
            "productManagementActionForm.matterNo"                                :      matterNo,
            "productManagementActionForm.productValidateMode"                     :      productValidateMode,
            "productManagementActionForm.entMstProduct.syouhinSysCode"            :      syouhinSysCode,
            "productManagementActionForm.entMstProduct.brandName"                 :      brandName,
            "productManagementActionForm.entMstProduct.productCode"               :      productCode,
            "productManagementActionForm.entMstProduct.productName"               :      productName,
            "productManagementActionForm.entMstProduct.productGroupCode"          :      productGroupCode,
            "productManagementActionForm.entMstProduct.descriptionSummary"        :      descriptionSummary,
            "productManagementActionForm.entMstProduct.productProperPriceFrom"    :      productProperPriceFrom,
            "productManagementActionForm.entMstProduct.productProperPriceTo"      :      productProperPriceTo,
            "productManagementActionForm.entMstProduct.rateFrom"                  :      rateFrom,
            "productManagementActionForm.entMstProduct.rateTo"                    :      rateTo,
            "productManagementActionForm.entMstProduct.siireName1"                :      siireName1,
            "productManagementActionForm.entMstProduct.siireName2"                :      siireName2,
            "productManagementActionForm.entMstProduct.supplierStatusName"        :      supplierStatusName,
            "productManagementActionForm.entMstProduct.productMaintenanceFlg"     :      productMaintenanceFlg,
            "productManagementActionForm.entMstProduct.productStatusName"         :      productStatusName,
            "productManagementActionForm.entMstProduct.productDelFlg"             :      productDelFlg,
            "productManagementActionForm.entMstProduct.fullName"                  :      fullName,
            "productManagementActionForm.entMstProduct.updatedOnFrom"             :      updatedOnFrom,
            "productManagementActionForm.entMstProduct.updatedOnTo"               :      updatedOnTo,
            "productManagementActionForm.entMstProduct.sortField"                 :      sortField,
            "productManagementActionForm.entMstProduct.sortDir"                   :      sortDir,
            "productManagementActionForm.entMstProduct.detailSearchFlag"          :      detailSearchFlag,
        };*/
    } else {
        return {
            "arraySyouhinSysCode"                               :      arraySyouhinSysCode,
            "productManagementActionForm.matterNo"              :      matterNo,
            "productManagementActionForm.productValidateMode"   :      productValidateMode,
            "productManagementActionForm.allCheckBoxValue"      :      $("#selectAllCheckbox").prop("checked")
        };
    }
};

function copyData() {
  clearMsgText();
  $.ajax({
    type: "POST",
    url: "ajaxCopyDataThrough2DB.html",
    dataType: "JSON",
    data: setParamToServerForMaintenance(),
    beforeSend: function () {
      closePopup();
      showProcessBar();
    },
    success: function (request) {
      hideProcessBar();
      var data = request.productManagementActionForm;
      if (data.productValidateMode == 1) {
        openPopup("confirmMaintenancePopup");
      } else if (data.productValidateMode == 2) {
        clearMsgText();
        $("#message").addClass("has-message success");
        $("#message").html(jQuery.format(msgComplete,
                                        "<a style='color: blue;' href='./matterDetail.html?matterNo=" + data.matterNo + "'>" + data.matterNo + "</a>"));
        /* BOE by Luong.Dai add event click on link redirect to matterDetail at 2014/04/01 */
        /*$('#message a').click(function(event) {
            //Show progress bar
            showProcessBar();
        });*/
        /* BOE by Luong.Dai add event click on link redirect to matterDetail */
        reloadMainGrid();
      /*BOE Nguyen.Chuong 2014/04/04 set productValidateMode = 0 to reset check flow*/
      } else if (data.productValidateMode == 3) {
        $("#message").addClass("has-message msg-error");
        $("#message").text(msgFail);
        productValidateMode = 0;
      } else if (data.productValidateMode == 4) {
          $("#message").addClass("has-message msg-error");
          $("#message").text(jQuery.format(jQuery("#maxProductMaintenanceErrorMsg").val(), jQuery("#maxNumberProductMaintenanceAction").val()));
          productValidateMode = 0;
      } else if (data.productValidateMode == 5) {
          $("#message").addClass("has-message msg-error");
          $("#message").text($("#hdNoDataMaintenance").val());
          productValidateMode = 0;
      } else if (data.productValidateMode == 6) {//List product contain some disable product.
          $("#message").addClass("has-message msg-error");
          $("#message").text($("#enableProductToMaintenanceErrorMsg").val());
          productValidateMode = 0;
      }
      /*EOE Nguyen.Chuong 2014/04/04*/
    },
    error: function (request, status, error) {
      hideProcessBar();
      jsonValue = jQuery.parseJSON(request.responseText);
      var data = jsonValue.productManagementActionForm;
      if (data.errorMode == "importConditionError") {
        openPopup("allProductsAreImportPopup");
      } else if (data.errorMode == "maintenanceError") {
        openPopup("cannotMaintenancePopup");
      }
      productValidateMode = 0;
    },
  });
};
function setNumbericInput() {
//  onlyInpuNumerric("tblSyouhinSysCode");
//  onlyInpuNumerric("tblProductGroupCode");
//  onlyInpuNumerric("tblProperPriceFrom");
//  onlyInpuNumerric("tblProperPriceTo");
//  onlyInpuNumerric("tblRateFrom");
//  onlyInpuNumerric("tblRateTo");
//  onlyInpuNumerric("txtMatterNo");
	
	//BOE Tran.Thanh 2014/27/03
	initNumberTextBox("tblSyouhinSysCode");
	initNumberTextBox("tblProductGroupCode");
	initNumberTextBox("tblProperPriceFrom");
	initNumberTextBox("tblProperPriceTo");
	initNumberTextBox("tblRateFrom");
	initNumberTextBox("tblRateTo");
	initNumberTextBox("txtMatterNo");
	//EOE Tran.Thanh 2014/27/03
};
function clearMsgText() {
  // remove all class of msg div and clear msg
  $("#message").removeClass("has-message success");
  $("#message").removeClass("has-message msg-error");
  $("#message").text("");
}
function reloadMainGrid() {
  $('#grid').data('kendoGrid').dataSource.read();
  $('#grid').data('kendoGrid').refresh();
};
function processMaintenanceProduct() {
  var matterNo = $("#txtMatterNo").val();
  if (!validateNumberic(matterNo)) {
    return;
  }
  $.ajax({
    type: "POST",
    url: "ajaxLoadMatterByMatterNo.html",
    dataType: "json",
    data: {
      "productManagementActionForm.entMstFactoryMatter.matterNo" : matterNo
    },
    beforeSend: function () {
      // hide message error if any.
      var parent = $("#txtMatterNo").parent();
      var errorMsgElem =  parent.find(".k-invalid-msg");
      errorMsgElem.hide();
      // show image loading.
      $("#loading-ajax-inline").css("display", "inline");
      // disable button
      $("#btnMaintenanceOk").attr('disabled','disabled');
    },
    success: function (data) {
      isLoadedFromSv = 1;
      validMatterNo = 1;
      var matterObj = data.productManagementActionForm.entMstFactoryMatter;
        // get message span element
      var parent = $("#txtMatterNo").parent();
      var errorMsgElem = parent.find(".k-invalid-msg");
      if (matterObj != null) {
        // valid
        $("#matterNoInvalidFlag").val("0");
        addMatterText(matterObj.matterName, matterObj.fullName);
        // hide image loading.
        jQuery("#loading-ajax-inline").css("display", "none");
        errorMsgElem.hide();
        // check if this event call from button maintenance or not
        if (isClickBtnMaintenance) {
          invalidMaterNo = $("#matterNoInvalidFlag").val();
          if (invalidMaterNo == 1) {
            return;
          }
          isClickBtnMaintenance = false;
          processPreCopyData();
        }
      } else {
        // invalid
        // set invalid flag
        $("#matterNoInvalidFlag").val("1");
          // get message.
          var errorMsg = $("#noMatterObj").val();
          // replace message.
          // hide image loading.
          jQuery("#loading-ajax-inline").css("display", "none");
          errorMsgElem.html('<span class="k-icon k-warning"> </span> ' + errorMsg);
          errorMsgElem.show();
          $("#spnMatterName").text("");
          $("#spnMatterUserName").text("");
          isClickBtnMaintenance = false;
      }
      // enable button submit
      $('#btnMaintenanceOk').removeAttr('disabled');
    },
    error: function () {
      // enable button submit
      $('#btnMaintenanceOk').removeAttr('disabled');
    }
  });
};
function processPreCopyData() {
    arraySyouhinSysCode = "";
    var data;
    //Loop all selected rows.
    $("#grid").find("input:checked").each(function() {
        //Seleted row
        var row = $(this).closest('tr');
        //get data
        rowData = $("#grid").data("kendoGrid").dataItem(row);
        //Get syouhinSysCode and add to list code return to Server.
        arraySyouhinSysCode += rowData.syouhinSysCode + "_";

    });
    copyData();
};
/* EOE productMangegement function */

/* BOE productReview function */
// Get init data for all block preview popup
function getInitPreviewPopupData(syouhinSysCode) {
    jQuery.ajax({
        type: "POST",
        url: "loadInitDataPreviewPopup.html",
        data: {
            syouhinSysCode: syouhinSysCode
        },
        dataType: "json",
        success: function(dataResult) {
            // List image of this product
            listSyouhinImage = dataResult.productManagementActionForm.listSyouhinImage;
            createListHiddenThumb(dataResult.productManagementActionForm.listSyouhinImage);
            createThumbList();
            // Fill data for product Info
            createProductInfoInit(dataResult.productManagementActionForm);
            // List model of this product
//            createListFitModel(dataResult);
            //Show preview popup
            var detailPopupKendo = $("#previewPopupHTML").data("kendoWindow");
            detailPopupKendo.setOptions({
                width: 570,
                height: 420
              });
            detailPopupKendo.center();
            detailPopupKendo.open();
            hideProcessBar();
            //Load ajax detail after show preview popup to user.
            loadAjaxForPreviewPopup();
        }
    });
};

// Fill data for product Info when init preview popup
function createProductInfoInit(productManagementActionForm) {
    var entMstProduct = productManagementActionForm.entMstProduct;
    // Fill data for productInfo right side
    // syouhin_sys_code
    jQuery("#productInfoSyouhinSysCode a").attr("href", "http://www.webike.net/sd/" + entMstProduct.syouhinSysCode + "/");
    jQuery("#productInfoSyouhinSysCode a").html(entMstProduct.syouhinSysCode);
    // brand_name
    jQuery("#productInfoBrandName span").html(entMstProduct.brandCode + " / " + entMstProduct.brandName);
    //Check brandName tooltip to show tooltip for brand name.
    var brandNamePreviewTooltip = $("#productInfoBrandName").data("kendoTooltip");
    //If brand name tooltip not exist then new tooltip
    if (brandNamePreviewTooltip  == undefined) {
        brandNamePreviewTooltip = $("#productInfoBrandName").kendoTooltip({
            content: $("#productInfoBrandName span").text(),
            animation : {
                close : {
                    effects : "fade:out"
                },
                open : {
                    effects : "fade:in",
                    duration : 300
                }
            }
        });
    } else {
        //If brand name tooltip exist then set new content
        brandNamePreviewTooltip.options.content = $("#productInfoBrandName span").text();
        brandNamePreviewTooltip.refresh();
    }
    // category_name
    jQuery("#productInfoCategoryName span").html(entMstProduct.categoryName);
    //Check category name tooltip to show tooltip for brand name.
    var categoryNamePreviewTooltip = $("#productInfoCategoryName").data("kendoTooltip");
    //If category name tooltip not exist then new tooltip
    if (categoryNamePreviewTooltip  == undefined) {
        categoryNamePreviewTooltip = $("#productInfoCategoryName").kendoTooltip({
            content: $("#productInfoCategoryName span").text(),
            animation : {
                close : {
                    effects : "fade:out"
                },
                open : {
                    effects : "fade:in",
                    duration : 300
                }
            }
        });
    } else {
        //If category name tooltip exist then set new content
        categoryNamePreviewTooltip.options.content = $("#productInfoCategoryName span").text();
        categoryNamePreviewTooltip.refresh();
    }
    // product_name
    jQuery("#productInfoProductName span").html(entMstProduct.productName);
    //Check product name tooltip to show tooltip for brand name.
    var productNamePreviewTooltip = $("#productInfoProductName").data("kendoTooltip");
    //If product name tooltip not exist then new tooltip
    if (productNamePreviewTooltip  == undefined) {
        productNamePreviewTooltip = $("#productInfoProductName").kendoTooltip({
            content: $("#productInfoProductName span").text(),
            animation : {
                close : {
                    effects : "fade:out"
                },
                open : {
                    effects : "fade:in",
                    duration : 300
                }
            }
        });
    } else {
        //If product name tooltip exist then set new content
        productNamePreviewTooltip.options.content = $("#productInfoProductName span").text();
        productNamePreviewTooltip.refresh();
    }
    // product_code
    jQuery("#productInfoProductCode span").html(entMstProduct.productCode);
    // product_ean_code
    jQuery("#productInfoProductEanCode span").html(entMstProduct.productEanCode);
    // product_group_code
    if('0' == $.trim(entMstProduct.productGroupCode)){
        jQuery("#productInfoProductGroupCode span").text(entMstProduct.productGroupCode);
    } else {
        var tagA = '<a href="./productManagement.html?groupCodeParam={0}&brandCodeParam={1}" style="color: blue;" target="_blank">{2}</a>';
        tagA = jQuery.format(tagA, entMstProduct.productGroupCode, entMstProduct.brandCode, entMstProduct.productGroupCode);
        jQuery("#productInfoProductGroupCode span").html(tagA);
    }
    // Product_proper_price
    jQuery("#productInfoProductProperPrice span").html("&#165;" + entMstProduct.productProperPrice);
    // product_sikiri_Price / product_proper_price
    jQuery("#productInfoSikiriPrice span").html("&#165;" + entMstProduct.sikiriPrice + " / " + entMstProduct.sikiriProperPricePercentage);
    // product_supplier_releaseDate
    jQuery("#productInfoProductSupplierReleaseDate span").html(entMstProduct.productSupplierReleaseDate);
    
    // Fill data for productInfo bottom side
    var listCondition =  new Array();
    if(entMstProduct.productOpenPriceFlg) {
        listCondition.push(jQuery("#hddProductOpenPriceFlg").val());
    }
    if(entMstProduct.productProperSellingFlg) {
        listCondition.push(jQuery("#hddProductProperSellingFlg").val());
    }
    if(entMstProduct.productOrderProductFlg) {
        listCondition.push(jQuery("#productOrderProductFlg").val());
    }
    if(entMstProduct.productNoReturnableFlg) {
        listCondition.push(jQuery("#hddProductNoReturnableFlg").val());
    }
    if(entMstProduct.productAmbigousImageFlg) {
        listCondition.push(jQuery("#hddProductAmbigousImageFlg").val());
    }
    // Init product condition multi select
    var multiProductCondition = jQuery("#multiselectProductCondition").data("kendoMultiSelect");
    // if product condition multi select don't exist then create it
    if(multiProductCondition == undefined) {
        jQuery("#multiselectProductCondition").kendoMultiSelect({
            dataSource: listCondition,
            value: listCondition
        });
        multiProductCondition = jQuery("#multiselectProductCondition").data("kendoMultiSelect");
    } else {
        // if product condition multi select existed then set data.
        multiProductCondition.setDataSource(listCondition);
        multiProductCondition.value(listCondition);
    }
    // set product condition multi select readonly
    multiProductCondition.readonly(true);
    
    // Fill product_hyouji_name
    jQuery("#productInfoProductHoyujiName").html(entMstProduct.hyoujiName);

    // Fill product_description_summary
    jQuery("#productInfoProductDescriptionSummary").html(entMstProduct.descriptionSummary);
    
    // Fill mst_siire.siire_name / mst_nouki.nouki_name1
    var siireNoukiStringTmp = "";
    for (var i = 0; i < productManagementActionForm.listSiireNouki.length; i++) {
      siireNoukiStringTmp += productManagementActionForm.listSiireNouki[i].siireName + " / " +  productManagementActionForm.listSiireNouki[i].noukiName1 + "<br>";
    };
    jQuery("#productInfoProductSiireNoukoName span").html(siireNoukiStringTmp);
    
    // Fill description_remarks
    jQuery("#descriptionRemarksPreviewBLock span").html(entMstProduct.descriptionRemarks);

    // Fill description_caution
    jQuery("#descriptionCautionPreviewBLock span").html(entMstProduct.descriptionCaution);

    // Fill description_sentence
    jQuery("#descriptionSentencePreviewBLock span").html(entMstProduct.descriptionSentence);
    
};

/**
* create hidden list to content list URL of image and thumb.
*/
function createListHiddenThumb(listImage) {
   var resultHTML = "";
   jQuery(listImage).each(function( index, element ) {
       if (index < 10) {
        // Check productThumb or productImage is not empty
           if (element.thumbnailPath !='' || element.imagePath !='') {
               // productThumb and productImage not empty
               resultHTML += '<input type="hidden"'
                          +         'id="thumb_' + index + '"'
                          +         'value="' + element.thumbnailPath + '" />';
               resultHTML += '<input type="hidden"'
                          +         'id="image_' + index + '"'
                          +         'value="' + element.imagePath + '" />';
           } else {
               // productThumb or productImage is empty
               // Set thumb and image is noImage
               resultHTML += '<input type="hidden"'
                          +         'id="thumb_' + index + '"'
                          +         'value="' + noImageURL + '" />';
               resultHTML += '<input type="hidden"'
                          +         'id="image_' + index + '/>"'
                          +         'value="' + noImageURL + '" />';
           }
       }
   });
   jQuery("#thumbHiddenDiv").html(resultHTML);
   // Chck listImage is not null
   if(null != listImage) {
       listThumbCount = listImage.length;
       return
   }
   listThumbCount = 0;
};

/**
* Function to generate imageBlock.
*/
function createThumbList() {
    var listThumbCnt = jQuery("#listThumbCount").val();
    // Create html
    // Add previous button
    var listThumbHTML = '<ul class="selector" id="selector">';
    var liClass = "display: list-item;";
    listThumbCnt = 10;
    // Loop to create list of thumb.
    for (var i = 0; i < listThumbCnt; i++) {
        var thumbURL = jQuery("#thumb_" + i).val();
        var imageURL = jQuery("#image_" + i).val();
        var thumbClass = "fancy_box";
        var liHTML = "";
        // Add noImage to list
        if (!thumbURL || !imageURL) {
            liHTML = '<li class="list_thumb" style="display: list-item;">'
                    +   '<a href="" onclick="return false;">'
                    +       '<img class="selector_thumb" src="' + noImageURL + '">'
                    +   '</a>'
                    + '</li>';
        } else {
            // Create thumb.
            liHTML = '<li thumbId="' + i + '" class="list_thumb" style="' + liClass + '">'
                    +   '<a href="' + imageURL + '" id="selector_' + i + '" class="' + thumbClass + '" rel="fancy_box" onclick="return false;">'
                    +       '<img class="selector_thumb" src="' + thumbURL + '">'
                    +   '</a>'
                    + '</li>';
        }
        listThumbHTML += liHTML;
    }
    // Add next button
    listThumbHTML += '</ul>';
    // Show list thumb.
    jQuery(".thumb_selector").html(listThumbHTML);

    // Change first thumb of list to selected thumb.
    // Get first thumb of list
    var selectedThumb = jQuery(".list_thumb a[class='fancy_box']")[0];
    if(selectedThumb) {
        // Change class of first thumb to selected.
        jQuery(selectedThumb).attr("class", "image_selected");
        // Show image thumb mapping with selected thumb.
        // Get No of selected thumb.
        var selectThumbId = jQuery(selectedThumb).parent().attr("thumbId");
        // Set current selected thumb
        currentSelectedThumb = selectThumbId;
        // Get image thumb url.
        var imageThumbURL = jQuery("#image_" + selectThumbId).val();
    } else {
        imageThumbURL = noImageURL;
    }
    // Show image thumb.
    if (imageThumbURL != noImageURL) {
        jQuery("#imageAhref").attr("href", imageThumbURL);
        jQuery("#imageAhref").attr("class", "fancy_box");
    } else {
        jQuery("#imageAhref").attr("href", "#");
        jQuery("#imageAhref").attr("class", "noImage");
    }
    
    jQuery("#imageThumb").attr("src", imageThumbURL);
    // Show thumb image and hide loading image
    jQuery("#imageThumb").css("display", "");
    jQuery("#imageThumbLoading").css("display", "none");
};

/**
 * switch thumb to image when mouse over thumb.
 */
function switchThumb(e) {
    // If mouseover to current selected thumb.
    if(jQuery("#selector_" + e).attr('class') === 'image_selected') {
        return;
    }
    // Check no image
    if(jQuery("#thumb_" + e).length < 1) {
        return;
    }
    // Remove old selected Thumb
    jQuery("#selector_" + currentSelectedThumb).attr('class', "fancy_box");
    // Change current select.
    currentSelectedThumb = e;
    // Set new selected Thumb
    jQuery("#selector_" + e).attr('class', "image_selected");

    // Change image Thumb
    jQuery("#imageAhref").attr("href", jQuery("#image_" + e).val());
    jQuery("#imageThumb").attr("src", jQuery("#image_" + e).val());
}

// create fit_model kendo grid for preview popup.
function initFitModelPopup (listSyouhinFitModel) {
    // Define data get from sever and parse to json
    var filterRowFitModelPopup = $(
            '<tr>' 
            +  '<th class="k-header" data-field="fitModelMaker">'
            +      '<input type="text" id="makerFitModelGridPopup" class="txtFilter k-textbox" style="width: 100%;"/>'
            +  '</th>' 
            +  '<th class="k-header" data-field="fitModelModel">'
            +      '<input type="text" id="modelFitModelGridPopup" class="txtFilter k-textbox" style="width: 100%;"/>'
            +  '</th>' 
            +  '<th class="k-header" data-field="fitModelStyle">'
            +      '<input type="text" id="styleFitModelGridPopup" class="txtFilter k-textbox" style="width: 100%;"/>'
            +  '</th>' 
            +  '</tr>');
    previewPopupFitModelGrid = $("#previewPopupFitModelGrid").kendoGrid({
      dataSource: {
          data: listSyouhinFitModel,
          schema: {
              model: {
                  fields: {
                        maker: {type: "string"}
                      , model: {type: "string"}
                      , style: {type: "string"}
                  }
              }
          }
      },
      height: 150,
      maxWidth: 550,
      scrollable: true,
      selectable: true,
      resizable : true,
      sortable: {
          mode: "single",
          allowUnsort: false
      },
      pageable: false,
      columns: [{
                  field: "maker",
                  title: $("#titleGridMaker").val(),
                  width: "25%",
                  attributes: {"class": "align-center"}
                },
                {
                  field: "model",
                  title: $("#titleGridModel").val(),
                  width: "50%",
                  attributes: {"class": "align-left"}
                },
                {
                  field: "style",
                  title: $("#titleGridStyle").val(),
                  width: "25%",
                  attributes: {"class": "align-left"}
                }],
      dataBound: function(e) {
          // Remove noData message
          jQuery("#previewPopupFitModelGrid .dataTables_empty").remove();
          if (this.dataSource.total() > 0) {
              // Add title for row
              var gridData = this.dataSource.view();
              var gridLength = gridData.length;
              for (var i = 0; i < gridLength; i++) {
                  var currentUid = gridData[i].uid;
                  var currenListCell = this.table.find("tr[data-uid='" + currentUid + "'] td");
                  if(gridData[i].fitModelMakerNoFitFlg == 1) {
                      $(currenListCell[0]).addClass("yellow");
                  }
                  if(gridData[i].fitModelModelNoFitFlg == 1) {
                      $(currenListCell[1]).addClass("yellow");
                  }
              }
          } else {
  // //Show no data message
              jQuery("#previewPopupFitModelGrid .k-grid-content").append('<div class="dataTables_empty">' + jQuery("#noDataMessage").val() + '</div>');
          }
      }
    });
  // add filter in to grid.
  previewPopupFitModelGrid.data("kendoGrid").thead.append(filterRowFitModelPopup);
};

// create product attribute all type kendo grid for preview popup.
function initProductAttributeAllTypePopup (listAttributeAllType) {
    // Define data get from sever and parse to json
    previewPopupAttributeAllTypeGrid = $("#previewPopupAttributeAllTypeGrid").kendoGrid({
      dataSource: {
          data: listAttributeAllType,
          schema: {
              model: {
                  fields: {
                        attributeName:    {type: "string"}
                      , attributeDisplay: {type: "string"}
                      , attributeValues:  {type: "string"}
                  }
              }
          }
      },
//      height: 79,
      maxWidth: 550,
      scrollable: true,
      selectable: true,
      resizable : true,
      sortable: {
          mode: "single",
          allowUnsort: false
      },
      pageable: false,
      columns: [{
                  field: "attributeName",
                  title: $("#titleGridAtributeName").val(),
                  width: "25%",
                  attributes: {"class": "align-center"}
                },
                {
                  field: "attributeDisplay",
                  title: $("#titleGridDisplayName").val(),
                  width: "50%",
                  attributes: {"class": "align-center"},
                  template : function(e) {
                    var result = "";
                    if (e.attributeDisplay == 'flg') {
                      result = "<input type='checkbox' disabled='disabled '";
                      if (e.attributeValues == '1') {
                        result += 'checked="true" ';
                      }
                      result += '>';
                    } else {
                      result = e.attributeDisplay;
                    }
                    return result;
                  }
                },
                {
                  field: "attributeValues",
                  title: $("#titleGridAdministrativeName").val(),
                  width: "25%",
                  attributes: {"class": "align-center"},
                  template : function(e) {
                    var result = "";
                    if (e.attributeDisplay == 'flg') {
                      result = "<input type='checkbox' disabled='disabled '";
                      if (e.attributeValues == '1') {
                        result += 'checked="true" ';
                      }
                      result += '>';
                    } else {
                      result = e.attributeValues;
                    }
                    return result;
                  }
                }],
      dataBound: function(e) {
          // Remove noData message
          jQuery("#previewPopupAttributeAllTypeGrid .dataTables_empty").remove();
          if (this.dataSource.total() < 0) {
  // //Show no data message
              jQuery("#previewPopupAttributeAllTypeGrid .k-grid-content").append('<div class="dataTables_empty">' + jQuery("#noDataMessage").val() + '</div>');
          }
      }
    });
};

// create video kendo grid for preview popup.
function initVideoPopup (listVideo) {
    // Define data get from sever and parse to json
    previewPopupVideoGrid = $("#previewPopupVideoPreviewGrid").kendoGrid({
      dataSource: {
          data: listVideo,
          schema: {
              model: {
                  fields: {
                        productVideoTitle:    {type: "string"}
                  }
              }
          }
      },
//      height: 108,
      maxWidth: 550,
      scrollable: true,
      selectable: true,
      resizable : true,
      sortable: {
          mode: "single",
          allowUnsort: false
      },
      pageable: false,
      columns: [{
                  field: "productVideoTitle",
                  title: $("#titleGridTitle").val(),
                  width: "25%",
                  attributes: {"class": "align-left"},
                  template : function (e) {
                    var result = "";
                    result += "<a style='color:blue;' href='javascript:handleClickingVideoLinkGrid()' value='" + e.productVideoURL + "'" + ">" + e.productVideoTitle + "</a>";
                    return result;
                  }
                }],
      dataBound: function(e) {
          // Remove noData message
          jQuery("#previewPopupVideoPreviewGrid .dataTables_empty").remove();
          if (this.dataSource.total() < 0) {
  // //Show no data message
              jQuery("#previewPopupVideoPreviewGrid .k-grid-content").append('<div class="dataTables_empty">' + jQuery("#noDataMessage").val() + '</div>');
          }
      }
    });
};

// Handler when click into link video in video grid => show video dialog.
function handleClickingVideoLinkGrid() {
    // Init event click after insert Row to Grid
    var gridListVideo = $("#previewPopupVideoPreviewGrid").data("kendoGrid");
    var selectedRows = gridListVideo.select();
    var data = gridListVideo.dataItem(selectedRows);

    $("#productVideoTitlePreviewPopup").html(data.productVideoTitle);
    // replace [watch?v= ] with [embed/] to show in popup
    var srcTmp = data.productVideoURL;
    srcTmp = srcTmp.replace('watch?v=', 'embed/');
    $("#popupVideoPreviewPopup").attr('src', srcTmp);
    $("#productVideoDescriptionPreviewPopup").html(data.productVideoDescription);       
    var videoPlayerDialog = $("#videoPlayerDialogPreviewPopup").data("kendoWindow");
    videoPlayerDialog.center();
    videoPlayerDialog.open();
};

/**
 * Resize height of inner grid when change size of kendo block
 * @param blockId blockId contain grid.
 */
function resizeGridByBlockId(blockId) {
    //Height of block
    var blockInnerHeight = jQuery("#" + blockId).innerHeight(),
        // height of block title
        titleHeight = jQuery("#" + blockId + " .title").outerHeight(),
        //Grid
        gridElement = $("#" + blockId + " .k-grid"),
        //Grid content.
        dataArea = gridElement.children(".k-grid-content"),
        //Other elements in grid: grid tittle, filter...
        otherElements = gridElement.children().not(".k-grid-content"),
        otherElementsHeight = 0;
    //Loop to count all height of other.
    otherElements.each(function(){
        otherElementsHeight += $(this).outerHeight();
    });
    //Change height of block content
    dataArea.height(blockInnerHeight - titleHeight - otherElementsHeight - 13);
}

// create link reason kendo grid for preview popup.
function initLinkReasonGridPopup (listLinkReason) {
    // Define data get from sever and parse to json
    previewPopuplinkReasonGrid = $("#previewPopuplinkReasonGrid").kendoGrid({
      dataSource: {
          data: listLinkReason,
          schema: {
              model: {
                  fields: {
                        productLinkTitle:  {type: "string"}
                  }
              }
          }
      },
//      height: 108,
      maxWidth: 550,
      scrollable: true,
      selectable: true,
      resizable : true,
      sortable: {
          mode: "single",
          allowUnsort: false
      },
      pageable: false,
      columns: [
                {
                  field: "productLinkTitle",
                  width: "100%",
                  title: $("#titleGridReasonLinkTitle").val(),
                  attributes: {"class": "align-left"},
                  template : function (e) {
                    var result = "";
                    result += e.linkReasonName + " <a style='color:blue;' href='" + e.productLinkURL + "' "
                                               +     "target='_blank'> "
                                               +     e.productLinkTitle + "</a>";
                    return result;
                  }
                }],
      dataBound: function(e) {
          // Remove noData message
          jQuery("#previewPopuplinkReasonGrid .dataTables_empty").remove();
          if (this.dataSource.total() < 0) {
  // //Show no data message
              jQuery("#previewPopuplinkReasonGrid .k-grid-content").append('<div class="dataTables_empty">' + jQuery("#noDataMessage").val() + '</div>');
          }
      }
    });
};

// create guest input kendo grid for preview popup.
function initGuestInputGridPopup (listGuestInput) {
    // Define data get from sever and parse to json
    previewPopupGuestInputGrid = $("#previewPopupGuestInputGrid").kendoGrid({
      dataSource: {
          data: listGuestInput,
          schema: {
              model: {
                  fields: {
                        productGuestInputTitle:         {type: "string"}
                      , productGuestInputDescription:   {type: "string"}
                  }
              }
          }
      },
      height: 55,
      maxWidth: 550,
      scrollable: true,
      selectable: true,
      resizable : true,
      sortable: {
          mode: "single",
          allowUnsort: false
      },
      pageable: false,
      columns: [
                {
                  field: "productGuestInputTitle",
                  width: "30%",
                  title: $("#titleGridGuestInputTitle").val(),
                  attributes: {"class": "align-left"}
                }
                ,{
                  field: "productGuestInputDescription",
                  width: "70%",
                  title: $("#titleGridGuestInputDescription").val(),
                  attributes: {"class": "align-left"}
                }],
      dataBound: function(e) {
          // Remove noData message
          jQuery("#previewPopupGuestInputGrid .dataTables_empty").remove();
          if (this.dataSource.total() < 0) {
  // //Show no data message
              jQuery("#previewPopupGuestInputGrid .k-grid-content").append('<div class="dataTables_empty">' + jQuery("#noDataMessage").val() + '</div>');
          }
      }
    });
};

// Init grid image list and load dialog.
function initImageListDiaglog() {
    $("#imageListDialogGridPreviewPopup").kendoGrid({
      dataSource: {
          data: listSyouhinImage,
          schema: {
              model: {
                  fields: {
                        thumbnailA:      {type: "string"}
                      , thumbnailB:      {type: "string"}
                      , imageA:          {type: "string"}
                      , imageB:          {type: "string"}
                      , sort:            {type: "string"}
                  }
              }
          }
      },
      width : 700,
      maxWidth: 740,
      maxHeight: 350,
      scrollable: true,
      selectable: true,
      resizable : true,
      sortable: {
          mode: "single",
          allowUnsort: false
      },
      pageable: false,
      columns: [
                {
                  field: "thumbnailPath",
                  width: "10%",
                  title: $("#titleGridImageListThumbnail").val(),
                  attributes: {"class": "align-center"},
                  template : function (e) {
                    var result = "";
                    result += '<a rel="fancy_box" href="' + e.thumbnailPath + '" class="fancy_box">';
                    result += '<img id="imageThumb" src="'+ e.thumbnailPath +'" style="width: 28px; height: 30px;/>';
                    result += '<img id="imageThumbLoading" src="./images/processing.gif" style="display: none;"/>';
                    result += '</a>';
                    return result;
                  }
                }
                ,{
                  field: "thumbnail",
                  width: "30%",
                  title: $("#titleGridImageListThumbnailPath").val(),
                  attributes: {"class": "align-center"}
                }
                ,{
                  field: "imagePath",
                  width: "10%",
                  title: $("#titleGridImageListDetailedImage").val(),
                  attributes: {"class": "align-center"},
                  template : function (e) {
                    var result = "";
                    result += '<a rel="fancy_box" href="' + e.thumbnailPath + '" class="fancy_box">';
                    result += '<img id="imageThumb" src="'+ e.thumbnailPath +'" style="width: 28px; height: 30px;/>';
                    result += '<img id="imageThumbLoading" src="./images/processing.gif" style="display: none;"/>';
                    result += '</a>';
                    return result;
                  }
                }
                ,{
                  field: "image",
                  width: "30%",
                  title: $("#titleGridImageListDetailedPath").val(),
                  attributes: {"class": "align-center"}
                }
                ,{
                  field: "sort",
                  width: "6%",
                  title: $("#titleGridImageListSequentialOrder").val(),
                  attributes: {"class": "align-center"}
                }],
      dataBound: function(e) {
          // Remove noData message
          jQuery("#previewPopupGuestInputGrid .dataTables_empty").remove();
          if (this.dataSource.total() < 0) {
  // //Show no data message
              jQuery("#previewPopupGuestInputGrid .k-grid-content").append('<div class="dataTables_empty">' + jQuery("#noDataMessage").val() + '</div>');
          }
      }
    });
    var listImageDialog = $("#imageListDialogPreviewPopup").data("kendoWindow");
    listImageDialog.center();
    listImageDialog.open();
    $("#imageListDialogPreviewPopup_wnd_title").css("margin-top", "2px");
};

// Set syouhinSysCodePreview when change selected rows of grid manage.
function setSyouhinSysCodeForPreviewPopup() {
  // Set syouhin_sys_code for preview popup
  var gridManage = $("#grid").data("kendoGrid");
  var selectedRowsGridManage = gridManage.select();
  var dataSelectedGridManage = gridManage.dataItem(selectedRowsGridManage);
  if (dataSelectedGridManage != null) {
      syouhinSysCodeToPreview = dataSelectedGridManage.syouhinSysCode;
  } else {
      syouhinSysCodeToPreview = "";
  }
}

//Toggle to show/hide block spliter.
function toggleToShowDetailBlockPreviewPopup() {
    var detailPopupKendo = $("#previewPopupHTML").data("kendoWindow");
    if (previewPopupMode == "simple") {
        // Set size of popup window
        detailPopupKendo.setOptions({
            width: 570,
            height: 400
          });
        //Set position of window
        jQuery(".k-window #previewPopupHTML").parent().css("left", "53px");
        jQuery(".k-window #previewPopupHTML").parent().css("top", "40px");
        // Hide right detail block
        // title
        jQuery("#rightPreviewBlockTitle").css("display", "none");
        // Contain
        jQuery("#rightReviewBlock").css("display", "none");
        // Hide 2 bottom block
        jQuery("#fitModelPreviewBlock").css("display", "none");
        jQuery("#guestInputPreviewBlock").css("display", "none");
        return;
    }
    // Set size of popup window
    detailPopupKendo.setOptions({
        width: 1180,
        height: 680
      });
    //Set position of window
    jQuery(".k-window #previewPopupHTML").parent().css("left", "53px");
    jQuery(".k-window #previewPopupHTML").parent().css("top", "40px");
    // Show right detail block
    // title
    jQuery("#rightPreviewBlockTitle").css("display", "block");
    // Contain
    jQuery("#rightReviewBlock").css("display", "block");
    // Show 2 bottom block
    jQuery("#fitModelPreviewBlock").css("display", "block");
    jQuery("#guestInputPreviewBlock").css("display", "block");
    //Trigger resize to fix error size bugs.
    var rightReviewBlockTeee = $("#rightReviewBlock").data("kendoSplitter");
    rightReviewBlockTeee.size(".k-pane:first", "70");
}

function loadAjaxForPreviewPopup() {
    // If simple mode then load data and show detail block
       if( !previewPopupLoadStatus) {
           $.ajax({
             url: 'loadDetailDataPreviewPopup.html',
             type: 'POST',
             dataType: 'json',
             data: {
               syouhinSysCode: syouhinSysCodeToPreview
             },
           })
           .success(function(data) {
             // Fill fitmodel table.
             initFitModelPopup(data.productManagementActionForm.listSyouhinFitModel);
             // Fit model grid filter
             jQuery("#fitModelPreviewBlock #previewPopupFitModelGrid").on('change', '.txtFilter', function (e) {
                var grid = $("#fitModelPreviewBlock #previewPopupFitModelGrid").data("kendoGrid");
                if (grid.dataSource.data().length > 0) {
                    grid.dataSource.filter({
                        logic: "and",
                        filters: [
                            {
                                operator: "contains",
                                value: jQuery("#makerFitModelGridPopup").val(),
                                field: "maker"
                            },
                            {
                                operator: "contains",
                                value: jQuery("#modelFitModelGridPopup").val(),
                                field: "model"
                            },
                            {
                                operator: "contains",
                                value: jQuery("#styleFitModelGridPopup").val(),
                                field: "style"
                            }
                        ]
                     });
                }
               });
             // Fill attribute all type table.
             initProductAttributeAllTypePopup(data.productManagementActionForm.listAttributeAllType);
             // Fill link reason grid
             initLinkReasonGridPopup(data.productManagementActionForm.listLinkReason);
             // Fill video table.
             initVideoPopup(data.productManagementActionForm.listVideo);
             // Fill guest input table.
             initGuestInputGridPopup(data.productManagementActionForm.listGuestInput);

             // Set load status to true
             previewPopupLoadStatus = true;
             //Hide loading image if it display and toggle to show detail.
             if("block" == jQuery("#processLoading").css("display")) {
              // set current mode to detail
                 previewPopupMode = "detail";
                 toggleToShowDetailBlockPreviewPopup();
                 hideProcessBar();
             }
           });
       }
   };

/* EOE productReview function */

/* BOE productDetailSearch function */

/* EOE productDetailSearch function */

 //Validate date range
function validateKousinDateRange() {
    var result = true;
    var fromDate = $("#tblUpdatedFrom").val();
    var toDate = $("#tblUpdatedTo").val();
    if (undefined != fromDate && "" != fromDate && undefined != toDate && "" != toDate) {
    	var dateFrom = new Date($("#tblUpdatedFrom").val());
    	var dateTo = new Date($("#tblUpdatedTo").val());
    	if (dateFrom > dateTo) {			
    		result = false;
    		$("#tblUpdatedFrom, #tblUpdatedTo").addClass("error");
    	}
   }
   return result;
}

   function validateTextboxKousinDate(idValue) {
   	 if ($("#" + idValue).val() == "") {
          jQuery("#" + idValue).removeClass("error");
          return true;
   	}
   	$("#" + idValue).val(formatDateTimeWithSlash($("#" + idValue).val()));
      if (!validateDateTime(jQuery("#" + idValue).val())) {
          //Invalid date
          jQuery("#" + idValue).addClass("error");
          return false;
      } else {
          //Valid date
          jQuery("#" + idValue).removeClass("error");
          return true;
      }
   }

   //Validate date
   function validateKousinDate() {
       var result = true;
       if (!validateTextboxKousinDate("tblUpdatedFrom")) {
           result = false;
       }
       if (!validateTextboxKousinDate("tblUpdatedTo")) {
           result = false;
       }
       if (!result) {
           return false;
       }
       result = validateKousinDateRange();
       return result;
   }
/************************************************ EOE Function ************************************************/ 
