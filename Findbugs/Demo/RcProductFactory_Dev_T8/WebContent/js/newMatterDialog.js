$(document).ready(function() {
	// Used for disabling submit button when Redmine API is being called.
	var apiRunning = false;
	// Used for disabling submit button when Create API is being called.
	var submitting = false;
	
	// Init everything.
	//boe thai.son
	//initMessagePanels();
	//function initMessagePanels equal 3 following lines:
	$('span[data-for = "redmineIssueId"]').remove();
	$('span[data-for = "matterName"]').remove();
	$('span[data-for = "changeUser"]').remove();
	//eoe thai.son
	initRedmineIssueId();
	initMatterName();
	initManagerList();
	initAddMatterButton();
	initNewMatterDialog();
	initCancelButton();
	initSubmitButton();
	enableSubmitButtonWhenRedmineApiRunning();
	enableSubmitButtonWhenCreateApiRunning();
	
	function initRedmineIssueId() {
		$("#redmineIssueId").focusout(function() {
			//boe thai.son
			// Remove error message.
			//clearErrorMessage("#redmineIssueId", "#redmineIssueIdMessage");
			$('span[data-for = "redmineIssueId"]').remove();
			//eoe thai.son
			// Get issueId.
			var issueId = $("#redmineIssueId").val().trim();
			
			// Validate numeric.
			var valid = validateRedmineIssueId(issueId);
			
			// Call API to check Remine issue ID.
			if(valid) {
				checkRedmineIssueId(issueId);
			}
		});
	}
	
	function initMatterName() {
		$("#matterName").focusout(function() {
			//boe thai.son
			// Remove error message.
			//clearErrorMessage("#matterName", "#matterNameMessage");
			$('span[data-for = "matterName"]').remove();
			//eoe thai.son
			// Get matterName.
			var matterName = $("#matterName").val().trim();
			
			// Validate matter name.
			validateMatterName(matterName);
		});
	}
	
	function initCancelButton() {
	    $("#btnCancel").click(function() {
	    	var newMatterDialog = $("#newMatterDialog").data("kendoWindow");
	        newMatterDialog.close();
	    });
	}
	
	function initSubmitButton() {
		$("#btn_submit").click(function(e) {
			//boe thai.son
			//clear message:
			$('span[data-for = "redmineIssueId"]').remove();
			$('span[data-for = "matterName"]').remove();
			$('span[data-for = "changeUser"]').remove();
			//eoe thai.son
			// Validate error.
			var errorCount = $(".newMatterDialogError").length;
			if(errorCount > 0) {
				return;
			}
			
			// Prevent multiple click.
			if(submitting) {
				return;
			}
			disableSubmitButtonWhenCreateApiRunning();
			
			// Submit only if all values is not empty and API is not running.
			var matterName = $("#matterName").val();
			var redmineIssueId = $("#redmineIssueId").val();
			var managerValue = $("#lstAdmin option:selected").val();
			
			// Validate all.
			var valid1 = validateRedmineIssueId(redmineIssueId);
			var valid2 = validateMatterName(matterName);
			var valid3 = validateManagerName(managerValue);
			
			if(!apiRunning && valid1 && valid2 && valid3) {
				createMatter();
			} else {
				enableSubmitButtonWhenCreateApiRunning();
			}
	    });
	}
	
	function createMatter() {
		var redmineIssueId = $("#redmineIssueId").val();
		var matterName = $("#matterName").val();
		var changeUser = $("#lstAdmin option:selected").val();
		
		$.ajax({
			type : 'POST',
			url : 'createNewMatter.html',
			dataType : 'json',
	 		 data : {
	 			'redmineIssueId': redmineIssueId,
	 			'matterName' : matterName,
	 			'changeUser' : changeUser
	 		 },
	 		beforeSend : function(){
				 showProcessBar();
		    },
			success : function(data) {
				console.log(data);
				if(data == null) {
					// TODO Thanh-san, handle error here please.
					enableSubmitButtonWhenCreateApiRunning();
					return;
				}
				
				// Close dialog.
		    	var newMatterDialog = $("#newMatterDialog").data("kendoWindow");
		    	newMatterDialog.close();
				
				var message = $("#insertSuccessMessage").val();
				// BOE hoang.ho 2014/04/11 fix bug message bar
//				$("#matterManageMessage").addClass("k-block k-success-colored").text(message).show();
				// BOE hoang.ho 2014/04/11 fix bug message bar
				
				// Refresh grid.
				viewMessage(message,0);
				$("#btnSearch").click();
			    
			    enableSubmitButtonWhenCreateApiRunning();
			},
			complete : function(){
	        	hideProcessBar();
	        }
		});
	}
	
	function initAddMatterButton() {
	    $("#btnAddNew").click(function(){
	    	var newMatterDialog = $("#newMatterDialog").data("kendoWindow");
	    	newMatterDialog.center().open();
	    });
	}
	
	function initNewMatterDialog() {
		var newMatterTitle = $("#newMatterTitle").val();
	    $("#newMatterDialog").kendoWindow({
	        maxWidth: 1000,
	        maxHeight: 800,
	        title: newMatterTitle,
	        resizable: false,
	        modal: true,
	        visible: false,
	        draggable: false,
	        open: function () {
	        	// TODO Init something on first loading.
	        	$("#redmineIssueId").val("");
	        	$("#matterName").val("");
	    		$("#lstAdmin").kendoDropDownList().data('kendoDropDownList').value(" ");
	    		//boe thai.son
	        	//initMessagePanels();
	    		//function initMessagePanels equal 3 following lines:
	    		$('span[data-for = "redmineIssueId"]').remove();
	    		$('span[data-for = "matterName"]').remove();
	    		$('span[data-for = "changeUser"]').remove();
	        	//eoe thai.son
	    		/*BOE Tran.Thanh Fix init Numeric TextBox*/
	    		//Utils.bindFormatOnTextfield("redmineIssueId", Utils._TF_NUMBER);
	    		numericTextBox("#redmineIssueId");
	    		/*EOE Tran.Thanh Fix init Numeric TextBox*/
	    		
	    		/*BOE TRAN_THANH 2014/03/24 */
	    		if ($("#hMatterChangeUserId").val() != null) {
	    			//console.log($("#hMatterChangeUserId").val());
	    			if ($("#hMatterChangeUserId").val().indexOf("LIMITED_USER_ID") >= 0) {
	    				var dropdownlist = $("#lstAdmin").kendoDropDownList().data('kendoDropDownList');
	    				dropdownlist.value($("#hLoginIdPopup").val());
	    				dropdownlist.enable(false);
	    			}
	    		}
	    		/*EOE TRAN_THANH 2014/03/24 */
	        }
	    });
	}
	
	function initManagerList() {
		$("#lstAdmin").kendoDropDownList().data('kendoDropDownList').value(" ");
		
		// TODO Should validate here???
		$("#lstAdmin").change(function() {
			var managerName = $(this).val();
			validateManagerName(managerName);
		});
	}

	function checkRedmineIssueId(issueId) {
		$.ajax({
			type : 'POST',
			url : 'selectIssueRedmineByIssueId.html',
			dataType : 'json',
	 		 data : {
	 			'issueId' : issueId
	 		 },
	 		beforeSend: function() {
	 			disableSubmitButtonWhenRedminApiRunning();
	 			showProcessBar();
	 		},
			success : function(data) {
				if(data["issueId"] == null || data["issueId"] == "") {
					// Get and show OK messge.
					var ngMessage = $("#newMatterNgMessage").val();
					//boe thai.son comment:
					//showNgMessage("#redmineIssueId", "#redmineIssu12345678eIdMessage", ngMessage);
					$('#divRedmineIssueId').append('<span class="k-widget k-tooltip k-tooltip-validation k-invalid-msg"'+
							'data-for="redmineIssueId" role="alert"><span class="k-icon k-warning"> </span>'+ngMessage+'</span>');
					//eoe thai.son
					// Clear inputs.
					$("#matterName").val("");
					// Reset comboBox.
					//BOE #7208 Tran.Thanh : not replace when role user
					if ($("#lstAdmin").data('kendoDropDownList').disabled = false) {
						$("#lstAdmin").data('kendoDropDownList').value(" ");
					}

				} else {
					// Get and show NG message.
					var okMessage = $("#newMatterOkMessage").val();
					//boe thai.son comment:
					//showOkMessage("#redmineIssueId", "#redmineIssueIdMessage", okMessage);
					$('#divRedmineIssueId').append('<span class="newMatterDialogOk" data-for="redmineIssueId" style="display: inline;">'
							+okMessage+'</span>');
					//eoe thai.son
					// Fill matter name.
					var subject = data.subject;
					$("#matterName").val(subject);
					//boe thai.son clear message of matterName( not of redmineIssueId)
					// Clear message (if any).
					//clearErrorMessage("#matterName", "#matterNameMessage");
					$('span[data-for = "matterName"]').remove();
					//eoe thai.son
					// Validate matter name AGAIN.
					validateMatterName(subject);
					
					// Show OK message.
					// TODO Uncomment to show OK message.
					//showOkMessage("#matterName", "#matterNameMessage", okMessage);
				    
					// Select manager.
					var assignedLogin = data.assignedLogin;
					$("#lstAdmin").kendoDropDownList().data('kendoDropDownList').value(assignedLogin);
				}
				
				enableSubmitButtonWhenRedmineApiRunning();
				hideProcessBar();
			}
		});
	}
	
	function validateMatterName(matterName) {
		var valid = true;
		if(matterName == "") {
			// Get and set error message.
			var requiredMessage = $("#newMatterRequiredMessage").val();
			var newMatterRedmineIssueIdLabel = $("#newMatterRedmineMatterLabel").val();
			requiredMessage = requiredMessage.replace("{0}", newMatterRedmineIssueIdLabel);
			//boe thai.son comment:
			//showErrorMessage("#matterName", "#matterNameMessage", requiredMessage);
			$('#divMatterName').append('<span class="k-widget k-tooltip k-tooltip-validation k-invalid-msg"'+
					'data-for="matterName" role="alert"><span class="k-icon k-warning"> </span>'+requiredMessage+'</span>');
			//eoe thai.son
			valid = false;
			
		} else if(matterName.length > 128) {
			var maxLengthMessage = $("#newMatterMaxLengthMessage").val();
			maxLengthMessage = maxLengthMessage.replace("{0}", 128);
			//boe thai.son comment:
			//showErrorMessage("#matterName", "#matterNameMessage", maxLengthMessage);
			$('#divMatterName').append('<span class="k-widget k-tooltip k-tooltip-validation k-invalid-msg"'+
					'data-for="matterName" role="alert"><span class="k-icon k-warning"> </span>'+maxLengthMessage+'</span>');
			//eoe thai.son
			valid = false;
			
		} else {
			//boe thai.son
			//clearErrorMessage("#matterName", "#matterNameMessage");
			$('span[data-for = "matterName"]').remove();
			//eoe thai.son
		}
		return valid;
	}
	
	function validateRedmineIssueId(issueId) {
		var valid = true;
		if(issueId == "") {
			var remindIssueLabel = $("#newMatterRedmineIssueIdLabel").val();
			var requiredMessage = $("#newMatterRequiredMessage").val().replace("{0}", remindIssueLabel);
			//boe thai.son comment:
			//showErrorMessage("#redmineIssueId", "#redmineIssueIdMessage", requiredMessage);
			$('#divRedmineIssueId').append('<span class="k-widget k-tooltip k-tooltip-validation k-invalid-msg"'+
					'data-for="redmineIssueId" role="alert"><span class="k-icon k-warning"> </span>'+requiredMessage+'</span>');
			//eoe thai.son
			valid = false;
			
		} else if(issueId.length > 8) {
			var message = $("#newMatterMaxLengthMessage").val();
			message = message.replace("{0}", 8);
			//boe thai.son comment:
			//showErrorMessage("#redmineIssueId", "#redmineIssueIdMessage", message);
			$('#divRedmineIssueId').append('<span class="k-widget k-tooltip k-tooltip-validation k-invalid-msg"'+
					'data-for="redmineIssueId" role="alert"><span class="k-icon k-warning"> </span>'+message+'</span>');
			//eoe thai.son
			valid = false;
			
		} else if(!validateNumberic(issueId)) {
			// Get and show number message.
			/* BOE change message by Luong.Dai at 2014/03/19*/
			/*var numberOnlyMessage = $("#newMatterNumberOnlyMessage").val().replace("{0}", 8);*/
			var numberOnlyMessage = $("#newMatterNumberOnlyMessage").val();
			/* EOE change message by Luong.Dai at 2014/03/19*/
			//boe thai.son comment:
			//showErrorMessage("#redmineIssueId", "#redmineIssueIdMessage", numberOnlyMessage);
			$('#divRedmineIssueId').append('<span class="k-widget k-tooltip k-tooltip-validation k-invalid-msg"'+
					'data-for="redmineIssueId" role="alert"><span class="k-icon k-warning"> </span>' + numberOnlyMessage + '</span>');
			//eoe thai.son
			valid = false;
			
		} else {
			//boe thai.son comment:
			//clearErrorMessage("#redmineIssueId", "#redmineIssueIdMessage");
			$('span[data-for = "redmineIssueId"]').remove();
			//eoe thai.son
		}
		
		return valid;
	}
	
	function validateManagerName(managerValue) {
		var valid = true;
		if(managerValue == " ") {
			var managerNameLabel = $("#newMatterRedmineManagerNameLabel").val();
			var requiredMessage = $("#newMatterRequiredMessage").val().replace("{0}", managerNameLabel);
			//boe thai.son comment:
			//showErrorMessage("", "#managerNameMessage", requiredMessage);
			$('#divChangeUser').append('<span class="k-widget k-tooltip k-tooltip-validation k-invalid-msg"'+
					'data-for="changeUser" role="alert"><span class="k-icon k-warning"> </span>'+requiredMessage+'</span>');
			//eoe thai.son
			valid = false;
		} else {
			//boe thai.son
			//clearErrorMessage("", "#managerNameMessage");
			$('span[data-for = "changeUser"]').remove();
			//eoe thai.son
		}
		return valid;
	}

	function disableSubmitButtonWhenRedminApiRunning() {
		apiRunning = true;
		$("#btn_submit").prop("disabled", true).addClass("k-state-disabled");
	}

	function enableSubmitButtonWhenRedmineApiRunning() {
		apiRunning = false;
		$("#btn_submit").prop("disabled", false).removeClass("k-state-disabled");
	}
	
	function disableSubmitButtonWhenCreateApiRunning() {
		submitting = true;
		$("#btn_submit").prop("disabled", true).addClass("k-state-disabled");
	}

	function enableSubmitButtonWhenCreateApiRunning() {
		submitting = false;
		$("#btn_submit").delay(1000).prop("disabled", false).removeClass("k-state-disabled");
	}
	
	function initMessagePanels() {
		clearErrorMessage("#redmineIssueId", "#redmineIssueIdMessage");
		clearErrorMessage("#matterName", "#matterNameMessage");
		clearErrorMessage("", "#managerNameMessage");
	}
	
	function showErrorMessage(inputSelector, messagePanelSelector, message) {
		$(messagePanelSelector).text(message).removeClass().addClass("newMatterDialogError").show();
		if(inputSelector != "") {
			$(inputSelector).removeClass().addClass("k-error-colored");	
		}
	}

	function clearErrorMessage(inputSelector, messagePanelSelector) {
		$(messagePanelSelector).text("").removeClass().hide();
		$(inputSelector).removeClass();
	}

	function showOkMessage(inputSelector, messagePanelSelector, message) {
		$(messagePanelSelector).text(message).removeClass().addClass("newMatterDialogOk").show();
	}
	
	function showNgMessage(inputSelector, messagePanelSelector, message) {
		$(messagePanelSelector).text(message).removeClass().addClass("newMatterDialogNg").show();
	}
});