function confirmDeleteUser(userId,delFlg) {
    jQuery.fancybox({
        modal : true,
        autoSize: true,
        content : "     <div>"
                 +"<div id='heading-popup'>"
                 +          'ユーザーロック'
                 +"</div>"
                 +"     <div id='content-popup'>"
                 +      "<p>"
                 +          'ユーザーをロックします。よろしいですか？'
                 + "    </p>"
                 +"     <div style='text-align:center;padding:5px;'>"
                 + "        <input id='fancyConfirm_ok' class='btn-size-medium btn-color-blue' type='button' value='はい' onclick='deleteUser(\""+userId+"\",\""+delFlg+"\");' style='margin:7px;padding:5px;width:67px;'>"
                 + "        <input id='fancyConfirm_cancel' class='btn-size-medium btn-color-default' type='button' value='キャンセル' onclick='jQuery.fancybox.close();' style='margin:7px;padding:5px;width:67px;'>"
                 +"     </div>"
                 +"     </div>"
                 + "</div>"
    });
};

function confirmUnlockUser(userId,delFlg) {
    jQuery.fancybox({
        modal : true,
        autoSize: true,
        content : "     <div>"
                 +"<div id='heading-popup'>"
                 +         'ユーザーアンロック'
                 +"     </div>"
                 +"     <div id='content-popup'>"
                 + "    <p>"
                 +         'ユーザーをアンロックします。よろしいですか？'
                 + "    </p>"
                 +"     <div style='text-align:center;padding:5px;'>"
                 + "        <input id='fancyConfirm_ok' class='btn-size-medium btn-color-blue' type='button' value='はい' onclick='deleteUser(\""+userId+"\",\""+delFlg+"\");' style='margin:7px;padding:5px;width:67px;'>"
                 + "        <input id='fancyConfirm_cancel' class='btn-size-medium btn-color-default' type='button' value='キャンセル' onclick='jQuery.fancybox.close();' style='margin:7px;padding:5px;width:67px;'>"
                 +"     </div>"
                 +"     </div>"
                 + "</div>"
    });
};

function deleteUser(userId,delFlg) {
    jQuery.fancybox.close();
    window.location.href = "./changeStatusDetail.html?userId="+userId+"&delFlg="+delFlg+"&parent=userDetail";
};

jQuery(document).ready(function(){	
    function requiredFunction(value, element) {
        if ("" == value.trim()) {
            var message = jQuery.format(requiredFill, jQuery(element).parent().prev().html());
            jQuery(element).next().html(message);
            jQuery(element).next().css("display", "inline-block");
            jQuery(element).addClass("error");

            return false;
        } else {
            return true;
        }
    }
    
    function requiredApiFunction(value, element) {
        if ("" == value.trim()) {
            var message = "APIアクセスキー";
            jQuery(element).next().html(message);
            jQuery(element).next().css("display", "inline-block");
            jQuery(element).addClass("error");

            return false;
        } else {
            return true;
        }
    }

    jQuery("#userId").focusout(function() {
        requiredFunction(jQuery(this).val().trim(), jQuery(this));
    });

    jQuery("#userFirstName").focusout(function() {
        requiredFunction(jQuery(this).val().trim(), jQuery(this));
    });

    jQuery("#userLastName").focusout(function() {
        requiredFunction(jQuery(this).val().trim(), jQuery(this));
    });

    jQuery("#userPasswd").focusout(function() {
        if(jQuery("#flgPassChange").val() == 1) {
            requiredFunction(jQuery(this).val().trim(), jQuery(this));
        }
    });

    jQuery("#userPasswdConfirm").focusout(function() {
        if(jQuery("#flgPassChange").val() == 1) {
            requiredFunction(jQuery(this).val().trim(), jQuery(this));
        }
    });
    
    jQuery.validator.addMethod("validateUserId", function(value, element) {
  	  return this.optional(element) || /^[a-z0-9_\-\.]*$/i.test(value);
  	}, inputInvalid);

    jQuery.validator.addMethod("alphaNumberOnly", function(value, element) {
        return this.optional(element) || /^[a-zA-Z0-9.-_]+$/i.test(value);
    }, IdOrPassInputInvalid);
    
    jQuery.validator.addMethod("alphaNumberPassWord", function(value, element) {
        return this.optional(element) || /^[a-zA-Z0-9]+$/i.test(value);
    }, IdOrPassInputInvalid);

    jQuery.validator.addMethod("alphaNumberJapanAndSpacesOnly", function(value, element) {
        return this.optional(element) || /^[a-zA-Z0-9０-９ｂ-ｚＢ-Ｚｦ-ﾟァ-ンｧ-ﾝﾞﾟ\u3040-\u309f\u30a0-\u30ff\u4e00-\u9faf\s]+$/i.test(value);
    }, inputInvalid);
    jQuery.validator.addMethod("alphaEmail", function(value, element) {
        return this.optional(element) || /^(([A-Za-z0-9]+_)|([A-Za-z0-9]+\-)|([A-Za-z0-9]+\.))*[A-Za-z0-9]+@(([A-Za-z0-9]+\-)|([A-Za-z0-9]+\.)|([A-Za-z0-9]+_))*[A-Za-z0-9]{1,}\.[a-zA-Z]{2,}$/i.test(value);
    }, jQuery.format(inputInvalid));

    jQuery.validator.addMethod("lengthMax",
          jQuery.validator.methods.maxlength,
          jQuery.format(inputOutOfRange));
    jQuery.validator.addMethod("LengthMin",
            jQuery.validator.methods.minlength,
            jQuery.format(inputLessThanRange));
    //Check max length for password
    jQuery.validator.addMethod("lengthMaxPassword",
            jQuery.validator.methods.maxlength,
            inputLengthPasswordInvalid);
    //Check min length for password
    jQuery.validator.addMethod("LengthMinPassword",
            jQuery.validator.methods.minlength,
            inputLengthPasswordInvalid);
    //Check max length for passwordConfirm
    jQuery.validator.addMethod("lengthMaxPasswordConfirm",
            jQuery.validator.methods.maxlength,
            inputLengthPasswordConfirmInvalid);
    //Check min length for passwordConfirm
    jQuery.validator.addMethod("LengthMinPasswordConfirm",
            jQuery.validator.methods.minlength,
            inputLengthPasswordConfirmInvalid);
    //Check passwordConfirm is equal with pass
    jQuery.validator.addMethod("passConfirmEqualPass",
            jQuery.validator.methods.equalTo,
            passConfirmNotEqualPass);

    jQuery.validator.addMethod("requiredFill", function(value, element) {
        return requiredFunction(value.trim(), element);
    }, jQuery.format(requiredFill));
    
    jQuery.validator.addMethod("requiredApi", function(value, element) {
        return requiredApiFunction(value.trim(), element);
    }, "APIアクセスキーは必須項目です。");
    

    jQuery.validator.addMethod("checkExistedUserId", function(value, element) {
        return !checkExistedUserId(value.trim());
    }, existedUser);
    
    jQuery.validator.addMethod("usingRedmineGetApiKey", function(value, element) {
        return !usingRedmineGetApiKey();
    }, canNotGetApiAccessKey);

    function checkExistedUserId(userId) {
        var checkResult = true;
        jQuery.ajax({
            type: "POST",
            url: "checkExistedUserId.html",
            data: {
                userId: userId
            },
            async: false,
            dataType: "json",
            success: function(dataResult) {
                checkResult = eval(dataResult["validUserId"]);
            }
        });
        return checkResult;
    };
    
    jQuery.validator.addClassRules("userId", { 
                  requiredFill: jQuery("#userIdLbl").html()
                  ,validateUserId: jQuery("#userIdLbl").html()
                  , lengthMax: 30
                  , checkExistedUserId: true
                  });

    jQuery.validator.addClassRules("userPasswd", { 
    	alphaNumberPassWord: true
        , lengthMaxPassword: 30
        , LengthMinPassword: 5
        });

    jQuery.validator.addClassRules("userPasswdConfirm", { 
    	alphaNumberPassWord: true
        , lengthMaxPasswordConfirm: 30
        , LengthMinPasswordConfirm: 5
        , passConfirmEqualPass: "#userPasswd"
        });
    
    jQuery.validator.addClassRules("userPasswdRequired", { 
        requiredFill: jQuery("#userPasswdLbl").html()
        });
    jQuery.validator.addClassRules("userPasswdConfirmRequired", { 
        requiredFill: jQuery("#userPasswdConfirmLbl").html()
        });
    jQuery.validator.addClassRules("userName", {
        lengthMax: 10
        });
    
    jQuery.validator.addClassRules("userFirstName", { 
        requiredFill: jQuery("#userFirstNameLbl").html()
        , alphaNumberJapanAndSpacesOnly: jQuery("#userFirstNameLbl").html()
        });

    jQuery.validator.addClassRules("userLastName", { 
        requiredFill: jQuery("#userLastNameLbl").html()
        , alphaNumberJapanAndSpacesOnly: jQuery("#userLastNameLbl").html()
        });
    jQuery.validator.addClassRules("userEmail", {
          lengthMax: 80
        });
    jQuery.validator.addClassRules("userRedmineApiKey", {
    	requiredApi: jQuery("#userRedmineApiKey").html()
        });    
    jQuery.validator.addClassRules("userEmail1", {
    	requiredFill: jQuery("#userEmail1Lbl").html()
        ,alphaEmail: jQuery("#userEmail1Lbl").html()
        });

    jQuery.validator.addClassRules("userEmail2", { 
        alphaEmail: jQuery("#userEmail2Lbl").html()
        });

    jQuery("#userDetailForm").validate({
            errorElement: "span",
            onkeyup: false,
            onclick: false
     });
});

function cancelClick(){
	var previousUrl = document.referrer;
    if("" != previousUrl) {
        window.location = previousUrl;
        return;
    }
    /* BOE by Luong.Dai at 2014/04/08 Show progress bar */
    showProcessBar();
    /* EOE by Luong.Dai at 2014/04/08 Show progress bar */
    window.location = defaultRedirectPage;
    return;
}

function make_base_auth(user, password) {
	  var tok = user + ':' + password;
	  var hash = btoa(tok);
	  return "Basic " + hash;
}


function usingRedmineGetApiKey() {
	var res = true;
    var username = jQuery("#userId").val();
    var password = jQuery("#userPasswd").val();
    
    if (username == "" || password == "") {
    	jQuery("#userId").valid();
    	jQuery("#userPasswd").valid();
    	return false;
    }

    jQuery.ajax({
        type: "POST",
        url: "selectRedmineApiKey.html",
        data: {
            userId: username,
            userPasswd: password
        },
        dataType: "json",
        success: function(dataResult) {
           var data = dataResult["apiAccessKey"];
           if(data.length > 0) {
        	   jQuery("#userRedmineApiKey").removeClass("error");
        	   jQuery("#userRedmineApiKeyError").html("");
        	   jQuery("#userRedmineApiKeyHidden").val(dataResult["apiAccessKey"]);
        	   jQuery("#userRedmineApiKey").val(dataResult["apiAccessKey"]);
           } else {
        	   res = false;
        	   jQuery("#userRedmineApiKey").val("");
        	   jQuery("#userRedmineApiKey").addClass("error");
        	   jQuery("#userRedmineApiKeyHidden").val("");
        	   jQuery("#userRedmineApiKeyError").html(canNotGetApiAccessKey);
           }
        },
        error: function (request, textStatus, errorThrown) {
            console.log(request.responseText);
            console.log(textStatus);
            console.log(errorThrown);
            
            res = false;
     	    jQuery("#userRedmineApiKey").val("");
     	    jQuery("#userRedmineApiKey").addClass("error");
     	    jQuery("#userRedmineApiKeyError").html(canNotGetApiAccessKey);
        }
    });
    return res;
}

