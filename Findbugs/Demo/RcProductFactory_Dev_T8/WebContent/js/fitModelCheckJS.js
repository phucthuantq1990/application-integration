//flag error in popup insert fit model
var ERROR_FIT_MODEL_FLG = false;

$(window).load(function(){
	/**
	 * BOF Handle keyup tab-key on field filter
	 * @rcv!cao.lan 2014/03/10
	 * **/
	$('#gridModelFit4 .k-grid-header-wrap').keyup(function(e) {
	    var code = e.keyCode || e.which;
	    // code = 9 is tab-key
	    if (code == '9') {
	    	// get scroll left in grid header 
	    	var left = $('#gridModelFit4 .k-grid-header-wrap').scrollLeft();
	    	// if tab-key and scroll left change then scroll left k-grid-content
    		$("#gridModelFit4 .k-grid-content").scrollLeft(left);
	    }
	 });
	/** EOF @rcv!cao.lan 2014/03/10 **/
});
var loadingImage = '<div><a href="http://www.webike.net/catalogue/10888/0-35-5041.jpg" id="selector_0" class="" rel="fancy_box" onclick="return false;">'
						+'<img class="selector_thumb" src="./images/processing.gif" style="margin-left: 110%;margin-top: 10%;">'
					+'</a></div>';
//------variables for bottom grid:
var bottomGrid;
var currentPage = 1;
var dataSource;
//Search condition
var matterNo = "";
var productId = "";
var productSyouhinSysCode = "";
var brandName = "";
var productName = "";
var productCode = "";
var productModelCheckFlg = "-1";
var productFitModelMaker = "";
var productFitModelModel = "";
var productFitModelMisMatchFlg = "";
var productModelErrorFlg = "0";
var descriptionSummary = "";
var productSelectDisplay = "";
var productProperPriceFrom = "";
var productProperPriceTo = "";
var categoryName = "";
var sortResetFlg = 0;
var msgFail = "";
var msgSuccess = "";
var msgUnchange = "";
var requiredFillMsg = "";
var maxLengthMsg = "";
var invalidInputMsg = "";

//Array Product Id Checkbox
var arrayProductId = "";
//Aray Product Model Error Flg
var arrayProductModelErrorFlg = "";

//------end bottom grid
//Current selected thumb(image thumb).
var currentSelectedThumb = "0";
var noImageURL;
//If Thumb loading, isImageThumbLoading = true
var isImageThumbLoading = false;
// If textbox focus, isTextboxFocus = true
var isTextboxFocus = false;

// globel variable for insert Mode and update mode.
var insertMode = true;

//global check popup add model
var modePopup = false;

////////////////BOE_Nguyen.Chuong//////////////////
var productSummaryTooltip;
var productDescriptionRemarksTooltip;
var productNameTooltip;
////////////////EOE_Nguyen.Chuong//////////////////

////////////////BOE_Nguyen.Hieu////////////////////
var selectedKendoClass = "k-alt k-state-selected";
var selectedRowClass = "selectedGridRow";
////////////////EOE_Nguyen.Hieu////////////////////

////////////////BOE_Vu.Long//////////////////////
//global code here
var maker = "";
var modelDisplacement = "";
var modelName = "";
//Index of current selected row
var currentIdxGrid2 = -1;
var currentIdxGrid4 = -1;
var isLoadFromGrid4 = false;
var grid4RowSelected = "";
var grid4CurrentProductId = "";
var grid4RowSelectedIndex = "";
var isProductRegistrationFlg = false;
////////////////EOE_Vu.Long//////////////////////
////////////////BOE_Hoang.Ho//////////////////////
//variable of Kendo validator.
var validatable;
var validatablePopUp;
// using store old value when update on model fit change list (grid 2).
var oldTxtMaker = "";
var oldTxtModel = "";
var oldTxtStyle = "";
////////////////EOE_Hoang.Ho//////////////////////
var elemsIdPreventKeyUpDownArr = ["txtModelMaker","txtDisplacement","txtmodelName","tbProductFitModelMaker"/*,"tbProductFitModelModel"*/];
var elemsClsPreventKeyUpDownArr = ["k-dropdown"];
var selectedRodGrid4 = "";
var dataSourceGrid4 = "";
var isFromChangeRow = false;
/*
 * Used for prevent hot key when dialog display.
 * return true when any dialog has opened.
 * @author hoang.ho
 * @since 2014/03/13
 */
function checkDialogIsOpen() {
	var result = false;
	$(".k-overlay").each(function(){
		var cssState = $(this).css("display");
		if (cssState == "block") {
			result = true;
		}
	});
	var cssState = $("#processLoading").css("display");
	if (cssState == "block") {
		result = true;
	}
	return result;
}
/*
 * Prevent short cut up and down on drop down list.
 * @author hoang.ho
 * @param event event of dropdown list
 * @since 2014/02/15
 */
function checkShortCutPrevent(event) {
	  var id = event.target.id;
	  if ($.inArray(id, elemsIdPreventKeyUpDownArr) > -1) {
		  return true;
	  }
	  var cls = event.target.className;
	  for (var i = 0; i < elemsClsPreventKeyUpDownArr.length; i++ ) {
		  if (cls.indexOf(elemsClsPreventKeyUpDownArr[i]) > -1) {
			  return true;
		  }
	  }
	  return false;
}
function addKeyPressOnGrid2() {
	  //hotkey up to select previous model in grid
	  shortcut.add("Up", function(e) {
		  // prevent hot key when dialog display.
		  if(checkDialogIsOpen()) {
			  e.preventDefault();
			  return;
		  }
		  //Prevent short cut up on drop down list.
		  if (checkShortCutPrevent(e)) {
			  e.preventDefault();
			  return;
		  }
		  changeRow(false, currentIdxGrid2, "gridModelFit2");
		  
		  var grid2 = $("#gridModelFit2").data("kendoGrid");
          var selectedRows = grid2.select();
		  var test = selectedRows.get(0);
		  var col1 = $(test).find("td:nth-child(2)");
		  loadVehicleDetail(col1);
	  });
	  
	  //hotkey down to select next model in grid
	  shortcut.add("Down", function(e) {
		  // prevent hot key when dialog display.
		  if(checkDialogIsOpen()) {
			  e.preventDefault();
			  return;
		  }
		  //Prevent short cut down on drop down list.
		  if (checkShortCutPrevent(e)) {
			  e.preventDefault();
			  return;
		  }
		  changeRow(true, currentIdxGrid2, "gridModelFit2");
		  
		  var grid2 = $("#gridModelFit2").data("kendoGrid");
          var selectedRows = grid2.select();
		  var test = selectedRows.get(0);
		  var col1 = $(test).find("td:nth-child(2)");
		  loadVehicleDetail(col1);
	  });
}

function removeKeyPressOnGrid2() {
	  shortcut.remove("Up");
	  shortcut.remove("Down");
}

function clearGrid2OldData() {
  $("#txtMakerDialog").val("");
  $("#txtModelDialog").val("");
  $("#txtStyleDialog").val("");
  $("#modelMakerSearch").val("");
  $("#modelDisplacementSearch").val("");
  $("#modelNameSearch").val("");
  clearKendoVailidate("txtMakerDialog");
  clearKendoVailidate("txtModelDialog");
  realoadGrid("addModelDialogGrid");
}

function clearKendoVailidate(id) {
  var parent = $("#" + id).parent();
  var errorMsgElem = parent.find(".k-invalid-msg");
  errorMsgElem.hide();
}

jQuery(document).ready(function() {
    $("#menu").kendoMenu();

	/* BOE hoang.ho 2014/03/18 common variable after page load completed*/
	requiredFillMsg = $("#requiredFill").val();
	maxLengthMsg = $("#maxLengthMsg").val();
	invalidInputMsg = $("#invalidInputMsg").val();
	
	msgFail = $("#msgFail").val();
	msgSuccess = $("#msgSuccess").val();
	msgUnchange = $("#msgUnchange").val();
    noImageURL = jQuery("#noImageURL").val();
    /* EOE hoang.ho 2014/03/18 common variable after page load completed*/
    
  ////////////////BOE_Nguyen.Chuong//////////////////
    //Create tooltip for productInfo
    //Tooltip for descriptionSummary
    productSummaryTooltip = $("#productInfoDescriptionSummary span").kendoTooltip({
        content: $("#productInfoDescriptionSummary span").html(),
        animation : {
            close : {
                effects : "fade:out"
            },
            open : {
                effects : "fade:in",
                duration : 300
            }
        }
    });
    //Tooltip for descriptionSummary
    productDescriptionRemarksTooltip = $("#productInfoDescriptionRemarks span").kendoTooltip({
        content: $("#productInfoDescriptionRemarks span").html(),
        animation : {
            close : {
                effects : "fade:out"
            },
            open : {
                effects : "fade:in",
                duration : 300
            }
        }
    });
    //Tooltip for productInfoProductName
    productNameTooltip = $("#productInfoProductName span").kendoTooltip({
        content: $("#productInfoProductName span").html(),
        animation : {
            close : {
                effects : "fade:out"
            },
            open : {
                effects : "fade:in",
                duration : 300
            }
        }
    });
  ////////////////EOE_Nguyen.Chuong//////////////////

  ////////////////BOE_Le.Dinh////////////////////////
  //documentready init code here
  inputNumbericOnly();
  ////////////////EOE_Le.Dinh///////////////////////

  ////////////////BOE_Vu.Long//////////////////////
  //documentready init code here
  //hotkey alt + s to call event in No.21
  shortcut.add("alt+s", function(e) {
	  // prevent hot key when dialog display.
	  if(checkDialogIsOpen()) {
		  e.preventDefault();
		  return;
	  }
      $("#btnUpdate2").trigger("click");
  });
  
  addKeyPressOnGrid2();

  //hotkey ctrl+shift+pageup to select previous product in grid
  shortcut.add("ctrl+shift+pageup", function(e) {
	  // prevent hot key when dialog display.
	  if(checkDialogIsOpen()) {
		  e.preventDefault();
		  return;
	  }
      if (currentIdxGrid4 != -1) {
    	  grid4CurrentProductId = getSelectedProductId();
          changeRow(false, currentIdxGrid4, "gridModelFit4");
          // Get selected row data items.
          var grid4 = $("#gridModelFit4").data("kendoGrid");
          // get selected row
          var selectedRows = grid4.select();
          // get data from row selected
          var data = grid4.dataItem(selectedRows);
          // get selected index
          grid4RowSelectedIndex = selectedRows.index();
          // process update selected row
          //checkProductChange(grid4CurrentProductId, grid4RowSelectedIndex);
          selectedRodGrid4 = selectedRows;
          dataSourceGrid4 = data;
          if (!validateDataGrid()) {
        	  showProcessBar();
        	  checkProductChange(grid4CurrentProductId, grid4RowSelectedIndex);
              /*// Get data of selected row
        	  if (data != null) {
        		  bindProductAllData(selectedRows, data);
        	  }*/
          } else {
              //isLoadFromGrid4 = true;
        	  var productResgistration = $("#prdResgistration").val();
        	  if (productResgistration == 0) {
        		  var initConfirmDialog = $("#confirmChangeRowDialog").data("kendoWindow");
                  initConfirmDialog.center().open();
        	  } else {
        		  showProcessBar();
            	  checkProductChange(grid4CurrentProductId, grid4RowSelectedIndex);
        	  }
          }
      }
  });
  //hotkey ctrl+shift+pagedown to select next product in grid
  shortcut.add("ctrl+shift+pagedown", function(e) {
	  // prevent hot key when dialog display.
	  if(checkDialogIsOpen()) {
		  e.preventDefault();
		  return;
	  }
      if (currentIdxGrid4 != -1) {
    	  grid4CurrentProductId = getSelectedProductId();
          changeRow(true, currentIdxGrid4, "gridModelFit4");
          // Get selected row data items.
          var grid4 = $("#gridModelFit4").data("kendoGrid");
          // get selected row
          var selectedRows = grid4.select();
          // get data from selected row
          var data = grid4.dataItem(selectedRows);
          // get selected index
          grid4RowSelectedIndex = selectedRows.index();
          // process update selected row
          //checkProductChange(grid4CurrentProductId, grid4RowSelectedIndex);
          selectedRodGrid4 = selectedRows;
          dataSourceGrid4 = data;
          if (!validateDataGrid()) {
        	  showProcessBar();
        	  checkProductChange(grid4CurrentProductId, grid4RowSelectedIndex);
              // Get data of selected row
        	  /*if (data != null) {
        		  bindProductAllData(selectedRows, data);
        	  }*/
          } else {
        	  //isLoadFromGrid4 = true;
        	  var productResgistration = $("#prdResgistration").val();
        	  if (productResgistration == 0) {
        		  var initConfirmDialog = $("#confirmChangeRowDialog").data("kendoWindow");
                  initConfirmDialog.center().open();
        	  } else {
        		  showProcessBar();
            	  checkProductChange(grid4CurrentProductId, grid4RowSelectedIndex);
        	  }
          }
      }
  });

  //Hotkey Alt + N to change mode new.
  shortcut.add("alt+N", function(e) {
	  // prevent hot key when dialog display.
	  if(checkDialogIsOpen()) {
		  e.preventDefault();
		  return;
	  }
      jQuery("#btnInsertMode").click(); 
  });

  //Start comfirm submit dialog.
  $("#confirmSubmitDialog").kendoWindow({
      maxWidth: 400,
      minWidth: 250,
      minHeight: 100,
      resizable: false,
      modal: true,
      visible: false,
      title : $("#hdTitle").val()
  });
  $("#btnSave").click(function (e) {
	  var productResgistration = $("#prdResgistration").val();
      e.preventDefault();
      // get msg string of error or success
      if (!validateDataGrid()) {
        $("#message").addClass("has-message msg-error");
        $("#message").text(msgUnchange);
        return;
      }
      // remove all class of msg div and clear msg
      clearMsgText();
      if (productResgistration == 1) {
    	  var win = $("#cannotChangeProductDialog").data("kendoWindow");
          win.center();
          win.open();
          return;
      }
      // show popup confirm
      var win = $("#confirmSubmitDialog").data("kendoWindow");
      win.center();
      win.open();
  });
  // End create dialog for screen.***
  $("#btnSubmit").click(function (event, selectedRows, data) {
    var win = $("#confirmSubmitDialog").data("kendoWindow");
    processButtonSave(selectedRows, data);
    win.close();
  });
  $("#btnCancelSubmit").click(function () {
    var win = $("#confirmSubmitDialog").data("kendoWindow");
    win.close();
  });
  ////////////////EOE_Vu.Long//////////////////////
    
    createThumbList();
    //Init Please Select Dialog
    initPleaseSelectDialog();
    //Init Duplicate Value Dialog
    initDuplicateValueDialog();
    //Init Add Model dialogy
    initAddModelDialog();
    //Init Confirm Dialog
    initConfirmDialog();
    // Init popup update
    initDisplayModelProduct();
    // Init popup cnofirm change row
    initConfirmChangeRowDialog();
    // Init can not change dialog
    initCannotChangeProductDialog();

    // Mouseover event for auto load thumb into image thumb.
    jQuery(document).on("mouseover", ".list_thumb", function() {
        thumbId = jQuery(this).attr('thumbId');
        if (thumbId === undefined)
            return false;
        // Switch thumb to image thumb
        switchThumb(thumbId);
    });
    
    // Load fancybox when click image thumb.
    jQuery("#imageAhref").click(function() {
        jQuery(".fancy_box").fancybox({
            'showCloseButton' : false,
            'enableEscapeButton' : true,
            'padding' : 10,
            'transitionIn' : 'elastic',
            'transitionOut' : 'fade',
            beforeShow: function(){
                // Thumb image is Loading
                isImageThumbLoading = true;
            },
            beforeClose: function() {
                // Thumb image is closed
                isImageThumbLoading = false;
            }
        });
    });

    //dataSource for attribute grid
    var dataSource = new kendo.data.DataSource({
        autoBind: false,
        // 50 product in 1 page
        transport: {
            read: {
                type: "POST",
                dataType: "json",
                url: "loadProductAllDataAjax.html",
                data:  function (data) {
                    
                },
                beforeSend :function() {
                    //Hide processing of Kendo.
                    jQuery("#grid .k-loading-image").css("background-image", "none");

                },
                cache: false,
            }
        },
        schema: {
            data: "fitModelCheckActionForm.listFitModelOfProduct"
        }
    });

     jQuery("#topLeftBlock #gridModelFit1").on('change', '.txtFilter', function (e) {
         var grid = $("#topLeftBlock #gridModelFit1").data("kendoGrid");
         if (grid.dataSource.data().length > 0) {
             grid.dataSource.filter({
                 logic: "and",
                 filters: [
                     {
                         operator: "contains",
                         value: jQuery("#fitModelMakerGrid1").val(),
                         field: "fitModelMaker"
                     },
                     {
                         operator: "contains",
                         value: jQuery("#fitModelModelGrid1").val(),
                         field: "fitModelModel"
                     },
                     {
                         operator: "contains",
                         value: jQuery("#fitModelStyleGrid1").val(),
                         field: "fitModelStyle"
                     }
                 ]
              });
         }
     });

    // Process insert row in kendo grid2e.preventDefault();
    function processInsert() {
        // get data from popup and insert into grid.

        var txtMaker = ($("#txtMaker").val() + "").trim();
        var txtModel = ($("#txtModel").val() + "").trim();
        var txtStyle = ($("#txtStyle").val() + "").trim();

        var grid = $("#gridModelFit2").data("kendoGrid");
        var dataSource = grid.dataSource;
    	
    	  // create fit model sort value
        var sortKey = 0;
        // Get grid from database.
        var raw = dataSource.data();
        var rowModel = "";
        var rowMarker = "";
        var rowStyle = "";
        var length = raw.length;
        for (var i = length - 1; i >= 0; i--) {
            // get value and check duplicate
            rowModel = raw[i].fitModelModel;
            rowMarker = raw[i].fitModelMaker;
            rowStyle = raw[i].fitModelStyle;
            if ((rowModel == txtModel) && (rowMarker == txtMaker) && (rowStyle == txtStyle)) {
                // show dialog duplicate.
         		var duplicateValueDialog = $("#duplicateValueDialog").data("kendoWindow");
         		duplicateValueDialog.center().open();
                return;
            }
            // get value for generate key.
            if (raw[i].fitModelSort < sortKey) {
            	sortKey = raw[i].fitModelSort;
            }
        }
        sortKey = sortKey - 1;

        dataSource.add({
        	"productId": $("#productId").val()
        	,"fitModelSort": sortKey
        	,"fitModelMaker": txtMaker
        	,"fitModelMakerNoFitFlg": 0
        	,"fitModelModel": txtModel
        	,"fitModelModelNoFitFlg": 0
        	,"fitModelStyle": txtStyle
        	,"fitModelDelFlg": 0
        });
        // mode insert
    }

    // Process update row selected in kendo grid2
    function processUpdate() {
        // get data from popup and insert into grid.
  	  	  var hdSort = $("#hdSort").val();
    	  var txtMaker = ($("#txtMaker").val() + "").trim();
          var txtModel = ($("#txtModel").val() + "").trim();
          var txtStyle = ($("#txtStyle").val() + "").trim();
          
          // compare old value.
          if ((oldTxtMaker == txtMaker) && (oldTxtModel == txtModel) && (oldTxtStyle == txtStyle)) {
        	  return;
          }

        var grid = $("#gridModelFit2").data("kendoGrid");
        var dataSource = grid.dataSource;

        // Get grid from data kendo.
        var raw = dataSource.data();
        var rowModel = "";
        var rowMarker = "";
        var rowStyle = "";
        var length = raw.length;
        for (var i = length - 1; i >= 0; i--) {
            // get value and check duplicate
            rowModel = raw[i].fitModelModel;
            rowMarker = raw[i].fitModelMaker;
            rowStyle = raw[i].fitModelStyle;
            if ((rowModel == txtModel) && (rowMarker == txtMaker) && (rowStyle == txtStyle)) {
                // show dialog duplicate.
         		var duplicateValueDialog = $("#duplicateValueDialog").data("kendoWindow");
         		duplicateValueDialog.center().open();
                return;
            }
        }
    	
    	 var rowCls = "#gridModelFit2 .kendoRow" + hdSort;
         $(rowCls).find(".kendoRowMaker").text(txtMaker);
         $(rowCls).find(".kendoRowModel").text(txtModel);
         $(rowCls).find(".kendoRowStyle").text(txtStyle);
         
         // set data into old value
         oldTxtMaker = txtModel;
         oldTxtModel = txtMaker;
         oldTxtStyle = txtStyle;

         for (var i = length - 1; i >= 0; i--) {
             if (raw[i].fitModelSort == hdSort) {
                 raw[i].fitModelMaker = txtMaker;
                 raw[i].fitModelModel = txtModel;
                 raw[i].fitModelStyle = txtStyle;
                 break;
             }
         }
    }

    // Process delete selected checkbox in kendo grid2
    function processDelete() {
    	 // Get grid from database.
        var grid = $("#gridModelFit2").data("kendoGrid");
        var dataSource = grid.dataSource;
        var raw = dataSource.data();
        var length = raw.length;
        var fitModelSortArr = new Array();
        var fitModelSort = 0;
        var codeLength = 0;
        // get list attribute has checked for delete.
        $("#gridModelFit2 .kendoRow").each(function () {
            var currObj = $(this);
            var checkCount = currObj.find(".chkFitModelGroup:checked").length;
            if (checkCount != undefined && checkCount > 0) {
            	fitModelSort = currObj.find(".kendoRowSort").val();
                fitModelSortArr.push(fitModelSort);
            }
        });
        codeLength = fitModelSortArr.length;
        // iterate and remove "done" items
        var item, i, j;
        for (i = length - 1; i >= 0; i--) {
            item = raw[i];
            for (j = 0; j < codeLength; j++) {
                if (item.fitModelSort == fitModelSortArr[j]) {
                    dataSource.remove(item);
                }
            }
        }
        if (dataSource.data().length <= 0) {
            //Disable filter
        	disableTxtFilterGrid2();
        	$(".k-grid-header .k-link").each(function () {
        		$(this).find("span").remove();
        	});
            if (0 == jQuery("#gridModelFit2 .dataTables_empty").length) {
                jQuery("#gridModelFit2 .k-grid-content").append('<div class="dataTables_empty">' + jQuery("#noDataMessage").val() + '</div>');
            }
        }
    }
    
    function disableTxtFilterGrid2() {
    	var grid = $("#gridModelFit2").data("kendoGrid");
        var dataSource = grid.dataSource;
        if (dataSource.data().length <= 0) {
        	$("#gridModelFit2 .txtFilter").attr("disabled", "disabled");
        } else {
        	$("#gridModelFit2 .txtFilter").removeAttr("disabled");
        }
    } 
    /* 
     * prepare data before transfer to server.
     * 1. Get data from kendogrid data source
     * 2. Iterate in data source and add into JsonString.
     */
    function prepareDataGrid2() {
        var strJson = "";
        //1. Get data from kendogrid data source
        var grid = $("#gridModelFit2").data("kendoGrid");
        var dataSource = grid.dataSource;
        var raw = dataSource.data();
        var length = raw.length;
        var fitModelSort = 0;
        var productId =  $("#productId").val();
        
        if (undefined == productId || "" == productId.trim()) {
        	return "";
        }

        //2. Iterate in data source and add into JsonString. 
        var item, i;
        for (i = 0; i < length; i++) {
            item = raw[i];
            // set id = 0, id id < -1 (new insert)
            fitModelSort = (item.fitModelSort < 0) ? 0 : item.fitModelSort;
            strJson +=
                "{" +
	                "\"productId\":" + productId 
	                + ",\"fitModelSort\":" + fitModelSort 
	                + ",\"fitModelMaker\":" + '\"' + item.fitModelMaker + '\"'
	                + ",\"fitModelMakerNoFitFlg\":" + item.fitModelMakerNoFitFlg 
	                + ",\"fitModelModel\":" + '\"' + item.fitModelModel  + '\"'
	                + ",\"fitModelModelNoFitFlg\":" + item.fitModelModelNoFitFlg 
	                + ",\"fitModelStyle\":" + '\"' + item.fitModelStyle  + '\"'
	                + ",\"fitModelDelFlg\":" + item.fitModelDelFlg 
	             + "},";
        }
        strJson = "[" + strJson.substring(0, strJson.length - 1) + "]";
        return strJson;
    }
    
    function prepareDataGridMultiProducts() {
        var strJson = "";
        //1. Get data from kendogrid data source
        var grid = $("#displayModelProductTable").data("kendoGrid");
        var dataSource = grid.dataSource;
        var raw = dataSource.data();
        var length = raw.length;

        //2. Iterate in data source and add into JsonString. 
        var item, i;
        for (i = 0; i < length; i++) {
            item = raw[i];
            // set id = 0, id id < -1 (new insert)
            strJson +=
                "{" 
	                + "\"fitModelMaker\":" + '\"' + item.fitModelMaker + '\"'
	                + ",\"fitModelModel\":" + '\"' + item.fitModelModel  + '\"'
	                + ",\"fitModelStyle\":" + '\"' + item.fitModelStyle  + '\"'
	             + "},";
        }
        strJson = "[" + strJson.substring(0, strJson.length - 1) + "]";
        return strJson;
    }
    
    /**
     * Get message with parameters. parameters format are: param1,param2
     * @param invalid input item will be get message.
     * @param msgFormat message format
     * 						ex: {0} is required field.
     * @author hoang.ho
     * @since 2014/01/22
     */
    function getMsgWithParams(input, msgFormat) {
        var params = input.data("parameters");
        if (params != undefined && params != "") {
            var paramArr = params.split(",");
            var length = paramArr.length;
            if (length > 0) {
                for (var i = 0; i < length; i++) {
                    msgFormat = msgFormat.replace("{" + i + "}", paramArr[i]);
                }
            }
        }
        return msgFormat;
    }

    /**
     * customer validate of kendo.
     * rules : define function for validate,
     *      return true/false -
     *          if false kendo will be get [messages] correlative
     * messages: define function for get message.
     *      return String message to show on User Interface.
     * @author hoang.ho
     * @since 2014/01/22
     */
    function validateModelDetails() {
        var kendoValidatorVar = $("#middleRightBlock .modelDetails").kendoValidator({
            rules: {
                required: function (input) {
                    //class-required will be check not blank
                    var inputCls = input.attr("class");
                    if (inputCls != undefined) {
                        if (inputCls.indexOf("k-required") > -1) {
                            return input.val() != "";
                        }
                    }
                    return true;
                },
	            maxLength : function (input) {
	                //class-required will be check not blank
	                var inputCls = input.attr("class");
	                if (inputCls != undefined) {
                	   var length  = getLengthValueFromCls(inputCls);
     	               var lengthInt = parseInt(length);
                    	if (input.val().length > lengthInt) {
                    		return false;
                    	}
	                }
	                return true;
	            },
	            //BOE Nguyen.Chuong 2014/03/26: validate special character.
                nonSpecialChar : function (input) {
                    return validateNonInputSpecialChar(input);
                }
	            //EOE Nguyen.Chuong 2014/03/26
            },
            messages: {
                required: function (input) {
                    return getMsgWithParams(input, requiredFillMsg);
                },
	            maxLength : function (input) {
	                //class-required will be check not blank
	                var inputCls = input.attr("class");
                	var length  = getLengthValueFromCls(inputCls);
                	var msg = maxLengthMsg.replace("{0}", length);
                	return getMsgWithParams(input, msg);
	            },
	            //BOE Nguyen.Chuong 2014/03/26: validate special character.
                nonSpecialChar : invalidInputMsg
	            //EOE Nguyen.Chuong 2014/03/26
            }
        });
        var kendoValidatorVarPopUp = $("#addModelDialog .modelDetails ul li").kendoValidator({
            rules: {
                required: function (input) {
                    //class-required will be check not blank
                    var inputCls = input.attr("class");
                    if (inputCls != undefined) {
                        if (inputCls.indexOf("k-required") > -1) {
                            /* BOE @rcv! Luong.Dai 2014/08/25: add error flag unable to save */
                            /*return input.val() != "";*/
                            if (input.val() == "") {
                                ERROR_FIT_MODEL_FLG = true;
                                return false;
                            }
                            /* BOE @rcv! Luong.Dai 2014/08/25: add error flag unable to save */
                        }
                    }
                    return true;
                },
	            maxLength : function (input) {
	                //class-required will be check not blank
	                var inputCls = input.attr("class");
	                if (inputCls != undefined) {
                	   var length  = getLengthValueFromCls(inputCls);
     	               var lengthInt = parseInt(length);
                    	if (input.val().length > lengthInt) {
                    		return false;
                    	}
	                }
	                return true;
	            },
                //BOE Nguyen.Chuong 2014/03/26: validate special character.
                nonSpecialChar : function (input) {
                    return validateNonInputSpecialChar(input);
                }
                //EOE Nguyen.Chuong 2014/03/26
	            
            },
            messages: {
                required: function (input) {
                    return getMsgWithParams(input, requiredFillMsg);
                },
	            maxLength : function (input) {
	                //class-required will be check not blank
	                var inputCls = input.attr("class");
	            	var length  = getLengthValueFromCls(inputCls);
	            	var msg = maxLengthMsg.replace("{0}", length);
	            	return getMsgWithParams(input, msg);
	            },
	            //BOE Nguyen.Chuong 2014/03/26: validate special character.
	            nonSpecialChar : invalidInputMsg
                //EOE Nguyen.Chuong 2014/03/26
            }
        });
        validatable = kendoValidatorVar.data("kendoValidator");
        validatablePopUp = kendoValidatorVarPopUp.data("kendoValidator");
    }
    initGrid1();
    initGrid2();
    // Start ajax initGrid3() 
	initGrid3();
	        	
    	/*
    	 * create auto complete for field by text field id and field name.
    	 * @param txtId id used for set kendo AutoComplete
    	 * @param field name used for filter in database. 
    	 * @author hoang.ho
    	 * @since 2014.02.13
    	 */
    	function createAutoCompleteModelById(txtId, fieldName) {
    		$(txtId).kendoAutoComplete({
    		    minLength : 1,
    		    dataSource : new kendo.data.DataSource({
    		        serverFiltering: true, 
    		        transport : {
    		            read : {
    		                type: "POST",
    		                dataType: "json",
    		                url: "ajaxLoadAutoCompleModels.html",
    		                data:  function (data) {
    		                    return setDataSourceForAutoComplete(fieldName);
    		                }
    		            }
    		        },
    		        schema: {
    		            data: "fitModelCheckActionForm.autoCompleteList",
    		        }
    		    })
    		});
    	}
    	createAutoCompleteModelById("#txtModelMaker", "modelMaker");
    	createAutoCompleteModelById("#txtDisplacement", "modelDisplacement");
    	createAutoCompleteModelById("#txtmodelName", "modelName");
    // End ajax initGrid3() 
//    initGrid3();
    // check data in datasource of grid to disable or enable text filter.
    disableTxtFilterGrid2();
    // add validate for form
    validateModelDetails();
	
	//Filter multi for grid2
    jQuery("#topRightBlock #gridModelFit2").on('change', '.txtFilter', function (e) {
        var grid = $("#topRightBlock #gridModelFit2").data("kendoGrid");
        if (grid.dataSource.data().length > 0) {
            grid.dataSource.filter({
                logic: "and",
                filters: [
                    {
                        operator: "contains",
                        value: jQuery("#fitModelMakerGrid2").val(),
                        field: "fitModelMaker"
                    },
                    {
                        operator: "contains",
                        value: jQuery("#fitModelModelGrid2").val(),
                        field: "fitModelModel"
                    },
                    {
                        operator: "contains",
                        value: jQuery("#fitModelStyleGrid2").val(),
                        field: "fitModelStyle"
                    }
                ]
             });
        }
    });

    // create event when text change for change candidate
    $("#candidate").on('change',function (e) {
    	// fiter grid 3 when change candidate
    	filterGrid3Common();
    });
    // Action onclick on item 6-8 on grid 2
    // Get data from select row and put into model detail tab .
    $('#gridModelFit2').on('click', '.kendoCell', function (e) {
        e.preventDefault();
        loadVehicleDetail(this);
    });
    
    // double click to row in grid and set maker and modelname model detail tab
    $("#gridModelFit3").on("dblclick", "tr.k-state-selected", function (e) {
        e.preventDefault();
        // reset value
        /*resetModelDetail();*/
        // get value
        var maker = $(this).find("td:first").text();
        var modelName = $(this).find("td:last").text();
        // set value
        $("#txtMaker").val(maker);
        $("#txtModel").val(modelName);
       
    });
    
    // Action click button insert mode.
    $("#btnInsertMode").click (function (e) {
        e.preventDefault();
        insertMode = true;
        // reset value.
        //BOE Nguyen.Chuong 2014/03/10: not clear info text box of maker, model, display when change to new mode.
//        resetModelDetail();
        jQuery("#txtAllModelFitInfo").text($("#hAddNew").val());
        // clean text
//      $("#txtModelMaker").val("");
//      $("#txtDisplacement").val("");
//      $("#txtmodelName").val("");
        //EOE Nguyen.Chuong 2014/03/10
        // clean validation message
        $("#middleRightBlock .modelDetails").find("span.k-tooltip-validation").hide();
        var e = $.Event('keyup');
        e.keyCode = 13; // Enter key
        $('#txtModelMaker').trigger(e);
    });
    $("#btnUpdate2").click (function (e) {
        // If validate failed. prevent submit.
    	var isOnlyModelInvalid = (validatable.errors().length == 1) && (validatable._errors["txtModel"] != null);
		if (!validatable.validate() && !isOnlyModelInvalid) {
			return false;
		}

        if (insertMode) {
            //Process to insert row
        	processInsert();
        	//Check filter is disable or not
        	if (0 != $("#gridModelFit2 .txtFilter:disabled").length) {
                //Enable filer for grid
                $("#gridModelFit2 .txtFilter").removeAttr("disabled");
            }
        } else {
        	processUpdate();
        }
        var jSonStr = prepareDataGrid2();
        $("#hListFitModelJsonStr").val(jSonStr);
    });
    
    // Action click button delete.
    // 1. check checkbox has checked or not. 
    //	 If not check show message. Else process. 
    // 2. Process update delete selected rows.
    $("#btnDelete").click(function (e) {
         e.preventDefault();
         clearMsgText();
         //1. Check checkbox has checked or not
         var chkFitModelGroupElems = $("#gridModelFit2").find(".chkFitModelGroup:checked");
         if (undefined == chkFitModelGroupElems || chkFitModelGroupElems.length <= 0) {
        	 // show message not choose check box
     		// Show PleaseSelect dialog.
     		var pleaseSelectDialog = $("#pleaseSelectDialog").data("kendoWindow");
     		pleaseSelectDialog.center().open();
     		return false;
         }
         // show popup confirm    	
         var win = $("#confirmDeleteDialog").data("kendoWindow");
         win.center();
         win.open();
    });
    
    function processButtonSave(selectedRows, dataFromGrid4) {
    	var jSonStr = prepareDataGrid2();	
    	$("#rqListModelJsonStr").val(jSonStr);
    	var productId = $("#productId").val();
    	var productErrorFlg = $("#productErrorFlg").val();
    	var matterNo = $("#matterNo").val();
    	$.ajax({
    		type: "POST",
    		url: "processDataFitModelBySingleProduct.html",
    		dataType: "json",
    		data: {
    			"fitModelCheckActionForm.listModelJsonStr": jSonStr,
    			"fitModelCheckActionForm.product.productId": productId,
    			"fitModelCheckActionForm.product.productModelErrorFlg" :productErrorFlg,
    			"fitModelCheckActionForm.factoryMatter.matterNo": matterNo,
    		},
    		beforeSend: function () {
    			// remove all class of msg div and clear msg
    			clearMsgText();
    			if (!isFromChangeRow) {
    				showProcessBar();
    			}
    		},
    		success: function (data) {
    			// reload grid 1 & 2
    			createListFitModel(data);
    			// reload grid 4
    			if (isFromChangeRow) {
    				checkProductChange(grid4CurrentProductId, grid4RowSelectedIndex);
    			} else {
    				var selectedIndex = getGrid4SelectedIndex();
    				reloadGrid4Page(selectedIndex);
    			}
    			if (data.fitModelCheckActionForm.msgCode == 1) {
    				$("#message").addClass("has-message success");
    				$("#message").text(msgSuccess);
    			} else {
    				$("#message").addClass("has-message msg-error");
    				$("#message").text(msgFail);
    			}
    			if (!isFromChangeRow) {
    				hideProcessBar();
    			}

    			if (modePopup == true) {
    				openAddModelDialog();
    				modePopup = false;
    			}
    			// check if load from grid 4
    			if (isLoadFromGrid4) {
    				if (dataFromGrid4 != null) {
    					bindProductAllData(selectedRows, dataFromGrid4);
    				}
    				// clear text of the process update in grid 2 when click row in grid 4
    				clearMsgText();
    				isLoadFromGrid4 = false;
    			}
    		},
    		error: function () {
    			$("#message").addClass("has-message msg-error");
				$("#message").text(msgFail);
    			hideProcessBar();
    		},
    	});
    }
    
    /* BOE event add model for multi products  */
    // event add
	$("#btnAdaptationModel").click(function(e) {
		/* BOE dang.nam 2014/06/06 select all*/
		getSearchCondition();
		var checkAll = ($("#selectAllCheckbox").attr('checked') == 'checked');
		/* EOE dang.nam 2014/06/06 select all*/ 
		
		// close diglog
		$("#displayModelProduct").data("kendoWindow").close();
		
	  	var jSonStr = prepareDataGridMultiProducts();
	  	matterNo = $("#matterNo").val();
	  	var currentProductId = $("#productId").val();
	  	$("#rqListModelJsonStr").val(jSonStr);
	  	$.ajax({
	  		type: "POST",
	  		url: "processDataFitModelByMultiProduct.html",
	  		dataType: "json",
	  		data: {
	  			"checkAll" : checkAll,
				"fitModelCheckActionForm.product.productId": currentProductId,
	  			"fitModelCheckActionForm.listModelJsonStr": jSonStr,
	  			"arrayProductId" : arrayProductId,
	  			"arrayProductModelErrorFlg" : arrayProductModelErrorFlg,
	  			"fitModelCheckActionForm.factoryMatter.matterNo": matterNo,
	  			/*BOE dang.nam 2014/06/06 select all*/
				"fitModelCheckActionForm.entTblFactoryProductNew.matterNo" :      jQuery("#matterNo").val(),
		        "fitModelCheckActionForm.entTblFactoryProductNew.productId" :      productId,
		        "fitModelCheckActionForm.entTblFactoryProductNew.productSyouhinSysCode" :           productSyouhinSysCode,
		        "fitModelCheckActionForm.entTblFactoryProductNew.brandName" :    brandName,
		        "fitModelCheckActionForm.entTblFactoryProductNew.productName" :    productName,
		        "fitModelCheckActionForm.entTblFactoryProductNew.productCode" :    productCode,
		        "fitModelCheckActionForm.entTblFactoryProductNew.productModelCheckFlg" : productModelCheckFlg,
		        "fitModelCheckActionForm.entTblFactoryProductNew.fitModelMaker" : productFitModelMaker,
		        "fitModelCheckActionForm.entTblFactoryProductNew.fitModelModel" : productFitModelModel,
		        "fitModelCheckActionForm.entTblFactoryProductNew.masterMisMatchFlg" : productFitModelMisMatchFlg,
		        "fitModelCheckActionForm.entTblFactoryProductNew.productModelErrorFlg" :         productModelErrorFlg,
		        "fitModelCheckActionForm.entTblFactoryProductNew.descriptionSummary" : descriptionSummary,
		        "fitModelCheckActionForm.entTblFactoryProductNew.productSelectDisplay" :   productSelectDisplay,
		        "fitModelCheckActionForm.entTblFactoryProductNew.productProperPriceFrom"    :   productProperPriceFrom,
		        "fitModelCheckActionForm.entTblFactoryProductNew.productProperPriceTo"      :   productProperPriceTo,
		        "fitModelCheckActionForm.entTblFactoryProductNew.categoryName"      :   categoryName,
		        "fitModelCheckActionForm.entTblFactoryProductNew.sortField"      :   "productId",
		        "fitModelCheckActionForm.entTblFactoryProductNew.sortDir"      :   ""
	        	/*EOE dang.nam 2014/06/06 select all*/
	  		},
	  		beforeSend: function () {
	  			// remove all class of msg div and clear msg
  				clearMsgText();
  		    	showProcessBar();
	  		},
	  		success: function (data) {
	  			createListFitModel(data);
	  			showMsgText(data.fitModelCheckActionForm.msgCode);
	  			clearProductGrid();
	  			hideProcessBar();
	  		},
	  		error: function () {
	  			hideProcessBar();
	  		},
	  	});
	  });
	 // event cancel
	 $("#btnCancelModel").click(function(e) {
			// close diglog
			$("#displayModelProduct").data("kendoWindow").close();
	 });
    /* EOE event add model for multi products  */
    
    $("#btnNo").click(function (e) {
        e.preventDefault();
        var win = $("#confirmDeleteDialog").data("kendoWindow");
        $("#msgConfirmResult").val("0");
        win.close();
    });
    $("#btnYes").click(function (e) {
        e.preventDefault();
        var win = $("#confirmDeleteDialog").data("kendoWindow");
        $("#msgConfirmResult").val("1");
        win.close();
        var jSonStr = prepareDataGrid2();
        $("#hListFitModelJsonStr").val(jSonStr);
    });
    
    // prevent sort when datasource has not value.
    $("#gridModelFit2 .k-grid-header .k-link").click(function (e) {
        var grid = $("#gridModelFit2").data("kendoGrid");
        var dataSource = grid.dataSource;
        if (dataSource.data().length <= 0) {
            $(this).find("span").remove();
            e.preventDefault();
            e.stopPropagation();
        }
    });

    // ***Begin create dialog for screen.
    // Start comfirm delete dialog.
    $("#confirmDeleteDialog").kendoWindow({
        maxWidth: 400,
        minWidth: 250,
        minHeight: 100,
        resizable: false,
        modal: true,
        visible: false,
        title : $("#hdTitle").val(),
        close: function () {
            var cfm = $("#msgConfirmResult").val();
            if (cfm == "1") {
                processDelete();
            }
        }
    });
    // End create dialog for screen.***	
    //----------------------------------------------------------process bottom grid----------------------
    initGrid4();
    
    //Set default DelFlg = Yes
    setSelectedValue("Yes");
    $('#listProductModelCheckFlg').kendoDropDownList();
    $('#listProductModelErrorFlg').kendoDropDownList();
    $('#listProductFitModelMisMatchFlg').kendoDropDownList();
    jQuery(document).on("keypress", "input.tbSearch", function(e) {
    	if (e.keyCode == 13) {
    		//jQuery("#btnSearch").click();
    		clickButtonSearch();
        }
    });
    //////////////////////////////new event
    //Click button reset
    jQuery("#btnReset").click(function() {
        //Set sort reset
        sortResetFlg = 1;
        resetSearchCondition();
        //jQuery("#btnSearch").click();
        clickButtonSearch();
    });
    // Button Search click
	jQuery("#btnSearch").click(function() {
		clickButtonSearch();
	});
    //////////////////////////////new event
    //Change DelFlg value
    jQuery("#listProductModelCheckFlg").change(function() {
    	clickButtonSearch();
    });
    jQuery("#listProductModelErrorFlg").change(function() {
    	clickButtonSearch();
    });
    jQuery("#listProductFitModelMisMatchFlg").change(function() {
        clickButtonSearch();
    });
    /* BOE by Luong.Dai show progress bar when click link at 2014/04/01 */
    /*jQuery('#linkToMatterDetail').click(function(e) {
    			//Show progress bar
    			showProcessBar();
    		});*/
    /* EOE by Luong.Dai show progress bar when click link */

});
//END DOCUMENT.READY

////////////////BOE_Nguyen.Chuong//////////////////
//Init grid 1.
// 1. get model-list json string and parse to list
// 2. add list into kendo grid
function initGrid1() {
    // Define data get from sever and parse to json
    var mistListFitModelJsonStr = $("#hListFitModelJsonStr").val();
    var mistFitModelJsonList =  $.parseJSON(mistListFitModelJsonStr); 
    var filterRow = $(
            '<tr>' 
            +  '<th class="k-header" data-field="fitModelMaker">'
            +      '<input type="text" id="fitModelMakerGrid1" maxLength="100" class="txtFilter k-textbox" style="width: 100%;"/>'
            +  '</th>' 
            +  '<th class="k-header" data-field="fitModelModel">'
            +      '<input type="text" id="fitModelModelGrid1" maxLength="1020" class="txtFilter k-textbox" style="width: 100%;"/>'
            +  '</th>' 
            +  '<th class="k-header" data-field="fitModelStyle">'
            +      '<input type="text" id="fitModelStyleGrid1" maxLength="512" class="txtFilter k-textbox" style="width: 100%;"/>'
            +  '</th>' 
            +  '</tr>');
    // create kendo grid.
    var grid = $("#gridModelFit1").kendoGrid({
        dataSource: {
            data: mistFitModelJsonList,
            schema: {
                model: {
                    fields: {
                          fitModelMaker: {type: "string"}
                        , fitModelModel: {type: "string"}
                        , fitModelStyle: {type: "string"}
                    }
                }
            }
        },
        height: 200,
        maxWidth: 550,
        scrollable: true,
        selectable: true,
        resizable : true,
        sortable: {
            mode: "single",
            allowUnsort: false
        },
        pageable: false,
        columns: [{
                    field: "fitModelMaker",
                    title: $("#titleGridMaker").val(),
                    width: "25%",
                    attributes: {"class": "align-center"}
                  },
                  {
                    field: "fitModelModel",
                    title: $("#titleGridModel").val(),
                    width: "50%",
                    attributes: {"class": "align-left"}
                  },
                  {
                    field: "fitModelStyle",
                    title: $("#titleGridStyle").val(),
                    width: "25%",
                    attributes: {"class": "align-left"}
                  }],
        dataBound: function(e) {
            //Remove noData message
            jQuery("#gridModelFit1 .dataTables_empty").remove();
            if (this.dataSource.total() > 0) {
                //Add title for row
                var gridData = this.dataSource.view();
                var gridLength = gridData.length;
                var i = 0;
                // loop through the grid data
                for (i; i < gridLength; i++) {
                	// get curren id
                    var currentUid = gridData[i].uid;
                    // get current row
                    var currentListCell = this.table.find("tr[data-uid='" + currentUid + "'] td");
                    // check model maker flg is error of not
                    // 1: error, 0: no error
                    if(gridData[i].fitModelMakerNoFitFlg == 1) {
                      // check if model maker is over length of warning
                      // 50 is the length of waring in DB
                      if (gridData[i].fitModelMaker.length >= 50) {
                        $(currentListCell[0]).addClass("red");
                      } else {
                        $(currentListCell[0]).addClass("yellow");
                      }
                    }
                    if(gridData[i].fitModelModelNoFitFlg == 1) {
                      if (gridData[i].fitModelModel.length >= 1001) {
                        $(currentListCell[1]).addClass("red");
                      } else {
                        $(currentListCell[1]).addClass("yellow");
                      }
                      }
                    if(gridData[i].fitModelStyle.length >= 256) {
                        $(currentListCell[2]).addClass("red");
                    } 
                }
            } else {
                //Show no data message
                jQuery("#gridModelFit1 .k-grid-content").append('<div class="dataTables_empty">' + jQuery("#noDataMessage").val() + '</div>');
            }
        }
    });
    
    // add filter in to grid.
    grid.data("kendoGrid").thead.append(filterRow);
}

//init grid model fit 2
// 1. get fit-model-check json string and parse to list
// 2. add list into kendo grid
function initGrid2() {
    // Define data get from sever and parse to json
    var modelFitListJsonStr = $("#hListFitModelJsonStr").val();
    var modelFitListJson =  $.parseJSON(modelFitListJsonStr);   
    var filterRow = $(
            '<tr>' 
            + '<th class="k-header" style="width: 60px"></th>' 
            + '<th class="k-header" data-field="fitModelMaker"><input type="text" id="fitModelMakerGrid2" maxLength="100" class="txtFilter k-textbox" style="width: 172px"/></th>' 
            + '<th class="k-header" data-field="fitModelModel"><input type="text" id="fitModelModelGrid2" maxLength="1020" class="txtFilter k-textbox" style="width: 220px"/></th>' 
            + '<th class="k-header" data-field="fitModelStyle"><input type="text" id="fitModelStyleGrid2" maxLength="512" class="txtFilter k-textbox" style="width: 103px"/></th>' 
            + '</tr>');
    // create kendo grid.
    var grid = $("#gridModelFit2").kendoGrid({
        dataSource: {
            data: modelFitListJson,
            schema: {
                model: {
                    fields: {
                        fitModelMaker: {type: "string"}
                        , fitModelModel: {type: "string"}
                        , fitModelStyle: {type: "string"}
                    }
                }
            }
        },
        height: 200,
        maxWidth: 550,
        scrollable: true,
        selectable: true,
        resizable : true,
        sortable: {
            mode: "single",
            allowUnsort: false
        },
        pageable: false,
        columns: [{
            field: "fitModelDelFlg",
            title: " ",
            width: "30px",
            sortable: false,
            template: "<input type='checkbox' value=' #= (fitModelDelFlg == 1)' class='chkFitModelGroup' data-bind='checked: fitModelDelFlg' #= (fitModelDelFlg == 1) ? checked='checked' : '' #/>"
        }, {
            field: "fitModelMaker",
            title: $("#hFitModelMaker").val(),
            width: "160px",
            attributes: {"class": "align-center"}
        }, {
            field: "fitModelModel",
            title: $("#hFitModelModel").val(),
            width: "200px",
            attributes: {"class": "align-left"}
        }, {
            field: "fitModelStyle",
            title: $("#hFitModelStyle").val(),
            width: "100px",
            attributes: {"class": "align-left"}
        } ],
        rowTemplate: kendo.template($("#rowTemplate").html()),
        altRowTemplate: kendo.template($("#altRowTemplate").html()),
        dataBound: function(e) {
            jQuery("#gridModelFit2 .dataTables_empty").remove();
            if (this.dataSource.total() == 0) {
                //Show no data message
                jQuery("#gridModelFit2 .k-grid-content").append('<div class="dataTables_empty">' + jQuery("#noDataMessage").val() + '</div>');
            }
            // set selected row at the 1st row
            currentIdxGrid2 = 0;
            this.element.find('tbody tr:first').addClass('k-state-selected');
            var selectedRows = this.select();
  		    var test = selectedRows.get(0);
  		    var col1 = $(test).find("td:nth-child(2)");
  		    loadVehicleDetail(col1);

        },
        change: function(e) {
			var selectedRows = this.select();
			// Selected new row
	        if (currentIdxGrid2 != selectedRows.index()) {
	        	currentIdxGrid2 = selectedRows.index();
	        }
        }
    });
    
    // add filter in to grid.
    grid.data("kendoGrid").thead.append(filterRow);
}

// Init grid 3.
// 1. get model-list json string and parse to list
// 2. add list into kendo grid
function initGrid3() {
	//if switch page, isNewPage = true
	var currentPageGrid3 = 1;
	var dataSourceGrid3;

	//Load data for grid
	function loadGrid3Data() {
	    var grid3 = $("#gridModelFit3").kendoGrid({
	        dataSource: dataSourceGrid3,
	        //sortable: true,
	        sortable: {
	            mode: "single",
	            allowUnsort: false
	        },
	        pageable: {
	            refresh: true,
	            pageSizes: [50, 100],
	            buttonCount: 5,
	            messages: {
	                display: jQuery("#pagingDisplay").val(),
	                empty: jQuery("#pagingEmpty").val(),
	                itemsPerPage: jQuery("#pagingItemsPerPage").val(),
	                first: jQuery("#pagingFirst").val(),
	                previous: jQuery("#pagingPrevious").val(),
	                next: jQuery("#pagingNext").val(),
	                last: jQuery("#pagingLast").val()
	            }
	        },
	        columns: [{
		                 field: "modelMaker",
		                 title: $("#hModelMaker").val(),
		                 width: "90px",
		                 attributes: {"class": "align-center"}
		             }, {
		                 field: "modelDisplacement",
		                 title: $("#hModelDisplacement").val(),
		                 attributes : {
		                     "class" : "align-right"
		                 },
		                 width: "60px"
		             }, {
		                 field: "modelName",
		                 title: $("#hModelName").val(),
		                 width: "200px",
		                 attributes: {"class": "align-left"}
	        		}],
	        height: 100,
	        maxWidth: 550,
	        scrollable: true,
	        selectable: true,
	        resizable : true,
	        sortable: { mode: "single", allowUnsort: false },
	        dataBound: function (e) {
	            //Remove noData message
	            jQuery("#gridModelFit3 .dataTables_empty").remove();
	            if (this.dataSource.total() > 0) {
	                var page = this.dataSource.page();
	                if (page != currentPageGrid3) {
	                	currentPageGrid3 = page;
	                    this.content.scrollTop(0);
	                }
	            } else {
	                //Show no data message
	                jQuery("#gridModelFit3 .k-grid-content").append('<div class="dataTables_empty">' + jQuery("#noDataMessage").val() + '</div>');
	            }
	        }
	    });

	    return grid3;
	}

		
	//Set data filter condition to sent to server
	function setParamsForDataSourceGrid3(sortField, sortDir) {
	    return {
	        "fitModelCheckActionForm.entMstModel.modelMaker" :      $("#txtModelMaker").val(),
	        "fitModelCheckActionForm.entMstModel.modelDisplacement" :      $("#txtDisplacement").val(),
	        "fitModelCheckActionForm.entMstModel.modelName" :      $("#txtmodelName").val(),
	        "fitModelCheckActionForm.entMstModel.sortField" : sortField,
	        "fitModelCheckActionForm.entMstModel.sortDir": sortDir
	    };
	}
	

		//Filter bar
	    var filterRowGrid3 = $(
	            '<tr>' 
	            + '<th class="k-header" data-field="modelMaker"><input type="text" maxLength="100" class="txtFilter k-textbox" id="txtModelMaker"/></th>' 
	            + '<th class="k-header" data-field="modelDisplacement"><input type="text" maxLength="11" class="txtFilter k-textbox" id="txtDisplacement"/></th>' 
	            + '<th class="k-header" data-field="modelName"><input type="text" class="txtFilter k-textbox" maxLength="50" id="txtmodelName" /></th>' 
	            + '</tr>');
	    //dataSource for brand master grid
	    dataSourceGrid3 = new kendo.data.DataSource({
	        serverPaging: true,
	        serverSorting: true,
	        autoBind: false,
	        // 50 product in 1 page
	        pageSize: 50,
	        transport: {
	            read: {
	                type: "POST",
	                dataType: "json",
	                url: "ajaxLoadModelList.html",
	                data: function (data) {
	                	var sortField = "modelCode + 0";
	                	var sortDir = "asc";
	                	if (data != undefined && data.sort != undefined && data.sort.length > 0) {
	                		sortField = data.sort[0].field;
	                		sortDir = data.sort[0].dir;
	                	}
	                    return setParamsForDataSourceGrid3(sortField, sortDir);
	                },
	                cache: false,
	            }
	        },
	        schema: {
	            data: "fitModelCheckActionForm.listMstModel",
	            total: "fitModelCheckActionForm.mstModelListCount"
	        }
	    });

	    var grid = loadGrid3Data();

	    //Add filter
	    grid.data("kendoGrid").thead.append(filterRowGrid3);
	  
	    //Button Search click
	    function grid3ReloadData() {
	    	//Reload data and goto first page
	    	grid.data("kendoGrid").dataSource.page(1);
	    	
	    	//Go to top of grid
	    	grid.data("kendoGrid").content.scrollTop(0);
	    	
	    }
//	    // create event when text change for filter grid 3. maybe make common method
	    $("#gridModelFit3").on('keyup', 'span.txtFilter', function (e) {
	    	 if (e.keyCode == 13) {
                 jQuery("#gridModelFit3 .dataTables_empty").remove();
	    		 grid3ReloadData();
	         }
	    });
}
////////////////EOE_Nguyen.Chuong//////////////////

////////////////BOE_Nguyen.Hieu////////////////////
function handleClickingRows() {
	$("#gridModelFit4 .k-grid-content tr").on({
		click: function(e) {
			clearMsgText();
			var row = this;
			var checkedAll = $("#selectAllCheckbox").prop("checked");
			if($(e.target).is("input[type=checkbox]")) {
				// CheckBox processing.
				// ...
				var checkBox = e.target;
				if($(checkBox).prop("checked")) {
					var grid4 = $("#gridModelFit4").data("kendoGrid");
					grid4.select(row);
				} else {
					$(row).removeClass("k-state-selected").removeAttr("aria-selected");
				}
			} else {
				// Get selected row data items.
				var grid4 = $("#gridModelFit4").data("kendoGrid");
		        var selectedRows = grid4.select();
	            var data = grid4.dataItem(selectedRows);
	            
	            if (!validateDataGrid()) {
	            	// Get data of selected row
	            	if (data != null) {
	            		bindProductAllData(selectedRows, data);
	            	}
	            } else {
	            	var productResgistration = $("#prdResgistration").val();
					if (productResgistration == 0) {
						isLoadFromGrid4 = true;
		                selectedRodGrid4 = selectedRows;
		                dataSourceGrid4 = data;
		                var initConfirmDialog = $("#confirmChangeRowDialog").data("kendoWindow");
		                initConfirmDialog.center().open();
					} else {
						if (data != null) {
		            		bindProductAllData(selectedRows, data);
		            	}
					}
	            }
	            if (checkedAll) {
					$("#selectAllCheckbox").prop("checked", false);
					deselectRow("#gridModelFit4 table tbody tr", ".productCheckbox", selectedKendoClass, selectedRowClass);
					$("#gridModelFit4 .k-grid-content .productCheckbox:checked").prop("checked", true);
				} else {
					$("#gridModelFit4 .k-grid-content .productCheckbox:checked").prop("checked", false);
				}
	            // Reselect new rows and check new checkBoxes.
	            grid4.select(selectedRows);
	            selectedRows.find(".productCheckbox").prop("checked", true);
			}
		}
	});
}
////////////////EOE_Nguyen.Hieu////////////////////

////////////////////////////////////////////////////////////////////////////////////bottom grid area///////////////////////////////////
//Reset all filter
function resetSearchCondition() {
	//clear all textbox
	jQuery("input.tbSearch").val('');
	
	// reset kendo dropdownlist
	resetKendoDropdownList("selectExcute");
	resetKendoDropdownList("listProductModelCheckFlg");
	resetKendoDropdownList("listProductModelErrorFlg");
	resetKendoDropdownList("listProductFitModelMisMatchFlg");
	//reset status list
	setSelectedValue("Yes");
}
function resetKendoDropdownList(id) {
	var dropdownList = $("#" + id).data("kendoDropDownList");
	dropdownList.select(0);
}
function clickButtonSearch(){
	$('#selectAllCheckbox').attr('checked',false);
	//-----------------------------------------------------------------jQuery("#btnSearch").click();----------------------------
	var validateResult = validateSearchFilter();
    if (validateResult){
        getSearchCondition();
        //Reload data and goto first page
        bottomGrid.data("kendoGrid").dataSource.page(1);
        //Go to top of grid
        bottomGrid.data("kendoGrid").content.scrollTop(0);
    }
	//------------------------------------------------------------------jQuery("#btnSearch").click();----------------------------
}
//Validate search filter
function validateSearchFilter() {
	var productIdResult = checkProductId();
	var productSyouhinSysCodeResult = validateNumbericAndBlank($("#tbProductSyouhinSysCode").val());
	var productProperPriceFromResult = validateNumbericAndBlank($("#tbProductProperPriceFrom").val());
	var productProperPriceToResult = validateNumbericAndBlank($("#tbProductProperPriceTo").val());
	if (!productIdResult || !productSyouhinSysCodeResult || !productProperPriceFromResult || !productProperPriceToResult) {
		return false;
	}
	return true;
}
function checkProductId() {
  // get product id val
  var productId = $("#tbProductId").val();
  // check if product id val is numberic
  // if so return true
  if (validateNumbericAndBlank(productId)) {
    // set nomal background
    $("#tbProductId").css("background-color", "#FFFFFF");
    return true;
  } else {
    // if product id is not numberic
    // then get first char of product id
    var p = productId.substring(0,1);
    // if the first char is p then
    // then check the rest of product id is numberic or not
    // if so return true
    // then return false
    // if the first char is not p then return false
    if (p.toLowerCase() == 'p') {
      var productIdNum = productId.substring(1, productId.length);
      if (validateNumbericAndBlank(productIdNum)) {
        // set nomal background
        $("#tbProductId").css("background-color", "#FFFFFF");
        return true;
      } else {
        // set error background
        $("#tbProductId").css("background-color", "#FFD9E6");
        return false;
      }
    } else {
      // set error background
      $("#tbProductId").css("background-color", "#FFD9E6");
      return false;
    }
  }
};
function initGrid4() {
	$("#selectExcute").kendoDropDownList();
	var filterRow = $(
			'<tr>'
				+ '<td></td>'
				/* BOE by Luong.Dai change maxLength of productId to P + 18 number, syouhinSySCode 18 number*/
				/*+ '<td><input class="tbSearch k-textbox" type="text" class="filter" maxLength="21" id="tbProductId" tabindex="1"/></td>'
				+ '<td><input class="tbSearch k-textbox" type="text" class="filter" maxLength="20" id="tbProductSyouhinSysCode" tabindex="2"/></td>'*/
				+ '<td><input class="tbSearch k-textbox" type="text" class="filter" maxLength="19" id="tbProductId" tabindex="1"/></td>'
				+ '<td><input class="tbSearch k-textbox" type="text" class="filter" maxLength="18" id="tbProductSyouhinSysCode" tabindex="2"/></td>'
				/* BOE by Luong.Dai change maxLength of productId to P + 19 number, syouhinSySCode 18 number*/
				+ '<td><input class="tbSearch k-textbox" type="text" class="filter" maxLength="50" id="tbBrandName" tabindex="3"/></td>'
				+ '<td><input class="tbSearch k-textbox" type="text" class="filter" maxLength="512" id="tbProductName" tabindex="4"/></td>'
				+ '<td><input class="tbSearch k-textbox" type="text" class="filter" maxLength="100" id="tbProductCode" tabindex="5"/></td>'
				+ '<td><select class="tbSearch cbbFilter" id="listProductModelCheckFlg" tabindex="6">'
				+ '<option value="-1"></option>'
				+ '<option value="0">' + $('#textProductModelCheckFlgEqual0').val() + '</option>'
				+ '<option value="1">' + $('#textProductModelCheckFlgEqual1').val() + '</option>'
				+ '</select></td>'
				+ '<td><input class="tbSearch" type="text" class="filter" maxLength="100" id="tbProductFitModelMaker" tabindex="7"/></td>'
				+ '<td><input class="tbSearch" type="text" class="filter" maxLength="1020" id="tbProductFitModelModel" tabindex="8"/></td>'
                + '<td><select class="tbSearch cbbFilter" id="listProductFitModelMisMatchFlg" tabindex="9">'
                + '<option value="-1"></option>'
                + '<option value="0">' + $('#textNoMisMatch').val() + '</option>'
                + '<option value="1">' + $('#textMisMatch').val() + '</option>'
                + '</select></td>'
				+ '<td><select class="tbSearch cbbFilter" id="listProductModelErrorFlg" tabindex="10">'
				/** BOE Remove option all @rcv!cao.lan 2014/03/14 Bug No.4-20140313_not_fix **/
				//+ '<option value="-1">���ׂ�</option>'
				/** EOE @rcv!cao.lan 2014/03/14 Bug No.4-20140313_not_fix **/
				+ '<option value="0"></option>'
				+ '<option value="1">'+$('#textProductModelErrorFlgEqual1').val()+'</option>'
				+ '</select></td>'
				+ '<td><input class="tbSearch k-textbox" type="text" class="filter" maxLength="35010" id="tbDescriptionSummary" tabindex="11"/></td>'
				+ '<td><input class="tbSearch k-textbox" type="text" class="filter" id="tbProductSelectDisplay" tabindex="12"/></td>'
				+ '<td tabindex="13">'
				+ '<label >From</label> <input class="tbSearch k-textbox" type="text" maxLength="8" id="tbProductProperPriceFrom" /><br/>'
				+ '<label >To</label> <input class="tbSearch k-textbox" type="text" maxLength="8" id="tbProductProperPriceTo" />'
				+ '</td>'
				+ '<td><input class="tbSearch k-textbox" type="text" class="filter" maxLength="255" id="tbCategoryName" tabindex="14"/></td>'
			+ '</tr>');
	dataSource = new kendo.data.DataSource({
        serverPaging: true,
        autoBind: false,
        serverSorting: true,
        // 50 product in 1 page
        pageSize: 50,
        transport: {
            read: {
                type: "POST",
                dataType: "json",
                url: "bottomGridFilter.html",
                data:  function (data) {
                    var sortField = "productId";
                    var sortDir = "asc";
                    if (sortResetFlg != 1) {
                        if (data != undefined && data.sort != undefined && data.sort.length > 0) {
                            sortField = data.sort[0].field;
                            sortDir = data.sort[0].dir;
                        }
                    } else {
                        if (data != undefined && data.sort != undefined && data.sort.length > 0) {
                            data.sort[0].field = '';
                            data.sort[0].dir = '';
                        }
                    }
                    return setDataForDataSource(sortField, sortDir);
                },
                beforeSend :function() {
                	clearMsgText();
                	//Hide processing of Kendo.
//                    jQuery("#gridModelFit4 .k-loading-image").css("background-image", "none");
//                    // show process bar before send request.
//                    showProcessBar();
                },
                complete: function() {
                    // hide process bar that the request succeeded
//                	hideProcessBar();
                    sortResetFlg = 0;
                    var grid = $("#gridModelFit4").data("kendoGrid");
                	if (grid4RowSelected !== "") {
        				var row = grid.tbody.find(">tr")[grid4RowSelected];
        				grid.select(row);
        				grid4RowSelected = "";
        				var selectedRows = grid.select();
        				handleCheckbox(selectedRows);
        			}
                },
                error: function() {
                    // hide process bar that the request failed
//                    hideProcessBar();
                    sortResetFlg = 0;
                },
                cache: false,
            }
        },
        schema: {
            data: "fitModelCheckActionForm.listProduct",
            total: "fitModelCheckActionForm.count"
        }
    });
	//end datasource.
	    bottomGrid = $("#gridModelFit4").kendoGrid({
		dataSource: dataSource,
        sortable: {
            mode: "single",
            allowUnsort: false
        },
	    pageable: {
	        refresh: true,
	        pageSizes: [50, 100],
	        buttonCount: 5,
	        messages: {
	          display:         jQuery("#pagingDisplay").val(),
	          empty:           jQuery("#pagingEmpty").val(),
	          itemsPerPage:    jQuery("#pagingItemsPerPage").val(),
	          first:           jQuery("#pagingFirst").val(),
	          previous:        jQuery("#pagingPrevious").val(),
	          next:            jQuery("#pagingNext").val(),
	          last:            jQuery("#pagingLast").val()
	        }
	    },
	    columns: [
				{
					title : "<input type='checkbox' id='selectAllCheckbox'/>",
					template : "<input type='checkbox' name='matterCheckbox' class='productCheckbox' id='#= productId #'  value='#= productModelErrorFlg #' productRegistration='#= productRegistrationFlg #'/>",
					attributes : {
						"class" : "align-center"
					},
					width : "30px"
				},
	            {
	  	          field:"productId",
	  	          template: function(myData){
	  	        	  return 'P'+myData.productId;
	  	          },
	  	          type: "number",
	  	          title: jQuery("#headerProductId").val(),
	  	          width: "95px", 
	  	          attributes: {"class": "align-right"} },
	  	        { 
	  	          field:"productSyouhinSysCode",
	  	          template: function(myData){
	  	        	  if(myData.productSyouhinSysCode == 0){
	  	        		  return $('#textSyouhinEqual0').val();//'�V�K���i';
	  	        	  }
	  	        	  return myData.productSyouhinSysCode;
	  	          },
	  	          type: "number",
	  	          title: jQuery("#headerProductSyouhinSysCode").val(),
	  	          width: "95px",
	  	          attributes: {"class": "align-right"}
	  	        },
	  	        { 
	  	          field:"brandName",
	  	          type: "string",
	  	          title: jQuery("#headerBrandName").val(),
	  	          width: "180px",
	  	          attributes: {"class": "align-left, row_table"}
	  	        },
	  	        { 
	  	          field:"productName",
	  	          type: "string",
	  	          title: jQuery("#headerProductName").val(),
	  	          width: "240px",
	  	          attributes: {"class": "align-left, row_table"}
	  	        },
	  	        {
	  	          field:"productCode",
	  	          type: "string",
	  	          title: jQuery("#headerProductCode").val(),
	  	          width: "180px",
	  	          attributes: {"class": "align-left, row_table"}
	  	        },
	  	        { 
	  	          field:"productModelCheckFlg",
	  	          template: function(myData){
	  	        	  if(myData.productModelCheckFlg == 0){
	  	        		return $('#textProductModelCheckFlgEqual0').val();//'���`�F�b�N';
	  	        	  }
	  	        	return $('#textProductModelCheckFlgEqual1').val();//'�`�F�b�N��';
	  	          },
	  	          type: "string",
	  	          title: jQuery("#headerProductModelCheckFlg").val(),
	  	          width: "130px",
	  	          attributes: {"class": "align-center"}
	  	        },
                {
	                  field:"fitModelMaker",
	                  type: "string",
	                  title: jQuery("#headerFitModelMaker").val(),
	                  width: "180px",
	                  attributes: {"class": "align-left, row_table"}
	            },
                {
                    field:"fitModelModel",
                    type: "string",
                    title: jQuery("#headerFitModelModel").val(),
                    width: "180px",
                    attributes: {"class": "align-left, row_table"}
                },
                { 
                    field: "masterMisMatchFlg",
                    template: function(myData){
                        if(myData.masterMisMatchFlg == 0){
                          return jQuery("#textNoMisMatch").val();
                        }
                      return jQuery("#textMisMatch").val();
                    },
                    type: "string",
                    title: jQuery("#headerFitModelMisMatchFlg").val(),
                    width: "130px",
                    attributes: {"class": "align-center"}
                },
	  	        { 
	  	          field: "productModelErrorFlg",
	  	          template: function(myData){
	  	        	  if(myData.productModelErrorFlg == 0){
	  	        		return "";
	  	        	  }
	  	        	return $('#textProductModelErrorFlgEqual1').val();
	  	          },
	  	          type: "string",
	  	          title: jQuery("#headerProductModelErrorFlg").val(),
	  	          width: "130px",
	  	          attributes: {"class": "align-center"}
	  	        },
	  	        { 
	  	          field:"descriptionSummary",
	  	          type: "date",
	  	          title: jQuery("#headerDescriptionSummary").val(),
	  	          width: "180px",
	  	          attributes: {"class": "align-left, row_table"}
	  	        },
  	        	{ 
	  	          field:"productSelectDisplay",
	  	          type: "string",
	  	          title: jQuery("#headerProductSelectDisplay").val(),
	  	          width: "180px",
	  	          attributes: {"class": "align-left, row_table"}
	  	        },
	  	        { 
	  	          field:"productProperPrice",
	  	          template: function(myData){
	  	        	  return $('#textProductProperPrice').val() + myData.productProperPrice;
	  	          },
	  	          type: "number",
	  	          title: jQuery("#headerProductProperPrice").val(),
	  	          width: "150px",
	  	          attributes: {"class": "align-right"}
	  	        },
	  	        { 
	  	          field: "categoryName",
	  	          type: "string",
	  	          title: jQuery("#headerCategoryName").val(),
	  	          width: "150px",
	  	          attributes: {"class": "align-left, row_table"}
	  	        },
	  	        ],
        height: 540,
        selectable: "multiple, row",
	    resizable : true,
	    dataBound: function(e){
	    	// uncomment if you wanna reset selectbox when paging, filter or sort
	    	/*var dropdownlist = $("#selectExcute").data("kendoDropDownList");
	    	// reset kendo dropdownlist
	    	dropdownlist.select(0);*/

	    	var grid = $("#gridModelFit4").data("kendoGrid");
	    	// Remove noData message
			jQuery("#gridModelFit4 .dataTables_empty").remove();
            jQuery("#selectAllCheckbox").attr("checked", false);
			if (this.dataSource.total() > 0) {
				// setCurrentSearchCondition();
				//i don't understand:
				/*var page = this.dataSource.page();

				if (page != currentPage) {
					currentPage = page;
					this.content.scrollTop(0);
				}*/

				// Add title for row
				var gridData = this.dataSource.view();
				for ( var i = 0; i < gridData.length; i++) {
					var currentUid = gridData[i].uid;
					var currenRow = grid.table.find("tr[data-uid='" + currentUid + "']");
					// $(currenRow).removeClass("k-alt");
					if (gridData[i].productModelCheckFlg == 1) {
						// Row checked
					    $(currenRow).removeClass("k-alt");
						$(currenRow).addClass("rowGridChecked");
					}
					if (gridData[i].productModelErrorFlg == 1) {
                        // Row error
					    $(currenRow).removeClass("k-alt");
					    $(currenRow).removeClass("rowGridChecked");
                        $(currenRow).addClass("rowGridError");
                    }
				}
			} else {
				// Show no data message
				jQuery("#gridModelFit4 .k-grid-content").append(
						'<div class="dataTables_empty" id="grid4EmptyElement">'
								+ jQuery("#noDataMessage")
										.val() + '</div>');
			}
			
			if (grid4RowSelected === "") {
				// set selected row at the 1st row
	            this.element.find('tbody tr:first').addClass('k-state-selected');
	            // Get selected row data items.
				var grid4 = $("#gridModelFit4").data("kendoGrid");
		        var selectedRows = grid4.select();
	            var data = grid4.dataItem(selectedRows);
	            handleCheckbox(selectedRows);
	            currentIdxGrid4 = 0;
	            
	            if (!validateDataGrid()) {
	            	// Get data of selected row
	            	if (data != null) {
	            		bindProductAllData(selectedRows, data);
	            	}
	            } else {
	            	isLoadFromGrid4 = true;
	                $("#btnSubmit").trigger("click", [selectedRows, data]);
	            }
			}
			
			// Handle clicking row.
			handleClickingRows();
			// Select all processing.
			handleSelectAll("#selectAllCheckbox", "#gridModelFit4 table tbody tr", ".productCheckbox", selectedKendoClass, selectedRowClass);
	    },
	    change: function(data) {
			var selectedRows = this.select();
			// Selected new row
	        if (currentIdxGrid4 != selectedRows.index()) {
	        	currentIdxGrid4 = selectedRows.index();
	        }
		},
    });
	    
	//Add filter
    bottomGrid.data("kendoGrid").thead.append(filterRow);
    // Set input numberic only in textbox search filter
	inputNumbericOnlyForBottomGrid();
	
	/*
	 * create auto complete for field by text field id and field name.
	 * @param txtId id used for set kendo AutoComplete
	 * @param field name used for filter in database. 
	 * @author hoang.ho
	 * @since 2014.03.12
	 */

	function setDataSourceForAutoComplete3(autoCompleteField) {
		if (autoCompleteField == "modelMaker") {
			return {
		        "fitModelCheckActionForm.entMstModel.modelMaker" :      $("#tbProductFitModelMaker").val(),
		        "fitModelCheckActionForm.autoCompleteField" :      autoCompleteField
		    };
		} else if (autoCompleteField == "modelName") {
			return {
		        "fitModelCheckActionForm.entMstModel.modelName" :      $("#tbProductFitModelModel").val(),
		        "fitModelCheckActionForm.autoCompleteField" :      autoCompleteField
		    };
		}
	    
	}
	function createAutoCompleteModelById3(txtId, fieldName) {
		$(txtId).kendoAutoComplete({
		    minLength : 1,
		    dataSource : new kendo.data.DataSource({
		        serverFiltering: true, 
		        transport : {
		            read : {
		                type: "POST",
		                dataType: "json",
		                url: "ajaxLoadAutoCompleModels.html",
		                data:  function (data) {
		                    return setDataSourceForAutoComplete3(fieldName);
		                }
		            }
		        },
		        schema: {
		            data: "fitModelCheckActionForm.autoCompleteList",
		        }
		    })
		});
	}
	createAutoCompleteModelById3("#tbProductFitModelMaker", "modelMaker");
	createAutoCompleteModelById3("#tbProductFitModelModel", "modelName");

}//end init grid4

/**
* create hidden list to content list URL of image and thumb.
*/
function createListHiddenThumb(listImage) {
   var resultHTML = "";
   jQuery(listImage).each(function( index, element ) {
       if (index < 4) {
        // Check productThumb or productImage is not empty
           if (element.productThumb !='' || element.productImage !='') {
               // productThumb and productImage not empty
               resultHTML += '<input type="hidden"'
                          +         'id="thumb_' + index + '"'
                          +         'value="' + element.productThumb + '" />';
               resultHTML += '<input type="hidden"'
                          +         'id="image_' + index + '"'
                          +         'value="' + element.productImage + '" />';
           } else {
               // productThumb or productImage is empty
               // Set thumb and image is noImage
               resultHTML += '<input type="hidden"'
                          +         'id="thumb_' + index + '"'
                          +         'value="' + noImageURL + '" />';
               resultHTML += '<input type="hidden"'
                          +         'id="image_' + index + '/>"'
                          +         'value="' + noImageURL + '" />';
           }
       }
   });
   jQuery("#thumbHiddenDiv").html(resultHTML);
   // Chck listImage is not null
   if(null != listImage) {
       listThumbCount = listImage.length;
       return
   }
   listThumbCount = 0;
}

function createListFitModel(data) {
    if (data != null) {
        // get data from server
        var grid2Data = data.fitModelCheckActionForm.listFitModelJsonStr;
        // set data to hidden field for process in popup
        $("#rqListModelJsonStr").val(grid2Data);
        $("#hListFitModelJsonStr").val(grid2Data);
        // reload grid1 and grid 2
        var mistFitModelJsonList =  $.parseJSON(grid2Data);
        //ReAdd dataSource for kendoGrid
        var grid1 = $("#gridModelFit1").data("kendoGrid");
        var grid2 = $("#gridModelFit2").data("kendoGrid");
        grid1.dataSource.data(mistFitModelJsonList);
        grid2.dataSource.data(mistFitModelJsonList);
        //Refresh to load new data
        grid1.refresh();
        grid2.refresh();
    }
}

function reloadGrid4Page(selectedRows) {
  grid4RowSelected = selectedRows;
  var grid = $("#gridModelFit4").data("kendoGrid");
  grid.dataSource.read();
  grid.refresh();
}

function reloadGrid4() {
  var grid = $("#gridModelFit4").data("kendoGrid");
  grid.dataSource.page(1);
  grid.content.scrollTop(0);
}

function realoadGrid(id) {
	  var grid = $("#" + id).data("kendoGrid");
	  grid.dataSource.page(1);
	  grid.content.scrollTop(0);
}

/**Nguyen.Chuong
 * get product data when click into row:
 * + list image and thumb.
 * + list fit model.
 * + product information.
 */
function getProductDataAjax(productId) {
    jQuery.ajax({
        type: "POST",
        url: "loadProductAllDataAjax.html",
        data: {
            productId: productId
        },
        dataType: "json",
        success: function(dataResult) {
            // List image of this product
            createListHiddenThumb(dataResult.fitModelCheckActionForm.listImage);
            createThumbList();
            // List model of this product
            createListFitModel(dataResult);
        }
    });
}

/**
 * Binding data for selected row
 */
function bindProductAllData(row, data) {
  var formatCurrentURL = window.location.pathname + "?" + jQuery("#productIdParamName").val() + "=" + data.productId;
  window.history.replaceState("", document.title, formatCurrentURL);
  //Load H1
  var productId = data.productId;
  var errorFlg = data.productModelErrorFlg;
  // get product check flg
  var productRegistrationComplete = data.productRegistrationFlg;
  $("#prdResgistration").val(productRegistrationComplete);
  // get product model error flg
  var productModelError = data.productModelErrorFlg;
  var productCheckText = "";
  var productModelErrorText = "";
  // get text from hidden field depend on  data of flg
  if (productRegistrationComplete == 1) {
    productCheckText = $("#msgProductCheckCompleted").val();
  } else {
    productCheckText = $("#msgProductUnchecked").val();
  }
  // get text from hidden field depend on  data of flg
  if (productModelError == 1) {
    productModelErrorText = $("#msgProductModelErrorFlg").val();
  }
  $("#spnProductStatus").text(productCheckText + productModelErrorText);
    jQuery("#productIdH1").text(productId);
    $("#productId").val(productId);
    $("#productErrorFlg").val(errorFlg);
    //Create link to webike syouhin page
    if(0 != data.productSyouhinSysCode && null != data.productSyouhinSysCode) {
        jQuery("#productSysCodeH1 a").attr("href", jQuery("#linkToSyouhinPage").val() + data.productSyouhinSysCode + "/") ;
        jQuery("#productSysCodeH1 a").html(data.productSyouhinSysCode);
    } else {
        //syouhin code null then show text
        jQuery("#productSysCodeH1").text(jQuery("#textSyouhinEqual0").val());
    }
    //Add loading image to thumb list, image, model table.
    jQuery("#imageThumb").attr("src", "./images/processing.gif");
    jQuery("#imageThumb").css("display", "none");
    jQuery("#imageThumbLoading").attr("src", "./images/processing.gif");
    jQuery("#imageThumbLoading").css("display", "");
    jQuery(".selector_thumb").attr("src", "./images/processing.gif");
    //Add loading image for model table
//    jQuery(".imgLoadingModelTable").css("display", "block");
//    jQuery("#modelListTable tbody").html("");
    jQuery("#gridModelFit1 .dataTables_empty").remove();
    jQuery("#gridModelFit1 table tbody").html(loadingImage);
    jQuery("#gridModelFit2 .dataTables_empty").remove();
    jQuery("#gridModelFit2 table tbody").html(loadingImage);

    //Load image, model for product.
    getProductDataAjax(data.productId);


    //Load info for product.
    //Set new name for new selected product.
    jQuery("#productInfoProductName span").text(data.productName);
    //Remove old kendoUI tooltip.
    jQuery("#productInfoProductName span").data("kendoTooltip").destroy();
    //Create new kendoUI tooltip 
    productNameTooltip = $("#productInfoProductName span").kendoTooltip({
        content: $("#productInfoProductName span").html(),
        animation : {
            close : {
                effects : "fade:out"
            },
            open : {
                effects : "fade:in",
                duration : 300
            }
        }
    });

    jQuery("#productInfoProductCode span").html(data.productCode);

    //Set new description for new selected product.
    jQuery("#productInfoDescriptionSummary span").html(data.descriptionSummary);
    //Remove old kendoUI tooltip.
    jQuery("#productInfoDescriptionSummary span").data("kendoTooltip").destroy();
    //Create new kendoUI tooltip 
    productSummaryTooltip = $("#productInfoDescriptionSummary span").kendoTooltip({
            content: $("#productInfoDescriptionSummary span").html(),
            animation : {
                close : {
                    effects : "fade:out"
                },
                open : {
                    effects : "fade:in",
                    duration : 300
                }
            }
        });

    //Set new description for new selected product.
    jQuery("#productInfoDescriptionRemarks span").html(data.descriptionRemarks);
    //Remove old kendoUI tooltip.
    jQuery("#productInfoDescriptionRemarks span").data("kendoTooltip").destroy();
    //Create new kendoUI tooltip 
    productDescriptionRemarksTooltip = $("#productInfoDescriptionRemarks span").kendoTooltip({
            content: $("#productInfoDescriptionRemarks span").html(),
            animation : {
                close : {
                    effects : "fade:out"
                },
                open : {
                    effects : "fade:in",
                    duration : 300
                }
            }
        });
    //Reset grid 3
    jQuery("#btnInsertMode").click();
}


//Set selected for list status
//Value = 0: Yes
//Value = 1: No
function setSelectedValue(value) {
	$("select option").filter(function() {
	    //may want to use $.trim in here
	    return $(this).text() == value; 
	}).prop('selected', true);
}
//Set data filter condition to sent to server
function setDataForDataSource(sortField, sortDir) {
    return {
    	"fitModelCheckActionForm.entTblFactoryProductNew.matterNo" :      jQuery("#matterNo").val(),
        "fitModelCheckActionForm.entTblFactoryProductNew.productId" :      productId,
        "fitModelCheckActionForm.entTblFactoryProductNew.productSyouhinSysCode" :           productSyouhinSysCode,
        "fitModelCheckActionForm.entTblFactoryProductNew.brandName" :    brandName,
        "fitModelCheckActionForm.entTblFactoryProductNew.productName" :    productName,
        "fitModelCheckActionForm.entTblFactoryProductNew.productCode" :    productCode,
        "fitModelCheckActionForm.entTblFactoryProductNew.productModelCheckFlg" : productModelCheckFlg,
        "fitModelCheckActionForm.entTblFactoryProductNew.fitModelMaker" : productFitModelMaker,
        "fitModelCheckActionForm.entTblFactoryProductNew.fitModelModel" : productFitModelModel,
        "fitModelCheckActionForm.entTblFactoryProductNew.masterMisMatchFlg" : productFitModelMisMatchFlg,
        "fitModelCheckActionForm.entTblFactoryProductNew.productModelErrorFlg" :         productModelErrorFlg,
        "fitModelCheckActionForm.entTblFactoryProductNew.descriptionSummary" : descriptionSummary,
        "fitModelCheckActionForm.entTblFactoryProductNew.productSelectDisplay" :   productSelectDisplay,
        "fitModelCheckActionForm.entTblFactoryProductNew.productProperPriceFrom"    :   productProperPriceFrom,
        "fitModelCheckActionForm.entTblFactoryProductNew.productProperPriceTo"      :   productProperPriceTo,
        "fitModelCheckActionForm.entTblFactoryProductNew.categoryName"      :   categoryName,
        "fitModelCheckActionForm.entTblFactoryProductNew.sortField"      :   sortField,
        "fitModelCheckActionForm.entTblFactoryProductNew.sortDir"      :   sortDir
    };
}
function setDataSourceForAutoComplete(autoCompleteField) {
    return {
        "fitModelCheckActionForm.entMstModel.modelMaker" :      $("#txtModelMaker").val(),
        "fitModelCheckActionForm.entMstModel.modelDisplacement" :      $("#txtDisplacement").val(),
        "fitModelCheckActionForm.entMstModel.modelName" :      $("#txtmodelName").val(),
        "fitModelCheckActionForm.autoCompleteField" :      autoCompleteField
    };
}

//Get filter condition from filter bar
function inputNumbericOnlyForBottomGrid() {
	/*onlyInpuNumerric("tbProductId");*/
	//Utils.bindFormatOnTextfield("tbProductSyouhinSysCode", Utils._TF_NUMBER);
	initNumberTextBox("tbProductSyouhinSysCode");
	//Utils.bindFormatOnTextfield("tbProductProperPriceFrom", Utils._TF_NUMBER);
	initNumberTextBox("tbProductProperPriceFrom");
	//Utils.bindFormatOnTextfield("tbProductProperPriceTo", Utils._TF_NUMBER);
	initNumberTextBox("tbProductProperPriceTo");
  initNumberTextBox("txtDisplacement");
}
function getSearchCondition() {
	var productIdVal = jQuery("#tbProductId").val();
	if (validateNumbericAndBlank(productIdVal)) {
		productId = productIdVal;
	} else {
		productId = productIdVal.substring(1, productIdVal.length);
	}
	productSyouhinSysCode = jQuery("#tbProductSyouhinSysCode").val();
	brandName = jQuery("#tbBrandName").val();
	productName = jQuery("#tbProductName").val();
	productCode = jQuery("#tbProductCode").val();
	productModelCheckFlg = jQuery("#listProductModelCheckFlg").val();
	productFitModelMaker = jQuery("#tbProductFitModelMaker").val();
	productFitModelModel = jQuery("#tbProductFitModelModel").val();
	productFitModelMisMatchFlg = jQuery("#listProductFitModelMisMatchFlg").val();
	productModelErrorFlg = jQuery("#listProductModelErrorFlg").val(); //TODO param
	descriptionSummary = jQuery("#tbDescriptionSummary").val();
	productSelectDisplay = jQuery("#tbProductSelectDisplay").val();
	productProperPriceFrom = jQuery("#tbProductProperPriceFrom").val();
	productProperPriceTo = jQuery("#tbProductProperPriceTo").val();
	categoryName = jQuery("#tbCategoryName").val();
	//Validate and trim value
    if (productId) {
    	productId = productId.trim();
    }
    if (productSyouhinSysCode) {
    	productSyouhinSysCode = productSyouhinSysCode.trim();
    }
    if (brandName) {
    	brandName = brandName.trim();
    }
    if (productName) {
    	productName = productName.trim();
    }
    if (productCode) {
    	productCode = productCode.trim();
    }
    if (productModelCheckFlg) {
    	productModelCheckFlg = productModelCheckFlg.trim();
    }
    if (productFitModelMaker) {
        productFitModelMaker = productFitModelMaker.trim();
    }
    if (productFitModelModel) {
        productFitModelModel = productFitModelModel.trim();
    }
    if (productFitModelMisMatchFlg) {
        productFitModelMisMatchFlg = productFitModelMisMatchFlg.trim();
    }
    if (productModelErrorFlg) {
    	productModelErrorFlg = productModelErrorFlg.trim();
    }
    if (descriptionSummary) {
    	descriptionSummary = descriptionSummary.trim();
    }
    if (productSelectDisplay) {
    	productSelectDisplay = productSelectDisplay.trim();
    }
    if (productProperPriceFrom) {
    	productProperPriceFrom = productProperPriceFrom.trim();
    }
    if (productProperPriceTo) {
    	productProperPriceTo = productProperPriceTo.trim();
    }
    if (categoryName) {
    	categoryName = categoryName.trim();
    }	

}

function paramToFitModelMutltiProduct() {
	// set array model error flg to default value
	arrayProductModelErrorFlg = "";
	// set array product id to default value
	arrayProductId = "";
	// set product model check to default value
	isProductRegistrationFlg = false;
	$('.productCheckbox:checked').each(function() {
		// get product model error flg from check box value
		var productModelErrorFlg = $(this).val(); //TODO param action
		// get product id from check box id
		var productId = $(this).attr('id');
		// add to array string
		arrayProductId += productId + "_";
		// add to array string
		arrayProductModelErrorFlg += productModelErrorFlg + "_";
		// get product model check from check box product registration attribute
		var productResgistration = $(this).attr("productRegistration");
		// check if any product model check is 1 (mean checked complete)
		// then set value ischeck to true
		if (productResgistration == 1) {
			isProductRegistrationFlg = true;
		}
	});
};

/* Thanh Start*/
function processInserFitModelByMultiProduct() {
	matterNo = $("#matterNo").val();
	var currentProductId = $("#productId").val();
	/* BOE dang.nam 2014/06/06 select all*/
	getSearchCondition();
	var checkAll = ($("#selectAllCheckbox").attr('checked') == 'checked');
	/* EOE dang.nam 2014/06/06 select all*/ 
	
	// load ajax delete recode
	$.ajax({
		type : 'POST',
		url : 'processInserFitModelByMultiProduct.html',
		dataType : 'json',
		data : {
			"checkAll":checkAll,
			"fitModelCheckActionForm.product.productId": currentProductId,
			"arrayProductId" : arrayProductId,
			"arrayProductModelErrorFlg" : arrayProductModelErrorFlg,
			"fitModelCheckActionForm.factoryMatter.matterNo": matterNo,
			"fitModelCheckActionForm.fitModel.fitModelMaker" : (jQuery("#txtMakerDialog").val() + "").trim(),
			"fitModelCheckActionForm.fitModel.fitModelModel" : (jQuery("#txtModelDialog").val() + "").trim(),
			"fitModelCheckActionForm.fitModel.fitModelStyle" : (jQuery("#txtStyleDialog").val() + "").trim(),
			/*BOE dang.nam 2014/06/06 select all*/
			"fitModelCheckActionForm.entTblFactoryProductNew.matterNo" :      jQuery("#matterNo").val(),
	        "fitModelCheckActionForm.entTblFactoryProductNew.productId" :      productId,
	        "fitModelCheckActionForm.entTblFactoryProductNew.productSyouhinSysCode" :           productSyouhinSysCode,
	        "fitModelCheckActionForm.entTblFactoryProductNew.brandName" :    brandName,
	        "fitModelCheckActionForm.entTblFactoryProductNew.productName" :    productName,
	        "fitModelCheckActionForm.entTblFactoryProductNew.productCode" :    productCode,
	        "fitModelCheckActionForm.entTblFactoryProductNew.productModelCheckFlg" : productModelCheckFlg,
	        "fitModelCheckActionForm.entTblFactoryProductNew.fitModelMaker" : productFitModelMaker,
	        "fitModelCheckActionForm.entTblFactoryProductNew.fitModelModel" : productFitModelModel,
	        "fitModelCheckActionForm.entTblFactoryProductNew.masterMisMatchFlg" : productFitModelMisMatchFlg,
	        "fitModelCheckActionForm.entTblFactoryProductNew.productModelErrorFlg" :         productModelErrorFlg, //TODO Insert
	        "fitModelCheckActionForm.entTblFactoryProductNew.descriptionSummary" : descriptionSummary,
	        "fitModelCheckActionForm.entTblFactoryProductNew.productSelectDisplay" :   productSelectDisplay,
	        "fitModelCheckActionForm.entTblFactoryProductNew.productProperPriceFrom"    :   productProperPriceFrom,
	        "fitModelCheckActionForm.entTblFactoryProductNew.productProperPriceTo"      :   productProperPriceTo,
	        "fitModelCheckActionForm.entTblFactoryProductNew.categoryName"      :   categoryName,
	        "fitModelCheckActionForm.entTblFactoryProductNew.sortField"      :   "productId",
	        "fitModelCheckActionForm.entTblFactoryProductNew.sortDir"      :   ""
        	/*EOE dang.nam 2014/06/06 select all*/
		},
		beforeSend: function () {
			// remove all class of msg div and clear msg
			clearMsgText();
	    	showProcessBar();
		},
		error: function () {
  			hideProcessBar();
		},
		success : function(data) {
			if (data == null) {
				showMsgText(0);
				return;
			} else {
				createListFitModel(data);
				showMsgText(data.fitModelCheckActionForm.msgCode);
			}
			clearProductGrid();
  			hideProcessBar();
		}
	});
}

function initAddModelDialogGrid() {//if switch page, isNewPage = true
	var currentPageGrid5 = 1;
	var dataSourceGrid5;

	//Load data for grid
	function loadGrid5Data() {
	    var grid5 = $("#addModelDialogGrid").kendoGrid({
	        dataSource: dataSourceGrid5,
	        //sortable: true,
	        sortable: {
	            mode: "single",
	            allowUnsort: false
	        },
	        pageable: {
	            refresh: true,
	            pageSizes: [50, 100],
	            buttonCount: 5,
	            messages: {
	                display: jQuery("#pagingDisplay").val(),
	                empty: jQuery("#pagingEmpty").val(),
	                itemsPerPage: jQuery("#pagingItemsPerPage").val(),
	                first: jQuery("#pagingFirst").val(),
	                previous: jQuery("#pagingPrevious").val(),
	                next: jQuery("#pagingNext").val(),
	                last: jQuery("#pagingLast").val()
	            }
	        },
	        columns: [{
		    			field: "modelMaker",
		    			title: $("#hModelMaker").val(),
		    			width: "90px",
		    			attributes: {"class": "align-center"}
		    		}, {
		    			field: "modelDisplacement",
		    			title: $("#hModelDisplacement").val(),
		    			width: "60px",
		    			attributes: {"class": "align-right"}
		    		}, {
		    			field: "modelName",
		    			title: $("#hModelName").val(),
		    			width: "200px",
		    			attributes: {"class": "align-left"}
	        		}],
	        height: 100,
	        maxWidth: 550,
	        scrollable: true,
	        selectable: true,
	        resizable : true,
	        sortable: { mode: "single", allowUnsort: false },
	        dataBound: function (e) {
	            //Remove noData message
	            jQuery("#addModelDialogGrid .dataTables_empty").remove();
	            if (this.dataSource.total() > 0) {
	                var page = this.dataSource.page();
	                if (page != currentPageGrid5) {
	                	currentPageGrid5 = page;
	                    this.content.scrollTop(0);
	                }
	            } else {
	                //Show no data message
	                jQuery("#addModelDialogGrid .k-grid-content").append('<div class="dataTables_empty">' + jQuery("#noDataMessage").val() + '</div>');
	            }
	        },
		    change: function(e) {
		    	//Get selected Row
				var selectedRows = this.select();
		        jQuery("#txtMakerDialog").val(this.dataItem(selectedRows).get("modelMaker"));
		        jQuery("#txtModelDialog").val(this.dataItem(selectedRows).get("modelName"));
            	validatablePopUp.validate();
		    }
	    });

	    return grid5;
	}

		
	//Set data filter condition to sent to server
	function setParamsForDataSourceGrid5(sortField, sortDir) {
	    return {
	        "fitModelCheckActionForm.entMstModel.modelMaker" :      $("#modelMakerSearch").val(),
	        "fitModelCheckActionForm.entMstModel.modelDisplacement" :      $("#modelDisplacementSearch").val(),
	        "fitModelCheckActionForm.entMstModel.modelName" :      $("#modelNameSearch").val(),
	        "fitModelCheckActionForm.entMstModel.sortField" : sortField,
	        "fitModelCheckActionForm.entMstModel.sortDir": sortDir
	    };
	}
	

		//Filter bar
	    var filterRowGrid5 = $(
				'<tr>'
				+ '<th class="k-header" style="text-align: center;" data-field="modelMaker"><input id="modelMakerSearch" type="text" class="txtFilter k-textbox" style="width: 80px"/></th>'
				+ '<th class="k-header" style="text-align: center;" data-field="modelDisplacement"><input id="modelDisplacementSearch" type="text" class="txtFilter k-textbox" style="width: 60px"/></th>'
				+ '<th class="k-header" style="text-align: center;" data-field="modelName"><input id="modelNameSearch" type="text" class="txtFilter k-textbox" style="width: 365px"/></th>'
				+ '</tr>');
	    //dataSource for brand master grid
	    dataSourceGrid5 = new kendo.data.DataSource({
	        serverPaging: true,
	        serverSorting: true,
	        autoBind: false,
	        // 50 product in 1 page
	        pageSize: 50,
	        transport: {
	            read: {
	                type: "POST",
	                dataType: "json",
	                url: "ajaxLoadModelList.html",
	                data: function (data) {
	                	var sortField = "modelCode + 0";
	                	var sortDir = "asc";
	                	if (data != undefined && data.sort != undefined && data.sort.length > 0) {
	                		sortField = data.sort[0].field;
	                		sortDir = data.sort[0].dir;
	                	}
	                    return setParamsForDataSourceGrid5(sortField, sortDir);
	                },
	                cache: false,
	            }
	        },
	        schema: {
	            data: "fitModelCheckActionForm.listMstModel",
	            total: "fitModelCheckActionForm.mstModelListCount"
	        }
	    });

	    var grid = loadGrid5Data();

	    //Add filter
	    grid.data("kendoGrid").thead.append(filterRowGrid5);
	  
	    //Button Search click
	    function grid5ReloadData() {
	    	// Reset message.
	    	$("#content #message").remove();
	    	//Reload data and goto first page
	    	grid.data("kendoGrid").dataSource.page(1);
	    	
	    	//Go to top of grid
	    	grid.data("kendoGrid").content.scrollTop(0);
	    	
	    }
//	    // create event when text change for filter grid 5. maybe make common method
	    $("#addModelDialogGrid").on('keyup', 'span.txtFilter', function (e) {
	    	 if (e.keyCode == 13) {
                 jQuery("#addModelDialogGrid .dataTables_empty").remove();
	    		 grid5ReloadData();
	         }
	    });
}

function checkValidateGrid() {
	var checkValidateGrid = validateDataGrid();
	if (checkValidateGrid == true) {
		var initConfirmDialog = $("#confirmDialog").data("kendoWindow");
		initConfirmDialog.center().open();
	} else {
		excuteAction();
	}
}

function openAddModelDialog() {
	var actionId = jQuery("#selectExcute").val();
	if (actionId == "0") {
		//Sampleimage3
		var initAddModelDialog = $("#addModelDialog").data("kendoWindow");
		initAddModelDialog.center().open();
	} else if (actionId == "1") {
		//Sampleimage4
		initDisplayModelProduct();
		var initAddModelDialog = $("#displayModelProduct").data("kendoWindow");
		initAddModelDialog.center().open();
	}
}

function excuteAction(){
	var checkedCount = $(".productCheckbox:checked").length;
	if (checkedCount > 0) {
		// TODO add code here to check product check or not
		paramToFitModelMutltiProduct();
		if (isProductRegistrationFlg) {
			openPopup("cannotChangeProductDialog");
			return;
		}
		var checkValidateGrid = validateDataGrid();
		if (checkValidateGrid == true) {
			var initConfirmDialog = $("#confirmDialog").data("kendoWindow");
			initConfirmDialog.center().open();
		} else {
			openAddModelDialog();
		}
	} else {
		// Show PleaseSelect dialog.
		var pleaseSelectDialog = $("#pleaseSelectDialog").data("kendoWindow");
		pleaseSelectDialog.center().open();
	}
}

function initPleaseSelectDialog() {
	// Init pleaseSelect dialog.
	$("#pleaseSelectDialog").kendoWindow({
		minWidth : 300,
		minWeight : 200,
		title : $("#hdTitle").val(),
		resizable : false,
		modal : true,
		visible : false,
		draggable : false
	});
	// Get pleaseSelect dialog.
	var pleaseSelectDialog = $("#pleaseSelectDialog").data("kendoWindow");
	$("#pleaseSelectDialogOk").click(function(e) {
		pleaseSelectDialog.close();
	});
}

function initCannotChangeProductDialog() {
	// Init pleaseSelect dialog.
	$("#cannotChangeProductDialog").kendoWindow({
		minWidth : 300,
		minWeight : 200,
		title : $("#hdTitle").val(),
		resizable : false,
		modal : true,
		visible : false,
		draggable : false
	});
	// Get can not change dialog.
	var cannotChangeDialog = $("#cannotChangeProductDialog").data("kendoWindow");
	$("#btnCannotChange").click(function(e) {
		cannotChangeDialog.close();
	});
}


function initDuplicateValueDialog() {
	// Init pleaseSelect dialog.
	$("#duplicateValueDialog").kendoWindow({
		minWidth : 300,
		minWeight : 200,
		title : $("#hdTitle").val(),
		resizable : false,
		modal : true,
		visible : false,
		draggable : false
	});
	// Get pleaseSelect dialog.
	var duplicateValueDialog = $("#duplicateValueDialog").data("kendoWindow");
	$("#btnDuplicateValueDialog").click(function(e) {
		duplicateValueDialog.close();
	});
}

function initAddModelDialog() {
	// Init pleaseSelect dialog.
	$("#addModelDialog").kendoWindow({
		height:430,
		/*
		maxWidth : 600,
		maxHeight : 800,
		*/
		width: 620,
		height: 428,
		title : $("#hdAddModelDialogTitle").val(),
		resizable : false,
		modal : true,
		visible : false,
		draggable : false,
		open: function() {
			removeKeyPressOnGrid2();
			clearGrid2OldData();
		},
		close: function() {
			addKeyPressOnGrid2();
		}
	});
	// Get pleaseSelect dialog.
	var initAddModelDialog = $("#addModelDialog").data("kendoWindow");
	$("#btnUpdateProductDialog").click(function(e) {
		/* BOE @rcv! Luong.Dai 2014/08/25: change validate, accepte save data at waining length*/
    // If validate failed. prevent submit.
		/*var isOnlyModelInvalid = (validatablePopUp.errors().length == 1) && (validatablePopUp._errors["txtModelDialog"] != null);
		if (!validatablePopUp.validate() && !isOnlyModelInvalid) {
			return false;
		}*/
    	ERROR_FIT_MODEL_FLG = false;
    	validatablePopUp.validate();
    	if (ERROR_FIT_MODEL_FLG) {
        	return false;
    	}
    /* BOE @rcv! Luong.Dai 2014/08/25: change validate, accepte save data at waining length*/
		//Update to Database
		processInserFitModelByMultiProduct();
		initAddModelDialog.close();
	});
	initAddModelDialogGrid();
	/*
	 * create auto complete for field by text field id and field name.
	 * @param txtId id used for set kendo AutoComplete
	 * @param field name used for filter in database. 
	 * @author hoang.ho
	 * @since 2014.02.13
	 */

	function setDataSourceForAutoComplete2(autoCompleteField) {
	    return {
	        "fitModelCheckActionForm.entMstModel.modelMaker" :      $("#modelMakerSearch").val(),
	        "fitModelCheckActionForm.entMstModel.modelDisplacement" :      $("#modelDisplacementSearch").val(),
	        "fitModelCheckActionForm.entMstModel.modelName" :      $("#modelNameSearch").val(),
	        "fitModelCheckActionForm.autoCompleteField" :      autoCompleteField
	    };
	}
	function createAutoCompleteModelById2(txtId, fieldName) {
		$(txtId).kendoAutoComplete({
		    minLength : 1,
		    dataSource : new kendo.data.DataSource({
		        serverFiltering: true, 
		        transport : {
		            read : {
		                type: "POST",
		                dataType: "json",
		                url: "ajaxLoadAutoCompleModels.html",
		                data:  function (data) {
		                    return setDataSourceForAutoComplete2(fieldName);
		                }
		            }
		        },
		        schema: {
		            data: "fitModelCheckActionForm.autoCompleteList",
		        }
		    })
		});
	}
	createAutoCompleteModelById2("#modelMakerSearch", "modelMaker");
	createAutoCompleteModelById2("#modelDisplacementSearch", "modelDisplacement");
	createAutoCompleteModelById2("#modelNameSearch", "modelName");
}

function initConfirmDialog() {
	// Init pleaseSelect dialog.
	$("#confirmDialog").kendoWindow({
		minWidth : 600,
		minWeight : 500,
		title : $("#hdTitle").val(),
		resizable : false,
		modal : true,
		visible : false,
		draggable : false
	});
	// Get pleaseSelect dialog.
	var initAddModelDialog = $("#confirmDialog").data("kendoWindow");
	$("#confirmDialogOk").click(function(e) {
		//Update to Database
		modePopup = true;
		initAddModelDialog.close();
		//Call submit to save model.
      $("#btnSubmit").click();
	});
	$("#confirmDialogCancel").click(function(e) {
		initAddModelDialog.close();
		openAddModelDialog();
	});
}

function initConfirmChangeRowDialog() {
	// Init pleaseSelect dialog.
	$("#confirmChangeRowDialog").kendoWindow({
		minWidth : 600,
		minWeight : 500,
		title : $("#hdTitle").val(),
		resizable : false,
		modal : true,
		visible : false,
		draggable : false
	});
	// Get pleaseSelect dialog.
	var initAddModelDialog = $("#confirmChangeRowDialog").data("kendoWindow");
	$("#confirmChangeRowDialogOk").click(function(e) {
		showProcessBar();
		isFromChangeRow = true;
		//Update to Database
		initAddModelDialog.close();
		//Call submit to save model.
      $("#btnSubmit").trigger("click", [selectedRodGrid4, dataSourceGrid4]);
	});
	$("#confirmChangeRowDialogCancel").click(function(e) {
		showProcessBar();
		//isLoadFromGrid4 = false;
		initAddModelDialog.close();
	    // process change row to check
	    checkProductChange(grid4CurrentProductId, grid4RowSelectedIndex);
		/*if (dataSourceGrid4 != null) {
  		  bindProductAllData(selectedRodGrid4, dataSourceGrid4);
		}*/
	});
}

function initDisplayModelProduct() {
	// Init pleaseSelect dialog.
	$("#displayModelProduct").kendoWindow({
		/*
		minWidth : 500,
		minHeight : 600,
		maxWidth: 650,
		maxHeight: 400,
		*/
		width: 600,
		height: 358,
		title : $("#hdDisplayModelProductTitle").val(),
		resizable : false,
		modal : true,
		visible : false,
		draggable : false
	});
	initModelProductGrid();
}

function initModelProductGrid() {
	// Define data get from sever and parse to json
	var modelFitListJsonStr = $("#hListFitModelJsonStr").val();
	var modelFitListJson = $.parseJSON(modelFitListJsonStr);	
	// create kendo grid.
	$("#displayModelProductTable").kendoGrid({
		dataSource: {
			data: modelFitListJson,
			schema: {
				model: {
					fields: {
						fitModelMaker: {type: "string"}
				    	, fitModelModel: {type: "string"}
				    	, fitModelStyle: {type: "string"}
					}
				}
			}
		},
        height: 170,
        maxWidth: 550,
        scrollable: true,
        selectable: true,
        resizable : true,
        sortable: {
            mode: "single",
            allowUnsort: false
        },
        pageable: false,
		columns: [
			{
				field: "fitModelMaker",
				title: $("#hFitModelMaker").val(),
				width: "150px",
				attributes: {"class": "align-center"}
			}
			, {
				field: "fitModelModel",
				title: $("#hFitModelModel").val(),
				width: "250px",
				attributes: {"class": "align-left"}
			}
			, {
				field: "fitModelStyle",
				title: $("#hFitModelStyle").val(),
				width: "150px",
				attributes: {"class": "align-left"}
			}
		],
		
	});
	$("#displayModelProductTable .k-grid-content").css("height","144px");
}
/* BOE process dialog add model for multi product Le.Dinh 2014/01/27 */
function validateDataGrid() {
    //1. Get data from kendogrid data source
    var grid1 = $("#gridModelFit1").data("kendoGrid");
    var dataSource1 = grid1.dataSource;
    var raw1 = dataSource1.data();
    var length1 = raw1.length;
    
    //1. Get data from kendogrid data source
    var grid2 = $("#gridModelFit2").data("kendoGrid");
    var dataSource2 = grid2.dataSource;
    var raw2 = dataSource2.data();
    var length2 = raw2.length;
    
    // check if different length then go to update
    if (length1 != length2) {
    	return true;
    } else {
    	for (var i = 0; i < length1; i++) {
    			// check if any data in the same row is different then go to update and break for
    			if (raw1[i].fitModelMaker != raw2[i].fitModelMaker
    					|| raw1[i].fitModelModel != raw2[i].fitModelModel
    					|| raw1[i].fitModelStyle != raw2[i].fitModelStyle) {
    				return true;
    				break;
    			} else {
    				// check the last index of array
    				if (i == (length1 - 1)) {
    					// if the last index is the same then return false and prevent update
    					if (raw1[i].fitModelMaker == raw2[i].fitModelMaker
    	    					&& raw1[i].fitModelModel == raw2[i].fitModelModel
    	    					&& raw1[i].fitModelStyle == raw2[i].fitModelStyle) {
    						return false;
    					}
    				}
    			}
    	}
    }
}

//Function to generate imageBlock.
function createThumbList() {
    var listThumbCnt = jQuery("#listThumbCount").val();
    //Create html
    //Add previous button
    var listThumbHTML = '<ul class="selector" id="selector">';
    var liClass = "display: list-item;";
    //Set thumb count to 4
//    if (listThumbCnt < 4) {
    listThumbCnt = 4;
//    }
    //Loop to create list of thumb.
    for (var i = 0; i < listThumbCnt; i++) {
        var thumbURL = jQuery("#thumb_" + i).val();
        var imageURL = jQuery("#image_" + i).val();
        var thumbClass = "fancy_box";
        var liHTML = "";
        //Add noImage to list
        if (!thumbURL || !imageURL) {
            liHTML = '<li class="list_thumb" style="display: list-item;">'
                    +   '<a href="" onclick="return false;">'
                    +       '<img class="selector_thumb" src="' + noImageURL + '">'
                    +   '</a>'
                    + '</li>';
        } else {
            //Create thumb.
            liHTML = '<li thumbId="' + i + '" class="list_thumb" style="' + liClass + '">'
                    +   '<a href="' + imageURL + '" id="selector_' + i + '" class="' + thumbClass + '" rel="fancy_box" onclick="return false;">'
                    +       '<img class="selector_thumb" src="' + thumbURL + '">'
                    +   '</a>'
                    + '</li>';
        }
        listThumbHTML += liHTML;
    }
    //Add next button
    listThumbHTML += '</ul>';
    //Show list thumb.
    jQuery(".thumb_selector").html(listThumbHTML);

    //Change first thumb of list to selected thumb.
    //Get first thumb of list
    var selectedThumb = jQuery(".list_thumb a[class='fancy_box']")[0];
    if(selectedThumb) {
        //Change class of first thumb to selected.
        jQuery(selectedThumb).attr("class", "image_selected");
        //Show image thumb mapping with selected thumb.
        //Get No of selected thumb.
        var selectThumbId = jQuery(selectedThumb).parent().attr("thumbId");
        //Set current selected thumb
        currentSelectedThumb = selectThumbId;
        //Get image thumb url.
        var imageThumbURL = jQuery("#image_" + selectThumbId).val();
    } else {
        imageThumbURL = noImageURL;
    }
    //Show image thumb.
    if (imageThumbURL != noImageURL) {
        jQuery("#imageAhref").attr("href", imageThumbURL);
        jQuery("#imageAhref").attr("class", "fancy_box");
    } else {
        jQuery("#imageAhref").attr("href", "#");
        jQuery("#imageAhref").attr("class", "noImage");
    }
    
    jQuery("#imageThumb").attr("src", imageThumbURL);
    //Show thumb image and hide loading image
    jQuery("#imageThumb").css("display", "");
    jQuery("#imageThumbLoading").css("display", "none");
}

/**
 * switch thumb to image when mouse over thumb.
 */
function switchThumb(e) {
    if (isTextboxFocus == false) {
        //If mouseover to current selected thumb.
        if(jQuery("#selector_" + e).attr('class') === 'image_selected') {
            return;
        }
        //Check no image
        if(jQuery("#thumb_" + e).length < 1) {
            return;
        }
        //Remove old selected Thumb
        jQuery("#selector_" + currentSelectedThumb).attr('class', "fancy_box");
        //Change current select.
        currentSelectedThumb = e;
        //Set new selected Thumb
        jQuery("#selector_" + e).attr('class', "image_selected");
    
        // Change image Thumb
        jQuery("#imageAhref").attr("href", jQuery("#image_" + e).val());
        jQuery("#imageThumb").attr("src", jQuery("#image_" + e).val());
    }
    return;
}

function clearMsgText() {
	// remove all class of msg div and clear msg
	$("#message").removeClass("has-message success");
	$("#message").removeClass("has-message msg-error");
	$("#message").text("");
}

function showMsgText(msgCode) {
	if (msgCode == 1) {
		$("#message").addClass("has-message success");
		$("#message").text(msgSuccess);
	} else {
		$("#message").addClass("has-message msg-error");
		$("#message").text(msgFail);
	}
}
////////////////////////////////////////////////////////////////////////////////////end bottom grid area///////////////////////////////////
////////////////BOE_Le.Dinh////////////////////////
//function process process code here
function filterGrid3Common() {
	// get value
	var number = $("#candidate").val();
	var modelText = $("#txtModel").val();
	var makerText = $("#txtMaker").val();
	
	// validate
	if (modelText == "" || makerText == "") {
		return;
	} else {
		if (!validateNumberic(number)) {
			return;
		} else {
			if (modelText.length < number) {
				$("#txtmodelName").val(modelText);
				txtTofillter = modelText;
			} else {
				var filterText = (modelText + "").trim().substring(0,number);
				$("#txtmodelName").val(filterText);
				txtTofillter = filterText;
			}
			$("#txtModelMaker").val(makerText);
		}
	}
	var e = $.Event('keyup');
    e.keyCode = 13; // Enter key
    $('#txtModelMaker').trigger(e);
}
//Input numberic only in textbox filter
function inputNumbericOnly() {
	//Utils.bindFormatOnTextfield("candidate", Utils._TF_NUMBER);
	initNumberTextBox("candidate");
}
//function process process code here
function clearProductGrid() {
	var grid = $("#gridModelFit4").data("kendoGrid");
	/*grid.refresh();*/
  grid.dataSource.page(1);
  //Go to top of grid
  grid.content.scrollTop(0);
}
function getColumnData() {
	var grid = $("#gridModelFit3").data().kendoGrid;
	$.each(grid.dataSource.view(), function () {
		if(maker.indexOf(this.modelMaker) == -1){
			maker += this.modelMaker + ",";
		}
		if(modelDisplacement.indexOf(this.modelDisplacement) == -1){
			modelDisplacement += this.modelDisplacement + ",";
		}
		if(modelName.indexOf(this.modelName) == -1){
			modelName += this.modelName + ",";
		}
    });
}
/**
 * Auto change scroll position 
 */
function autoChangeScrollPosition(gridId, top, down) {
    //height of view port of datagrid
    var heightViewPort = jQuery("#" + gridId + " .k-grid-content").css("height");
    heightViewPort = parseInt(heightViewPort.replace("px", ""));
    //Current selected row object.
    var selectedRow = jQuery('tr[class="'+ jQuery("#" + gridId + " .k-state-selected").attr("class") + '"]');
    //Position of selected row base on current to top of view port.
    var selectedPosition = selectedRow.offset().top - jQuery("#" + gridId + " .k-grid-content").offset().top;
    
    //selected row upper of current view port.
    if (selectedPosition <= 26) {
        jQuery("#" + gridId + " .k-grid-content").scrollTop(jQuery("#" + gridId + " .k-grid-content").scrollTop() - jQuery("#" + gridId + " .k-grid-content").offset().top
                                                        + jQuery(selectedRow).offset().top - top );
    } else if (selectedPosition + 50 >= heightViewPort) { //selected row under of current view port.
        jQuery("#" + gridId + " .k-grid-content").scrollTop(jQuery("#" + gridId + " .k-grid-content").scrollTop() - jQuery("#" + gridId + " .k-grid-content").offset().top
                                                        + jQuery(selectedRow).offset().top - down);
    }
}
/**
 * Change selected row
 * @param isNext check next(true) or previous(false)
 * @param index
 * @param gridId
 */
function changeRow(isNext, index, gridId) {
	var newIdx = 0;
	var grid = $("#" + gridId).data("kendoGrid");
	//Next row
	if (isNext) {
		if (index == grid._data.length - 1) {
			newIdx = index;
		} else {
			newIdx = index + 1;
		}
	} else {
	    //Previous row
		if (index == 0 || index == -1) {
			newIdx = 0;
		} else {
			newIdx = index - 1;
		}
	}
	// remove curren select row
	var row = grid.tbody.find(">tr")[index];
	$(row).removeClass("k-state-selected").removeAttr("aria-selected");
	// get new selected row
	var row1 = grid.tbody.find(">tr")[newIdx];
	grid.select(row1);

  // Auto scroll follow selected row.
  if (gridId == "gridModelFit3") {
	  // set auto scroll for grid
	  // param is grid id and height scroll top, height scroll down
	  autoChangeScrollPosition(gridId, 40, 100);
  } else if (gridId == "gridModelFit4") {
	  // set auto scroll for grid
	  // param is grid id and height scroll top, height scroll down
	  autoChangeScrollPosition(gridId, 10, 10);
  } else if(gridId == "gridModelFit2") {
	  // set auto scroll for grid
	  // param is grid id and height scroll top, height scroll down
	  autoChangeScrollPosition(gridId, 40, 100);
  }
}

function loadVehicleDetail(rowSelector) {
    // reset value
    resetModelDetail();
    // set insert mode false.
    insertMode = false;
    var parentTR = $(rowSelector).parent();
    // Get value.
    var kendoRowMaker = parentTR.find(".kendoRowMaker").text();
    var kendoRowModel = parentTR.find(".kendoRowModel").text();
    var kendoRowStyle = parentTR.find(".kendoRowStyle").text();
    
    // get hidden field 
    var kendoRowSort = parentTR.find(".kendoRowSort").val();
    var kendoRowMakerFlg = parentTR.find(".kendoRowMakerFlg").val();
    var kendoRowModelFlg = parentTR.find(".kendoRowModelFlg").val();

    var modelFitInfo = $.format('[{0}] {1} {2}', kendoRowMaker, kendoRowModel, kendoRowStyle);

    // Set value model details
    $("#txtAllModelFitInfo").text(modelFitInfo);
    $("#hdSort").val(kendoRowSort);
    $("#txtMaker").val(kendoRowMaker);
    $("#txtModel").val(kendoRowModel);
    $("#txtStyle").val(kendoRowStyle);

    // set old value to variable 
    //using for compare old value before update on model fit change list (grid 2).
     oldTxtMaker = kendoRowMaker;
     oldTxtModel = kendoRowModel;
     oldTxtStyle = kendoRowStyle;
    
    // set color into model details
    if (kendoRowMakerFlg == "1") {
    	$("#txtMaker").addClass("yellow");
    }
    if (kendoRowModelFlg == "1") {
    	$("#txtModel").addClass("yellow");
    }
    
    // continue filter Grid3
    filterGrid3Common();
}

function resetModelDetail() {
	$("#txtAllModelFitInfo").text($("#hAddNew").val());
	$("#hdSort, #txtMaker, #txtModel, #txtStyle").val("");
	$("#txtMaker, #txtModel").removeClass("yellow");
}

function checkProductChange(productId, selectedIndex) {
  $.ajax({
    type: "POST",
    url: "ajaxCheckProductChange.html",
    dataType: "JSON",
    data: {
      "productId": productId,
    },
    beforeSend: function (data) {

    },
    success: function (data) {
      reloadGrid4Page(selectedIndex);
      if (dataSourceGrid4 != null) {
  		  bindProductAllData(selectedRodGrid4, dataSourceGrid4);
	  }
      hideProcessBar();
    },
    error: function (data) {
      //hideProcessBar();
    },
    complete: function (data) {
      isFromChangeRow = false;
    }
  });
}

function handleCheckbox(row) {
  var checkedAll = $("#selectAllCheckbox").prop("checked");
  if (checkedAll) {
    $("#selectAllCheckbox").prop("checked", false);
    deselectRow("#gridModelFit4 table tbody tr", ".productCheckbox", selectedKendoClass, selectedRowClass);
    $("#gridModelFit4 .k-grid-content .productCheckbox:checked").prop("checked", true);
  } else {
    $("#gridModelFit4 .k-grid-content .productCheckbox:checked").prop("checked", false);
  }
  row.find(".productCheckbox").prop("checked", true);
}

function getSelectedProductId() {
	// Get selected row data items.
    var grid4 = $("#gridModelFit4").data("kendoGrid");
    // get selected row
    var selectedRows = grid4.select();
    // get data from selected row
    var data = grid4.dataItem(selectedRows);
    // product id get from hotkey
    var hkProductId = data.productId;
    return hkProductId;
}

function getGrid4SelectedIndex() {
	// Get selected row data items.
    var grid4 = $("#gridModelFit4").data("kendoGrid");
    // get selected row
    var selectedIndex = grid4.select().index();
    return selectedIndex;
}
function openPopup(id) {
    var win = $("#" + id).data("kendoWindow");
    win.center();
    win.open();
}
////////////////EOE_Vu.Long//////////////////////