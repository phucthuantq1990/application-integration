//if switch page, isNewPage = true
var currentPage = 1;
var dataSource;

//Search condition
var categoryCode = "";
var categoryName = "";
var bunruiCode = "";
var bunruiName = "";
var categoryAttributeCount = "";
var categoryAttributeCountFrom = "";
var categoryAttributeCountTo = "";
var categoryDelFlg = "Yes";
var updatedOn = "";
var updatedFrom = "";
var updatedTo = "";

// Max length constants.
var NUMERIC_MIN = 0;
var CATEGORY_CODE_NUMERIC_MAX = 2147483647;
var BUNRUI_CODE_NUMERIC_MAX = 9999;
var CATEGORY_COUNT_NUMERIC_MAX = 2147483647;
var CATEGORY_NAME_LEN = 255;
var BUNRUI_NAME_LEN = 50;
var CATEGORY_CODE_LEN = 8;
var BUNRUI_CODE_LEN = 4;

//Load data for grid
function loadGridData() {
    var grid = $("#grid").kendoGrid({
        dataSource: dataSource,
        //sortable: true,
        sortable: {
            mode: "single",
            allowUnsort: false
        },
        pageable: {
            refresh: true,
            pageSizes: [50, 100],
            buttonCount: 5,
            messages: {
                display: jQuery("#pagingDisplay").val(),
                empty: jQuery("#pagingEmpty").val(),
                itemsPerPage: jQuery("#pagingItemsPerPage").val(),
                first: jQuery("#pagingFirst").val(),
                previous: jQuery("#pagingPrevious").val(),
                next: jQuery("#pagingNext").val(),
                last: jQuery("#pagingLast").val()
            }
        },
        columns: [{
            field: "categoryCode",
            type: "number",
            title: jQuery("#tbCategoryCode").val(),
            width: "65px",
            attributes: {
                "class": "align-right"
            }
        }, {
            field: "categoryName",
            type: "string",
            title: jQuery("#tbCategoryName").val(),
            width: "115px",
            attributes: {
                "class": "align-left"
            }
        }, {
            field: "bunruiCode",
            type: "number",
            title: jQuery("#tbBunruiCode").val(),
            width: "90px",
            attributes: {
                "class": "align-right"
            }
        }, {
            field: "bunruiName",
            type: "string",
            title: jQuery("#tbBunruiName").val(),
            width: "110px",
            attributes: {
                "class": "align-left"
            }
        }, {
            field: "categoryAttributeCount",
            type: "number",
            title: jQuery("#tbCategoryAttributeCount").val(),
            width: "110px",
            attributes: {
                "class": "align-center attributeCount"
            }
        }, {
            field: "categoryDelFlg",
            type: "boolean",
            title: jQuery("#tbCategoryDelFlg").val(),
            width: "62px",
            attributes: {
                "class": "align-center"
            }
        }, {
            field: "updatedOn",
            type: "date",
            title: jQuery("#tbUpdatedOn").val(),
            width: "188px",
            attributes: {
                "class": "align-center"
            }
        }],
        height: 540,
        selectable: true,
        resizable: true,
        dataBound: function (e) {
            //Handle event
            handleClickingRowsGridCategory();
            //Remove noData message
            jQuery(".dataTables_empty").remove();
            if (this.dataSource.total() > 0) {
                setCurrentSearchCondition();
                var page = this.dataSource.page();

                if (page != currentPage) {
                    currentPage = page;
                    this.content.scrollTop(0);
                }
                
                if (dataSource != undefined && dataSource.view() != undefined) {
                	
                	//Add title for row
                	var gridData = this.dataSource.view();
                	var gridLength = gridData.length;
                	for (var i = 0; i < gridLength; i++) {
                		var currentUid = gridData[i].uid;
                		
                		var currenRow = this.table.find("tr[data-uid='" + currentUid + "']");
                		$(currenRow).attr("title", jQuery("#hostAddress").val() + "/categoryEdit.html?insertMode=0&categoryCode=" + gridData[i].categoryCode);
                		//Check attributeCount to show as red text
                		if (gridData[i].categoryAttributeCount == '0') {
                			$(currenRow).find("td.attributeCount").addClass("attributeCountZero");
                		}                		
                	}
                }

            } else {
                //Show no data message
                jQuery(".k-grid-content").append('<div class="dataTables_empty">' + jQuery("#noDataMessage").val() + '</div>');
            }
        }
        /* BOE by Luong.Dai at 2014/04/11 change event to double click */
        /*,
        change: function (e) {
            var selectedRows = this.select();

            var categoryCodeTransfer = this.dataItem(selectedRows).get("categoryCode");

            // Redirect to edit brand page
            window.location = jQuery("#hostAddress").val() + "/categoryEdit.html?insertMode=0&categoryCode=" + categoryCodeTransfer;
        }*/
        /* EOE by Luong.Dai at 2014/04/11 change event to double click */
    });

    return grid;
}

/**
 * Handle double clicking on row in Grid.
 */
function handleClickingRowsGridCategory() {
    $("#grid .k-grid-content tr").on({
        dblclick: function(e) {
            var grid = $('#grid').data('kendoGrid');
            var selectedRows = grid.select();

            var categoryCodeTransfer = grid.dataItem(selectedRows).get("categoryCode");

            /* BOE by Luong.Dai at 2014/04/11 Show progress bar */
            showProcessBar();
            /* EOE by Luong.Dai at 2014/04/11 Show progress bar */
            // Redirect to edit brand page
            window.location = jQuery("#hostAddress").val() + "/categoryEdit.html?insertMode=0&categoryCode=" + categoryCodeTransfer;
        }
    });
};

// Get attribute codes for search categories.
function getAttrCodes() {
    var result = "";
    
    // Only create result if chooseList was initialized.
	var chooseList = $("#cboChooseList").data("kendoDropDownList");
	if(chooseList != undefined) {
		// Loop through the multiSelects which are checked.
	    $("#divShow .divContains .attrCodeGroupChk:checked").each(function () {
	        var parent = $(this).parent();
	        
	        // Get divContainId.
	        var divContainId = parent.attr("id");
	        
	        // Prepare multiSelect selector.
	        var multiSelectSelector = "#" + divContainId + " select.attrCodeGroupSelect";
	        
	        // Get attribute list of each multiSelect component.
	        var attrList = $(multiSelectSelector).data("kendoMultiSelect").value();
	        
	        // Append to result.
            var attrCount = attrList.length;
            for (var i = 0; i < attrCount; i++) {
                if (attrList[i] != "") {
                    result += attrList[i] + ",";
                }
            }
	    });
	}

	// Remove the last comma.
    if (result.length > 0)
        result = result.substr(0, result.length - 1);
    
    // Return result string.
    return result;
};
//Set data filter condition to sent to server
function setParamsForDataSource(sortField, sortDir) {
    return {
        "categoryManageActionForm.entMstCategory.categoryCode": categoryCode,
        "categoryManageActionForm.entMstCategory.categoryName": categoryName,
        "categoryManageActionForm.entMstCategory.bunruiCode": bunruiCode,
        "categoryManageActionForm.entMstCategory.bunruiName": bunruiName,
        "categoryManageActionForm.entMstCategory.categoryAttributeCount": categoryAttributeCount,
        "categoryManageActionForm.entMstCategory.categoryAttributeCountFrom": categoryAttributeCountFrom,
        "categoryManageActionForm.entMstCategory.categoryAttributeCountTo": categoryAttributeCountTo,
        "categoryManageActionForm.entMstCategory.categoryDelFlg": categoryDelFlg,
        "categoryManageActionForm.entMstCategory.updatedFrom": updatedFrom,
        "categoryManageActionForm.entMstCategory.updatedTo": updatedTo,
        "categoryManageActionForm.entMstCategory.attributeCodes": getAttrCodes(),
        "categoryManageActionForm.entMstCategory.sortField" : sortField,
        "categoryManageActionForm.entMstCategory.sortDir": sortDir
    };
}

//Get filter condition from filter bar
function getSearchCondition() {
    categoryCode = $("#categoryCode").val();
    categoryName = $("#categoryName").val();
    bunruiCode = $("#bunruiCode").val();
    bunruiName = $("#bunruiName").val();
    categoryAttributeCountFrom = $("#categoryAttributeCountFrom").val();
    categoryAttributeCountTo = $("#categoryAttributeCountTo").val();
    categoryDelFlg = $("#categoryDelFlg").val();
    updatedFrom = $("#updatedFrom").val();
    updatedTo = $("#updatedTo").val();
}

//set search condition to filter bar
function setCurrentSearchCondition() {
    $("#categoryCode").val(categoryCode);
    $("#categoryName").val(categoryName);
    $("#bunruiCode").val(bunruiCode);
    $("#bunruiName").val(bunruiName);
    $("#categoryAttributeCountFrom").val(categoryAttributeCountFrom);
    $("#categoryAttributeCountTo").val(categoryAttributeCountTo);
    //If delFlg != null
    if (categoryDelFlg) {
        jQuery("#categoryDelFlg").val(categoryDelFlg);
    } else {
        //DelFlg = null: Set value is All
        jQuery("#categoryDelFlg").val(" ");
    }
    jQuery("#updatedFrom").val(updatedFrom);
    jQuery("#updatedTo").val(updatedTo);
}

// remove attr list no checked
function removeAttributeNotChecked() {
    $(".attrCodeGroupChk:not(:checked)").each(function () {
        var parent = $(this).parent();
        
        // remove div.
        parent.remove();
    });
}

// reset attribute list
function resetAttrList() {
    // remove divContains
    $("#divShow .divContains").remove();
    // enable all option disabled.
    $('#cboChooseList option').prop("disabled", false);
    // show search header
    
    // Refresh dropDown to remove the disabled attributes.
    var chooseList = $("#cboChooseList").data("kendoDropDownList");
    chooseList.refresh();

    var currentVal = jQuery("#btnSearchHeader");
    if (currentVal.hasClass("rightToggleBtn")) {
        jQuery("#searchOptionDiv").slideToggle("fast");
        currentVal.removeClass("rightToggleBtn");
        currentVal.addClass("downToggleBtn");
    }
}

//Reset all filter
function resetSearchCondition() {
    //clear all textbox
    jQuery("input.tbSearch").val('');

    //reset status list
    setSelectedValue("Yes");
    $('#categoryDelFlg').kendoDropDownList();

    // reset attributes list
    resetAttrList();

}

//Set selected for list status
//Value = 0: Yes
//Value = 1: No
function setSelectedValue(value) {
    $("select option").filter(function () {
        //may want to use $.trim in here
        return $(this).text() == value;
    }).prop('selected', true);
}

//Validate date.
function validateTextboxKousinDate(idValue) {
	if ($("#" + idValue).val() == "") {
        jQuery("#" + idValue).removeClass("error");
        return true;
	}
	$("#" + idValue).val(formatDateTimeWithSlash($("#" + idValue).val()));
    if (!validateDateTime(jQuery("#" + idValue).val())) {
        //Invalid date
        jQuery("#" + idValue).addClass("error");
        return false;
    } else {
        //Valid date
        jQuery("#" + idValue).removeClass("error");
        return true;
    }
}
//Validate date range 
function validateKousinDateRange() {
    var result = true;
    var fromDate = $("#updatedFrom").val();
    var toDate = $("#updatedTo").val();
    if (undefined != fromDate && "" != fromDate && undefined != toDate && "" != toDate) {
        var dateFrom = new Date($("#updatedFrom").val());
        var dateTo = new Date($("#updatedTo").val());
        if (dateFrom > dateTo) {
            result = false;
            $("#updatedFrom, #updatedTo").addClass("error");
        }
    }
    return result;
}
//Validate date
function validateKousinDate() {
    var result = true;
    if (!validateTextboxKousinDate("updatedFrom")) {
        result = false;
    }
    if (!validateTextboxKousinDate("updatedTo")) {
        result = false;
    }
    if (!result) {
        return false;
    }
    result = validateKousinDateRange();
    return result;
}
// validate date attribute-count range
function validateCateAttrCountRange() {
    var result = true;
    var fromCount = $("#categoryAttributeCountFrom").val();
    var toCount = $("#categoryAttributeCountTo").val();
    if (undefined != fromCount && "" != fromCount && undefined != toCount && "" != toCount) {
        if (parseInt(fromCount) > parseInt(toCount)) {
            result = false;
            $("#categoryAttributeCountFrom, #categoryAttributeCountTo").addClass("error");
        }
    }
    return result;
}

//validate date attribute-count
function validateCateAttrCount() {
    var result = true;
    $("#categoryAttributeCountFrom, #categoryAttributeCountTo").each(function () {
        if (!validateNumbericAndBlank($(this).val())) {
            result = false;
            $(this).addClass("error");
        } else {
            $(this).removeClass("error");
        }
    });
    if (!result) {
        return false;
    }
    result = validateCateAttrCountRange();

    return result;
}

jQuery(document).ready(function () {
	//Filter bar
	var filterRow = $(
			'<tr>'
				+ '<td><input class="tbSearch k-input" type="text" id="categoryCode" maxlength="' + CATEGORY_CODE_LEN + '" /></td>'
				+ '<td><input class="tbSearch k-textbox" type="text" id="categoryName" maxlength="' + CATEGORY_NAME_LEN + '"  /></td>'
				+ '<td><input class="tbSearch k-input" type="text" id="bunruiCode" maxlength="' + BUNRUI_CODE_LEN + '" /></td>'
				+ '<td><input class="tbSearch k-textbox" type="text" id="bunruiName" maxlength="' + BUNRUI_NAME_LEN + '" /></td>'
				
				/*
				+ '<td>'
					+ '<input class="tbSearch haft-size k-input" type="text" id="categoryAttributeCountFrom"/>'
					+ ' ~ <input class="tbSearch haft-size k-input" type="text" id="categoryAttributeCountTo"/>'
				+ '</td>'
				*/
				
				+ "<td>"
				+ "<div>"
				+ 	"<div>"
				+ 		'<span class="dateTimeLabel">From</span>'
				+ 		'<span>'
				+ 			'<input id="categoryAttributeCountFrom" type="text" class="tbSearch k-input filterTextBox" maxlength="8" style="width: 90px;" />'
				+ 		'</span>'
				+ 	"</div>"
				+ 	"<div>"
				+ 		'<span class="dateTimeLabel">To</span>'
				+ 		'<span>'
				+ 			'<input id="categoryAttributeCountTo" type="text" class="tbSearch k-input filterTextBox" maxlength="8" style="width: 90px;" />'
				+ 		'</span>'
				+ 	"<div>"
				+ "</div>"
				+ "</td>"
				
				+ '<td><select class="tbSearch" id="categoryDelFlg" style="width: 90%;">'
				+ '<option value=" ">All</option>'
				+ '<option value="Yes">Yes</option>'
				+ '<option value="No">No</option>'
				+ '</select></td>'
				
				/*
				+ '<td>'
					+ '<input class="tbSearch haft-size k-input" type="text" id="updatedFrom"/>'
					+ ' ~ <input class="tbSearch haft-size k-input" type="text" id="updatedTo"/>'
				+ '</td>'
				*/
				
				+ "<td>"
				+ "<div>"
				+ 	"<div>"
				+ 		'<span class="dateTimeLabel">From</span>'
				+ 		'<span>'
				+ 			'<input id="updatedFrom" type="text" class="tbSearch haft-size filter" tabindex="8" style="width: 168px;" />'
				+ 		'</span>'
				+ 	"</div>"
				+ 	"<div>"
				+ 		'<span class="dateTimeLabel">To</span>'
				+ 		'<span>'
				+ 			'<input id="updatedTo" type="text" class="tbSearch haft-size filter" tabindex="9" style="width: 168px;" />'
				+ 		'</span>'
				+ 	"<div>"
				+ "</div>"
				+ "</td>"
				
			+ '</tr>');
	//Back to the previous screen
	//If doesn't exists, go to Top page
	function backToPreviousPage() {
		//BOE THAI.SON FIX BUG "BACK TO SIDE MAP"
	    /*//Get previous URL
	    var previousUrl = document.referrer;
	    //Validate previousUrl
	    if ("" != previousUrl) {
	        //Go to previous page
	        window.location = previousUrl;
	        return;
	    }*/
	    //Go to Top page
		/* BOE by Luong.Dai at 2014/04/08 Show progress bar */
		showProcessBar();
		/* EOE by Luong.Dai at 2014/04/08 Show progress bar */
	    window.location = defaultRedirectPage;
	  //eOE THAI.SON FIX BUG "BACK TO SIDE MAP"
	}
	
    //dataSource for brand master grid
    dataSource = new kendo.data.DataSource({
        serverPaging: true,
        serverSorting: true,
        autoBind: false,
        // 50 product in 1 page
        pageSize: 50,
        transport: {
            read: {
                type: "POST",
                dataType: "json",
                url: "ajaxGetCategoryList.html",
                data: function (data) {
                	var sortField = "categoryCode";
                	var sortDir = "asc";
                	if (data != undefined && data.sort != undefined && data.sort.length > 0) {
                		sortField = data.sort[0].field;
                		sortDir = data.sort[0].dir;
                	}
                    return setParamsForDataSource(sortField, sortDir);
                },
                beforeSend :function() {
                    // show process bar before send request.
                	showProcessBar();
                },
                complete: function() {
                    // hide process bar that the request succeeded
                	hideProcessBar();
                },
	            error: function() {
	                // hide process bar that the request failed
	            	hideProcessBar();
	            },
                cache: false,
            }
        },
        schema: {
            data: "categoryManageActionForm.entMstCategoryList",
            total: "categoryManageActionForm.count"
        }
    });

    var grid = loadGridData();

    //Add filter
    grid.data("kendoGrid").thead.append(filterRow);
    //Set default DelFlg = Yes
    setSelectedValue("Yes");
    /*BOE  Thai.Son 2014/01/17*/
    $('#categoryDelFlg').kendoDropDownList();
    /*EOE  Thai.Son 2014/01/17*/
    //Click button Back
    jQuery("#btnBack").click(function () {
        backToPreviousPage();
    });

    //Click button reset
    jQuery("#btnResetFilter").click(function () {
        resetSearchCondition();
        jQuery("#btnSearch").click();
    });
    //Button Search click
    jQuery("#btnSearch").click(function () {
    	// Reset message.
    	
    	$("#content #message").remove();
        var valid = true;
        if (!validateKousinDate()) {
            valid = false;
        }
        if (!validateCateAttrCount()) {
            valid = false;
        }

        if (valid) {
            // remove divContains of checkbox not checked.
            removeAttributeNotChecked();

            getSearchCondition();
            //Reload data and goto first page
            grid.data("kendoGrid").dataSource.page(1);

            //Go to top of grid
            grid.data("kendoGrid").content.scrollTop(0);
        }
        
        // Refresh dropDown to remove the disabled attributes.
        var chooseList = $("#cboChooseList").data("kendoDropDownList");
        chooseList.refresh();
    });
    //Press enter key in textbox filter
    jQuery(document).on("keypress", "input.tbSearch", function (e) {
        if (e.keyCode == 13) {
            jQuery("#btnSearch").click();
        }
    });
    //Change DelFlg value
    jQuery("#categoryDelFlg").change(function () {
        jQuery("#btnSearch").click();
    });

    jQuery('#btnSearchHeader').click(function () {
        jQuery("#searchOptionDiv").slideToggle("fast");
        var currentVal = jQuery("#btnSearchHeader");
        if (currentVal.hasClass("rightToggleBtn")) {
            currentVal.removeClass("rightToggleBtn");
            currentVal.addClass("downToggleBtn");
        } else {
            currentVal.removeClass("downToggleBtn");
            currentVal.addClass("rightToggleBtn");
        }
    });

    // Clone attribute-search-condition div template.
    function cloneBelongAttrDiv(value, text) {
    	// Clone from template and append to divShow.
        $("#divTemplate .divContains:first-child").clone().appendTo("#divShow");
        var newDivContains = $("#divShow .divContains:last-child");
        
        // Prepare divContain id.
        var divContainId = newDivContains.attr("id") + value;
        
        // Change its id and text.
        newDivContains.attr("id", divContainId);
        newDivContains.find(".clsAttrName").text(text);
        
        // Init attribute dropDownList.
        // NOTE: showListbox processing won't work!!!
//        var dropDownListSelector = "#" + divContainId + " .attrCodeGroupSelect";
//        $(dropDownListSelector).kendoDropDownList();
        
        // Init attribute multiSelect.
        var multiSelector = "#" + divContainId + " select.attrCodeGroupSelect";
        $(multiSelector).kendoMultiSelect({
        	maxSelectedItems: 10 //only three or less items could be selected
        });
        // Select nothing.
        $(multiSelector).data("kendoMultiSelect").value("");
        
        // change combobox to listbox event.
        /*
        $(".showListbox").click(function (e) {
            e.preventDefault();
            var parent = $(this).parent();
            if (parent.find(".attrCodeGroupSelect").attr("multiple")) {
                parent.find(".attrCodeGroupSelect").attr("multiple", false);
            } else {
                parent.find(".attrCodeGroupSelect").attr("multiple", true);
            }
        });*/
        
        // hide all information of attribute when un-check.
        $(".attrCodeGroupChk").change(function (e) {
            var parent = $(this).parent();
            if ($(this).is(":checked")) {
                parent.find(".showHidePanel").show();
            } else {
                parent.find(".showHidePanel").hide();
            }
        });
        
        /* Intending to mimic Kendo dropDownList.
        $(".attrCodeGroupSelect").hover(
	        function(e) {
	        	var isMulti = $(this).prop("multiple");
	        	if(!isMulti) {
	        		$(this).css('background-color', '#8ebc00');	
	        	}
	        },
	        function(e) {
	        	$(this).css('background-color', 'inherit');
	        }
        ).click(function(){
        	$(this).css('background-color', 'inherit');
        });
        */
    }
    
    // Clone  attribute-search-condition div template when chose attribute.
    /*
    $("#cboChooseList").change(function (e) {
        if ($(this).val() != "") {
            cloneBelongAttrDiv($(this).val(), $(this).find('option:selected').text());
        }
    });*/
    
    // Make it Kendo.
    $("#cboChooseList").kendoDropDownList({
    	select: function(e) {
    		// Disable if it is not default value.
    		if(e.item.text() != "") {
        		// Disable selected item.
        		$(e.item).prop("disabled", true);
        		$(e.item).addClass("k-state-disabled");
    		}
    	},
    	change: function(e) {
    		// Get selected index.
    		var index = e.sender.select();
    		
    		// Get selected item.
    		var selectedItem = e.sender.dataItem(index);
    		
    		// Clone layout.
            if (this.value() != "") {
                cloneBelongAttrDiv(this.value(), selectedItem.text);
            }
            
            // Reset to default value.
            e.sender.value("");
            
            // Remove focus.
            $("#cboChooseList").blur();
    	}
    	
    });

    // validate Date
    /*
    $("#updatedFrom, #updatedTo").focusout(function () {
        validateKousinDate();
    });
    */

    $("#categoryAttributeCountFrom, #categoryAttributeCountTo").focusout(function () {
        validateCateAttrCount();
    });
    
    function initDateTimePicker() {
        $("#updatedFrom").kendoDateTimePicker({
            interval: 60,
            format: "yyyy/MM/dd HH:mm:ss",
            timeFormat: "HH:mm:ss",
        });
        
        $("#updatedTo").kendoDateTimePicker({
            interval: 60,
            format: "yyyy/MM/dd HH:mm:ss",
            timeFormat: "HH:mm:ss",
        });
    }
    
    function initCategoryCodeNumeric() {
    	$("#categoryCode").kendoNumericTextBox({
    		value : "",
    		format: "#",
            decimals: 0,
    		min : NUMERIC_MIN,
    		max : CATEGORY_CODE_NUMERIC_MAX,
    		step : 1
    	});
    }
    
    function initBunruiCodeNumeric() {
    	$("#bunruiCode").kendoNumericTextBox({
    		value : "",
    		format: "#",
            decimals: 0,
    		min : NUMERIC_MIN,
    		max: BUNRUI_CODE_NUMERIC_MAX,
    		step : 1
    	});
    }
    
    function initAttributeCountFrom() {
    	jQuery("#categoryAttributeCountFrom").kendoNumericTextBox({
    		value : "",
    		format: "#",
            decimals: 0,
    		min : NUMERIC_MIN,
    		max : CATEGORY_COUNT_NUMERIC_MAX,
    		step : 1
    	});
    }
    
    function initAttributeCountTo() {
    	jQuery("#categoryAttributeCountTo").kendoNumericTextBox({
    		value : "",
    		format: "#",
            decimals: 0,
    		min : NUMERIC_MIN,
    		max : CATEGORY_COUNT_NUMERIC_MAX,
    		step : 1
    	});
    }
    
    function handleDateTimePicker() {
        // Handle focusout event.
        $("#updatedFrom").focusout(function() {
        	validateDateTimePicker('#updatedFrom', '#updatedFrom', '#updatedTo');
        });
        $("#updatedTo").focusout(function() {
        	validateDateTimePicker('#updatedTo', '#updatedFrom', '#updatedTo');
        });
    }
    
    function handleCreateButton() {
    	$("#createCategory").click(function(){
    		window.location = "categoryEdit.html";
    	});
    }
    
    initCategoryCodeNumeric();
    initBunruiCodeNumeric();
    initAttributeCountFrom();
    initAttributeCountTo();
    initDateTimePicker();
    handleDateTimePicker();
    handleCreateButton();
});