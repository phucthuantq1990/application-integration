/* BOE hoang.ho 2014/02/26 */
//column checkbox of grid:
var sortFieldCSV = "";
var sortDirCSV = "";
var selectedKendoClass = "k-alt k-state-selected";
var selectedRowClass = "selectedGridRow";
var MAX_PRODUCT_OF_MATTER = 50000;
var totalProductOfMatter = 0;
//------variables for bottom grid:
var closeBtnFlg;
//var dataSourceCSVFitModel;
var sortResetFlg = 1;
var ROLE;
var limitRole 		= "LIMITED_USER_ID";
var generalRole 	= "MASTER_SHOW_ROLE_ID";
var matterRole 		= "MASTER_EDIT_ROLE_ID";

//BOE  @rcv!Tran.THanh 2014/07/10 # : Add global variable to check error import csv
var showToolTips = false;
var importValid = false;
//EOE  @rcv!Tran.THanh 2014/07/10 # : Add global variable to check error import csv

//BOE Tran.Thanh Fixbug muti init image browser 2014/03/31
var hadInitImageBrowser = false; // true if had been init
//EOE Tran.Thanh Fixbug muti init image browser 2014/03/31

function clearMsgText() {
	// remove all class of msg div and clear msg
	$("#message").removeClass("has-message success");
	$("#message").removeClass("has-message msg-error");
	$("#message").text("");
}
//function setSearchConditionsForCSVFitModel(sortField, sortDir) {
//    return {
//        "matterDetailActionForm.entCSVProductFitModel.productId" :      $("#csvProductId").val(),
//        "matterDetailActionForm.entCSVProductFitModel.productSyouhinSysCode" :           $("#csvProductSyouhinSysCode").val(),
//        "matterDetailActionForm.entCSVProductFitModel.fitModelMaker" :    $("#csvFitModelMaker").val(),
//        "matterDetailActionForm.entCSVProductFitModel.fitModelModel" :    $("#csvFitModelModel").val(),
//        "matterDetailActionForm.entCSVProductFitModel.fitModelStyle" :    $("#csvFitModelStyle").val(),
//        "matterDetailActionForm.entCSVProductFitModel.sortField"      :   sortField,
//        "matterDetailActionForm.entCSVProductFitModel.sortDir"      :   sortDir
//    };
//};
//function initGridCSVFitModel() {
//	
//	var filterRow = $(
//			'<tr>'
//				+ '<td></td>'
//				+ '<td><input class="tbSearch k-input" type="text" class="filter" tabindex="1"/></td>'
//				+ '<td><input class="tbSearch k-input" type="text" class="filter" id="csvProductId" tabindex="2"/></td>'
//				+ '<td><input class="tbSearch k-input" type="text" class="filter" id="csvProductSyouhinSysCode" tabindex="3"/></td>'
//				+ '<td><input class="tbSearch k-input" type="text" class="filter" id="csvFitModelMaker" tabindex="4"/></td>'
//				+ '<td><input class="tbSearch k-input" type="text" class="filter" id="csvFitModelModel" tabindex="5"/></td>'
//				+ '<td><input class="tbSearch k-input" type="text" class="filter" id="csvFitModelStyle" tabindex="6"/></td>'
//			+ '</tr>');
//	dataSourceCSVFitModel = new kendo.data.DataSource({
//        serverPaging: true,
//        autoBind: false,
//        serverSorting: true,
//        // 50 product in 1 page
//        pageSize: 50,
//        transport: {
//            read: {
//                type: "POST",
//                dataType: "json",
//                url: "ajaxLoadProductFitModelList.html",
//                data:  function (data) {
//                    var sortField = "productId";
//                    var sortDir = "asc";
//                    if (sortResetFlg != 1) {
//                        if (data != undefined && data.sort != undefined && data.sort.length > 0) {
//                            sortField = data.sort[0].field;
//                            sortDir = data.sort[0].dir;
//                        }
//                    } else {
//                        if (data != undefined && data.sort != undefined && data.sort.length > 0) {
//                            data.sort[0].field = '';
//                            data.sort[0].dir = '';
//                        }
//                    }
//                    return setSearchConditionsForCSVFitModel(sortField, sortDir);
//                },
//                beforeSend :function() {
//                	clearMsgText();
//                },
//                cache: false,
//            }
//        },
//        schema: {
//            data: "matterDetailActionForm.entCSVProductFitModelList",
//            total: "matterDetailActionForm.entCSVProductFitModelCount"
//        }
//    });
//	//end datasource.
//		gridCSVFitModel = $("#gridCSVFitModel").kendoGrid({
//		dataSource: dataSourceCSVFitModel,
//        sortable: {
//            mode: "single",
//            allowUnsort: false
//        },
//	    pageable: {
//	        refresh: true,
//	        pageSizes: [50, 100],
//	        buttonCount: 5,
//	        messages: {
//	          display:         jQuery("#pagingDisplay").val(),
//	          empty:           jQuery("#pagingEmpty").val(),
//	          itemsPerPage:    jQuery("#pagingItemsPerPage").val(),
//	          first:           jQuery("#pagingFirst").val(),
//	          previous:        jQuery("#pagingPrevious").val(),
//	          next:            jQuery("#pagingNext").val(),
//	          last:            jQuery("#pagingLast").val()
//	        }
//	    },
//	    columns: [
//				{
//					title : "<input type='checkbox' id='csvSelectAllCheckbox'/>",
//					template : "<input type='checkbox' name='csvModelCheckbox' " +
//							" class='csvModelCheckbox' id='#= productId #'  />",
//					attributes : {
//						"class" : "align-center"
//					},
//					width : "30px"
//				},
//				{ 
//			          field:"importMode",
//			          title: $("#hdImportMode").val(),
//			          width: "120px",
//			          attributes: {"class": "align-left, row_table"}
//			    },
//				{ 
//			          field:"productId",
//			          template: function(myData){ return 'P'+myData.productId;},
//			          title: $("#hdIdentificationNumber").val(),
//			          width: "120px",
//			          attributes: {"class": "align-left, row_table"}
//			    },
//				{ 
//			          field:"productSyouhinSysCode",
//			          title: $("#hdSystemProductCode").val(),
//			          width: "120px",
//			          attributes: {"class": "align-left, row_table"}
//			    },
//				{ 
//			          field:"fitModelMaker",
//			          title: $("#hdMaker").val(),
//			          width: "120px",
//			          attributes: {"class": "align-left, row_table"}
//			    },
//				{ 
//			          field:"fitModelModel",
//			          title: $("#hdCarModel").val(),
//			          width: "120px",
//			          attributes: {"class": "align-left, row_table"}
//			    },
//				{ 
//			          field:"fitModelStyle",
//			          title: $("#hdBikeModel").val(),
//			          width: "120px",
//			          attributes: {"class": "align-left, row_table"}
//			    }
//	  	        ],
//        height: 240,
//        selectable: "multiple, row",
//	    resizable : true,
//	    dataBound: function(e){
//	    	// Remove noData message
//			jQuery("#gridCSVFitModel .dataTables_empty").remove();
//			if (this.dataSource.total() > 0) {
//				var grid = $("#gridCSVFitModel").data("kendoGrid");
//				// Add title for row
//				var gridData = this.dataSource.view();
//				for ( var i = 0; i < gridData.length; i++) {
//					var currentUid = gridData[i].uid;
//					var currenRow = grid.table.find("tr[data-uid='" + currentUid + "']");
//					// $(currenRow).removeClass("k-alt");
//					if (gridData[i].productModelCheckFlg == 1) {
//						// Row checked
//					    $(currenRow).removeClass("k-alt");
//						$(currenRow).addClass("rowGridChecked");
//					}
//					if (gridData[i].productModelErrorFlg == 1) {
//                        // Row error
//					    $(currenRow).removeClass("k-alt");
//					    $(currenRow).removeClass("rowGridChecked");
//                        $(currenRow).addClass("rowGridError");
//                    }
//				}
//			} else {
//				// Show no data message
//				jQuery("#gridCSVFitModel .k-grid-content").append(
//						'<div class="dataTables_empty">'
//								+ jQuery("#noDataMessage")
//										.val() + '</div>');
//			}
//			// Select all processing.
//			handleSelectAll("#csvSelectAllCheckbox", "#gridCSVFitModel table tbody tr", ".csvModelCheckbox"
//					, selectedKendoClass, selectedRowClass);
//	    },
//	    change: function(data) {
//		},
//    });
//	//Add filter
//	gridCSVFitModel.data("kendoGrid").thead.append(filterRow);
//}
	
function clickButtonSearch(){
	$('#csvSelectAllCheckbox').attr('checked',false);
        getSearchCondition();
        //Reload data and goto first page
        gridCSVFitModel.data("kendoGrid").dataSource.page(1);
        //Go to top of grid
        gridCSVFitModel.data("kendoGrid").content.scrollTop(0);
}
/* EOE hoang.ho 2014/02/26 */

/**
* Reset control for popup csv to init
* @author 	SonTT
*/
function resetWindowCSV(){
	//reset tab Export:
	$("#csvmode").data("kendoDropDownList").value(ALL_IMPORTMODE);

	//Default checked all checkbox
	$('#exportItems :checkbox').attr({"disabled": false,"checked": true});
	//reset combobox mode export
	$("#csv_export_attr").data("kendoMultiSelect").enable(true);
	//reset Multiselect:
	$("#csv_export_attr").data("kendoMultiSelect").value("");
	$('#csv_ul_mod_1 .chkDisabled').attr({"disabled": true, "checked": true});
	//--------------reset tab Import:
	//BOE #7206 Nguyen.Chuong 2014/5/22 remove change default option to fitModel mode
	//$("#importMode").data("kendoDropDownList").value(ALL_IMPORTMODE);
	//$("#importMode").data("kendoDropDownList").value(COMPATIBLE_MODEL_IMPORTMODE);
	//EOE #7206 Nguyen.Chuong 2014/5/22 remove change default option to fitModel mode
	//Hide grid temp data mode 1
	$('#gridMode1').hide();
	//Hide grid temp data mode 2
    $('#gridCSVFitModel').hide();
    //Hide grid temp data mode 3
    $('#gridAttribute').hide();
    //Show field option for csv file
	$('#csvfieldsetImport').show();
	//Reset option data
	$("#importComboboxFieldDelimiter").data("kendoDropDownList").value(",");
	$('#boundedSymbolTxt').val("");
	//Enable combobox mode import
	$("#importMode").data("kendoDropDownList").enable(true);
	// reset import mode.res
	$("#importCountStatus").hide();
	//Change button import to default
	changeCSVImportButton(0);
	//Reset tooltip import status
	var tooltips = $("#importCountStatus").data("kendoTooltip");
    if (tooltips != null && tooltips != undefined) {
    	tooltips.destroy();
    }
	// hide progress bar
    var pb = $("#progressBar").data("kendoProgressBar");
    pb.value(false);
    $("#progressBar").hide();
	//click the first tab:
    $("#tabstrip").data("kendoTabStrip").select(0);

	//BOE @rcv! Luong.Dai 2014/07/31: Show default export all
    var id_ul_mod = "#csv_ul_mod_" + $("#csvmode").val();
    $(id_ul_mod).css('display','');
    $(id_ul_mod + ' :checkbox').attr({"disabled": false});
    $(id_ul_mod + ' .chkDisabled').attr({"disabled": true, "checked": true});
    $("#field_csv_export_attr").css('display','');
    $("#csv_export_attr_taglist").css('display','');                    
    $("#csv_export_attr").data("kendoMultiSelect").enable(true);
    //EOE @rcv! Luong.Dai 2014/07/31: Show default export all
}
/////////////////////////////////////--------------end reset Window

//column checkbox of grid:
function mode1_handleClickingRows() {

}
//------variables for bottom grid:
var gridMode1;
//var currentPage = 1;
var mode1_dataSource;
//70 fields auto-gen code:
var mode1_tbupdateMode = "";
var mode1_tbproductId = "";
var mode1_tbproductSyouhinSysCode = "";
var mode1_tbproductBrandCode = "";
var mode1_tbproductCategoryCode = "";
var mode1_tbproductName = "";
var mode1_tbproductCode = "";
var mode1_tbproductWebikeCodeFlg = "";
var mode1_tbproductEanCode = "";
var mode1_tbproductProperPrice = "";
var mode1_tbsupplierPricePrice = "";
var mode1_tbproductGroupCode = "";
var mode1_tbproductSupplierReleaseDate = "";
var mode1_tbproductOpenPriceFlg = "";
var mode1_tbproductProperSellingFlg = "";
var mode1_tbproductOrderProductFlg = "";
var mode1_tbproductNoReturnableFlg = "";
var mode1_tbproductAmbiguousImageFlg = "";
var mode1_tbdescriptionSummary = "";
var mode1_tbdescriptionRemarks = "";
var mode1_tbdescriptionCaution = "";
var mode1_tbdescriptionSentence = "";
var mode1_tbsiireCode1 = "";
var mode1_tbnoukiCode1 = "";
var mode1_tbsiireCode2 = "";
var mode1_tbnoukiCode2 = "";
var mode1_tbcompatibleModel = "";
var mode1_tbproductImageThumbnailPath1 = "";
var mode1_tbproductImageDetailPath1 = "";
var mode1_tbproductImageThumbnailPath2 = "";
var mode1_tbproductImageDetailPath2 = "";
var mode1_tbproductImageThumbnailPath3 = "";
var mode1_tbproductImageDetailPath3 = "";
var mode1_tbproductImageThumbnailPath4 = "";
var mode1_tbproductImageDetailPath4 = "";
var mode1_tbproductImageThumbnailPath5 = "";
var mode1_tbproductImageDetailPath5 = "";
var mode1_tbproductImageThumbnailPath6 = "";
var mode1_tbproductImageDetailPath6 = "";
var mode1_tbproductImageThumbnailPath7 = "";
var mode1_tbproductImageDetailPath7 = "";
var mode1_tbproductImageThumbnailPath8 = "";
var mode1_tbproductImageDetailPath8 = "";
var mode1_tbproductImageThumbnailPath9 = "";
var mode1_tbproductImageDetailPath9 = "";
var mode1_tbproductImageThumbnailPath10 = "";
var mode1_tbproductImageDetailPath10 = "";
var mode1_tbselectCode1 = 0;
var mode1_tbselectName1 = "";
var mode1_tbselectCode2 = 0;
var mode1_tbselectName2 = "";
var mode1_tbselectCode3 = 0;
var mode1_tbselectName3 = "";
var mode1_tbselectCode4 = 0;
var mode1_tbselectName4 = "";
var mode1_tbselectCode5 = 0;
var mode1_tbselectName5 = "";
var mode1_tbselectCode6 = 0;
var mode1_tbselectName6 = "";
var mode1_tbselectCode7 = 0;
var mode1_tbselectName7 = "";
var mode1_tbselectCode8 = 0;
var mode1_tbselectName8 = "";
var mode1_tbselectCode9 = 0;
var mode1_tbselectName9 = "";
var mode1_tbselectCode10 = 0;
var mode1_tbselectName10 = "";
var mode1_tbcustomersConfirmationItem = "";
var mode1_tblink = "";
var mode1_tbanimation = "";
//end 70 fields
var mode1_sortResetFlg = 0;

/**
* resert filter condtion
* @AUTHOR  Luong.Dai
*/
function resetFilterConditionPopup() {
	//Reset mode 1
	mode1_tbupdateMode = "";
	mode1_tbproductId = "";
	mode1_tbproductSyouhinSysCode = "";
	mode1_tbproductBrandCode = "";
	mode1_tbproductCategoryCode = "";
	mode1_tbproductName = "";
	mode1_tbproductCode = "";
	mode1_tbproductWebikeCodeFlg = "";
	mode1_tbproductEanCode = "";
	mode1_tbproductProperPrice = "";
	mode1_tbsupplierPricePrice = "";
	mode1_tbproductGroupCode = "";
	mode1_tbproductSupplierReleaseDate = "";
	mode1_tbproductOpenPriceFlg = "";
	mode1_tbproductProperSellingFlg = "";
	mode1_tbproductOrderProductFlg = "";
	mode1_tbproductNoReturnableFlg = "";
	mode1_tbproductAmbiguousImageFlg = "";
	mode1_tbdescriptionSummary = "";
	mode1_tbdescriptionRemarks = "";
	mode1_tbdescriptionCaution = "";
	mode1_tbdescriptionSentence = "";
	mode1_tbsiireCode1 = "";
	mode1_tbnoukiCode1 = "";
	mode1_tbsiireCode2 = "";
	mode1_tbnoukiCode2 = "";
	mode1_tbcompatibleModel = "";
	mode1_tbproductImageThumbnailPath1 = "";
	mode1_tbproductImageDetailPath1 = "";
	mode1_tbproductImageThumbnailPath2 = "";
	mode1_tbproductImageDetailPath2 = "";
	mode1_tbproductImageThumbnailPath3 = "";
	mode1_tbproductImageDetailPath3 = "";
	mode1_tbproductImageThumbnailPath4 = "";
	mode1_tbproductImageDetailPath4 = "";
	mode1_tbproductImageThumbnailPath5 = "";
	mode1_tbproductImageDetailPath5 = "";
	mode1_tbproductImageThumbnailPath6 = "";
	mode1_tbproductImageDetailPath6 = "";
	mode1_tbproductImageThumbnailPath7 = "";
	mode1_tbproductImageDetailPath7 = "";
	mode1_tbproductImageThumbnailPath8 = "";
	mode1_tbproductImageDetailPath8 = "";
	mode1_tbproductImageThumbnailPath9 = "";
	mode1_tbproductImageDetailPath9 = "";
	mode1_tbproductImageThumbnailPath10 = "";
	mode1_tbproductImageDetailPath10 = "";
	mode1_tbselectCode1 = 0;
	mode1_tbselectName1 = "";
	mode1_tbselectCode2 = 0;
	mode1_tbselectName2 = "";
	mode1_tbselectCode3 = 0;
	mode1_tbselectName3 = "";
	mode1_tbselectCode4 = 0;
	mode1_tbselectName4 = "";
	mode1_tbselectCode5 = 0;
	mode1_tbselectName5 = "";
	mode1_tbselectCode6 = 0;
	mode1_tbselectName6 = "";
	mode1_tbselectCode7 = 0;
	mode1_tbselectName7 = "";
	mode1_tbselectCode8 = 0;
	mode1_tbselectName8 = "";
	mode1_tbselectCode9 = 0;
	mode1_tbselectName9 = "";
	mode1_tbselectCode10 = 0;
	mode1_tbselectName10 = "";
	mode1_tbcustomersConfirmationItem = "";
	mode1_tblink = "";
	mode1_tbanimation = "";
	//end 70 fields
	mode1_sortResetFlg = 0;

	//Reset mode 2
	productIdMode2 = "";
	productSyouhinSysCodeMode2 = "";
	fitModelMakerMode2 = "";
	fitModelModelMode2 = "";
	fitModelStyleMode2 = "";
	importModeMode2 = "";

	//Reset mode 3
	productIdMode3 = "";
	productSyouhinSysCodeMode3 = "";
	attributeNameMode3 = "";
	attributeValueMode3 = "";
	attributeDisplayMode3 = "";
	importModeMode3 = "";
}

function mode1_clearMsgText() {
	// remove all class of msg div and clear msg
	$("#message").removeClass("has-message success");
	$("#message").removeClass("has-message msg-error");
	$("#message").text("");
}
function mode1_getSearchCondition(){
	
	//70 fields auto-gen code
	mode1_tbupdateMode = jQuery("#mode1_tbupdateMode").val();
	mode1_tbproductId = jQuery("#mode1_tbproductId").val();
	mode1_tbproductSyouhinSysCode = jQuery("#mode1_tbproductSyouhinSysCode").val();
	mode1_tbproductBrandCode = jQuery("#mode1_tbproductBrandCode").val();
	mode1_tbproductCategoryCode = jQuery("#mode1_tbproductCategoryCode").val();
	mode1_tbproductName = jQuery("#mode1_tbproductName").val();
	mode1_tbproductCode = jQuery("#mode1_tbproductCode").val();
	mode1_tbproductWebikeCodeFlg = jQuery("#mode1_tbproductWebikeCodeFlg").val();
	mode1_tbproductEanCode = jQuery("#mode1_tbproductEanCode").val();
	mode1_tbproductProperPrice = jQuery("#mode1_tbproductProperPrice").val();
	mode1_tbsupplierPricePrice = jQuery("#mode1_tbsupplierPricePrice").val();
	mode1_tbproductGroupCode = jQuery("#mode1_tbproductGroupCode").val();
	mode1_tbproductSupplierReleaseDate = jQuery("#mode1_tbproductSupplierReleaseDate").val();
	mode1_tbproductOpenPriceFlg = jQuery("#mode1_tbproductOpenPriceFlg").val();
	mode1_tbproductProperSellingFlg = jQuery("#mode1_tbproductProperSellingFlg").val();
	mode1_tbproductOrderProductFlg = jQuery("#mode1_tbproductOrderProductFlg").val();
	mode1_tbproductNoReturnableFlg = jQuery("#mode1_tbproductNoReturnableFlg").val();
	mode1_tbproductAmbiguousImageFlg = jQuery("#mode1_tbproductAmbiguousImageFlg").val();
	mode1_tbdescriptionSummary = jQuery("#mode1_tbdescriptionSummary").val();
	mode1_tbdescriptionRemarks = jQuery("#mode1_tbdescriptionRemarks").val();
	mode1_tbdescriptionCaution = jQuery("#mode1_tbdescriptionCaution").val();
	mode1_tbdescriptionSentence = jQuery("#mode1_tbdescriptionSentence").val();
	mode1_tbsiireCode1 = jQuery("#mode1_tbsiireCode1").val();
	mode1_tbnoukiCode1 = jQuery("#mode1_tbnoukiCode1").val();
	mode1_tbsiireCode2 = jQuery("#mode1_tbsiireCode2").val();
	mode1_tbnoukiCode2 = jQuery("#mode1_tbnoukiCode2").val();
	mode1_tbcompatibleModel = jQuery("#mode1_tbcompatibleModel").val();
	mode1_tbproductImageThumbnailPath1 = jQuery("#mode1_tbproductImageThumbnailPath1").val();
	mode1_tbproductImageDetailPath1 = jQuery("#mode1_tbproductImageDetailPath1").val();
	mode1_tbproductImageThumbnailPath2 = jQuery("#mode1_tbproductImageThumbnailPath2").val();
	mode1_tbproductImageDetailPath2 = jQuery("#mode1_tbproductImageDetailPath2").val();
	mode1_tbproductImageThumbnailPath3 = jQuery("#mode1_tbproductImageThumbnailPath3").val();
	mode1_tbproductImageDetailPath3 = jQuery("#mode1_tbproductImageDetailPath3").val();
	mode1_tbproductImageThumbnailPath4 = jQuery("#mode1_tbproductImageThumbnailPath4").val();
	mode1_tbproductImageDetailPath4 = jQuery("#mode1_tbproductImageDetailPath4").val();
	mode1_tbproductImageThumbnailPath5 = jQuery("#mode1_tbproductImageThumbnailPath5").val();
	mode1_tbproductImageDetailPath5 = jQuery("#mode1_tbproductImageDetailPath5").val();
	mode1_tbproductImageThumbnailPath6 = jQuery("#mode1_tbproductImageThumbnailPath6").val();
	mode1_tbproductImageDetailPath6 = jQuery("#mode1_tbproductImageDetailPath6").val();
	mode1_tbproductImageThumbnailPath7 = jQuery("#mode1_tbproductImageThumbnailPath7").val();
	mode1_tbproductImageDetailPath7 = jQuery("#mode1_tbproductImageDetailPath7").val();
	mode1_tbproductImageThumbnailPath8 = jQuery("#mode1_tbproductImageThumbnailPath8").val();
	mode1_tbproductImageDetailPath8 = jQuery("#mode1_tbproductImageDetailPath8").val();
	mode1_tbproductImageThumbnailPath9 = jQuery("#mode1_tbproductImageThumbnailPath9").val();
	mode1_tbproductImageDetailPath9 = jQuery("#mode1_tbproductImageDetailPath9").val();
	mode1_tbproductImageThumbnailPath10 = jQuery("#mode1_tbproductImageThumbnailPath10").val();
	mode1_tbproductImageDetailPath10 = jQuery("#mode1_tbproductImageDetailPath10").val();
	mode1_tbselectCode1 = jQuery("#mode1_tbselectCode1").val() == ""?0:parseInt(jQuery("#mode1_tbselectCode1").val());
	mode1_tbselectName1 = jQuery("#mode1_tbselectName1").val();
	mode1_tbselectCode2 = jQuery("#mode1_tbselectCode2").val() == ""?0:parseInt(jQuery("#mode1_tbselectCode2").val());
	mode1_tbselectName2 = jQuery("#mode1_tbselectName2").val();
	mode1_tbselectCode3 = jQuery("#mode1_tbselectCode3").val() == ""?0:parseInt(jQuery("#mode1_tbselectCode3").val());
	mode1_tbselectName3 = jQuery("#mode1_tbselectName3").val();
	mode1_tbselectCode4 = jQuery("#mode1_tbselectCode4").val() == ""?0:parseInt(jQuery("#mode1_tbselectCode4").val());
	mode1_tbselectName4 = jQuery("#mode1_tbselectName4").val();
	mode1_tbselectCode5 = jQuery("#mode1_tbselectCode5").val() == ""?0:parseInt(jQuery("#mode1_tbselectCode5").val());
	mode1_tbselectName5 = jQuery("#mode1_tbselectName5").val();
	mode1_tbselectCode6 = jQuery("#mode1_tbselectCode6").val() == ""?0:parseInt(jQuery("#mode1_tbselectCode6").val());
	mode1_tbselectName6 = jQuery("#mode1_tbselectName6").val();
	mode1_tbselectCode7 = jQuery("#mode1_tbselectCode7").val() == ""?0:parseInt(jQuery("#mode1_tbselectCode7").val());
	mode1_tbselectName7 = jQuery("#mode1_tbselectName7").val();
	mode1_tbselectCode8 = jQuery("#mode1_tbselectCode8").val() == ""?0:parseInt(jQuery("#mode1_tbselectCode8").val());
	mode1_tbselectName8 = jQuery("#mode1_tbselectName8").val();
	mode1_tbselectCode9 = jQuery("#mode1_tbselectCode9").val() == ""?0:parseInt(jQuery("#mode1_tbselectCode9").val());
	mode1_tbselectName9 = jQuery("#mode1_tbselectName9").val();
	mode1_tbselectCode10 = jQuery("#mode1_tbselectCode10").val() == ""?0:parseInt(jQuery("#mode1_tbselectCode10").val());
	mode1_tbselectName10 = jQuery("#mode1_tbselectName10").val();
	mode1_tbcustomersConfirmationItem = jQuery("#mode1_tbcustomersConfirmationItem").val();
	mode1_tblink = jQuery("#mode1_tblink").val();
	mode1_tbanimation = jQuery("#mode1_tbanimation").val();
	//end 70 fields auto-gen code
	
	//70 fields auto trim code:
	if (mode1_tbupdateMode) {
		mode1_tbupdateMode = mode1_tbupdateMode.trim();
	}
	if (mode1_tbproductId) {
		mode1_tbproductId = mode1_tbproductId.trim();
	}
	if (mode1_tbproductSyouhinSysCode) {
		mode1_tbproductSyouhinSysCode = mode1_tbproductSyouhinSysCode.trim();
	}
	if (mode1_tbproductBrandCode) {
		mode1_tbproductBrandCode = mode1_tbproductBrandCode.trim();
	}
	if (mode1_tbproductCategoryCode) {
		mode1_tbproductCategoryCode = mode1_tbproductCategoryCode.trim();
	}
	if (mode1_tbproductName) {
		mode1_tbproductName = mode1_tbproductName.trim();
	}
	if (mode1_tbproductCode) {
		mode1_tbproductCode = mode1_tbproductCode.trim();
	}
	if (mode1_tbproductWebikeCodeFlg) {
		mode1_tbproductWebikeCodeFlg = mode1_tbproductWebikeCodeFlg.trim();
	}
	if (mode1_tbproductEanCode) {
		mode1_tbproductEanCode = mode1_tbproductEanCode.trim();
	}
	if (mode1_tbproductProperPrice) {
		mode1_tbproductProperPrice = mode1_tbproductProperPrice.trim();
	}
	if (mode1_tbsupplierPricePrice) {
		mode1_tbsupplierPricePrice = mode1_tbsupplierPricePrice.trim();
	}
	if (mode1_tbproductGroupCode) {
		mode1_tbproductGroupCode = mode1_tbproductGroupCode.trim();
	}
	if (mode1_tbproductSupplierReleaseDate) {
		mode1_tbproductSupplierReleaseDate = mode1_tbproductSupplierReleaseDate.trim();
	}
	if (mode1_tbproductOpenPriceFlg) {
		mode1_tbproductOpenPriceFlg = mode1_tbproductOpenPriceFlg.trim();
	}
	if (mode1_tbproductProperSellingFlg) {
		mode1_tbproductProperSellingFlg = mode1_tbproductProperSellingFlg.trim();
	}
	if (mode1_tbproductOrderProductFlg) {
		mode1_tbproductOrderProductFlg = mode1_tbproductOrderProductFlg.trim();
	}
	if (mode1_tbproductNoReturnableFlg) {
		mode1_tbproductNoReturnableFlg = mode1_tbproductNoReturnableFlg.trim();
	}
	if (mode1_tbproductAmbiguousImageFlg) {
		mode1_tbproductAmbiguousImageFlg = mode1_tbproductAmbiguousImageFlg.trim();
	}
	if (mode1_tbdescriptionSummary) {
		mode1_tbdescriptionSummary = mode1_tbdescriptionSummary.trim();
	}
	if (mode1_tbdescriptionRemarks) {
		mode1_tbdescriptionRemarks = mode1_tbdescriptionRemarks.trim();
	}
	if (mode1_tbdescriptionCaution) {
		mode1_tbdescriptionCaution = mode1_tbdescriptionCaution.trim();
	}
	if (mode1_tbdescriptionSentence) {
		mode1_tbdescriptionSentence = mode1_tbdescriptionSentence.trim();
	}
	if (mode1_tbsiireCode1) {
		mode1_tbsiireCode1 = mode1_tbsiireCode1.trim();
	}
	if (mode1_tbnoukiCode1) {
		mode1_tbnoukiCode1 = mode1_tbnoukiCode1.trim();
	}
	if (mode1_tbsiireCode2) {
		mode1_tbsiireCode2 = mode1_tbsiireCode2.trim();
	}
	if (mode1_tbnoukiCode2) {
		mode1_tbnoukiCode2 = mode1_tbnoukiCode2.trim();
	}
	if (mode1_tbcompatibleModel) {
		mode1_tbcompatibleModel = mode1_tbcompatibleModel.trim();
	}
	if (mode1_tbproductImageThumbnailPath1) {
		mode1_tbproductImageThumbnailPath1 = mode1_tbproductImageThumbnailPath1.trim();
	}
	if (mode1_tbproductImageDetailPath1) {
		mode1_tbproductImageDetailPath1 = mode1_tbproductImageDetailPath1.trim();
	}
	if (mode1_tbproductImageThumbnailPath2) {
		mode1_tbproductImageThumbnailPath2 = mode1_tbproductImageThumbnailPath2.trim();
	}
	if (mode1_tbproductImageDetailPath2) {
		mode1_tbproductImageDetailPath2 = mode1_tbproductImageDetailPath2.trim();
	}
	if (mode1_tbproductImageThumbnailPath3) {
		mode1_tbproductImageThumbnailPath3 = mode1_tbproductImageThumbnailPath3.trim();
	}
	if (mode1_tbproductImageDetailPath3) {
		mode1_tbproductImageDetailPath3 = mode1_tbproductImageDetailPath3.trim();
	}
	if (mode1_tbproductImageThumbnailPath4) {
		mode1_tbproductImageThumbnailPath4 = mode1_tbproductImageThumbnailPath4.trim();
	}
	if (mode1_tbproductImageDetailPath4) {
		mode1_tbproductImageDetailPath4 = mode1_tbproductImageDetailPath4.trim();
	}
	if (mode1_tbproductImageThumbnailPath5) {
		mode1_tbproductImageThumbnailPath5 = mode1_tbproductImageThumbnailPath5.trim();
	}
	if (mode1_tbproductImageDetailPath5) {
		mode1_tbproductImageDetailPath5 = mode1_tbproductImageDetailPath5.trim();
	}
	if (mode1_tbproductImageThumbnailPath6) {
		mode1_tbproductImageThumbnailPath6 = mode1_tbproductImageThumbnailPath6.trim();
	}
	if (mode1_tbproductImageDetailPath6) {
		mode1_tbproductImageDetailPath6 = mode1_tbproductImageDetailPath6.trim();
	}
	if (mode1_tbproductImageThumbnailPath7) {
		mode1_tbproductImageThumbnailPath7 = mode1_tbproductImageThumbnailPath7.trim();
	}
	if (mode1_tbproductImageDetailPath7) {
		mode1_tbproductImageDetailPath7 = mode1_tbproductImageDetailPath7.trim();
	}
	if (mode1_tbproductImageThumbnailPath8) {
		mode1_tbproductImageThumbnailPath8 = mode1_tbproductImageThumbnailPath8.trim();
	}
	if (mode1_tbproductImageDetailPath8) {
		mode1_tbproductImageDetailPath8 = mode1_tbproductImageDetailPath8.trim();
	}
	if (mode1_tbproductImageThumbnailPath9) {
		mode1_tbproductImageThumbnailPath9 = mode1_tbproductImageThumbnailPath9.trim();
	}
	if (mode1_tbproductImageDetailPath9) {
		mode1_tbproductImageDetailPath9 = mode1_tbproductImageDetailPath9.trim();
	}
	if (mode1_tbproductImageThumbnailPath10) {
		mode1_tbproductImageThumbnailPath10 = mode1_tbproductImageThumbnailPath10.trim();
	}
	if (mode1_tbproductImageDetailPath10) {
		mode1_tbproductImageDetailPath10 = mode1_tbproductImageDetailPath10.trim();
	}
	if (mode1_tbselectName1) {
		mode1_tbselectName1 = mode1_tbselectName1.trim();
	}
	if (mode1_tbselectName2) {
		mode1_tbselectName2 = mode1_tbselectName2.trim();
	}
	if (mode1_tbselectName3) {
		mode1_tbselectName3 = mode1_tbselectName3.trim();
	}
	if (mode1_tbselectName4) {
		mode1_tbselectName4 = mode1_tbselectName4.trim();
	}
	if (mode1_tbselectName5) {
		mode1_tbselectName5 = mode1_tbselectName5.trim();
	}
	if (mode1_tbselectName6) {
		mode1_tbselectName6 = mode1_tbselectName6.trim();
	}
	if (mode1_tbselectName7) {
		mode1_tbselectName7 = mode1_tbselectName7.trim();
	}
	if (mode1_tbselectName8) {
		mode1_tbselectName8 = mode1_tbselectName8.trim();
	}
	if (mode1_tbselectName9) {
		mode1_tbselectName9 = mode1_tbselectName9.trim();
	}
	if (mode1_tbselectName10) {
		mode1_tbselectName10 = mode1_tbselectName10.trim();
	}
	if (mode1_tbcustomersConfirmationItem) {
		mode1_tbcustomersConfirmationItem = mode1_tbcustomersConfirmationItem.trim();
	}
	if (mode1_tblink) {
		mode1_tblink = mode1_tblink.trim();
	}
	if (mode1_tbanimation) {
		mode1_tbanimation = mode1_tbanimation.trim();
	}
	//end 70 fields auto trim code:
}
 function mode1_setDataForDataSource(sortField, sortDir) {
    return {
    	"matterDetailActionForm.entCSVMode1.importMode" : mode1_tbupdateMode,//not use "matterDetailActionForm.entCSVMode1.updateMode"
    	//Gen code for columns:
    	"matterDetailActionForm.entCSVMode1.productId" : mode1_tbproductId,
    	"matterDetailActionForm.entCSVMode1.productSyouhinSysCode" : mode1_tbproductSyouhinSysCode,
    	"matterDetailActionForm.entCSVMode1.productBrandCode" : mode1_tbproductBrandCode,
    	"matterDetailActionForm.entCSVMode1.productCategoryCode" : mode1_tbproductCategoryCode,
    	"matterDetailActionForm.entCSVMode1.productName" : mode1_tbproductName,
    	"matterDetailActionForm.entCSVMode1.productCode" : mode1_tbproductCode,
    	"matterDetailActionForm.entCSVMode1.productWebikeCodeFlg" : mode1_tbproductWebikeCodeFlg,
    	"matterDetailActionForm.entCSVMode1.productEanCode" : mode1_tbproductEanCode,
    	"matterDetailActionForm.entCSVMode1.productProperPrice" : mode1_tbproductProperPrice,
    	"matterDetailActionForm.entCSVMode1.supplierPricePrice" : mode1_tbsupplierPricePrice,
    	"matterDetailActionForm.entCSVMode1.productGroupCode" : mode1_tbproductGroupCode,
    	"matterDetailActionForm.entCSVMode1.productSupplierReleaseDate" : mode1_tbproductSupplierReleaseDate,
    	"matterDetailActionForm.entCSVMode1.productOpenPriceFlg" : mode1_tbproductOpenPriceFlg,
    	"matterDetailActionForm.entCSVMode1.productProperSellingFlg" : mode1_tbproductProperSellingFlg,
    	"matterDetailActionForm.entCSVMode1.productOrderProductFlg" : mode1_tbproductOrderProductFlg,
    	"matterDetailActionForm.entCSVMode1.productNoReturnableFlg" : mode1_tbproductNoReturnableFlg,
    	"matterDetailActionForm.entCSVMode1.productAmbiguousImageFlg" : mode1_tbproductAmbiguousImageFlg,
    	"matterDetailActionForm.entCSVMode1.descriptionSummary" : mode1_tbdescriptionSummary,
    	"matterDetailActionForm.entCSVMode1.descriptionRemarks" : mode1_tbdescriptionRemarks,
    	"matterDetailActionForm.entCSVMode1.descriptionCaution" : mode1_tbdescriptionCaution,
    	"matterDetailActionForm.entCSVMode1.descriptionSentence" : mode1_tbdescriptionSentence,
    	"matterDetailActionForm.entCSVMode1.siireCode1" : mode1_tbsiireCode1,
    	"matterDetailActionForm.entCSVMode1.noukiCode1" : mode1_tbnoukiCode1,
    	"matterDetailActionForm.entCSVMode1.siireCode2" : mode1_tbsiireCode2,
    	"matterDetailActionForm.entCSVMode1.noukiCode2" : mode1_tbnoukiCode2,
    	"matterDetailActionForm.entCSVMode1.compatibleModel" : mode1_tbcompatibleModel,
    	"matterDetailActionForm.entCSVMode1.productImageThumbnailPath1" : mode1_tbproductImageThumbnailPath1,
    	"matterDetailActionForm.entCSVMode1.productImageDetailPath1" : mode1_tbproductImageDetailPath1,
    	"matterDetailActionForm.entCSVMode1.productImageThumbnailPath2" : mode1_tbproductImageThumbnailPath2,
    	"matterDetailActionForm.entCSVMode1.productImageDetailPath2" : mode1_tbproductImageDetailPath2,
    	"matterDetailActionForm.entCSVMode1.productImageThumbnailPath3" : mode1_tbproductImageThumbnailPath3,
    	"matterDetailActionForm.entCSVMode1.productImageDetailPath3" : mode1_tbproductImageDetailPath3,
    	"matterDetailActionForm.entCSVMode1.productImageThumbnailPath4" : mode1_tbproductImageThumbnailPath4,
    	"matterDetailActionForm.entCSVMode1.productImageDetailPath4" : mode1_tbproductImageDetailPath4,
    	"matterDetailActionForm.entCSVMode1.productImageThumbnailPath5" : mode1_tbproductImageThumbnailPath5,
    	"matterDetailActionForm.entCSVMode1.productImageDetailPath5" : mode1_tbproductImageDetailPath5,
    	"matterDetailActionForm.entCSVMode1.productImageThumbnailPath6" : mode1_tbproductImageThumbnailPath6,
    	"matterDetailActionForm.entCSVMode1.productImageDetailPath6" : mode1_tbproductImageDetailPath6,
    	"matterDetailActionForm.entCSVMode1.productImageThumbnailPath7" : mode1_tbproductImageThumbnailPath7,
    	"matterDetailActionForm.entCSVMode1.productImageDetailPath7" : mode1_tbproductImageDetailPath7,
    	"matterDetailActionForm.entCSVMode1.productImageThumbnailPath8" : mode1_tbproductImageThumbnailPath8,
    	"matterDetailActionForm.entCSVMode1.productImageDetailPath8" : mode1_tbproductImageDetailPath8,
    	"matterDetailActionForm.entCSVMode1.productImageThumbnailPath9" : mode1_tbproductImageThumbnailPath9,
    	"matterDetailActionForm.entCSVMode1.productImageDetailPath9" : mode1_tbproductImageDetailPath9,
    	"matterDetailActionForm.entCSVMode1.productImageThumbnailPath10" : mode1_tbproductImageThumbnailPath10,
    	"matterDetailActionForm.entCSVMode1.productImageDetailPath10" : mode1_tbproductImageDetailPath10,
    	"matterDetailActionForm.entCSVMode1.selectCode1" : mode1_tbselectCode1,
    	"matterDetailActionForm.entCSVMode1.selectName1" : mode1_tbselectName1,
    	"matterDetailActionForm.entCSVMode1.selectCode2" : mode1_tbselectCode2,
    	"matterDetailActionForm.entCSVMode1.selectName2" : mode1_tbselectName2,
    	"matterDetailActionForm.entCSVMode1.selectCode3" : mode1_tbselectCode3,
    	"matterDetailActionForm.entCSVMode1.selectName3" : mode1_tbselectName3,
    	"matterDetailActionForm.entCSVMode1.selectCode4" : mode1_tbselectCode4,
    	"matterDetailActionForm.entCSVMode1.selectName4" : mode1_tbselectName4,
    	"matterDetailActionForm.entCSVMode1.selectCode5" : mode1_tbselectCode5,
    	"matterDetailActionForm.entCSVMode1.selectName5" : mode1_tbselectName5,
    	"matterDetailActionForm.entCSVMode1.selectCode6" : mode1_tbselectCode6,
    	"matterDetailActionForm.entCSVMode1.selectName6" : mode1_tbselectName6,
    	"matterDetailActionForm.entCSVMode1.selectCode7" : mode1_tbselectCode7,
    	"matterDetailActionForm.entCSVMode1.selectName7" : mode1_tbselectName7,
    	"matterDetailActionForm.entCSVMode1.selectCode8" : mode1_tbselectCode8,
    	"matterDetailActionForm.entCSVMode1.selectName8" : mode1_tbselectName8,
    	"matterDetailActionForm.entCSVMode1.selectCode9" : mode1_tbselectCode9,
    	"matterDetailActionForm.entCSVMode1.selectName9" : mode1_tbselectName9,
    	"matterDetailActionForm.entCSVMode1.selectCode10" : mode1_tbselectCode10,
    	"matterDetailActionForm.entCSVMode1.selectName10" : mode1_tbselectName10,
    	"matterDetailActionForm.entCSVMode1.customersConfirmationItem" : mode1_tbcustomersConfirmationItem,
    	"matterDetailActionForm.entCSVMode1.link" : mode1_tblink,
    	"matterDetailActionForm.entCSVMode1.animation" : mode1_tbanimation,
    	//End 70 columns.
    	"matterDetailActionForm.importedCSVTableName":                              importedCSVTableName,
    	"matterDetailActionForm.entCSVMode1.sortField"      :   sortField,
    	"matterDetailActionForm.entCSVMode1.sortDir"      :   sortDir
    };
}
var headerCSVMode1;
function mode1_initGridVerSion2(){
	
	//headerCSVMode1 is global variable of Chuong.
	var listHeader = headerCSVMode1;
    var columns = [];
    var stringFilterRow = '<tr id="filterRow">';
    //begin for:
	for(var k=0;k<listHeader.length;++k){
	    if(listHeader[k] == jQuery("#headerupdateMode").val()){
            stringFilterRow += '<td><input class="tbSearch k-input" type="text" class="filter" id="mode1_tbupdateMode" tabindex="1"/></td>';
            columns.push(                   
                            {
                                  field:"importMode", //"updateMode"
                                  type: "string",
                                  title: jQuery("#headerupdateMode").val(),
                                  width: "95px", 
                                  attributes: {"class": "align-left, row_table"}
                            });
        }else if(listHeader[k] == jQuery("#headerproductId").val()){
            stringFilterRow += '<td><input class="tbSearch k-input" type="text" class="filter" id="mode1_tbproductId" tabindex="2"/></td>';
            columns.push(
                            {
                              field:"productId",
                              template: function(myData){
                              	/* BOE #7206 Luong.Dai Show new product with product in mode new */
                              		/*return 'P'+myData.productId;*/
                              		if (myData.importMode == 'n') {
                              			return $('#newProduct').val();
                              		}
            	  	        	  	return 'P'+myData.productId;
            	  	        	/* BOE #7206 Luong.Dai Show new product with product in mode new */
            	  	          },
                              type: "number",
                              title: jQuery("#headerproductId").val(),
                              width: "150px", 
                              attributes: {"class": "align-right"} 
                            });
        }else if(listHeader[k] == jQuery("#headerproductSyouhinSysCode").val()){
            stringFilterRow += '<td><input class="tbSearch k-input" type="text" class="filter" id="mode1_tbproductSyouhinSysCode" tabindex="3"/></td>';
            columns.push(
                            { 
                              field:"productSyouhinSysCode",                      
                              type: "number",
                              title: jQuery("#headerproductSyouhinSysCode").val(),
                              width: "150px",
                              attributes: {"class": "align-right"}
                            });
        }else if(listHeader[k] == jQuery("#headerproductBrandCode").val()){
            stringFilterRow += '<td><input class="tbSearch k-input" type="text" class="filter" id="mode1_tbproductBrandCode" tabindex="4"/></td>';
            columns.push(
                            { 
                                  field:"productBrandCode",
                                  type: "number",
                                  title: jQuery("#headerproductBrandCode").val(),
                                  width: "150px",
                                  attributes: {"class": "align-right"}
                            });
        }else if(listHeader[k] == jQuery("#headerproductCategoryCode").val()){
            stringFilterRow += '<td><input class="tbSearch k-input" type="text" class="filter" id="mode1_tbproductCategoryCode" tabindex="3"/></td>';
            columns.push(
                            { 
                              field:"productCategoryCode",
                              type: "number",
                              title: jQuery("#headerproductCategoryCode").val(),
                              width: "150px",
                              attributes: {"class": "align-right"}
                            });
        }else if(listHeader[k] == jQuery("#headerproductName").val()){
            stringFilterRow += '<td><input class="tbSearch k-input" type="text" class="filter" id="mode1_tbproductName" tabindex="3"/></td>';
            columns.push(
                            { 
                              field:"productName",
                              type: "string",
                              title: jQuery("#headerproductName").val(),
                              width: "150px",
                              attributes: {"class": "align-left, row_table"}
                            });
        }else if(listHeader[k] == jQuery("#headerproductCode").val()){
            stringFilterRow += '<td><input class="tbSearch k-input" type="text" class="filter" id="mode1_tbproductCode" tabindex="3"/></td>';
            columns.push(
                            
                            { 
                              field: "productCode",
                              type: "string",
                              title: jQuery("#headerproductCode").val(),
                              width: "150px",
                              attributes: {"class": "align-left, row_table"}
                            });
        }else if(listHeader[k] == jQuery("#headerproductWebikeCodeFlg").val()){
            stringFilterRow += '<td><input class="tbSearch k-input" type="text" class="filter" id="mode1_tbproductWebikeCodeFlg" tabindex="3"/></td>';
            columns.push(
                            { 
                                  field: "productWebikeCodeFlg",
                                  type: "number",
                                  title: jQuery("#headerproductWebikeCodeFlg").val(),
                                  width: "150px",
                                  attributes: {"class": "align-center"}
                            });
        }else if(listHeader[k] == jQuery("#headerproductEanCode").val()){
            stringFilterRow += '<td><input class="tbSearch k-input" type="text" class="filter" id="mode1_tbproductEanCode" tabindex="3"/></td>';
            columns.push(
                            { 
                                  field: "productEanCode",
                                  type: "string",
                                  title: jQuery("#headerproductEanCode").val(),
                                  width: "150px",
                                  attributes: {"class": "align-left, row_table"}
                            });
        }else if(listHeader[k] == jQuery("#headerproductProperPrice").val()){
            stringFilterRow += '<td><input class="tbSearch k-input" type="text" class="filter" id="mode1_tbproductProperPrice" tabindex="3"/></td>';
            columns.push(
                          { 
                                  field: "productProperPrice",
                                  type: "string",
                                  title: jQuery("#headerproductProperPrice").val(),
                                  width: "150px",
                                  attributes: {"class": "align-right"}
                            });
        }else if(listHeader[k] == jQuery("#headersupplierPricePrice").val()){
            stringFilterRow += '<td><input class="tbSearch k-input" type="text" class="filter" id="mode1_tbsupplierPricePrice" tabindex="3"/></td>';
            columns.push(
                          { 
                                  field: "supplierPricePrice",
                                  type: "string",
                                  title: jQuery("#headersupplierPricePrice").val(),
                                  width: "150px",
                                  attributes: {"class": "align-right"}
                            });
        }else if(listHeader[k] == jQuery("#headerproductGroupCode").val()){
            stringFilterRow += '<td><input class="tbSearch k-input" type="text" class="filter" id="mode1_tbproductGroupCode" tabindex="3"/></td>';
            columns.push(
                          { 
                                  field: "productGroupCode",
                                  type: "string",
                                  title: jQuery("#headerproductGroupCode").val(),
                                  width: "150px",
                                  attributes: {"class": "align-right"}
                            });
        }else if(listHeader[k] == jQuery("#headerproductSupplierReleaseDate").val()){
            stringFilterRow += '<td><input class="tbSearch k-input" type="text" class="filter" id="mode1_tbproductSupplierReleaseDate" tabindex="3"/></td>';
            columns.push(
                          { 
                                  field: "productSupplierReleaseDate",
                                  type: "string",
                                  title: jQuery("#headerproductSupplierReleaseDate").val(),
                                  width: "150px",
                                  attributes: {"class": "align-right"}
                            });
        }else if(listHeader[k] == jQuery("#headerproductOpenPriceFlg").val()){
            stringFilterRow += '<td><input class="tbSearch k-input" type="text" class="filter" id="mode1_tbproductOpenPriceFlg" tabindex="3"/></td>';
            columns.push(
                          { 
                                  field: "productOpenPriceFlg",
                                  type: "string",
                                  title: jQuery("#headerproductOpenPriceFlg").val(),
                                  width: "150px",
                                  attributes: {"class": "align-center"}
                            });
        }else if(listHeader[k] == jQuery("#headerproductProperSellingFlg").val()){
            stringFilterRow += '<td><input class="tbSearch k-input" type="text" class="filter" id="mode1_tbproductProperSellingFlg" tabindex="3"/></td>';
            columns.push(
                          { 
                                  field: "productProperSellingFlg",
                                  type: "string",
                                  title: jQuery("#headerproductProperSellingFlg").val(),
                                  width: "150px",
                                  attributes: {"class": "align-center"}
                            });
        }else if(listHeader[k] == jQuery("#headerproductOrderProductFlg").val()){
            stringFilterRow += '<td><input class="tbSearch k-input" type="text" class="filter" id="mode1_tbproductOrderProductFlg" tabindex="3"/></td>';
            columns.push(
                          { 
                                  field: "productOrderProductFlg",
                                  type: "string",
                                  title: jQuery("#headerproductOrderProductFlg").val(),
                                  width: "150px",
                                  attributes: {"class": "align-center"}
                            });
        }else if(listHeader[k] == jQuery("#headerproductNoReturnableFlg").val()){
            stringFilterRow += '<td><input class="tbSearch k-input" type="text" class="filter" id="mode1_tbproductNoReturnableFlg" tabindex="3"/></td>';
            columns.push(
                          { 
                                  field: "productNoReturnableFlg",
                                  type: "string",
                                  title: jQuery("#headerproductNoReturnableFlg").val(),
                                  width: "150px",
                                  attributes: {"class": "align-center"}
                            });
        }else if(listHeader[k] == jQuery("#headerproductAmbiguousImageFlg").val()){
            stringFilterRow += '<td><input class="tbSearch k-input" type="text" class="filter" id="mode1_tbproductAmbiguousImageFlg" tabindex="3"/></td>';
            columns.push(
                          { 
                                  field: "productAmbiguousImageFlg",
                                  type: "string",
                                  title: jQuery("#headerproductAmbiguousImageFlg").val(),
                                  width: "150px",
                                  attributes: {"class": "align-center"}
                            });
        }else if(listHeader[k] == jQuery("#headerdescriptionSummary").val()){
            stringFilterRow += '<td><input class="tbSearch k-input" type="text" class="filter" id="mode1_tbdescriptionSummary" tabindex="3"/></td>';
            columns.push(
                          { 
                                  field: "descriptionSummary",
                                  type: "string",
                                  title: jQuery("#headerdescriptionSummary").val(),
                                  width: "150px",
                                  attributes: {"class": "align-left, row_table"}
                            });
        }else if(listHeader[k] == jQuery("#headerdescriptionRemarks").val()){
            stringFilterRow += '<td><input class="tbSearch k-input" type="text" class="filter" id="mode1_tbdescriptionRemarks" tabindex="3"/></td>';
            columns.push(
                          { 
                                  field: "descriptionRemarks",
                                  type: "string",
                                  title: jQuery("#headerdescriptionRemarks").val(),
                                  width: "150px",
                                  attributes: {"class": "align-left, row_table"}
                            });
        }else if(listHeader[k] == jQuery("#headerdescriptionCaution").val()){
            stringFilterRow += '<td><input class="tbSearch k-input" type="text" class="filter" id="mode1_tbdescriptionCaution" tabindex="3"/></td>';
            columns.push(
                          { 
                                  field: "descriptionCaution",
                                  type: "string",
                                  title: jQuery("#headerdescriptionCaution").val(),
                                  width: "150px",
                                  attributes: {"class": "align-left, row_table"}
                            });
        }else if(listHeader[k] == jQuery("#headerdescriptionSentence").val()){
            stringFilterRow += '<td><input class="tbSearch k-input" type="text" class="filter" id="mode1_tbdescriptionSentence" tabindex="3"/></td>';
            columns.push(
                          { 
                                  field: "descriptionSentence",
                                  type: "string",
                                  title: jQuery("#headerdescriptionSentence").val(),
                                  width: "150px",
                                  attributes: {"class": "align-left, row_table"}
                            });
        }else if(listHeader[k] == jQuery("#headersiireCode1").val()){
            stringFilterRow += '<td><input class="tbSearch k-input" type="text" class="filter" id="mode1_tbsiireCode1" tabindex="3"/></td>';
            columns.push(
                          { 
                                  field: "siireCode1",
                                  type: "string",
                                  title: jQuery("#headersiireCode1").val(),
                                  width: "150px",
                                  attributes: {"class": "align-left, row_table"}
                            });
        }else if(listHeader[k] == jQuery("#headernoukiCode1").val()){
            stringFilterRow += '<td><input class="tbSearch k-input" type="text" class="filter" id="mode1_tbnoukiCode1" tabindex="3"/></td>';
            columns.push(
                          { 
                                  field: "noukiCode1",
                                  type: "string",
                                  title: jQuery("#headernoukiCode1").val(),
                                  width: "150px",
                                  attributes: {"class": "align-left, row_table"}
                            });
        }else if(listHeader[k] == jQuery("#headersiireCode2").val()){
            stringFilterRow += '<td><input class="tbSearch k-input" type="text" class="filter" id="mode1_tbsiireCode2" tabindex="3"/></td>';
            columns.push(
                          { 
                                  field: "siireCode2",
                                  type: "string",
                                  title: jQuery("#headersiireCode2").val(),
                                  width: "150px",
                                  attributes: {"class": "align-left, row_table"}
                            });
        }else if(listHeader[k] == jQuery("#headernoukiCode2").val()){
            stringFilterRow += '<td><input class="tbSearch k-input" type="text" class="filter" id="mode1_tbnoukiCode2" tabindex="3"/></td>';
            columns.push(
                          { 
                                  field: "noukiCode2",
                                  type: "string",
                                  title: jQuery("#headernoukiCode2").val(),
                                  width: "150px",
                                  attributes: {"class": "align-left, row_table"}
                            });
        }else if(listHeader[k] == jQuery("#headercompatibleModel").val()){
            stringFilterRow += '<td><input class="tbSearch k-input" type="text" class="filter" id="mode1_tbcompatibleModel" tabindex="3"/></td>';
            columns.push(
                          { 
                                  field: "compatibleModel",
                                  type: "string",
                                  title: jQuery("#headercompatibleModel").val(),
                                  width: "150px",
                                  attributes: {"class": "align-left, row_table"}
                            });
        }else if(listHeader[k] == jQuery('#headerproductImageThumbnailPath').val() + '1'){
            stringFilterRow += '<td><input class="tbSearch k-input" type="text" class="filter" id="mode1_tbproductImageThumbnailPath1" tabindex="3"/></td>';
            columns.push(
                            //////-----------------------------Begin--------- X :  X runs from 1 --> 10. Now, I test X = 1-->3

                          { 
                                field: 'productImageThumbnailPath1',
                                type: 'string',
                                title: jQuery('#headerproductImageThumbnailPath').val() + '1',
                                width: '150px',
                                attributes: {'class': 'align-left, row_table'} 
                                });
        }else if(listHeader[k] == jQuery('#headerproductImageDetailPath').val() + '1'){
            stringFilterRow += '<td><input class="tbSearch k-input" type="text" class="filter" id="mode1_tbproductImageDetailPath1" tabindex="3"/></td>';
            columns.push(
                                { 
                                field: 'productImageDetailPath1',
                                type: 'string',
                                title: jQuery('#headerproductImageDetailPath').val() + '1',
                                width: '150px',
                                attributes: {'class': 'align-left, row_table'}
                                });
        }else if(listHeader[k] == jQuery('#headerproductImageThumbnailPath').val() + '2'){
            stringFilterRow += '<td><input class="tbSearch k-input" type="text" class="filter" id="mode1_tbproductImageThumbnailPath2" tabindex="3"/></td>';
            columns.push(
                                { 
                                field: 'productImageThumbnailPath2',
                                type: 'string',
                                title: jQuery('#headerproductImageThumbnailPath').val() + '2',
                                width: '150px',
                                attributes: {'class': 'align-left, row_table'} 
                                });
        }else if(listHeader[k] == jQuery('#headerproductImageDetailPath').val() + '2'){
            stringFilterRow += '<td><input class="tbSearch k-input" type="text" class="filter" id="mode1_tbproductImageDetailPath2" tabindex="3"/></td>';
            columns.push(
                                { 
                                field: 'productImageDetailPath2',
                                type: 'string',
                                title: jQuery('#headerproductImageDetailPath').val() + '2',
                                width: '150px',
                                attributes: {'class': 'align-left, row_table'}
                                });
        }else if(listHeader[k] == jQuery('#headerproductImageThumbnailPath').val() + '3'){
            stringFilterRow += '<td><input class="tbSearch k-input" type="text" class="filter" id="mode1_tbproductImageThumbnailPath3" tabindex="3"/></td>';
            columns.push(
                                { 
                                field: 'productImageThumbnailPath3',
                                type: 'string',
                                title: jQuery('#headerproductImageThumbnailPath').val() + '3',
                                width: '150px',
                                attributes: {'class': 'align-left, row_table'} 
                                });
        }else if(listHeader[k] == jQuery('#headerproductImageDetailPath').val() + '3'){
            stringFilterRow += '<td><input class="tbSearch k-input" type="text" class="filter" id="mode1_tbproductImageDetailPath3" tabindex="3"/></td>';
            columns.push(
                                { 
                                field: 'productImageDetailPath3',
                                type: 'string',
                                title: jQuery('#headerproductImageDetailPath').val() + '3',
                                width: '150px',
                                attributes: {'class': 'align-left, row_table'}
                                });
        }else if(listHeader[k] == jQuery('#headerproductImageThumbnailPath').val() + '4'){
            stringFilterRow += '<td><input class="tbSearch k-input" type="text" class="filter" id="mode1_tbproductImageThumbnailPath4" tabindex="3"/></td>';
            columns.push(
                                { 
                                field: 'productImageThumbnailPath4',
                                type: 'string',
                                title: jQuery('#headerproductImageThumbnailPath').val() + '4',
                                width: '150px',
                                attributes: {'class': 'align-left, row_table'} 
                                });
        }else if(listHeader[k] == jQuery('#headerproductImageDetailPath').val() + '4'){
            stringFilterRow += '<td><input class="tbSearch k-input" type="text" class="filter" id="mode1_tbproductImageDetailPath4" tabindex="3"/></td>';
            columns.push(
                                { 
                                field: 'productImageDetailPath4',
                                type: 'string',
                                title: jQuery('#headerproductImageDetailPath').val() + '4',
                                width: '150px',
                                attributes: {'class': 'align-left, row_table'}
                                });
        }else if(listHeader[k] == jQuery('#headerproductImageThumbnailPath').val() + '5'){
            stringFilterRow += '<td><input class="tbSearch k-input" type="text" class="filter" id="mode1_tbproductImageThumbnailPath5" tabindex="3"/></td>';
            columns.push(
                                { 
                                field: 'productImageThumbnailPath5',
                                type: 'string',
                                title: jQuery('#headerproductImageThumbnailPath').val() + '5',
                                width: '150px',
                                attributes: {'class': 'align-left, row_table'} 
                                });
        }else if(listHeader[k] == jQuery('#headerproductImageDetailPath').val() + '5'){
            stringFilterRow += '<td><input class="tbSearch k-input" type="text" class="filter" id="mode1_tbproductImageDetailPath5" tabindex="3"/></td>';
            columns.push(
                                { 
                                field: 'productImageDetailPath5',
                                type: 'string',
                                title: jQuery('#headerproductImageDetailPath').val() + '5',
                                width: '150px',
                                attributes: {'class': 'align-left, row_table'}
                                });
        }else if(listHeader[k] == jQuery('#headerproductImageThumbnailPath').val() + '6'){
            stringFilterRow += '<td><input class="tbSearch k-input" type="text" class="filter" id="mode1_tbproductImageThumbnailPath6" tabindex="3"/></td>';
            columns.push(
                                { 
                                field: 'productImageThumbnailPath6',
                                type: 'string',
                                title: jQuery('#headerproductImageThumbnailPath').val() + '6',
                                width: '150px',
                                attributes: {'class': 'align-left, row_table'} 
                                });
        }else if(listHeader[k] == jQuery('#headerproductImageDetailPath').val() + '6'){
            stringFilterRow += '<td><input class="tbSearch k-input" type="text" class="filter" id="mode1_tbproductImageDetailPath6" tabindex="3"/></td>';
            columns.push(
                                { 
                                field: 'productImageDetailPath6',
                                type: 'string',
                                title: jQuery('#headerproductImageDetailPath').val() + '6',
                                width: '150px',
                                attributes: {'class': 'align-left, row_table'}
                                });
        }else if(listHeader[k] == jQuery('#headerproductImageThumbnailPath').val() + '7'){
            stringFilterRow += '<td><input class="tbSearch k-input" type="text" class="filter" id="mode1_tbproductImageThumbnailPath7" tabindex="3"/></td>';
            columns.push(
                                { 
                                field: 'productImageThumbnailPath7',
                                type: 'string',
                                title: jQuery('#headerproductImageThumbnailPath').val() + '7',
                                width: '150px',
                                attributes: {'class': 'align-left, row_table'} 
                                });
        }else if(listHeader[k] == jQuery('#headerproductImageDetailPath').val() + '7'){
            stringFilterRow += '<td><input class="tbSearch k-input" type="text" class="filter" id="mode1_tbproductImageDetailPath7" tabindex="3"/></td>';
            columns.push(
                                { 
                                field: 'productImageDetailPath7',
                                type: 'string',
                                title: jQuery('#headerproductImageDetailPath').val() + '7',
                                width: '150px',
                                attributes: {'class': 'align-left, row_table'}
                                });
        }else if(listHeader[k] == jQuery('#headerproductImageThumbnailPath').val() + '8'){
            stringFilterRow += '<td><input class="tbSearch k-input" type="text" class="filter" id="mode1_tbproductImageThumbnailPath8" tabindex="3"/></td>';
            columns.push(
                                { 
                                field: 'productImageThumbnailPath8',
                                type: 'string',
                                title: jQuery('#headerproductImageThumbnailPath').val() + '8',
                                width: '150px',
                                attributes: {'class': 'align-left, row_table'} 
                                });
        }else if(listHeader[k] == jQuery('#headerproductImageDetailPath').val() + '8'){
            stringFilterRow += '<td><input class="tbSearch k-input" type="text" class="filter" id="mode1_tbproductImageDetailPath8" tabindex="3"/></td>';
            columns.push(
                                { 
                                field: 'productImageDetailPath8',
                                type: 'string',
                                title: jQuery('#headerproductImageDetailPath').val() + '8',
                                width: '150px',
                                attributes: {'class': 'align-left, row_table'}
                                });
        }else if(listHeader[k] == jQuery('#headerproductImageThumbnailPath').val() + '9'){
            stringFilterRow += '<td><input class="tbSearch k-input" type="text" class="filter" id="mode1_tbproductImageThumbnailPath9" tabindex="3"/></td>';
            columns.push(
                                { 
                                field: 'productImageThumbnailPath9',
                                type: 'string',
                                title: jQuery('#headerproductImageThumbnailPath').val() + '9',
                                width: '150px',
                                attributes: {'class': 'align-left, row_table'} 
                                });
        }else if(listHeader[k] == jQuery('#headerproductImageDetailPath').val() + '9'){
            stringFilterRow += '<td><input class="tbSearch k-input" type="text" class="filter" id="mode1_tbproductImageDetailPath9" tabindex="3"/></td>';
            columns.push(
                                { 
                                field: 'productImageDetailPath9',
                                type: 'string',
                                title: jQuery('#headerproductImageDetailPath').val() + '9',
                                width: '150px',
                                attributes: {'class': 'align-left, row_table'}
                                });
        }else if(listHeader[k] == jQuery('#headerproductImageThumbnailPath').val() + '10'){
            stringFilterRow += '<td><input class="tbSearch k-input" type="text" class="filter" id="mode1_tbproductImageThumbnailPath10" tabindex="3"/></td>';
            columns.push(
                                { 
                                field: 'productImageThumbnailPath10',
                                type: 'string',
                                title: jQuery('#headerproductImageThumbnailPath').val() + '10',
                                width: '150px',
                                attributes: {'class': 'align-left, row_table'} 
                                });
        }else if(listHeader[k] == jQuery('#headerproductImageDetailPath').val() + '10'){
            stringFilterRow += '<td><input class="tbSearch k-input" type="text" class="filter" id="mode1_tbproductImageDetailPath10" tabindex="3"/></td>';
            columns.push(
                                { 
                                field: 'productImageDetailPath10',
                                type: 'string',
                                title: jQuery('#headerproductImageDetailPath').val() + '10',
                                width: '150px',
                                attributes: {'class': 'align-left, row_table'}
                                });
        }else if(listHeader[k] == jQuery('#headerselectCode').val() + '1'){
            stringFilterRow += '<td><input class="tbSearch k-input" type="text" class="filter" id="mode1_tbselectCode1" tabindex="3"/></td>';
            columns.push(
                                ///////////////////selectCode-selectName:
                                { 
                                    field: 'selectCode1',
                                    type: 'number',
                                    title: jQuery('#headerselectCode').val() + '1',
                                    width: '150px',
                                    attributes: {"class": "align-right"}
                                    });
        }else if(listHeader[k] == jQuery('#headerselectName').val() + '1'){
            stringFilterRow += '<td><input class="tbSearch k-input" type="text" class="filter" id="mode1_tbselectName1" tabindex="3"/></td>';
            columns.push(
                                    { 
                                    field: 'selectName1',
                                    type: 'string',
                                    title: jQuery('#headerselectName').val() + '1',
                                    width: '150px',
                                    attributes: {'class': 'align-left, row_table'}
                                    });
        }else if(listHeader[k] == jQuery('#headerselectCode').val() + '2'){
            stringFilterRow += '<td><input class="tbSearch k-input" type="text" class="filter" id="mode1_tbselectCode2" tabindex="3"/></td>';
            columns.push(
                                    { 
                                    field: 'selectCode2',
                                    type: 'number',
                                    title: jQuery('#headerselectCode').val() + '2',
                                    width: '150px',
                                    attributes: {"class": "align-right"}
                                    });
        }else if(listHeader[k] == jQuery('#headerselectName').val() + '2'){
            stringFilterRow += '<td><input class="tbSearch k-input" type="text" class="filter" id="mode1_tbselectName2" tabindex="3"/></td>';
            columns.push(
                                    { 
                                    field: 'selectName2',
                                    type: 'string',
                                    title: jQuery('#headerselectName').val() + '2',
                                    width: '150px',
                                    attributes: {'class': 'align-left, row_table'}
                                    });
        }else if(listHeader[k] == jQuery('#headerselectCode').val() + '3'){
            stringFilterRow += '<td><input class="tbSearch k-input" type="text" class="filter" id="mode1_tbselectCode3" tabindex="3"/></td>';
            columns.push(
                                    { 
                                    field: 'selectCode3',
                                    type: 'number',
                                    title: jQuery('#headerselectCode').val() + '3',
                                    width: '150px',
                                    attributes: {"class": "align-right"}
                                    });
        }else if(listHeader[k] == jQuery('#headerselectName').val() + '3'){
            stringFilterRow += '<td><input class="tbSearch k-input" type="text" class="filter" id="mode1_tbselectName3" tabindex="3"/></td>';
            columns.push(
                                    { 
                                    field: 'selectName3',
                                    type: 'string',
                                    title: jQuery('#headerselectName').val() + '3',
                                    width: '150px',
                                    attributes: {'class': 'align-left, row_table'}
                                    });
        }else if(listHeader[k] == jQuery('#headerselectCode').val() + '4'){
            stringFilterRow += '<td><input class="tbSearch k-input" type="text" class="filter" id="mode1_tbselectCode4" tabindex="3"/></td>';
            columns.push(
                                    { 
                                    field: 'selectCode4',
                                    type: 'number',
                                    title: jQuery('#headerselectCode').val() + '4',
                                    width: '150px',
                                    attributes: {"class": "align-right"}
                                    });
        }else if(listHeader[k] == jQuery('#headerselectName').val() + '4'){
            stringFilterRow += '<td><input class="tbSearch k-input" type="text" class="filter" id="mode1_tbselectName4" tabindex="3"/></td>';
            columns.push(
                                    { 
                                    field: 'selectName4',
                                    type: 'string',
                                    title: jQuery('#headerselectName').val() + '4',
                                    width: '150px',
                                    attributes: {'class': 'align-left, row_table'}
                                    });
        }else if(listHeader[k] == jQuery('#headerselectCode').val() + '5'){
            stringFilterRow += '<td><input class="tbSearch k-input" type="text" class="filter" id="mode1_tbselectCode5" tabindex="3"/></td>';
            columns.push(
                                    { 
                                    field: 'selectCode5',
                                    type: 'number',
                                    title: jQuery('#headerselectCode').val() + '5',
                                    width: '150px',
                                    attributes: {"class": "align-right"}
                                    });
        }else if(listHeader[k] == jQuery('#headerselectName').val() + '5'){
            stringFilterRow += '<td><input class="tbSearch k-input" type="text" class="filter" id="mode1_tbselectName5" tabindex="3"/></td>';
            columns.push(
                                    { 
                                    field: 'selectName5',
                                    type: 'string',
                                    title: jQuery('#headerselectName').val() + '5',
                                    width: '150px',
                                    attributes: {'class': 'align-left, row_table'}
                                    });
        }else if(listHeader[k] == jQuery('#headerselectCode').val() + '6'){
            stringFilterRow += '<td><input class="tbSearch k-input" type="text" class="filter" id="mode1_tbselectCode6" tabindex="3"/></td>';
            columns.push(
                                    { 
                                    field: 'selectCode6',
                                    type: 'number',
                                    title: jQuery('#headerselectCode').val() + '6',
                                    width: '150px',
                                    attributes: {"class": "align-right"}
                                    });
        }else if(listHeader[k] == jQuery('#headerselectName').val() + '6'){
            stringFilterRow += '<td><input class="tbSearch k-input" type="text" class="filter" id="mode1_tbselectName6" tabindex="3"/></td>';
            columns.push(
                                    { 
                                    field: 'selectName6',
                                    type: 'string',
                                    title: jQuery('#headerselectName').val() + '6',
                                    width: '150px',
                                    attributes: {'class': 'align-left, row_table'}
                                    });
        }else if(listHeader[k] == jQuery('#headerselectCode').val() + '7'){
            stringFilterRow += '<td><input class="tbSearch k-input" type="text" class="filter" id="mode1_tbselectCode7" tabindex="3"/></td>';
            columns.push(
                                    { 
                                    field: 'selectCode7',
                                    type: 'number',
                                    title: jQuery('#headerselectCode').val() + '7',
                                    width: '150px',
                                    attributes: {"class": "align-right"}
                                    });
        }else if(listHeader[k] == jQuery('#headerselectName').val() + '7'){
            stringFilterRow += '<td><input class="tbSearch k-input" type="text" class="filter" id="mode1_tbselectName7" tabindex="3"/></td>';
            columns.push(
                                    { 
                                    field: 'selectName7',
                                    type: 'string',
                                    title: jQuery('#headerselectName').val() + '7',
                                    width: '150px',
                                    attributes: {'class': 'align-left, row_table'}
                                    });
        }else if(listHeader[k] == jQuery('#headerselectCode').val() + '8'){
            stringFilterRow += '<td><input class="tbSearch k-input" type="text" class="filter" id="mode1_tbselectCode8" tabindex="3"/></td>';
            columns.push(
                                    { 
                                    field: 'selectCode8',
                                    type: 'number',
                                    title: jQuery('#headerselectCode').val() + '8',
                                    width: '150px',
                                    attributes: {"class": "align-right"}
                                    });
        }else if(listHeader[k] == jQuery('#headerselectName').val() + '8'){
            stringFilterRow += '<td><input class="tbSearch k-input" type="text" class="filter" id="mode1_tbselectName8" tabindex="3"/></td>';
            columns.push(
                                    { 
                                    field: 'selectName8',
                                    type: 'string',
                                    title: jQuery('#headerselectName').val() + '8',
                                    width: '150px',
                                    attributes: {'class': 'align-left, row_table'}
                                    });
        }else if(listHeader[k] == jQuery('#headerselectCode').val() + '9'){
            stringFilterRow += '<td><input class="tbSearch k-input" type="text" class="filter" id="mode1_tbselectCode9" tabindex="3"/></td>';
            columns.push(
                                    { 
                                    field: 'selectCode9',
                                    type: 'number',
                                    title: jQuery('#headerselectCode').val() + '9',
                                    width: '150px',
                                    attributes: {"class": "align-right"}
                                    });
        }else if(listHeader[k] == jQuery('#headerselectName').val() + '9'){
            stringFilterRow += '<td><input class="tbSearch k-input" type="text" class="filter" id="mode1_tbselectName9" tabindex="3"/></td>';
            columns.push(
                                    { 
                                    field: 'selectName9',
                                    type: 'string',
                                    title: jQuery('#headerselectName').val() + '9',
                                    width: '150px',
                                    attributes: {'class': 'align-left, row_table'}
                                    });
        }else if(listHeader[k] == jQuery('#headerselectCode').val() + '10'){
            stringFilterRow += '<td><input class="tbSearch k-input" type="text" class="filter" id="mode1_tbselectCode10" tabindex="3"/></td>';
            columns.push(
                                    { 
                                    field: 'selectCode10',
                                    type: 'number',
                                    title: jQuery('#headerselectCode').val() + '10',
                                    width: '150px',
                                    attributes: {"class": "align-right"}
                                    });
        }else if(listHeader[k] == jQuery('#headerselectName').val() + '10'){
            stringFilterRow += '<td><input class="tbSearch k-input" type="text" class="filter" id="mode1_tbselectName10" tabindex="3"/></td>';
            columns.push(
                                    { 
                                    field: 'selectName10',
                                    type: 'string',
                                    title: jQuery('#headerselectName').val() + '10',
                                    width: '150px',
                                    attributes: {'class': 'align-left, row_table'}
                                    });
    /////////--------------------------End--------------- X :  X runs from 1 --> 10.
        }else if(listHeader[k] == jQuery("#headercustomersConfirmationItem").val()){
            stringFilterRow += '<td><input class="tbSearch k-input" type="text" class="filter" id="mode1_tbcustomersConfirmationItem" tabindex="3"/></td>';
            columns.push(
                          { 
                                  field: "customersConfirmationItem",
                                  type: "string",
                                  title: jQuery("#headercustomersConfirmationItem").val(),
                                  width: "150px",
                                  attributes: {"class": "align-left, row_table"}
                            });
        }else if(listHeader[k] == jQuery("#headerlink").val()){
            stringFilterRow += '<td><input class="tbSearch k-input" type="text" class="filter" id="mode1_tblink" tabindex="3"/></td>';
            columns.push(
                          { 
                                  field: "link",
                                  type: "string",
                                  title: jQuery("#headerlink").val(),
                                  width: "150px",
                                  attributes: {"class": "align-left, row_table"}
                            });
        }else if(listHeader[k] == jQuery("#headeranimation").val()){
            stringFilterRow += '<td><input class="tbSearch k-input" type="text" class="filter" id="mode1_tbanimation" tabindex="3"/></td>';
            columns.push(
                          { 
                                  field: "animation",
                                  type: "string",
                                  title: jQuery("#headeranimation").val(),
                                  width: "150px",
                                  attributes: {"class": "align-left, row_table"}
                            });
        }
		
	}//end for.
	stringFilterRow += '</tr>';
	var filterRow = $(stringFilterRow);
	//----------------create grid:
	//BOE @rcv!Tran.Thanh 2014/07/10 : init empty grid when import have error ro
	if (importValid) {
		mode1_dataSource = new kendo.data.DataSource({
	        serverPaging: true,
	        autoBind: false,
	        serverSorting: true,
	        // 50 product in 1 page
	        pageSize: 50,
	        transport: {
	            read: {
	                type: "POST",
	                dataType: "json",
	                url: "gridMode1.html",
	                data:  function (data) {
	                    var sortField = "productId";
	                    var sortDir = "asc";
	                    if (mode1_sortResetFlg != 1) {
	                        if (data != undefined && data.sort != undefined && data.sort.length > 0) {
	                            sortField = data.sort[0].field;
	                            sortFieldCSV = data.sort[0].field;
	                            sortDir = data.sort[0].dir;
	                            sortDirCSV = data.sort[0].dir;
	                        }
	                    } else {
	                        if (data != undefined && data.sort != undefined && data.sort.length > 0) {
	                            data.sort[0].field = '';
	                            data.sort[0].dir = '';
	                        }
	                    }
	                    return mode1_setDataForDataSource(sortField, sortDir);
	                },
	                beforeSend :function() {
	                	mode1_clearMsgText();
	                	//Hide processing of Kendo.
	//                    jQuery("#gridMode1 .k-loading-image").css("background-image", "none");
	//                    // show process bar before send request.
	//                    showProcessBar();
	                },
	                complete: function() {
	                    // hide process bar that the request succeeded
	//                	hideProcessBar();
	                    mode1_sortResetFlg = 0;
	                },
	                error: function() {
	                    // hide process bar that the request failed
	//                    hideProcessBar();
	                    mode1_sortResetFlg = 0;
	                },
	                cache: false,
	            }
	        },
	        schema: {
	            data: "matterDetailActionForm.listEntCSVMode1",
	            total: "matterDetailActionForm.count"
	        }
	    });
	//end datasource.
	} else {
		var gridDataJson = $.parseJSON("[]");
		mode1_dataSource = new kendo.data.DataSource({
	      	data: gridDataJson
	    });
	}
	//EOE @rcv!Tran.Thanh 2014/07/10 : init empty grid when import have error row
	
    gridMode1 = $("#gridMode1").kendoGrid({
		dataSource: mode1_dataSource,
        sortable: {
            mode: "single",
            allowUnsort: false
        },
	    pageable: {
	        refresh: true,
	        pageSizes: [50, 100],
	        buttonCount: 5,
	        messages: {
	          display:         jQuery("#pagingDisplay").val(),
	          empty:           jQuery("#pagingEmpty").val(),
	          itemsPerPage:    jQuery("#pagingItemsPerPage").val(),
	          first:           jQuery("#pagingFirst").val(),
	          previous:        jQuery("#pagingPrevious").val(),
	          next:            jQuery("#pagingNext").val(),
	          last:            jQuery("#pagingLast").val()
	        }
	    },
	    columns: columns,
	    height: 470,
	    minHeight: 470,
        selectable: true,
	    resizable : true,
	    dataBound: function(e){
	    	// Remove noData message
			jQuery("#gridMode1 .dataTables_empty").remove();
			if (this.dataSource.total() == 0) {
				// Show no data message
				jQuery("#gridMode1 .k-grid-content").append(
						'<div class="dataTables_empty" id="grid4EmptyElement">'
								+ jQuery("#noDataMessage")
										.val() + '</div>');
			}
			
			// Handle clicking row.
//			mode1_handleClickingRows();
			// Select all processing.
//			handleSelectAll("#selectAllCheckbox", "#gridMode1 table tbody tr", ".productCheckbox", selectedKendoClass, selectedRowClass);
	    },
	    change: function(data) {
	    	
		},
    });
	    
	//Add filter
    gridMode1.data("kendoGrid").thead.append(filterRow);
    //+++++After Refresh dynamic grid, CSS is no effect, So I create CSS area for dynamic grid
    //Add bottom border for header row, align center for header of grid
    $('#gridMode1 .k-grid-header .k-grid-header-wrap thead tr th[role=columnheader]').attr( "style", "border-bottom: 1px solid #DADADA !important;text-align: center;");
    
    //+++++End Area CSS for dynamic grid
    // Set input numberic only in textbox search filter
	mode1_inputNumbericOnly();
	//----------------

}
function mode1_inputNumbericOnly() {
	Utils.bindFormatOnTextfield("mode1_tbproductId", Utils._TF_NUMBER);
	Utils.bindFormatOnTextfield("mode1_tbproductSyouhinSysCode", Utils._TF_NUMBER);
	Utils.bindFormatOnTextfield("mode1_tbproductBrandCode", Utils._TF_NUMBER);
	Utils.bindFormatOnTextfield("mode1_tbproductCategoryCode", Utils._TF_NUMBER);
	Utils.bindFormatOnTextfield("mode1_tbproductWebikeCodeFlg", Utils._TF_NUMBER);
	Utils.bindFormatOnTextfield("mode1_tbproductProperPrice", Utils._TF_NUMBER);
	Utils.bindFormatOnTextfield("mode1_tbsupplierPricePrice", Utils._TF_NUMBER);
	Utils.bindFormatOnTextfield("mode1_tbproductGroupCode", Utils._TF_NUMBER);
	Utils.bindFormatOnTextfield("mode1_tbproductOpenPriceFlg", Utils._TF_NUMBER);
	Utils.bindFormatOnTextfield("mode1_tbproductProperSellingFlg", Utils._TF_NUMBER);
	Utils.bindFormatOnTextfield("mode1_tbproductOrderProductFlg", Utils._TF_NUMBER);
	Utils.bindFormatOnTextfield("mode1_tbproductNoReturnableFlg", Utils._TF_NUMBER);
	Utils.bindFormatOnTextfield("mode1_tbproductAmbiguousImageFlg", Utils._TF_NUMBER);
	Utils.bindFormatOnTextfield("mode1_tbselectCode1", Utils._TF_NUMBER);
	Utils.bindFormatOnTextfield("mode1_tbselectCode2", Utils._TF_NUMBER);
	Utils.bindFormatOnTextfield("mode1_tbselectCode3", Utils._TF_NUMBER);
	Utils.bindFormatOnTextfield("mode1_tbselectCode4", Utils._TF_NUMBER);
	Utils.bindFormatOnTextfield("mode1_tbselectCode5", Utils._TF_NUMBER);
	Utils.bindFormatOnTextfield("mode1_tbselectCode6", Utils._TF_NUMBER);
	Utils.bindFormatOnTextfield("mode1_tbselectCode7", Utils._TF_NUMBER);
	Utils.bindFormatOnTextfield("mode1_tbselectCode8", Utils._TF_NUMBER);
	Utils.bindFormatOnTextfield("mode1_tbselectCode9", Utils._TF_NUMBER);
	Utils.bindFormatOnTextfield("mode1_tbselectCode10", Utils._TF_NUMBER);

}


	
function mode1_clickButtonSearch(){
	
	$('#selectAllCheckbox').attr('checked',false);
	//--------jQuery("#btnSearch").click();
//	var validateResult = mode1_validateSearchFilter();
//    if (validateResult){
	//No validate, because I prevented user do wrong input:
        mode1_getSearchCondition();
        //Reload data and goto first page
        gridMode1.data("kendoGrid").dataSource.page(1);
        //Go to top of grid
        gridMode1.data("kendoGrid").content.scrollTop(0);
//    }
	//-------jQuery("#btnSearch").click();
	
}

//Validate search filter
/*function mode1_validateSearchFilter() {

	var productIdResult = validateNumbericAndBlank($("#tbProductId").val());
	var productSyouhinSysCodeResult = validateNumbericAndBlank($("#tbProductSyouhinSysCode").val());
	var productProperPriceFromResult = validateNumbericAndBlank($("#tbProductProperPriceFrom").val());
	var productProperPriceToResult = validateNumbericAndBlank($("#tbProductProperPriceTo").val());
	if (!productIdResult || !productSyouhinSysCodeResult || !productProperPriceFromResult || !productProperPriceToResult) {
		return false;
	}
	return true;
	
}
*/
///////////////////////////////////////////////////////////////////////////////////////////////sontt: variable/////////////////////////////////////////////////////////////////////////////
var ALL_IMPORTMODE = "1";
var COMPATIBLE_MODEL_IMPORTMODE = "2";
var ATTRIBUTE_IMPORTMODE = "3";
var dataSource;
var filterRow;

// Search condition
var matterNo = "";
var productId = "";
var productSyouhinSysCode = "";
var productBrandCode = "";
var brandName = "";
var productName = "";
var productCode = "";
var productEanCode = "";
var showProductRegistrationFlg = "";
var productGeneralErrorFlg = "";
var productCategoryErrorFlg = "";
var productModelErrorFlg = "";
var productAttributeErrorFlg = "";
var updatedUserId = "";
var updatedOnFrom = "";
var updatedOnTo = "";
// Confirm dialog of kendo
// @author hoang.ho
function showDialog (id, msg) {
	var divMsg = $(id).find("div.message");
	divMsg.text(msg); 
	var win = $(id).data("kendoWindow");
	win.center().open();
}

// Change status of import csv button
// 0 is disable
// 1 is enable
// 2 is change to submit save button - color red
// @author hoang.ho
function changeCSVImportButton (value) {
	var item = $("#importBtnCsvAnalyze");
	var analyText = $("#hdBtnCsvAnalyze").val();
	var reflectedText = $("#hdBtnCsvReflectedInDatabase").val();
	if (value == 0) {
		item.attr("disabled", "disabled");
		item.removeClass("btnRed");
		item.val(analyText);
		//BOE # Tran.Thanh 2014/07/09 : disable button export error csv
		$("#exportErrorCSV").attr("disabled", "disabled");
		//EOE # Tran.Thanh 2014/07/09 : disable button export error csv
	} else if (value == 1) {
		item.removeAttr("disabled");
		item.removeClass("btnRed");
		item.val(analyText);
	} else if (value == 2) {
		item.removeAttr("disabled");
		item.addClass("btnRed");
		item.val(reflectedText);
	}
	
}


$(window).load(function(){
	/**
	 * BOF Handle keyup tab-key on field filter
	 * @rcv!cao.lan 2014/03/10
	 * **/
	$('.k-grid-header-wrap').keyup(function(e) {
	    var code = e.keyCode || e.which;
	    // code = 9 is tab-key
	    if (code == '9') {
	    	// get scroll left in grid header 
	    	var left = $('.k-grid-header-wrap').scrollLeft();
	    	// if tab-key and scroll left change then scroll left k-grid-content
    		$(".k-grid-content").scrollLeft(left);
	    }
	 });
	
	/** EOF @rcv!cao.lan 2014/03/10 **/
});
/* BOE initImageBrowser @rcv!Le.Dinh 2014/03/11. */
function initImageBrowser() {
	 $("#imageBrowser").kendoImageBrowser({
	        transport: {
	             read: {
	            	 url:"readImageDescriptionSentence.html",
	            	 type: "POST",
	            	 dataType : "json",
	            	 data: {
	            		  matterNo: $("#hdMatterNo").val()  
	            	  }
	             },
	             destroy: {
	            	 url:"deleteImageDescriptionSentence.html",
	            	 type: "POST",
		           	 data: {
		        		  matterNo: $("#hdMatterNo").val()  
		        	 }
	             },
	             thumbnailUrl : function (argument,fileName) {
	             	return $("#hTmpImage").val() +"/" + $("#hdMatterNo").val() + "/" + fileName;
	             },
	             uploadUrl: "upLoadImageDescriptionSentence.html?matterNo=" + $("#hdMatterNo").val(),
	             imageUrl : function (fileName) {
	              	return $("#hTmpImage").val() +"/" + $("#hdMatterNo").val() + "/" + fileName;
	             }
	        }
	 });
}
/* EOE initImageBrowser @rcv!Le.Dinh 2014/03/11. */
// -------------------------------------------------------------------------------
$(document).ready(function() {
	//remove button insert
	$("#insertImgBtn").css("display","none");
	
	/* BOE process upload image @rcv!Le.Dinh 2014/03/11. */
	$("#dialogImageBrowser").kendoWindow({
		  height:450,
		  width : 700,
		  maxWidth : 900,
		  maxHeight : 600,
		  resizable : false,
		  modal : true,
		  visible : false,
		  draggable : false,
		  //BOE Tran.Thanh Fixbug muti image browser 2014/03/31
		  open : function () {
			  if (!hadInitImageBrowser) {
				  initImageBrowser();
				  hadInitImageBrowser = true;
			  }
		  }
		  //EOE Tran.Thanh Fixbug muti image browser 2014/03/31
		 });

		 $('#btnimageBrowser').click(function(event) {
//		  var dialogImageBrowser = $("#dialogImageBrowser").data("kendoWindow");
//		   dialogImageBrowser.center().open();
			 openDialogUploadImage();
			 
		   //BOE Tran.Thanh : Fixbug show many image browser 2014/03/31
		   //initImageBrowser();
		   //EOE Tran.Thanh : Fixbug show many image browser 2014/03/31
		 });
	/* EOE process upload image @rcv!Le.Dinh 2014/03/11. */
	/* BOE fix authoriy by Luong.Dai 2014/03/24 */
	ROLE = $('#role').val();
	/* EOE fix authoriy by Luong.Dai */
    //boe Chuong O
    $("#exportItemsCheckBoxAll").on("click",function() {
        var exportItemsCheckBoxAllStatus = jQuery("#exportItemsCheckBoxAll").prop('checked');
        jQuery("#popup_csv input[type='checkbox']").each(function(){
        	if($(this).attr("disabled") != "disabled") {
        		$(this).prop('checked', exportItemsCheckBoxAllStatus);
        	}
        });
    });
    /*    BOE Nguyen.Chuong 2014/03/14: Modify to export product all page when checkbox all checked.*/
    function createHiddenInputParam(name, value){
        var hiddenField = document.createElement("input");
        hiddenField.setAttribute("name", name);
        hiddenField.setAttribute("value", value);
        return hiddenField;
    };
    //Get filter param to export CSV all page.
    function setParamForExportCSV(form, productIds, headerList, attributeCodes, matterNo, exportAll, sortField, sortDir) {
        //Filter of grid
        form.appendChild(createHiddenInputParam("matterDetailActionForm.product.matterNo", $("#matterNo").text()));

		form.appendChild(createHiddenInputParam("matterDetailActionForm.product.productId", productId));
		form.appendChild(createHiddenInputParam("matterDetailActionForm.product.productSyouhinSysCode", productSyouhinSysCode));
		form.appendChild(createHiddenInputParam("matterDetailActionForm.product.productBrandCode", productBrandCode));
		form.appendChild(createHiddenInputParam("matterDetailActionForm.product.brandName", brandName));
		form.appendChild(createHiddenInputParam("matterDetailActionForm.product.productName", productName));
		form.appendChild(createHiddenInputParam("matterDetailActionForm.product.productCode", productCode));
		form.appendChild(createHiddenInputParam("matterDetailActionForm.product.productEanCode", productEanCode));
		form.appendChild(createHiddenInputParam("matterDetailActionForm.product.showProductRegistrationFlg", showProductRegistrationFlg));
		form.appendChild(createHiddenInputParam("matterDetailActionForm.product.productGeneralErrorFlg", productGeneralErrorFlg));
		form.appendChild(createHiddenInputParam("matterDetailActionForm.product.productCategoryErrorFlg", productCategoryErrorFlg));
		form.appendChild(createHiddenInputParam("matterDetailActionForm.product.productModelErrorFlg", 	productModelErrorFlg));
		form.appendChild(createHiddenInputParam("matterDetailActionForm.product.productAttributeErrorFlg", productAttributeErrorFlg));
		form.appendChild(createHiddenInputParam("matterDetailActionForm.product.updatedUserId",	updatedUserId));
		form.appendChild(createHiddenInputParam("matterDetailActionForm.product.updatedOnFrom",	updatedOnFrom));
		form.appendChild(createHiddenInputParam("matterDetailActionForm.product.updatedOnTo", updatedOnTo));
		form.appendChild(createHiddenInputParam("matterDetailActionForm.product.sortField", sortField));
		form.appendChild(createHiddenInputParam("matterDetailActionForm.product.sortDir", sortDir));

        //Add list syouhinSysCodes.
        form.appendChild(createHiddenInputParam("matterDetailActionForm.productIds", productIds));
        form.appendChild(createHiddenInputParam("matterDetailActionForm.exportAll", exportAll));
        form.appendChild(createHiddenInputParam("matterDetailActionForm.headerList", headerList));
        form.appendChild(createHiddenInputParam("matterDetailActionForm.attributeCodes", attributeCodes));
    }
    
    $("#btnCSVStartExport").on("click", function() {
        var checkCount = $("#table-content .productCheckbox:checked").length;
        if (checkCount <= 0) {
            return;
        }
        $('#pleaseSelectDialog').css('display','none');
        
        var exportMode = $("select[name='selectCsvMode']").val();
        var productIds = "";
        $("input:checkbox[name='productCheckbox']:checked").each(function() {
            productIds += "," + $(this).attr("id");
        });
        if (productIds.length > 0) {
            // remove the first ","
            productIds = productIds.replace(",", "");
        }
        var matterNo = $("#matterNo").text();
        var requestURL = "";
        var exportAll = $("#checkBoxAll").prop("checked");
        //Check number of product to export is over max when all product checkBox is checked.
        if(exportAll) {
          //Check number of product to export is over max.
            var maxNumberExport = jQuery("#maxNumberProductExportCSV").val();
            if($("#grid").data("kendoGrid").dataSource.total() > maxNumberExport) {
                //Show error message
                $.when(kendo.ui.ExtAlertDialog.show({
                    title: "", 
                    label: jQuery("#confirmYes").val(),
                    message: $.format(jQuery("#maxProductExportCSVErrorMsg").val(), maxNumberExport),
                    width: 393,
                    icon: "k-ext-information" })
                ).done();
            }
        }
        var headerList = "";
        var attributeCodes = $("#csv_export_attr").val();
        if (exportMode == "1") {
            requestURL = "exportCSVMode1";
            // mode 1

            $("#exportItems input:checked").each(function() {
                headerList += "," + $(this).attr("id");
            });
            if (headerList.length > 0) {
                // remove the first ","
                headerList = headerList.replace(",", "");
            }
            if (attributeCodes != null) {
                attributeCodes = attributeCodes.join();
            } else {
                attributeCodes = "";
            }
            
            /* BOE Fix bug #9 cannot export all CSV file @rcv!nguyen.hieu 2014/03/10. */
            /*
            url = "exportCSVMode1.html?productIds=" +  productIds + "&&headerList=" + headerList 
                + "&&attributeCodes=" + attributeCodes + "&&matterNo=" + matterNo;
                */
//            var checkAllChecked = $("#checkBoxAll").prop("checked");
//            if(checkAllChecked) {
//            	url = "exportCSVMode1.html?productIds=" +  productIds + "&&headerList=" + headerList 
//                + "&&attributeCodes=" + attributeCodes + "&&matterNo=" + matterNo + "&&exportAll=true";	
//            } else {
//                url = "exportCSVMode1.html?productIds=" +  productIds + "&&headerList=" + headerList 
//                + "&&attributeCodes=" + attributeCodes + "&&matterNo=" + matterNo;
//            }

            /* EOE Fix bug #9 cannot export all CSV file. */
        } else if (exportMode == "2") {
            // mode 2
//            url = "exportCSVMode2.html?productIds=" +  productIds + "&&matterNo=" + matterNo;
            requestURL= "exportCSVMode2";
        } else {
            // mode 3
            requestURL = "exportCSVMode3";
            if (attributeCodes != null) {
                attributeCodes = attributeCodes.join();
            } else {
                attributeCodes = "";
            }
//            url = "exportCSVMode3.html?productIds=" +  productIds + "&&attributeCodes=" + attributeCodes + "&&matterNo=" + matterNo;
        }
//        window.location.href = url;
        
        var form  =  document.createElement("form");
        form.setAttribute("method", "post");
        form.setAttribute("action", "./" + requestURL + ".html");

        //Create filter param
        setParamForExportCSV(form, productIds, headerList, attributeCodes, matterNo, exportAll, sortFieldCSV, sortDirCSV);
        document.body.appendChild(form);
        form.submit();

        document.body.removeChild(form);
    });
    /*    EOE Nguyen.Chuong 2014/03/14*/
    //eoe Chuong O
    // BOE hoang.ho 2014/02/27 init process bar
    // define common dialog
	 $("#alertDialog, #confirmReflectDialog, #confirmCancelDialog").kendoWindow({
	        minWidth: 270,
	        resizable: false,
	        title: $("#hDialogTitle").val(),
	        modal: true,
	        visible: false
	});
	 // define button close dialog
	$("#btnAlertOK, #btnConfirmReflectCancel, #btnCancelReflectDataCancel").click(function(evt) {
		 var divDialog = $(this).closest('div.dialog');
		 var id = divDialog.attr("id");
		 $("#"+id).data("kendoWindow").close();
	});

	$("#btnConfirmReflectOK").click(function(evt){
		// Process ajax process data
		$.ajax({
	            url: 'reflectCSVData.html',
	            type: 'POST',
	            data: {
	            	"importMode": $("#importMode").val(),
	            	"matterDetailActionForm.entMstFactoryMatterNew.matterNo": $("#hdMatterNo").val(),
	            	"matterDetailActionForm.importedCSVTableName" :  $("#hdImportedCSVTableName").val(),
	            	"matterDetailActionForm.errorCSVTableName" :  $("#hdErrorCSVTableName").val(),
	            	"matterDetailActionForm.realFileName" :  $("#hdRealFileName").val(),
	            	"matterDetailActionForm.csvfileName":  $("#hdCsvfileName").val(),
	            	"matterDetailActionForm.splitSymbol":  $("#importComboboxFieldDelimiter").val(),
	            	"matterDetailActionForm.boundedFieldSymbol":  $("#boundedSymbolTxt").val(),
	            	"matterDetailActionForm.countNewProductInCSVFile":  $("#hCountNewProductInCSV").val()
	            },
	            cache: false,
	            dataType: 'json',
	            success: function(data)
	            {
	            	if (data.messageErrorDeadlock != null) {
	            		//if exception deadlock
	            		viewMessage(data.messageErrorDeadlock, 0);
	            		deletedTempTable();
	            	} else {
	            		//else orther exception
	            		viewMessage($("#hdCsvReflectDataSuccess").val(), 0);
	            	}
	            },
	            error : function(data) {
	            	//viewMessage($("#hdCvReflectDataFail").val(), 1);
	            	viewMessage($("#hmDeadlock").val(), 0);
	            	deletedTempTable();
	            },
	            complete: function(data){
	            	$("#fileupload").val('');
	            	$("#popup_csv").data("kendoWindow").close();
	            	resetFilterConditionPopup();
	            	searchProductByConditon();
	            	//deletedTempTable();
	            	hideProcessBar();
	            }
	        });
	        // ** BOE hoang.ho 2014/02/27 **
		$("#confirmReflectDialog").data("kendoWindow").close();
		showProcessBar();
	});
	$("#btnCancelReflectDataOK").click(function(evt){
		//BOE @rcv!Tran.Thanh 2014/07/11 #9956 : add logic delete error csv when have cancel import
		if (!importValid) {
			//File CSV import was not valid, then delete error file on server
			deleteErrorCSV();
		} else {
			//File CSV import was not valid, then delete temporary table on db
			cancelImportCSV();
		}
		//EOE @rcv!Tran.Thanh 2014/07/11 #9956 : add logic delete error csv when have cancel import
		$("#confirmCancelDialog").data("kendoWindow").close();
		showProcessBar();
	});
	
	function deleteErrorCSV() {
		$.ajax({
            url: 'deleteErrorCSV.html',
            type: 'POST',
            data: {
            	"pathErrorCSV" : $("#fileuploadName").val()
            },
            cache: false,
            dataType: 'json',
            success: function(data) {
            	viewMessage($("#hdCsvCancelImportErrorMsg").val(), 0);
            },
            complete: function(data){
            	$('#gridMode1').hide();
                $('#gridCSVFitModel').hide();
                $('#gridAttribute').hide();
            	$('#csvfieldsetImport').show();
            	$("#importMode").data("kendoDropDownList").enable(true);
            	//Reset search filter
            	resetFilterConditionPopup();
            	// reset import mode.
            	$("#importCountStatus").hide();
            	changeCSVImportButton(0);
            	var tooltips = $("#importCountStatus").data("kendoTooltip");
                if (tooltips != null && tooltips != undefined) {
                	tooltips.destroy();
                }
            	// show progress bar
                var pb = $("#progressBar").data("kendoProgressBar");
                pb.value(false);
                $("#progressBar").hide();
            	$("#fileupload").val('');
            	changeCSVImportButton(0);
            	if (closeBtnFlg) {
            		$("#popup_csv").data("kendoWindow").close();
            	}
            	hideProcessBar();
            }
        });
	}
	
	function cancelImportCSV() {
		$.ajax({
            url: 'cancelImportCSVData.html',
            type: 'POST',
            data: {
            	"importMode": $("#importMode").val(),
            	"matterDetailActionForm.importedCSVTableName" :  $("#hdImportedCSVTableName").val(),
            	"matterDetailActionForm.errorCSVTableName" :  $("#hdErrorCSVTableName").val(),
            },
            cache: false,
            dataType: 'json',
            success: function(data)
            {
            	viewMessage($("#hdCsvCancelImportErrorMsg").val(), 0);
            },
            complete: function(data){
            	$('#gridMode1').hide();
                $('#gridCSVFitModel').hide();
                $('#gridAttribute').hide();
            	$('#csvfieldsetImport').show();
            	$("#importMode").data("kendoDropDownList").enable(true);
            	//Reset search filter
            	resetFilterConditionPopup();
            	// reset import mode.
            	$("#importCountStatus").hide();
            	changeCSVImportButton(0);
            	var tooltips = $("#importCountStatus").data("kendoTooltip");
                if (tooltips != null && tooltips != undefined) {
                	tooltips.destroy();
                }
            	// show progress bar
                var pb = $("#progressBar").data("kendoProgressBar");
                pb.value(false);
                $("#progressBar").hide();
            	$("#fileupload").val('');
            	changeCSVImportButton(0);
            	if (closeBtnFlg) {
            		$("#popup_csv").data("kendoWindow").close();
            	}
            	hideProcessBar();
            }
        });
	}
	
    $("#progressBar").kendoProgressBar({
        min: 0,
        max: 100,
        type: "value",
        animation: false/*{duration : 100}*/,
        change: function(e) {
        	this.progressStatus.css("width","96%");
        	//BOE #7872 Tran.Thanh : fix bug 100% view wrong position
            //this.progressStatus.html($.format("{0}<span style='float:right'>100%</span>",$("#fileuploadName").val()));
            this.progressStatus.html($.format("<span id='nameCSV' style=''>{0}</span><span id='percent' style=''>100%</span>",$("#fileuploadName").val()));
            //EOE #7872 Tran.Thanh : fix bug 100% view wrong position
        }
    });
    $("#progressBar").hide();

    $("#fileupload").click(function(e){
    	closeBtnFlg = false;
    	var value = $(this).val();
    	// test value is exist or not.
    	var currentText = $("#importBtnCsvAnalyze").val();
    	var reflectedText = $("#hdBtnCsvReflectedInDatabase").val();
    	if (value != null && value != undefined && value != '' && currentText == reflectedText) {
    		showDialog("#confirmCancelDialog", $("#hdConfirmCancelReflectCsvMsg").val());
//    		$(this).val('');
    		e.preventDefault();
    		e.stopPropagation();
    	}
    });
    $("#fileupload").change(function(e){
    	if ($(this).val() != '') {
    		changeCSVImportButton(1);
    	} else {
    		changeCSVImportButton(0);
    	}
    });
	changeCSVImportButton(0);
    // EOE hoang.ho 2014/02/27 init process bar 
///////////////////////////////////////////////////////////////////////////////////////////////sontt: document ready/////////////////////////////////////////////////////////////////////////////
	/*jQuery(document).on("keypress", "input.tbSearch", function(e) {
		if (e.keyCode == 13) {
			//jQuery("#btnSearch").click();
			mode1_clickButtonSearch();
	    }
	});*/
    $("#gridMode1").on("keypress", "input.tbSearch", function(e) {
		if (e.keyCode == 13) {
			//jQuery("#btnSearch").click();
			mode1_clickButtonSearch();
	    }
	});
	$("#gridCSVFitModel").on("keypress", "input.tbSearch", function(e) {
		if (e.keyCode == 13) {
			clickButtonSearch();
	    }
	});
	//sontt-comment:
	//mode1_initGrid();
	//begin hide grids
	$('#gridMode1').hide();
	$('#gridCSVFitModel').hide();
	$('#gridAttribute').hide();
	//end 
///////////////////////////////////////////////////////////////////////////////////////////////sontt: end document ready/////////////////////////////////////////////////////////////////////////////
	//boe thai.son
	initPleaseSelectDialog();
	//eoe thai.son
	// Show progress bar for loadding grid
	showProcessBar();
	// Create filter row in grid
	filterRow = $('<tr>'
			+ '<td></td>'
			+ '<td></td>'
			/* BOE by Luong.Dai change maxLength of productId to P + 18 number, syouhinSySCode 18 number*/
			/*+ '<td><input class="tbSearch filter k-textbox" maxLength="21" 	type="text" id="tbProductId"/></td>'
			+ '<td><input class="tbSearch filter k-textbox" maxLength="20" 	type="text" id="tbProductSyouhinSysCode"/></td>'*/
			+ '<td><input class="tbSearch filter k-textbox" maxLength="19" 	type="text" id="tbProductId"/></td>'
			+ '<td><input class="tbSearch filter k-textbox" maxLength="18" 	type="text" id="tbProductSyouhinSysCode"/></td>'
			/* BOE by Luong.Dai change maxLength of productId to P + 19 number, syouhinSySCode 18 number*/
			+ '<td><input class="tbSearch filter k-textbox" maxLength="8" 	type="text" id="tbProductBrandCode"/></td>'
			+ '<td><input class="tbSearch filter k-textbox" maxLength="50"	type="text" id="tbName"/></td>'
			+ '<td><input class="tbSearch filter k-textbox" maxLength="512"	type="text" id="tbProductName"/></td>'
			+ '<td><input class="tbSearch filter k-textbox" maxLength="100"	type="text" id="tbProductCode"/></td>'
			+ '<td><input class="tbSearch filter k-textbox" maxLength="40"	type="text" id="tbProductEanCode"/></td>'
			+ '<td>'
			+ jQuery("#hiddenRegistrationFlg").val()
			+ '</td>'
			+ createComboboxYesNo("tbproductGeneralErrorFlg")
			+ createComboboxYesNo("tbproductCategoryErrorFlg")
			+ createComboboxYesNo("tbNatterFactoryErrorCountModel")
			+ createComboboxYesNo("tbproductAttributeErrorFlg")
			+ '<td>' + jQuery("#hiddenListUser").val()
			+ '</td>' + customHtmlDateInput() + '</tr>');
	// dataSource for brand master grid
	dataSource = new kendo.data.DataSource({
		serverPaging : true,
		autoBind : false,
		// 50 product in 1 page
		pageSize : 50,
		serverSorting : true,
		transport : {
			read : {
				type : "POST",
				dataType : "json",
				url : "ajaxGetProductList.html",
				data : function(data) {
					var sortField = "productId";
					var sortDir = "asc";
					if (data != undefined
							&& data.sort != undefined
							&& data.sort.length > 0) {
						sortField = data.sort[0].field;
						sortDir = data.sort[0].dir;
					}
					return setDataForDataSource(sortField,
							sortDir);
				},
				cache : false,
				complete : function (data,status) {
					if (status == "success") {
						//Set total product of matter
						totalProductOfMatter = $.parseJSON(data.responseText).matterDetailActionForm.numberProduct;
						//BOE Tran.Thanh 2014/06/05 : add logic disable checkbox select all when no data filter
						var countFilter = $.parseJSON(data.responseText).matterDetailActionForm.count;
						if (countFilter == "0") {
							$("#checkBoxAll").attr("disabled", true);
						} else {
							$("#checkBoxAll").attr("disabled", false);
						}
						//EOE Tran.Thanh 2014/06/05 : add logic disable checkbox select all when no data filter
					}
				}
			}
		},
		schema : {
			data : "matterDetailActionForm.listProduct",
			total : "matterDetailActionForm.count"
		}
	});

	var grid = initGrid();
	grid.data("kendoGrid").thead.append(filterRow);
	
	jQuery("#size").kendoDropDownList({
		change: function() {
			// BOE: Triet.Nguyen
			// $("#lstButton .k-dropdown-wrap").css("background-color",
			// "#FFFFFF");
			$("#lstButton .k-dropdown-wrap").css("background-color", "none");
			// EOE: Triet.Nguyen
		}
	});
	jQuery("#lstUser").kendoDropDownList();
	/* BOE by Luong.Dai fix authority at 2014/03/24 */
	if (ROLE.indexOf(limitRole) >= 0
		&& ROLE.indexOf(generalRole) < 0
		&& ROLE.indexOf(matterRole) < 0) {
		//User has role is limitedUser
		//Disable cbx userId
		jQuery("#lstUser").data('kendoDropDownList').enable(false);
	}
	/* EOE by Luong.Dai fix authority */
	// Set kendo theme for combobox in filter
	jQuery("#cbbRegistrationFlg").kendoDropDownList();
	jQuery("#tbproductGeneralErrorFlg").kendoDropDownList();
	jQuery("#tbproductCategoryErrorFlg").kendoDropDownList();
	jQuery("#tbNatterFactoryErrorCountModel").kendoDropDownList();
	jQuery("#tbproductAttributeErrorFlg").kendoDropDownList();
	jQuery("#tbUpdatedUserId").kendoDropDownList();
	// Press enter in textbox bunrui search
	jQuery(document).on("keypress", "#tbProductId"
							+ ", #tbProductSyouhinSysCode"
							+ ", #tbProductBrandCode"
							+ ", #tbName" + ", #tbProductName"
							+ ", #tbProductCode"
							+ ", #tbProductEanCode"
							+ ", #tbUpdatedUserId"
							+ ", #tbUpdatedOnFrom"
							+ ", #tbUpdatedOnTo", function(e) {
		if (e.keyCode == 13) {
			searchProductByConditon();
		}
	});

	// Change value at cbbRegistrationFlg
	jQuery(" #cbbRegistrationFlg,"
			+ "#tbproductGeneralErrorFlg, "
			+ "#tbproductCategoryErrorFlg, "
			+ "#tbNatterFactoryErrorCountModel, "
			+ "#tbproductAttributeErrorFlg, "
			+ "#tbUpdatedUserId,"
			+ "#tbUpdatedOnFrom,"
			+ "#tbUpdatedOnTo").change(function() {
		searchProductByConditon();
	});

	// confirmDeleteProducts()
	confirmDeleteProducts();
	// updateMatter
	updateMatter();
	// checkIssue exist or not exist
	checkExistIssue();
						
	// Add datePicker for updateOn textBox.
	kendo.culture("ja-JP");
	
	// kendo for updateOn From textBox.
	//BOE: Triet.Nguyen
	jQuery("#tbUpdatedOnFrom").kendoDateTimePicker({
		interval: 60,
        format: "yyyy/MM/dd HH:mm:ss",
        timeFormat: "HH:mm:ss",
	});	
	// Set value kendo object.
	updateOnFromKendo = jQuery("#tbUpdatedOnFrom").data("kendoDateTimePicker");

	// kendo for updateOn To textBox.
	//BOE: Triet.Nguyen
	jQuery("#tbUpdatedOnTo").kendoDateTimePicker({
		interval: 60,
        format: "yyyy/MM/dd HH:mm:ss",
        timeFormat: "HH:mm:ss",
	});	
	// Set value kendo object.
	updateOnToKendo = jQuery("#tbUpdatedOnTo").data("kendoDateTimePicker");
	//EOE: Triet.Nguyen

	// Open datePicker when focus
	//jQuery("#tbUpdatedOnFrom").focus(function() {
	//	updateOnFromKendo.open();
	//});
	//jQuery("#tbUpdatedOnTo").focus(function() {
	//	updateOnToKendo.open();
	//});

	// Validate From search value of kousinDate
	//jQuery("#tbUpdatedOnFrom").focusout(function() {
	//	validateTextboxKousinDate("tbUpdatedOnFrom");
	//});

	// Validate To search value of kousinDate
	//jQuery("#tbUpdatedOnTo").focusout(function() {
	//	validateTextboxKousinDate("tbUpdatedOnTo");
	//});
	
	$("#updatedOnFormSearch").focusout(function() {
    	validateDateTimePicker('#updatedOnFormSearch', '#updatedOnFormSearch', '#updatedOnFormToSearch');
    });
    $("#updatedOnFormToSearch").focusout(function() {
    	validateDateTimePicker('#updatedOnFormToSearch', '#updatedOnFormSearch', '#updatedOnFormToSearch');
    });

	// Set input numberic only in textbox search filter
	inputNumbericOnly();

	// Validate textbox matterName
	$("#matterName").change(function() {
		validateTextboxMatterName();
	});
	// Reset value in screen
	resetValueInScreen();
	//BOE thai.son refix this bug
	// BOE 2014/01/29 Nguyen.Chuong add double click handler
	// double click to row in grid then redirect to fitModelCheck page.
    // BOE: Triet.Nguyen
	
	/* BOE Fix bug #8 double clicking moves to Product Edit  @rcv!nguyen.hieu 2014/03/11. */
	/*
    $("#grid").on("dblclick", "tr.k-state-selected", function () {
        var productId = $(this).find("td:first input").attr("id");
        jQuery("#productIdSubmitParam").val(productId);
        // Submit form
        jQuery("#submitForm").submit();
        
    });
    */
	/*
	* @Author: Nguyen.Chuong
	* @Date: 2014/04/17
	* @Description: current filter of mainGrid to form.
	* @param: 
	* + form: form to submit to productEdit page.
	*/
	function setParamToFormRedirectProductEdit(form) {
//	    var keyWordProductId = $('#tbProductId').val();
//	    if (keyWordProductId != null && keyWordProductId != undefined && keyWordProductId != "") {
//	        if (keyWord[0] == "P") {
//	            //remove P and sent keyWord to server
//	            return keyWord.substring(1);
//	        }
//	        return ;
//	    }
//	    return null;
        form.appendChild(createHiddenInputParam("form.productSearch.productId",                     productId.trim()));
        form.appendChild(createHiddenInputParam("form.productSearch.productSyouhinSysCode",         productSyouhinSysCode.trim()));
        form.appendChild(createHiddenInputParam("form.productSearch.productBrandCode",              productBrandCode.trim()));
        form.appendChild(createHiddenInputParam("form.productSearch.brandName",                     brandName.trim()));
        form.appendChild(createHiddenInputParam("form.productSearch.productName",                   productName.trim()));
        form.appendChild(createHiddenInputParam("form.productSearch.productCode",                   productCode.trim()));
        form.appendChild(createHiddenInputParam("form.productSearch.productEanCode",                productEanCode.trim()));
        form.appendChild(createHiddenInputParam("form.productSearch.showProductRegistrationFlg",    showProductRegistrationFlg.trim()));
        form.appendChild(createHiddenInputParam("form.productSearch.productGeneralErrorFlg",        productGeneralErrorFlg.trim()));
        form.appendChild(createHiddenInputParam("form.productSearch.productCategoryErrorFlg",       productCategoryErrorFlg.trim()));
        form.appendChild(createHiddenInputParam("form.productSearch.productModelErrorFlg",          productModelErrorFlg.trim()));
        form.appendChild(createHiddenInputParam("form.productSearch.productAttributeErrorFlg",      productAttributeErrorFlg.trim()));
        form.appendChild(createHiddenInputParam("form.productSearch.updatedUserId",                 updatedUserId.trim()));
        form.appendChild(createHiddenInputParam("form.productSearch.updatedOnFrom",                 updatedOnFrom.trim()));
        form.appendChild(createHiddenInputParam("form.productSearch.updatedOnTo",                   updatedOnTo.trim()));
        var gridData = $("#grid").data("kendoGrid").dataSource;
        //Set page and pageSize param
        form.appendChild(createHiddenInputParam("form.page",                                        gridData.page()));
        form.appendChild(createHiddenInputParam("form.pageSize",                                    gridData.pageSize()));
        //Set sortField and sortDir
        var sortFieldParam = "productId";
        var sortDirParam = "asc";
        if (gridData != undefined && gridData.sort() != undefined && gridData.sort.length > 0) {
            sortFieldParam = gridData.sort()[0].field;
            sortDirParam = gridData.sort()[0].dir;
            if (sortFieldParam == "showProductSyouhinSysCode") {
                sortFieldParam = "productSyouhinSysCode";
            }
        }
        form.appendChild(createHiddenInputParam("form.productSearch.sortField",                     sortFieldParam));
        form.appendChild(createHiddenInputParam("form.productSearch.sortDir",                       sortDirParam));
        form.appendChild(createHiddenInputParam("form.searchFromMatterDetailFlg",                   true));
	}
	
	function handleGridRowDoubleClick() {
		$("#grid").on("dblclick", ".k-grid-content tr", function(){
			// Get productId & syouhinSysCode.
			var productId = $(this).find(".productCheckbox").attr("id");
//			var syouhinSysCode = $(this).find("td:nth-child(3)").text();
			//Create action value
			var actionString = "./" + "productEdit" + ".html?";
			//add param productId.
			actionString += "productId=" + productId;
			//Add param syouhinsysCode.
//			actionString += "&sysCode=" + syouhinSysCode;
			/* BOE by Luong.Dai show progress bar at 2014/04/01 */
			showProcessBar();
			/* EOE by Luong.Dai show progress bar */
			// Redirect to Product Edit.
			/*BOE #7205 Nguyen.Chuong 2014/04/17 change to submit form to set param filter redirect productEdit page.*/
//			window.location = "productEdit.html?productId=" + productId + "&sysCode=" + syouhinSysCode;
			var form  =  document.createElement("form");
			form.setAttribute("method", "post");
			form.setAttribute("action", actionString);
			//Create filter param
	        setParamToFormRedirectProductEdit(form);
	        document.body.appendChild(form);
	        form.submit();
	        document.body.removeChild(form);
			/*EOE #7205 Nguyen.Chuong 2014/04/17 change to submit form to set param filter redirect productEdit page.*/
		});
	}
	handleGridRowDoubleClick();
	/* EOE Fix bug #8 double clicking moves to Product Edit. */
	
	/*$("#grid").on("dblclick", function () {
        var productId = $(this).find("td:first input").attr("id");
        jQuery("#productIdSubmitParam").val(productId);
        // Submit form
        jQuery("#submitForm").submit();
        
    });*/
	//BOE thai.son refix this bug
    // dialog
    $("#confirmBackDialog").kendoWindow({
        maxWidth: 400,
        resizable: false,
        modal: true,
        visible: false,
        title: $("#title-confirm-back").val(),
        activate: function () {
            jQuery("#fancyConfirm_cancel").focus();
        }
	});
    $("#fancyConfirm_cancel").click(function(e) {
        e.preventDefault();
        var win = $("#confirmBackDialog").data("kendoWindow");
        win.close();
    });
    // EOE: Triet.Nguyen
    //boe thai.son
    $("#confirmDeleteDialog").kendoWindow({
        maxWidth: 400,
        resizable: false,
        modal: true,
        visible: false,
        title: $("#windowNamedeleteDialog").val(),//$("#title-confirm-back").val(),
        activate: function () {
            jQuery("#confirmDelete_cancel").focus();
        }
	});
    $("#confirmDelete_ok").click(function(e) {
    	deleteProducts();
        var win = $("#confirmDeleteDialog").data("kendoWindow");
        win.close();
    });
    $("#confirmDelete_cancel").click(function(e) {
        e.preventDefault();
        var win = $("#confirmDeleteDialog").data("kendoWindow");
        win.close();
    });
    //eoe thai.son
    
    $("#popup_csv").kendoWindow({
        width : 950,
        height : 630,
        title : $("#csv_popup_title").val(),
        resizable : false,
        modal : true,
        visible : false,
        actions: [
            "Close"
        ],
        close : function(e) {
        	closeBtnFlg = true;
        	var value = $("#fileupload").val();
        	// test value is exist or not.
        	var currentText = $("#importBtnCsvAnalyze").val();
        	var reflectedText = $("#hdBtnCsvReflectedInDatabase").val();
        	if (value != null && value != undefined && value != '' && currentText == reflectedText) {
        		showDialog("#confirmCancelDialog", $("#hdConfirmCancelReflectCsvMsg").val());
        		e.preventDefault();
        	}
        }
    });
    
    $("#tabstrip").kendoTabStrip({
        animation:  {
            open: {
                effects: "fadeIn"
            }
        }
    });
    $("#csvmode").kendoDropDownList({
        change: function() {
            $("#lstButton .k-dropdown-wrap").css("background-color", "none");
            $("#exportItems ul").css('display','none');
            $("#field_csv_export_attr").css('display','none');
            
//            //disable or enable input:
//            if($("#csvmode").val() == ALL_IMPORTMODE){
//            	//Luc nay dung check het:
//            	$('#exportItems :checkbox').attr({"disabled": false/*,"checked": true*/});
//            	$("#csv_export_attr").data("kendoMultiSelect").enable(true);
//            	
//            }else if($("#csvmode").val() == ATTRIBUTE_IMPORTMODE){
//            	//disable checkbox, enable multiselect
//            	$('#exportItems :checkbox').attr({"disabled": true/*,"checked": false*/});
//            	$("#csv_export_attr").data("kendoMultiSelect").enable(true);
//            }else{
//            	//disable both: disable checkbox but don't remove check
//            	$('#exportItems :checkbox').attr({"disabled": true/*,"checked": false*/});
//        		$("#csv_export_attr").data("kendoMultiSelect").enable(false);
//            }

            var id_ul_mod = "#csv_ul_mod_" + $("#csvmode").val();
            $(id_ul_mod).css('display','');
            switch ($("#csvmode").val()) {
                case ALL_IMPORTMODE:
                    $(id_ul_mod + ' :checkbox').attr({"disabled": false});
                    $(id_ul_mod + ' .chkDisabled').attr({"disabled": true, "checked": true});
                    $("#field_csv_export_attr").css('display','');
                    $("#csv_export_attr_taglist").css('display','');                    
                    $("#csv_export_attr").data("kendoMultiSelect").enable(true);
                    break;
                case COMPATIBLE_MODEL_IMPORTMODE:
                    $(id_ul_mod + ' :checkbox').attr({"disabled": true,"checked": true});
                    $("#field_csv_export_attr").css('display','none');
                    $("#csv_export_attr_taglist").css('display','none');
                    $("#csv_export_attr").data("kendoMultiSelect").enable(false);
                    break;
                case ATTRIBUTE_IMPORTMODE:
                    $(id_ul_mod + ' :checkbox').attr({"disabled": true,"checked": true});
                    $("#field_csv_export_attr").css('display','');
                    $("#csv_export_attr_taglist").css('display','');
                    $("#csv_export_attr").data("kendoMultiSelect").enable(true);
                    break;
            }
        }
    });
    $('#csv_export_attr option[value=""]').remove();
    $("#csv_export_attr").kendoMultiSelect({});
    //tab import:
    jQuery("#importMode").kendoDropDownList();
    jQuery("#importComboboxFieldDelimiter").kendoDropDownList();

    //BOE @rcv!Tran.Thanh 2014/07/10 #9956 : create event click button export error csv
    $('#exportErrorCSV').click(function(){
    	var form  =  document.createElement("form");
        form.setAttribute("method", "post");
        form.setAttribute("action", "./" + "downloadErrorCSV" + ".html");
        form.style.display="none";
        //form.setAttribute("accept-charset", "shift-jis");

        //Create filter param
        form.appendChild(createHiddenInputParam("pathErrorCSV", $("#fileuploadName").val()));
        document.body.appendChild(form);
        form.submit();
    });
    //EOE @rcv!Tran.Thanh 2014/07/10 #9956 : create event click button export error csv
    
    $('#importBtnCsvAnalyze').click(function(){
    	if ($("#importBtnCsvAnalyze").val() == $("#hdBtnCsvReflectedInDatabase").val()) {
    		// call ajax start reflect database.
    		showDialog("#confirmReflectDialog", $("#hdConfirmReflectCsvMsg").val());
    		return;
    	}
    	changeCSVImportButton(0);
    	//begin hide grids
    	$('#gridMode1').hide();
        $('#gridCSVFitModel').hide();
        $('#gridAttribute').hide();
        $('#importCountStatus').hide();
        /* BOE #7206 Luong.Dai disable combobox mode */
        $("#importMode").data("kendoDropDownList").enable(false);
        /* EOE #7206 Luong.Dai disable combobox mode */
        //end 
        //hide fieldset:
    	$('#csvfieldsetImport').hide();
    	//end
    	if($("#importMode").val() == ALL_IMPORTMODE){
    		//myTestGridIndex++;
            //refresh lai grid, refresh html of gridMode1:
            var parent = $('#gridCSVFitModel').parent();
            $('#gridMode1').remove();
            parent.append('<div id="gridMode1"></div>');
            //remember do events 2 times with dynamic grid. If not title of grid is not show.
            //fix bug dynamic grid cannot send data filter to server When I push enter. This happen, because the event is died, so I do event again:
            $("#gridMode1").on("keypress", "input.tbSearch", function(e) {
        		if (e.keyCode == 13) {
        			//jQuery("#btnSearch").click();
        			mode1_clickButtonSearch();
        	    }
        	});
            uploadFiles();
    		//mode1_initGridVerSion2();
    		//$('#gridMode1').show();
        }else if($("#importMode").val() == COMPATIBLE_MODEL_IMPORTMODE){
        	//hoang.ho
        	uploadFiles();
        }else if(jQuery("#importMode").val() == ATTRIBUTE_IMPORTMODE) {
            //Import CSV to DB
            uploadFiles();
        }
    });

	/* BOE by Luong.Dai 2014/03/11 for MatterDetail */
	handleClickButtonNewProduct();
	/* EOE by Luong.Dai 2014/03/11 for MatterDetail */
	
	/* BOE by Tran.Thanh 2014/04/04 for fixbug maxSize upload */
	$('#msUploadImageSize').on('show', function(){
    	$.when(kendo.ui.ExtAlertDialog.show({
            title: "", 
            label: $("#confirmYes").val(),
            message: $('#msUploadImageSize').val(),
            icon: "k-ext-information" })
        ).done(function () {
            //console.log("User clicked the OK button");
        });
	});
	$('#msUploadDiskSize').on('show', function(){
        $.when(kendo.ui.ExtAlertDialog.show({
            title: "", 
            label: $("#confirmYes").val(),
            message: $('#msUploadDiskSize').val(),
            icon: "k-ext-information" })
        ).done(function () {
            //console.log("User clicked the OK button");
        });
    });
	/* EOE by Tran.Thanh 2014/04/04 for fixbug maxSize upload */
});

/* BOE by Luong.Dai 2014/03/11 for MatterDetail */
function handleClickButtonNewProduct() {
	$('#btnNewProduct').click(function(event) {
		//Check number has more than 50000 product
		if (totalProductOfMatter >= MAX_PRODUCT_OF_MATTER) {
			//Show message
			var msgWarning = $('#maxProductOfMatter').val();
			var lblOk = $("#confirmYes").val();
	    	$.when(kendo.ui.ExtAlertDialog.show({
			    title: "", 
			    label: lblOk,
			    message: msgWarning,
			    icon: "k-ext-information" })
			).done(function () {
			    console.log("User click OK button");
			});
		} else {
			//redirect to new product page
			var link = "./productEdit.html?matterNo=" + $('#hdMatterNo').val();
			window.location = link;
		}
	});
}
/* EOE by Luong.Dai 2014/03/11 for MatterDetail */

// checkIssue exist or not exist
function checkExistIssue() {
	// process get value of issue
	var issueId = $("#tooltip").text();
	issueId = issueId.replace("#", "");
	if (issueId == '') {
		return;
	}
	// before check issue exist or not => set text = ''
	$("#tooltip").text("");
	
	// load ajax to check current issue exist or not
	$.ajax({
		type : 'POST',
		url : 'selectIssueRedmineByIssueId.html',
		dataType : 'json',
		data : {
			'issueId' : issueId
		},
		success : function(data) {
			if (data['responeCode'] == 'success') {
				$("#tooltip").text("#" + issueId);
			} else {
				// change design tag <a> to <label>
				$("#tooltip").replaceWith("<label id=\"tooltip\" > </label>");
				$("#tooltip").text("#" + issueId);
				$("#tooltip").css("float", "left");
				$("#tooltip").css("color", "red");
			}
			hideProcessBar();
		}
	});
}

// load tooltip
function showTooltip() {
	var issueId = $("#tooltip").text();
	issueId = issueId.replace("#", "");
	if (issueId == '') {
		return;
	}
	/*BOE Nguyen.Chuong 2014/04/04 add check flow to show only one tootip and load content only one time.selectIssueRedmineByIssueId*/
	if ($("#tooltip").data("kendoTooltip") == undefined) {
	    $.ajax({
            type : 'POST',
            url : 'selectIssueRedmineByIssueId.html',
            dataType : 'json',
            data : {
                'issueId' : issueId
            },
            success : function(data) {
                loadkendoTooltip(data);
            }
        });
	} else {
	    $("#tooltip").data("kendoTooltip").show();
	}
	/*EOE Nguyen.Chuong 2014/04/04*/
}

// loadkendoTooltip()
function loadkendoTooltip(data) {
	var tooltip = $("#tooltip").kendoTooltip({
		content : function(e) {
			var html = initHtmlToolTip(data);
			return html;
		},
		position : "top",
		width : "auto",
		animation : {
			close : {
				effects : "fade:out"
			},
			open : {
				effects : "fade:in",
				duration : 1000
			}
		}
	}).data("kendoTooltip");
	tooltip.show($("#tooltip"));
}

// initHtmlToolTip
function initHtmlToolTip(data) {
	var subject = "";
	var assignedName = "";
	var startDate = "";
	var dueDate = "";

	if (data['subject']) {
		subject = data['subject'];
	}
	if (data['assignedName']) {
		assignedName = data['assignedName'];
	}
	if (data['startDate']) {
		startDate = data['startDate'];
	}
	if (data['dueDate']) {
		dueDate = data['dueDate'];
	}

	var html = "<table>" +
				// subject
				"<tr>" + "<td align=\"left\">" + $("#matterIssueSubject").val() + "</td>"
				+ "<td align=\"left\">" + ": " + subject + "</td>" + "</tr>" +
				// assignedName
				"<tr>" + "<td align=\"left\">" + $("#matterAssignedTo").val()
				+ "</td>" + "<td align=\"left\">" + ": " + assignedName + "</td>"
				+ "</tr>" +
				// startDate
				"<tr>" + "<td align=\"left\">" + $("#matterStartDate").val()
				+ "</td>" + "<td align=\"left\">" + ": " + startDate + "</td>"
				+ "</tr>" +
				// dueDate
				"<tr>" + "<td align=\"left\">" + $("#matterDueDate").val()
				+ "</td>" + "<td align=\"left\">" + ": " + dueDate + "</td>"
				+ "</tr>" +
			"</table>";
	return html;
}

// select all
function selectAllCheckbox() {
	$('.productCheckbox').each(function() {
		if ($('#checkBoxAll').attr('checked')) {
			$(this).attr('checked', true);
		} else {
			$(this).attr('checked', false);
		}
	});
}
//boe thai.son
function initPleaseSelectDialog() {
	// Init pleaseSelect dialog.
	$("#pleaseSelectDialog").kendoWindow({
		minWidth : 300,
		minWeight : 200,
		//boe thai.son
		title : $("#windowNamepleaseSelectDialog").val(),
		resizable : false,
		modal : true,
		visible : false,
		draggable : false
	});
	// Get pleaseSelect dialog.
	var pleaseSelectDialog = $("#pleaseSelectDialog").data("kendoWindow");
	$("#pleaseSelectDialogOk").click(function(e) {
		pleaseSelectDialog.close();
	});
}
//eoe thai.son
// delete products
function confirmDeleteProducts() {
	$("#btnExcute").click(function(e) {
		e.preventDefault();
		// get value in case delete
		var statusDelte = $("#size option:selected").val();
		if (statusDelte == 'default') {
			// Change color of comboBox to pink
			$("#lstButton .k-dropdown-wrap").css("background-color", "#FFD9E6");
		} else {
		//BOE Tran.Thanh 2014/06/04 : Change from if else to switch case
//		if (statusDelte != 0) {
//		    // Show CSV Popup
//		    if(statusDelte == 3) {
//		        $('#msPleaseSelectDialog').css('display','none');
//		        /* BOE #7206 Luong.Dai 2014/05/27 Fix css disable */
//                /*$("#btnCSVStartExport").removeClass("k-state-disabled");*/
//                /*$("#btnCSVStartExport").addClass("k-button");*/
//		        $("#btnCSVStartExport").prop("disabled", false);
//		        /* EOE #7206 Luong.Dai 2014/05/27 Fix css disable */
//                
//		    	// BOE hoang.ho 2014/02/27 Prevent open dialog when not choose product.
//		    	// confirm show dialog
//		    	var checkCount = $("#table-content .productCheckbox:checked").length;
//		    	if (checkCount <= 0) {
//		    		//boe thai.son
//		    		//fancyAlert($("#noSelectedProduct").val());
//		    		//$("#pleaseSelectDialog").data("kendoWindow").center().open();
//		    		//eoe thai.son
//		    		//return;
//
//		    		$('#msPleaseSelectDialog').css('display','');
//		    		/* BOE #7206 Luong.Dai 2014/05/27 Fix css disable */
//		            /*$("#btnCSVStartExport").addClass("k-state-disabled");*/
//		            /*$("#btnCSVStartExport").removeClass("k-button");*/
//		            $("#btnCSVStartExport").prop("disabled", true);
//		            /* EOE #7206 Luong.Dai 2014/05/27 Fix css disable */
//		    	}
//		    	// BOE hoang.ho 2014/02/27 Prevent open dialog when not choose product.
//		    	//sontt: reset WindowCSV:
//		    	resetWindowCSV();
//		        $("#popup_csv").data("kendoWindow").open();
//		        $("#popup_csv").data("kendoWindow").center();
//		        /*$(".k-window").css("left",((screen.availWidth)/2-150)+"px");
//		        $(".k-window").css("top",((screen.availHeight)/2-100)+"px");*/
//		    }
//		    
//		    // Validate CSV mode1.		    
//		    if(statusDelte == 4) {
//			    validateCsvMode1();		    	
//		    }
//		    
//			return;
//		}
//		// confirm show dialog
//		var checkCount = $(".productCheckbox:checked").length;
//		if (checkCount <= 0) {
//			//boe thai.son
//			//fancyAlert($("#noSelectedProduct").val());
//			$("#pleaseSelectDialog").data("kendoWindow").center().open();
//			//eoe thai.son
//			return;
//		}
//		confirmDialogDetele();

	    // Show CSV Popup
			switch (statusDelte) {
				case "0":
					// confirm show dialog
					var checkCount = $(".productCheckbox:checked").length;
					if (checkCount <= 0) {
						//boe thai.son
						//fancyAlert($("#noSelectedProduct").val());
						$("#pleaseSelectDialog").data("kendoWindow").center().open();
						//eoe thai.son
						break;
					}
					confirmDialogDetele();
					break;
				case "3" :
					$('#msPleaseSelectDialog').css('display','none');
			        /* BOE #7206 Luong.Dai 2014/05/27 Fix css disable */
		            /*$("#btnCSVStartExport").removeClass("k-state-disabled");*/
		            /*$("#btnCSVStartExport").addClass("k-button");*/
			        $("#btnCSVStartExport").prop("disabled", false);
			        /* EOE #7206 Luong.Dai 2014/05/27 Fix css disable */
		            
			    	// BOE hoang.ho 2014/02/27 Prevent open dialog when not choose product.
			    	// confirm show dialog
			    	var checkCount = $("#table-content .productCheckbox:checked").length;
			    	if (checkCount <= 0) {
			    		//boe thai.son
			    		//fancyAlert($("#noSelectedProduct").val());
			    		//$("#pleaseSelectDialog").data("kendoWindow").center().open();
			    		//eoe thai.son
			    		//return;

			    		$('#msPleaseSelectDialog').css('display','');
			    		/* BOE #7206 Luong.Dai 2014/05/27 Fix css disable */
			            /*$("#btnCSVStartExport").addClass("k-state-disabled");*/
			            /*$("#btnCSVStartExport").removeClass("k-button");*/
			            $("#btnCSVStartExport").prop("disabled", true);
			            /* EOE #7206 Luong.Dai 2014/05/27 Fix css disable */
			    	}
			    	// BOE hoang.ho 2014/02/27 Prevent open dialog when not choose product.
			    	//sontt: reset WindowCSV:
			    	resetWindowCSV();
			        $("#popup_csv").data("kendoWindow").open();
			        $("#popup_csv").data("kendoWindow").center();
			        /*$(".k-window").css("left",((screen.availWidth)/2-150)+"px");
			        $(".k-window").css("top",((screen.availHeight)/2-100)+"px");*/
			        break;
				case "4":
					validateCsvMode1();
					break;
			}
		}
	});
}

/**
 * Validate CSV mode1.
 * @author nguyen.hieu
 * @date 2014-03-10
 */
function validateCsvMode1() {
	// Get matter no.
	var matterNo = $("#hdMatterNo").val();
	
	// Call AJAX to validate CSV mode1.
	$.ajax({
		url: "validateCsvMode1.html",
		data: {"matterNo": matterNo},
		type: "POST",
		dataType: "json",
		beforeSend: function(jqXHR, settings) {
			showProcessBar();
		},
		success: function(data, textStatus, jqXHR) {
			if(data == null) {
				showValidationFailureMessage();
				hideProcessBar();
				return;
			}
			
			showValidationSuccessMessage();
			
	        // Reload grid.
			var grid = $('#grid').data('kendoGrid');
			grid.dataSource.page(1);
			grid.content.scrollTop(0);
			
			hideProcessBar();
		},
		error: function(jqXHR, textStatus, errorThrown) {
			showValidationFailureMessage();
			hideProcessBar();
			return;
		}
	});
}

/**
 * Show validation success message.
 * @author nguyen.hieu
 * @date 2014-03-10
 */
function showValidationSuccessMessage() {
	var csvMode1ValidationSuccessMesssage = $("#csvMode1ValidationSuccess").val();
	viewMessage(csvMode1ValidationSuccessMesssage, 0);
}

/**
 * Show validation failure message.
 * @author nguyen.hieu
 * @date 2014-03-10
 */
function showValidationFailureMessage() {
	var csvMode1ValidationFailureMesssage = $("#csvMode1ValidationFailure").val();
	viewMessage(csvMode1ValidationFailureMesssage, 1);
}

// confirm dialog box deleteProduct
function confirmDialogDetele() {
	//boe thai.son
	/*jQuery.fancybox({
		modal : true,
		autoSize : true,
		content : "<div>"
				+ "     <div id='content-popup'>"
				+ "    <p>"
				+ jQuery("#content-confirm-deleteProducts").val()
				+ "    </p>"
				+ "     <div style='text-align:center;padding:5px;'>"
				+ "        <input id='fancyConfirm_ok' class='btn-size-medium btn-color-blue' type='button' "
				+ "	value='"
				+ jQuery("#btn-yes").val()
				+ "' onclick='deleteProducts(); jQuery.fancybox.close();' style='margin:7px;padding:5px;width:67px;'>"
				+ "        <input id='fancyConfirm_cancel' class='btn-size-medium btn-color-default' type='button' "
				+ " value='"
				+ jQuery("#btn-no").val()
				+ "' onclick='jQuery.fancybox.close();' style='margin:7px;padding:5px;width:67px;'>"
				+ "     </div>" + "     </div>" + "</div>"
	});*/
	var win = $("#confirmDeleteDialog").data("kendoWindow");
	win.center();
	win.open();
	//eoe thai.son
}

var deleteReleaseProduct = 0;
// deleteProducts
function deleteProducts() {
	var arrayProductId = "";
	var arrayProductUpdateOn = "";
	$('.productCheckbox:checked').each(function() {
		var productId = $(this).attr('id');
		var upDateTime = $(this).val();
		arrayProductId += productId + "_";
		arrayProductUpdateOn += upDateTime + "_";
	});

	// load ajax delete recode
	$.ajax({
		type : 'POST',
		url : 'deleteProductList.html',
		dataType : 'json',
		data : {
			'arrayProductId' : arrayProductId,
			'arrayProductUpdateOn' : arrayProductUpdateOn,
			"matterDetailActionForm.product.matterNo" :                 $("#matterNo").text(),
		    "matterDetailActionForm.product.productId" :                productId,
		    "matterDetailActionForm.product.productSyouhinSysCode" :    productSyouhinSysCode,
		    "matterDetailActionForm.product.productBrandCode" :         productBrandCode,
		    "matterDetailActionForm.product.brandName" :                brandName,
		    "matterDetailActionForm.product.productName" :              productName,
		    "matterDetailActionForm.product.productCode" :              productCode,
		    "matterDetailActionForm.product.productEanCode" :           productEanCode,
		    "matterDetailActionForm.product.showProductRegistrationFlg" : showProductRegistrationFlg,
		    "matterDetailActionForm.product.productGeneralErrorFlg" :   productGeneralErrorFlg,
		    "matterDetailActionForm.product.productCategoryErrorFlg" :  productCategoryErrorFlg,
		    "matterDetailActionForm.product.productModelErrorFlg" :     productModelErrorFlg,
		    "matterDetailActionForm.product.productAttributeErrorFlg" : productAttributeErrorFlg,
		    "matterDetailActionForm.product.updatedUserId" :            updatedUserId,
		    "matterDetailActionForm.product.updatedOnFrom" :            updatedOnFrom,
		    "matterDetailActionForm.product.updatedOnTo" :              updatedOnTo,
		    "matterDetailActionForm.product.sortField" :                "productId",
		    "matterDetailActionForm.product.sortDir" :                  "asc",
		    'deleteReleaseProduct' :                                   deleteReleaseProduct,
		},
		beforeSend : function() {
			showProcessBar();
		},
		success : function(data) {
			deleteReleaseProduct = 0;
			// load list product again with page index = 1
			if (data.statusDelete == "success") {
				searchProductByConditon();
				var message = $("#deleteSuccess").val();
				viewMessage(message, 0);
			} else {
				if (data.hadProductRelease != null) {
					var messageDelete = $("#releasedProductNotDelete").val();
					var labelOk = $("#btn-yes").val();
					var labelCancel = $("#btn-no").val();
					$.when(kendo.ui.ExtOkCancelDialog.show({
					    title: "",
					    labelOK: labelOk,
					    labelCancel: labelCancel,
					    message: messageDelete,
					    icon: "k-ext-warning" })
					).done(function (response) {
						if (response.button == "OK") {
							deleteReleaseProduct = 1;
							deleteProducts();
						}
					});
				} else {
					/* BOE # Luong.Dai fix show message delete fail */
					/*var message = $("#otherUpdate").val();
					message = message.replace("{0}", data.userUpdated);
					message = message.replace("{1}", data.updatedOn);*/
					var message = $('#deleteFail').val();
					/* EOE # Luong.Dai fix show message delete fail */
					viewMessage(message, 1);

					// Get search condition
					getSearchCondition();
					var grid = $('#grid').data('kendoGrid');
					// Reload data and goto first page
					grid.dataSource.page(1);
					// Go to top of grid
					grid.content.scrollTop(0);
				}
			}
			hideProcessBar();
		}
	});
}

// updateMatter
function updateMatter() {
	$("#btnUpdate").click(function(e) {
		if (!checkChangeDataWhenUpdate()) {
			// Unchange data
			var message = $("#unChange").val();
			viewMessage(message, 1);
			return;
		}
		if (!validateTextboxMatterName()) {
			return;
		}
		e.preventDefault();
		// load ajax delete recode
		$.ajax({
			type : 'POST',
			url : 'updateMatterDetail.html',
			dataType : 'json',
			data : {
				"matterDetailActionForm.entMstFactoryMatterNew.matterNo" : 				$("#matterNo").text(),
				"matterDetailActionForm.entMstFactoryMatterNew.matterRedmineIssueId" : 	$("#tooltip").text().replace("#", ""),
				"matterDetailActionForm.entMstFactoryMatterNew.matterName" : 			$("#matterName").val(),
				"matterDetailActionForm.entMstFactoryMatterNew.matterChargeUserId" : 	getChangeUserId(),
				"matterDetailActionForm.entMstFactoryMatterNew.matterDelFlg" : 			getDelFlg(),
				"matterDetailActionForm.entMstFactoryMatterNew.oldUpdateOn" : 			$("#oldUpdatedOn").val()
			},
			beforeSend : function() {
				showProcessBar();
			},
			success : function(data) {
				if (data.statusUpdate == "success") {
					window.location.href = "./matterDetail.html?matterNo=" + $("#matterNo").text();
				} else if (data.statusUpdate == "otherUpdate") {
					var message = $("#otherUpdate").val();
					message = message.replace("{0}",data.userUpdated);
					message = message.replace("{1}",data.updatedOn);
					viewMessage(message, 1);
				} else if (data.statusUpdate == "noPermission") {
					window.location = "error.html?error=noPermission";
				} else {
					var message = $("#updateFail").val();
					viewMessage(message, 1);
				}
				hideProcessBar();
			}
		});
	});
}

// handle handleBtnGetIssue
function handleBtnGetIssue() {
	// change design tag <a> to input
	var issueId = $("#tooltip").text();
	issueId = issueId.replace("#", "");
	/** Add max length is 8, Bug No.1 - 20140313_not_fix @rcv!cao.lan 2014/03/15 #6906 **/
	$("#tooltip").replaceWith("<input type=\"text\" name=\"tooltipInput\" class=\"k-textbox\" id=\"tooltipInput\" maxLength=\"8\" />");
	$("#tooltipInput").val(issueId);
	$("#tooltipInput").css("width", "40px");
	$("#tooltipInput").css("float", "left");

	// change design button getIssue to button Search Issue
	$("#btnChangeIssue").replaceWith("<input type=\"button\" class=\"k-button\" id=\"btnSearchIssue\" onclick=\"javascript:btnSearchIssue();\" />");
	$("#btnSearchIssue").val($("#btnSearchIssueHidden").val());
	
	// disable button update
	disableButtonKendo("btnUpdate");
	
	/** BOE Detect iput nummeric and paste, Bug No.1 - 20140313_not_fix @rcv!Tran.Thanh 2014/03/19 #6906 **/
	// onlyInpuNumerric
	initNumberTextBox("tooltipInput");
	//onlyInpuNumerric1Byte("tooltipInput");
	detectPaste("tooltipInput");
	/** EOE @rcv!cao.lan 2014/03/15 #6906 **/
}

// handle btnSearchIssue
function btnSearchIssue() {
	// Validate require tooltip
	if (!validateTextboxTooltip()) {
		return;
	}
	// get value from input
	var newIssue = $("#tooltipInput").val();

	$.ajax({
		type : 'POST',
		url : 'selectIssueRedmineByIssueId.html',
		dataType : 'json',
		data : {
			'issueId' : newIssue
		},
		success : function(data) {
			if (data['issueId']) {
				var urlDetailIssueRedmine = "linkToDetailIssueRedmine.html?issueId=" + data['issueId'];
				// change design tag input to <a>
				$("#tooltipInput").replaceWith("<a target=\"_blank\" id=\"tooltip\" onmouseover=\"showTooltip()\" > </a>");
				$("#tooltip").attr('href',urlDetailIssueRedmine);
				$("#tooltip").text("#" + data['issueId']);
			} else {
				// change design tag input to <a>
				$("#tooltipInput").replaceWith("<label id=\"tooltip\" > </label>");
				$("#tooltip").text("#" + newIssue);
				$("#tooltip").css("float", "left");
				$("#tooltip").css("color", "red");
			}
			// change design button SearchIssue to button getIssue
			$("#btnSearchIssue").replaceWith("<input type=\"button\" class=\"k-button\" id=\"btnChangeIssue\" onclick=\"javascript:handleBtnGetIssue();\" />");
			$("#btnChangeIssue").val($("#btnChangeIssueHidden").val());
			enableButtonKendo("btnUpdate");
		},
		error : function() {
		}
	});
}

//Show confirm message
function clickBack() {
	if(!checkChangeDataWhenUpdate()){
		backToPreviousPage();
	} else {
    	// open popup
    	var win = $("#confirmBackDialog").data("kendoWindow");
    	win.center();
    	win.open();
    }
}

// Back to the previous screen
// If doesn't exists, go to Top page
function backToPreviousPage() {
	//BOE THAI.SON FIX BUG "BACK TO SIDE MAP"
	/*// Get previous URL
	var previousUrl = document.referrer;
	if ("" != previousUrl) {
		window.history.go(-1);
		return;
	}*/
	/* BOE By Luong.Dai show progress bar at 2014/04/01 */
	showProcessBar();
	/* EOE By Luong.Dai show progress bar at */
	// Go to Top page
	window.location = "./matterManage.html";
	//eOE THAI.SON FIX BUG "BACK TO SIDE MAP"
}
// Validate and compare and auto format date
//function validateTextboxKousinDate(textBoxId) {
//	// Format date to YYYY/MM/DD
//	jQuery("#" + textBoxId).val(formatDateWithSlash(jQuery("#" + textBoxId).val()));
//	// Value of validate date
//	var validateDateValue = validateDate(jQuery("#" + textBoxId).val());
//	// Value of compare fromDate and toDate
//	var compareTwoDateValue = compareTwoDate(jQuery("#tbUpdatedOnFrom").val(),
//	jQuery("#tbUpdatedOnTo").val());
//	// If validate true and compare date true
//	if (validateDateValue == 1 && compareTwoDateValue != '-1') {
//		// Valid date
//		jQuery("#" + textBoxId).removeClass("error");
//		return true;
//	}
//	// Invalid date
//	jQuery("#" + textBoxId).addClass("error");
//	return false;
//}

// initGrid
function initGrid() {
	$("#vertical").kendoSplitter({
		orientation : "vertical",
		panes : [ {
			collapsible : false
		} ]
	});

	$("#horizontal").kendoSplitter({
		panes : [ {
			collapsible : false,
			resizable : false,
			size : "460px"
		}, {
			collapsible : false,
			resizable : false,
			size : "460px"
		} ]
	});

	var errorCount1Text = $("#errorCountEqual1").val();
	var redRegistrationFlg = $("#redRegistrationFlg").val();
	var grayRegistrationFlg = $("#grayRegistrationFlg").val();

	var grid = $("#grid").kendoGrid({
		dataSource : dataSource,
		height : 430,
		selectable: "multiple row",
		/* BOE Fix bug #6 resizable grid @rcv!nguyen.hieu 2014/03/11. */
		resizable: true,
		/* EOE Fix bug #6 resizable grid. */
		sortable : {
			mode : "single",
			allowUnsort : false
		},
		pageable : {
			refresh : true,
			pageSizes : [ 50, 100 ],
			buttonCount : 5,
			messages : {
				display : jQuery("#pagingDisplay").val(),
				empty : jQuery("#pagingEmpty").val(),
				itemsPerPage : jQuery("#pagingItemsPerPage").val(),
				first : jQuery("#pagingFirst").val(),
				previous : jQuery("#pagingPrevious").val(),
				next : jQuery("#pagingNext").val(),
				last : jQuery("#pagingLast").val()
			}
		},
		columns : [
				{
					title : "",
					template : "<input type='checkbox' name='productCheckbox' class='productCheckbox' id='#= productId #'  value='#= updatedOn #'/>",
					width : "25px"
				},
				/* # BOE Luong.Dai 2014/06/11 add link to productPreview page */
				{
					title : $('#headerPreviewProduct').val(),
					template : function(data) {
						//productId
						var productId = data.productId;
						//View link to productReview
						return "<a target='_blank' class='linkToProductReview' href='productPreview.html?productId=" + productId + "'>" + $('#headerPreviewProduct').val() + "</a>";
					},
					width : "70px"
				},
				/* EOE Luong.Dai 2014/06/11 add link to productPreview page */
				{
					field:"productId",
					//template : "P#: productId #",
					template : function(data) {
						/* BOE Fix bug #8 Clicking link moves to Fit Model Check @rcv!nguyen.hieu 2014/03/11. */
						/*
						var productId = "P" + data.productId;
						var SysCode = data.showProductSyouhinSysCode;
						return "<a href='productEdit.html?productId="+data.productId+"&sysCode="+SysCode+"'>"+productId+"</a>";
						*/
						// Get productID.
				        var productId = data.productId;
				        // Create fitModelCheck link.
				        /* BOE by Luong.Dai add class for tag a: fix bug show progress bar when click link at 2014/04/01 */
				        /*return "<a href='fitModelCheck.html?productId=" + productId + "'>" + "P" + productId + "</a>";*/
				        return "<a class='linkToFitModel' href='fitModelCheck.html?productId=" + productId + "'>" + "P" + productId + "</a>";
				        /* EOE by Luong.Dai add class for tag a: fix bug show progress bar when click link */
				        /* EOE Fix bug #8 Clicking link moves to Fit Model Check. */
					},
					title : $("#headerProductId").val(),
					width : "100px",
					attributes : {
						"class" : "align-center"
					}
				},
				{
					field : "showProductSyouhinSysCode",
					title : $("#headerProductSyouhinSysCode").val(),
					width : "100px",
					attributes : {
						"class" : "align-right"
					}
				    /*BOE Nguyen.Chuong 2014/04/23 able sort.*/
//				    , sortable : false
				    /*EOE Nguyen.Chuong 2014/04/23 able sort.*/
				},
				{
					field : "productBrandCode",
					title : $("#headerProductBrandCode").val(),
					width : "100px",
					attributes : {
						"class" : "align-right"
					}
				},
				{
					field : "brandName",
					title : $("#headerBrandName").val(),
					width : "165px",
					attributes : {
						/* BOE Fix bug #7 left aligned brandName @rcv!nguyen.hieu 2014/03/11. */
						//"class" : "align-right"
						/* EOE Fix bug #7 left aligned brandName. */
					}
				},
				{
					field : "productName",
					title : $("#headerProductName").val(),
					width : "120px"
				},
				{
					field : "productCode",
					title : $("#headerProductItem").val(),
					width : "120px",
					attributes : {
						"class" : "align-center"
					}
				},
				{
					field : "productEanCode",
					title : $("#headerProductEanCode").val(),
					width : "120px",
					attributes : {
						"class" : "align-center"
					}
				},
				{
					field : "showProductRegistrationFlg",
					template : "#if(showProductRegistrationFlg == '"
							+ redRegistrationFlg
							+ "'){# "
							+ "<span class='error'>#:showProductRegistrationFlg#</span> #}"
							+ " else {#"
							+ "#:showProductRegistrationFlg# #}#",
					title : $("#headerShowProductRegistrationFlg").val(),
					width : "100px",
					attributes : {
						"class" : "align-center"
					},
					sortable : false
				},
				{
					field : "productGeneralErrorFlg",
					template : "#if(productGeneralErrorFlg > 0){# <span class='error'>"
							+ errorCount1Text + "</span> #}#",
					title : $("#headerProductGeneralErrorFlg").val(),
					width : "110px",
					attributes : {
						"class" : "align-center"
					}
				},
				{
					field : "productCategoryErrorFlg",
					template : "#if(productCategoryErrorFlg > 0){# <span class='error'>"
							+ errorCount1Text + "</span> #}#",
					title : $("#headerProductCategoryErrorFlg").val(),
					width : "110px",
					attributes : {
						"class" : "align-center"
					}
				},
				{
					field : "productModelErrorFlg",
					template : "#if(productModelErrorFlg > 0){# <span class='error'>"
							+ errorCount1Text + "</span> #}#",
					title : $("#headerProductModelErrorFlg").val(),
					width : "110px",
					attributes : {
						"class" : "align-center"
					}
				},
				{
					field : "productAttributeErrorFlg",
					template : "#if(productAttributeErrorFlg > 0){# <span class='error'>"
							+ errorCount1Text + "</span> #}#",
					title : $("#headerProductAttributeErrorFlg").val(),
					width : "110px",
					attributes : {
						"class" : "align-center"
					}
				}, {
					field : "updatedUserName",
					title : $("#headerCreatedUserId").val(),
					width : "170px",
					attributes : {
						"class" : "align-center"
					}
				}, {
					field : "updatedOn",
					title : $("#headerUpdatedOn").val(),
					width : "240px",
					attributes : {
						"class" : "align-center"
					},
					template : function(a) {
						return a.updatedOnToString;
					}
				} ],
		dataBound : function() {
			// Remove noData message
			jQuery(".dataTables_empty").remove();
		    // Set uncheck checkall
		    $('#checkBoxAll').prop('checked', false);
			if (this.dataSource.total() <= 0) {
				// Show no data message
				jQuery(".k-grid-content").append('<div class="dataTables_empty">'
												+ jQuery("#noDataMessage").val()
												+ '</div>');
			} else {
				// Get product data in grid
		    	var gridData = this.dataSource.view();
		    	// Number product in page
		    	var length = gridData.length;
		    	// Add color gray
			    for (var i = 0; i < length; i++) {
			    	var currentUid = gridData[i].uid;
			    		
			    	var currenRow = this.table.find("tr[data-uid='" + currentUid + "']");
			    	$(currenRow).removeClass("k-alt");
			    	
			    	if (gridData[i].showProductRegistrationFlg == grayRegistrationFlg) {
			    		// add color gray for this row
			    		$(currenRow).addClass("gray");
			        }
			    }
			}            
            handleSelectAll("#checkBoxAll", "#grid table tbody tr", ".productCheckbox", selectedKendoClass, selectedRowClass);
            gridSingleClick("#grid", "#checkBoxAll", ".productCheckbox");
            /* BOE by Luong.Dai show progress bar when click link redirect to fitModel at 2014/04/01 */
           /* $('.linkToFitModel').click(function(e) {
            	//Show progress bar
            	showProcessBar();
            });*/
            /* EOE by Luong.Dai show progress bar when click link redirect to fitModel at 2014/04/01 */
		}
	});

	return grid;
}

// Set data filter condition to sent to server
function setDataForDataSource(sortField, sortDir) {
	return {
		"matterDetailActionForm.product.matterNo" : 				$("#matterNo").text(),
		"matterDetailActionForm.product.productId" : 				productId,
		"matterDetailActionForm.product.productSyouhinSysCode" : 	productSyouhinSysCode,
		"matterDetailActionForm.product.productBrandCode" : 		productBrandCode,
		"matterDetailActionForm.product.brandName" : 				brandName,
		"matterDetailActionForm.product.productName" : 				productName,
		"matterDetailActionForm.product.productCode" : 				productCode,
		"matterDetailActionForm.product.productEanCode" : 			productEanCode,
		"matterDetailActionForm.product.showProductRegistrationFlg" : showProductRegistrationFlg,
		"matterDetailActionForm.product.productGeneralErrorFlg" : 	productGeneralErrorFlg,
		"matterDetailActionForm.product.productCategoryErrorFlg" : 	productCategoryErrorFlg,
		"matterDetailActionForm.product.productModelErrorFlg" : 	productModelErrorFlg,
		"matterDetailActionForm.product.productAttributeErrorFlg" : productAttributeErrorFlg,
		"matterDetailActionForm.product.updatedUserId" : 			updatedUserId,
		"matterDetailActionForm.product.updatedOnFrom" : 			updatedOnFrom,
		"matterDetailActionForm.product.updatedOnTo" : 				updatedOnTo,
		"matterDetailActionForm.product.sortField" : 				sortField,
		"matterDetailActionForm.product.sortDir" : 					sortDir
	};
}

// Get filter condition from filter bar
function getSearchCondition() {
	matterNo = $("#matterNo").text();
	productId = $("#tbProductId").val().replace(/[^0-9]/g, '');
	productSyouhinSysCode = $("#tbProductSyouhinSysCode").val();
	productBrandCode = $("#tbProductBrandCode").val();
	brandName = $("#tbName").val();
	productName = $("#tbProductName").val();
	productCode = $("#tbProductCode").val();
	productEanCode = $("#tbProductEanCode").val();
	showProductRegistrationFlg = $("#cbbRegistrationFlg").val();
	productGeneralErrorFlg = $("#tbproductGeneralErrorFlg").val();
	productCategoryErrorFlg = $("#tbproductCategoryErrorFlg").val();
	productModelErrorFlg = $("#tbNatterFactoryErrorCountModel").val();
	productAttributeErrorFlg = $("#tbproductAttributeErrorFlg").val();
	updatedUserId = $("#tbUpdatedUserId").val();
	updatedOnFrom = $("#tbUpdatedOnFrom").val();
	updatedOnTo = $("#tbUpdatedOnTo").val();

	trimSearchCondition();
}

function trimSearchCondition() {
	if (matterNo) {
		matterNo = matterNo.trim();
	}
	if (productId) {
		productId = productId.trim();
	}
	if (productSyouhinSysCode) {
		productSyouhinSysCode = productSyouhinSysCode.trim();
	}
	if (productBrandCode) {
		productBrandCode = productBrandCode.trim();
	}
	if (brandName) {
		brandName = brandName.trim();
	}
	if (productName) {
		productName = productName.trim();
	}
	if (productCode) {
		productCode = productCode.trim();
	}
	if (productEanCode) {
		productEanCode = productEanCode.trim();
	}
	if (showProductRegistrationFlg) {
		showProductRegistrationFlg = showProductRegistrationFlg.trim();
	}
	if (productGeneralErrorFlg) {
		productGeneralErrorFlg = productGeneralErrorFlg.trim();
	}
	if (productCategoryErrorFlg) {
		productCategoryErrorFlg = productCategoryErrorFlg.trim();
	}
	if (productModelErrorFlg) {
		productModelErrorFlg = productModelErrorFlg.trim();
	}
	if (productAttributeErrorFlg) {
		productAttributeErrorFlg = productAttributeErrorFlg.trim();
	}
	if (updatedUserId) {
		updatedUserId = updatedUserId.trim();
	}
	if (updatedOnFrom) {
		updatedOnFrom = updatedOnFrom.trim();
	}
	if (updatedOnTo) {
		updatedOnTo = updatedOnTo.trim();
	}
}

// Filter data by search condition
function searchProductByConditon() {
	// Validate search filter
	var validateResult = validateSearchFilter();
	if (validateResult) {
		// Get search condition
		getSearchCondition();
		var grid = $('#grid').data('kendoGrid');
		// Reload data and goto first page
		grid.dataSource.page(1);
		// Go to top of grid
		grid.content.scrollTop(0);
	}
}
// customHTML for Date
function customHtmlDateInput() {
	var html = '<td>'
			+ '<div class=\"dateWrap\"> <span class=\"dateCustom\">From</span> <span> <input type="search" id="tbUpdatedOnFrom" class="tbSearch k-input filterTextBox" style="width: 180px;" /></span><br/>'
			+ '<span class=\"dateCustom\">To</span>   <span> <input type="search" id="tbUpdatedOnTo" class="tbSearch k-input filterTextBox" style="width: 180px;" /></span></div>'
			+ '</td>';
	return html;
}

// create combobox Yes/No filter
function createComboboxYesNo(id) {
	var html = '<td><select id="' + id + '" class="cbbFilter"">'
			+ '<option value=" "></option>' + '<option value="0">'
			+ $("#errorCountEqual0").val() + '</option>' + '<option value="1">'
			+ $("#errorCountEqual1").val() + '</option>' + '</select></td>';

	return html;
}

// Get selected user
function getChangeUserId() {
	var combo = $("#lstUser").data("kendoDropDownList");
	return combo.value();
}

// Input numberic only in textbox filter
function inputNumbericOnly() {
	// BOE: Triet.Nguyen
	// onlyInpuNumerric("tbProductId");
	// EOE: Triet.Nguyen
	//Utils.bindFormatOnTextfield("tbProductSyouhinSysCode", Utils._TF_NUMBER);
	initNumberTextBox("tbProductSyouhinSysCode");
	//Utils.bindFormatOnTextfield("tbProductBrandCode", Utils._TF_NUMBER);
	initNumberTextBox("tbProductBrandCode");
}

//BOE: Triet.Nguyen - Validate DateTime
//Nguyen.Chuong #7205: move validate date to commonJS.
//Validate date
function validateKousinDate() {
     var result = true;
     if (!validateTextboxKousinDate("tbUpdatedOnFrom")) {
         result = false;
     }
     if (!validateTextboxKousinDate("tbUpdatedOnTo")) {
         result = false;
     }
     if (!result) {
         return false;
     }
     result = validateKousinDateRange("tbUpdatedOnFrom", "tbUpdatedOnTo");
     return result;
}
//EOE: Triet.Nguyen - Validate DateTime

// Validate search filter
function validateSearchFilter() {
	// BOE: Triet.Nguyen
	var productIdResult = validateNumbericAndCharacter($("#tbProductId").val());
	// EOE: Triet.Nguyen
	var productSyouhinSysCodeResult = validateNumbericAndBlank($("#tbProductSyouhinSysCode").val());
	var productBrandCodeResult = validateNumbericAndBlank($("#tbProductBrandCode").val());
	
	//BOE: 2014/02/19 Nguyen.Triet - Validate DateTime
	var validateDate = validateKousinDate();
	//var updatedOnFromResult = validateDate($("#tbUpdatedOnFrom").val());
	//var updatedOnToResult = validateDate($("#tbUpdatedOnTo").val());

	if (!productIdResult || !productSyouhinSysCodeResult
			|| !productBrandCodeResult 
			/*|| !updatedOnFromResult || !updatedOnToResult*/
			|| !validateDate) {
		return false;
	}
	//EOE: 2014/02/19 Nguyen.Triet - Validate DateTime

	return true;
}

// Get del flg value
function getDelFlg() {
	if ($("#delFlag").prop("checked")) {
		return 1;
	} else {
		return 0;
	}
}

// View message
function viewMessage(msg, msgCode) {
	/* BOE # Luong.Dai 2014/07/04 move to top of function */
	$("#message").removeAttr('class');
	/* BOE # Luong.Dai move to top of function */
	// Remove message
	if (null == msg || "" == msg) {
		$("#message").text("");
		/* BOE # Luong.Dai 2014/07/04 move to top of function */
		/*$("#message").removeAttr('class');*/
		/* BOE # Luong.Dai move to top of function */
	} else {
		$("#message").text(msg);
		$("#message").addClass("has-message");
		// Message success
		if (msgCode == 0) {
			$("#message").addClass("success");
		} else {
			$("#message").addClass("msg-error");
		}
	}
}

// Validate require
function validateRequire(input) {
	var value = $("#" + input).val();

	if (null == value || "" == value) {
		return false;
	}
	return true;
}

// Validate textbox require
function validateTextboxMatterName() {
	// Validate null
	if (!validateRequire("matterName")) {
		var label = $("#lblMatterName").text();
		var message = jQuery("#requiredFill").val();
		var showMessage = message.replace('{0}', label);

		$("#spnMatterNameError").text(showMessage);
		$("#matterName").css("background-color", "#FFD9E6");

		return false;
	} else if ($("#matterName").val().length > 128) {
		var message = jQuery("#outOfRange").val();
		var showMessage = message.replace('{0}', 128);

		$("#spnMatterNameError").text(showMessage);
		$("#matterName").css("background-color", "#FFD9E6");

		return false;
	}
	else {
		$("#spnMatterNameError").text("");
		$("#matterName").css("background-color", "#FFFFFF");

		return true;
	}
}

// Validate textbox tooltip
function validateTextboxTooltip() {
	if (!validateRequire("tooltipInput")) {
		var message = $("#requiredFill").val();
		var label = $("#lblTooltip").text();
		var showMessage = message.replace('{0}', label);

		$("#spnTooltipError").text(showMessage);
		$("#tooltipInput").css("background-color", "#FFD9E6");
		return false;
	}
	$("#spnTooltipError").text("");
	$("#tooltipInput").css("background-color", "#FFFFFF");
	return true;
}

// Check change data
function checkChangeDataWhenUpdate() {
	if (getDelFlg() == 1) {
		return true;
	}

	// Get old data
	var oldToolTip = $("#oldToolTip").val();
	var oldMatterName = $("#oldMatterName").val();
	var oldChangeUser = $("#oldChangeUser").val();
	// Get current data
	var currentToolTip = $("#tooltip").text().replace("#", "");
	var currentMatterName = $("#matterName").val();
	var currentChangeUser = getChangeUserId();
	// Check change value
	if (oldToolTip != currentToolTip || oldMatterName != currentMatterName
			|| oldChangeUser != currentChangeUser) {
		return true;
	}
	return false;
}
// function disable button
function disableButtonKendo(buttonId) {
	$("#"+buttonId).prop("disabled",true);
	$("#"+buttonId).addClass("k-state-disabled");
}
// function enable button
function enableButtonKendo(buttonId) {
	$("#"+buttonId).prop("disabled",false);
	$("#"+buttonId).removeClass("k-state-disabled");
}

// Reset value in screen
function resetValueInScreen() {
	$("#btnReset").click(function () {
		/* BOE by Luong.Dai at 2014/04/08 Show progress bar */
		showProcessBar();
		/* EOE by Luong.Dai at 2014/04/08 Show progress bar */
		location.reload();
	});
}

//BOE Nguyen.Chuong

//Upload CSV and import to DB
var importedCSVTableName;
function uploadFiles(){
	//event.stopPropagation(); // Stop stuff happening
	//event.preventDefault(); // Totally stop stuff happening
	// show progress bar
    var pb = $("#progressBar").data("kendoProgressBar");
    pb.value(false);
    $("#progressBar").show();
    // reset file name.
    $("#fileuploadName").val("");
	// flow1.1 check other import running
	$.ajax({
        url: 'ajaxCheckImportCondition.html',
        type: 'POST',
        data: {
        	"matterDetailActionForm.entMstFactoryMatterNew.matterNo": $("#hdMatterNo").val()
        },
        cache: false,
        dataType: 'json',
        success: function(data) {
        	var csvImportStatus = data.matterDetailActionForm.csvImportStatus;
        	if (csvImportStatus == 1) {
        		$("#fileupload").val('');
        		$("#progressBar").hide();
        		$('#csvfieldsetImport').show();
        		$("#importMode").data("kendoDropDownList").enable(true);
        		var message = $("#hdCsvOnWorkingErrorMsg").val();
        		showDialog("#alertDialog",message);
        		return;
        	}
        	//Upload file
		    var file = $('#fileupload').prop('files')[0];
		    var size = file.size;
		    var fileName = file.name;
		    fileName = fileName.toLowerCase();
		    // set file name into hidden field
		    $("#fileuploadName").val(fileName);
		    var fileType = getExt(fileName);
		    var maxSize = 50*1024*1024; // 50MB
		    if(size > maxSize){
		    	/* BOE #7206 Luong.Dai call function to show message */
		    	/*$("#fileupload").val('');
		    	$("#progressBar").hide();
        		$('#csvfieldsetImport').show();
		    	// flow1.2 max size
		    	var message = $("#hdCsvSizeErrorMsg").val();
		    	showDialog("#alertDialog", message);*/
		    	var message = $("#hdCsvSizeErrorMsg").val();
		    	resetPopupImport(message);
		    	/* EOE #7206 Luong.Dai call function to show message */
		    	return;
		        // Show log error upload file
		    } else if(fileType!='csv'){
		    	/* BOE #7206 Luong.Dai call function to show message */
		        // Show log error if file is not CSV
		        /*$("#fileupload").val('');
		        $("#progressBar").hide();
		        $('#csvfieldsetImport').show();
        		var message = $("#hdCsvFileTypeErrorMsg").val();
        		showDialog("#alertDialog",message);*/
        		var message = $("#hdCsvFileTypeErrorMsg").val();
        		resetPopupImport(message);
        		/* EOE #7206 Luong.Dai call function to show message */
        		return;
		    } else {
		        var data = new FormData();
		        
		        data.append("fileCSV", file);
		        // ** BOE hoang.ho 2014/02/27 **
		        data.append("importMode", $("#importMode").val());
		        data.append("matterDetailActionForm.entMstFactoryMatterNew.matterNo", $("#hdMatterNo").val());
		        // get spliter symbol and bounded symbol
		        data.append("matterDetailActionForm.splitSymbol", $("#importComboboxFieldDelimiter").val());
		        data.append("matterDetailActionForm.boundedFieldSymbol", $("#boundedSymbolTxt").val());
		        // ** EOE hoang.ho 2014/02/27 **
		
		        // Using ajax upload file into server.
		        $.ajax({
		            url: 'importCSV.html',
		            type: 'POST',
		            data: data,
		            cache: false,
		            dataType: 'json',
		            processData: false, // Don't process the files
		            contentType: false, // Set content type to false as jQuery will tell the server its a query string request
		            //BOE #7881 Tran.Thanh : Show process bar when upload csv
		            beforeSend: function() {
		            	//BOE @rcv!Tran.Thanh 2014/07/10 #9956 : reset showTooltip
		            	showToolTips = false;
		            	importValid = false;
		            	$("#importBtnCsvAnalyze").attr("disabled", "disabled");
		            	$("#exportErrorCSV").attr("disabled", "disabled");
		            	//EOE @rcv!Tran.Thanh 2014/07/10 #9956 : reset showTooltip
		            	showProcessBar();
		            },
		            //EOE #7881 Tran.Thanh : Show process bar when upload csv
		            success: function(data, textStatus, jqXHR)
		            {
		            	//BOE #7881 Tran.Thanh : Show process bar when upload csv
		            	hideProcessBar();
		            	//EOE #7881 Tran.Thanh : Show process bar when upload csv
		            	// get status error.
		            	var csvImportStatus = data.matterDetailActionForm.csvImportStatus;
		            	var message = "";
		            	/* BOE by Luong.Dai add message code 2, 7 */
		            	if (csvImportStatus == 2) {
		            		// max size > 50M
							message = $("#hdCsvSizeErrorMsg").val();
		            	} else if (csvImportStatus == 3) {
		            		/* BOE #7206 Luong.Dai fix bug redirect to error */
		            		/*message = $("#hdCsvUploadErrorMsg").val();*/
		            		window.location = "./error.html?error=errorSystem";
		            		return;
		            		/* EOE #7206 Luong.Dai fix bug redirect to error */
		            	} else if (csvImportStatus == 4){
		            		message = $("#hdCsvHeaderNotExistErrorMsg").val();
		            	} else if (csvImportStatus == 6) {
		            		message = $('#csvWrongHeader').val();
		            	} else if (csvImportStatus == 7) {
		            		// Cannot create temp table
		            		/* BOE #7206 Luong.Dai 2014/05/23 Rediarec to error page */
		                    /*message = $('#csvCannotCreateTmpTable').val();*/
		                    window.location = "./error.html?error=errorSystem";
		                    /* EOE #7206 Luong.Dai 2014/05/23 Rediarec to error page */
		            		
		            	} else if (csvImportStatus == 8) {
		            		//import over 25000 product
		            		message = $("#maximumImportProduct").val();
		            	} else if (csvImportStatus == 9) {
		            		//maximum matter after import csv
		            		message = $("#maximumImportMatter").val();
		            	}
		            	//BOE Tinh.Phan Show error when CSV invalid format.
		            	else if (csvImportStatus == 11) {
		            		//format of CSV invalid
		            		message = $("#invalidFormatCSVMess").val();
		            		//alert(message);
		            	}
		            	//EOE Tinh.Phan Show error when CSV invalid format.
		            	/* EOE by Luong.Dai add message code 2, 7 */
		            	if (message != "") {
		            		/* BOE #7206 Luong.Dai call function to show message */
		            		/*$("#fileupload").val('');
		            	    $("#progressBar").hide();
		            	    $('#csvfieldsetImport').show();
		            	    showDialog("#alertDialog", message);*/
		            	    /* EOE #7206 Luong.Dai call function to show message */
		            	    resetPopupImport(message);
			         		return;
		            	}
		            	 //BOE #7206 Nguyen.Chuong add check import status error when import false.
		                if(!data.matterDetailActionForm.importCSVStatus) {
		                	/* BOE #7206 Luong.Dai 2014/05/23 Rediarec to error page */
		                    /*message = "error when import CSV";
		                    showDialog("#alertDialog", message);*/
		                    window.location = "./error.html?error=errorSystem";
		                    /* EOE #7206 Luong.Dai 2014/05/23 Rediarec to error page */
                            return;
		                }
		                //EOE #7206 Nguyen.Chuong add check import status error when import false.
		            	// show process bar success
		            	pb.value(100);
		            	$('#gridMode1').hide();
		            	$('#gridCSVFitModel').hide();
		            	$("#gridAttribute").hide();
		            	// set data in hidden field
	                	 $("#hdImportedCSVTableName").val(data.matterDetailActionForm.importedCSVTableName);
	                	 $("#hdErrorCSVTableName").val(data.matterDetailActionForm.errorCSVTableName);
	                	 $("#hdRealFileName").val(data.matterDetailActionForm.realFileName);
	                	 $("#hdCsvfileName").val(data.matterDetailActionForm.csvfileName);

	                	 /*
		        		if ($("#importMode").val() == ALL_IMPORTMODE){
		        			importedCSVTableName = data.matterDetailActionForm.importedCSVTableName;
		        			headerCSVMode1 = data.matterDetailActionForm.listCSVHeaderMode1;
		        			mode1_initGridVerSion2();
		        			$("#hCountNewProductInCSV").val(data.matterDetailActionForm.countNewProductInCSVFile);
		        			$('#gridMode1').show();
		                } else if ($("#importMode").val() == COMPATIBLE_MODEL_IMPORTMODE){
		                	// import mode 2
		                    importedCSVTableName = data.matterDetailActionForm.importedCSVTableName;
		                	//Nguyen.Chuong
		                    initGridCSVMode2();
		                	$('#gridCSVFitModel').show();
		                } else if(jQuery("#importMode").val() == ATTRIBUTE_IMPORTMODE) {
		                	//import mode 3
		                	importedCSVTableName = data.matterDetailActionForm.importedCSVTableName;
		                	
		                	//Grid attribute mode.
		                	initGridCSVMode3();
		                	$("#gridAttribute").css("display", "block");
		                }*/
		                changeCSVImportButton(2);

		                //BOE @rcv!Tran.Thanh 2014/07/10 #9956 : move init grid after init tooltip
		                if ($("#importMode").val() == ALL_IMPORTMODE) {
		                	//BOE @rcv!Tran.Thanh 2014/07/16 : init message error when import csv
		                	if (csvImportStatus == 10) {
			            		// status 10 is CSV Import was not valid
			            		//Set import status is false
			            		importValid = false;
			            		//BOE # Tran.Thanh 2014/07/09: enable, disable button
			                	$("#importBtnCsvAnalyze").attr("disabled", "disabled");
			                	$("#exportErrorCSV").removeAttr('disabled');
			                	$("#hdPathErrorCSV").val($("#fileuploadName").val());
			                	$("#importCountStatus").show();
			                	$("#importCountStatus").text($("#errorImportCSV").val());
			                	$("#importCountStatus").css("color", "red");
			                	
			                	//Disable button reflect data to DB
			                	//$("#importBtnCsvAnalyze").val($("#hdBtnCsvAnalyze").val());
			                	$("#importBtnCsvAnalyze").removeClass("btnRed");
			                	//$("#importBtnCsvAnalyze").attr("disabled", "disabled");
			                	//changeCSVImportButton(1);
				                //EOE # Tran.Thanh 2014/07/09 : enable, disable button
				                //EOE # Tran.Thanh 2014/07/09 : enable, disable button
			            	} else {
			            		//Init error message when import success
			            		initMessageCountErrorKnowAndUnknow(data);
			            		//Set import status is false
			            		importValid = true;
			            	}
		                	//EOE @rcv!Tran.Thanh 2014/07/16 : init message error when import csv

		        			importedCSVTableName = data.matterDetailActionForm.importedCSVTableName;
		        			headerCSVMode1 = data.matterDetailActionForm.listCSVHeaderMode1;
		        			mode1_initGridVerSion2();
		        			$("#hCountNewProductInCSV").val(data.matterDetailActionForm.countNewProductInCSVFile);
		        			$('#gridMode1').show();

		                } else if ($("#importMode").val() == COMPATIBLE_MODEL_IMPORTMODE){
		                	// import mode 2
		                    importedCSVTableName = data.matterDetailActionForm.importedCSVTableName;
		                	//Nguyen.Chuong
		                    initGridCSVMode2();
		                	$('#gridCSVFitModel').show();
		                	
		                	//BOE @rcv!Tran.Thanh 2014/07/16 : init message count error know and error unknow
		                	initMessageCountErrorKnowAndUnknow(data);
		                	//EOE @rcv!Tran.Thanh 2014/07/16 : init message count error know and error unknow
		                } else if(jQuery("#importMode").val() == ATTRIBUTE_IMPORTMODE) {
		                	//import mode 3
		                	importedCSVTableName = data.matterDetailActionForm.importedCSVTableName;
		                	
		                	//Grid attribute mode.
		                	initGridCSVMode3();
		                	$("#gridAttribute").css("display", "block");

		                	//BOE @rcv!Tran.Thanh 2014/07/16 : init message count error know and error unknow
		                	initMessageCountErrorKnowAndUnknow(data);
		                	//EOE @rcv!Tran.Thanh 2014/07/16 : init message count error know and error unknow
		                }
		                //EOE @rcv!Tran.Thanh 2014/07/10 #9956 : move init grid after init tooltip
		            },
		            error: function(jqXHR, textStatus, errorThrown)
		            {
                       	/* BOE #7206 Luong.Dai 2014/05/23 Rediarec to error page */
	                    /*message = "error when import CSV";
	                    showDialog("#alertDialog", message);*/
	                    window.location = "./error.html?error=errorSystem";
	                    /* EOE #7206 Luong.Dai 2014/05/23 Rediarec to error page */
	                    return;
		            }
		        });
		    }
        }
	});
}

/**
 * Tran.Thanh
 * init message error
 * @param data
 */
function initMessageCountErrorKnowAndUnknow(data) {
	// Set status import
    var errorKnownRows = data.matterDetailActionForm.outputInfoImportCSV.errorKnownRows;
    var errorKnownRowId = data.matterDetailActionForm.outputInfoImportCSV.errorKnownRowId;
    var errorUnknownRows = data.matterDetailActionForm.outputInfoImportCSV.errorUnknownRows;
    var errorUnknownRowId = data.matterDetailActionForm.outputInfoImportCSV.errorUnknownRowId;
    var totalRecords = data.matterDetailActionForm.outputInfoImportCSV.totalRecords;
    if (errorKnownRowId != null && errorUnknownRowId != null && totalRecords != null) {
    	var importCountStatus = $.format($("#hdImportCsvCountMsg").val(), errorKnownRowId, errorUnknownRowId, totalRecords);
    	$("#importCountStatus").show();
        $("#importCountStatus").text(importCountStatus);
        
        var tooltips = $("#importCountStatus").data("kendoTooltip");
        if (tooltips != null && tooltips != undefined) {
        	tooltips.destroy();
        }
    }

    //BOE @rcv!Tran.Thanh 2014/07/10 #9956 : move to beforeSend
    //var showToolTips = false;
    //EOE @rcv!Tran.Thanh 2014/07/10 #9956 : move to beforeSend
    var arrayMsg = new Array();
    if (errorKnownRows != null && errorKnownRows.length > 0) {
    	showToolTips = true;
    	arrayMsg.push($.format($("#hdImportCsvErrorMsg").val(), errorKnownRows));
    }
    if (errorUnknownRows != null && errorUnknownRows.length > 0) {
    	showToolTips = true;
    	arrayMsg.push($.format($("#hdImportCsvErrorUnknownMsg").val(), errorUnknownRows));
    }
    if (showToolTips) {
    	$("#importCountStatus").css("color", "red");
    	if ((!csvImportStatus == 10)) {
    		$("#importCountStatus").kendoTooltip({
        		content : arrayMsg.join("<br/>"),
        		animation : {
        			close : { effects : "fade:out" },
        			open : { effects : "fade:in", duration : 300 },
        			width: 400
        		}
        	});
    	}
    } else {
    	$("#importCountStatus").css("color", "gray");
    }
}

/**
* Hide process bar
* Reset file upload
* Show error message
*/
function resetPopupImport(message) {
	$("#fileupload").val('');
    $("#progressBar").hide();
    $('#csvfieldsetImport').show();
    $("#importMode").data("kendoDropDownList").enable(true);
    showDialog("#alertDialog", message);
}

function getExt(fileName)
{
    var ext = fileName.split('.').pop();
    if(ext == fileName) return "";
    return ext;
}


//mode 2
var filterRowCSVMode2;
var datasourceCSVMode2;
var gridCSVMode2;

var productIdMode2 = "";
var productSyouhinSysCodeMode2 = "";
var fitModelMakerMode2 = "";
var fitModelModelMode2 = "";
var fitModelStyleMode2 = "";
var importModeMode2 = "";
var sortFieldMode2;
var sortDirMode2;

var currentPageMode2;

function setDataForDataSourceCSVMode2() {
    return {
        "matterDetailActionForm.entTblTmpCSVFilter.productId":                      productIdMode2,
        "matterDetailActionForm.entTblTmpCSVFilter.productSyouhinSysCode":          productSyouhinSysCodeMode2,
        "matterDetailActionForm.entTblTmpCSVFilter.fitModelMaker":                  fitModelMakerMode2,
        "matterDetailActionForm.entTblTmpCSVFilter.fitModelModel":                  fitModelModelMode2,
        "matterDetailActionForm.entTblTmpCSVFilter.fitModelStyle":                  fitModelStyleMode2,
        "matterDetailActionForm.entTblTmpCSVFilter.importMode":                     importModeMode2,
        "matterDetailActionForm.entTblTmpCSVFilter.sortField":                      sortFieldMode2,
        "matterDetailActionForm.entTblTmpCSVFilter.sortDir":                        sortDirMode2,
        "matterDetailActionForm.importedCSVTableName":                              importedCSVTableName,
        "matterDetailActionForm.importedCSVMode":                                   $("#importMode").val()
    };
};

function setSearchConditionImportCSVMode2() {
    productIdMode2                      = jQuery("#tbProductIdMode2").val();
    productSyouhinSysCodeMode2          = jQuery("#tbProductSyouhinSysCodeMode2").val();
    fitModelMakerMode2                  = jQuery("#fitModelMakerMode2").val();
    fitModelModelMode2                  = jQuery("#fitModelModelMode2").val();
    fitModelStyleMode2                  = jQuery("#fitModelStyleMode2").val();
    importModeMode2                     = jQuery("#importModeMode2").val();
    gridCSVMode2.data("kendoGrid").dataSource.page(1);
    gridCSVMode2.data("kendoGrid").content.scrollTop(0);
};

//Init grid model CSV
function initGridCSVMode2() {
    //Filter row
    filterRowCSVMode2 = $(
            '<tr>'
                + '<td><input class="tbSearch k-textbox" type="text" class="filter" id="importModeMode2"                tabindex="1"/></td>'
                + '<td><input class="tbSearch k-textbox" type="text" class="filter" id="tbProductIdMode2"               tabindex="2"/></td>'
                + '<td><input class="tbSearch k-textbox" type="text" class="filter" id="tbProductSyouhinSysCodeMode2"   tabindex="3"/></td>'
                + '<td><input class="tbSearch k-textbox" type="text" class="filter" id="fitModelMakerMode2"             tabindex="4"/></td>'
                + '<td><input class="tbSearch k-textbox" type="text" class="filter" id="fitModelModelMode2"             tabindex="5"/></td>'
                + '<td><input class="tbSearch k-textbox" type="text" class="filter" id="fitModelStyleMode2"             tabindex="6"/></td>'
            + '</tr>'
    );
    
    // Press enter in textbox in importCSV mode 2 grid search
    jQuery(document).on("keypress", "#tbProductIdMode2"
                                + ", #tbProductSyouhinSysCodeMode2"
                                + ", #fitModelMakerMode2"
                                + ", #fitModelModelMode2"
                                + ", #fitModelStyleMode2", function(e) {
        if (e.keyCode == 13) {
            setSearchConditionImportCSVMode2();
        }
    });

    //Datasource
    datasourceCSVMode2 = new kendo.data.DataSource({
        serverPaging: true,
        autoBind: false,
        serverSorting: true,
        // 50 product in 1 page
        pageSize: 50,
        transport: {
            read: {
                type: "POST",
                dataType: "json",
                url: "loadDataForImportCSV.html",
                data:  function (data) {
                    sortFieldMode2 = "";
                    sortDirMode2 = "";
                    //Get current sort value
                    if (data != undefined && data.sort != undefined && data.sort.length > 0) {
                        sortFieldMode2 = data.sort[0].field;
                        sortDirMode2 = data.sort[0].dir;
                    }
                    return setDataForDataSourceCSVMode2();
                },
                beforeSend :function() {
                    clearMsgText();
                    //Hide processing of Kendo.
//                    jQuery("#gridCompatibleModel .k-loading-image").css("background-image", "none");
//                    // show process bar before send request.
//                    showProcessBar();
                },
                complete: function() {
                    // hide process bar that the request succeeded
//                  hideProcessBar();
                    sortResetFlg = 0;
                },
                error: function() {
                    // hide process bar that the request failed
//                    hideProcessBar();
                    sortResetFlg = 0;
                },
                cache: false,
            }
        },
        schema: {
            data: "matterDetailActionForm.listImportedProduct",
            total: "matterDetailActionForm.listImportedProductSize"
        }
    });
    //Grid
    gridCSVMode2 = $("#gridCSVFitModel").kendoGrid({
        dataSource: datasourceCSVMode2,
        sortable: {
            mode: "single",
            allowUnsort: false
        },
        pageable: {
            refresh: true,
            pageSizes: [50, 100],
            buttonCount: 5,
            messages: {
              display:         jQuery("#pagingDisplay").val(),
              empty:           jQuery("#pagingEmpty").val(),
              itemsPerPage:    jQuery("#pagingItemsPerPage").val(),
              first:           jQuery("#pagingFirst").val(),
              previous:        jQuery("#pagingPrevious").val(),
              next:            jQuery("#pagingNext").val(),
              last:            jQuery("#pagingLast").val()
            }
        },
        columns: [
                {
                    field:"importMode",
                    template: function(myData){
                        return myData.importMode;
                    },
                    type: "string",
                    title: $("#hdImportMode").val(),
                    width: "95px", 
                    attributes: {"class": "align-right"} },
                {
                    field:"productId",
                    template: function(myData){
                        return 'P' + myData.productId;
                    },
                    type: "number",
                    title: $("#hdIdentificationNumber").val(),
                    width: "95px", 
                    attributes: {"class": "align-right"} },
                { 
                    field:"productSyouhinSysCode",
                    template: function(myData){
                        if(myData.productSyouhinSysCode == 0){
                            return $('#textSyouhinEqual0').val();
                        }
                        return myData.productSyouhinSysCode;
                    },
                    type: "number",
                    title: $("#hdSystemProductCode").val(),
                    width: "95px",
                    attributes: {"class": "align-right"}
                },
                { 
                    field:"fitModelMaker",
                    type: "string",
                    title: $("#hdMaker").val(),
                    width: "180px",
                    attributes: {"class": "align-left, row_table"}
                },
                { 
                	field:"fitModelModel",
                    type: "string",
                    title: $("#hdCarModel").val(),
                    width: "180px",
                    attributes: {"class": "align-left, row_table"}
                },
                { 
                    field: "fitModelStyle",
                    type: "string",
                    title: $("#hdBikeModel").val(),
                    width: "150px",
                    attributes: {"class": "align-left, row_table"}
                },
                ],
        height: 470,
        minHieght: 470,
        selectable: "multiple, row",
        resizable : true,
        dataBound: function(e){
            // Remove noData message
            jQuery("#gridCSVFitModel .dataTables_empty").remove();
            if (this.dataSource.total() > 0) {
                var grid = $("#gridCSVFitModel").data("kendoGrid");
                // setCurrentSearchCondition();

                var page = this.dataSource.page();

                if (page != currentPageMode2) {
                    currentPageMode2 = page;
                    this.content.scrollTop(0);
                }

                // Add title for row
                var gridData = this.dataSource.view();
                for ( var i = 0; i < gridData.length; i++) {
                    var currentUid = gridData[i].uid;
                    var currenRow = grid.table.find("tr[data-uid='" + currentUid + "']");
                    // $(currenRow).removeClass("k-alt");
                    if (gridData[i].productModelCheckFlg == 1) {
                        // Row checked
                        $(currenRow).removeClass("k-alt");
                        $(currenRow).addClass("rowGridChecked");
                    }
                    if (gridData[i].productModelErrorFlg == 1) {
                        // Row error
                        $(currenRow).removeClass("k-alt");
                        $(currenRow).removeClass("rowGridChecked");
                        $(currenRow).addClass("rowGridError");
                    }
                }
            } else {
                // Show no data message
                jQuery("#gridCSVFitModel .k-grid-content").append(
                        '<div class="dataTables_empty" >'
                                + jQuery("#noDataMessage")
                                        .val() + '</div>');
            }
            
            // Handle clicking row.
//            handleClickingRows();
            // Select all processing.
//            handleSelectAll("#selectAllCheckbox", "#gridCompatibleModel table tbody tr", ".productCheckbox", selectedKendoClass, selectedRowClass);
        },
        change: function(data) {
            
        },
    });

    //Add filter
    gridCSVMode2.data("kendoGrid").thead.append(filterRowCSVMode2);
}


// mode 3
var filterRowCSVMode3;
var datasourceCSVMode3;
var gridCSVMode3;

var productIdMode3 = "";
var productSyouhinSysCodeMode3 = "";
var attributeNameMode3 = "";
var attributeValueMode3 = "";
var attributeDisplayMode3 = "";
var importModeMode3 = "";
var sortFieldMode3;
var sortDirMode3;

var currentPageMode3;

function setDataForDataSourceCSVMode3() {
    return {
        "matterDetailActionForm.entTblTmpCSVFilter.productId":                      productIdMode3,
        "matterDetailActionForm.entTblTmpCSVFilter.productSyouhinSysCode":          productSyouhinSysCodeMode3,
        "matterDetailActionForm.entTblTmpCSVFilter.attributeName":                  attributeNameMode3,
        "matterDetailActionForm.entTblTmpCSVFilter.attributeValue":                 attributeValueMode3,
        "matterDetailActionForm.entTblTmpCSVFilter.attributeDisplay":               attributeDisplayMode3,
        "matterDetailActionForm.entTblTmpCSVFilter.importMode":                     importModeMode3,
        "matterDetailActionForm.entTblTmpCSVFilter.sortField":                      sortFieldMode3,
        "matterDetailActionForm.entTblTmpCSVFilter.sortDir":                        sortDirMode3,
        "matterDetailActionForm.importedCSVTableName":                              importedCSVTableName,
        "matterDetailActionForm.importedCSVMode":                                   $("#importMode").val()
    };
};

function setSearchConditionImportCSVMode3() {
    productIdMode3 = jQuery("#tbProductIdMode3").val();
    jQuery("#tbProductIdMode3").removeClass("error");
    if(!validateNumberic(productIdMode3)) {
        if(validateNumbericAndCharacter(productIdMode3)) {
            productIdMode3 = productIdMode3.substring(1,productIdMode3.length);
        } else {
            productIdMode3 = "";
            jQuery("#tbProductIdMode3").addClass("error");
        }
    }
    productSyouhinSysCodeMode3          = jQuery("#tbProductSyouhinSysCodeMode3").val();
    attributeNameMode3                  = jQuery("#attributeNameMode3").val();
    attributeValueMode3                 = jQuery("#attributeValueMode3").val();
    attributeDisplayMode3               = jQuery("#attributeDisplayMode3").val();
    importModeMode3                     = jQuery("#importModeMode3").val();
    gridCSVMode3.data("kendoGrid").dataSource.page(1);
    gridCSVMode3.data("kendoGrid").content.scrollTop(0);
};

//Init grid attribute CSV
function initGridCSVMode3() {
    //Filter row
    filterRowCSVMode3 = $(
            '<tr>'
                + '<td><input class="tbSearch k-textbox" type="text" class="filter" id="importModeMode3"                tabindex="1"/></td>'
                + '<td><input class="tbSearch k-textbox" type="text" class="filter" id="tbProductIdMode3"               tabindex="2"/></td>'
                + '<td><input class="tbSearch k-textbox" type="text" class="filter" id="tbProductSyouhinSysCodeMode3"   tabindex="3"/></td>'
                + '<td><input class="tbSearch k-textbox" type="text" class="filter" id="attributeNameMode3"             tabindex="4"/></td>'
                + '<td><input class="tbSearch k-textbox" type="text" class="filter" id="attributeValueMode3"            tabindex="5"/></td>'
                + '<td><input class="tbSearch k-textbox" type="text" class="filter" id="attributeDisplayMode3"          tabindex="6"/></td>'
            + '</tr>'
    );
    
    // Press enter in textbox in importCSV mode 3 grid search
    jQuery(document).on("keypress", "#importModeMode3"
                                + ", #tbProductIdMode3"
                                + ", #tbProductSyouhinSysCodeMode3"
                                + ", #attributeNameMode3"
                                + ", #attributeValueMode3"
                                + ", #attributeDisplayMode3", function(e) {
        if (e.keyCode == 13) {
            setSearchConditionImportCSVMode3();
        }
    });

    //Datasource
    datasourceCSVMode3 = new kendo.data.DataSource({
        serverPaging: true,
        autoBind: false,
        serverSorting: true,
        // 50 product in 1 page
        pageSize: 50,
        transport: {
            read: {
                type: "POST",
                dataType: "json",
                url: "loadDataForImportCSV.html",
                data:  function (data) {
                    sortFieldMode3 = "";
                    sortDirMode3 = "";
                    //Get current sort value
                    if (data != undefined && data.sort != undefined && data.sort.length > 0) {
                        sortFieldMode3 = data.sort[0].field;
                        sortDirMode3 = data.sort[0].dir;
                    }
                    return setDataForDataSourceCSVMode3();
                },
                beforeSend :function() {
                    clearMsgText();
                    //Hide processing of Kendo.
//                    jQuery("#gridCompatibleModel .k-loading-image").css("background-image", "none");
//                    // show process bar before send request.
//                    showProcessBar();
                },
                complete: function() {
                    // hide process bar that the request succeeded
//                  hideProcessBar();
                    sortResetFlg = 0;
                },
                error: function() {
                    // hide process bar that the request failed
//                    hideProcessBar();
                    sortResetFlg = 0;
                },
                cache: false,
            }
        },
        schema: {
            data: "matterDetailActionForm.listImportedProduct",
            total: "matterDetailActionForm.listImportedProductSize"
        }
    });
    //Grid
    gridCSVMode3 = $("#gridAttribute").kendoGrid({
        dataSource: datasourceCSVMode3,
        sortable: {
            mode: "single",
            allowUnsort: false
        },
        pageable: {
            refresh: true,
            pageSizes: [50, 100],
            buttonCount: 5,
            messages: {
              display:         jQuery("#pagingDisplay").val(),
              empty:           jQuery("#pagingEmpty").val(),
              itemsPerPage:    jQuery("#pagingItemsPerPage").val(),
              first:           jQuery("#pagingFirst").val(),
              previous:        jQuery("#pagingPrevious").val(),
              next:            jQuery("#pagingNext").val(),
              last:            jQuery("#pagingLast").val()
            }
        },
        columns: [
                {
                    field:"importMode",
                    template: function(myData){
                        return myData.importMode;
                    },
                    type: "string",
                    title: $("#hdImportMode").val(),
                    width: "95px", 
                    attributes: {"class": "align-right"} },
                {
                    field:"productId",
                    template: function(myData){
                        return 'P' + myData.productId;
                    },
                    type: "number",
                    title: $("#hdIdentificationNumber").val(),
                    width: "95px", 
                    attributes: {"class": "align-right"} },
                { 
                    field:"productSyouhinSysCode",
                    template: function(myData){
                        if(myData.productSyouhinSysCode == 0){
                            return $('#textSyouhinEqual0').val();
                        }
                        return myData.productSyouhinSysCode;
                    },
                    type: "number",
                    title: $("#hdSystemProductCode").val(),
                    width: "95px",
                    attributes: {"class": "align-right"}
                },
                { 
                    field:"attributeName",
                    type: "string",
                    title: $("#hdAtributeName").val(),
                    width: "180px",
                    attributes: {"class": "align-left, row_table"}
                },
                { 
                    field:"attributeValue",
                    type: "string",
                    title: $("#hdAdministrativeName").val(),
                    width: "180px",
                    attributes: {"class": "align-left, row_table"}
                },
                { 
                    field: "attributeDisplay",
                    type: "string",
                    title: $("#hdDisplayName").val(),
                    width: "150px",
                    attributes: {"class": "align-left, row_table"}
                },
                ],
        height: 470,
        minHeight: 470,
        selectable: "multiple, row",
        resizable : true,
        dataBound: function(e){
            // Remove noData message
            jQuery("#gridAttribute .dataTables_empty").remove();
            if (this.dataSource.total() > 0) {
                var grid = $("#gridAttribute").data("kendoGrid");
                // setCurrentSearchCondition();

                var page = this.dataSource.page();

                if (page != currentPageMode3) {
                    currentPageMode3 = page;
                    this.content.scrollTop(0);
                }

                // Add title for row
                var gridData = this.dataSource.view();
                for ( var i = 0; i < gridData.length; i++) {
                    var currentUid = gridData[i].uid;
                    var currenRow = grid.table.find("tr[data-uid='" + currentUid + "']");
                    // $(currenRow).removeClass("k-alt");
                    if (gridData[i].productModelCheckFlg == 1) {
                        // Row checked
                        $(currenRow).removeClass("k-alt");
                        $(currenRow).addClass("rowGridChecked");
                    }
                    if (gridData[i].productModelErrorFlg == 1) {
                        // Row error
                        $(currenRow).removeClass("k-alt");
                        $(currenRow).removeClass("rowGridChecked");
                        $(currenRow).addClass("rowGridError");
                    }
                }
            } else {
                // Show no data message
                jQuery("#gridAttribute .k-grid-content").append(
                        '<div class="dataTables_empty" >'
                                + jQuery("#noDataMessage")
                                        .val() + '</div>');
            }
            
            // Handle clicking row.
//            handleClickingRows();
            // Select all processing.
//            handleSelectAll("#selectAllCheckbox", "#gridCompatibleModel table tbody tr", ".productCheckbox", selectedKendoClass, selectedRowClass);
        },
        change: function(data) {
            
        },
    });

    //Add filter
    gridCSVMode3.data("kendoGrid").thead.append(filterRowCSVMode3);
}
//EOE Nguyen.Chuong

function deletedTempTable() {
	$.ajax({
        url: 'cancelImportCSVData.html',
        type: 'POST',
        data: {
        	"importMode": $("#importMode").val(),
        	"matterDetailActionForm.importedCSVTableName" :  $("#hdImportedCSVTableName").val(),
        	"matterDetailActionForm.errorCSVTableName" :  $("#hdErrorCSVTableName").val(),
        },
        cache: false,
        dataType: 'json',
        success: function(data)
        {
        	viewMessage($("#hdCsvCancelImportErrorMsg").val(), 0);
        },
        complete: function(data) {
        	$('#gridMode1').hide();
            $('#gridCSVFitModel').hide();
            $('#gridAttribute').hide();
        	$('#csvfieldsetImport').show();
        	// reset import mode.
        	$("#importCountStatus").hide();
        	changeCSVImportButton(0);
        	var tooltips = $("#importCountStatus").data("kendoTooltip");
            if (tooltips != null && tooltips != undefined) {
            	tooltips.destroy();
            }
        	// show progress bar
            var pb = $("#progressBar").data("kendoProgressBar");
            pb.value(false);
            $("#progressBar").hide();
        	$("#fileupload").val('');
        	changeCSVImportButton(0);
        	if (closeBtnFlg) {
        		$("#popup_csv").data("kendoWindow").close();
        	}
        	hideProcessBar();
        }
    });
}