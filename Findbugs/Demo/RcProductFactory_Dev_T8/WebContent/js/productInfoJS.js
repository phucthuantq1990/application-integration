var selectedRow;
//Current selected thumb(image thumb).
var currentSelectedThumb = "0";
//First showed thumb of list thumb.
var currentFirstThumb = "0";
// Index of current selected row
var currentIdx = 0;
// Current page selected
var currentPage = 1;
// if switch page, isNewPage = true
var isNewPage = true;
    
//product_code
var product_code;
// status of nodeId
var error = false;
// If Thumb loading, isImageThumbLoading = true
var isImageThumbLoading = false;
// If textbox focus, isTextboxFocus = true
var isTextboxFocus = false;
// Id of node selected
var nodeFinal = "0000";

jQuery(document).ready(function() {
	labelCheckStatus = $("select option").filter(function() {
								// may want to use $.trim in here
								return $(this).val() == 10;
						}).html();
	
	// Handler hotkey
    jQuery(document).keydown(function(e) {
        keyPressHandler(e);
    });
    
	product_code = "";

	// Mouseover event for auto load thumb into image thumb.
	jQuery(document).on("mouseover", ".list_thumb", function() {
		thumbId = jQuery(this).attr('thumbId');
		if (thumbId === undefined)
			return false;
		// Switch thumb to image thumb
		switchThumb(thumbId);
	});
	// Load fancybox when click image thumb.
	jQuery("#imageAhref").click(function() {
		jQuery(".fancy_box").fancybox({
			'showCloseButton' : false,
			'enableEscapeButton' : true,
			'padding' : 10,
			'transitionIn' : 'elastic',
			'transitionOut' : 'fade',
			beforeShow: function(){
				// Thumb image is Loading
				isImageThumbLoading = true;
			},
			beforeClose: function() {
				// Thumb image is closed
			    isImageThumbLoading = false;
			}
		});
	});

	// Load fancybox when click hotkey link
	jQuery("#hotkey_link").click(function() {
		jQuery("a.fancybox").fancybox({
			autoSize: false,
            autoDimensions: false,
            height: 270,
            width: 900,
            fitToView: false,
			type: "iframe",
			'showCloseButton' : false,
			'enableEscapeButton' : true,
			'padding' : 10,
			'transitionIn' : 'elastic',
			'transitionOut' : 'fade',
			beforeShow: function(){
				// Transparent hotkey board
				$(".fancybox-skin").css("opacity", "0.85");
				$(".fancybox-skin").css("background","none repeat scroll 0 center #000000");
			}
		});
	});

	getListTreeView();
	//Validate bunrui textBox
	jQuery("#nodeId").focusout(function(event) {
		$("#listTreeView").jstree('close_all', -1);
		$("#listTreeView").jstree('close_all', -1);
		// Set successSearch message and bunruiKeySearch to null
	    jQuery("#successSearch").html("");
	    jQuery("#bunruiKeySearch").val("");
	    error = false;
	    var bunruiCodeTxt = jQuery("#nodeId").val();
	    var nodeId = document.getElementById(bunruiCodeTxt);
	    var bunruiName = $(nodeId).children('a').text();
	    if ("" == bunruiCodeTxt ) {
	        error = true;
	        var selected = $("#listTreeView").jstree('get_selected');
            var tree = jQuery.jstree._reference('#listTreeView');
            tree.deselect_node(selected);
	    } else if (!/^[0-9]+$/i.test(bunruiCodeTxt)) {
           error = true;
           var selected = $("#listTreeView").jstree('get_selected');
           var tree = jQuery.jstree._reference('#listTreeView');
           tree.deselect_node(selected);
	    } else {
	        if(bunruiName == "") {
	            error = true;
	            var selected = $("#listTreeView").jstree('get_selected');
	            var tree = jQuery.jstree._reference('#listTreeView');
	            tree.deselect_node(selected);
	        } else {
	            var selected = $("#listTreeView").jstree('get_selected');
	            var tree = jQuery.jstree._reference('#listTreeView');
	            tree.deselect_node(selected);

	            jQuery("#listTreeView").jstree("select_node", "#" + bunruiCodeTxt + "");

	            var selected = $("#listTreeView").jstree('get_selected');
	            if ($(selected).size() == 0) {
	                error = true;
	            } else {
	                tree = jQuery.jstree._reference('#listTreeView');
	                var children = tree._get_children(selected);

	                if ($(children).size() != 0) {
	                    error = true;
	                }
	            }
	        }
	    }
	    //Set bunruiTxt and bunrui name follow error flag
	    //bunrui code invalid
	    if (error) {
	        jQuery("#nodeDetail").html("");
	        jQuery("#nodeDetail").prop("title", null);
	        jQuery("#errorNode").css("display", "inline");
	        jQuery("#nodeId").addClass("error");

	        return;
	    }
	    //Bunrui code valid
        jQuery("#nodeDetail").html(bunruiName);
        jQuery("#nodeDetail").prop("title", bunruiName);
        jQuery("#errorNode").css("display", "none");
        jQuery("#nodeId").removeClass("error");
	});
	
	jQuery('.tbSearch').focus(function(){
		// Textbox focus
		isTextboxFocus = true;
	});
	
	jQuery('.tbSearch').focusout(function(){
		// Textbox focus out
		isTextboxFocus = false;
	});
	
	jQuery('#btn_search_product').click(function(){
		// Get list product by search params
		getListProductAjax();
	});
	
	jQuery('#btn_update_bunrui').click(function(){
		// Update bunrui code
		updateCode();
	});
	
	// Load table of product
	loadKendoGrid();
});

// Load table for product info
function loadKendoGrid(){
	$("#no4").kendoGrid({
	    dataSource: {
	    	serverPaging: true,
	    	// 50 product in 1 page
	    	pageSize: 50,
	    	transport: {
	    		read: {
	    			type: "POST",
	        		dataType: "json",
	        		url: "getListProductFilter.html",
	                data: {
	                	matterNo : matterNo,
	                    systemProductCode : jQuery("#txtSystemProductCode").val().trim(),
	                    manufactoryCode : jQuery("#txtManufactoryCode").val().trim(),
	                    bunruiCode : jQuery("#txtBunruiCode").val().trim(),
	                    statusProductCode : jQuery("#cbbStatusProductCode").val(),
	    			}
	    		}
	    	},
	    	schema: {
	    		data: "listProduct",
	    		total: "count",
	    	}
	    },
	    height: 402,
	    pageable: {
	        refresh: true,
	        pageSizes: [50, 100],
	        buttonCount: 5,
	        messages: {
	          display: "{2}件中{0}~{1}件を表示",
	          empty: "",
	          itemsPerPage: "件",
	          first: "先頭ページに戻る。",
	          previous: "前ページに戻る。",
	          next: "次ページに進む。",
	          last: "最終ページに進む。"
	        }
	    },
	    columns: [
	        { field:"sysProductCode",      title: titleSystemProductCode,  width: "10%" },
	        { field:"brandName",           title: titleBrandName,          width: "20%" },
	        { field:"productName",         title: titleProductName,        width: "20%" },
	        { field:"manufactoryCode",     title: titleMakerProductCode,   width: "10%" },
	        { field:"bunruiCode",          title: titleBunruiCode,         width: "10%" },
	        { field:"bunruiName",          title: titleBunruiName,         width: "15%" },
	        { field: "productStatusName",  title: titleStatus,             width: "15%" }],
	    resizable : true,
	    selectable: true,
	    change: function(e) {
	    	var selectedRows = this.select();

	    	if (isNewPage == true) {
	    		autoChangeScrollPosition();
	    	}
	    	
	    	// Selected new row
	        if (currentIdx != selectedRows.index() 
	        		|| isNewPage == true) {
	        	isNewPage = false;
	        	
		        currentIdx = selectedRows.index();
		        
		        var grid = $("#no4").data("kendoGrid");
		        
		        // Set state of button Up and Down
		        setStatusOfButton(grid);
		        
		        var data = grid.dataItem(selectedRows);
		        
		        // Get data of selected row
		        bindMainTableRowClick(selectedRows, data);
	        }
	    },
	    dataBound: function(e){
	    	if (this.dataSource.total() >= 0) {
		        var grid = $("#no4").data("kendoGrid");
		    	var gridData = grid.dataSource.view();
		    	var page = grid.dataSource.page();
		    	
		    	// Check null data
		    	if (gridData.length == 0 || gridData == null) {
			    	jQuery(".dataTables_empty").remove();
			    	jQuery(".k-grid-content").append('<div class="dataTables_empty">' + noDataMessage + '</div>');
			    	
			    	// Reset product info
			    	resetProductDetail();
			    } else {
			    	jQuery(".dataTables_empty").remove();
	
			    	// Check switch new page
				    if(page != currentPage){
				    	// Switch new paging
				    	currentIdx = 0;
				    	isNewPage  = true;
				    	currentPage = page;
				    }
	
				    // Add row error or row checked
				    for (var i = 0; i < gridData.length; i++) {
				    	var currentUid = gridData[i].uid;
				    		
				    	var currenRow = grid.table.find("tr[data-uid='" + currentUid + "']");
				    	$(currenRow).removeClass("k-alt");
				    	
				    	if (gridData[i].productErrorFlag == "1") {
				    		// Row error
				    		$(currenRow).addClass("rowErrorProduct");
				        } else if (gridData[i].productStatusCode == "10") {
				        	//Row checked
				        	$(currenRow).addClass("rowCheckedProduct");
				        }
				    }
				    	
				    var grid = $("#no4").data("kendoGrid");
				    // Set selected first row
				    var row1 = grid.tbody.find(">tr")[currentIdx];
				    grid.select(row1);
			    }
		    } else {
		    	// Redirect to error page
		    	 window.location = "./error.html?error=param";
		    }
	    }
	});
}

/**
 * switch thumb to image when mouse over thumb.
 */
function switchThumb(e) {
	if (isTextboxFocus == false) {
	    //If mouseover to current selected thumb.
	    if(jQuery("#selector_" + e).attr('class') === 'image_selected') {
	        return;
	    }
	    //Check no image
	    if(jQuery("#thumb_" + e).length < 1) {
	        return;
	    }
	    //Remove old selected Thumb
	    jQuery("#selector_" + currentSelectedThumb).attr('class', "fancy_box");
	    //Change current select.
	    currentSelectedThumb = e;
	    //Set new selected Thumb
	    jQuery("#selector_" + e).attr('class', "image_selected");
	
		// Change image Thumb
		jQuery("#imageAhref").attr("href", jQuery("#image_" + e).val());
		jQuery("#imageThumb").attr("src", jQuery("#image_" + e).val());
	}
	return;
}

/**
 * create hidden list to content list URL of image and thumb.
 */
function createListHiddenThumb(listImage) {
    var resultHTML = "";
    jQuery(listImage).each(function( index, element ) {
    	// Check productThumb or productImage is not empty
        if (element.productThumb !='' || element.productImage !='') {
        	// productThumb and productImage not empty
            resultHTML += '<input type="hidden"'
                       +         'id="thumb_' + index + '"'
                       +         'value="' + element.productThumb + '" />';
            resultHTML += '<input type="hidden"'
                       +         'id="image_' + index + '"'
                       +         'value="' + element.productImage + '" />';
        } else {
        	// productThumb or productImage is empty
        	// Set thumb and image is noImage
            resultHTML += '<input type="hidden"'
                       +         'id="thumb_' + index + '"'
                       +         'value="' + noImageURL + '" />';
            resultHTML += '<input type="hidden"'
                       +         'id="image_' + index + '/>"'
                       +         'value="' + noImageURL + '" />';
        }
    });
    jQuery("#thumbHiddenDiv").html(resultHTML);
    // Chck listImage is not null
    if(null != listImage) {
        listThumbCount = listImage.length;
        return
    }
    listThumbCount = 0;
}

/**
 * Set status of up and down button
 */
function setStatusOfButton(grid) {
	// If selected row is first row
	// disable Up button
	if (currentIdx <= 0) {
		jQuery("#prev_button").attr("disabled", "disabled");
	} else {
		jQuery("#prev_button").removeAttr("disabled");
	}
	
	var pageSize = grid._data.length - 1;
	
	// If selected row is last row
	// disable Down button
	if (currentIdx >= pageSize) {
		jQuery("#next_button").attr("disabled", "disabled");
	} else {
		jQuery("#next_button").removeAttr("disabled");
	}
}

/**
 * create thumb list into thumb selector area.
 */
function createThumbList() {
    var listThumbCnt = listThumbCount;
    //Create html
    //Add previous button
    var listThumbHTML = '<ul class="selector" id="selector">';
    var liClass = "display: list-item;";
    //Set thumb count to 4
    if (listThumbCnt < 4) {
        listThumbCnt = 4;
    }
    //Loop to create list of thumb.
    for (var i = 0; i < listThumbCnt; i++) {
        var thumbURL = jQuery("#thumb_" + i).val();
        var imageURL = jQuery("#image_" + i).val();
        var thumbClass = "fancy_box";
        var liHTML = "";
        //Add noImage to list
        if (!thumbURL || !imageURL) {
            liHTML = '<li class="list_thumb" style="display: list-item;">'
                    +   '<a href="" onclick="return false;">'
                    +       '<img class="selector_thumb" src="' + noImageURL + '">'
                    +   '</a>'
                    + '</li>';
        } else {
            //Create thumb.
            liHTML = '<li thumbId="' + i + '" class="list_thumb" style="' + liClass + '">'
                    +   '<a href="' + imageURL + '" id="selector_' + i + '" class="' + thumbClass + '" rel="fancy_box" onclick="return false;">'
                    +       '<img class="selector_thumb" src="' + thumbURL + '">'
                    +   '</a>'
                    + '</li>';
        }
        listThumbHTML += liHTML;
    }
    //Add next button
    listThumbHTML += '</ul>';
    //Show list thumb.
    jQuery(".thumb_selector").html(listThumbHTML);

    //Change first thumb of list to selected thumb.
    //Get first thumb of list
    var selectedThumb = jQuery(".list_thumb a[class='fancy_box']")[0];
    if(selectedThumb) {
        //Change class of first thumb to selected.
        jQuery(selectedThumb).attr("class", "image_selected");
        //Show image thumb mapping with selected thumb.
        //Get No of selected thumb.
        var selectThumbId = jQuery(selectedThumb).parent().attr("thumbId");
        //Set current selected thumb
        currentSelectedThumb = selectThumbId;
        //Get image thumb url.
        var imageThumbURL = jQuery("#image_" + selectThumbId).val();
    } else {
        imageThumbURL = noImageURL;
    }
    //Show image thumb.
    if (imageThumbURL != noImageURL) {
    	jQuery("#imageAhref").attr("href", imageThumbURL);
    	jQuery("#imageAhref").attr("class", "fancy_box");
    } else {
    	jQuery("#imageAhref").attr("href", "#");
    	jQuery("#imageAhref").attr("class", "noImage");
    }
    
    jQuery("#imageThumb").attr("src", imageThumbURL);
    //Show thumb image and hide loading image
    jQuery("#imageThumb").css("display", "");
    jQuery("#imageThumbLoading").css("display", "none");
}

/**
 * create product's model table info.
 */
function createProductModelTable(listModel) {
    var resultHTML = "";
    //Loop to create table
    var ieCheckFlag = ((jQuery.browser.msie && jQuery.browser.version  == 9.0))?true:false;
    if(ieCheckFlag) {
        jQuery(listModel).each(function(index, element) {
            resultHTML += '<tr>'
                       +    '<td class="clmModelTable1" title="' + element.productMaker + '">'
                       +        '&nbsp'+ makeShortString(element.productMaker, 7)
                       +    '</td>'
                       +    '<td class="clmModelTable2" title="' + element.productModel + '">'
                       +        '&nbsp'+ makeShortString(element.productModel, 15)
                       +    '</td>'
                       +    '<td class="clmModelTable3" title="' + element.productStyle + '">'
                       +        '&nbsp'+ makeShortString(element.productStyle, 7)
                       +    '</td>'
                       +  '</tr>';
        });
    } else {
        jQuery(listModel).each(function(index, element) {
            resultHTML += '<tr>'
                       +    '<td class="clmModelTable1" title="' + element.productMaker + '">'
                       +        '&nbsp'+element.productMaker
                       +    '</td>'
                       +    '<td class="clmModelTable2" title="' + element.productModel + '">'
                       +        '&nbsp'+element.productModel
                       +    '</td>'
                       +    '<td class="clmModelTable3" title="' + element.productStyle + '">'
                       +        '&nbsp'+element.productStyle
                       +    '</td>'
                       +  '</tr>';
        });
    }
    jQuery("#modelListTable tbody").html(resultHTML);
    //Hide loading image for model table
    jQuery(".imgLoadingModelTable").css("display", "none");
}

/**
 * get product info when click into row:.
 * + list image and thumb.
 * + list model.
 * + product information.
 */
function getProductDataAjax(productCode) {
    jQuery.ajax({
        type: "POST",
        url: "selectProductDataAjax.html",
        data: {
            productId: productCode
        },
        dataType: "json",
        success: function(dataResult) {
           // List image of this product
        	var listImage = dataResult["listImage"];
        	// List model of this product
           var listModel = dataResult["listModel"];

           createListHiddenThumb(listImage);
           createThumbList();
           createProductModelTable(listModel);
        }
    });
}

/**
 * get list product filter
 */
function getListProductAjax() {
	// Reset current value
	currentIdx = 0;
	currentPage = 1;
	isNewPage = true;
	
	// Reset data
	$('#no4').data().kendoGrid.destroy();
	$('#no4').empty();
	
	// Load new data
	loadKendoGrid();
	
	// Statistic data
	countData();
	
	// Focus button search
	// Lost focus textbox
	$('#btn_search_product').focus();
}

/**
 * Count total product error
 * Count total product of matter
 * Count total checked product
 */
function countData() {
	jQuery
	.ajax({
		type : 'POST',
		url : 'countNumberForMatter.html',
		dataType : "json",
		data : {
			matterNo : matterNo
		},
		success : function(res) {			
			var error = res['error'];
			
			if (error == "param") {
				// Redirect to error page
		    	 window.location = "./error.html?error=param";
			}
			
		    //Count total error product
		    var totalProductError = res["totalProductError"];
		    //Count all product of matter.
		    var totalAllProduct = res["totalAllProduct"];
		    //Count all finished product.
		    var totalCheckedProduct = res["totalCheckedProduct"];
		    
		    //Change total product checked and total product.
		    jQuery("#totalProduct").html(totalCheckedProduct + "/ " + totalAllProduct);
			
		    // Check number product error
			if (totalProductError > 0) {
				document.getElementById("btn_release").disabled = true;
			} else {
				document.getElementById("btn_release").disabled = false;
			}
		} 
	});
}

/**
 * Convert tab <br/> to string "\n"
 */
function convertTagBRtoN() {
    var str = $("#txtProductSummary").text();
    var regex = /<br\s*[\/]?>/gi;
    $("#txtProductSummary").html(str.replace(regex, "\n"));
}

/**
 * Binding data for selected row
 */
function bindMainTableRowClick(row, data) {
	//Add loading image to thumb list, image, model table.
	//jQuery("#imageThumb").attr("src", "./images/processing.gif");
	jQuery("#imageThumb").css("display", "none");
	jQuery("#imageThumbLoading").attr("src", "./images/processing.gif");
	jQuery("#imageThumbLoading").css("display", "");
    jQuery(".selector_thumb").attr("src", "./images/processing.gif");
    //Add loading image for model table
    jQuery(".imgLoadingModelTable").css("display", "block");
    jQuery("#modelListTable tbody").html("");

    product_code = data.productCode;

    //Get image, model for product.
    getProductDataAjax(data.productCode);
    //Get other info for product.
    jQuery("#txtProductName").html(data.productName);
    jQuery("#txtManufactoryCodeDetail").html(data.manufactoryCode);
    jQuery("#txtBrandInfo").html(data.brandCode + " : " + data.brandName);
    jQuery("#txtProductSummary").html(data.productSummary);
    jQuery("#nodeId").val(data.bunruiCode);
    jQuery("#nodeId").focusout();

    jQuery("#successSearch").html("");
    jQuery("#bunruiKeySearch").val("");

    convertTagBRtoN();
}

/**
 * Reset product detail
 */
function resetProductDetail() {
	jQuery("#imageAhref").attr("href", "#");
	jQuery("#imageAhref").attr("class", 'noImage');
	jQuery("#imageThumb").attr("src", noImageURL);
    jQuery(".selector_thumb").attr("src", noImageURL);
    jQuery("#modelListTable tbody").html("");
    jQuery("#thumbHiddenDiv").html("");
    jQuery("a[id^='selector']").attr("href", "#");
    jQuery("a[id^='selector']").attr("class", "list_thumb");
    //Get other info for product.
    jQuery("#txtProductName").html("");
    jQuery("#txtManufactoryCodeDetail").html("");
    jQuery("#txtBrandInfo").html("");
    jQuery("#txtProductSummary").html("");
    jQuery("#nodeId").val("");
    jQuery("#nodeId").focusout();
}


/**
 * Auto change scroll position 
 */
function autoChangeScrollPosition() {
    //height of view port of datagrid
    var heightViewPort = jQuery(".k-grid-content").css("height");
    heightViewPort = parseInt(heightViewPort.replace("px", ""));
    //Current selected row object.
    var selectedRow = jQuery('tr[class="'+ jQuery(".k-state-selected").attr("class") + '"]');
    //Position of selected row base on current to top of view port.
    var selectedPosition = selectedRow.offset().top - jQuery(".k-grid-content").offset().top;
    
    //selected row upper of current view port.
    if (selectedPosition <= 26) {
        jQuery(".k-grid-content").scrollTop(jQuery(".k-grid-content").scrollTop() - jQuery(".k-grid-content").offset().top
                                                        + jQuery(selectedRow).offset().top - 40 );
    } else if (selectedPosition + 50 >= heightViewPort) { //selected row under of current view port.
        jQuery(".k-grid-content").scrollTop(jQuery(".k-grid-content").scrollTop() - jQuery(".k-grid-content").offset().top
                                                        + jQuery(selectedRow).offset().top - 260);
    }
}

/**
 * Change selected row to next row
 */
function changeToNextRow() {
	var newIdx = currentIdx + 1;
	var grid = $("#no4").data("kendoGrid");

	// If index out of range
	// Set index equal last row
	if(newIdx >= grid._data.length) {
		newIdx--;
	}

	var row1 = grid.tbody.find(">tr")[newIdx];
    grid.select(row1);

    //Auto scroll follow selected row.
    autoChangeScrollPosition();
}

/**
 * Change selected row to first row
 */
function changeToPrevRow() {
	var newIdx = currentIdx - 1;
	var grid = $("#no4").data("kendoGrid");

	// if index < 0
	// Set index equal 0
	if(newIdx < 0) {
		newIdx = 0;
	}

	var row1 = grid.tbody.find(">tr")[newIdx];
    grid.select(row1);
    
    //Auto scroll follow selected row.
    autoChangeScrollPosition();
}

/**
 * Handler key press
 * @param e event
 */
function keyPressHandler(e) {
	// Press alt key to focus textbox
	// Check thumbe loading and key press is alt key
	if (isImageThumbLoading == false) {
		if (e.altKey) {
			switch (e.which) {
			//Alt + 1
			case 49:
				e.preventDefault();
				focusItemById("bunruiKeySearch");
				break;
			//Alt + 2
			case 50:
				e.preventDefault();
				focusItemById("nodeId");
				break;
			//Alt + 3
			case 51:
				e.preventDefault();
				focusItemById("txtSystemProductCode");
				break;
			//Alt + numpad 1
			case 97:
				e.preventDefault();
				focusItemById("bunruiKeySearch");
				break;
			//Alt + numpad 2
			case 98:
				e.preventDefault();
				focusItemById("nodeId");
				break;
			//Alt + numpad 3
			case 99:
				e.preventDefault();
				focusItemById("txtSystemProductCode");
				break;
			}
		} else if (e.ctrlKey) {
			//Ctrl + /
			if (e.which == 191) {
				e.preventDefault();
				jQuery('#hotkey_link').click();
			}
		}
	}
	
	// Check thumb image loading and not focus text
	if (isImageThumbLoading == false
			&& isTextboxFocus == false) {
		if(e.which == 34 && e.shiftKey) {
	        // shift + page down
	        e.preventDefault();
	        gotoLastPage();
	    } else if (e.which == 33 && e.shiftKey) {
	    	// shift + page up
	        e.preventDefault();
	        gotoFirstPage();
	    } else {
	    	switch (e.which) {
	        case 38://Up
	            e.preventDefault();
	            changeToPrevRow();
	            break;
//	        case 78://Button N
//	            e.preventDefault();
//	            changeToNextRow();
//	            break;
	        case 40://down
	            e.preventDefault();
	            changeToNextRow();
	            break;
//	        case 80://Button P
//	            e.preventDefault();
//	            changeToPrevRow();
//	            break;
	        case 49://number 1
	            switchThumb(0);
	            break;
	        case 50://number 2
	            switchThumb(1);
	            break;
	        case 51://number 3
	            switchThumb(2);
	            break;
	        case 52://number 4
	            switchThumb(3);
	            break;
	        case 33: //Page up
	        	e.preventDefault();
	        	gotoPrevPage();
	        	break;
	        case 34: //Page down
	        	e.preventDefault();
	        	gotoNextPage();
	        	break;
	        case 36: //Home
	        	e.preventDefault();
	        	gotoFirstRow();
	        	break;
	        case 35: //End
	        	e.preventDefault();
	        	gotoEndRow();
	        	break;
		    case 97://numpad 1
	            switchThumb(0);
	            break;
	        case 98://numpad 2
	            switchThumb(1);
	            break;
	        case 99://numpad 3
	            switchThumb(2);
	            break;
	        case 100://numpad 4
	            switchThumb(3);
	            break;
	    	}
	    }
	} else {
		// Disable function key when thumb image is loading
		var keyCode = e.which;
		
		if (keyCode == 35
			|| keyCode == 36) {
			e.preventDefault();
	    }
	}
}

/**
 * focus item by id
 */
function focusItemById(idName) {
	var item = document.getElementById(idName);
    // Check exist item
	if (item != null) {
    	item.focus();
    }
}

/**
 * Go to prev page
 */
function gotoPrevPage() {
	jQuery(".k-i-arrow-w").click();
}

/**
 * Go to next page
 */
function gotoNextPage() {
	jQuery(".k-i-arrow-e").click();
}

/**
 * Go to first row
 */
function gotoFirstRow(){
	var grid = $("#no4").data("kendoGrid");
    // Set selected first row
    var row1 = grid.tbody.find(">tr")[0];
    
    if (row1 != null) {
    	grid.select(row1);
    }

    //Auto scroll follow selected row.
    autoChangeScrollPosition();
}

/**
 * Go to end row
 */
function gotoEndRow(){
	var grid = $("#no4").data("kendoGrid");
	
	var endIdx = grid._data.length - 1;
	
    // Set selected end row
	// Check table has data
    if(endIdx >= 0) {
    	var row1 = grid.tbody.find(">tr")[endIdx];
    
    	grid.select(row1);
    }

    //Auto scroll follow selected row.
    autoChangeScrollPosition();
}

/**
 *  Go to last page
 */
function gotoLastPage() {
	jQuery(".k-i-seek-e").click();
}

/**
 *  Go to first page
 */
function gotoFirstPage() {
	jQuery(".k-i-seek-w").click();
}

/**
 * Update bunrui_code
 */
function updateBunruiCode() {
	if (product_code != "") {
		var nodeCode = jQuery('#nodeId').val();
		jQuery
				.ajax({
					type : 'POST',
					url : 'updateBunruiCode.html',
					dataType : "json",
					data : {
						bunruiCode : nodeCode,
						productCode : product_code,
						matterNo : matterNo
					},
					success : function(res) {					
						var grid = $('#no4').data('kendoGrid');
						var selectRow = grid.select();
						
					    //Count total error product
					    var totalProductError = res["totalProductError"];
					    //Count all product of matter.
					    var totalAllProduct = res["totalAllProduct"];
					    //Count all finished product.
					    var totalCheckedProduct = res["totalCheckedProduct"];
					    
					    //Change total product checked and total product.
					    jQuery("#totalProduct").html(totalCheckedProduct + "/ " + totalAllProduct);
						
					    // Check number product error
						if (totalProductError > 0) {
							document.getElementById("btn_release").disabled = true;
						} else {
							document.getElementById("btn_release").disabled = false;
						}
						
						var status = document.getElementById("cbbStatusProductCode");
						
						var s_value = status.options[status.selectedIndex].value;
						
						// Check product updated not true for search status params
						if (s_value != "" 
							&& s_value != checkedNo) {
							var maxIdx = grid._data.length - 1;
							// Set new selected row
							if (currentIdx >= maxIdx){
								currentIdx = maxIdx - 1;
							}
							
							grid.removeRow(selectRow);
							
							return;
						}

						grid.dataSource.data()[currentIdx].set('productStatusName', labelCheckStatus);
						grid.dataSource.data()[currentIdx].set('productStatusCode', "10");
						grid.dataSource.data()[currentIdx].set('productErrorFlag', "0");
						
						// Search Code in search bar
						var bunruiSearchCode = jQuery('#txtBunruiCode').val();

						// Check table product search by bunrui code
						if (bunruiSearchCode == "") {
							// Data doesn't search by bunrui code

							grid.dataSource.data()[currentIdx].set('bunruiCode', nodeCode);
							
							// Set bunrui name
							var bunrui_name = jQuery('#nodeDetail').html().replace(/&nbsp;/gi, '');
							grid.dataSource.data()[currentIdx].set('bunruiName', bunrui_name);
						} else if (bunruiSearchCode != nodeCode) {
							// Data search by bunrui code
							// Delete row in data table
							grid.removeRow(selectRow);
							return;
						}
					} 
				});
	}
}

/**
 * Get list tree view
 */
function getListTreeView() {
	$("#listTreeView").jstree({
		// List of active plugins
		"plugins" : [ "json_data", "ui", "search", "type", "crrm" ],
		// Create tree with jSon data
		"json_data" : {
			"ajax" : {
				"type" : "POST",
				"url" : "selectTreeViewJson.html",
				"dataType" : "json",
				"data" : function(n) {
					return {
						"parentTreeViewId" : n.attr ? n.attr("id") : "0000"
					};
				},
				"success" : function(dataResult) {
					data = [];
					var result = eval(dataResult["treeView"]);
					if (result.length > 0) {
						jQuery.each(result, function(i, item) {
							data.push(item);
						});
					}
					return data;
				}
			}
		},
		"search" : {
			"show_only_matches" : false // show only matches node with keySearch - [true, false]
		},
		"types" : {
			"max_depth" : -2, // not set
			"max_children" : -2, // not set
			"valid_children" : [ "drive" ],
			"types" : { // UI of node, have 3 type : default, 
				// The default type
				"default" : {
					"valid_children" : "none",
					"icon" : {
						"image" : "/static/v.1.0pre/_demo/file.png"
					}
				},
				// The `folder` type
				"folder" : {
					"valid_children" : [ "default", "folder" ],
					"icon" : {
						"image" : "/static/v.1.0pre/_demo/folder.png"
					}
				},
				// The `drive` nodes
				"drive" : {
					"valid_children" : [ "default", "folder" ],
					"icon" : {
						"image" : "/static/v.1.0pre/_demo/root.png"
					},
					"start_drag" : false,
					"move_node" : false,
					"delete_node" : false,
					"remove" : false
				}
			}
		},
		// the UI plugin - it handles selecting/deselecting/hovering nodes
		"ui" : {
			// this makes the node with ID node_4 selected onload
			"initially_select" : [ "node_1" ],
			"select_multiple_modifier" : false,
			"select_range_modifier" : false
		},
		// the core plugin - not many options here
		"core" : {
			"initially_open" : [ "node_2", "node_3" ]
		},
		"themes" : {
			"theme" : "default",
			"dots" : true,
			"icons" : true,
		}
	}).bind("select_node.jstree", function(e, data) {
		bunruiCode = jQuery("#bunruiCode").val();
		if (bunruiCode == "") {
			bunruiCode = "0000";
		}
		loadSelectedNode(data.args[0].innerText);
		scrollTree();
		return data.inst.toggle_node(data.rslt.obj);
	}).bind("open_node.jstree", function(event, data) {
	}).bind("loaded.jstree", function(event, data) {
		jQuery("#nodeId").focusout();
		scrollTree();
	}).bind("search.jstree", function(e, data) {
		jQuery("#successSearch").html(data.rslt.nodes.length + "件が見つかりました。");
		//scrollTree();
	});
}

function loadSelectedNode(code) {
	// Get value of bunrui code
	jQuery("#bunruiCode").val(code);
	
	// Get id of selected node
	var k = $("#listTreeView").jstree('get_selected').attr('id');
	// Get selected node
	var selected = $("#listTreeView").jstree('get_selected');
	var tree = jQuery.jstree._reference('#listTreeView');
	// Get children of selected node
	var children = tree._get_children(selected);
	// If children of selected node is 0 then set
	if ($(children).size() == 0) {
		// Remove class selectedNode of node selected before
		jQuery("#"+nodeFinal).removeClass("selectedNode");
		// Hide error message
		jQuery("#errorNode").css("display", "none");
		// Set nodeId is id of selected Node
		jQuery("#nodeId").val(k);
		// Set node detail and set title
		jQuery("#nodeDetail").html(selected[0].textContent);
		jQuery("#nodeDetail").prop("title", selected[0].textContent);
		// Remove class error of nodeId
		jQuery("#nodeId").removeClass("error");
		// Add class selectedNode to selected Node
		selected.addClass("selectedNode");
		// Set nodeFinal is id of selected node
		nodeFinal = k;
	}
	scrollTree();
}

function scrollTree(){
	jQuery("#listTreeView").scrollTop(0);
	var bunruiCodeTxt = $('#listTreeView').jstree('get_selected').attr('id');
	jQuery("#listTreeView").jstree("select_node", "#" + bunruiCodeTxt + "");
	jQuery("#listTreeView").jstree("open_node", "#" + bunruiCodeTxt + "");
	var h1 = jQuery("#"+bunruiCodeTxt+"").offset().top;
	var h2 = jQuery("#listTreeView"+"").offset().top;
	jQuery("#listTreeView").scrollTop(h1-h2-90);
	return;
}

function searchTreeView() {
	// Get value of keySearch
	var keySearch = jQuery("#bunruiKeySearch").val();
	// If keySearch not empty then search
	if (keySearch == "") {
		jQuery("#successSearch").html(null);
	}
	$("#listTreeView").jstree("search", keySearch);
	
	jQuery("#listTreeView").scrollTop(0);
	$("#listTreeView").jstree('get_selected').removeClass("selectedNode");
//	var bunruiCodeTxt = $('#listTreeView').jstree('get_selected').attr('id');
//	jQuery("#listTreeView").jstree("select_node", "#" + bunruiCodeTxt + "");
//	jQuery("#listTreeView").jstree("open_node", "#" + bunruiCodeTxt + "");
	
	// Get first node after search
	var search = jQuery(".jstree-search")[0];
	// Get id of node search
	var nodeIdSearch = jQuery(search).parent().attr("id");
	//jQuery(search).parent().addClass("selectedNode");
	
	var h1 = jQuery("#"+nodeIdSearch+"").offset().top;
	var h2 = jQuery("#listTreeView"+"").offset().top;
	jQuery("#listTreeView").scrollTop(h1-h2-90);
	
	return;
}

/**
 * Update bunrui code
 */
function updateCode() {
	error = false;
	// Validate nodeId before update
	jQuery("#nodeId").focusout();
	// If not error then update
	if (error == false) {
		// Update bunrui code to DB
		updateBunruiCode();
	}
}

function releaseMatter() {
    jQuery.ajax({
        type : 'POST',
        url : 'releaseProduct.html',
        dataType : "json",
        data : {
            matterNo : matterNo
        },
        error : function() {
            window.location = defaultErrorPage;
            return;
        },
        success : function(res) {
            var result = eval(res);
            if (result["updateResult"]) {
                if("" != originalPage) {
                    window.location = originalPage;
                    return;
                }
                window.location = defaultRedirectPage;
                return;
            }
        }
    });
}

/**
 * Press enter in textbox bunrui code
 */
jQuery('#nodeId').live('keypress', function(e) {
	var p = e.which;
	// If Enter then update code
	if (p == 13) {
		updateCode();
	}
});

/**
 * Press enter in textbox search category
 * @param event
 */
function HandleBunruiKeySearchPress(event) {
	var key = event.which || event.charCode || event.keyCode || 0;
    if (key == 13) {
    	// Close all node before search, twice
    	$("#listTreeView").jstree('close_all', -1);
		$("#listTreeView").jstree('close_all', -1);
		// Search Tree View
    	searchTreeView();
    }
}

/**
 * Press enter in textbox SystemProductCode, ManufactoryCode, BunruiCode
 * @param evt
 */
function HandleKeyPress(evt) {
    var key = evt.which || evt.charCode || evt.keyCode || 0;

    if (key == 13) {
    	getListProductAjax();
    }
}