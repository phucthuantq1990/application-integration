package net.webike.RcProductFactory.mapper;

import java.sql.SQLException;
import java.util.List;

import net.webike.RcProductFactory.entity.EntMstSyouhinSelect;

/**
 * MstSyouhinSelectMapper interface.
 */
public interface MstSyouhinSelectMapper {
	/**
     * @author		nguyen.hieu
     * @date		2014/02/17
	 * @return		Syouhin select list.
	 * @throws		SQLException SQLException.
	 */
    List<EntMstSyouhinSelect> selectSyouhinSelectList() throws SQLException;
    /**
     * @author      hoang.ho
     * @date        2014/03/06
     * @return      Syouhin select list.
     * @throws      SQLException SQLException.
     */
    List<EntMstSyouhinSelect> selectSyouhinSelectListNotOrdered() throws SQLException;
}
