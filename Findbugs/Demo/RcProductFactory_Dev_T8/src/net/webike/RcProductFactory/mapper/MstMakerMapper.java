/************************************************************************
 * File Name	： MstMakerMapper.java
 * Author		： Long Vu
 * Version		： 1.0.0
 * Date Created	： 2014/02/10
 * Description	： Mst maker mapper.
 *
 ***********************************************************************/
package net.webike.RcProductFactory.mapper;

import java.util.List;

import net.webike.RcProductFactory.entity.EntMstMaker;

/**
 * Interface MstMakerMapper.
 */
public interface MstMakerMapper {

    /************************************************************************
     * <b>Description:</b><br>
     * get list maker.
     *
     * @author      Long Vu
     * @date        Feb 10, 2014
     * @return      List<EntMstMaker>
     ************************************************************************/
    List<EntMstMaker> selectMakerList();

}
