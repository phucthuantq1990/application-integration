/************************************************************************
 * File Name    ： MstAttributeGroupMapper.java
 * Author       ： Long Vu
 * Version      ： 1.0.0
 * Date Created ： 2014/01/15
 * Date Updated ： 2014/01/15
 * Description  ： MstAttributeGroup Mapper.
 ***********************************************************************/
package net.webike.RcProductFactory.mapper;

import java.util.List;
import java.util.Map;

import net.webike.RcProductFactory.entity.EntMstAttributeGroup;

/**
 * MstAttributeGroup Mapper.
 * @author Long Vu
 * Date Created ： 2014/01/15
 */
public interface MstAttributeGroupMapper {

    /************************************************************************
     * <b>Description:</b><br>
     *  selec list attributeGroup by attribute code.
     *
     * @author		Luong.Dai
     * @date		Jan 15, 2014
     * @param attributeCode Integer
     * @return		List<EntMstAttributeGroup>
     ************************************************************************/
    List<EntMstAttributeGroup> selectListAttrGroupByCode(Integer attributeCode);

    /************************************************************************
     * <b>Description:</b><br>
     * delete data to attribute group table.
     *
     * @author      Long Vu
     * @date        Jan 14, 2014
     * @param       deleteCode map to delete.
     ************************************************************************/
    void deleteAttrGroupByCode(Map<String, String> deleteCode);

    /************************************************************************
     * <b>Description:</b><br>
     * update data to attribute group table.
     *
     * @author      Long Vu
     * @date        Jan 16, 2014
     * @param       attrGroup object to update.
     * @return      int
     ************************************************************************/
    int updateAttrGroupByCode(EntMstAttributeGroup attrGroup);

    /************************************************************************
     * <b>Description:</b><br>
     * insert data to attribute group table.
     *
     * @author      Long Vu
     * @date        Jan 14, 2014
     * @param       attrGroup map to delete.
     * @return      int
     ************************************************************************/
    int insertAttrGroupByCode(EntMstAttributeGroup attrGroup);

    /************************************************************************
     * <b>Description:</b><br>
     *  insert new attribute group to DB.
     *
     * @author		Luong.Dai
     * @date		Jan 16, 2014
     * @param attrGroup EntMstAttributeGroup
     * @return		Integer
     ************************************************************************/
    Integer insertNewAttributeGroup(EntMstAttributeGroup attrGroup);
}
