/************************************************************************
 * File Name	： MstFactoryMatterMapper.java
 * Author		： Nguyen.Chuong
 * Version		： 1.0.0
 * Date Created	： 2013/09/24
 * Date Updated	： 2013/09/24
 * Description	： Contain method to get data from database .
 *
 ***********************************************************************/
package net.webike.RcProductFactory.mapper;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import net.webike.RcProductFactory.entity.EntMstFactoryMatter;
import net.webike.RcProductFactory.entity.EntMstFactoryMatterNew;

import org.apache.ibatis.annotations.Param;

/**
 * Interface MstFactoryMatterMapper.
 */
public interface MstFactoryMatterMapper {

    /************************************************************************
     * <b>Description:</b><br>
     *  select list of matter from db with 2 param: status and user.
     *
     * @author		Nguyen.Chuong
     * @date		Sep 10, 2013
     * @param       status String
     * @param       user String
     * @return		List<EntMstFactoryMatter>
     ************************************************************************/
    List<EntMstFactoryMatter> selectMatter(@Param("status")String status, @Param("user")String user);

    /************************************************************************
     * <b>Description:</b><br>
     *  select detail PJ from db with pargam : matter_no.
     *
     * @author		Tran.Thanh
     * @date		24 Sep 2013
     * @param 		matterNo matter no
     * @return		EntMstFactoryMatter
     ************************************************************************/
    EntMstFactoryMatter getDetailPJByMatterNo(@Param("matterNo")String matterNo);

    /************************************************************************
     * <b>Description:</b><br>
     *  delete Matter by mater_no and set kousin_user_id is userId.
     *
     * @author		Tran.Thanh
     * @date		24 Sep 2013
     * @param 		userId matter kousin user id
     * @param 		matterNo matter no
     ************************************************************************/
    void deleteMatterByMatterNo(@Param("userId")String userId, @Param("matterNo")String matterNo);

    /************************************************************************
     * <b>Description:</b><br>
     *  update matter detail.
     *
     * @author      Tran.Thanh
     * @date        24 Sep 2013
     * @param       entMstFactoryMatter EntMstFactoryMatter
     ************************************************************************/
    void updateDetailMatter(EntMstFactoryMatter entMstFactoryMatter);

    /************************************************************************
     * <b>Description:</b><br>
     * get maximum matter_no.
     *
     * @author      Luong.Dai
     * @date        Nov 7, 2013
     * @return      Integer
     ************************************************************************/
    int selectMaxMatterNo();

    /************************************************************************
     * <b>Description:</b><br>
     *  insert new matter.
     *
     * @author      Luong.Dai
     * @date        Nov 7, 2013
     * @param       entFactoryMatter EntMstFactoryMatter
     ************************************************************************/
    void insertNewMatter(EntMstFactoryMatter entFactoryMatter);

    /************************************************************************
    * <b>Description:</b><br>
    *  release matter: change matter status to "10".
    *
    * @author      Nguyen.Chuong
    * @date        Nov 7, 2013
    * @param       userId
    * @param       matterNo String
    * @param       userId login user
    ************************************************************************/
    void releaseMatter(@Param("userId")String userId, @Param("matterNo")String matterNo);

    /* BOE add new mapper by Tran.Thanh 2014/01/17 #B07 */

	/* EOE add new mapper by Tran.Thanh */

    /**
     * Select matter manage list.
     * @author		nguyen.hieu
     * @date		2014-01-18
     * @param		offset Offset.
     * @param		numOfRows Number of rows.
     * @return		Matter list.
     */
    List<EntMstFactoryMatterNew> selectMatterManageList(@Param("offset")int offset, @Param("numOfRows")int numOfRows);

    /**
     * Select filtered matter manage list.
     * @author		nguyen.hieu
     * @date		2014-01-18
     * @param		offset Offset.
     * @param		numOfRows Number of rows.
     * @param		entMstFactoryMatterNew matterNew.
     * @return		Filtered matter manage list.
     */
    List<EntMstFactoryMatterNew> selectFilteredMatterManageList(@Param("offset")int offset,
    		@Param("limit")int numOfRows,
    		@Param("entMstFactoryMatterNew")EntMstFactoryMatterNew entMstFactoryMatterNew);


    /************************************************************************
     * <b>Description:</b><br>
     *  Select list filter.
     *
     * @author		Tran.Thanh
     * @date		20 Jan 2014
     * @param entMstFactoryMatterNew ent query
     * @return		int
     ************************************************************************/
    int selectFilteredMatterManageCount(@Param("entMstFactoryMatterNew")EntMstFactoryMatterNew entMstFactoryMatterNew);

    /* BOE add new mapper by Le.Dinh 2014/01/17 #B07 */
    /************************************************************************
     * <b>Description:</b><br>
     *  selected factory matter by matter code.
     *
     * @author		Luong.Dai
     * @date		Jan 18, 2014
     * @param matterNo Integer
     * @return		EntMstFactoryMatterNew
     ************************************************************************/
    EntMstFactoryMatterNew selectFactoryMatterByMatterCode(Long matterNo);

    /************************************************************************
     * <b>Description:</b><br>
     *  update matter info.
     *
     * @author		Luong.Dai
     * @date		Jan 20, 2014
     * @param matter EntMstFactoryMatterNew
     * @return		Integer
     * * @throws SQLException       Integer
     ************************************************************************/
    Integer updateMatterDataByMatterCode(EntMstFactoryMatterNew matter) throws SQLException;
	/* EOE add new mapper by Le.Dinh */

    /************************************************************************
     * <b>Description:</b><br>
     *  updateMatterErrorFlgByMatterCode.
     *
     * @author		Tran.Thanh
     * @date		28 Feb 2014
     * @param 		matter matter
     * @throws SQLException		void
     ************************************************************************/
    void updateMatterErrorFlgByMatterCode(EntMstFactoryMatterNew matter) throws SQLException;

    /**
     * Insert new matter.
     * @author      nguyen.hieu
     * @date        2014-01-20
     * @param entFactoryMatterNew Matter.
     */
    void insertNewMatterNew(EntMstFactoryMatterNew entFactoryMatterNew);

    /**
     * Delete matter.
     * @author      nguyen.hieu
     * @date       2014-01-20
     * @param		matterNo Matter no.
     * @param		updatedUserId Updadated user ID.
     * @param		matterChargeUserId MatterCharge User Id.
     */
    void deleteMatterNew(@Param("matterNo")long matterNo
    					, @Param("updatedUserId")String updatedUserId
    					, @Param("matterChargeUserId")String matterChargeUserId);

    /**
     * Delete unreleased products.
     * @author      nguyen.hieu
     * @date		2014-01-21
     * @param		matterNo Matter no.
     */
    void deleteUnreleasedProducts(@Param("matterNo")long matterNo);

    /**
     * Get matter manage list by matterId.
     * @author      nguyen.hieu
     * @date       2014-01-21
     * @param		matterNoList Matter no list.
     * @return		Matter list.
     */
    List<EntMstFactoryMatterNew> selectMatterListByMatterNo(List<Long> matterNoList);

    /**
     * Delete unreleased product infos.
     * @author      nguyen.hieu
     * @date        2014-01-21
     * @param		matterNo Matter no.
     */
    void deleteUnreleasedProductInfos(@Param("matterNo")long matterNo);

    /************************************************************************
     * <b>Description:</b><br>
     * update matter error count model.
     *
     * @author      Long Vu
     * @return      int
     * @date        Jan 24, 2014
     * @param       factoryMatter factory matter object
     ************************************************************************/
    int updateMatterErrorCountModel(EntMstFactoryMatter factoryMatter);

    /************************************************************************
     * <b>Description:</b><br>
     * get error count model by matter id.
     *
     * @author      Long Vu
     * @return      int
     * @date        Jan 24, 2014
     * @param       matterNo matter id
     ************************************************************************/
    int selectErrorCountModelByMatterId(long matterNo);

    /************************************************************************
     * <b>Description:</b><br>
     * load matter by matter no.
     *
     * @return      String
     * @param       matterNo matter no
     * @author      Long Vu
     * @date        Feb 17, 2014
     ************************************************************************/
    EntMstFactoryMatter selectMatterByMatterNo(long matterNo);

    /************************************************************************
     * <b>Description:</b><br>
     * insert new matter object.
     *
     * @return      String
     * @param       entMstFactoryMatter matter object insert condition
     * @author      Long Vu
     * @param       map matter id
     * @date        Feb 24, 2014
     ************************************************************************/
    int insertNewMatterObj(@Param("entMstFactoryMatter")EntMstFactoryMatter entMstFactoryMatter, @Param("map")Map<String, Long>  map);

    /************************************************************************
     * <b>Description:</b><br>
     *  update matter_factory_count of matter when maintenance exitsed matter: count = current + number of product.
     *
     * @author		Nguyen.Chuong
     * @date		Mar 15, 2014
     * @param       matterNo int matter to update
     * @param       numberOfProductMaintenance int number of product
     * @param       updatedUserId String update user Id
     * @return		int
     ************************************************************************/
    int updateCountProductOfMatter(@Param("matterNo")long matterNo
                                 , @Param("numberOfProductMaintenance")int numberOfProductMaintenance
                                 , @Param("updatedUserId")String updatedUserId);

    /************************************************************************
     * <b>Description:</b><br>
     *  select All Filtered Matter Manage.
     *
     * @author		Tran.Thanh
     * @date		May 6, 2014
     * @param 		entMstFactoryMatterNew ent for query.
     * @return		List<EntMstFactoryMatterNew>
     ************************************************************************/
    List<EntMstFactoryMatterNew> selectAllFilteredMatterManage(
    		@Param("entMstFactoryMatterNew")EntMstFactoryMatterNew entMstFactoryMatterNew);
}
