/************************************************************************
 * File Name	： MstBrandMapper.java
 * Author		： Tran.Thanh
 * Version		： 1.0.0
 * Date Created	： 2013/12/30
 * Date Updated	： 2013/12/30
 * Description	： Contain method to get data from database .
 *
 ***********************************************************************/
package net.webike.RcProductFactory.mapper;

import java.util.List;

import net.webike.RcProductFactory.entity.EntMstMarume;

/**
 * Interface MstMarumeMapper.
 */
public interface MstMarumeMapper {


    /************************************************************************
     * <b>Description:</b><br>
     *  select list all marume.
     *
     * @author		Tran.Thanh
     * @date		30 Dec 2013
     * @return		List<EntMstMarume>
     ************************************************************************/
    List<EntMstMarume> selectListAllMarume();

}
