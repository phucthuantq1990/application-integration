/************************************************************************
 * File Name    ： MstCategoryMapper.java
 * Author       ： hoang.ho
 * Version      ： 1.0.0
 * Date Created ： 2014/01/08
 * Date Updated ： 2014/01/08
 * Description  ： Contain method to get data from database .
 *
 ***********************************************************************/
package net.webike.RcProductFactory.mapper;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import net.webike.RcProductFactory.entity.EntMstAttribute;
import net.webike.RcProductFactory.entity.EntMstBunrui;
import net.webike.RcProductFactory.entity.EntMstCategory;
import net.webike.RcProductFactory.entity.EntTblCategoryAttribute;

import org.apache.ibatis.annotations.Param;

/**
 * @author hoang.ho
 *
 */
public interface MstCategoryMapper {
    /**
     * Get category list.
     * @param searchConditions filter information of category management
     * @return List<EntMstCategory>
     */
    List<EntMstCategory> selectCategoryManageList(EntMstCategory searchConditions);

    /**
     * Get size of category list.
     * @param searchConditions filter information of category management
     * @return List<EntMstCategory>
     */
    int selectCategoryManageListCount(EntMstCategory searchConditions);

    /**
     * Select Attribute List.
     * @return List<EntMstAttribute>
     */
    List<EntMstAttribute> selectAttributeList();

    /**
     * Get category_attribute relate with attribute Code.
     * @param attributeCode attribute Code
     * @return String
     */
    String selectCategoryCodes(@Param("attributeCodes")String attributeCode);

    /************************************************************************
     * <b>Description:</b><br>
     * get bunrui by bunrui code.
     *
     * @author      Long Vu
     * @date        Jan 10, 2014
     * @param bunruiCode bunrui code
     * @return      List<EntMstBunrui>
     ************************************************************************/
    EntMstBunrui getBunrui(String bunruiCode);
    /************************************************************************
     * <b>Description:</b><br>
     * get userName by userId.
     *
     * @author      thai son
     * @date        Jan 10, 2014
     * @param userId String
     * @return      String
     ************************************************************************/
    String getUserNameByUserIdInMstFactoryUser(String userId);

    /************************************************************************
     * <b>Description:</b><br>
     *  search and get list bunrui by bunruiName.
     *
     * @author		Luong.Dai
     * @date		Jan 8, 2014
     * @param bunruiName String
     * @return		List<EntMstBunrui>
     ************************************************************************/
    List<EntMstBunrui> getListBunrui(String bunruiName);

    /************************************************************************
     * <b>Description:</b><br>
     * get category by code.
     *
     * @author      Long Vu
     * @date        Jan 10, 2014
     * @param categoryCode category code
     * @return      List<EntMstBunrui>
     ************************************************************************/
    EntMstCategory selectCategoryByCode(int categoryCode);

    /************************************************************************
     * <b>Description:</b><br>
     * get catgory-attribute list by category code.
     *
     * @author      Long Vu
     * @date        Jan 10, 2014
     * @param categoryCode category code
     * @return      List<EntMstBunrui>
     ************************************************************************/
    List<EntTblCategoryAttribute> selectCatAttributeByCode(int categoryCode);

    /************************************************************************
     * <b>Description:</b><br>
     * update category Name by bunrui code.
     *
     * @author      Long Vu
     * @date        Jan 10, 2014
     * @param category category
     * @return      int
     ************************************************************************/
    int updateCategoryNameByBunruiCode(EntMstCategory category);

    /************************************************************************
     * <b>Description:</b><br>
     * delete category_attribute data when user click delete.
     *
     * @author      Long Vu
     * @date        Jan 10, 2014
     * @param deletedCode deletedCode
     ************************************************************************/
    void deleteCatAttributeByCode(Map<String, String> deletedCode);

    /************************************************************************
     * <b>Description:</b><br>
     *  insert data to category_attribute table.
     *
     * @author      Long Vu
     * @date        Jan 10, 2014
     * @param obj   object for insert
     * @return      List<EntMstCategory>
     ************************************************************************/
    int insertCatAttribute(EntTblCategoryAttribute obj);

    /************************************************************************
     * <b>Description:</b><br>
     *  get category name list for comparing with insert to category table.
     *
     * @author      Long Vu
     * @date        Jan 10, 2014
     * @param categoryName to check exist name in DB
     * @return      List<EntMstCategory>
     ************************************************************************/
    List<EntMstCategory> selectCategoryNameList(String categoryName);

    /************************************************************************
     * <b>Description:</b><br>
     *  get category name list for comparing with insert to category table.
     *
     * @author      Long Vu
     * @date        Jan 10, 2014
     * @param obj   object to update
     * @return int
     ************************************************************************/
    int updateCatAttribute(EntTblCategoryAttribute obj);

    /************************************************************************
     * <b>Description:</b><br>
     *  get list category without given category code to compare with the given category name.
     *
     * @author               Long Vu
     * @date                 Jan 10, 2014
     * @param category       object to get data
     * @return List<EntMstCategory>
     ************************************************************************/
    List<EntMstCategory> selectCategoryNameListWithoutCode(EntMstCategory category);

    /************************************************************************
     * <b>Description:</b><br>
     * insert to category table and get key insert.
     *
     * @author               Long Vu
     * @date                 Jan 10, 2014
     * @param category   object to insert
     * @param map returnCategoryCode
     * @return int
     ************************************************************************/
    int insertCategoryByCode(@Param("category") EntMstCategory category, @Param("map") Map<String, Integer> map);

    /************************************************************************
     * <b>Description:</b><br>
     *  Get category by category name.
     *
     * @author		Luong.Dai
     * @date		Jan 13, 2014
     * @param category EntMstCategory
     * @return		EntMstCategory
     ************************************************************************/
    EntMstCategory selectCategoryByName(EntMstCategory category);

    /* BOE of Luong.Dai for productManage at 02/12/2014 */
    /************************************************************************
     * <b>Description:</b><br>
     *  Select list category_code and category_name.
     *  Condition: del_flg = 0.
     *
     * @author		Luong.Dai
     * @date		Feb 12, 2014
     * @return		List<EntMstCategory>
     ************************************************************************/
    List<EntMstCategory> selectListCategoryCodeAndName();
    /* EOE of Luong.Dai for productManage */

    /**
     * Get category list.
     * @author		nguyen.hieu
     * @date		2014-02-17
     * @return		Category list.
     * @throws		SQLException SQLException.
     */
    List<EntMstCategory> selectCategoryList() throws SQLException;
}
