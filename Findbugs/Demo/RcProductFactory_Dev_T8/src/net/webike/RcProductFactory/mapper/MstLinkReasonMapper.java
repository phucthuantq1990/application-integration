/************************************************************************
 * File Name	： MstBrandMapper.java
 * Author		： Tran.Thanh
 * Version		： 1.0.0
 * Date Created	： 2014/02/18
 * Date Updated	： 2014/02/18
 * Description	： Contain method to get data from database .
 *
 ***********************************************************************/
package net.webike.RcProductFactory.mapper;

import java.util.List;

import net.webike.RcProductFactory.entity.EntMstLinkReason;

/**
 * Interface MstMarumeMapper.
 */
public interface MstLinkReasonMapper {

    /************************************************************************
     * <b>Description:</b><br>
     *  select List All Link Reason.
     *
     * @author		Tran.Thanh
     * @date		18 Feb 2014
     * @return		List<EntMstLinkReason>
     ************************************************************************/
    List<EntMstLinkReason> selectListAllLinkReason();

}
