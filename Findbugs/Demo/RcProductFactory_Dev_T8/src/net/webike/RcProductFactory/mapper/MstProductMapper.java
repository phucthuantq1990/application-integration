/************************************************************************
 * File Name	： MstProductMapper.java
 * Author		： Nguyen.Chuong
 * Version		： 1.0.0
 * Date Created	： 2014/02/15
 * Date Updated	： 2014/02/15
 * Description	： Contain method to get data from rc_syouhin.mst_product database .
 *
 ***********************************************************************/
package net.webike.RcProductFactory.mapper;

import java.sql.SQLException;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import net.webike.RcProductFactory.entity.EntCSVMode1;
import net.webike.RcProductFactory.entity.EntCSVMode3;
import net.webike.RcProductFactory.entity.EntMstProduct;
import net.webike.RcProductFactory.entity.MstProductSqlCondition;


/**
 * Interface MstProductMapper.
 */
public interface MstProductMapper {

    /************************************************************************
     * <b>Description:</b><br>
     *  Select data for load init data of productManagement popup review by syouhin_sys_code.
     *
     * @author      Nguyen.Chuong
     * @date        Feb 15, 2014
     * @param       syouhinSysCode Long
     * @return      EntMstProduct data from db
     ************************************************************************/
    EntMstProduct selectProductDataForProductReviewPopupBySyouhinCode(Long syouhinSysCode);
    /************************************************************************
     * <b>Description:</b><br>
     * Select id of product to load product manage list for grid, paging, sort and filter.
     *
     * @return      List<EntMstProduct>
     * @param       limit page number
     * @param       offset number data in 1 page
     * @param       product product object condition
     * @author      hoang.ho
     * @date        2014/03/12
     ************************************************************************/
    List<String> selectProductIdList(@Param("limit")int limit, @Param("offset")int offset, @Param("product")EntMstProduct product);

    /************************************************************************
     * <b>Description:</b><br>
     * load product manage list for grid, paging, sort and filter.
     *
     * @return      List<EntMstProduct>
     * @param       limit page number
     * @param       offset number data in 1 page
     * @param       product product object condition
     * @author      Long Vu
     * @date        Feb 15, 2014
     ************************************************************************/
    List<EntMstProduct> selectProductList(@Param("limit")int limit, @Param("offset")int offset, @Param("product")EntMstProduct product);

    /************************************************************************
     * <b>Description:</b><br>
     * get total record of product manage list.
     *
     * @return      List<EntMstProduct>
     * @param       product product object condition
     * @author      Long Vu
     * @date        Feb 15, 2014
     ************************************************************************/
    int selectTotalRecordOfGrid(@Param("product")EntMstProduct product);

    /************************************************************************
     * <b>Description:</b><br>
     * update product del fgl.
     *
     * @return      String
     * @param       productList list syouhin_sys_code
     * @param       prdObj delele flag
     * @author      Long Vu
     * @date        Feb 17, 2014
     ************************************************************************/
    int updateProductDelFlg(@Param("productList")List<Long> productList, @Param("prdObj")EntMstProduct prdObj);

    /************************************************************************
     * <b>Description:</b><br>
     * update product supplier status code.
     *
     * @return      String
     * @param       productList product list
     * @param       entMstProduct product
     * @author      Long Vu
     * @date        Feb 17, 2014
     ************************************************************************/
    int updateProductSupplierStatusCode(@Param("productList")List<Long> productList, @Param("entMstProduct")EntMstProduct entMstProduct);

    /**
     * Select detail search product list.
     * @author		nguyen.hieu
     * @date		2014-02-21
     * @param		limit			Limit.
     * @param		offset		Offset.
     * @param		product		Product.
     * @param       sqlCondition     store flag to inner join others table.
     * @return		Detail search product id list.
     * @throws		SQLException SQLException.
     */
    List<String> selectDetailSearchProductList(@Param("limit")int limit, @Param("offset")int offset,
    		@Param("product")EntMstProduct product, @Param("sqlCondition")MstProductSqlCondition sqlCondition) throws SQLException;

    /************************************************************************
     * <b>Description:</b><br>
     * get product brand code and product group code list.
     *
     * @author      Long Vu
     * @param       prdIdList product id list
     * @return      List<EntMstProduct>
     * @date        Feb 21, 2014
     ************************************************************************/
    List<EntMstProduct> selectProductBrdCodeAndGrpCodeListWithoutGroup0(@Param("productList")List<Long> prdIdList);

    /************************************************************************
     * <b>Description:</b><br>
     * filter only syouhin_sys_code group 0 by list syouhin_sys_code.
     *
     * @author      Nguyen.Chuong
     * @param       prdIdList product id list
     * @return      List<EntMstProduct>
     * @date        Feb 21, 2014
     ************************************************************************/
    List<EntMstProduct> selectSyouhinSysCodeOnlyGroup0ByListSyouhin(@Param("productList")List<Long> prdIdList);

    /************************************************************************
     * <b>Description:</b><br>
     * get product list for updating data.
     *
     * @author      Long Vu
     * @param       prd product object
     * @return      List<EntMstProduct>
     * @date        Feb 21, 2014
     ************************************************************************/
    List<EntMstProduct> selectProductListByBrdCodeAndGrpCode(EntMstProduct prd);

	/**
     * Select detail search product list count.
     * @author		nguyen.hieu
     * @date		2014-02-21
     * @param		product		Product.
     * @param       sqlCondition     store flag to inner join others table.
     * @return		Detail search product list count.
     * @throws		SQLException SQLException.
     */
    int selectDetailSearchProductListCount(@Param("product")EntMstProduct product
        , @Param("sqlCondition")MstProductSqlCondition sqlCondition) throws SQLException;

    /************************************************************************
     * <b>Description:</b><br>
     *  update Product_Maintenance_Flg To Zero.
     *
     * @author		Tran.Thanh
     * @date		18 Mar 2014
     * @param 		list List Syouhin Sys Code
     * @param 		updatedUserId updatedUserId
     * @return		int
     ************************************************************************/
    int updateProductMaintenanceFlgToZeroBySyouhinSysCode(@Param("list") List<String> list,
    													@Param("updatedUserId") String updatedUserId);

    /************************************************************************
     * <b>Description:</b><br>
     * update product maintenance.
     *
     * @author      Long Vu
     * @param       prd product object
     * @return      int
     * @date        Feb 21, 2014
     ************************************************************************/
    int updateProductMaintenanceFlg(EntMstProduct prd);

    /************************************************************************
     * <b>Description:</b><br>
     * update multiple product maintenance by syouhin sys code list.
     *
     * @author      Doan Chuong
     * @param       list List<String>
     * @param       updatedUserId String
     * @return      int
     * @date        March 12, 2014
     ************************************************************************/
    int updateProductMaintenanceFlgBySyouhinSysCodeList(@Param("list") List<String> list, @Param("updatedUserId") String updatedUserId);

    /************************************************************************
     * <b>Description:</b><br>
     * Select syouhin fit model by syouhin sys code list.
     *
     * @author      Doan.Chuong
     * @param       syouhinSysCodeList List<String>
     * @return      List<EntCSVMode3>
     * @date        March 3, 2014
     ************************************************************************/
    List<EntCSVMode3> selectSyouhinFitModelBySyouhinSysCodeList(List<String> syouhinSysCodeList);

    /************************************************************************
     * <b>Description:</b><br>
     *  Select syouhin and all of its attribute.
     *
     * @author      Doan.Chuong
     * @date        March 3, 2014
     * @param       syouhinSysCodeList List<String>
     * @param       attributeCodeList List<String>
     * @return      List<EntCSVMode3>
     ************************************************************************/
    List<EntCSVMode3> selectProductAttributeBySyouhinSysCodeList(@Param("syouhinSysCodeList") List<String> syouhinSysCodeList,
        @Param("attributeCodeList") List<String> attributeCodeList);

    /************************************************************************
     * <b>Description:</b><br>
     *  Select product's info for export CSV mode 1 (tables with 1-1 mapping).
     *
     * @author      Doan.Chuong
     * @date        March 3, 2014
     * @param       syouhinSysCodeList List<String>
     * @return      List<EntCSVMode1>
     ************************************************************************/
    List<EntCSVMode1> selectSyouhinInfoExportCSVMode1SingleTable(List<String> syouhinSysCodeList);

    /************************************************************************
     * <b>Description:</b><br>
     *  Select product fit model for export CSV Mode 1.
     *
     * @author      Doan.Chuong
     * @date        March 3, 2014
     * @param       syouhinSysCodeList List<String>
     * @return      List<EntCSVMode1>
     ************************************************************************/
    List<EntCSVMode1> selectProductFitModelBySyouhinSysCodeList(List<String> syouhinSysCodeList);

    /************************************************************************
     * <b>Description:</b><br>
     *  Select product image info for export CSV Mode 1.
     *
     * @author      Doan.Chuong
     * @date        March 3, 2014
     * @param       syouhinSysCodeList List<String>
     * @return      List<EntCSVMode1>
     ************************************************************************/
    List<EntCSVMode1> selectProductImageInfoBySyouhinSysCodeList(List<String> syouhinSysCodeList);

    /************************************************************************
     * <b>Description:</b><br>
     *  Select product select info for export CSV Mode 1.
     *
     * @author      Doan.Chuong
     * @date        March 3, 2014
     * @param       syouhinSysCodeList List<String>
     * @return      List<EntCSVMode1>
     ************************************************************************/
    List<EntCSVMode1> selectProductSelectInfoBySyouhinSysCodeList(List<String> syouhinSysCodeList);

    /************************************************************************
     * <b>Description:</b><br>
     *  Select product guest input for export CSV Mode 1.
     *
     * @author      Doan.Chuong
     * @date        March 3, 2014
     * @param       syouhinSysCodeList List<String>
     * @return      List<EntCSVMode1>
     ************************************************************************/
    List<EntCSVMode1> selectProductGuestInputInfoBySyouhinSysCodeList(List<String> syouhinSysCodeList);

    /************************************************************************
     * <b>Description:</b><br>
     *  Select product link info for export CSV Mode 1.
     *
     * @author      Doan.Chuong
     * @date        March 3, 2014
     * @param       syouhinSysCodeList List<String>
     * @return      List<EntCSVMode1>
     ************************************************************************/
    List<EntCSVMode1> selectProductLinkInfoBySyouhinSysCodeList(List<String> syouhinSysCodeList);

    /************************************************************************
     * <b>Description:</b><br>
     *  Select product video info for export CSV Mode 1.
     *
     * @author      Doan.Chuong
     * @date        March 3, 2014
     * @param       syouhinSysCodeList List<String>
     * @return      List<EntCSVMode1>
     ************************************************************************/
    List<EntCSVMode1> selectProductVideoInfoBySyouhinSysCodeList(List<String> syouhinSysCodeList);

    /************************************************************************
     * <b>Description:</b><br>
     * select product list.
     *
     * @author      Long Vu
     * @date        March 3, 2014
     * @param       prdIdList List<prdIdList>
     * @return      List<EntMstProduct>
     ************************************************************************/
    List<EntMstProduct> selectproductListByPrdIdList(List<Long> prdIdList);

    /************************************************************************
     * <b>Description:</b><br>
     *  select count list product syouhin_sys_code by list product syouhin_sys_code with maintenance_flg = 0.
     *
     * @author		Nguyen.Chuong
     * @date		Mar 25, 2014
     * @param       prdIdList list<Long>
     * @return		int
     ************************************************************************/
    int selectCountProductSyouhinNotMaintenacedByPrdIdList(List<Long> prdIdList);

    /************************************************************************
     * <b>Description:</b><br>
     *  select list product syouhin_sys_code by list product syouhin_sys_code with maintenance_flg = 0.
     *
     * @author		Nguyen.Chuong
     * @date		Mar 19, 2014
     * @param       prdIdList List<Long>
     * @return		List<Long>
     ************************************************************************/
    List<Long> selectproductSyouhinNotMaintenacedByPrdIdList(List<Long> prdIdList);

    /************************************************************************
     * <b>Description:</b><br>
     *  count list product syouhin_sys_code disabled(product_del_flg = 1).
     *
     * @author      Nguyen.Chuong
     * @date        Mar 19, 2014
     * @param       prdIdList List<Long>
     * @return      int
     ************************************************************************/
    int selectCountProductSyouhinDisabledByPrdIdList(List<Long> prdIdList);

    /**
     * Update product maintenance flag.
     * @author nguyen.hieu
     * @date 2014-03-11
     * @param product Product.
     * @throws SQLException SQLException.
     */
    void updateProductMaintenanceFlgAdvanced(@Param("product")EntMstProduct product) throws SQLException;

    /************************************************************************
     * <b>Description:</b><br>
     *  select list all syouhin_sys_code from DB for export CSV all check.
     *
     * @author		Nguyen.Chuong
     * @date		Mar 13, 2014
     * @param       entMstProduct EntMstProduct contain filter on server
     * @return		List<String>
     ************************************************************************/
//    List<String> selectAllProductsForExportCSV(@Param("product")EntMstProduct entMstProduct);

    /************************************************************************
     * <b>Description:</b><br>
     * Select syouhin sys code list by status type (0:delete, 1:edit, 2:upload).
     *
     * @return      List<Long>
     * @param       list List<Long>
     * @param       statusType int
     * @author      Doan.Chuong
     * @date        2014/03/19
     ************************************************************************/
    List<Long> selectSyouhinSysCodeListByStatus(@Param("list") List<Long> list, @Param("statusType") int statusType);

    /************************************************************************
     * <b>Description:</b><br>
     *  Update maintenanceFlag by list syouhinSysCode.
     *
     * @author		Luong.Dai
     * @date		Jun 5, 2014
     * @param 		maintenanceFlg			Maintenance Flag
     * @param 		loginID					Login user ID
     * @param 		lstSyouhinSysCode		List syouhin_sys_code
     * @return		Integer					Update result
     ************************************************************************/
    Integer updateProductMaintenanceFlgByListSyouhinSysCode(@Param("maintenanceFlg") int maintenanceFlg
                                                          , @Param("loginID") String loginID
                                                          , @Param("lstSyouhinSysCode") List<Long> lstSyouhinSysCode);
}
