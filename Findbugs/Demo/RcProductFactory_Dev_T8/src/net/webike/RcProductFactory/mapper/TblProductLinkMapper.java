/************************************************************************
 * File Name	： TblProductLinkMapper.java
 * Author		： Tran.Thanh
 * Version		： 1.0.0
 * Date Created	： 2014/02/18
 * Date Updated	： 2014/02/18
 * Description	： Contain method to get data from rc_product_factory.tbl_factory_product_link.
 *
 ***********************************************************************/
package net.webike.RcProductFactory.mapper;

import java.util.List;

import net.webike.RcProductFactory.entity.EntTblProductLink;

/**
 * Interface TblProductVideoMapper.
 */
public interface TblProductLinkMapper {


//    /************************************************************************
//     * <b>Description:</b><br>
//     *  select Link By ProductId.
//     *
//     * @author		Tran.Thanh
//     * @date		18 Feb 2014
//     * @param 		productId productId
//     * @return		List<EntTblFactoryProductLink>
//     ************************************************************************/
//    List<EntTblFactoryProductLink> selectLinkByProductId(Integer productId);

    /************************************************************************
     * <b>Description:</b><br>
     *  select link reason by syouhin_sys_code for rc_syouhin.tbl_product_link.
     *
     * @author		Nguyen.Chuong
     * @date		Feb 19, 2014
     * @param       syouhinSysCode Long
     * @return		List<EntTblFactoryProductLink>
     ************************************************************************/
    List<EntTblProductLink> selectLinkReasonBySyouhinSysCode(Long syouhinSysCode);

}
