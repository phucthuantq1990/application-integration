/************************************************************************
 * File Name    ： TblFactoryImportResultMapper.java
 * Author       ： hoang.ho
 * Version      ： 1.0.0
 * Date Created ： 2014/03/04
 * Date Updated ： 2014/03/04
 * Description  ： Contain method to connect and process with database TblFactoryImportResult table.
 *
 ***********************************************************************/
package net.webike.RcProductFactory.mapper;

import java.util.List;

import net.webike.RcProductFactory.entity.EntTblFactoryImportResult;

import org.apache.ibatis.annotations.Param;

/**
 * Contain properties of TblFactoryImportResultMapper.
 * @author Hoang.Ho
 * @since 2014/03/04
 */
public interface TblFactoryImportResultMapper {
    /**
     * Insert import result list.
     * @param entImportResultList list for insert.
     * @author Hoang.Ho
     * @since 2014/03/04
     */
    void insertImportResultList(@Param("entImportResultList")List<EntTblFactoryImportResult> entImportResultList);
}
