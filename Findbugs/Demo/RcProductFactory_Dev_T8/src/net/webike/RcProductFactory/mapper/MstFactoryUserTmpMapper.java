/************************************************************************
 * File Name	： MstFactoryUserMapper.java
 * Author		： Nguyen.Chuong
 * Version		： 1.0.0
 * Date Created	： 2013/09/24
 * Date Updated	： 2013/09/24
 * Description	： Contain method to get data from database .
 *
 ***********************************************************************/
package net.webike.RcProductFactory.mapper;

import java.util.List;

import net.webike.RcProductFactory.entity.EntMstFactoryUserTmp;

import org.apache.ibatis.annotations.Param;

/**
 * Interface MstFactoryUserMapper.
 */
public interface MstFactoryUserTmpMapper {

    /************************************************************************
     * <b>Description:</b><br>
     *  select user by user_id.
     *
     * @author		Tran.Thanh
     * @date		9 Jan 2014
     * @param       id user_id.
     * @return		EntMstFactoryUser
     ************************************************************************/
    EntMstFactoryUserTmp selectUserTmpById(@Param("Id")String id);

    /************************************************************************
     * <b>Description:</b><br>
     *  insert new user.
     *
     * @author		Tran.Thanh
     * @date		10 Jan 2014
     * @param ent		void
     ************************************************************************/
    void insertNewUserTmp(EntMstFactoryUserTmp ent);

    /************************************************************************
     * <b>Description:</b><br>
     *  check existed user_id in database.
     *
     * @author		Tran.Thanh
     * @date		10 Jan 2014
     * @param id userId
     * @return		String
     ************************************************************************/
    String selectUserIdExistedOrNot(@Param("Id")String id);


    /************************************************************************
     * <b>Description:</b><br>
     *  delete user by user_id and updatedUserId.
     *
     * @author		Tran.Thanh
     * @date		10 Jan 2014
     * @param userId userId
     * @param updatedUserId		void
     ************************************************************************/
    void deleteUserByUserId(@Param("userId")String userId,
    						@Param("updatedUserId")String updatedUserId);


    /************************************************************************
     * <b>Description:</b><br>
     *  active user by userId and updatedUserId.
     *
     * @author		Tran.Thanh
     * @date		10 Jan 2014
     * @param userId userId.
     * @param updatedUserId		void
     ************************************************************************/
    void activeUserByUserId(@Param("userId")String userId,
							@Param("updatedUserId")String updatedUserId);

    /************************************************************************
     * <b>Description:</b><br>
     *  update user info with param get from Entity.
     *
     * @author		Tran.Thanh
     * @date		4 Oct 2013
     * @param 		entUser	 entityUser.
     ************************************************************************/
    void updateUserInfo(EntMstFactoryUserTmp entUser);

    /************************************************************************
     * <b>Description:</b><br>
     *  Select user with conditions.
     *
     * @author      Tran.Thanh
     * @date        4 Oct 2013
     * @param       entUser entity user
     * @return      List<EntMstFactoryUser>
     ************************************************************************/
    List<EntMstFactoryUserTmp> selectFilterUser(EntMstFactoryUserTmp entUser);

    /************************************************************************
     * <b>Description:</b><br>
     *  Select user by userID with new table.
     *
     * @author		Tran.Thanh
     * @date		14 Jan 2014
     * @param 		id String
     * @return		EntMstFactoryUser
     ************************************************************************/
    EntMstFactoryUserTmp selectUserById(@Param("Id")String id);

    /************************************************************************
     * <b>Description:</b><br>
     *  Select list all user with new table.
     *
     * @author		Tran.Thanh
     * @date		14 Jan 2014
     * @return		List<EntMstFactoryUserTmp>
     ************************************************************************/
    List<EntMstFactoryUserTmp> selectAllUser();

    /************************************************************************
     * <b>Description:</b><br>
     *  Select list all user for editabrand page.
     *
     * @author		Tran.Thanh
     * @date		14 Jan 2014
     * @return		List<EntMstFactoryUser>
     ************************************************************************/
    List<EntMstFactoryUserTmp> selectListAllUser();

    /************************************************************************
     * <b>Description:</b><br>
     * get all full name of user in DB.
     *
     * @return      List<String>
     * @author      Long Vu
     * @date        Feb 18, 2014
     ************************************************************************/
    List<String> selectAllFullname();
}
