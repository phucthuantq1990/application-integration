/************************************************************************
 * File Name	： TblFactoryProductSelectMapper.java
 * Author		： Luong.Dai
 * Version		： 1.0.0
 * Date Created	： 2014/02/20
 * Date Updated	： 2014/02/20
 * Description	： Contain method to get data from rc_product_factory.tbl_factory_product_select.
 *
 ***********************************************************************/
package net.webike.RcProductFactory.mapper;

import java.util.List;

import net.webike.RcProductFactory.entity.EntMstProduct;
import net.webike.RcProductFactory.entity.EntTblFactoryProductSelect;

import org.apache.ibatis.annotations.Param;


/**
 * Interface TblProductVideoMapper.
 */
public interface TblFactoryProductSelectMapper {
    /************************************************************************
     * <b>Description:</b><br>
     *  select list select code of product by productId.
     *
     * @author		Luong.Dai
     * @date		Feb 20, 2014
     * @param       select EntTblFactoryProductSelect
     * @return		List<EntTblFactoryProductSelect>
     ************************************************************************/
    List<EntTblFactoryProductSelect> selectListSelectCodeByProductId(EntTblFactoryProductSelect select);

    /************************************************************************
     * <b>Description:</b><br>
     *  select productSelectDisplay by productId and selectCode.
     *
     * @author		Luong.Dai
     * @date		Feb 21, 2014
     * @param       select EntTblFactoryProductSelect
     * @return		String
     ************************************************************************/
    String selectProductSelectDisplayByProductIdAndSelectCode(EntTblFactoryProductSelect select);

    /************************************************************************
     * <b>Description:</b><br>
     *  select list select of product by select code array.
     *
     * @author      hoang.ho
     * @date        2014/02/25
     * @param       selectCodeList string select code list
     * @return      List<EntTblFactoryProductSelect>
     ************************************************************************/
    List<EntTblFactoryProductSelect> selectListSelectByCodeList(@Param("selectCodeList")String selectCodeList);

    /************************************************************************
     * <b>Description:</b><br>
     * insert new factory product select.
     *
     * @author      Long Vu
     * @param       prd insert condition
     * @date        Feb 25, 2014
     * @return      int
     ************************************************************************/
    int insertNewFactoryProductSelect(EntMstProduct prd);

    /************************************************************************
     * <b>Description:</b><br>
     *  Update select display value.
     *
     * @author		Luong.Dai
     * @date		Mar 3, 2014
     * @param       select EntTblFactoryProductSelect
     * @return		Integer
     ************************************************************************/
    Integer updateSelectDisplayByProductIdAndSelectCode(EntTblFactoryProductSelect select);

    /************************************************************************
     * <b>Description:</b><br>
     *  delete list select of product by select_code.
     *
     * @author		Luong.Dai
     * @date		Mar 4, 2014
     * @param       productId           Long
     * @param       listDeleteSelect    String
     ************************************************************************/
    void deleteListProductSelectOfProduct(@Param("productId")Long productId, @Param("listDeleteSelect")String listDeleteSelect);

    /************************************************************************
     * <b>Description:</b><br>
     *  Insert new select of product.
     *
     * @author		Luong.Dai
     * @date		Mar 4, 2014
     * @param       select  EntTblFactoryProductSelect
     * @return		Integer
     ************************************************************************/
    Integer insertNewSelectOfProduct(EntTblFactoryProductSelect select);

    /************************************************************************
     * <b>Description:</b><br>
     *  insertMultipleProductSelect.
     *
     * @author      Doan.Chuong
     * @date        Mar 6, 2014
     * @param       list  List<EntTblFactoryProductSelect>
     * @return      Integer
     ************************************************************************/
    Integer insertMultipleProductSelect(@Param("list")List<EntTblFactoryProductSelect> list);

    /************************************************************************
     * <b>Description:</b><br>
     *  selectProductSelectBySyouhinSysCodeList.
     *
     * @author      Doan.Chuong
     * @date        Mar 12, 2014
     * @param       list  List<String>
     * @return      List<EntTblFactoryProductSelect>
     ************************************************************************/
    List<EntTblFactoryProductSelect> selectProductSelectBySyouhinSysCodeList(List<String> list);

    /************************************************************************
     * <b>Description:</b><br>
     *  Delete list select not exists in list current select.
     *
     * @author		Luong.Dai
     * @date		Mar 4, 2014
     * @param       productId           Long
     * @param       listDeleteSelect    String
     ************************************************************************/
    void deleteListProductSelectNotExistsInCurrentSelect(@Param("productId")Long productId, @Param("listDeleteSelect")String listDeleteSelect);

    /************************************************************************
     * <b>Description:</b><br>
     *  Select list select display by list productId and select code.
     *
     * @author		Luong.Dai
     * @date		Jun 14, 2014
     * @param 		selectCode		select code
     * @param 		lstProductId	list productId
     * @return		List<EntTblFactoryProductSelect>	list select display
     ************************************************************************/
    List<EntTblFactoryProductSelect> selectProductSelectDisplayByListProductIdAndSelectCode(@Param("selectCode")Integer selectCode
                                                                                          , @Param("lstProductId")List<Long> lstProductId);

    /************************************************************************
     * <b>Description:</b><br>
     *  select list select display of product id.
     *
     * @author		Luong.Dai
     * @date		Jun 16, 2014
     * @param 		productId		product id to select
     * @param 		lstSelectCode	list select code
     * @return		List<EntTblFactoryProductSelect>	result
     ************************************************************************/
    List<EntTblFactoryProductSelect> selectProductSelectDisplayOfProductBySelectCode(@Param("productId")Long productId
                                                                                   , @Param("lstSelectCode")List<EntTblFactoryProductSelect> lstSelectCode);
}
