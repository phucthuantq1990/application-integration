///************************************************************************
// * File Name	: MstFactoryUserMapper.java
// * Author		: Nguyen.Chuong
// * Version		: 1.0.0
// * Date Created	: 2013/09/24
// * Date Updated	: 2013/09/24
// * Description	: Contain method to get data from database .
// *
// ***********************************************************************/
//package net.webike.RcProductFactory.mapper;
//
//import java.util.List;
//
//import net.webike.RcProductFactory.entity.EntMstFactoryUser;
//
//import org.apache.ibatis.annotations.Param;
//
///**
// * Interface MstFactoryUserMapper.
// */
//public interface MstFactoryUserMapper {
//
//    /************************************************************************
//     * <b>Description:</b><br>
//     *  select all user from mst_factory_user table.
//     *
//     * @author		Nguyen.Chuong
//     * @date		Sep 10, 2013
//     * @return		List<EntMstFactoryUser>
//     ************************************************************************/
//    List<EntMstFactoryUser> selectAllUser();
//
//    /************************************************************************
//     * <b>Description:</b><br>
//     *  select list all user ( del_flg = 0 && del_flg = 1 ).
//     *
//     * @author		Tran.Thanh
//     * @date		6 Jan 2014
//     * @return		List<EntMstFactoryUser>
//     ************************************************************************/
//    List<EntMstFactoryUser> selectListAllUser();
//
//
//    /************************************************************************
//     * <b>Description:</b><br>
//     *  select user from mst_factory_user table by Id.
//     *
//     * @author		Nguyen.Chuong
//     * @date		Sep 11, 2013
//     * @param       id string
//     * @return		List<EntMstFactoryUser>
//     ************************************************************************/
//    EntMstFactoryUser selectUserById(@Param("Id")String id);
//
//    /************************************************************************
//     * <b>Description:</b><br>
//     *  Select user with conditions.
//     *
//     * @author		Tran.Thanh
//     * @date		4 Oct 2013
//     * @param 		entUser entity user
//     * @return		List<EntMstFactoryUser>
//     ************************************************************************/
//    List<EntMstFactoryUser> selectFilterUser(EntMstFactoryUser entUser);
//
//
//    /************************************************************************
//     * <b>Description:</b><br>
//     *  delete user by user_id and kousin_id.
//     *
//     * @author		Tran.Thanh
//     * @date		3 Oct 2013
//     * @param 		userId user_id
//     * @param 		kousinId	kousin_id
//     ************************************************************************/
//    void deleteUserByUserId(@Param("userId")String userId,
//    						@Param("kousinId")String kousinId);
//
//    /************************************************************************
//     * <b>Description:</b><br>
//     *  active user by user_id and kousin_id.
//     *
//     * @author		Tran.Thanh
//     * @date		3 Oct 2013
//     * @param 		userId user_id
//     * @param 		kousinId kousin_id
//     ************************************************************************/
//    void activeUserByUserId(@Param("userId")String userId,
//							@Param("kousinId")String kousinId);
//
//    /************************************************************************
//     * <b>Description:</b><br>
//     *  select all info user by userId.
//     *
//     * @author		Tran.Thanh
//     * @date		3 Oct 2013
//     * @param 		userId user_id
//     * @return		EntMstFactoryUser
//     ************************************************************************/
//    EntMstFactoryUser selectAllInfoUserById(@Param("userId")String userId);
//
//    /************************************************************************
//     * <b>Description:</b><br>
//     *  insert new user to database.
//     *
//     * @author		Nguyen.Chuong
//     * @date		Oct 2, 2013
//     * @param       ent EntMstFactoryUser
//     * @return      Integer
//     ************************************************************************/
//    Integer insertNewUser(EntMstFactoryUser ent);
//
//    /************************************************************************
//     * <b>Description:</b><br>
//     *  check userId is existed or not in DB.
//     *
//     * @author      Nguyen.Chuong
//     * @date        Sep 11, 2013
//     * @param       id string
//     * @return      userId String
//     ************************************************************************/
//    String selectUserIdExistedOrNot(@Param("Id")String id);
//
//    /************************************************************************
//     * <b>Description:</b><br>
//     *  update user info with param get from Entity.
//     *
//     * @author		Tran.Thanh
//     * @date		4 Oct 2013
//     * @param 		entUser	 entityUser.
//     ************************************************************************/
//    void updateUserInfo(EntMstFactoryUser entUser);
//}
