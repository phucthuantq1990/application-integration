/************************************************************************
 * File Name    ： TblSyouhinFitModelMapper.java
 * Author       ： Nguyen.Chuong
 * Version      ： 1.0.0
 * Date Created ： 2014/02/17
 * Date Updated ： 2014/02/17
 * Description  ： TblSyouhinFitModelMapper to process data of rc_syouhin.tbl_syouhin_fit_model page.
 ***********************************************************************/
package net.webike.RcProductFactory.mapper;

import java.util.List;

import net.webike.RcProductFactory.entity.EntTblSyouhinFitModel;

/**
 * TblSyouhinFitModelMapper to process data of rc_syouhin.tbl_syouhin_fit_model page.
 */
public interface TblSyouhinFitModelMapper {

    /************************************************************************
     * <b>Description:</b><br>
     *  select list syouhinFitModel by syouhinSysCode.
     * @author      Long Vu
     * @date        Feb 12, 2014
     * @param       syouhinSysCode Long
     * @return      List<EntTblSyouhinFitModel>
     ************************************************************************/
    List<EntTblSyouhinFitModel> selectListSyouhinFitModelBySyouhinSysCode(Long syouhinSysCode);
}
