/************************************************************************
 * File Name	： TblFactoryProductDescriptionMapper.java
 * Author		： Tran.Thanh
 * Version		： 1.0.0
 * Date Created	： 2014/02/12
 * Date Updated	： 2014/02/12
 * Description	： Contain method to get data from database .
 *
 ***********************************************************************/
package net.webike.RcProductFactory.mapper;

import java.sql.SQLException;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import net.webike.RcProductFactory.entity.EntMstProduct;
import net.webike.RcProductFactory.entity.EntTblFactoryProductDescription;
import net.webike.RcProductFactory.entity.EntTblFactoryProductNew;

/**
 * Interface MstMarumeMapper.
 */
public interface TblFactoryProductDescriptionMapper {

	/************************************************************************
	 * <b>Description:</b><br>
	 *  select Remarks By ProductId From Tbl_Factory_Product_Description table.
	 *
	 * @author		Tran.Thanh
	 * @date		12 Feb 2014
	 * @param 		productId Long
	 * @return		EntTblFactoryProductDescription
	 ************************************************************************/
	EntTblFactoryProductDescription selectRemarksByProductId(Long productId);

	/************************************************************************
	 * <b>Description:</b><br>
	 *  insert New Product Description.
	 *
	 * @author		Tran.Thanh
	 * @date		12 Feb 2014
	 * @param       entTblFactoryProductDescription entTblFactoryProductDescription
	 * @return		long
	 ************************************************************************/
	long insertNewProductDescription(EntTblFactoryProductDescription entTblFactoryProductDescription);

	/************************************************************************
	 * <b>Description:</b><br>
	 *  update Product Description.
	 *
	 * @author		Tran.Thanh
	 * @date		13 Feb 2014
	 * @param 		entTblFactoryProductDescription		void
	 ************************************************************************/
	void updateProductDescription(EntTblFactoryProductDescription entTblFactoryProductDescription);

	/************************************************************************
	 * <b>Description:</b><br>
	 *  Delete product Description by productId.
	 *
	 * @author		Tran.Thanh
	 * @date		21 Feb 2014
	 * @param productId		void
	 ************************************************************************/
	void deleteProductDescription(@Param("productId")String productId);

    /************************************************************************
     * <b>Description:</b><br>
     * insert new factory product description.
     *
     * @author      Long Vu
     * @param       prd insert condition
     * @date        Feb 25, 2014
     * @return      int
     ************************************************************************/
    int insertNewFactoryProductDescription(EntMstProduct prd);

    /************************************************************************
     * <b>Description:</b><br>
     *  update Product Description By Product.
     *
     * @author		Tran.Thanh
     * @date		25 Feb 2014
     * @param 		entTblFactoryProductNew		void
     ************************************************************************/
    void updateProductDescriptionByProduct(EntTblFactoryProductNew entTblFactoryProductNew);

    /************************************************************************
     * <b>Description:</b><br>
     *  insertNewFactoryProductDescriptionByProduct.
     * @author		Le.Dinh
     * @date		3 Mar 2014
     * @param 		entTblFactoryProductNew	EntTblFactoryProductNew
     * @return		Integer
     * @throws 		SQLException	SQLException
     ************************************************************************/
    Integer insertNewFactoryProductDescriptionByProduct(EntTblFactoryProductNew entTblFactoryProductNew) throws SQLException;

    /************************************************************************
     * <b>Description:</b><br>
     * insert multiple new factory product description.
     *
     * @author      Doan Chuong
     * @date        Match 12, 2014
     * @param       list List<String>
     * @param       productId int
     * @param       updatedUserId String
     * @return      int
     ************************************************************************/
    int insertMultipleNewFactoryProductDescription(@Param("list") List<String> list
                                                 , @Param("productId") long productId
                                                 , @Param("updatedUserId") String updatedUserId);

    /************************************************************************
     * <b>Description:</b><br>
     *  Select product description by product id.
     *
     * @author		Luong.Dai
     * @date		Mar 25, 2014
     * @param 		productId		Long
     * @return		EntTblFactoryProductDescription
     ************************************************************************/
    EntTblFactoryProductDescription selectProductDescriptionByProductId(@Param("productId")Long productId);

    /************************************************************************
     * <b>Description:</b><br>
     *  update Factory List Product Description.
     *
     * @author		Tran.Thanh
     * @date		Apr 23, 2014
     * @param 		list list ProductId
     * @param 		entTblFactoryProductDescription EntTblFactoryProductDescription
     * @return		Integer
     ************************************************************************/
    Integer updateProductDescriptionByModeReplace(@Param("list") List<Long> list,
    	@Param("entTblFactoryProductDescription") EntTblFactoryProductDescription entTblFactoryProductDescription);

    /************************************************************************
     * <b>Description:</b><br>
     *  Insert list description into table rc_product_factory.tbl_factory_product_description.
     *
     * @author		Luong.Dai
     * @date		May 22, 2014
     * @param 		listDescription 	List to insert
     * @return		Integer				Insert result
     ************************************************************************/
    Integer insertListNewFactoryProductDescriptionByProduct(List<EntTblFactoryProductDescription> listDescription);
}
