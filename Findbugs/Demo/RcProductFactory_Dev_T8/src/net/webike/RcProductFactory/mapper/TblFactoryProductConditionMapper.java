/************************************************************************
 * File Name	： TblFactoryProductConditionMapper.java
 * Author		： Long Vu
 * Version		： 1.0.0
 * Date Created	： 2014/02/24
 * Date Updated	： 2014/02/24
 * Description	： TblFactoryProductConditionMapper.
 *
 ***********************************************************************/
package net.webike.RcProductFactory.mapper;

import java.sql.SQLException;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import net.webike.RcProductFactory.entity.EntMstProduct;
import net.webike.RcProductFactory.entity.EntTblFactoryProductCondition;
import net.webike.RcProductFactory.entity.EntTblFactoryProductGeneral;

/**
 * Interface TblFactoryProductConditionMapper.
 */
public interface TblFactoryProductConditionMapper {

    /************************************************************************
     * <b>Description:</b><br>
     * insert new factory product condition.
     *
     * @author      Long Vu
     * @param       prd insert condition
     * @date        Feb 25, 2014
     * @return      int
     ************************************************************************/
    int insertNewFactoryProductCondition(EntMstProduct prd);

    /************************************************************************
     * <b>Description:</b><br>
     *  updateFactoryProductCondition.
     *
     * @author		Le.Dinh
     * @date		26 Feb 2014
     * @param 		entTblFactoryProductGeneral	EntTblFactoryProductGeneral
     * @return		Integer
     * @throws 		SQLException		SQLException
     ************************************************************************/
    Integer updateFactoryProductCondition(EntTblFactoryProductGeneral entTblFactoryProductGeneral) throws SQLException;

    /************************************************************************
     * <b>Description:</b><br>
     *  update Factory List Product Condition.
     *
     * @author		Tran.Thanh
     * @date		Apr 22, 2014
     * @param 		list list productID
     * @param 		entTblFactoryProductGeneral EntTblFactoryProductGeneral
     * @return		number of row has effect
     * @throws 		SQLException		Integer
     ************************************************************************/
    Integer updateFactoryListProductCondition(@Param("list") List<Long> list,
			@Param("entTblFactoryProductGeneral") EntTblFactoryProductGeneral entTblFactoryProductGeneral) throws SQLException;

    /************************************************************************
     * <b>Description:</b><br>
     *  insertNewFactoryProductConditionByProductCondition.
     *
     * @author		Le.Dinh
     * @date		3 Mar 2014
     * @param 		entTblFactoryProductGeneral	EntTblFactoryProductGeneral
     * @return		Integer
     * @throws 		SQLException	SQLException
     ************************************************************************/
    Integer	insertNewFactoryProductConditionByProductGeneral(EntTblFactoryProductGeneral entTblFactoryProductGeneral) throws SQLException;

    /************************************************************************
     * <b>Description:</b><br>
     *  insert list data into table tbl_factory_product_condition in rc_product_factory DB.
     *
     * @author		Nguyen.Chuong
     * @date		May 22, 2014
     * @param       listCondition List<EntTblFactoryProductCondition>
     * @return      Integer
     ************************************************************************/
    Integer insertMulipleProductCondition(@Param("listCondition")List<EntTblFactoryProductCondition> listCondition);

    /************************************************************************
     * <b>Description:</b><br>
     * insert multiple new factory product condition.
     *
     * @author      Doan Chuong
     * @date        Match 12, 2014
     * @param       list List<String>
     * @param       productId int
     * @param       updatedUserId String
     * @return      int
     ************************************************************************/
    int insertMultipleNewFactoryProductCondition(@Param("list") List<String> list
                                               , @Param("productId") long productId
                                               , @Param("updatedUserId") String updatedUserId);

}
