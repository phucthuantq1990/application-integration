/************************************************************************
 * File Name	： TblFactoryProductNumberingMapper.java
 * Author		： Le.Dinh
 * Version		： 1.0.0
 * Date Created	： 2014/03/03
 * Date Updated	： 2014/03/03
 * Description	： Contain method to get data from database .
 *
 ***********************************************************************/
package net.webike.RcProductFactory.mapper;

import java.sql.SQLException;


/**
 * Interface MstMarumeMapper.
 */
public interface TblFactoryProductNumberingMapper {

	/************************************************************************
	 * <b>Description:</b><br>
	 *  selectMaxProductIdPlusOne.
	 * @author		Le.Dinh
	 * @date		3 Mar 2014
	 * @return		Long
	 * @throws 		SQLException	SQLException
	 ************************************************************************/
	Long selectMaxProductIdPlusOne() throws SQLException;

	/************************************************************************
	 * <b>Description:</b><br>
	 *  insertNewTblFactoryProductNumbering.
	 * @author		Le.Dinh
	 * @date		3 Mar 2014
	 * @param 		productIdMax	Integer
	 * @return		Integer
	 * @throws 		SQLException	SQLException
	 ************************************************************************//*
	Integer insertNewTblFactoryProductNumbering(Integer productIdMax) throws SQLException;*/

	/************************************************************************
     * <b>Description:</b><br>
     * Update product Id Max in table numbering.
     * @author      Long Vu
     * @date        10 Mar 2014
     * @param       productIdMax    long
     * @return      Integer
     ************************************************************************/
    int updateTblFactoryProductNumbering(long productIdMax);
}
