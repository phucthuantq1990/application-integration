/************************************************************************
 * File Name	： TblFactoryImportConditionMapper.java
 * Author		： Long Vu
 * Version		： 1.0.0
 * Date Created	： 2014/02/21
 * Date Updated	： 2014/02/21
 * Description	：TblFactoryImportConditionMapper .
 *
 ***********************************************************************/
package net.webike.RcProductFactory.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import net.webike.RcProductFactory.entity.EntTblFactoryImportCondition;


/**
 * Interface TblFactoryImportConditionMapper.
 */
public interface TblFactoryImportConditionMapper {

    /************************************************************************
     * <b>Description:</b><br>
     * get import conditon list by matter no.
     *
     * @return      List<EntTblFactoryImportCondition>
     * @param       matterNo matter no
     * @author      Long Vu
     * @date        Feb 21, 2014
     ************************************************************************/
    List<EntTblFactoryImportCondition> selectImportConditionListByMatterNo(long matterNo);

    /************************************************************************
     * <b>Description:</b><br>
     * insert new import condition.
     *
     * @return      int
     * @param       entTblFactoryImportCondition insert condition
     * @author      Long Vu
     * @param       importMap map to store import condition code return from database when insert success.
     * @date        Feb 24, 2014
     ************************************************************************/
    int insertImportCondition(@Param("entTblFactoryImportCondition")EntTblFactoryImportCondition entTblFactoryImportCondition
                            , @Param("map")Map<String, Integer> importMap);

    /************************************************************************
     * <b>Description:</b><br>
     * update import condition by count up import condition exe count.
     *
     * @return      int
     * @param       entTblFactoryImportCondition insert condition
     * @author      Long Vu
     * @date        Feb 24, 2014
     ************************************************************************/
    int updateImportConditionByCode(EntTblFactoryImportCondition entTblFactoryImportCondition);

    /************************************************************************
     * <b>Description:</b><br>
     * update import condition by count up import condition exe count.
     *
     * @return      int
     * @param       entTblFactoryImportCondition insert condition
     * @author      Doan Chuong
     * @date        March 12, 2014
     ************************************************************************/
    int updateImportConditionExeCount(EntTblFactoryImportCondition entTblFactoryImportCondition);

    /************************************************************************
     * <b>Description:</b><br>
     * update import condition.
     *
     * @return      int
     * @param       entTblFactoryImportCondition store values to update.
     * @author      hoang.ho
     * @date        2014/03/04
     ************************************************************************/
    int updateImportCondition(EntTblFactoryImportCondition entTblFactoryImportCondition);

    /************************************************************************
     * <b>Description:</b><br>
     * update import condition status.
     *
     * @return      int
     * @param       entTblFactoryImportCondition insert condition
     * @author      Long Vu
     * @date        Feb 27, 2014
     ************************************************************************/
    int updateImportConditionStatus(EntTblFactoryImportCondition entTblFactoryImportCondition);

    /***********************************************************************
     * <b>Description:</b><br>
     *  Get import condition not working by matter number.
     *
     * @author      hoang.ho
     * @date        2014/02/27
     * @param       matterNo matter number
     * @return      import condition code
     ************************************************************************/
     List<EntTblFactoryImportCondition> getImportConditionNotWorkingByMatterNo(@Param("matterNo") long matterNo);

}
