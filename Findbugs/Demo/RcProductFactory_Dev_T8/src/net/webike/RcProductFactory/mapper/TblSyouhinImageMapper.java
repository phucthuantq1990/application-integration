/************************************************************************
 * File Name	： MstSiireMapper.java
 * Author		： Le.Dinh
 * Version		： 1.0.0
 * Date Created	： 2014/02/12
 * Date Updated	： 2014/02/12
 * Description	： Contain method to get data from database .
 *
 ***********************************************************************/
package net.webike.RcProductFactory.mapper;

import java.util.List;

import net.webike.RcProductFactory.entity.EntTblSyouhinImage;

/**
 * Interface MstMarumeMapper.
 */
public interface TblSyouhinImageMapper {
    /************************************************************************
     * <b>Description:</b><br>
     *  select list image by syouhinSysCode from tbl_syouhin_image in rc_syouhin.
     *
     * @author		Nguyen.Chuong
     * @date		Feb 15, 2014
     * @param       syouhinSysCode integer
     * @return		List<EntTblSyouhinImage>
     ************************************************************************/
    List<EntTblSyouhinImage> selectListImageBySyouhinSysCode(Long syouhinSysCode);
}
