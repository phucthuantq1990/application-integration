/************************************************************************
 * File Name	： TblFactorySettingMapper.java
 * Author		： Nguyen.Chuong
 * Version		： 1.0.0
 * Date Created	： 2014/01/13
 * Description	： Contain method to get data from database .
 *
 ***********************************************************************/
package net.webike.RcProductFactory.mapper;

import java.util.List;

import net.webike.RcProductFactory.entity.EntTblFactorySetting;

/**
 * Interface TblFactorySettingMapper.
 */
public interface TblFactorySettingMapper {


    /************************************************************************
     * <b>Description:</b><br>
     *  Select setting object by setting_code.
     *
     * @author		Nguyen.Chuong
     * @date		Jan 13, 2014
     * @param       settingCode String
     * @return		settingCode
     ************************************************************************/
     String selectSettingValueBySettingCode(String settingCode);

     /************************************************************************
     * <b>Description:</b><br>
     *  Select List Factory Setting.
     *
     * @author		Tran.Thanh
     * @date		13 Jan 2014
     * @return		List<EntTblFactorySetting>
     ************************************************************************/
    List<EntTblFactorySetting> selectListFactorySetting();
}
