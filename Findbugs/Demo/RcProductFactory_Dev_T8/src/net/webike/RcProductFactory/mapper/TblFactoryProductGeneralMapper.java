/************************************************************************
 * File Name    ： TblFactoryProductGeneralMapper.java
 * Author       ：Le Dinh
 * Version      ： 1.0.0
 * Date Created ： 2014/02/11
 * Date Updated ： 2014/02/11
 * Description  ： TblFactoryProductGeneralMapper.
 ***********************************************************************/
package net.webike.RcProductFactory.mapper;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import net.webike.RcProductFactory.entity.EntMstProduct;
import net.webike.RcProductFactory.entity.EntTblFactoryProductGeneral;
import net.webike.RcProductFactory.entity.EntTblFactoryProductNew;


/**
 * TblFactoryProductGeneralMapper.
 */
public interface TblFactoryProductGeneralMapper {
	/************************************************************************
	 * <b>Description:</b><br>
	 *  selectProductFactoryProductGeneralByProductid.
	 * @author		Le.Dinh
	 * @date		11 Feb 2014
	 * @param		productId	Long
	 * @return		EntTblFactoryProductGeneral	EntTblFactoryProductGeneral
	 * @throws 		SQLException	SQLException
	 ************************************************************************/
	EntTblFactoryProductGeneral selectProductFactoryProductGeneralByProductid(Long productId) throws SQLException;

	/************************************************************************
	 * <b>Description:</b><br>
	 *  updateProductFactoryProductGeneral.
	 *
	 * @author		Le.Dinh
	 * @date		25 Feb 2014
	 * @param 		entTblFactoryProductGeneral	updateProductFactoryProductGeneral
	 * @return		Integer
	 * @throws 		SQLException	SQLException
	 ************************************************************************/
	Integer updateProductFactoryProductGeneral(EntTblFactoryProductGeneral entTblFactoryProductGeneral) throws SQLException;

	/************************************************************************
	 * <b>Description:</b><br>
	 *  select category_code of product by productId.
	 *
	 * @author		Luong.Dai
	 * @date		Feb 12, 2014
	 * @param       productId Long
	 * @return      category_code Integer
	 * @throws      SQLException exception
	 ************************************************************************/
	Integer selectCategoryCodeByProductId(Long productId) throws SQLException;

	/************************************************************************
	 * <b>Description:</b><br>
	 *  select product brand code, product group code and product matter no.
	 *
	 * @author		Luong.Dai
	 * @date		Feb 19, 2014
	 * @param       productId String
	 * @return      EntTblFactoryProductGeneral
	 * @throws      SQLException Integer
	 ************************************************************************/
	EntTblFactoryProductGeneral selectProductGeneralValueByProductId(String productId) throws SQLException;

    /************************************************************************
     * <b>Description:</b><br>
     * insert new factory product general.
     *
     * @author      Long Vu
     * @date        Feb 24, 2014
     * @param       prd mst_product
     * @return      int
     ************************************************************************/
    int insertNewFactoryProductGeneral(EntMstProduct prd);

    /************************************************************************
     * <b>Description:</b><br>
     *  insertNewFactoryProductGeneralByProductGeneral.
     *
     * @author		Le.Dinh
     * @date		3 Mar 2014
     * @param 		entTblFactoryProductGeneral	EntTblFactoryProductGeneral
     * @return		Integer
     * @throws 		SQLException		SQLException
     ************************************************************************/
    Integer	insertNewFactoryProductGeneralByProductGeneral(EntTblFactoryProductGeneral entTblFactoryProductGeneral) throws SQLException;

    /************************************************************************
     * <b>Description:</b><br>
     *  updateProductGeneralByProductId.
     *
     * @author		Tran.Thanh
     * @date		25 Feb 2014
     * @param 		entTblFactoryProductNew		void
     ************************************************************************/
    void updateProductGeneralByProductId(EntTblFactoryProductNew entTblFactoryProductNew);

    /************************************************************************
     * <b>Description:</b><br>
     *  Update product_code, product_proper_price and product_ean_code.
     *
     * @author		Luong.Dai
     * @date		Mar 3, 2014
     * @param       product EntTblFactoryProductNew
     * @return		Integer
     ************************************************************************/
    Integer updateProductCodeProperPriceAndEANCodeByProductId(EntTblFactoryProductNew product);

    /************************************************************************
     * <b>Description:</b><br>
     *  insert new product to product general.
     *
     * @author		Luong.Dai
     * @date		Mar 4, 2014
     * @param       product     EntTblFactoryProductGeneral
     * @return		Integer
     ************************************************************************/
    Integer insertNewFactoryProductGeneralOfGroupProduct(EntTblFactoryProductGeneral product);

    /************************************************************************
     * <b>Description:</b><br>
     *  Select updatedOn of productGeneral.
     *
     * @author		Luong.Dai
     * @date		Mar 4, 2014
     * @param       productId   Long
     * @return		Date
     ************************************************************************/
    Date selectUpdatedOnOfProductByProductId(Long productId);

    /************************************************************************
     * <b>Description:</b><br>
     * insert multiple new factory product general.
     *
     * @author      Doan Chuong
     * @date        Match 12, 2014
     * @param       list List<String>
     * @param       productId int
     * @param       updatedUserId String
     * @return      int
     ************************************************************************/
    int insertMultipleNewFactoryProductGeneral(@Param("list") List<String> list
                                             , @Param("productId") long productId
                                             , @Param("updatedUserId") String updatedUserId);

    /************************************************************************
     * <b>Description:</b><br>
     *  Update product group code.
     *
     * @author		Luong.Dai
     * @date		Mar 20, 2014
     * @param 		product		EntTblFactoryProductGeneral
     * @return		Integer
     ************************************************************************/
    Integer updateProductGroupCodeByProductId(EntTblFactoryProductGeneral product);

    /************************************************************************
     * <b>Description:</b><br>
     *  update ProductGeneral Popup Block 15.
     *
     * @author		Tran.Thanh
     * @date		Apr 22, 2014
     * @param 		list list ProductId
     * @param 		entTblFactoryProductGeneral ent for query
     * @return		Integer
     ************************************************************************/
    Integer updateProductGeneralPopupBlock15(@Param("list") List<Long> list,
			@Param("entTblFactoryProductGeneral") EntTblFactoryProductGeneral entTblFactoryProductGeneral);

    /************************************************************************
     * <b>Description:</b><br>
     * Delete product general by product id.
     *
     * @author		Luong.Dai
     * @date		May 12, 2014
     * @param 		lstProductId	List product id
     * @return		Integer			Result
     ************************************************************************/
    Integer deleteProductGeneralByListProductId(@Param("list") List<Long> lstProductId);

    /************************************************************************
     * <b>Description:</b><br>
     *  insert list product general.
     *
     * @author		Luong.Dai
     * @date		May 19, 2014
     * @param 		lstProductGeneral				List product general
     * @return		Integer							Insert result
     ************************************************************************/
    Integer insertListNewFactoryProductGeneralByProductGeneral(
                                        @Param("lstProductGeneral") List<EntTblFactoryProductGeneral> lstProductGeneral);

    /************************************************************************
     * <b>Description:</b><br>
     *  count total group code in matter.
     *
     * @author		Tran.Thanh
     * @date		Jun 13, 2014
     * @param 		matterNo matter number
     * @return		int
     ************************************************************************/
    List<EntTblFactoryProductGeneral> selectListGeneralByMatterNo(@Param("matterNo")Long matterNo);

    /************************************************************************
     * <b>Description:</b><br>
     * Select product proper price and product supplier price of priduct.
     *
     * @author		Luong.Dai
     * @date		Jun 16, 2014
     * @param 		productId						Product id
     * @return		EntTblFactoryProductGeneral		Ent result
     ************************************************************************/
    EntTblFactoryProductGeneral selectProductPriceByProductId(String productId);
}
