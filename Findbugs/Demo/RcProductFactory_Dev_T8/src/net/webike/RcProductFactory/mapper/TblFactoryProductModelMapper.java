/************************************************************************
 * File Name    ： TblFactoryProductModelMapper.java
 * Author       ： Long Vu
 * Version      ： 1.0.0
 * Date Created ： 2014/01/23
 * Date Updated ： 2014/01/23
 * Description  ： TblFactoryProductModel Mapper.
 ***********************************************************************/
package net.webike.RcProductFactory.mapper;

import java.sql.SQLException;
import java.util.List;

import net.webike.RcProductFactory.entity.EntMstProduct;
import net.webike.RcProductFactory.entity.EntTblFactoryProductFitModel;
import net.webike.RcProductFactory.entity.EntTblFactoryProductNew;
import net.webike.RcProductFactory.entity.EntTblTmpCSV;

import org.apache.ibatis.annotations.Param;

/**
 * TblFactoryProductModel Mapper.
 * @author Long Vu
 * Date Created ： 2014/01/25
 */
public interface TblFactoryProductModelMapper {

    /************************************************************************
     * <b>Description:</b><br>
     *  select list productFitModel by productCode.
     * @author      Nguyen.Chuong
     * @date        Jan 23, 2014
     * @param       entproduct content filter and productCode
     * @return      List<EntTblFactoryProductModel>
     ************************************************************************/
    List<EntTblFactoryProductFitModel> selectListFitModelOfProduct(EntTblFactoryProductFitModel entproduct);

    /************************************************************************
     * <b>Description:</b><br>
     * delete fit model by product id.
     *
     * @author      Long Vu
     * @date        Jan 23, 2014
     * @param       pl list product id.
     ************************************************************************/
    void deleteFitModel(List<Long> pl);

    /************************************************************************
     * <b>Description:</b><br>
     * get list fit model with max model sort.
     *
     * @author      Long Vu
     * @return      List<EntTblFactoryProductFitModel>
     * @date        Jan 23, 2014
     * @param       prdIdList list product id.
     ************************************************************************/
    List<EntTblFactoryProductFitModel> selectListFitModelWithMaxModelSort(List<Long> prdIdList);

    /************************************************************************
     * <b>Description:</b><br>
     * insert fit model object to DB.
     *
     * @author      Long Vu
     * @return      int
     * @date        Jan 23, 2014
     * @param       obj fit model object.
     ************************************************************************/
    int insertToFitModel(EntTblFactoryProductFitModel obj);
    
    /************************************************************************
     * <b>Description:</b><br>
     * Insert fitModel multi rows.
     *
     * @author      Nguyen.Chuong
     * @return      int
     * @date        2014/08/15
     * @param       listFitModelToInsert List<EntTblFactoryProductFitModel> of one product.
     ************************************************************************/
    int insertFitModelMultiRow(@Param("listFitModelToInsert")List<EntTblFactoryProductFitModel> listFitModelToInsert);

    /************************************************************************
     * <b>Description:</b><br>
     *  select list productFitModel by productId.
     * @author      Long Vu
     * @date        Feb 12, 2014
     * @param       productId product Id
     * @return      List<EntTblFactoryProductModel>
     ************************************************************************/
    List<EntTblFactoryProductFitModel> selectListFitModelByProductId(Long productId);

    /************************************************************************
     * <b>Description:</b><br>
     * insert new fit model object by product.
     * @author      Long Vu
     * @date        Feb 25, 2014
     * @param       prd insert condition
     * @return      int
     ************************************************************************/
    int insertNewFactoryProductFitModel(EntMstProduct prd);

    /************************************************************************
     * <b>Description:</b><br>
     * Delete fit model by condition.
     * @author      hoang.ho
     * @date        2014/03/03
     * @param       entTblTmpCSV store delete condition
     * @return      number row affect
     ************************************************************************/
    int deleteFactoryProductFitModel(@Param("entMstProduct")EntTblTmpCSV entTblTmpCSV);

    /**
     * get total number product model error by product id.
     * @param productId productId
     * @author      hoang.ho
     * @date        2014/03/03
     * @return      number error records.
     */
    int getProductFitModelErrorState(@Param("productId")int productId);

    /**
     * insert multiple fit model.
     * @param fitModelList store information of fit model.
     * @param loginUserId loginUserId
     * @author      hoang.ho
     * @date        2014/03/03
     * @return      number error records.
     */
    int insertMultipleFitModel(@Param("fitModelList")List<EntTblTmpCSV> fitModelList, @Param("loginUserId")String loginUserId);
    /**
     * delete multiple fit model by products id.
     * @param deletingProductsStr store products id with format: 1,2,3,4,5
     * @author      hoang.ho
     * @date        2014/03/03
     * @return      number error records.
     */
    int deleteMultipleFitModelByProductsId(@Param("deletingProductsStr")String deletingProductsStr);

    /************************************************************************
     * <b>Description:</b><br>
     *  selectSyouhinFitModelBySyouhinSysCodeList.
     *
     * @author      Doan.Chuong
     * @date        Mar 12, 2014
     * @param       list  List<String>
     * @return      List<EntTblTmpCSV>
     ************************************************************************/
    List<EntTblTmpCSV> selectSyouhinFitModelBySyouhinSysCodeList2(List<String> list);

    /************************************************************************
     * <b>Description:</b><br>
     * Select list fit model by product id .
     *
     * @author		Luong.Dai
     * @date		Jun 13, 2014
     * @param 		entSearch		Ent has search params
     * @return		List<EntTblFactoryProductFitModel>	result
     ************************************************************************/
    List<EntTblFactoryProductFitModel> selectListFitModelWithMakerLogo(EntTblFactoryProductFitModel entSearch);

    /************************************************************************
     * <b>Description:</b><br>
     *  get list productId has fitModel duplicate with entity model.
     *  Duplicate is same modelMaker, modelModel and modelStyle.
     *
     * @author		Luong.Dai
     * @date		Aug 22, 2014
     * @param 		lstProduct		List product to check
     * @param 		model			Entity fit model to check
     * @return		List<Long>		List productId is duplicate
     * @throws 		SQLException	Exception when excute query
     ************************************************************************/
    List<Long> selectListProductIdByFitModelData(@Param("lstProduct")List<EntTblFactoryProductNew> lstProduct
    												, @Param("model") EntTblFactoryProductFitModel model) throws SQLException;

    /************************************************************************
     * <b>Description:</b><br>
     *  Insert multi fit model to DB.
     *
     * @author		Luong.Dai
     * @date		Aug 22, 2014
     * @param 		lstProduct		List product has fit model
     * @param 		model			entity model to insert
     * @param 		updateUserId	user insert data
     * @return		Integer			insert result: number row effect
     * @throws 		SQLException	exception when excute
     ************************************************************************/
    Integer insertMultiFitModel(@Param("lstProduct")List<EntTblFactoryProductNew> lstProduct
								, @Param("model") EntTblFactoryProductFitModel model
								, @Param("updateUserId")String updateUserId) throws SQLException;
}
