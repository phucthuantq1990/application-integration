/************************************************************************
 * File Name	： MstNoukiMapper.java
 * Author		： Le.Dinh
 * Version		： 1.0.0
 * Date Created	： 2014/02/12
 * Date Updated	： 2014/02/12
 * Description	： Contain method to get data from database .
 *
 ***********************************************************************/
package net.webike.RcProductFactory.mapper;

import java.sql.SQLException;
import java.util.List;

import net.webike.RcProductFactory.entity.EntMstNouki;

/**
 * Interface MstNoukiMapper.
 */
public interface MstNoukiMapper {


    /************************************************************************
     * <b>Description:</b><br>
     *  selectListAllNouki.
     *
     * @author		Le.Dinh
     * @date		2014/02/12
     * @return		List<EntMstSiire>
     * @throws		SQLException	SQLException
     ************************************************************************/
    List<EntMstNouki> selectListAllNouki() throws SQLException;

    /**
     * Get nouki list.
     * @author		nguyen.hieu
     * @since		2014/02/17
     * @return		Nouki list.
     * @throws SQLException SQLException.
     */
    List<EntMstNouki> selectNoukiList() throws SQLException;
}
