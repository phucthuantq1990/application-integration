/************************************************************************
 * File Name	： MstBunruikaisouMapper.java
 * Author		： Tran.Thanh
 * Version		： 1.0.0
 * Date Created	： 2013/10/23
 * Date Updated	： 2013/10/23
 * Description	： Contain method to get data from database .
 *
 ***********************************************************************/
package net.webike.RcProductFactory.mapper;

import java.util.List;

import net.webike.RcProductFactory.entity.EntCategory;
import net.webike.RcProductFactory.entity.EntMstBunrui;
import net.webike.RcProductFactory.entity.EntMstBunruikaisou;

/**
 * Interface MstFactoryUserMapper.
 */
public interface MstBunruiMapper {


    /************************************************************************
     * <b>Description:</b><br>
     *  get children category by parent category.
     *
     * @author		Tran.Thanh
     * @date		23 Oct 2013
     * @param       entBunruikaisou entBunruikaisou.
     * @return		List<EntMstBunruikaisou>
     ************************************************************************/
    List<EntMstBunrui> selectListBunruiByOyaCode(EntMstBunruikaisou entBunruikaisou);

    /************************************************************************
     * <b>Description:</b><br>
     *  Select Category Tree View.
     *
     * @author		Tran.Thanh
     * @date		25 Oct 2013
     * @return		EntCategory
     ************************************************************************/
    List<EntCategory> selectCategory();
}
