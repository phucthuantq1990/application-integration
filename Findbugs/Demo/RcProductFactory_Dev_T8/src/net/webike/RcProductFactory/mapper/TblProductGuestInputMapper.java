/************************************************************************
 * File Name    ： TblProductGuestInputMapper.java
 * Author       ：Nguyen.Chuong
 * Version      ： 1.0.0
 * Date Created ： 2014/02/19
 * Date Updated ： 2014/02/19
 * Description  ： TblProductGuestInputMapper to query with rc_syouhin.tbl_product_guest_input.
 ***********************************************************************/
package net.webike.RcProductFactory.mapper;

import java.util.List;

import net.webike.RcProductFactory.entity.EntTblProductGuestInput;

/**
 * TblProductGuestInputMapper.
 */
public interface TblProductGuestInputMapper {
    /************************************************************************
     * <b>Description:</b><br>
     *  Select list productGuestInput by syouhin_sys_code.
     *
     * @author		Luong.Dai
     * @date		Feb 19, 2014
     * @param       syouhinSysCode Long
     * @return		List<EntTblProductGuestInput>
     ************************************************************************/
    List<EntTblProductGuestInput> selectListProductGuestInputBySyouhinSysCode(Long syouhinSysCode);
}
