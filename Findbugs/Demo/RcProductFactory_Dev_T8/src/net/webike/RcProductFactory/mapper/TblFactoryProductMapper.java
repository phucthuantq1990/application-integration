/************************************************************************
 * File Name    ： TblProductAttributeMapper.java
 * Author       ： Nguyen.Chuong
 * File Name	： TblFactoryProductMapper.java
 * Author		： Nguyen.Chuong
 * Version		： 1.0.0
 * Date Created	： 2014/02/15
 * Date Updated	： 2014/02/15
 * Description	： Contain method to get data from rc_product_factory.tbl_factory_product database .
 *
 ***********************************************************************/
package net.webike.RcProductFactory.mapper;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import net.webike.RcProductFactory.entity.EntCSVMode1;
import net.webike.RcProductFactory.entity.EntCSVMode3;
import net.webike.RcProductFactory.entity.EntCSVProductFitModel;
import net.webike.RcProductFactory.entity.EntMstFactoryMatter;
import net.webike.RcProductFactory.entity.EntMstFactoryProductStatus;
import net.webike.RcProductFactory.entity.EntMstProduct;
import net.webike.RcProductFactory.entity.EntProduct;
import net.webike.RcProductFactory.entity.EntTblFactoryProductGeneral;
import net.webike.RcProductFactory.entity.EntTblFactoryProductImage;
import net.webike.RcProductFactory.entity.EntTblFactoryProductModel;
import net.webike.RcProductFactory.entity.EntTblFactoryProductNew;

import org.apache.ibatis.annotations.Param;

/**
 * Interface TblFactoryProductMapper.
 */
public interface TblFactoryProductMapper {


    /************************************************************************
     * <b>Description:</b><br>
     *  Select all product by matter_no.
     *
     * @author		Nguyen.Chuong
     * @date		Oct 29, 2013
     * @return		List<EntProduct>
     * @param       entProduct EntProduct
     * @param       limit int limit
     * @param       offset int offset
     ************************************************************************/
    List<EntProduct> selectProductListByMatterNo(@Param("entProduct")EntProduct entProduct
                                              , @Param("limit")int limit
                                              , @Param("offset")int offset);

    /************************************************************************
     * <b>Description:</b><br>
     *  select product model(maker, model, style) by product_code.
     *
     * @author		Nguyen.Chuong
     * @date		Oct 29, 2013
     * @return		List<EntTblFactoryProductModel>
     * @param       entProduct EntProduct
     ************************************************************************/
    List<EntTblFactoryProductModel> selectProductModelByProductCode(EntTblFactoryProductNew entProduct);

    /************************************************************************
     * <b>Description:</b><br>
     *  select all image and thumb by product_code.
     *
     * @author		Nguyen.Chuong
     * @date		Oct 29, 2013
     * @return		List<EntTblFactoryProductImage>
     * @param       entProduct EntProduct
     ************************************************************************/
    List<EntTblFactoryProductImage> selectListImageAndThumbByProductId(EntTblFactoryProductNew entProduct);

    /************************************************************************
     * <b>Description:</b><br>
     *  Select all value of mst_factory_product_status table.
     *
     * @author		Nguyen.Chuong
     * @date		Oct 31, 2013
     * @return		List<EntMstFactoryProductStatus>
     ************************************************************************/
    List<EntMstFactoryProductStatus> selectAllProductStatus();

    /************************************************************************
     * <b>Description:</b><br>
     * count number product of matter, count number of checked product, count error product.
     *
     * @author	Nguyen.Chuong
     * @date	Nov 4, 2013
     * @param   entProduct : entProduct contain matterNo
     *                                          , sysProductCode
     *                                          , brandCode
     *                                          , manufactoryCode
     *                                          , bunruiCode
     *                                          , productStatusCode
     * @param   checkedStatusCode : String Constant of checked status code.
     * @return	EntMstFactoryMatter
     ************************************************************************/
    EntMstFactoryMatter countProductOfMatter(@Param("entProduct")EntProduct entProduct,
                                             @Param("checkedStatusCode")String checkedStatusCode);

    /************************************************************************
     * <b>Description:</b><br>
     * Update bunrui_code.
     *
     * @author  Luong.Dai
     * @date    Nov 6, 2013
     * @param   entProduct : EntProduct
     ************************************************************************/
    void updateBunruiCode(EntProduct entProduct);

    /************************************************************************
     * <b>Description:</b><br>
     * Update product_status_code.
     *
     * @author  Luong.Dai
     * @date    Nov 11, 2013
     * @param   entProduct : EntProduct
     ************************************************************************/
    void updateProductStatusCode(EntProduct entProduct);

    /* BOE add new mapper by Luong.Dai 2014/01/18 #B07 */
    /************************************************************************
     * <b>Description:</b><br>
     *  get list product by matterCode.
     *
     * @author		Luong.Dai
     * @date		Jan 18, 2014
     * @param product EntTblFactoryProductNew
     * @return		List<EntTblFactoryProductNew>
     ************************************************************************/
    List<EntTblFactoryProductNew> selectListProductByMatterCode(EntTblFactoryProductNew product);

    //BOE #SiuNhan Tran.Thanh 2014/06/04 : Add query to get list product no limited by entity filter
    /************************************************************************
     * <b>Description:</b><br>
     *  get list product no limited by entity product filter.
     *
     * @author		Tran.Thanh
     * @date		July 04, 2014
     * @param 		product EntTblFactoryProductNew
     * @return		List<EntTblFactoryProductNew>
     ************************************************************************/
    List<EntTblFactoryProductNew> selectListProductByProductFilterNoLimited(EntTblFactoryProductNew product);
    //EOE #SiuNhan Tran.Thanh 2014/06/04 : Add query to get list product no limited by entity filter

    /************************************************************************
     * <b>Description:</b><br>
     *  select total record of product for pagging.
     *
     * @author		Luong.Dai
     * @date		Jan 20, 2014
     * @param product EntTblFactoryProductNew
     * @return		Integer
     ************************************************************************/
    Integer selectTotalRecordOfFilterProduct(EntTblFactoryProductNew product);

    /************************************************************************
     * <b>Description:</b><br>
     *  select product updated_on and updated_user_id.
     *
     * @author		Luong.Dai
     * @date		Jan 21, 2014
     * @param       productId Long
     * @return		EntTblFactoryProductNew
     ************************************************************************/
    EntTblFactoryProductNew selectProductUpdatedOnByProductCode(Long productId);
    /************************************************************************
     * <b>Description:</b><br>
     *  select data for bottom grid of page ProductFitModel.
     *
     * @author      Thai.Son
     * @date        Jan 23, 2014
     * * @param  limit int
     * * @param  offset int
     * @param  entTblFactoryProductNew EntTblFactoryProductNew
     * @return      List<EntTblFactoryProductNew>
     ************************************************************************/
    List<EntTblFactoryProductNew> selectBottomGridByMatterNoAndFilterForFitModelCheck(@Param("limit")int limit
                                                                            , @Param("offset")int offset
                                                                            , @Param("entTblFactoryProductNew")EntTblFactoryProductNew entTblFactoryProductNew);
    /************************************************************************
     * <b>Description:</b><br>
     *  count record when get data from DB.
     *
     * @author      Thai.Son
     * @date        Jan 16, 2014
     * @param       entTblFactoryProductNew entity contain filter
     * @return      int
     ************************************************************************/
    int selectTotalRecordOfFilterBottomGrid(@Param("entTblFactoryProductNew") EntTblFactoryProductNew entTblFactoryProductNew);
    /************************************************************************
     * <b>Description:</b><br>
     *  Delete product_general by productId.
     *
     * @author		Luong.Dai
     * @date		Jan 21, 2014
     * @param       productId Long
     * @return		Integer
     ************************************************************************/
    Integer deleteProductsGeneral(Long productId);
    /* EOE add new mapper by Luong.Dai */
    /************************************************************************
     * <b>Description:</b><br>
     *  Delete product by productId list.
     *
     * @author		Le.Dinh
     * @date		20 Jan 2014
     * @param 		productIds	Integer
     * @return		Integer
     * @throws		SQLException	SQLException
     ************************************************************************/
    Integer deleteProductList(List<Long> productIds) throws SQLException;
    /************************************************************************
     * <b>Description:</b><br>
     *  Delete product_general by productId list.
     *
     * @author      Luong.Dai
     * @date        Jan 21, 2014
     * @param       productIds Long
     * @return      Integer
     * @throws      SQLException    SQLException
     ************************************************************************/
    Integer deleteProductGeneralList(List<Long> productIds) throws SQLException;
    /* EOE add new mapper by Luong.Dai */
    /************************************************************************
     * <b>Description:</b><br>
     *  deleteProducts.
     *
     * @author      Le.Dinh
     * @date        20 Jan 2014
     * @param       productId   Integer
     * @return      Integer
     * @throws      SQLException    SQLException
     ************************************************************************/
    Integer deleteProducts(@Param("productId")Long productId) throws SQLException;

    /************************************************************************
     * <b>Description:</b><br>
     * select list product by product list id to get model error flg.
     * @author      Long Vu
     * @date        Jan 24, 2014
     * @param       pl product id list
     * @return      List<EntTblFactoryProductModel>
     ************************************************************************/
    List<EntTblFactoryProductNew> selectProductListByProductIdList(List<Long> pl);

    /************************************************************************
     * <b>Description:</b><br>
     *  count product has error flg by list product id.
     *
     * @author		Tran.Thanh
     * @date		May 20, 2014
     * @param 		productIdList list product id.
     * @return		Integer
     ************************************************************************/
    EntTblFactoryProductNew selectTotalErrorFlgProduct(@Param("productIdList") List<String> productIdList);

    /************************************************************************
     * <b>Description:</b><br>
     * update product model error flg.
     * @author      Long Vu
     * @date        Jan 24, 2014
     * @param       product product
     * @return      List<EntTblFactoryProductModel>
     ************************************************************************/
    int updateProductModelErrorFlg(EntTblFactoryProductNew product);
    
    /************************************************************************
     * <b>Description:</b><br>
     * update product model error flg.
     * @author      Nguyen.Chuong
     * @date        2014/08/19
     * @param       listProduct List<EntTblFactoryProductNew>
     * @param		modelErrorFlg int
     * @param		updatedUserId string
     * @return      round update.
     ************************************************************************/
    int updateMultiProductModelErrorFlg(@Param("listProduct")List<EntTblFactoryProductNew> listProduct
    								  , @Param("modelErrorFlg") int modelErrorFlg
    								  , @Param("updatedUserId") String updatedUserId);

    /************************************************************************
     * <b>Description:</b><br>
     *  Select EntProductNew by productId at tbl_factory_product.
     *
     * @author		Nguyen.Chuong
     * @date		Jan 27, 2014
     * @param       productId productId
     * @return		EntTblFactoryProductNew
     ************************************************************************/
    EntTblFactoryProductNew selectEntProductByProductId(Long productId);

    /************************************************************************
     * <b>Description:</b><br>
     *  Select product info by Syouhin_sys_code at tbl_factory_product.
     *
     * @author      hoang.ho
     * @date        2014/03/03
     * @param       syouhinSysCode Long
     * @return      EntTblFactoryProductNew
     ************************************************************************/
    EntTblFactoryProductNew selectEntProductBySyouhinSysCode(Long syouhinSysCode);

    /************************************************************************
     * <b>Description:</b><br>
     * Update product model check flg when every data is update ok.
     *
     * @author      Long Vu
     * @date        Feb 11, 2014
     * @param       productId productId
     * @return      int
     ************************************************************************/
    int updateProductModelCheckFlg(Long productId);

    /************************************************************************
     * <b>Description:</b><br>
     *  select list all product for view in productEdit.
     *
     * @author      Luong.Dai
     * @date        Feb 14, 2014
     * @param       product EntTblFactoryProductNew
     * @return      List<EntTblFactoryProductNew>
     ************************************************************************/
    List<EntTblFactoryProductNew> selectAllListProduct(EntTblFactoryProductNew product);

    /************************************************************************
     * <b>Description:</b><br>
     *  count total product for pagging in productEdit.
     *
     * @author		Luong.Dai
     * @date		Feb 15, 2014
     * @param       product EntTblFactoryProductNew
     * @return		Integer
     ************************************************************************/
    Integer selectCountListProduct(EntTblFactoryProductNew product);

    /************************************************************************
     * <b>Description:</b><br>
     *  select product Matter No by Product_id in table rc_product_factory.tbl_factory_product.
     *
     * @author		Tran.Thanh
     * @date		15 Feb 2014
     * @param 		productId productId
     * @return		EntTblFactoryProductNew
     ************************************************************************/
    EntTblFactoryProductNew selectMatterNoByProductId(@Param("productId")Long productId);

    /************************************************************************
     * <b>Description:</b><br>
     *  check exists product in tbl_factory_product and tbl_factory_product_general.
     *
     * @author		Luong.Dai
     * @date		Feb 17, 2014
     * @param       productId Long
     * @return		EntTblFactoryProductNew
     ************************************************************************/
    EntTblFactoryProductNew selectProductFromProductAndProductGeneral(@Param("productId")Long productId);

    /************************************************************************
     * <b>Description:</b><br>
     *  select product_proper_price and supplier_price_price by productId.
     *
     * @author		Luong.Dai
     * @date		Feb 18, 2014
     * @param       productId Long
     * @return		EntTblFactoryProductNew
     ************************************************************************/
    EntTblFactoryProductNew selectProductProperAndSupplierPriceByProductId(Long productId);

    /************************************************************************
     * <b>Description:</b><br>
     *  Get siire marume code from product id.
     *  when product id has more tb_factory_product_supplier
     *      then choose by supplier_syouhin_siire_order = 1
     *
     * @author      hoang.ho
     * @date        Feb 26, 2014
     * @param       productId Long
     * @return      Integer siire_marume_code
     * @throws      SQLException Integer
     ************************************************************************/
    Integer selectSiireMarumeCodeByProductId(Long productId) throws SQLException;
    /************************************************************************
     * <b>Description:</b><br>
     *  Select list product of same group.
     *
     * @author		Luong.Dai
     * @date		Feb 19, 2014
     * @param       productGen EntTblFactoryProductGeneral
     * @return		List<EntTblFactoryProductNew>
     ************************************************************************/
    List<EntTblFactoryProductNew> selectProductOfSameGroup(EntTblFactoryProductGeneral productGen);
    /************************************************************************
     * <b>Description:</b><br>
     *  Select max product id.
     *
     * @author      Long Vu
     * @date        Feb 24, 2014
     * @return      long
     ************************************************************************/
    long selectMaxProductId();

    /************************************************************************
     * <b>Description:</b><br>
     * insert new factory product.
     *
     * @author      Long Vu
     * @param       matterNo matter id
     * @param       prd insert condition
     * @date        Feb 24, 2014
     * @return      int
     ************************************************************************/
    int insertNewFactoryProduct(@Param("matterNo")long matterNo, @Param("prd")EntMstProduct prd);

    /************************************************************************
     * <b>Description:</b><br>
     * insert multiple new factory product.
     *
     * @author      Doan Chuong
     * @param       matterNo matter id
     * @param       prdList data
     * @date        March 12, 2014
     * @return      int
     ************************************************************************/
    int insertMultipleNewFactoryProduct(@Param("matterNo")long matterNo, @Param("prdList")List<EntMstProduct> prdList);

    /************************************************************************
     * <b>Description:</b><br>
     *  update Product DateUpdatedOn By ProductId.
     *
     * @author		Tran.Thanh
     * @date		25 Feb 2014
     * @param entTblFactoryProductNew		void
     ************************************************************************/
    void updateProductDate(EntTblFactoryProductNew entTblFactoryProductNew);

    /************************************************************************
     * <b>Description:</b><br>
     *  Select List Product Error FLg By ProductId.
     *
     * @author		Tran.Thanh
     * @date		28 Feb 2014
     * @param 		productId productId
     * @return		List<EntTblFactoryProductNew>
     ************************************************************************/
    EntTblFactoryProductNew selectErrorFlgByProductId(@Param("productId")int productId);

    /************************************************************************
     * <b>Description:</b><br>
     *  update Product Error Flg.
     *
     * @author		Tran.Thanh
     * @date		28 Feb 2014
     * @param entTblFactoryProductNew		void
     ************************************************************************/
    void updateProductErrorFlg(EntTblFactoryProductNew entTblFactoryProductNew);

    /************************************************************************
     * <b>Description:</b><br>
     *  Select product and fit model check information with product id list.
     *
     * @author      hoang.ho
     * @date        2014/02/26
     * @param      entCSVProductFitModel  store all search conditions
     * @param       limit int limit
     * @param       offset int offset
     * @return      List<EntCSVProductFitModel>
     ************************************************************************/
    List<EntCSVProductFitModel> selectProductFitModelList(@Param("limit")int limit
                                                        , @Param("offset")int offset
                                                        , @Param("searchConditions")EntCSVProductFitModel entCSVProductFitModel);

    /************************************************************************
     * <b>Description:</b><br>
     *  Select count product and fit model check information with product id.
     *
     * @author      hoang.ho
     * @date        2014/02/26
     * @param      entCSVProductFitModel  store all search conditions
     * @return      Integer total of records
     ************************************************************************/
    Integer selectProductFitModelCount(@Param("searchConditions")EntCSVProductFitModel entCSVProductFitModel);

    /************************************************************************
     * <b>Description:</b><br>
     *  Select products and all of its attribute.
     *
     * @author      Doan.Chuong
     * @date        Feb 26, 2014
     * @param       productIdList List<String>
     * @param       attributeCodeList List<String>
     * @return      List<EntCSVMode3>
     ************************************************************************/
    List<EntCSVMode3> selectProductAttributeByProductIds(@Param("productIdList") List<String> productIdList
                                                      ,  @Param("attributeCodeList") List<String> attributeCodeList);

    /************************************************************************
     * <b>Description:</b><br>
     *  Select products and all of its fit model.
     *
     * @author      Doan.Chuong
     * @date        Feb 26, 2014
     * @param       productIdList List<String>
     * @return      List<EntCSVMode3>
     ************************************************************************/
    List<EntCSVMode3> selectProductFitModelByProductIds(List<String> productIdList);

    /************************************************************************
     * <b>Description:</b><br>
     *  Select product's info for export CSV mode 1 (tables with 1-1 mapping).
     *
     * @author      Doan.Chuong
     * @date        Feb 27, 2014
     * @param       productIdList List<String>
     * @return      List<EntCSVMode1>
     ************************************************************************/
    List<EntCSVMode1> selectProductInfoExportCSVMode1SingleTable(List<String> productIdList);

    /************************************************************************
     * <b>Description:</b><br>
     *  Select product compatible model for export CSV Mode 1.
     *
     * @author      Doan.Chuong
     * @date        Feb 28, 2014
     * @param       productIdList List<String>
     * @return      List<EntCSVMode1>
     ************************************************************************/
    List<EntCSVMode1> selectProductCompatibleModelByProductIds(List<String> productIdList);

    /************************************************************************
     * <b>Description:</b><br>
     *  Select product image info for export CSV Mode 1.
     *
     * @author      Doan.Chuong
     * @date        Feb 28, 2014
     * @param       productIdList List<String>
     * @return      List<EntCSVMode1>
     ************************************************************************/
    List<EntCSVMode1> selectProductImageInfoByProductIds(List<String> productIdList);

    /************************************************************************
     * <b>Description:</b><br>
     *  Select product select info for export CSV Mode 1.
     *
     * @author      Doan.Chuong
     * @date        Feb 28, 2014
     * @param       productIdList List<String>
     * @return      List<EntCSVMode1>
     ************************************************************************/
    List<EntCSVMode1> selectProductSelectInfoByProductIds(List<String> productIdList);

    /************************************************************************
     * <b>Description:</b><br>
     *  Select product guest input for export CSV Mode 1.
     *
     * @author      Doan.Chuong
     * @date        Feb 28, 2014
     * @param       productIdList List<String>
     * @return      List<EntCSVMode1>
     ************************************************************************/
    List<EntCSVMode1> selectProductGuestInputInfoByProductIds(List<String> productIdList);

    /************************************************************************
     * <b>Description:</b><br>
     *  Select product link info for export CSV Mode 1.
     *
     * @author      Doan.Chuong
     * @date        Feb 28, 2014
     * @param       productIdList List<String>
     * @return      List<EntCSVMode1>
     ************************************************************************/
    List<EntCSVMode1> selectProductLinkInfoByProductIds(List<String> productIdList);

    /************************************************************************
     * <b>Description:</b><br>
     *  Select product video info for export CSV Mode 1.
     *
     * @author      Doan.Chuong
     * @date        Feb 28, 2014
     * @param       productIdList List<String>
     * @return      List<EntCSVMode1>
     ************************************************************************/
    List<EntCSVMode1> selectProductVideoInfoByProductIds(List<String> productIdList);

    /************************************************************************
     * <b>Description:</b><br>
     *  Update product group code by productId.
     *
     * @author		Luong.Dai
     * @date		Mar 3, 2014
     * @param       product EntTblFactoryProductGeneral
     * @return		Integer
     ************************************************************************/
    Integer updateProductGroupCodeByProductId(EntTblFactoryProductGeneral product);

    /************************************************************************
     * <b>Description:</b><br>
     *  Update product_syouhin_sys_code by product_id.
     *
     * @author		Luong.Dai
     * @date		Mar 3, 2014
     * @param       product EntTblFactoryProductNew
     * @return		Integer
     ************************************************************************/
    Integer updateSyouhinSysCodeProductId(EntTblFactoryProductNew product);

    /**
     * update product error flag by product id and check flag information.
     * @param entMstProduct EntTblFactoryProductNew
     * @author      hoang.ho
     * @date        2014/03/03
     */
    void updateProductCheckFlag(@Param("entMstProduct")EntTblFactoryProductNew entMstProduct);
    /**
     * select all product by matter no.
     * @param       matterNo matterNo
     * @author      hoang.ho
     * @date        2014/03/04
     * @return      EntTblFactoryProductNew list
     */
    List<EntTblFactoryProductNew> selectEntProductByMatterNo(@Param("matterNo")long matterNo);

    /************************************************************************
     * <b>Description:</b><br>
     *  Insert new product with syouhin_sys_code, matter_no.
     *
     * @author      Luong.Dai
     * @date        Mar 4, 2014
     * @param       product EntTblFactoryProductNew
     * @return      Integer
     ************************************************************************/
    Integer insertNewProductWithSyouhinSysCodeAndMatterNo(EntTblFactoryProductNew product);

    /************************************************************************
     * <b>Description:</b><br>
     *  Select updatedOn of product.
     *
     * @author      Luong.Dai
     * @date        Mar 4, 2014
     * @param       productId   Long
     * @return      Date
     ************************************************************************/
    Date selectUpdatedOnOfProduct(Long productId);

    /**
     * Select product info list for validation.
     * @param		product EntTblFactoryProductNew.
     * @return		List<EntCSVMode1>
     * @throws		SQLException SQLExcepion.
     */
    List<EntCSVMode1> selectProductInfoListForValidation(EntTblFactoryProductNew product) throws SQLException;

    /**
     * Select product image list for validation.
     * @param		product EntTblFactoryProductNew
     * @return		List<EntCSVMode1>
     * @throws		SQLException SQLExcepion.
     */
    List<EntCSVMode1> selectProductImageListForValidation(EntTblFactoryProductNew product) throws SQLException;

    /**
     * Select product select list for validation.
     * @param		product EntTblFactoryProductNe
     * @return		List<EntCSVMode1>
     * @throws		SQLException SQLExcepion.
     */
    List<EntCSVMode1> selectProductSelectListForValidation(EntTblFactoryProductNew product) throws SQLException;

    /**
     * Update product general error flag.
     * @param product Input param.
     * @throws SQLException SQLException.
     */
    void updateProductGeneralErrorFlg(@Param("product")EntTblFactoryProductNew product) throws SQLException;

    /**
     * Update product category error flag.
     * @param product Input param.
     * @throws SQLException SQLException.
     */
    void updateProductCategoryErrorFlg(@Param("product")EntTblFactoryProductNew product) throws SQLException;

    /************************************************************************
     * <b>Description:</b><br>
     *  Count number product of matter by matterNo.
     *
     * @author		Luong.Dai
     * @date		Mar 10, 2014
     * @param       matterNo        Long
     * @return      Integer         Number product of matter
     * @throws      SQLException	Exception
     ************************************************************************/
    Integer selectCountNumberProductOfMatter(@Param("matterNo")Long matterNo) throws SQLException;

    /**
     * Select product ID list by matter no.
     * @author		nguyen.hieu
     * @date		2014-03-10
     * @param		product EntTblFactoryProductNew.
     * @return		List<String>
     * @throws		SQLException SQLExcepion.
     */
    List<String> selectProductIdListByFilterForExportCSV(EntTblFactoryProductNew product) throws SQLException;

    /************************************************************************
     * <b>Description:</b><br>
     * get product object by product id.
     *
     * @param       productId productId
     * @author      Long Vu
     * @return      EntTblFactoryProductNew
     * @date        Mar 11, 2014
     ************************************************************************/
    EntTblFactoryProductNew selectProductByProductId(long productId);

    /**
     * Select productSyouhinSysCode from productIdList.
     * @author nguyen.hieu
     * @date 2014-03-12
     * @param productIdList Product ID list.
     * @return List<Integer>
     * @throws SQLException SQLException.
     */
    List<Long> selectProductSyouhinSysCodeFromProductIdList(@Param("productIdList")List<Long> productIdList) throws SQLException;

    /**
     * Select productSyouhinSysCode from productMatterNo.
     * @author nguyen.hieu
     * @date 2014-03-12
     * @param productMatterNo Product matter no.
     * @return List<Integer>
     * @throws SQLException SQLException.
     */
    List<Long> selectProductSyouhinSysCodeFromProductMatterNo(long productMatterNo) throws SQLException;

    /************************************************************************
     * <b>Description:</b><br>
     *  Update productGeneralCount of product.
     *
     * @author		Luong.Dai
     * @date		Mar 14, 2014
     * @param       product     EntTblFactoryProductNew
     * @return      Integer
     * @throws      SQLException Exception
     ************************************************************************/
    Integer updateProductGeneralCountError(EntTblFactoryProductNew product) throws SQLException;

    /************************************************************************
     * <b>Description:</b><br>
     *  insert duplicate data to DB.
     *
     * @author		Luong.Dai
     * @date		Mar 25, 2014
     * @param 		product			EntTblFactoryProductNew
     * @return		Integer			Result
     * @throws 		SQLException	Exception
     ************************************************************************/
    Integer duplicateNewProduct(EntTblFactoryProductNew product) throws SQLException;

    /************************************************************************
     * <b>Description:</b><br>
     *  Update all error flag of product.
     *
     * @author		Luong.Dai
     * @date		Apr 7, 2014
     * @param 		product		Product need to update
     * @return		Integer		update result
     ************************************************************************/
    Integer updateAllErrorFlgOfProduct(EntTblFactoryProductNew product);

    /************************************************************************
     * <b>Description:</b><br>
     *  select List ProductId By Search Condion Block 15.
     *
     * @author		Tran.Thanh
     * @date		Apr 21, 2014
     * @param 		product EntyProductSearch
     * @return		List<EntTblFactoryProductNew>
     ************************************************************************/
    List<EntTblFactoryProductNew> selectListProductIdFilter(EntTblFactoryProductNew product);

    /************************************************************************
     * <b>Description:</b><br>
     *  Count number release product of matter with filter.
     *
     * @author		Luong.Dai
     * @date		May 12, 2014
     * @param 		product		Product with search params
     * @return		Integer		Number release product
     ************************************************************************/
    Integer countReleaseProductWithFilter(EntTblFactoryProductNew product);

    /************************************************************************
     * <b>Description:</b><br>
     *  select all product not release with filter.
     *
     * @author		Luong.Dai
     * @date		May 12, 2014
     * @param 		product			Product with search filter
     * @return		List<Long>	List product id
     ************************************************************************/
    List<Long> selectAllProductNotReleasedWithFilter(EntTblFactoryProductNew product);

    /************************************************************************
     * <b>Description:</b><br>
     *  Delete list product from list productId get from CSV mode 1.
     *
     * @author		Luong.Dai
     * @date		May 19, 2014
     * @param 		listProducts		list product to delete
     * @return		int
     ************************************************************************/
    int deleteProductListCSVMode1(List<EntCSVMode1> listProducts);

    /************************************************************************
     * <b>Description:</b><br>
     *  Insert list product to DB.
     *
     * @author		Luong.Dai
     * @date		May 19, 2014
     * @param 		lstProduct		List data of product
     * @return		int				insert result
     ************************************************************************/
    int insertListNewProductWithSyouhinSysCodeAndMatterNo(@Param("lstProduct")List<EntTblFactoryProductNew> lstProduct);

    /************************************************************************
     * <b>Description:</b><br>
     *  Count total error of list product.
     *
     * @author		Luong.Dai
     * @date		Jun 3, 2014
     * @param 		lstProductId 				List product id
     * @return		EntTblFactoryProductNew		result
     ************************************************************************/
    EntTblFactoryProductNew selectTotalErrorFlgProductByLstProductId(@Param("lstProductId") List<Long> lstProductId);
    /************************************************************************
     * <b>Description:</b><br>
     *  count total product in matter.
     *
     * @author      Tran.Thanh
     * @date        Jun 13, 2014
     * @param       matterNo matter numer
     * @return      int total product in matter
     ************************************************************************/
    int countTotalProductInMatter(@Param("matterNo")Long matterNo);

    /************************************************************************
     * <b>Description:</b><br>
     *  index of Product In Matter.
     *
     * @author      Tran.Thanh
     * @date        Jun 13, 2014
     * @param       productId product id
     * @param       matterNo Long
     * @return      int
     ************************************************************************/
    int indexProductInMatter(@Param("productId")Long productId, @Param("matterNo")Long matterNo);

    /************************************************************************
     * <b>Description:</b><br>
     *  select Long MatterNo By ProductId.
     *
     * @author      Tran.Thanh
     * @date        Jun 13, 2014
     * @param       productId productId
     * @return      Long
     ************************************************************************/
    Long selectLongMatterNoByProductId(@Param("productId")Long productId);

    /************************************************************************
     * <b>Description:</b><br>
     *  Select list product of group.
     *
     * @author		Luong.Dai
     * @date		Jun 14, 2014
     * @param 		product			Search param
     * @return		List<Long>		List productId
     ************************************************************************/
    List<Long> selectProductIdGroup(EntTblFactoryProductGeneral product);

    /************************************************************************
     * <b>Description:</b><br>
     *  select Long group code By ProductId.
     *
     * @author		Luong.Dai
     * @date		Jun 14, 2014
     * @param 		productId		productId
     * @return		Long			Group code
     ************************************************************************/
    Long selectGroupCodeOfProductByProductId(Long productId);
    /************************************************************************
     * <b>Description:</b><br>
     *  Select one next/previous productId in matter.<br>
     *  - If load next productId => orderBy param = ASC<br>
     *  - If load previous productId => orderBy param = DESC<br>
     * @author      Nguyen.Chuong
     * @date        Jun 13, 2014
     * @param       productId Long
     * @param       matterNo Long
     * @param       orderBy String
     * @return      long
     ************************************************************************/
    Long selectOneNextOrPreProductInMatter(@Param("productId")Long productId
                                         , @Param("matterNo")Long matterNo
                                         , @Param("orderBy")String orderBy);

    /************************************************************************
     * <b>Description:</b><br>
     *  select one next productId in matter with group code.<br>
     *  - If select productId have groupCode > 0 then param changeModeInGroup = true<br>
     *  - If select productId have groupCode = 0 then param changeModeInGroup = false<br>
     *  - If load next productId => orderBy param = ASC<br>
     *  - If load previous productId => orderBy param = DESC<br>
     * @author      Nguyen.Chuong
     * @date        Jun 13, 2014
     * @param       productId Long
     * @param       matterNo Long
     * @param       changeModeInGroup boolean
     * @param       orderBy String
     * @return      long
     ************************************************************************/
    Long selectOneNextOrPreProductByGroup(@Param("productId")Long productId
                                        , @Param("matterNo")Long matterNo
                                        , @Param("changeModeInGroup")boolean changeModeInGroup
                                        , @Param("orderBy")String orderBy);

    /************************************************************************
     * <b>Description:</b><br>
     *  select list product not release by list product id.
     *
     * @author		Luong.Dai
     * @date		Jun 14, 2014
     * @param 		lstProductid		list product Id
     * @return		List<Long>			list product not release
     ************************************************************************/
    List<Long> selectProductNotReleaseByListProductId(@Param("lstProductid")List<Long> lstProductid);

    /************************************************************************
     * <b>Description:</b><br>
     *  select status check when preview product
     *
     * @author      Dau.Phuong
     * @date        July 17, 2014
     * @param       entTblFactoryProductNew           entTblFactoryProductNew
     * @return      EntTblFactoryProductNew
     ************************************************************************/
    EntTblFactoryProductNew selectStatusCheckPreviewProduct(@Param("entTblFactoryProductNew")EntTblFactoryProductNew entTblFactoryProductNew);

    /************************************************************************
     * <b>Description:</b><br>
     *  update status check when preview product
     *
     * @author      Dau.Phuong
     * @date        July 17, 2014
     * @param       entTblFactoryProductNew           entTblFactoryProductNew
     ************************************************************************/
    void updateStatusPopUpCheck(@Param("entTblFactoryProductNew")EntTblFactoryProductNew entTblFactoryProductNew);

    /************************************************************************
     * <b>Description:</b><br>
     *  check user exist (request user)
     *
     * @author      Dau.Phuong
     * @date        July 17, 2014
     * @param       productId           Product id
     * @return      EntTblFactoryProductNew
     ************************************************************************/
    EntTblFactoryProductNew checkExistedRequestUser(Long productId);
    // BOE @rcv!Luong.Tuong 2014/07/15 #9955 :  count total product in matter using for validate duplicate
    /************************************************************************
     * <b>Description:</b><br>
     *  count total product in matter using for validate duplicate  .
     *
     * @author      Luong.Luong
     * @date        Jul 15, 2014
     * @param       matterNo matter number
     * @param       productCode  String
     * @param       productEanCode  String
     * @return      int total product in matter using for validate duplicate 
     ************************************************************************/
    int countCheckInvalidDataInMatter(@Param("matterNo")Long matterNo
                                     , @Param("productCode")String productCode
                                     , @Param("productEanCode")String productEanCode);
    // EOE @rcv!Luong.Tuong 2014/07/15 #9955 :  count total product in matter using for validate duplicate

    /************************************************************************
     * <b>Description:</b><br>
     *  count number product check 1, check 2 and total product of matter.
     *
     * @author		Luong.Dai
     * @date		Jul 30, 2014
     * @param 		matterNo	MatterNo
     * @return		EntTblFactoryProductNew ent result
     * @throws 		SQLException			Exception when query		
     ************************************************************************/
    EntTblFactoryProductNew countTotalProductCheckInMatter(@Param("matterNo")Long matterNo) throws SQLException;

    /************************************************************************
     * <b>Description:</b><br>
     *  select matter name, change user id and release infor of product.
     *
     * @author		Luong.Dai
     * @date		Aug 6, 2014
     * @param 		syouhinSysCode					syouhin sys code
     * @return		List<EntTblFactoryProductNew>	List result
     * @throws 		SQLException					Error when excute query
     ************************************************************************/
    List<EntTblFactoryProductNew> selectMatterDetailByProductSyouhinSysCode(@Param("syouhinSysCode")Long syouhinSysCode) throws SQLException;

    /************************************************************************
     * <b>Description:</b><br>
     *  update productModelErrorFlg of list product by list productId.
     *
     * @author		Luong.Dai
     * @date		Aug 22, 2014
     * @param 		lstProduct		List product Id
     * @param 		flag			New productModelErrorFlg
     * @param 		user			Update user ID
     * @return		Integer			Update result: number row effect
     * @throws 		SQLException	Exception when excute query
     ************************************************************************/
    Integer updateMultiProductModelErrorFlgByListId(@Param("lstProduct")List<Long> lstProduct
    										, @Param("productModelErrorFlg")Integer flag
    										, @Param("updatedUserId")String user) throws SQLException;
}
