/************************************************************************
 * File Name	： MstBrandMapper.java
 * Author		： Nguyen.Chuong
 * Version		： 1.0.0
 * Date Created	： 2013/12/27
 * Date Updated	： 2013/12/27
 * Description	： Contain method to get data from database .
 *
 ***********************************************************************/
package net.webike.RcProductFactory.mapper;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import net.webike.RcProductFactory.entity.EntMstBrand;

import org.apache.ibatis.annotations.Param;

/**
 * Interface MstBrandMapper.
 */
public interface MstBrandMapper {

    /************************************************************************
     * <b>Description:</b><br>
     *  Select all brand from mst_brand.
     *
     * @author		Nguyen.Chuong
     * @date		Oct 29, 2013
     * @return		List<EntProduct>
     * @param       limit int limit get from db
     * @param       offset int start row select from db
     * @param       entMstBrand content filter value
     ************************************************************************/
    List<EntMstBrand> selectListBrand(@Param("limit")int limit
                                    , @Param("offset")int offset
                                    , @Param("entMstBrand")EntMstBrand entMstBrand);

    /************************************************************************
     * <b>Description:</b><br>
     *  Count total record of filter.
     *
     * @author		Luong.Dai
     * @date		Dec 30, 2013
     * @param entMstBrand content filter value
     * @return		int: total record
     ************************************************************************/
    int selectTotalRecordOfFilter(EntMstBrand entMstBrand);

    /************************************************************************
     * <b>Description:</b><br>
     *  select brand by brand_code.
     *
     * @author		Tran.Thanh
     * @date		30 Dec 2013
     * @param 		entMstBrand entMstBrand.
     * @return		EntMstBrand
     ************************************************************************/
    EntMstBrand selectBrandByBrandCode(EntMstBrand entMstBrand);

    /************************************************************************
     * <b>Description:</b><br>
     *  update brand info.
     *
     * @author		Tran.Thanh
     * @date		30 Dec 2013
     * @param entMstBrand		void
     ************************************************************************/
    void updateBrand(EntMstBrand entMstBrand);

    /************************************************************************
     * <b>Description:</b><br>
     *  insert new Brand.
     *
     * @author		Tran.Thanh
     * @date		30 Dec 2013
     * @param 		entMstBrand entity brand to insert.
     * @param		map Map<String, Object>
     * @return		int
     ************************************************************************/
    int insertNewBrand(@Param("entMstBrand") EntMstBrand entMstBrand, @Param("map") Map<String, Object> map);

    /************************************************************************
     * <b>Description:</b><br>
     *  update brand logo by brand_code.
     *
     * @author		Tran.Thanh
     * @date		3 Jan 2014
     * @param 		brandCode	String
     * @param 		logo	String
     ************************************************************************/
    void updateBrandLogo(@Param("brandCode") String brandCode, @Param("logo") String logo);

    /**
     * Select brand list.
     * @return
     * @since 2014-02-20
     * @return Brand list.
     */
    List<EntMstBrand> selectBrandList();

    /************************************************************************
     * <b>Description:</b><br>
     * select brand by brand name and brand code.
     * @param brand store brand name and brand code.
     * @author      hoang.ho
     * @since        2014/02/18
     * @return      String
     ************************************************************************/
    EntMstBrand selectBrandByNameAndCode(EntMstBrand brand);

    /************************************************************************
     * <b>Description:</b><br>
     *  select brand by integer brand code.
     *
     * @author      Doan.Chuong
     * @date        2014/03/04
     * @param       brandCode int.
     * @return      EntMstBrand
     ************************************************************************/
    EntMstBrand selectMstBrandByBrandCode(int brandCode);

    /**
     * Select all brand list.
     * @return
     * @since 2014-03-05
     * @return Brand list.
     */
    List<EntMstBrand> selectAllBrandList();

    /**
     * Select undeleted brand by brandCode.
     * @author nguyen.hieu
     * @date 2014-03-14
     * @param entMstBrand Brand.
     * @return EntMstBrand
     * @throws SQLException SQLException.
     */
    EntMstBrand selectBrandByBrandCodeNotDeleted(EntMstBrand entMstBrand) throws SQLException;
}
