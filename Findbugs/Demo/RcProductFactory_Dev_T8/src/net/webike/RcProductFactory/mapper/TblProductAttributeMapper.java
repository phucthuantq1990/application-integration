/************************************************************************
 * File Name    ： TblProductAttributeMapper.java
 * Author       ： Nguyen.Chuong
 * Version      ： 1.0.0
 * Date Created ： 2014/02/17
 * Date Updated ： 2014/02/17
 * Description  ： contain function to process with rc_syouhin:
 *  - tbl_product_attribute_flag
 *  - tbl_product_attribute_float
 *  - tbl_product_attribute_group
 *  - tbl_product_attribute_integer
 *  - tbl_product_attribute_string
 *
 ***********************************************************************/
package net.webike.RcProductFactory.mapper;

import java.util.List;

import net.webike.RcProductFactory.entity.EntMstAttribute;
import net.webike.RcProductFactory.entity.EntMstProduct;

/**
 * contain function to process with rc_syouhin:.
 *  - tbl_product_attribute_flag
 *  - tbl_product_attribute_float
 *  - tbl_product_attribute_group
 *  - tbl_product_attribute_integer
 *  - tbl_product_attribute_string
 */
public interface TblProductAttributeMapper {

    /************************************************************************
     * <b>Description:</b><br>
     *  select List Attribute Flg By SyouhinCode from tbl_product_attribute_flag.
     *
     * @author		Nguyen.Chuong
     * @date		Feb 17, 2014
     * @param       syouhinSysCode Long
     * @return		List<EntMstAttribute>
     ************************************************************************/
    List<EntMstAttribute> selectListAttributeFlgBySyouhinCode(Long syouhinSysCode);

    /************************************************************************
     * <b>Description:</b><br>
     *  select List Attribute float By SyouhinCode from tbl_product_attribute_float.
     *
     * @author      Nguyen.Chuong
     * @date        Feb 17, 2014
     * @param       syouhinSysCode Long
     * @return      List<EntMstAttribute>
     ************************************************************************/
    List<EntMstAttribute> selectListAttributeFloatBySyouhinCode(Long syouhinSysCode);

    /************************************************************************
     * <b>Description:</b><br>
     *  select List Attribute group By SyouhinCode from tbl_product_attribute_group.
     *
     * @author      Nguyen.Chuong
     * @date        Feb 17, 2014
     * @param       syouhinSysCode Long
     * @return      List<EntMstAttribute>
     ************************************************************************/
    List<EntMstAttribute> selectListAttributeGroupBySyouhinCode(Long syouhinSysCode);

    /************************************************************************
     * <b>Description:</b><br>
     *  select List Attribute integer By SyouhinCode from tbl_product_attribute_integer.
     *
     * @author      Nguyen.Chuong
     * @date        Feb 17, 2014
     * @param       syouhinSysCode Long
     * @return      List<EntMstAttribute>
     ************************************************************************/
    List<EntMstAttribute> selectListAttributeIntegerBySyouhinCode(Long syouhinSysCode);

    /************************************************************************
     * <b>Description:</b><br>
     *  select List Attribute string By SyouhinCode from tbl_product_attribute_string.
     *
     * @author      Nguyen.Chuong
     * @date        Feb 17, 2014
     * @param       syouhinSysCode Long
     * @return      List<EntMstAttribute>
     ************************************************************************/
    List<EntMstAttribute> selectListAttributeStringBySyouhinCode(Long syouhinSysCode);

    /************************************************************************
     * <b>Description:</b><br>
     * insert new factory product attribute flag.
     *
     * @author      Long Vu
     * @param       prd insert condition
     * @date        Feb 25, 2014
     * @return      int
     ************************************************************************/
    int insertNewFactoryProductAttributeFlag(EntMstProduct prd);

    /************************************************************************
     * <b>Description:</b><br>
     * insert new factory product attribute float.
     *
     * @author      Long Vu
     * @param       prd insert condition
     * @date        Feb 25, 2014
     * @return      int
     ************************************************************************/
    int insertNewFactoryProductAttributeFloat(EntMstProduct prd);

    /************************************************************************
     * <b>Description:</b><br>
     * insert new factory product attribute grouop.
     *
     * @author      Long Vu
     * @param       prd insert condition
     * @date        Feb 25, 2014
     * @return      int
     ************************************************************************/
    int insertNewFactoryProductAttributeGroup(EntMstProduct prd);

    /************************************************************************
     * <b>Description:</b><br>
     * insert new factory product attribute integer.
     *
     * @author      Long Vu
     * @param       prd insert condition
     * @date        Feb 25, 2014
     * @return      int
     ************************************************************************/
    int insertNewFactoryProductAttributeInteger(EntMstProduct prd);

    /************************************************************************
     * <b>Description:</b><br>
     * insert new factory product attribute string.
     *
     * @author      Long Vu
     * @param       prd insert condition
     * @date        Feb 25, 2014
     * @return      int
     ************************************************************************/
    int insertNewFactoryProductAttributeString(EntMstProduct prd);

    /************************************************************************
     * <b>Description:</b><br>
     *  deleteFactoryAttributeFlagByProductIdAndAttributeCode.
     *
     * @author		Tran.Thanh
     * @date		26 Feb 2014
     * @param entMstAttribute		void
     ************************************************************************/
    void deleteFactoryAttributeFlagByProductIdAndAttributeCode(EntMstAttribute entMstAttribute);

    /************************************************************************
     * <b>Description:</b><br>
     *  deleteFactoryAttributeFloatByProductIdAndAttributeCode.
     *
     * @author		Tran.Thanh
     * @date		26 Feb 2014
     * @param entMstAttribute		void
     ************************************************************************/
    void deleteFactoryAttributeFloatByProductIdAndAttributeCode(EntMstAttribute entMstAttribute);

    /************************************************************************
     * <b>Description:</b><br>
     *  deleteFactoryAttributeGroupByProductIdAndAttributeCode.
     *
     * @author		Tran.Thanh
     * @date		26 Feb 2014
     * @param entMstAttribute		void
     ************************************************************************/
    void deleteFactoryAttributeGroupByProductIdAndAttributeCode(EntMstAttribute entMstAttribute);

    /************************************************************************
     * <b>Description:</b><br>
     *  deleteFactoryAttributeIntegerByProductIdAndAttributeCode.
     *
     * @author		Tran.Thanh
     * @date		26 Feb 2014
     * @param entMstAttribute		void
     ************************************************************************/
    void deleteFactoryAttributeIntegerByProductIdAndAttributeCode(EntMstAttribute entMstAttribute);

    /************************************************************************
     * <b>Description:</b><br>
     *  deleteFactoryAttributeStringByProductIdAndAttributeCode.
     *
     * @author		Tran.Thanh
     * @date		26 Feb 2014
     * @param entMstAttribute		void
     ************************************************************************/
    void  deleteFactoryAttributeStringByProductIdAndAttributeCode(EntMstAttribute entMstAttribute);

    /************************************************************************
     * <b>Description:</b><br>
     *  insertFactoryAttributeFlag.
     *
     * @author		Tran.Thanh
     * @date		5 Mar 2014
     * @param 		entMstAttribute EntMstAttribute
     * @return		int
     ************************************************************************/
    int insertFactoryAttributeFlag(EntMstAttribute entMstAttribute);


    /************************************************************************
     * <b>Description:</b><br>
     *  insertFactoryAttributeFloat.
     *
     * @author		Tran.Thanh
     * @date		5 Mar 2014
     * @param 		entMstAttribute EntMstAttribute
     * @return		int
     ************************************************************************/
    int insertFactoryAttributeFloat(EntMstAttribute entMstAttribute);


    /************************************************************************
     * <b>Description:</b><br>
     *  insertFactoryAttributeGroup.
     *
     * @author		Tran.Thanh
     * @date		5 Mar 2014
     * @param 		entMstAttribute EntMstAttribute
     * @return		int
     ************************************************************************/
    int insertFactoryAttributeGroup(EntMstAttribute entMstAttribute);


    /************************************************************************
     * <b>Description:</b><br>
     *  insertFactoryAttributeInteger.
     *
     * @author		Tran.Thanh
     * @date		5 Mar 2014
     * @param 		entMstAttribute EntMstAttribute
     * @return		int
     ************************************************************************/
    int insertFactoryAttributeInteger(EntMstAttribute entMstAttribute);


    /************************************************************************
     * <b>Description:</b><br>
     *  insertFactoryAttributeString.
     *
     * @author		Tran.Thanh
     * @date		5 Mar 2014
     * @param 		entMstAttribute EntMstAttribute
     * @return		int
     ************************************************************************/
    int insertFactoryAttributeString(EntMstAttribute entMstAttribute);


    /************************************************************************
     * <b>Description:</b><br>
     *  updateFactoryAttributeFlag.
     *
     * @author		Tran.Thanh
     * @date		5 Mar 2014
     * @param 		entMstAttribute EntMstAttribute
     * @return		int
     ************************************************************************/
    int updateFactoryAttributeFlag(EntMstAttribute entMstAttribute);


    /************************************************************************
     * <b>Description:</b><br>
     *  updateFactoryAttributeFloat.
     *
     * @author		Tran.Thanh
     * @date		5 Mar 2014
     * @param 		entMstAttribute EntMstAttribute
     * @return		int
     ************************************************************************/
    int updateFactoryAttributeFloat(EntMstAttribute entMstAttribute);


    /************************************************************************
     * <b>Description:</b><br>
     *  updateFactoryAttributeGroup.
     *
     * @author		Tran.Thanh
     * @date		5 Mar 2014
     * @param 		entMstAttribute EntMstAttribute
     * @return		int
     ************************************************************************/
    int updateFactoryAttributeGroup(EntMstAttribute entMstAttribute);


    /************************************************************************
     * <b>Description:</b><br>
     *  updateFactoryAttributeInteger.
     *
     * @author		Tran.Thanh
     * @date		5 Mar 2014
     * @param 		entMstAttribute EntMstAttribute
     * @return		int
     ************************************************************************/
    int updateFactoryAttributeInteger(EntMstAttribute entMstAttribute);

    /************************************************************************
     * <b>Description:</b><br>
     *  updateFactoryAttributeString.
     *
     * @author		Tran.Thanh
     * @date		5 Mar 2014
     * @param 		entMstAttribute EntMstAttribute
     * @return		int
     ************************************************************************/
    int updateFactoryAttributeString(EntMstAttribute entMstAttribute);
}
