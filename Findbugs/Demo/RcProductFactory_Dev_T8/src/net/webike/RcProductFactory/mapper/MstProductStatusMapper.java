/************************************************************************
 * File Name	： MstProductStatusMapper.java
 * Author		： Long Vu
 * Version		： 1.0.0
 * Date Created	： 2014/02/18
 * Date Updated	： 2014/02/18
 * Description	： MstProductStatusMapper.
 *
 ***********************************************************************/
package net.webike.RcProductFactory.mapper;

import java.util.List;

/**
 * Interface MstProductStatusMapper.
 */
public interface MstProductStatusMapper {

    /************************************************************************
     * <b>Description:</b><br>
     * get all product status name.
     *
     * @return      List<String>
     * @author      Long Vu
     * @date        Feb 19, 2014
     ************************************************************************/
    List<String> selectAllProductStatusName();

}
