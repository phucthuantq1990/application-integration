/************************************************************************
 * File Name	： TblFactoryProductImageMapper.java
 * Author		： Tran.Thanh
 * Version		： 1.0.0
 * Date Created	： 2014/02/24
 * Date Updated	： 2014/02/24
 * Description	： Contain method to get data from rc_product_factory.tbl_factory_product_image.
 *
 ***********************************************************************/
package net.webike.RcProductFactory.mapper;

import java.util.List;

import net.webike.RcProductFactory.entity.EntMstProduct;
import net.webike.RcProductFactory.entity.EntTblFactoryProductImage;

import org.apache.ibatis.annotations.Param;

/**
 * Interface TblProductVideoMapper.
 */
public interface TblFactoryProductImageMapper {

    /************************************************************************
     * <b>Description:</b><br>
     *  insert New Product Link.
     *
     * @author		Tran.Thanh
     * @date		21 Feb 2014
     * @param 		entTblFactoryProductLink entTblFactoryProductLink
     * @return		Integer
     ************************************************************************/
    //Integer insertNewProductLink(EntTblFactoryProductLink entTblFactoryProductLink);

    /************************************************************************
     * <b>Description:</b><br>
     *  update product Image by product_id and product_image_sort.
     *
     * @author		Tran.Thanh
     * @date		21 Feb 2014
     * @param entTblFactoryProductImage		void
     ************************************************************************/
    void updateProductImage(EntTblFactoryProductImage entTblFactoryProductImage);


    /************************************************************************
     * <b>Description:</b><br>
     *  deleteProductImageByProductIdAndProductImageSort.
     *
     * @author		Tran.Thanh
     * @date		Jul 2, 2014
     * @param 		list list image to delete
     * @param		sortImage image sort.
     * @return		int number of row effect
     ************************************************************************/
    //BOE #9601 Tran.Thanh : add process to save muti image
    //void deleteProductImageByProductIdAndProductImageSort(EntTblFactoryProductImage entTblFactoryProductImage);
    int deleteProductImageByProductIdAndProductImageSort(@Param("list")List<EntTblFactoryProductImage> list,
    		@Param("imageSort") int sortImage);
    //EOE #9601 Tran.Thanh : add process to save muti image

    /************************************************************************
     * <b>Description:</b><br>
     *  Select List productId by ListProductId and ImageSort.
     *
     * @author		Tran.Thanh
     * @date		Jul 1, 2014
     * @param 		listProductId ListProductId
     * @param 		ent contains imageSort
     * @return		List<EntTblFactoryProductImage>
     ************************************************************************/
    List<EntTblFactoryProductImage> selectProductImageByListProductIdAndImageSort(
    			@Param("list")List<Long> listProductId, @Param("ent")EntTblFactoryProductImage ent);

    /************************************************************************
     * <b>Description:</b><br>
     *  delete All Product Image By ProductId.
     *
     * @author		Tran.Thanh
     * @date		18 Mar 2014
     * @param productId		void
     ************************************************************************/
    void deleteAllProductImageByProductId(@Param("productId")Long productId);

    /************************************************************************
     * <b>Description:</b><br>
     * insert new factory product image.
     *
     * @author      Long Vu
     * @param       prd insert condition
     * @date        Feb 25, 2014
     * @return      int
     ************************************************************************/
    int insertNewFactoryProductImage(EntMstProduct prd);

    /************************************************************************
     * <b>Description:</b><br>
     *  insertFactoryProductImage.
     *
     * @author		Tran.Thanh
     * @date		5 Mar 2014
     * @param 		entTblFactoryProductImage EntTblFactoryProductImage
     * @return		int
     ************************************************************************/
    int insertFactoryProductImage(EntTblFactoryProductImage entTblFactoryProductImage);

    /**
     * insert multiple product image.
     * @param       list data.
     * @author      Doan.Chuong
     * @date        2014/03/05
     * @return      number error records.
     */
    int insertMultipleProductImage(@Param("list")List<EntTblFactoryProductImage> list);

    /************************************************************************
     * <b>Description:</b><br>
     *  selectProductImageBySyouhinSysCodeList.
     *
     * @author      Doan.Chuong
     * @date        Mar 12, 2014
     * @param       list  List<String>
     * @return      List<EntTblFactoryProductImage>
     ************************************************************************/
    List<EntTblFactoryProductImage> selectProductImageBySyouhinSysCodeList(List<String> list);

    /************************************************************************
     * <b>Description:</b><br>
     *  replace All Image in TblFactoryProductImage.
     *
     * @author		Tran.Thanh
     * @date		Jul 1, 2014
     * @param 		listProductId list Long product id need update
     * @param 		ent entImage
     * @return		int
     ************************************************************************/
    int updateTypeReplaceAll(@Param("list")List<Long> listProductId, @Param("ent")EntTblFactoryProductImage ent);

    /************************************************************************
     * <b>Description:</b><br>
     *  insert Product Image ReplaceAll.
     *
     * @author		Tran.Thanh
     * @date		Jul 2, 2014
     * @param 		listProductId list productId to insert
     * @param 		ent entity to insert
     * @return		int number of row effect
     ************************************************************************/
    int insertProductImageReplaceAll(@Param("list")List<Long> listProductId, @Param("ent")EntTblFactoryProductImage ent);

    /************************************************************************
     * <b>Description:</b><br>
     *  update Product Image ReplaceAll.
     *
     * @author		Tran.Thanh
     * @date		Jul 2, 2014
     * @param 		listProductId list productId to insert
     * @param 		ent entity to insert
     * @return		int number of row effect
     ************************************************************************/
    int updateProductImageReplaceAll(@Param("list")List<Long> listProductId, @Param("ent")EntTblFactoryProductImage ent);
}
