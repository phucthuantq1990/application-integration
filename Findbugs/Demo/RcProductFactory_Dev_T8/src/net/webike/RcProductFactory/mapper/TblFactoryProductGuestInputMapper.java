/************************************************************************
 * File Name    ： TblFactoryProductGuestInputMapper.java
 * Author       ：Luong.Dai
 * Version      ： 1.0.0
 * Date Created ： 2014/02/18
 * Date Updated ： 2014/02/18
 * Description  ： TblFactoryProductGuestInputMapper.
 ***********************************************************************/
package net.webike.RcProductFactory.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import net.webike.RcProductFactory.entity.EntMstProduct;
import net.webike.RcProductFactory.entity.EntTblFactoryProductGuestInput;


/**
 * TblFactoryProductGuestInputMapper.
 */
public interface TblFactoryProductGuestInputMapper {
    /************************************************************************
     * <b>Description:</b><br>
     *  Select list productGuestInput by productId.
     *
     * @author		Luong.Dai
     * @date		Feb 18, 2014
     * @param       productId Long
     * @return		List<EntTblFactoryProductGuestInput>
     ************************************************************************/
    List<EntTblFactoryProductGuestInput> selectListProductGuestInputByProductId(Long productId);

    /************************************************************************
     * <b>Description:</b><br>
     * insert new factory product guest input.
     *
     * @author      Long Vu
     * @param       prd insert condition
     * @date        Feb 25, 2014
     * @return      int
     ************************************************************************/
    int insertNewFactoryProductGuestInput(EntMstProduct prd);

    /************************************************************************
     * <b>Description:</b><br>
     *  insertNewProductGuestInput by entity productGuestInput.
     *
     * @author		Tran.Thanh
     * @date		25 Feb 2014
     * @param 		entTblFactoryProductGuestInput entTblFactoryProductGuestInput
     * @return		int
     ************************************************************************/
    int insertNewProductGuestInput(EntTblFactoryProductGuestInput entTblFactoryProductGuestInput);

    /************************************************************************
     * <b>Description:</b><br>
     *  updateProductGuestInput.
     *
     * @author		Tran.Thanh
     * @date		25 Feb 2014
     * @param 		entTblFactoryProductGuestInput		void
     ************************************************************************/
    void updateProductGuestInput(EntTblFactoryProductGuestInput entTblFactoryProductGuestInput);

    /************************************************************************
     * <b>Description:</b><br>
     *  deleteProductGuestInputByProductIdAndProductGuestSort.
     *
     * @author		Tran.Thanh
     * @date		25 Feb 2014
     * @param entTblFactoryProductGuestInput		void
     ************************************************************************/
    void deleteProductGuestInputByProductIdAndSort(EntTblFactoryProductGuestInput entTblFactoryProductGuestInput);

    /************************************************************************
     * <b>Description:</b><br>
     *  insertMultipleProductGuestInput.
     *
     * @author      Doan.Chuong
     * @date        Mar 7, 2014
     * @param       list  List<EntTblFactoryProductGuestInput>
     * @return      Integer
     ************************************************************************/
    Integer insertMultipleProductGuestInput(List<EntTblFactoryProductGuestInput> list);

    /************************************************************************
     * <b>Description:</b><br>
     *  selectProductGuestInputBySyouhinSysCodeList.
     *
     * @author      Doan.Chuong
     * @date        Mar 12, 2014
     * @param       list  List<String>
     * @return      List<EntTblFactoryProductGuestInput>
     ************************************************************************/
    List<EntTblFactoryProductGuestInput> selectProductGuestInputBySyouhinSysCodeList(List<String> list);

    /************************************************************************
     * <b>Description:</b><br>
     *  Delete all guestg input of product by productId.
     *
     * @author		Luong.Dai
     * @date		Apr 4, 2014
     * @param 		productId ProductId of product
     * @return		Long
     ************************************************************************/
    Integer deleteAllGuestInputOfProduct(@Param("productId")Long productId);
}
