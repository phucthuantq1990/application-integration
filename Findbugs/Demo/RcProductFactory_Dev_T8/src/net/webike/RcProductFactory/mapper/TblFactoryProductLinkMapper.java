/************************************************************************
 * File Name	： TblProductLinkMapper.java
 * Author		： Tran.Thanh
 * Version		： 1.0.0
 * Date Created	： 2014/02/18
 * Date Updated	： 2014/02/18
 * Description	： Contain method to get data from rc_product_factory.tbl_factory_product_link.
 *
 ***********************************************************************/
package net.webike.RcProductFactory.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import net.webike.RcProductFactory.entity.EntMstProduct;
import net.webike.RcProductFactory.entity.EntTblFactoryProductLink;

/**
 * Interface TblProductVideoMapper.
 */
public interface TblFactoryProductLinkMapper {


    /************************************************************************
     * <b>Description:</b><br>
     *  select Link By ProductId.
     *
     * @author		Tran.Thanh
     * @date		18 Feb 2014
     * @param 		productId Long
     * @param       linkSort  String
     * @return		List<EntTblFactoryProductLink>
     ************************************************************************/
    List<EntTblFactoryProductLink> selectLinkByProductId(@Param("productId")Long productId
                                                       , @Param("linkSort")String linkSort);

    /************************************************************************
     * <b>Description:</b><br>
     *  insert New Product Link.
     *
     * @author		Tran.Thanh
     * @date		21 Feb 2014
     * @param 		entTblFactoryProductLink entTblFactoryProductLink
     * @return		Integer
     ************************************************************************/
    Integer insertNewProductLink(EntTblFactoryProductLink entTblFactoryProductLink);

    /************************************************************************
     * <b>Description:</b><br>
     *  update product Link by product_id and product_link_sort.
     *
     * @author		Tran.Thanh
     * @date		21 Feb 2014
     * @param entTblFactoryProductLink		void
     ************************************************************************/
    void updateProductlink(EntTblFactoryProductLink entTblFactoryProductLink);

    /************************************************************************
     * <b>Description:</b><br>
     *  delete Product Link By ProductId And ProductLinkSort.
     *
     * @author		Tran.Thanh
     * @date		21 Feb 2014
     * @param entTblFactoryProductLink		void
     ************************************************************************/
    void deleteProductLinkByProductIdAndProductLinkSort(EntTblFactoryProductLink entTblFactoryProductLink);

    /************************************************************************
     * <b>Description:</b><br>
     * insert new factory product link.
     *
     * @author      Long Vu
     * @param       prd insert condition
     * @date        Feb 25, 2014
     * @return      int
     ************************************************************************/
    int insertNewFactoryProductLink(EntMstProduct prd);

    /************************************************************************
     * <b>Description:</b><br>
     *  insertMultipleProductLink.
     *
     * @author      Doan.Chuong
     * @date        Mar 7, 2014
     * @param       list  List<EntTblFactoryProductLink>
     * @return      Integer
     ************************************************************************/
    Integer insertMultipleProductLink(List<EntTblFactoryProductLink> list);

    /************************************************************************
     * <b>Description:</b><br>
     *  selectProductLinkBySyouhinSysCodeList.
     *
     * @author      Doan.Chuong
     * @date        Mar 12, 2014
     * @param       list  List<String>
     * @return      List<EntTblFactoryProductLink>
     ************************************************************************/
    List<EntTblFactoryProductLink> selectProductLinkBySyouhinSysCodeList(List<String> list);

    /************************************************************************
     * <b>Description:</b><br>
     *  Select list link of product by productId.
     *
     * @author		Luong.Dai
     * @date		Apr 7, 2014
     * @param 		productId 						ID of product
     * @return		List<EntTblFactoryProductLink>	List link of product
     ************************************************************************/
    List<EntTblFactoryProductLink> selectProductLinkValueByProductId(Long productId);
}
