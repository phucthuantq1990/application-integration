/************************************************************************
 * File Name	： MstModelMapper.java
 * Author		： Nguyen.Chuong
 * Version		： 1.0.0
 * Date Created	： 2014/01/23
 * Description	： Contain method to get data from database .
 *
 ***********************************************************************/
package net.webike.RcProductFactory.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import net.webike.RcProductFactory.entity.EntMstModel;

/**
 * Interface MstModelMapper.
 */
public interface MstModelMapper {

    /************************************************************************
     * <b>Description:</b><br>
     *  select list model all and filter for checkFitModel page.
     *
     * @author		Nguyen.Chuong
     * @date		Jan 23, 2014
     * @param       entMstModel contain param
     * @param       limit limit
     * @param       offset offset
     * @return		List<EntMstModel>
     ************************************************************************/
    List<EntMstModel> selectListModel(
          @Param("limit")int limit
          , @Param("offset")int offset
          , @Param("entMstModel")EntMstModel entMstModel);

    /************************************************************************
     * <b>Description:</b><br>
     *  Select list model count with filter.
     *
     * @author      hoang.ho
     * @date        Feb 13, 2014
     * @param       entMstModel contain param
     * @return      List<int>
     ************************************************************************/
    int selectListModelCount(@Param("entMstModel")EntMstModel entMstModel);

    /************************************************************************
     * <b>Description:</b><br>
     *  select list field of model after filter for checkFitModel page.
     *
     * @author      hoang.ho
     * @date        Feb 13, 2014
     * @param       autoCompleteField field used for get data
     * @param       entMstModel contain param
     * @return      List<String>
     ************************************************************************/
    List<String> selectModelFilterList(
            @Param("autoCompleteField")String autoCompleteField
            , @Param("entMstModel") EntMstModel entMstModel);

    /************************************************************************
     * <b>Description:</b><br>
     * get model all model list in DB.
     *
     * @author      Long Vu
     * @date        Feb 10, 2014
     * @return      List<EntMstModel>
     ************************************************************************/
    List<EntMstModel> selectAllListModel();

}
