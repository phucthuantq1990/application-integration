/************************************************************************
 * File Name	： TblProductVideoMapper.java
 * Author		： Nguyen.Chuong
 * Version		： 1.0.0
 * Date Created	： 2014/02/17
 * Date Updated	： 2014/02/17
 * Description	： Contain method to get data from rc_syouhinh.tbl_product_video.
 *
 ***********************************************************************/
package net.webike.RcProductFactory.mapper;

import java.util.List;

import net.webike.RcProductFactory.entity.EntTblProductVideo;

/**
 * Interface TblProductVideoMapper.
 */
public interface TblProductVideoMapper {

    /************************************************************************
     * <b>Description:</b><br>
     *  select list video link from rc_syouhin.tbl_product_video by syouhinSysCode.
     *
     * @author		Nguyen.Chuong
     * @date		Feb 17, 2014
     * @param       syouhinSysCode Long
     * @return		List<EntTblProductVideo>
     ************************************************************************/
    List<EntTblProductVideo> selectLinkVideoBySyouhinSysCode(Long syouhinSysCode);
}
