/************************************************************************
 * File Name	： TblFactoryCalibrationRequestWholeMapper.java
 * Author		： Nguyen.Chuong
 * Version		： 1.0.0
 * Date Created	： 2014/07/17
 * Date Updated	： 2014/07/17
 * Description	： Contain method to get data from rc_product_factory.tbl_factory_calibration_request_whole.
 *
 ***********************************************************************/
package net.webike.RcProductFactory.mapper;

import net.webike.RcProductFactory.entity.EntTblFactoryCalibrationRequestWhole;

/**
 * Interface TblFactoryCalibrationRequestWholeMapper.
 */
public interface TblFactoryCalibrationRequestWholeMapper {


    /************************************************************************
     * <b>Description:</b><br>
     *  select calibration request whole of matter in table tbl_factory_calibration_request_whole.
     *
     * @author		Nguyen.Chuong
     * @date		2014/07/17
     * @param 		matterNo Long
     * @return		EntTblFactoryCalibrationRequestWhole
     ************************************************************************/
    EntTblFactoryCalibrationRequestWhole selectCalibrationRequestWhole(Long matterNo);

    /************************************************************************
     * <b>Description:</b><br>
     *  save tab all value of popup.
     *
     * @author      Nguyen.Chuong
     * @date        Jul 17, 2014
     * @param       ent EntTblFactoryCalibrationRequestWhole of tab 1 content matterNo, value, user update id
     ************************************************************************/
    void insertTabAllPopup(EntTblFactoryCalibrationRequestWhole ent);

    /************************************************************************
     * <b>Description:</b><br>
     *  save tab memo value of popup.
     *
     * @author      Nguyen.Chuong
     * @date        Jul 17, 2014
     * @param       ent EntTblFactoryCalibrationRequestWhole of tab 3 content matterNo, value, user update id
     ************************************************************************/
    void insertTabMemoPopup(EntTblFactoryCalibrationRequestWhole ent);

}
