/************************************************************************
 * File Name    ： TblFactoryProductSupplierMapper.java
 * Author       ：Le Dinh
 * Version      ： 1.0.0
 * Date Created ： 2014/02/11
 * Date Updated ： 2014/02/11
 * Description  ： TblFactoryProductSupplierMapper.
 ***********************************************************************/
package net.webike.RcProductFactory.mapper;

import java.sql.SQLException;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import net.webike.RcProductFactory.entity.EntMstProduct;
import net.webike.RcProductFactory.entity.EntTblFactoryProductNew;
import net.webike.RcProductFactory.entity.EntTblFactoryProductSupplier;

/**
 * TblFactoryProductGeneralMapper.
 */
public interface TblFactoryProductSupplierMapper {
	/************************************************************************
	 * <b>Description:</b><br>
	 *  selectlistSupplierByProductId.
	 *
	 * @author		Le.Dinh
	 * @date		12 Feb 2014
	 * @param 		productId	Long
	 * @return		List<EntTblFactoryProductSupplier>
	 * @throws 		SQLException	SQLException
	 ************************************************************************/
	List<EntTblFactoryProductSupplier> selectListSupplierByProductId(Long productId) throws SQLException;

	/************************************************************************
	 * <b>Description:</b><br>
	 *  deleteSupplierByProductIdAndsiireCode.
	 *
	 * @author		Le.Dinh
	 * @date		12 Feb 2014
	 * @param 		productId	Long
	 * @param 		siireCode	String
	 * @return		Integer
	 * @throws 		SQLException	SQLException
	 ************************************************************************/
	Integer deleteSupplierByProductIdAndsiireCode(@Param("productId")Long productId , @Param("siireCode")String siireCode) throws SQLException;

	/************************************************************************
	 * <b>Description:</b><br>
	 *  updateSupplierByProductIdAndsiireCode.
	 *
	 * @author		Le.Dinh
	 * @date		25 Feb 2014
	 * @param 		entTblFactoryProductSupplier	EntTblFactoryProductSupplier
	 * @return		Integer
	 * @throws 		SQLException	SQLException
	 ************************************************************************/
	Integer updateSupplierByProductIdAndsiireCode(EntTblFactoryProductSupplier	entTblFactoryProductSupplier) throws SQLException;

	/************************************************************************
	 * <b>Description:</b><br>
	 *  insertSupplierByProductIdAndsiireCode.
	 *
	 * @author		Le.Dinh
	 * @date		25 Feb 2014
	 * @param 		entTblFactoryProductSupplier	EntTblFactoryProductSupplier
	 * @return		Integer
	 * @throws 		SQLException	SQLException
	 ************************************************************************/
	Integer insertSupplierByProductIdAndsiireCode(EntTblFactoryProductSupplier	entTblFactoryProductSupplier) throws SQLException;
	/************************************************************************
     * <b>Description:</b><br>
     * insert new factory product supplier.
     *
     * @author      Long Vu
     * @param       prd insert condition
     * @date        Feb 25, 2014
     * @return      int
     ************************************************************************/
    int insertNewFactoryProductSupplier(EntMstProduct prd);

    /************************************************************************
     * <b>Description:</b><br>
     *  Update product_supplier_price by productId.
     *
     * @author		Luong.Dai
     * @date		Mar 3, 2014
     * @param       product EntTblFactoryProductNew
     * @return		Integer
     ************************************************************************/
    Integer updateSupplierPriceByProductId(EntTblFactoryProductNew product);

    /************************************************************************
     * <b>Description:</b><br>
     *  selectProductSupplierBySyouhinSysCodeList.
     *
     * @author      Doan.Chuong
     * @date        Mar 12, 2014
     * @param       list  List<String>
     * @return      List<EntTblFactoryProductSupplier>
     ************************************************************************/
    List<EntTblFactoryProductSupplier> selectProductSupplierBySyouhinSysCodeList(List<String> list);

    /************************************************************************
     * <b>Description:</b><br>
     *  insertMultipleProductSupplier.
     *
     * @author      Doan.Chuong
     * @date        Mar 12, 2014
     * @param       list  List<EntTblFactoryProductSupplier>
     * @return      Integer
     ************************************************************************/
    Integer insertMultipleProductSupplier(List<EntTblFactoryProductSupplier> list);

    /************************************************************************
     * <b>Description:</b><br>
     *  Delete all supplier of product by productId.
     *
     * @author		Luong.Dai
     * @date		Apr 4, 2014
     * @param 		productId ID of product.
     * @return		Long
     ************************************************************************/
    Integer deleteAllSupplierByProductId(@Param("productId")Long productId);
}
