/************************************************************************
 * File Name    ： TblTmpCSVMapper.java
 * Author       ： Nguyen.Chuon
 * Version      ： 1.0.0
 * Date Created ： 2014/02/26
 * Date Updated ： 2014/02/26
 * Description  ： TblTmpCSVMapper Mapper to process with CSV temp table.
 ***********************************************************************/
package net.webike.RcProductFactory.mapper;

import java.util.List;

import net.webike.RcProductFactory.entity.EntCSVMode1;
import net.webike.RcProductFactory.entity.EntTblFactoryImportResult;
import net.webike.RcProductFactory.entity.EntTblTmpCSV;

import org.apache.ibatis.annotations.Param;

/**
 * TblTmpCSVMapper Mapper.
 */
public interface TblTmpCSVMapper {
    /************************************************************************
     * <b>Description:</b><br>
     *  create table temp for insert temp data of import CSV mode 1 all column with out image and select column.
     *
     * @author      Nguyen.Chuong
     * @param       tableName table Name
     * @date        Feb 26, 2014
     ************************************************************************/
    void createTblTmpCSVMode1A(@Param("tableName")String tableName);

    /************************************************************************
     * <b>Description:</b><br>
     *  create table temp for insert temp data of import CSV mode 1 only image and select column.
     *
     * @author      Nguyen.Chuong
     * @param       tableName table Name
     * @date        Feb 26, 2014
     ************************************************************************/
    void createTblTmpCSVMode1B(@Param("tableName")String tableName);

    /************************************************************************
     * <b>Description:</b><br>
     *  create table temp for insert temp data of import CSV mode 2.
     *
     * @author      Nguyen.Chuong
     * @param       tableName table Name
     * @date        Feb 26, 2014
     ************************************************************************/
    void createTblTmpCSVMode2(@Param("tableName")String tableName);

    /************************************************************************
     * <b>Description:</b><br>
     *  create table temp for insert temp data of import CSV mode 3.
     *
     * @author		Nguyen.Chuong
     * @param       tableName table Name
     * @date		Feb 26, 2014
     ************************************************************************/
    void createTblTmpCSVMode3(@Param("tableName")String tableName);

    /************************************************************************
     * <b>Description:</b><br>
     *  createTblTmpCSVImportResult.
     *
     * @author      Doan.Chuong
     * @param       tableName table Name
     * @date        March 05, 2014
     ************************************************************************/
    void createTblTmpCSVImportResult(@Param("tableName")String tableName);

    /************************************************************************
     * <b>Description:</b><br>
     *  dropTableTmpByName.
     *
     * @author      Doan.Chuong
     * @param       tableName table Name
     * @date        March 05, 2014
     ************************************************************************/
    void dropTableTmpByName(@Param("tableName")String tableName);
    /************************************************************************
     * <b>Description:</b><br>
     *.
     * @author      Thai.Son
     * @date        Jan 23, 2014
     * @param       importedCSVTableName String
     * @param       limit int
     * @param       offset int
     * @param       entCSVMode1 EntCSVMode1
     * @return      List<EntTblFactoryProductNew>
     ************************************************************************/
    List<EntCSVMode1> selectListTempDataForGridMode1(@Param("importedCSVTableName")String importedCSVTableName
                                                   , @Param("limit")int limit
                                                   , @Param("offset")int offset
                                                   , @Param("entCSVMode1")EntCSVMode1 entCSVMode1);
    /************************************************************************
     * <b>Description:</b><br>
     *  count record when get data from DB.
     *
     * @author      Thai.Son
     * @date        Jan 16, 2014
     * @param       importedCSVTableName String
     * @param       entCSVMode1 EntCSVMode1 contain filter
     * @return      int
     ************************************************************************/
    int selectTotalRecordForGridMode1(@Param("importedCSVTableName")String importedCSVTableName
                                    , @Param("entCSVMode1") EntCSVMode1 entCSVMode1);
    /************************************************************************
     * <b>Description:</b><br>
     *  insertTblFactoryImportResultFromTmp.
     *
     * @author      Doan.Chuong
     * @param       tableName table Name
     * @param       importConditionCode long
     * @date        March 05, 2014
     ************************************************************************/
    void insertTblFactoryImportResultFromTmp(@Param("tableName")String tableName
                                           , @Param("importConditionCode")long importConditionCode);

    /************************************************************************
     * <b>Description:</b><br>
     *  Insert temp product mode 1 to tbl_tmp table all column with out image and select column.
     *
     * @author      Nguyen.Chuong
     * @date        Feb 27, 2014
     * @param       importedCSVTableName table name to insert
     * @param       listTmpCSVMode1 list contain temp product
     ************************************************************************/
    void insertTmpProductToTblTmpModel1A(@Param("importedCSVTableName")String importedCSVTableName
                                       , @Param("listTmpCSVMode1")List<EntCSVMode1> listTmpCSVMode1);

    /************************************************************************
     * <b>Description:</b><br>
     *  Insert temp product mode 1 to tbl_tmp table only image and select column..
     *
     * @author      Nguyen.Chuong
     * @date        Feb 27, 2014
     * @param       importedCSVTableName table name to insert
     * @param       listTmpCSVMode1 list contain temp product
     ************************************************************************/
    void insertTmpProductToTblTmpModel1B(@Param("importedCSVTableName")String importedCSVTableName
                                       , @Param("listTmpCSVMode1")List<EntCSVMode1> listTmpCSVMode1);

    /************************************************************************
     * <b>Description:</b><br>
     *  Insert temp product mode 2 to tbl_tmp table.
     *
     * @author      Nguyen.Chuong
     * @date        Feb 27, 2014
     * @param       importedCSVTableName table name to insert
     * @param       listEntTmp list contain temp product
     ************************************************************************/
    void insertTmpProductToTblTmpModel2(@Param("importedCSVTableName")String importedCSVTableName
                                      , @Param("listEntTmp")List<EntTblTmpCSV> listEntTmp);

    /************************************************************************
     * <b>Description:</b><br>
     *  insertTblTmpCSVImportResult.
     *
     * @author      Doan.Chuong
     * @date        March 05, 2014
     * @param       tableName table name to insert
     * @param       listEntTmp list contain temp product
     ************************************************************************/
    void insertTblTmpCSVImportResult(@Param("tableName")String tableName
                                      , @Param("listEntTmp")List<EntTblFactoryImportResult> listEntTmp);

    /************************************************************************
     * <b>Description:</b><br>
     *  Insert temp product mode 3 to tbl_tmp table.
     *
     * @author		Nguyen.Chuong
     * @date		Feb 27, 2014
     * @param       importedCSVTableName table name to insert
     * @param       listEntTmp list contain temp product
     ************************************************************************/
    void insertTmpProductToTblTmpModel3(@Param("importedCSVTableName")String importedCSVTableName
                                      , @Param("listEntTmp")List<EntTblTmpCSV> listEntTmp);

    /************************************************************************
     * <b>Description:</b><br>
     *  select filter list product imported to DB mode 2.
     *
     * @author      Nguyen.Chuong
     * @date        Mar 4, 2014
     * @param       importedCSVTableName String
     * @param       entCSVFilter EntTblTmpCSV filter object
     * @param       limit int
     * @param       offset int
     * @return      List<EntTblTmpCSVMode3>
     ************************************************************************/
    List<EntTblTmpCSV> selectListImportedCSVMode2Filter(@Param("importedCSVTableName")String importedCSVTableName
                                                      , @Param("entCSVFilter")EntTblTmpCSV entCSVFilter
                                                      , @Param("limit")Integer limit
                                                      , @Param("offset")Integer offset);

    /************************************************************************
     * <b>Description:</b><br>
     *  total select filter list product imported to DB mode 2.
     *
     * @author      Nguyen.Chuong
     * @date        Mar 4, 2014
     * @param       importedCSVTableName String
     * @param       entCSVFilter EntTblTmpCSV filter object
     * @return      int total record
     ************************************************************************/
    int selectTotalListImportedCSVMode2Filter(@Param("importedCSVTableName")String importedCSVTableName
                                            , @Param("entCSVFilter")EntTblTmpCSV entCSVFilter);

    /************************************************************************
     * <b>Description:</b><br>
     *  select filter list product imported to DB mode 3.
     *
     * @author		Nguyen.Chuong
     * @date		Mar 4, 2014
     * @param       importedCSVTableName String
     * @param       entCSVFilter EntTblTmpCSV filter object
     * @param       limit int
     * @param       offset int
     * @return		List<EntTblTmpCSVMode3>
     ************************************************************************/
    List<EntTblTmpCSV> selectListImportedCSVMode3Filter(@Param("importedCSVTableName")String importedCSVTableName
                                                      , @Param("entCSVFilter")EntTblTmpCSV entCSVFilter
                                                      , @Param("limit")Integer limit
                                                      , @Param("offset")Integer offset);

    /************************************************************************
     * <b>Description:</b><br>
     *  total select filter list product imported to DB mode 3.
     *
     * @author		Nguyen.Chuong
     * @date		Mar 4, 2014
     * @param       importedCSVTableName String
     * @param       entCSVFilter EntTblTmpCSV filter object
     * @return		int total record
     ************************************************************************/
    int selectTotalListImportedCSVMode3Filter(@Param("importedCSVTableName")String importedCSVTableName
                                            , @Param("entCSVFilter")EntTblTmpCSV entCSVFilter);

    /************************************************************************
     * <b>Description:</b><br>
     *  get list product_id which is released or not base on param product_id list and released_status.
     *
     * @author		Nguyen.Chuong
     * @date		May 20, 2014
     * @param       listProductId List<Long>
     * @param       releasedStatus boolean: true: get product released, false: get product unreleased
     * @return		List<String>
     ************************************************************************/
    List<String> selectListProductIdReleaseByProductIdList(@Param("listProductId")List<Long> listProductId
			, @Param("releasedStatus")boolean releasedStatus);
    /*List<String> selectListProductIdReleaseByProductIdList(@Param("listEntProductId")List<EntCSVMode1> listEntProductId
                                                         , @Param("releasedStatus")boolean releasedStatus);*/

    /************************************************************************
     * <b>Description:</b><br>
     *  select list temporary table of user.
     *
     * @author		Tran.Thanh
     * @date		May 27, 2014
     * @param 		loginId login user id
     * @return		List<String> list name of temporary table
     ************************************************************************/
    List<String> selectListTmpCSVTableByLoginId(@Param("loginId")String loginId);
}
