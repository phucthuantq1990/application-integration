/************************************************************************
 * File Name	： TblFactoryProductSupplierPriceMapper.java
 * Author		： Long Vu
 * Version		： 1.0.0
 * Date Created	： 2014/02/24
 * Date Updated	： 2014/02/24
 * Description	： TblFactoryProductSupplierPriceMapper.
 *
 ***********************************************************************/
package net.webike.RcProductFactory.mapper;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import net.webike.RcProductFactory.entity.EntMstProduct;
import net.webike.RcProductFactory.entity.EntTblFactoryProductNew;
import net.webike.RcProductFactory.entity.EntTblFactoryProductSupplierPrice;

/**
 * Interface TblFactoryProductSupplierPriceMapper.
 */
public interface TblFactoryProductSupplierPriceMapper {

    /************************************************************************
     * <b>Description:</b><br>
     * insert new factory product supplier price.
     *
     * @author      Long Vu
     * @param       prd insert condition
     * @date        Feb 25, 2014
     * @return      int
     ************************************************************************/
    int insertNewFactoryProductSupplierPrice(EntMstProduct prd);

    /************************************************************************
     * <b>Description:</b><br>
     *  insert new supplier_price for group product.
     *
     * @author		Luong.Dai
     * @date		Mar 4, 2014
     * @param       product     EntTblFactoryProductNew
     * @return		Integer
     ************************************************************************/
    Integer insertNewSupplierPriceOfGroupProduct(EntTblFactoryProductNew product);

    /************************************************************************
     * <b>Description:</b><br>
     *  insertMultipleSupplierPrice.
     *
     * @author		Nguyen.Chuong
     * @date		May 23, 2014
     * @param 		listSupplierPrice list EntTblFactoryProductSupplierPrice
     * @return		Integer
     ************************************************************************/
    Integer insertMultipleSupplierPrice(@Param ("listSupplierPrice") List<EntTblFactoryProductSupplierPrice> listSupplierPrice);

    /************************************************************************
     * <b>Description:</b><br>
     *  select updatedOn of supplierPrice.
     *
     * @author		Luong.Dai
     * @date		Mar 4, 2014
     * @param       productId   Integer
     * @return		Date
     ************************************************************************/
    Date selectUpdatedOnOfSupplierPriceByProductId(Integer productId);

    /************************************************************************
     * <b>Description:</b><br>
     * insert multiple new factory product supplier price.
     *
     * @author      Doan Chuong
     * @date        Match 12, 2014
     * @param       list List<String>
     * @param       productId int
     * @param       updatedUserId String
     * @return      int
     ************************************************************************/
    int insertMultipleNewFactoryProductSupplierPrice(@Param ("list") List<String> list
                                                   , @Param ("productId") long productId
                                                   , @Param("updatedUserId") String updatedUserId);

    /************************************************************************
     * <b>Description:</b><br>
     *  update Product Supplier Price Price.
     *
     * @author		Tran.Thanh
     * @date		Apr 23, 2014
     * @param 		list list productId
     * @param 		entTblFactoryProductSupplierPrice EntTblFactoryProductSupplierPrice for query.
     * @return		int
     ************************************************************************/
    int updateProductSupplierPricePrice(@Param ("list") List<Long> list
    		, @Param ("entTblFactoryProductSupplierPrice") EntTblFactoryProductSupplierPrice entTblFactoryProductSupplierPrice);
}
