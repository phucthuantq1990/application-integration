/************************************************************************
 * File Name	： MstSyouhinAllMapper.java
 * Author		： Tran.Thanh
 * Version		： 1.0.0
 * Date Created	： 2014/02/20
 * Date Updated	： 2014/02/20
 * Description	： Contain method to get data from webikesh.mst_syouhin_all.
 *
 ***********************************************************************/
package net.webike.RcProductFactory.mapper;

import java.util.List;

import net.webike.RcProductFactory.entity.EntMstSyouhinSearch;

/**
 * Interface MstSyouhinAllMapper.
 */
public interface MstSyouhinSearchMapper {

    /************************************************************************
     * <b>Description:</b><br>
     *  filter data from webikesh.mst_syouhin_all.
     *
     * @author		Tran.Thanh
     * @date		20 Feb 2014
     * @param 		syouhin EntMstSyouhinAll
     * @return		List<EntMstSyouhinAll>
     ************************************************************************/
    List<EntMstSyouhinSearch> selectSyouhinSearch(EntMstSyouhinSearch syouhin);
}
