/************************************************************************
 * File Name	： TblFactoryProductVideoMapper.java
 * Author		： Tran.Thanh
 * Version		： 1.0.0
 * Date Created	： 2014/02/12
 * Date Updated	： 2014/02/12
 * Description	： Contain method to get data from database .
 *
 ***********************************************************************/
package net.webike.RcProductFactory.mapper;

import java.util.List;

import net.webike.RcProductFactory.entity.EntMstProduct;
import net.webike.RcProductFactory.entity.EntTblFactoryProductVideo;

import org.apache.ibatis.annotations.Param;

/**
 * Interface MstMarumeMapper.
 */
public interface TblFactoryProductVideoMapper {

	/************************************************************************
	 * <b>Description:</b><br>
	 * select Video By ProductId From Tbl_Factory_Product_Video table.
	 *
	 * @author		Tran.Thanh
	 * @date		12 Feb 2014
	 * @param 		productId productId
	 * @return		EntTblFactoryProductDescription
	 ************************************************************************/
	List<EntTblFactoryProductVideo> selectVideoByProductId(Long productId);

	/************************************************************************
	 * <b>Description:</b><br>
	 *  select list video by list product_id.
	 *
	 * @author		Tran.Thanh
	 * @date		24 Feb 2014
	 * @param 		listVideo List<Integer>
	 * @return		List<EntTblFactoryProductVideo>
	 ************************************************************************/
	List<EntTblFactoryProductVideo> selectVideoByListProductId(@Param("listVideo")List<Integer> listVideo);

	/************************************************************************
	 * <b>Description:</b><br>
	 *  insert New Product Video.
	 *
	 * @author		Tran.Thanh
	 * @date		14 Feb 2014
	 * @param 		entTblFactoryProductVideo entTblFactoryProductVideo
	 * @return		Integer
	 ************************************************************************/
	Integer insertNewProductVideo(EntTblFactoryProductVideo entTblFactoryProductVideo);

	/************************************************************************
	 * <b>Description:</b><br>
	 *  update Product Video.
	 *
	 * @author		Tran.Thanh
	 * @date		14 Feb 2014
	 * @param 		entTblFactoryProductVideo entTblFactoryProductVideo
	 ************************************************************************/
	void updateProductVideo(EntTblFactoryProductVideo entTblFactoryProductVideo);

	/************************************************************************
	 * <b>Description:</b><br>
	 *  delete Product Video By ProductId And ProductVideoSort.
	 *
	 * @author		Tran.Thanh
	 * @date		14 Feb 2014
	 * @param 		entTblFactoryProductVideo entTblFactoryProductVideo
	 ************************************************************************/
	void deleteProductVideoByProductIdAndProductVideoSort(EntTblFactoryProductVideo entTblFactoryProductVideo);

	/************************************************************************
     * <b>Description:</b><br>
     * insert new factory product video.
     *
     * @author      Long Vu
     * @param       prd insert condition
     * @date        Feb 25, 2014
     * @return      int
     ************************************************************************/
    int insertNewFactoryProductVideo(EntMstProduct prd);

    /**
     * insert multiple product video.
     * @param       list data.
     * @author      Doan.Chuong
     * @date        2014/03/05
     * @return      number error records.
     */
    int insertMultipleProductVideo(@Param("list")List<EntTblFactoryProductVideo> list);

    /************************************************************************
     * <b>Description:</b><br>
     *  selectProductVideoBySyouhinSysCodeList.
     *
     * @author      Doan.Chuong
     * @date        Mar 12, 2014
     * @param       list  List<String>
     * @return      List<EntTblFactoryProductVideo>
     ************************************************************************/
    List<EntTblFactoryProductVideo> selectProductVideoBySyouhinSysCodeList(List<String> list);

    /************************************************************************
     * <b>Description:</b><br>
     *  delete all video of product.
     *
     * @author		Luong.Dai
     * @date		Apr 4, 2014
     * @param 		productId Ent has productId
     * @return		Integer
     ************************************************************************/
    Integer deleteAllVideoOfProduct(@Param("productId")Long productId);
}
