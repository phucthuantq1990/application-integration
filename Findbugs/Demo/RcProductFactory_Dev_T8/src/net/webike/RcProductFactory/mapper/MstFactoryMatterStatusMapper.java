/************************************************************************
 * File Name	： MstFactoryMatterStatusMapper.java
 * Author		： Nguyen.Chuong
 * Version		： 1.0.0
 * Date Created	： 2013/09/24
 * Date Updated	： 2013/09/24
 * Description	： Contain method to get data from database .
 *
 ***********************************************************************/
package net.webike.RcProductFactory.mapper;

import java.util.List;

import net.webike.RcProductFactory.entity.EntMstFactoryMatterStatus;

/**
 * Interface MstFactoryMatterStatusMapper.
 */
public interface MstFactoryMatterStatusMapper {

    /************************************************************************
     * <b>Description:</b><br>
     *  select all mater status from mst_factory_matter_status.
     *
     * @author		Nguyen.Chuong
     * @date		Sep 10, 2013
     * @return		List<EntMstFactoryMatterStatus>
     ************************************************************************/
    List<EntMstFactoryMatterStatus> selectAllMatterStatus();

}
