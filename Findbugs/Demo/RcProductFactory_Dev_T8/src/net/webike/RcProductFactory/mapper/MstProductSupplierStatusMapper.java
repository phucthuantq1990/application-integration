/************************************************************************
 * File Name	： MstProductSupplierStatusMapper.java
 * Author		： Le.Dinh
 * Version		： 1.0.0
 * Date Created	： 2014/02/12
 * Date Updated	： 2014/02/12
 * Description	： MstProductSupplierStatusMapper .
 *
 ***********************************************************************/
package net.webike.RcProductFactory.mapper;

import java.sql.SQLException;
import java.util.List;

import net.webike.RcProductFactory.entity.EntMstProductSupplierStatus;


/**
 * Interface MstProductSupplierStatusMapper.
 */
public interface MstProductSupplierStatusMapper {

	/************************************************************************
	 * <b>Description:</b><br>
	 *  rselectListAllSupplierStatus.
	 *
	 * @author		Le.Dinh
	 * @date		13 Feb 2014
	 * @return		List<EntMstProductSupplierStatus>
	 * @throws 		SQLException		SQLException
	 ************************************************************************/
	List<EntMstProductSupplierStatus> selectListAllSupplierStatus() throws SQLException;

	/************************************************************************
     * <b>Description:</b><br>
     * get supplier status name.
     *
     * @return      List<String>
     * @author      Long Vu
     * @date        Feb 19, 2014
     ************************************************************************/
    List<String> selectAllSupplierStatusName();

}
