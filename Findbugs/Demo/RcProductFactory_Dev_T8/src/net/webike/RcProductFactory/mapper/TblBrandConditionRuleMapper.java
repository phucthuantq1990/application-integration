/************************************************************************
 * File Name	： TblBrandConditionRuleMapper.java
 * Author		： Luong.Dai
 * Version		： 1.0.0
 * Date Created	： 2014/02/18
 * Date Updated	： 2014/02/18
 * Description	： Contain method to get data from database .
 *
 ***********************************************************************/
package net.webike.RcProductFactory.mapper;

import net.webike.RcProductFactory.entity.EntTblBrandConditionRule;
import net.webike.RcProductFactory.entity.EntTblFactoryProductNew;

/**
 * Interface TblBrandConditionRuleMapper.
 */
public interface TblBrandConditionRuleMapper {
    /************************************************************************
     * <b>Description:</b><br>
     *  replace your description for this method here.
     *
     * @author		Luong.Dai
     * @date		Feb 18, 2014
     * @param       product EntTblFactoryProductNew
     * @return		EntTblBrandConditionRule
     ************************************************************************/
    EntTblBrandConditionRule selectDefaultFlagOfProduct(EntTblFactoryProductNew product);

    /************************************************************************
     * <b>Description:</b><br>
     *  Select rult by brandCode and properPrice.
     *
     * @author		Luong.Dai
     * @date		Feb 18, 2014
     * @param       product EntTblFactoryProductNew
     * @return		EntTblBrandConditionRule
     ************************************************************************/
    EntTblBrandConditionRule selectRuleFlagByBrandCodeAndProperPrice(EntTblFactoryProductNew product);
}
