package net.webike.RcProductFactory.common.logging;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.apache.log4j.Logger;
import org.springframework.dao.DataAccessException;

/**
 * Interceptor for logging of service,dao.. in spring.
 * @author hoang.ho
 * @since 2014/01/07
 */
public class ContextLoggingInterceptor implements MethodInterceptor {

    /**
     * Logging of service,dao by Tiers name (ex: Dao, Service).
     * @param method method call.
     * @return Object
     * @throws Throwable exception when occur
     * @see org.aopalliance.intercept.MethodInterceptor#invoke(org.aopalliance.intercept.MethodInvocation)
     */
    public Object invoke(MethodInvocation method) throws Throwable {

        // Define logger
        Logger logger = Logger.getLogger(method.getMethod().getDeclaringClass());

        Object ret = null;
        String className = method.getMethod().getDeclaringClass().getCanonicalName();
        String methodName = method.getMethod().getName();

        // Write log start method.
        logger.debug(String.format("SERVICE-DAO : Start execute method [%s] in class [%s]", methodName, className));
        //System.out.println(String.format("SERVICE-DAO : Start execute method [%s] in class [%s]", methodName, className));
        try {
            ret = method.proceed();
        } catch (DataAccessException e) {
            logger.error(String.format("SERVICE-DAO : DataAccessException in method [%s] in class [%s] : %s", methodName, className, e.toString()));
        } catch (Exception e) {
            logger.error(String.format("SERVICE-DAO : Exception in method [%s] in class [%s] : %s", methodName, className, e.toString()));
        }

        logger.debug(String.format("SERVICE-DAO : End execute method [%s] in class [%s]", methodName, className));

        return ret;
    }

}
