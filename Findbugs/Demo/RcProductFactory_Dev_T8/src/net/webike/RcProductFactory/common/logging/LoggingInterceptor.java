package net.webike.RcProductFactory.common.logging;

import org.apache.log4j.Logger;
import org.springframework.dao.DataAccessException;

import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.Interceptor;

/**
 * Interceptor for logging of action in struts2.
 * @author hoang.ho
 * @since 2014/01/07
 */
public class LoggingInterceptor implements Interceptor {

    private static final long serialVersionUID = 1L;

    /**
     * Intercepter used for write log when execute method in action.
     * @return screenName
     * @param invocation method call.
     * @throws Exception Exception
     */
    public String intercept(ActionInvocation invocation) throws Exception {
        // Define logger
        Logger logger = Logger.getLogger(invocation.getAction().getClass());

        // Get class name and method name for write log.
        String className = invocation.getAction().getClass().getName();
        String methodName = invocation.getProxy().getMethod();

        // Write log start method.
        logger.debug(String.format("ACTION : Start execute method [%s] in class [%s]", methodName, className));

        // Define result return of screen.
        String result = "";

        // Call method and (write log when exception occur - if any)
        try {
            result = invocation.invoke();
        } catch (DataAccessException e) {
            logger.error(String.format("ACTION : DataAccessException in method [%s] in class [%s] : %s", methodName, className, e.toString()));
            result = "errorOther";
        } catch (Exception e) {
            logger.error(String.format("ACTION : Exception in method [%s] in class [%s] : %s", methodName, className, e.toString()));
            result = "errorOther";
        }

        // Write log end method.
        logger.debug(String.format("ACTION : End execute method [%s] in class [%s]", methodName, className));

        return result;
    }

    /**
     * Init logiin interceptors.
     */
    public void init() {
        System.out.println("Initializing MyLoggingInterceptor...");
    }
    /**
     * Destroy logiin interceptors.
     */
    public void destroy() {
        System.out.println("Destroying MyLoggingInterceptor...");
    }
}
