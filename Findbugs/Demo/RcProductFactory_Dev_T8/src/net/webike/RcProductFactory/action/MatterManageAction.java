/************************************************************************
 * file name	： MatterManageAction.java
 * author		： Le.Dinh
 * version		： 1.0.0
 * date created	： 17 Jan 2014
 * date updated	： 17 Jan 2014
 * description	： MatterManageAction
 *
 ***********************************************************************/
package net.webike.RcProductFactory.action;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import net.webike.RcProductFactory.action.form.MatterManageActionForm;
import net.webike.RcProductFactory.action.form.NewMasterActionForm;
import net.webike.RcProductFactory.entity.EntMstFactoryMatterNew;
import net.webike.RcProductFactory.entity.EntMstFactoryUserTmp;
import net.webike.RcProductFactory.entity.EnumRegistrationFlg;
import net.webike.RcProductFactory.exception.ExclusionException;
import net.webike.RcProductFactory.service.MatterManageService;
import net.webike.RcProductFactory.service.MatterService;
import net.webike.RcProductFactory.service.UserTmpService;

import org.apache.commons.lang.StringUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * class process all for matter manage page.
 */
@ParentPackage(value = "CommonInterceptor")
@Results(
	{
		@Result(name = "success", location = "matterManage.jsp"),
		// Get matter list.
		@Result(name = "list", type = "json"),
	    // Return error page when error other.
		@Result(name = "errorOther", type = "redirect", location = "error.html"),
		// Return error page when parameter fail
		//@Result(name = "error", type = "redirect", location = "error.html?error=param"),

		/* New matter dialog - Begin */
		@Result(name = "insertSuccess", type = "redirect", location = "matterManage.html"),
		/* New matter dialog - End */
	}
)
public class MatterManageAction extends CommonAction {
	private static final long serialVersionUID = -1601577291050503494L;

	@Autowired
	private MatterManageActionForm matterManageActionForm;
	@Autowired
	private MatterManageService matterManageService;

	private String page;
	private String pageSize;

	/* New matter dialog - Begin */
	@Autowired
	private NewMasterActionForm newMasterActionForm;
	@Autowired
	private UserTmpService userTmpService;
	@Autowired
	private MatterService matterService;

	// Used for checking exclusive problem.
	private EntMstFactoryMatterNew lastUpdatedUser;

	/* New matter dialog - End */

	@Override
	@Action("/matterManage")
	public String execute() {
		Map<String, String> listRegistrationFlg = EnumRegistrationFlg.getHashMapAllFlg();
		matterManageActionForm.setListRegistrationFlg(listRegistrationFlg);
		/* New matter dialog - Begin */
		// Validate login user.
		if (!super.validateLoginUser()) {
			return "errorOther";
		}

		// Get user list.
		List<EntMstFactoryUserTmp> lstUser = userTmpService.selectAllUser();
		userNameValueOfListEnt(lstUser);
		newMasterActionForm.setListAdmin(lstUser);
		this.getCommonForm().setListCommonUser(lstUser);
		//Get list user
		/* New matter dialog - End */

		return SUCCESS;
	}

	/**
	 * @return the matterManageActionForm
	 */
	public MatterManageActionForm getMatterManageActionForm() {
		return matterManageActionForm;
	}

	/**
	 * @param matterManageActionForm the matterManageActionForm to set
	 */
	public void setMatterManageActionForm(MatterManageActionForm matterManageActionForm) {
		this.matterManageActionForm = matterManageActionForm;
	}

	/**
	 * Get matter manage list.
	 * @return String
	 */
	@Action("/selectMatterManageList")
	public String selectMatterManageList() {
		try {
			int offset = Integer.parseInt(this.getRequest().getParameter("offset"));
			int numberOfRow = Integer.parseInt(this.getRequest().getParameter("numberOfRow"));
			List<EntMstFactoryMatterNew> matterManageList = matterManageService.selectMatterManageList(offset, numberOfRow);
			matterManageActionForm.setEntMstFactoryMatterList(matterManageList);
		} catch (NumberFormatException e) {
			this.getLogger().error("[MatterManageAction] An error execute function");
			this.getLogger().error(e.toString());
		} catch (SQLException e) {
			this.getLogger().error("[MatterManageAction] An error execute function");
			this.getLogger().error(e.toString());
		}
		return "list";
	}

	/**
	 * Select matter manage list.
	 * @author		nguyen.hieu
	 * @date		2014-01-18
	 * @return		Action name.
	 */
	@Action("/selectFilteredMatterManageList")
	public String selectFilteredMatterManageList() {
		try {
			//Check login userId
			if (!validateLoginUser()) {
                return "errorOther";
            }
		    // Check missing params pageSize and pageNumber
		    if (StringUtils.isEmpty(pageSize) || StringUtils.isEmpty(page)) {
		        // Return page error params
		        return "list";
		    }
			// Get data list.
			int pageSizeInt = Integer.parseInt(pageSize);
			int pageInt = Integer.parseInt(page);
			EntMstFactoryMatterNew entQuery = matterManageActionForm.getEntMstFactoryMatterNew();
			if (this.getCommonForm().getLoginAuthority().contains("LIMITED_USER_ID")) {
				entQuery.setMatterChargeUserId(this.getCommonForm().getLoginId());
			}
			List<EntMstFactoryMatterNew> filteredMatterManageList = matterManageService.selectFilteredMatterManageList(pageInt
			                                                                                                         , pageSizeInt
			                                                                                                         , entQuery);
			matterManageActionForm.setEntMstFactoryMatterList(filteredMatterManageList);

			// Get data count.
			int filteredMatterManageCount = matterManageService.selectFilteredMatterManageCount(matterManageActionForm.getEntMstFactoryMatterNew());
			matterManageActionForm.setEntMstFactoryMatterCount(filteredMatterManageCount);
		} catch (SQLException e) {
			this.getLogger().error("[MatterManageAction] An error execute function");
			this.getLogger().error(e.toString());
		}
		return "list";
	}

	/**
	 * @return the page
	 */
	public String getPage() {
		return page;
	}

	/**
	 * @param page the page to set
	 */
	public void setPage(String page) {
		this.page = page;
	}

	/**
	 * @return the pageSize
	 */
	public String getPageSize() {
		return pageSize;
	}

	/**
	 * @param pageSize the pageSize to set
	 */
	public void setPageSize(String pageSize) {
		this.pageSize = pageSize;
	}

	/**
	 * Delete matter list.
	 * @author		nguyen.hieu
	 * @date		2014-02-20
	 * @return		Action name.
	 */
	@Action("/deleteMatterList")
	public String deleteMatterList() {
		try {
			// Validate loginUser
			if (!super.validateLoginUser()) {
				return "errorOther";
			}

			/*BOE dang.nam 16/6/2014 add popupConfirm param*/
			String popupConfirm = this.getRequest().getParameter("popupConfirm");
			/*EOE dang.nam 16/6/2014 add popupConfirm param*/

			// Get arrayMatterId to List Int
			String arrayMatterId = this.getRequest().getParameter("arrayMatterId");
			if (StringUtils.isEmpty(arrayMatterId)) {
				return "errorOther";
			}
			String[] arrayMatters =  arrayMatterId.split("_");

			/*BOE Tran.Thanh 2014/05/06 : add select all checkbox */
			List<Long> matterNoList = new ArrayList<Long>();
			//List<String> updateOnList = new ArrayList<String>();

			/*
			//get dateTime filter when filter grid
			String dateTimeFilter = this.getRequest().getParameter("dateTimeFilter");

			//convert String dateTimeFilter to Date with format
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Date dateFilter = simpleDateFormat.parse(StringUtils.replace(dateTimeFilter, "T", " "));
			String strDateFilter = simpleDateFormat.format(dateFilter);
			Date dateFilterFormated = simpleDateFormat.parse(strDateFilter);
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");*/

			//Case select all and not select all
			if (arrayMatters[0].equals("selectAllCheckbox")) {
				//If select all, get entity filter and query to db
				EntMstFactoryMatterNew entQuery = matterManageActionForm.getEntMstFactoryMatterNew();
				if (this.getCommonForm().getLoginAuthority().contains("LIMITED_USER_ID")) {
					entQuery.setMatterChargeUserId(this.getCommonForm().getLoginId());
				}
				List<EntMstFactoryMatterNew> filteredMatterManageList
								= matterManageService.selectAllFilteredMatterManage(entQuery);
				//if list filter has record, add to matterNoList and updateOnList
				if (filteredMatterManageList.size() > 0) {
					for (EntMstFactoryMatterNew entMaster : filteredMatterManageList) {
						matterNoList.add(entMaster.getMatterNo());
						/*
						try {
							String updatedTimeFromDb = simpleDateFormat.format(entMaster.getUpdatedOn());
							Date date = simpleDateFormat.parse(updatedTimeFromDb);
							if (date.compareTo(dateFilterFormated) < 0) {
								updateOnList.add(simpleDateFormat.format(date));
							} else {
								throw new ExclusionException(entMaster.getMatterNo(), entMaster.getMatterName(),
										entMaster.getUpdatedUserId(), entMaster.getUpdatedOn());
							}
						} catch (ParseException ex) {
							this.getLogger().error("[MatterManageAction] An error execute function deleteMatterList");
							this.getLogger().error(ex.toString());
						}*/
					}
				} else {
					throw new ExclusionException(getText("message.noProductDeleted"));
				}
			} else {
				//Parse list matterNo to List Int
				for (int i = 0; i < arrayMatters.length; i++) {
					matterNoList.add(Long.parseLong(arrayMatters[i]));
				}

				// Get arrayUpdateOn to List String
				/*
				String arrayUpdateOn = this.getRequest().getParameter("arrayUpdateOn");
				if (StringUtils.isEmpty(arrayUpdateOn)) {
					return "errorOther";
				}
				String[] arrayUpdateDate =  arrayUpdateOn.split("_");
				//Parse list updateOn to List String
				for (int i = 0; i < arrayMatters.length; i++) {
					try {
						//StringUtils.replace(arrayUpdateDate[i], "T", " ");
						Date date = simpleDateFormat.parse(StringUtils.replace(arrayUpdateDate[i], "T", " "));
						updateOnList.add(simpleDateFormat.format(date));
					} catch (ParseException ex) {
						this.getLogger().error("[MatterManageAction] An error execute function");
						this.getLogger().error(ex.toString());
					}
				}*/
			}

			// Get user name login
			String user = this.getCommonForm().getLoginId();
			// Get matterChargeUserId from Form
			String matterChargeUserId = "";
			if (this.getCommonForm().getLoginAuthority().contains("LIMITED_USER_ID")) {
				matterChargeUserId = this.getCommonForm().getLoginId();
			}
			// deleteMatter by List Matter
			//matterService.deleteNewMatterNewList(matterNoList, user, updateOnList, matterChargeUserId);
			//boolean result = matterService.deleteNewMatterNewList(matterNoList, user, matterChargeUserId);

			/*BOE dang.nam 16/6/2014 add popupConfirm param*/
			boolean result = matterService.deleteNewMatterNewList(matterNoList, user, matterChargeUserId, popupConfirm);
			/*EOE dang.nam 16/6/2014 add popupConfirm param*/

			/*if (!result) {
				throw new ExclusionException(getText("message.noProductDeleted"));
			}*/

			/*BOE dang.nam 16/6/2014 check popup*/

			if ((!result) && (popupConfirm.equals("false"))) {
				throw new ExclusionException(getText("message.noProductDeleted"));
			}
			/*EOE dang.nam 16/6/2014 check popup*/

		} catch (SQLException e) {
			this.getLogger().error("[MatterManageAction] An error execute function");
			this.getLogger().error(e.toString());
		} catch (ExclusionException e) {
			lastUpdatedUser = new EntMstFactoryMatterNew();
			lastUpdatedUser.setErrorMessage(e.getMessageNoProduct());
			/*
			lastUpdatedUser = new EntMstFactoryMatterNew();
			lastUpdatedUser.setMatterNo(e.getMatterNo());
			lastUpdatedUser.setUpdatedOn(e.getUpdatedDate());
			lastUpdatedUser.setMatterName(e.getMatterName());
			lastUpdatedUser.setUpdatedUserId(e.getUpdatedUserId());
			EntMstFactoryUserTmp user = userTmpService.selectUserById(e.getUpdatedUserId());
	        if (user != null) {
	            lastUpdatedUser.setManagerName(user.getUserLastName() + " " + user.getUserFirstName());
	        }*/
		} catch (Exception e) {
			this.getLogger().error("[MatterManageAction] An error execute function");
			this.getLogger().error(e.toString());
		}
		/*EOE Tran.Thanh 2014/05/06 : add select all checkbox */
		return "list";
	}

	/* New matter dialog - Begin */
	/************************************************************************
	 * <b>Description:</b><br>
	 * combine last name and first name to create userName. Format:lastName" "firstName
	 *
	 * @author		Luong.Dai
	 * @date		Nov 15, 2013
	 * @param listUser List<EntMstFactoryUser>
	 ************************************************************************/
	private void userNameValueOfListEnt(List<EntMstFactoryUserTmp> listUser) {
		int size = listUser.size();

		for (int i = 0; i < size; i++) {
			EntMstFactoryUserTmp entUser = listUser.get(i);

			// userName = [last name][1 space 1byte][first name]
			entUser.setUserName(entUser.getUserLastName() + " " + entUser.getUserFirstName());
		}
	}

	/************************************************************************
	 * <b>Description:</b><br>
	 *  Add new matter new to database.
	 *
	 * @author		Luong.Dai
	 * @date		Nov 7, 2013
	 * @return	  String
	 * @throws Exception		String
	 ************************************************************************/
	@Action("/createNewMatter")
	public String createNewMatter() throws Exception {
		try {
			// Validate loginUser
			if (!super.validateLoginUser()) {
				return "errorOther";
			}

			// Check null or empty params matterName and changeUser
			// If params is null, return to error page
			if (StringUtils.isEmpty(this.getRequest().getParameter("matterName").trim())
					|| StringUtils.isEmpty(this.getRequest().getParameter("changeUser").trim())
					|| StringUtils.isEmpty(this.getRequest().getParameter("redmineIssueId").trim())) {
				return "errorInvalidParam";
			}

			// Get params and set value into entFactoryMatter entity
			EntMstFactoryMatterNew entFactoryMatterNew = createInsertParams();

			// Insert value to database
			matterService.insertNewMatterNew(entFactoryMatterNew);
		} catch (Exception e) {
			//Catch error insert false.
			this.getLogger().error("[NewMatterAction] An error to insert new matter at"
									+ "addNewMatter() function");
			this.getLogger().error(e.toString());
		}
		return "list";
	}

	public NewMasterActionForm getForm() {
		return newMasterActionForm;
	}

	public void setForm(NewMasterActionForm form) {
		this.newMasterActionForm = form;
	}

	public EntMstFactoryMatterNew getLastUpdatedUser() {
		return lastUpdatedUser;
	}

	/************************************************************************
	 * <b>Description:</b><br>
	 *  Get params from request and set value to entity.
	 *
	 * @author		Luong.Dai
	 * @date		Nov 7, 2013
	 * @return		EntMstFactoryMatter
	 ************************************************************************/
	private EntMstFactoryMatterNew createInsertParams() {
		// Get user name login
		String user = this.getCommonForm().getLoginId();

		// Set value to entity
		EntMstFactoryMatterNew factoryMatterNew = new EntMstFactoryMatterNew();
		factoryMatterNew.setMatterRedmineIssueId(0);
		factoryMatterNew.setMatterName(this.getRequest().getParameter("matterName"));
		factoryMatterNew.setMatterRedmineIssueId(Integer.parseInt(getRequest().getParameter("redmineIssueId")));
		factoryMatterNew.setMatterChargeUserId(this.getRequest().getParameter("changeUser"));

		// Set appropriate values here.
		factoryMatterNew.setMatterFactoryCount(0);
		factoryMatterNew.setMatterFactoryErrorCountGeneral(0);
		factoryMatterNew.setMatterFactoryErrorCountCategory(0);
		factoryMatterNew.setMatterFactoryErrorCountModel(0);
		factoryMatterNew.setMatterFactoryErrorCountAttribute(0);
		factoryMatterNew.setMatterRegistrationFlg(0);
		factoryMatterNew.setMatterDelFlg(0);

		factoryMatterNew.setCreatedUserId(user);
		factoryMatterNew.setUpdatedUserId(user);

		return factoryMatterNew;
	}
	/* New matter dialog - End */
}
