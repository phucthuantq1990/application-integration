/************************************************************************
 * File Name    ： ProductFitModel.java
 * Author       ： Nguyen.Chuong
 * Version      ： 1.0.0
 * Date Created ： 2014/01/22
 * Description  ： product fit model action form.
 *
 ***********************************************************************/
package net.webike.RcProductFactory.action.form;

import java.util.List;

import net.webike.RcProductFactory.entity.EntMstFactoryMatter;
import net.webike.RcProductFactory.entity.EntMstModel;
import net.webike.RcProductFactory.entity.EntTblFactoryProductFitModel;
import net.webike.RcProductFactory.entity.EntTblFactoryProductImage;
import net.webike.RcProductFactory.entity.EntTblFactoryProductNew;
import net.webike.RcProductFactory.util.Constant;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * product fit model action form.
 * @author Nguyen.Chuong
 * Date Created ： 2014/01/22
 */
@Component
@Scope("request")
public class FitModelCheckActionForm {
    //--bottom grid
    private EntTblFactoryProductNew entTblFactoryProductNew;
    private int count = 0;
    // Json for bottom grid:
    private String listBottomGridJsonStr;
    //--end bottom grid
    //List image of product
    private List<EntTblFactoryProductImage> listImage;
    //Link no image from constant
    private String noImageURL = Constant.NO_IMAGE_URL;
    //List mstModel in DB
    private List<EntMstModel> listMstModel;
    //List fitModel of product
    private List<EntTblFactoryProductFitModel> listFitModelOfProduct;
    //List product of matter
    private List<EntTblFactoryProductNew> listProduct;
    //Object factory matter
    private EntMstFactoryMatter factoryMatter;
    //List fit model
    private List<EntTblFactoryProductFitModel> listFitModel;
    //Object product
    private EntTblFactoryProductNew product;
    // Json string of list fit model.
    private String listFitModelJsonStr;
    // Json string og List mstModel in DB
    private String listModelJsonStr;
    // FitModel to Inser FitModel By MultiProduct
    private EntTblFactoryProductFitModel fitModel;
    // EntMstModel to filter
    private EntMstModel entMstModel;
    // return message code. 0: fail, 1: success
    private Integer msgCode;
    // msg String
    private String msgString;
    // list value of auto complete;
    private List<String> autoCompleteList;
    // filed name of auto complete;
    private String autoCompleteField;
    // total count of value mst model .
    private int mstModelListCount;
    /**
     * @return the listImage
     */
    public List<EntTblFactoryProductImage> getListImage() {
        return listImage;
    }
    /**
     * @param listImage the listImage to set
     */
    public void setListImage(List<EntTblFactoryProductImage> listImage) {
        this.listImage = listImage;
    }
    /**
     * @return the noImageURL
     */
    public String getNoImageURL() {
        return noImageURL;
    }
    /**
     * @param noImageURL the noImageURL to set
     */
    public void setNoImageURL(String noImageURL) {
        this.noImageURL = noImageURL;
    }
    /**
     * @return the listMstModel
     */
    public List<EntMstModel> getListMstModel() {
        return listMstModel;
    }
    /**
     * @param listMstModel the listMstModel to set
     */
    public void setListMstModel(List<EntMstModel> listMstModel) {
        this.listMstModel = listMstModel;
    }
    /**
     * @return the listFitModelOfProduct
     */
    public List<EntTblFactoryProductFitModel> getListFitModelOfProduct() {
        return listFitModelOfProduct;
    }
    /**
     * @param listFitModelOfProduct the listFitModelOfProduct to set
     */
    public void setListFitModelOfProduct(
            List<EntTblFactoryProductFitModel> listFitModelOfProduct) {
        this.listFitModelOfProduct = listFitModelOfProduct;
    }
    /**
     * @return the listProduct
     */
    public List<EntTblFactoryProductNew> getListProduct() {
        return listProduct;
    }
    /**
     * @param listProduct the listProduct to set
     */
    public void setListProduct(List<EntTblFactoryProductNew> listProduct) {
        this.listProduct = listProduct;
    }
    /**
     * @return the factoryMatter
     */
    public EntMstFactoryMatter getFactoryMatter() {
        return factoryMatter;
    }
    /**
     * @param factoryMatter the factoryMatter to set
     */
    public void setFactoryMatter(EntMstFactoryMatter factoryMatter) {
        this.factoryMatter = factoryMatter;
    }
    /**
     * @return the listFitModel
     */
    public List<EntTblFactoryProductFitModel> getListFitModel() {
        return listFitModel;
    }
    /**
     * @param listFitModel the listFitModel to set
     */
    public void setListFitModel(List<EntTblFactoryProductFitModel> listFitModel) {
        this.listFitModel = listFitModel;
    }
    /**
     * @return the product
     */
    public EntTblFactoryProductNew getProduct() {
        return product;
    }
    /**
     * @param product the product to set
     */
    public void setProduct(EntTblFactoryProductNew product) {
        this.product = product;
    }
    /**
     * @return the listFitModelJsonStr
     */
    public String getListFitModelJsonStr() {
        return listFitModelJsonStr;
    }
    /**
     * @param listFitModelJsonStr the listFitModelJsonStr to set
     */
    public void setListFitModelJsonStr(String listFitModelJsonStr) {
        this.listFitModelJsonStr = listFitModelJsonStr;
    }
    /**
     * @return the listModelJsonStr
     */
    public String getListModelJsonStr() {
        return listModelJsonStr;
    }
    /**
     * @param listModelJsonStr the listModelJsonStr to set
     */
    public void setListModelJsonStr(String listModelJsonStr) {
        this.listModelJsonStr = listModelJsonStr;
    }
    public String getListBottomGridJsonStr() {
        return listBottomGridJsonStr;
    }
    public void setListBottomGridJsonStr(String listBottomGridJsonStr) {
        this.listBottomGridJsonStr = listBottomGridJsonStr;
    }
    public EntTblFactoryProductNew getEntTblFactoryProductNew() {
        return entTblFactoryProductNew;
    }
    public void setEntTblFactoryProductNew(EntTblFactoryProductNew entTblFactoryProductNew) {
        this.entTblFactoryProductNew = entTblFactoryProductNew;
    }
    public int getCount() {
        return count;
    }
    public void setCount(int count) {
        this.count = count;
    }
	/**
	 * @return the fitModel
	 */
	public EntTblFactoryProductFitModel getFitModel() {
		return fitModel;
	}
	/**
	 * @param fitModel the fitModel to set
	 */
	public void setFitModel(EntTblFactoryProductFitModel fitModel) {
		this.fitModel = fitModel;
	}
    /**
     * @return the msgCode
     */
    public Integer getMsgCode() {
        return msgCode;
    }
    /**
     * @param msgCode the msgCode to set
     */
    public void setMsgCode(Integer msgCode) {
        this.msgCode = msgCode;
    }
    /**
     * @return the msgString
     */
    public String getMsgString() {
        return msgString;
    }
    /**
     * @param msgString the msgString to set
     */
    public void setMsgString(String msgString) {
        this.msgString = msgString;
    }
    /**
     * @return the entMstModel
     */
    public EntMstModel getEntMstModel() {
        return entMstModel;
    }
    /**
     * @param entMstModel the entMstModel to set
     */
    public void setEntMstModel(EntMstModel entMstModel) {
        this.entMstModel = entMstModel;
    }
    /**
     * @return the autoCompleteList
     */
    public List<String> getAutoCompleteList() {
        return autoCompleteList;
    }
    /**
     * @param autoCompleteList the autoCompleteList to set
     */
    public void setAutoCompleteList(List<String> autoCompleteList) {
        this.autoCompleteList = autoCompleteList;
    }
    /**
     * @return the autoCompleteField
     */
    public String getAutoCompleteField() {
        return autoCompleteField;
    }
    /**
     * @param autoCompleteField the autoCompleteField to set
     */
    public void setAutoCompleteField(String autoCompleteField) {
        this.autoCompleteField = autoCompleteField;
    }
    /**
     * @return the mstModelListCount
     */
    public int getMstModelListCount() {
        return mstModelListCount;
    }
    /**
     * @param mstModelListCount the mstModelListCount to set
     */
    public void setMstModelListCount(int mstModelListCount) {
        this.mstModelListCount = mstModelListCount;
    }
}
