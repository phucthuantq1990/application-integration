/************************************************************************
 * File Name	： brandMasterManagerAction.java
 * Author		： Nguyen.Chuong
 * Version		： 1.0.0
 * Date Created	： 2013/12/27
 * Date Updated	： 2013/12/27
 * Description	： brand master manager for brandMasterManager page.
 *
 ***********************************************************************/
package net.webike.RcProductFactory.action;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import net.webike.RcProductFactory.action.form.BrandMasterManagerActionForm;
import net.webike.RcProductFactory.entity.EntMstBrand;
import net.webike.RcProductFactory.entity.EntMstFactoryUserTmp;
import net.webike.RcProductFactory.service.BrandService;
import net.webike.RcProductFactory.service.UserTmpService;
import net.webike.RcProductFactory.util.Constant;

import org.apache.commons.lang.StringUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.apache.struts2.interceptor.SessionAware;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;

/**
 * brandMasterManagerAction.
 */
@Scope("request")
@ParentPackage(value = "CommonInterceptor")
@Results(
    {
    	// On loading default matter detail page
        @Result(name = "success", location = "brandMasterManage.jsp"),
        //Return json
        @Result(name = "list", type = "json"),
        // Return error page when parameter fail
        @Result(name = "error", type = "redirect", location = "error.html?error=param"),
        // Return error page when parameter fail
        @Result(name = "errorNoPermission", type = "redirect", location = "error.html?error=noPermission"),
        // Return error page when error other.
        @Result(name = "errorOther", type = "redirect", location = "error.html")
    }
)
public class BrandMasterManagerAction extends CommonAction implements SessionAware {
    /*BOE Thai.Son 2014/01/17*/
    private Map<String, Object> session;
    /*EOE Thai.Son 2014/01/17*/
	/**
	 * @description replace your description for this field here
	 */
	private static final long serialVersionUID = 1L;
	private List<EntMstBrand> listBrand;
	//Total record of filter
	private int count = 0;
    // Number product in 1 page
    private String page;
    // Page number to get product data
    private String pageSize;
    // Host address
    private String hostAddress;
    // msgSuccessWhenEditBrand
    private String msgSuccessWhenEditBrand = "";
	/**
     * Matter Detail Action Form.
     */
    @Autowired
    private BrandMasterManagerActionForm brandMasterManagerActionForm;

    @Autowired
    private BrandService brandService;
    @Autowired
    private UserTmpService userTmpService;

    @Override
    @Action("/brandMasterManager")
    public String execute() {
        try {
            //Validate loginUser
            if (!validateLoginUser()) {
                return "errorOther";
            }
            //Get Request url
            String url = this.getRequest().getRequestURL().toString();
            /* BOE process when edit brand succes Le.Dinh 2014/01/16 #B07 */
            /*BOE  Thai.Son 2014/01/17*/
            //msgSuccessWhenEditBrand = this.getRequest().getParameter("msgSuccessWhenEditBrand");
            msgSuccessWhenEditBrand = (String) session.get(Constant.SESSION_MSG_CODE);
            session.remove(Constant.SESSION_MSG_CODE);
            /*EOE  Thai.Son 2014/01/17 */
            /* EOE process when edit brand succes */
            if (StringUtils.isNotEmpty(url)) {
                //Remove URI
                int idx = url.lastIndexOf("/");
                hostAddress = url.substring(0, idx);
            }
            List<EntMstFactoryUserTmp> lstUser = userTmpService.selectAllUser();
            this.getCommonForm().setListCommonUser(lstUser);
        } catch (Exception e) {
            this.getLogger().error("[BrandMasterManagerAction] An error execute function");
            this.getLogger().error(e.toString());
            return "errorOther";
        }
        return SUCCESS;
    }

    /************************************************************************
     * <b>Description:</b><br>
     *  load list brand for brandMasterManger page with filter.
     *
     * @author		Nguyen.Chuong
     * @date		Dec 27, 2013
     * @return		String
     ************************************************************************/
    @Action("/brandMasterManagerFilter")
    public String brandMasterManagerFilter() {
        try {
            // Check missing params pageSize and pageNumber
            if (StringUtils.isEmpty(pageSize) || StringUtils.isEmpty(page)) {
                // Return page error params
                return "list";
            }
            int pageSizeInt = Integer.parseInt(pageSize);
            int pageInt = Integer.parseInt(page);

            //Get listAllBrand for init page
            listBrand = brandService.selectListBrand(pageSizeInt, pageInt, brandMasterManagerActionForm.getEntMstBrand());
            brandMasterManagerActionForm.setListBrand(listBrand);
            //Check list of brand
            count = brandService.selectTotalRecordOfFilter(brandMasterManagerActionForm.getEntMstBrand());
        } catch (SQLException sqlEx) {
            this.getLogger().error("[BrandMasterManagerAction] An error execute function");
            this.getLogger().error(" An error SQLException");
            this.getLogger().error(sqlEx.toString());
            return "errorOther";
        } catch (Exception e) {
            this.getLogger().error("[BrandMasterManagerAction] An error execute function");
            this.getLogger().error(e.toString());
            return "errorOther";
        }
        return "list";
    }

    /**
     * @return the listBrand
     */
    public List<EntMstBrand> getListBrand() {
        return listBrand;
    }

    /**
     * @param listBrand the listBrand to set
     */
    public void setListBrand(List<EntMstBrand> listBrand) {
        this.listBrand = listBrand;
    }

    /**
     * @return the brandMasterManagerActionForm
     */
    public BrandMasterManagerActionForm getBrandMasterManagerActionForm() {
        return brandMasterManagerActionForm;
    }

    /**
     * @param brandMasterManagerActionForm the brandMasterManagerActionForm to set
     */
    public void setBrandMasterManagerActionForm(
            BrandMasterManagerActionForm brandMasterManagerActionForm) {
        this.brandMasterManagerActionForm = brandMasterManagerActionForm;
    }

    /**
     * @return the count
     */
    public int getCount() {
        return count;
    }

    /**
     * @param count the count to set
     */
    public void setCount(int count) {
        this.count = count;
    }

    /**
     * @return the page
     */
    public String getPage() {
        return page;
    }

    /**
     * @param page the page to set
     */
    public void setPage(String page) {
        this.page = page;
    }

    /**
     * @return the pageSize
     */
    public String getPageSize() {
        return pageSize;
    }

    /**
     * @param pageSize the pageSize to set
     */
    public void setPageSize(String pageSize) {
        this.pageSize = pageSize;
    }

    /**
     * @return the hostAddress
     */
    public String getHostAddress() {
        return hostAddress;
    }

    /**
     * @param hostAddress the hostAddress to set
     */
    public void setHostAddress(String hostAddress) {
        this.hostAddress = hostAddress;
    }

	public String getMsgSuccessWhenEditBrand() {
		return msgSuccessWhenEditBrand;
	}

	public void setMsgSuccessWhenEditBrand(String msgSuccessWhenEditBrand) {
		this.msgSuccessWhenEditBrand = msgSuccessWhenEditBrand;
	}

	/*BOE  Thai.Son 2014/01/17*/
    @Override
    public void setSession(Map<String, Object> session) {
        this.session = session;
    }
}
