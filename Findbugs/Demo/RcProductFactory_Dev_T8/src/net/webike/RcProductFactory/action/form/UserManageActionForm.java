/************************************************************************
 * File Name	： UserManageActionForm.java
 * Author		： Tran.Thanh
 * Version		： 1.0.0
 * Date Created	： 2013/10/02
 * Date Updated	： 2013/10/02
 * Description	： contain data userManage page.
 *
 ***********************************************************************/
package net.webike.RcProductFactory.action.form;

import java.util.List;
import java.util.TreeMap;

import net.webike.RcProductFactory.entity.EntMstFactoryUserTmp;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * UserManageActionForm.
 */
@Component
@Scope("session")
public class UserManageActionForm {

	/**
	 * @listAllUser list all users
	 */
	private List<EntMstFactoryUserTmp> listAllUser;

	/**---------------------------------------------------------------------------.
     * @description hashmap contains all values of authority.
     */
    private TreeMap<String, String> authorityTreeMap;

    /**---------------------------------------------------------------------------.
     * @description hashmap contains all values of status.
     */
    private TreeMap<String, String> statusTreeMap;

	/**
	 * @return the authorityTreeMap
	 */
	public TreeMap<String, String> getAuthorityTreeMap() {
		return authorityTreeMap;
	}

	/**
	 * @param authorityTreeMap the authorityTreeMap to set
	 */
	public void setAuthorityTreeMap(TreeMap<String, String> authorityTreeMap) {
		this.authorityTreeMap = authorityTreeMap;
	}

	/**
	 * @return the statusTreeMap
	 */
	public TreeMap<String, String> getStatusTreeMap() {
		return statusTreeMap;
	}

	/**
	 * @param statusTreeMap the statusTreeMap to set
	 */
	public void setStatusTreeMap(TreeMap<String, String> statusTreeMap) {
		this.statusTreeMap = statusTreeMap;
	}

	/**
     * @return listAllUser
     */
	public List<EntMstFactoryUserTmp> getListAllUser() {
		return listAllUser;
	}

	/**
     * @param listAllUser list all users.
     */
	public void setListAllUser(List<EntMstFactoryUserTmp> listAllUser) {
		this.listAllUser = listAllUser;
	}
 }
