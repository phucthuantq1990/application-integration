/* Copyright (c) 2012 Rivercrane.All Rights Reserved. */

package net.webike.RcProductFactory.action.form;

import java.util.List;

import net.webike.RcProductFactory.entity.EntMstFactoryMatterStatus;
import net.webike.RcProductFactory.entity.EntMstFactoryUserTmp;
import net.webike.RcProductFactory.util.Constant;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Common from.
 */
@Component
@Scope("prototype")
public class CommonForm {
    //Default redirect page
    private String defaultRedirectPage = Constant.DEFAULT_REDIRECT_PAGE;
    //Default error page
    private String defaultErrorPage = Constant.DEFAULT_ERROR_PAGE;

    private String loginId;
    private String loginFirstName;
    private String loginLastName;
    //private String loginAuthority;
    private List<String> loginAuthority;
    private String errorMessage;
    private String successMessage;
    private List<EntMstFactoryMatterStatus> listCommonStatus;
    //private List<EntMstFactoryUser> listCommonUser;
    private List<EntMstFactoryUserTmp> listCommonUser;
    private String apiAccessKey;
    private String passWordCredentials;

    /**
     * @return loginId
     */
    public String getLoginId() {
        return loginId;
    }

    /**
     * @param loginId Login user id
     */
    public void setLoginId(String loginId) {
        this.loginId = loginId;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    /**
     * @return the loginFirstName
     */
    public String getLoginFirstName() {
        return loginFirstName;
    }

    /**
     * @param loginFirstName the loginFirstName to set
     */
    public void setLoginFirstName(String loginFirstName) {
        this.loginFirstName = loginFirstName;
    }

    /**
     * @return the loginLastName
     */
    public String getLoginLastName() {
        return loginLastName;
    }

    /**
     * @param loginLastName the loginLastName to set
     */
    public void setLoginLastName(String loginLastName) {
        this.loginLastName = loginLastName;
    }

    /**
     * @return the loginAuthority
     */
//    public String getLoginAuthority() {
//        return loginAuthority;
//    }

    /**
     * @param loginAuthority the loginAuthority to set
     */
//    public void setLoginAuthority(String loginAuthority) {
//        this.loginAuthority = loginAuthority;
//    }

    /**
     * @return the listCommonStatus
     */
    public List<EntMstFactoryMatterStatus> getListCommonStatus() {
        return listCommonStatus;
    }

    /**
     * @param listCommonStatus the listCommonStatus to set
     */
    public void setListCommonStatus(List<EntMstFactoryMatterStatus> listCommonStatus) {
        this.listCommonStatus = listCommonStatus;
    }

    /**
	 * @return the listCommonUser
	 */
	public List<EntMstFactoryUserTmp> getListCommonUser() {
		return listCommonUser;
	}

	/**
	 * @param listCommonUser the listCommonUser to set
	 */
	public void setListCommonUser(List<EntMstFactoryUserTmp> listCommonUser) {
		this.listCommonUser = listCommonUser;
	}

	/**
     * @return the successMessage
     */
    public String getSuccessMessage() {
        return successMessage;
    }

    /**
     * @param successMessage the successMessage to set
     */
    public void setSuccessMessage(String successMessage) {
        this.successMessage = successMessage;
    }

    /**
     * @return the defaultRedirectPage
     */
    public String getDefaultRedirectPage() {
        return defaultRedirectPage;
    }

    /**
     * @param defaultRedirectPage the defaultRedirectPage to set
     */
    public void setDefaultRedirectPage(String defaultRedirectPage) {
        this.defaultRedirectPage = defaultRedirectPage;
    }

    /**
     * @return the defaultErrorPage
     */
    public String getDefaultErrorPage() {
        return defaultErrorPage;
    }

    /**
     * @param defaultErrorPage the defaultErrorPage to set
     */
    public void setDefaultErrorPage(String defaultErrorPage) {
        this.defaultErrorPage = defaultErrorPage;
    }

	/**
	 * @return the loginAuthority
	 */
	public List<String> getLoginAuthority() {
		return loginAuthority;
	}

	/**
	 * @param loginAuthority the loginAuthority to set
	 */
	public void setLoginAuthority(List<String> loginAuthority) {
		this.loginAuthority = loginAuthority;
	}

	/**
	 * @return the apiAccessKey
	 */
	public String getApiAccessKey() {
		return apiAccessKey;
	}

	/**
	 * @param apiAccessKey the apiAccessKey to set
	 */
	public void setApiAccessKey(String apiAccessKey) {
		this.apiAccessKey = apiAccessKey;
	}

	/**
	 * @return the passWordCredentials
	 */
	public String getPassWordCredentials() {
		return passWordCredentials;
	}

	/**
	 * @param passWordCredentials the passWordCredentials to set
	 */
	public void setPassWordCredentials(String passWordCredentials) {
		this.passWordCredentials = passWordCredentials;
	}

}
