/************************************************************************
 * File Name    ： ErrorAction.java
 * Author       ： Nguyen.Chuong
 * Version      ： 1.0.0
 * Date Created ： 2013/09/24
 * Date Updated ： 2013/09/24
 * Description  ： error action to control error.
 *
 ***********************************************************************/
package net.webike.RcProductFactory.action;

import net.webike.RcProductFactory.util.Constant;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 * Error Action.
 */
@Controller
@Scope("request")
@ParentPackage(value = "CommonInterceptor")
@Results(
    {
        @Result(name = "error", location = "error.jsp")
    }
)
public class ErrorAction extends CommonAction {

    /**
	 * @serialVersionUID serialVersion
	 */
	private static final long serialVersionUID = 1L;

	@Override
    @Action("/error")
    public String execute() throws Exception {
        String errorParam = this.getRequest().getParameter("error");
        if (Constant.ERROR_ERROR_PARAM.equals(errorParam)) {
            this.getCommonForm().setErrorMessage(getText("message.error.invalidParam"));
        } else if (Constant.ERROR_403.equals(errorParam)) {
            this.getCommonForm().setErrorMessage(getText("message.error.403"));
        } else if (Constant.ERROR_404.equals(errorParam)) {
            this.getCommonForm().setErrorMessage(getText("message.error.404"));
        } else if (Constant.ERROR_NO_PERMISSION.equals(errorParam)) {
            this.getCommonForm().setErrorMessage(getText("message.error.noPermission"));
        } else if (Constant.ERROR_NO_PERMISSION_EXCUTE.equals(errorParam)) {
            this.getCommonForm().setErrorMessage(getText("message.error.noPermissionExcute"));
        } else if (Constant.ERROR_NO_DATA.equals(errorParam)) {
            this.getCommonForm().setErrorMessage(getText("message.error.noData"));
        } else if (Constant.ERROR_SYSTEM_ERROR.equals(errorParam)) {
        	this.getCommonForm().setErrorMessage(getText("message.error.errorSystem"));
        } else {
            this.getCommonForm().setErrorMessage(getText("message.error.other"));
        }
        return "error";
    }

}
