/************************************************************************
 * File Name    ： AttributeEditActionForm.java
 * Author       ： Long Vu
 * Version      ： 1.0.0
 * Date Created ： 2014/01/15
 * Date Updated ： 2014/01/15
 * Description  ： attribute edit action form.
 *
 ***********************************************************************/
package net.webike.RcProductFactory.action.form;

import java.util.List;
import java.util.Map;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import net.webike.RcProductFactory.entity.EntMstAttribute;
import net.webike.RcProductFactory.entity.EntMstAttributeGroup;

/**
 * attribute edit action form.
 * @author Long Vu
 * Date Created ： 2014/01/15
 */
@Component
@Scope("request")
public class AttributeActionForm {

    private EntMstAttribute attribute;
    private List<EntMstAttributeGroup> attributeGroupList;
    /** mode for check insert mode. 0: udpate, 1: insert */
    private Integer insertMode;
    private String userId;
    private List<EntMstAttributeGroup> oldAttrGroupList;
    private EntMstAttribute oldAttr;

    //List of attribute.
    private List<EntMstAttribute> listAttribute;

    //List type enum.
    private Map<String, String> listType;
    //Total record of filter
    private int count = 0;

    // data for insert, update and delete in attribute_group table.
    private List<EntMstAttributeGroup> attrGroupUpdatedList;
    private List<EntMstAttributeGroup> attrGroupInsertdList;
    private String deletedCodeList;

    // data for showing error msg.
    /** error msg code. 0: updateError, 1: insertError, 2: edit by other */
    private String msgCode;
    private String errorMsg;
    /** object to store the updated user and time of update in the mode of updating by other error.*/
    private EntMstAttribute updatedAttr;
    // json for LstAttributeGroup.
    private String jsonLstAttributeGroup;
    /**
     * @return the attribute
     */
    public EntMstAttribute getAttribute() {
        return attribute;
    }
    /**
     * @param attribute the attribute to set
     */
    public void setAttribute(EntMstAttribute attribute) {
        this.attribute = attribute;
    }
    /**
     * @return the attributeGroupList
     */
    public List<EntMstAttributeGroup> getAttributeGroupList() {
        return attributeGroupList;
    }
    /**
     * @param attributeGroupList the attributeGroupList to set
     */
    public void setAttributeGroupList(List<EntMstAttributeGroup> attributeGroupList) {
        this.attributeGroupList = attributeGroupList;
    }
    /**
     * @return the insertMode
     */
    public Integer getInsertMode() {
        return insertMode;
    }
    /**
     * @param insertMode the insertMode to set
     */
    public void setInsertMode(Integer insertMode) {
        this.insertMode = insertMode;
    }
    /**
     * @return the listAttribute
     */
    public List<EntMstAttribute> getListAttribute() {
        return listAttribute;
    }
    /**
     * @param listAttribute the listAttribute to set
     */
    public void setListAttribute(List<EntMstAttribute> listAttribute) {
        this.listAttribute = listAttribute;
    }
    /**
     * @return the listType
     */
    public Map<String, String> getListType() {
        return listType;
    }
    /**
     * @param listType the listType to set
     */
    public void setListType(Map<String, String> listType) {
        this.listType = listType;
    }
    /**
     * @return the attrGroupUpdatedList
     */
    public List<EntMstAttributeGroup> getAttrGroupUpdatedList() {
        return attrGroupUpdatedList;
    }
    /**
     * @param attrGroupUpdatedList the attrGroupUpdatedList to set
     */
    public void setAttrGroupUpdatedList(List<EntMstAttributeGroup> attrGroupUpdatedList) {
        this.attrGroupUpdatedList = attrGroupUpdatedList;
    }
    /**
     * @return the attrGroupInsertdList
     */
    public List<EntMstAttributeGroup> getAttrGroupInsertdList() {
        return attrGroupInsertdList;
    }
    /**
     * @param attrGroupInsertdList the attrGroupInsertdList to set
     */
    public void setAttrGroupInsertdList(List<EntMstAttributeGroup> attrGroupInsertdList) {
        this.attrGroupInsertdList = attrGroupInsertdList;
    }
    /**
     * @return the deletedCodeList
     */
    public String getDeletedCodeList() {
        return deletedCodeList;
    }
    /**
     * @param deletedCodeList the deletedCodeList to set
     */
    public void setDeletedCodeList(String deletedCodeList) {
        this.deletedCodeList = deletedCodeList;
    }
    /**
     * @return the userId
     */
    public String getUserId() {
        return userId;
    }
    /**
     * @param userId the userId to set
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }
    /**
     * @return the oldAttrGroupList
     */
    public List<EntMstAttributeGroup> getOldAttrGroupList() {
        return oldAttrGroupList;
    }
    /**
     * @param oldAttrGroupList the oldAttrGroupList to set
     */
    public void setOldAttrGroupList(List<EntMstAttributeGroup> oldAttrGroupList) {
        this.oldAttrGroupList = oldAttrGroupList;
    }
    /**
     * @return the oldAttr
     */
    public EntMstAttribute getOldAttr() {
        return oldAttr;
    }
    /**
     * @param oldAttr the oldAttr to set
     */
    public void setOldAttr(EntMstAttribute oldAttr) {
        this.oldAttr = oldAttr;
    }
    /**
     * @return the jsonLstAttributeGroup
     */
    public String getJsonLstAttributeGroup() {
        return jsonLstAttributeGroup;
    }
    /**
     * @param jsonLstAttributeGroup the jsonLstAttributeGroup to set
     */
    public void setJsonLstAttributeGroup(String jsonLstAttributeGroup) {
        this.jsonLstAttributeGroup = jsonLstAttributeGroup;
    }

    /**
     * @return the msgCode
     */
    public String getMsgCode() {
        return msgCode;
    }
    /**
     * @param msgCode the msgCode to set
     */
    public void setMsgCode(String msgCode) {
        this.msgCode = msgCode;
    }
    /**
     * @return the errorMsg
     */
    public String getErrorMsg() {
        return errorMsg;
    }
    /**
     * @param errorMsg the errorMsg to set
     */
    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }
    /**
     * @return the updatedAttr
     */
    public EntMstAttribute getUpdatedAttr() {
        return updatedAttr;
    }
    /**
     * @param updatedAttr the updatedAttr to set
     */
    public void setUpdatedAttr(EntMstAttribute updatedAttr) {
        this.updatedAttr = updatedAttr;
    }
    /**
     * @return the count
     */
    public int getCount() {
        return count;
    }
    /**
     * @param count the count to set
     */
    public void setCount(int count) {
        this.count = count;
    }
}
