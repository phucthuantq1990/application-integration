/************************************************************************
 * File Name    ： JSonAction.java
 * Author        ： Nguyen.Chuong
 * Version        ： 1.0.0
 * Date Created    ： 2013/09/24
 * Date Updated    ： 2013/09/24
 * Description    ： JSonAction for get json from ajax.
 *
 ***********************************************************************/
package net.webike.RcProductFactory.action;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

import net.webike.RcProductFactory.service.SettingService;
import net.webike.RcProductFactory.util.Constant;
import net.webike.RcProductFactory.util.RedmineUtils;
import net.webike.RcProductFactory.util.Utils;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.taskadapter.redmineapi.RedmineException;
import com.taskadapter.redmineapi.bean.Issue;
import com.taskadapter.redmineapi.bean.User;

/**
 * JSonAction for get json from ajax.
 */
@Controller
@Scope("request")
@ParentPackage(value = "json-default")
@Results(
    {
        @Result(name = "redmineApiKey", type = "json"),
        @Result(name = "issueRedmine" , type = "json",
        params = {"contentType", "text/html", "root", "mapIssue" , "ignoreHierarchy", "false" }),
        @Result(name = "errorOther", type = "redirect", location = "error.html")
    }
)
public class RedmineJSonAction extends CommonAction {

    /**
	 * @description serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	private static final String CURRENT_CLASS = "[RedmineJSonAction]";

	// Api access key get from Redmine
    private String apiAccessKey = "";
    // API redmine get Issue
    private Map<Object, Object> mapIssue;
    @Autowired
    private SettingService settingService;

    /************************************************************************
     * <b>Description:</b><br>
     *  Select Redmine Api Key from Redmine.
     *
     * @author		Tran.Thanh
     * @date		13 Jan 2014
     * @return		String
     * @throws 		IOException	IOException
     ************************************************************************/
    @Action("/selectRedmineApiKey")
    public String selectRedmineApiKey() throws IOException {
    	// get API key from redmine API
    	String userId = this.getRequest().getParameter("userId");
    	String userPasswd = this.getRequest().getParameter("userPasswd");
    	String apiAccesKey = "";
    	User userRedmine = null;
    	try {
    		String urlRedmineInDb = settingService.selectSettingValueBySettingCode("REDMINE_URL");
			String redmineUserUrl = urlRedmineInDb + Constant.REDMINE_CURRENT_USER_URL;
			String redmineHostName = settingService.selectSettingValueBySettingCode("REDMINE_HOST_NAME");
			HttpsURLConnection httpsCon = RedmineUtils.getResultHttpsConWithUser(
															userId,
															userPasswd,
															redmineUserUrl,
															redmineHostName
															);

			if (httpsCon.getResponseCode() == Constant.HTTPSCON_RESPONSECODE) {
				userRedmine = RedmineUtils.getJsonUserRedmine(httpsCon);
			}
		} catch (RedmineException e) {
            this.getLogger().error(CURRENT_CLASS + " An error execute redmine");
            this.getLogger().error(e.toString());
		}
    	if (userRedmine != null) {
    		apiAccesKey = userRedmine.getApiKey();
    	}

    	this.setApiAccessKey(apiAccesKey);
        return "redmineApiKey";
    }

    /************************************************************************
     * <b>Description:</b><br>
     *  selectIssueRedmineByIssueId.
     *
     * @author		Le.Dinh
     * @date		18 Jan 2014
     * @return		String
     * @throws 		IOException		IOException
     ************************************************************************/
    @Action("/selectIssueRedmineByIssueId")
    public String selectIssueRedmineByIssueId() throws IOException {
        //Validate loginUser
        if (!validateLoginUser()) {
            return "errorOther";
        }
    	String apiKey = this.getCommonForm().getApiAccessKey();
    	String issueId = this.getRequest().getParameter("issueId");
    	String redmineUrl = settingService.selectSettingValueBySettingCode("REDMINE_URL");
    	String fullUrlIssue = redmineUrl + Constant.REDMINE_ISSUES_URL + issueId + ".json?";

    	mapIssue = new HashMap<Object, Object>();
    	mapIssue.put("responeCode", "fail");
		try {
			String redmineHostName = settingService.selectSettingValueBySettingCode("REDMINE_HOST_NAME");
			String redmineURL = settingService.selectSettingValueBySettingCode("REDMINE_URL");
			HttpsURLConnection httpsCon = RedmineUtils.getResultHttpsConWithApiKey(
																apiKey,
																fullUrlIssue,
																redmineHostName);
			if (httpsCon.getResponseCode() == Constant.HTTPSCON_RESPONSECODE) {
				Issue issue = RedmineUtils.getJsonIssuesRedmine(httpsCon);
				mapIssue.put("issueId", issue.getId());
				mapIssue.put("projectId", issue.getProject().getId());
				mapIssue.put("projectName", issue.getProject().getName());
				mapIssue.put("subject", issue.getSubject());
				if (issue.getStartDate() != null) {
					mapIssue.put("startDate", Utils.formatDateFull(issue.getStartDate()));
				}
				if (issue.getDueDate() != null) {
					mapIssue.put("dueDate", Utils.formatDateFull(issue.getDueDate()));
				}

				// get AssigneloginId from authorId in redmine
				if (issue.getAssignee() != null) {
					String fullUrlAssignUserLogin = redmineURL + Constant.REDMINE_USER_URL + issue.getAssignee().getId() + ".json?";
					HttpsURLConnection httpsConAssigned = RedmineUtils.getResultHttpsConWithApiKey(
																apiKey,
																fullUrlAssignUserLogin,
																redmineHostName);
					if (httpsConAssigned.getResponseCode() == Constant.HTTPSCON_RESPONSECODE) {
						User user = RedmineUtils.getJsonUserRedmine(httpsConAssigned);
						mapIssue.put("assignedLogin", user.getLogin());
						mapIssue.put("assignedName", user.getFullName());
					}
				}
				mapIssue.put("responeCode", "success");
			}
		} catch (RedmineException e) {
            this.getLogger().error(CURRENT_CLASS + " An error execute redmine");
            this.getLogger().error(e.toString());
		}
    	return "issueRedmine";
    }

	/**
	 * @return the apiAccessKey
	 */
	public String getApiAccessKey() {
		return apiAccessKey;
	}

	/**
	 * @param apiAccessKey the apiAccessKey to set
	 */
	public void setApiAccessKey(String apiAccessKey) {
		this.apiAccessKey = apiAccessKey;
	}

	/**
	 * @return the mapIssue
	 */
	public Map<Object, Object> getMapIssue() {
		return mapIssue;
	}

	/**
	 * @param mapIssue the mapIssue to set
	 */
	public void setMapIssue(Map<Object, Object> mapIssue) {
		this.mapIssue = mapIssue;
	}

}
