/************************************************************************
 * File Name	： newUserActionForm.java
 * Author		： Nguyen.Chuong
 * Version		： 1.0
 * Date Created	： 2013/10/02
 * Date Updated	： 2013/10/02
 * Description	： contain data for newUserActionForm.
 ***********************************************************************/
package net.webike.RcProductFactory.action.form;

import java.util.HashMap;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * NewUserActionForm class.
 */
@Component
@Scope("session")
public class NewUserActionForm {

    /**---------------------------------------------------------------------------.
     * @description hashmap contains all values of authority.
     */
    private HashMap<String, String> authorityHashmap;

    /**---------------------------------------------------------------------------.
     * @description hashmap contains all values of status.
     */
    private HashMap<String, String> statusHashmap;

    /**
     * @return the authorityHashmap
     */
    public HashMap<String, String> getAuthorityHashmap() {
        return authorityHashmap;
    }

    /**
     * @param authorityHashmap the authorityHashmap to set
     */
    public void setAuthorityHashmap(HashMap<String, String> authorityHashmap) {
        this.authorityHashmap = authorityHashmap;
    }

    /**
     * @return the statusHashmap
     */
    public HashMap<String, String> getStatusHashmap() {
        return statusHashmap;
    }

    /**
     * @param statusHashmap the statusHashmap to set
     */
    public void setStatusHashmap(HashMap<String, String> statusHashmap) {
        this.statusHashmap = statusHashmap;
    }

}
