/************************************************************************
 * File Name    ： ProductInfoAction.java
 * Author        ： Nguyen.Chuong
 * Version        ： 1.0.0
 * Date Created    ： 2013/10/22
 * Date Updated    ： 2013/10/22
 * Description    ： productInformation action to load info of product
 *
 ***********************************************************************/
package net.webike.RcProductFactory.action;

import net.webike.RcProductFactory.action.form.ProductInfoActionForm;
import net.webike.RcProductFactory.entity.EntMstFactoryMatter;
import net.webike.RcProductFactory.entity.EntProduct;
import net.webike.RcProductFactory.service.MatterService;
import net.webike.RcProductFactory.service.ProductService;
import net.webike.RcProductFactory.util.Constant;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Controller;

/**
 * PJ Detail Action Class.
 */
@Controller
@Scope("request")
@ParentPackage(value = "CommonInterceptor")
@Results(
    {
        // On loading default user manage page
        @Result(name = "success", location = "productInfo.jsp"),
        // Return error page with error params
        @Result(name = "error", type = "redirect", location = "error.html?error=${error}"),
        // Return error page when parameter fail
        @Result(name = "errorOther", type = "redirect", location = "error.html")
    }
)
public class ProductInfoAction extends CommonAction {

    /**
     * @serialVersionUID
     */
    private static final long serialVersionUID = 1L;

    private String matterNo;

    // Error params for error page
    private String error;
    /**---------------------------------------------------------------------------.
     * @description actionForm for productInfoAction.
     */
    @Autowired
    private ProductInfoActionForm productInfoActionForm;
    @Autowired
    private ProductService productService;

    /**
     * Matter Service.
     */
    @Autowired
    private MatterService matterService;

    @Override
    @Action("/productInfo")
    public String execute() {
    	try {
            //Validate loginUser
            if (!validateLoginUser()) {
                return "errorOther";
            }
            //Check required parameter
            if (StringUtils.isEmpty(matterNo)) {
                // Return error page with error parameter
                error = Constant.ERROR_ERROR_PARAM;

                return "error";
            }
            //Check valid matterNo params
            boolean isNumber = NumberUtils.isDigits(matterNo);
        	if (StringUtils.isEmpty(matterNo) || !isNumber) {
        	    error = Constant.ERROR_ERROR_PARAM;

                return "error";
            }
            // Get matter detail by matterNo
            EntMstFactoryMatter entMatter = matterService.selectDetailMatterByMatterNo(matterNo);
            // Check entMatter not null
            if (null == entMatter) {
                error = Constant.ERROR_ERROR_PARAM;

                return "error";
            }
            // Check if not admin, cannot loading data from another operator, return error page
            //if (Constant.OPERATOR_AUTHORITY.equals(this.getCommonForm().getLoginAuthority())) {
            String matterChargeUserId = entMatter.getMatterChargeUserId();
            // If matterChargeUserId not equals login ID, userId not has permission
            if (!matterChargeUserId.equals(this.getCommonForm().getLoginId().toString())) {
                error = Constant.ERROR_NO_PERMISSION;
                return "error";
            }
            productInfoActionForm.setMatterNo(matterNo);
            //Set original page to redirect after release matter.
            productInfoActionForm.setOriginalPage(this.getRequest().getHeader("Referer"));

            //Get list init status of product.
            productInfoActionForm.setListProductStatus(productService.selectAllProductStatus());

            EntProduct entProduct = new EntProduct();
            entProduct.setMatterNo(matterNo);

            //Get count product of matter.
            productInfoActionForm.setEntMatter(productService.countProductOfMatter(entProduct));

            // No product in this matter
            if (productInfoActionForm.getEntMatter().getTotalAllProduct() <= 0) {
                // Set nodata error
                error = Constant.ERROR_NO_DATA;

                return "error";
            }
        } catch (DataAccessException e) {
            this.getLogger().error("[ProductInfoAction] An error to get data in "
                                    + "execute function");
            this.getLogger().error(e.toString());
            return "errorOther";
        } catch (Exception e) {
            this.getLogger().error("[ProductInfoAction] An error execute function");
            this.getLogger().error(e.toString());
            return "errorOther";
        }
        return SUCCESS;
    }

    /**
     * @return the productInfoActionForm
     */
    public ProductInfoActionForm getProductInfoActionForm() {
        return productInfoActionForm;
    }

    /**
     * @param productInfoActionForm the productInfoActionForm to set
     */
    public void setProductInfoActionForm(ProductInfoActionForm productInfoActionForm) {
        this.productInfoActionForm = productInfoActionForm;
    }

    /**
	 * @return the matterService
	 */
	public MatterService getMatterService() {
		return matterService;
	}

	/**
	 * @param matterService the matterService to set
	 */
	public void setMatterService(MatterService matterService) {
		this.matterService = matterService;
	}

    /**
     * @return the matterNo
     */
    public String getMatterNo() {
        return matterNo;
    }

    /**
     * @param matterNo the matterNo to set
     */
    public void setMatterNo(String matterNo) {
        this.matterNo = matterNo;
    }

    /**
     * @return the productService
     */
    public ProductService getProductService() {
        return productService;
    }

    /**
     * @param productService the productService to set
     */
    public void setProductService(ProductService productService) {
        this.productService = productService;
    }

    /**
     * @return the error
     */
    public String getError() {
        return error;
    }

    /**
     * @param error the error to set
     */
    public void setError(String error) {
        this.error = error;
    }
}
