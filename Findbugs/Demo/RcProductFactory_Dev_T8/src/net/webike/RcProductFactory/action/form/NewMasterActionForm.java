/************************************************************************
 * File Name    ： NewMasterActionForm.java
 * Author        ： Luong.Dai
 * Version        ： 1.0.0
 * Date Created    ： 2013/11/06
 * Date Updated    ： 2013/11/07
 * Description    ： contain data of matter for newMatter page.
 *
 ***********************************************************************/

package net.webike.RcProductFactory.action.form;

import java.util.List;
import net.webike.RcProductFactory.entity.EntMstFactoryUserTmp;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * NewMasterActionForm.
 */
@Component
@Scope("session")
public class NewMasterActionForm {
    // List Administrator
    private List<EntMstFactoryUserTmp> listAdmin;

    /**
     * @return the listAdmin
     */
    public List<EntMstFactoryUserTmp> getListAdmin() {
        return listAdmin;
    }

    /**
     * @param listAdmin the listAdmin to set
     */
    public void setListAdmin(List<EntMstFactoryUserTmp> listAdmin) {
        this.listAdmin = listAdmin;
    }
}
