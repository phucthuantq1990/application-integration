/************************************************************************
 * File Name    ： ProductInfoActionForm.java
 * Author        ： Nguyen.Chuong
 * Version        ： 1.0.0
 * Date Created    ： 2013/10/22
 * Date Updated    ： 2013/10/33
 * Description    ： contain data of productInfoAction for productInfo page.
 *
 ***********************************************************************/
package net.webike.RcProductFactory.action.form;

import java.util.List;
import java.util.TreeMap;

import net.webike.RcProductFactory.entity.EntMstFactoryMatter;
import net.webike.RcProductFactory.entity.EntMstFactoryProductStatus;
import net.webike.RcProductFactory.entity.EntMstFactoryUserTmp;
import net.webike.RcProductFactory.entity.EntProduct;
import net.webike.RcProductFactory.entity.EntTblFactoryProductImage;
import net.webike.RcProductFactory.entity.EntTblFactoryProductModel;
import net.webike.RcProductFactory.util.Constant;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * UserManageActionForm.
 */
@Component
@Scope("session")
public class ProductInfoActionForm {
    private String matterNo;
    //Original page to redirect after release matter
    private String originalPage = "";

    private List<EntMstFactoryProductStatus> listProductStatus;
    private EntMstFactoryMatter entMatter;
    /**---------------------------------------------------------------------------.
     * @description list url image of product.
     */
    private List<EntTblFactoryProductImage> listImage;
    private List<EntTblFactoryProductModel> listModel;
    private List<EntProduct> listProduct;
    private String noImageURL = Constant.NO_IMAGE_URL;

    // Add properties by luong.dai
    private String checkedNo = Constant.PRODUCT_CHECKED_STATUS_CODE;

    /** =============================THANH =================================== */
    /**
     * message error.
     */
    private String message;

    /**
     * EntMstFactoryMatter.
     */
    private List<EntMstFactoryMatter> listMatter;

    /**
     * @return message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message セットする message
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * @return the listMatter
     */
    public List<EntMstFactoryMatter> getListMatter() {
        return listMatter;
    }

    /**
     * @param listMatter the listMatter to set
     */
    public void setListMatter(List<EntMstFactoryMatter> listMatter) {
        this.listMatter = listMatter;
    }

    /**
	 * @listAllUser list all users
	 */
	private List<EntMstFactoryUserTmp> listAllUser;

	/**---------------------------------------------------------------------------.
     * @description hashmap contains all values of authority.
     */
    private TreeMap<String, String> authorityTreeMap;

    /**---------------------------------------------------------------------------.
     * @description hashmap contains all values of status.
     */
    private TreeMap<String, String> statusTreeMap;

	/**
	 * @return the authorityTreeMap
	 */
	public TreeMap<String, String> getAuthorityTreeMap() {
		return authorityTreeMap;
	}

	/**
	 * @param authorityTreeMap the authorityTreeMap to set
	 */
	public void setAuthorityTreeMap(TreeMap<String, String> authorityTreeMap) {
		this.authorityTreeMap = authorityTreeMap;
	}

	/**
	 * @return the statusTreeMap
	 */
	public TreeMap<String, String> getStatusTreeMap() {
		return statusTreeMap;
	}

	/**
	 * @param statusTreeMap the statusTreeMap to set
	 */
	public void setStatusTreeMap(TreeMap<String, String> statusTreeMap) {
		this.statusTreeMap = statusTreeMap;
	}

	/**
     * @return listAllUser
     */
	public List<EntMstFactoryUserTmp> getListAllUser() {
		return listAllUser;
	}

	/**
     * @param listAllUser list all users.
     */
	public void setListAllUser(List<EntMstFactoryUserTmp> listAllUser) {
		this.listAllUser = listAllUser;
	}

    /**
     * @return the listProduct
     */
    public List<EntProduct> getListProduct() {
        return listProduct;
    }

    /**
     * @param listProduct the listProduct to set
     */
    public void setListProduct(List<EntProduct> listProduct) {
        this.listProduct = listProduct;
    }

    /**
     * @return the listImage
     */
    public List<EntTblFactoryProductImage> getListImage() {
        return listImage;
    }

    /**
     * @param listImage the listImage to set
     */
    public void setListImage(List<EntTblFactoryProductImage> listImage) {
        this.listImage = listImage;
    }

    /**
     * @return the listModel
     */
    public List<EntTblFactoryProductModel> getListModel() {
        return listModel;
    }

    /**
     * @param listModel the listModel to set
     */
    public void setListModel(List<EntTblFactoryProductModel> listModel) {
        this.listModel = listModel;
    }

    /**
     * @return the noImageURL
     */
    public String getNoImageURL() {
        return noImageURL;
    }

    /**
     * @param noImageURL the noImageURL to set
     */
    public void setNoImageURL(String noImageURL) {
        this.noImageURL = noImageURL;
    }

    /**
     * @return the listProductStatus
     */
    public List<EntMstFactoryProductStatus> getListProductStatus() {
        return listProductStatus;
    }

    /**
     * @param listProductStatus the listProductStatus to set
     */
    public void setListProductStatus(List<EntMstFactoryProductStatus> listProductStatus) {
        this.listProductStatus = listProductStatus;
    }

    /**
     * @return the matterNo
     */
    public String getMatterNo() {
        return matterNo;
    }

    /**
     * @param matterNo the matterNo to set
     */
    public void setMatterNo(String matterNo) {
        this.matterNo = matterNo;
    }

    /**
     * @return the entMatter
     */
    public EntMstFactoryMatter getEntMatter() {
        return entMatter;
    }

    /**
     * @param entMatter the entMatter to set
     */
    public void setEntMatter(EntMstFactoryMatter entMatter) {
        this.entMatter = entMatter;
    }

    /**
     * @return the checkedNo
     */
    public String getCheckedNo() {
        return checkedNo;
    }

    /**
     * @param checkedNo the checkedNo to set
     */
    public void setCheckedNo(String checkedNo) {
        this.checkedNo = checkedNo;
    }

    /**
     * @return the originalPage
     */
    public String getOriginalPage() {
        return originalPage;
    }

    /**
     * @param originalPage the originalPage to set
     */
    public void setOriginalPage(String originalPage) {
        this.originalPage = originalPage;
    }
 }
