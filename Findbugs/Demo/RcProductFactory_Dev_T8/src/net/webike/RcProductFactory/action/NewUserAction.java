/************************************************************************
 * File Name	： NewUserAction.java
 * Author		： Nguyen.Chuong
 * Version		： 1.0
 * Date Created	： 2013/10/02
 * Date Updated	： 2013/10/02
 * Description	： insert new user to database.
 *
 ***********************************************************************/
package net.webike.RcProductFactory.action;

import net.webike.RcProductFactory.action.form.UserDetailActionForm;
import net.webike.RcProductFactory.entity.EntMstFactoryUserTmp;
import net.webike.RcProductFactory.entity.UserAuthority;
import net.webike.RcProductFactory.entity.UserStatus;
import net.webike.RcProductFactory.service.SettingService;
import net.webike.RcProductFactory.service.UserTmpService;
import net.webike.RcProductFactory.util.Constant;
import net.webike.RcProductFactory.util.Utils;
import net.webike.RcProductFactory.util.ValidateUtils;

import org.apache.commons.lang.StringUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 * newUserAction Class.
 */
@Controller
@Scope("request")
@ParentPackage(value = "CommonInterceptor")
@Results(
    {
    	// On loading default newUser page
        @Result(name = "success", location = "userDetail.jsp"),
        // Return newUser page
        @Result(name = "insert", type = "redirect", location = "newUser.html"),
        // Return error page when parameter fail
        @Result(name = "error", type = "redirect", location = "error.html?error=param"),
        // Return error page when other error
        @Result(name = "errorOther", type = "redirect", location = "error.html")
    }
)
public class NewUserAction extends CommonAction {

	/**
	 * @description serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;

	@Autowired
	private UserTmpService userTmpService;
	@Autowired
	private SettingService settingService;

	/**
	 * UserDetailActionForm attribute.
	 * @description actionForm for contain all attributes.
	 */
	@Autowired
	private UserDetailActionForm userDetailActionForm = new UserDetailActionForm();

    @Override
    @Action("/newUser")
    public String execute() {
        //Validate loginUser
        if (!super.validateLoginUser()) {
            return "errorOther";
        }

        //Get init authority and status hashmap.
        userDetailActionForm.setAuthorityTreeMap(UserAuthority.getListTreeMap());
        userDetailActionForm.setStatusTreeMap(UserStatus.getListTreeMap());

        //Check insert status to load init data.
        if (userDetailActionForm.getInsertStatus()) {
            userDetailActionForm.setEntUserTmp(null);
            userDetailActionForm.setSuccessMessage(null);
            userDetailActionForm.setFailMessage(null);
        }

        //Set insertStatus = true for reload page load init data.
        userDetailActionForm.setInsertStatus(true);

        //Set basic value to load data in jsp.
        userDetailActionForm.setActionName("newUser");
        userDetailActionForm.setSubmitActionName(getText("insertUser.html"));
        userDetailActionForm.setH1Title(getText("text.label.newUserlH1"));
        userDetailActionForm.setDetailTabId("centerTab");

        return "success";
    }

    /************************************************************************
     * <b>Description:</b><br>
     *  Get input data, validate and insert new user to DB.
     *
     * @author		Nguyen.Chuong
     * @date		Oct 2, 2013
     * @return		String
     ************************************************************************/
    @Action("/insertUser")
    public String insertUser() {
        try {
        	// Check userDetailActionForm has an successMessage
            if (StringUtils.isNotEmpty(userDetailActionForm.getSuccessMessage())) {
                userDetailActionForm.setEntUserTmp(null);
                userDetailActionForm.setSuccessMessage("");
                userDetailActionForm.setFailMessage("");
                userDetailActionForm.setInsertStatus(true);
                return "success";
            }

            // Validate loginUser
            if (!validateLoginUser()) {
                return "errorOther";
            }
            // Get insert ent.
            EntMstFactoryUserTmp entUserTmp = userDetailActionForm.getEntUserTmp();
            // Check required field
            if (null == entUserTmp) {
                return "insert";
            }
            if (StringUtils.isEmpty(entUserTmp.getUserId())
                    || StringUtils.isEmpty(entUserTmp.getUserFirstName())
                    || StringUtils.isEmpty(entUserTmp.getUserLastName())
                    || StringUtils.isEmpty(entUserTmp.getUserDelFlg())
                    || StringUtils.isEmpty(entUserTmp.getUserRedmineApiKey())
                    || StringUtils.isEmpty(entUserTmp.getUserPasswd())
                    || StringUtils.isEmpty(entUserTmp.getUserPasswdConfirm())) {
                return "insert";
            }
            // Get Api AccessKey
            String apiKey = this.getRequest().getParameter("userRedmineApiKeyHidden");
            if (StringUtils.isNotEmpty(apiKey)) {
            	entUserTmp.setUserRedmineApiKey(apiKey);
            } else {
            	return "insert";
			}
            // Validate entity, show message when error and return to newUser action.
            if (!ValidateUtils.validateUserEnt(entUserTmp)) {
                userDetailActionForm.setFailMessage(Utils.getProp("message.success.insertFail"));
                userDetailActionForm.setSuccessMessage("");
                userDetailActionForm.setInsertStatus(false);
                return "insert";
            }

            // check confirm API again
            String urlRedmineInDb = settingService.selectSettingValueBySettingCode("REDMINE_URL");
			String redmineUserUrl = urlRedmineInDb + Constant.REDMINE_CURRENT_USER_URL;
			String redmineHostName = settingService.selectSettingValueBySettingCode("REDMINE_HOST_NAME");
            String apiKeyConfirm = Utils.selectRedmineApiKey(
            									entUserTmp.getUserId(),
            									entUserTmp.getUserPasswd(),
            									redmineUserUrl,
            									redmineHostName);
            if (StringUtils.isEmpty(apiKeyConfirm) || !apiKeyConfirm.equals(apiKey)) {
                userDetailActionForm.setFailMessage(Utils.getProp("message.error.confirmApiKeyIsNotOK"));
                userDetailActionForm.setSuccessMessage("");
                userDetailActionForm.setInsertStatus(false);
            	return "insert";
            }


            // Set tourokuUserId as loginId for insert to 4 last column.
            entUserTmp.setCreatedUserId(this.getCommonForm().getLoginId());
            entUserTmp.setUpdatedUserId(this.getCommonForm().getLoginId());
            // Convert String password to Md5
            entUserTmp.setUserPasswd(Utils.convertStringToMd5(entUserTmp.getUserPasswd()));

            // Excecute insert process.
            boolean result = userTmpService.insertNewUserTmp(entUserTmp);
            // Check result: if true return success and show success message, load page..
            if (result) {
                userDetailActionForm.setEntUserTmp(null);
                userDetailActionForm.setSuccessMessage(Utils.getProp("message.success.insertSuccess"));
                userDetailActionForm.setFailMessage("");
                userDetailActionForm.setInsertStatus(true);
                return "success";
            }
        } catch (Exception e) {
            //Catch error insert false.
            this.getLogger().error("[NewUserAction] An error to insert new user at"
                                    + "insertUser() function");
            this.getLogger().error(e.toString());
            e.printStackTrace();
            //Set return value
            setReturnValue(false);
            //Set password back to normal.
            userDetailActionForm.getEntUserTmp().setUserPasswd(userDetailActionForm.getEntUserTmp().getUserPasswdConfirm());
            //Return to newUserAction.
            return "insert";
        }
        //Set return value
        setReturnValue(false);
        //Set password back to normal.
        userDetailActionForm.getEntUserTmp().setUserPasswd(userDetailActionForm.getEntUserTmp().getUserPasswdConfirm());

        //Return to newUserAction.
        return "insert";
    }
    /************************************************************************
     * <b>Description:</b><br>
     *  replace your description for this method here.
     *
     * @author		Nguyen.Chuong
     * @date		Nov 19, 2013
     * @param       resultFlag boolean to check insert process success or fail.
     ************************************************************************/
    public void setReturnValue(boolean resultFlag) {
        //Insert success case
        if (resultFlag) {
            //Set insert success message.
            userDetailActionForm.setSuccessMessage(Utils.getProp("message.success.insertSuccess"));
            //Set fail message blank
            userDetailActionForm.setFailMessage("");
            //Set status insert is true.
            userDetailActionForm.setInsertStatus(true);
            return;
        }
        //Insert fail
        //Set insert success message blank.
        userDetailActionForm.setSuccessMessage("");
        //Set fail message
        userDetailActionForm.setFailMessage(Utils.getProp("message.success.insertFail"));
        //Set insert Status to false.
        userDetailActionForm.setInsertStatus(false);
    }

    /**
     * @return the userDetailActionForm
     */
    public UserDetailActionForm getUserDetailActionForm() {
        return userDetailActionForm;
    }

    /**
     * @param userDetailActionForm the userDetailActionForm to set
     */
    public void setUserDetailActionForm(UserDetailActionForm userDetailActionForm) {
        this.userDetailActionForm = userDetailActionForm;
    }
}
