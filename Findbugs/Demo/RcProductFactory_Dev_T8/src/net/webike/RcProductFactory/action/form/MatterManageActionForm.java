/************************************************************************
 * file name	： MatterManageActionForm.java
 * author		： Le.Dinh
 * version		： 1.0.0
 * date created	： 17 Jan 2014
 * date updated	： 17 Jan 2014
 * description	： MatterManageActionForm
 *
 ***********************************************************************/
package net.webike.RcProductFactory.action.form;

import java.util.List;
import java.util.Map;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import net.webike.RcProductFactory.entity.EntMstFactoryMatterNew;

/**
 * MatterManageActionForm.
 */
@Component
@Scope("session")
public class MatterManageActionForm {
	private EntMstFactoryMatterNew entMstFactoryMatterNew;

	private List<EntMstFactoryMatterNew> entMstFactoryMatterList;
	private int entMstFactoryMatterCount;

	//List type enum.
    private Map<String, String> listRegistrationFlg;


	public void setEntMstFactoryMatterCount(int entMstFactoryMatterCount) {
		this.entMstFactoryMatterCount = entMstFactoryMatterCount;
	}

	public int getEntMstFactoryMatterCount() {
		return entMstFactoryMatterCount;
	}

	/**
	 * @return the entMstFactoryMatterNew
	 */
	public EntMstFactoryMatterNew getEntMstFactoryMatterNew() {
		return entMstFactoryMatterNew;
	}

	/**
	 * @param entMstFactoryMatterNew the entMstFactoryMatterNew to set
	 */
	public void setEntMstFactoryMatterNew(EntMstFactoryMatterNew entMstFactoryMatterNew) {
		this.entMstFactoryMatterNew = entMstFactoryMatterNew;
	}

	/**
	 * @return the entMstFactoryMatterList
	 */
	public List<EntMstFactoryMatterNew> getEntMstFactoryMatterList() {
		return entMstFactoryMatterList;
	}

	/**
	 * @param entMstFactoryMatterList the entMstFactoryMatterList to set
	 */
	public void setEntMstFactoryMatterList(
			List<EntMstFactoryMatterNew> entMstFactoryMatterList) {
		this.entMstFactoryMatterList = entMstFactoryMatterList;
	}

	/**
	 * @return the listRegistrationFlg
	 */
	public Map<String, String> getListRegistrationFlg() {
		return listRegistrationFlg;
	}

	/**
	 * @param listRegistrationFlg the listRegistrationFlg to set
	 */
	public void setListRegistrationFlg(Map<String, String> listRegistrationFlg) {
		this.listRegistrationFlg = listRegistrationFlg;
	}

}
