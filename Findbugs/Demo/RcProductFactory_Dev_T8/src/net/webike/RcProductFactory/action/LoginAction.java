
package net.webike.RcProductFactory.action;

import net.webike.RcProductFactory.util.Constant;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 * Login Action.
 */
@Controller
@Scope("request")
@ParentPackage(value = "CommonInterceptor")
@Results(
    {
        @Result(name = "success", location = "login.jsp")
    }
)
public class LoginAction extends CommonAction {

    /**
	 * @serialVersionUID serialVersion
	 */
	private static final long serialVersionUID = 1L;

	@Override
    @Action("/login")
    public String execute() throws Exception {
        String errorValue = this.getRequest().getParameter(Constant.PARAM_ERROR);
        if (Constant.ERROR_LOGIN_VALUE.equals(errorValue)) {
            this.getCommonForm().setErrorMessage(getText("message.error.IdOrPassWrong"));
        }
        return SUCCESS;
    }

}
