/************************************************************************
 * file name    :EditBrandAction.java
 * author		:Tran.Thanh
 * version		:1.0.0
 * date created	:27 Dec 2013
 * date updated	:27 Dec 2013
 * description	:Action for edit brand
 *
 ***********************************************************************/
package net.webike.RcProductFactory.action;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import net.webike.RcProductFactory.action.form.EditBrandActionForm;
import net.webike.RcProductFactory.entity.EntMstBrand;
import net.webike.RcProductFactory.entity.EntMstFactoryUserTmp;
import net.webike.RcProductFactory.entity.EntMstMarume;
import net.webike.RcProductFactory.entity.ImageUpload;
import net.webike.RcProductFactory.service.BrandService;
import net.webike.RcProductFactory.service.MarumeService;
import net.webike.RcProductFactory.service.UserTmpService;
//import net.webike.RcProductFactory.service.UserService;
import net.webike.RcProductFactory.util.Constant;
import net.webike.RcProductFactory.util.Utils;
import net.webike.RcProductFactory.util.ValidateUtils;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.apache.struts2.interceptor.SessionAware;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataAccessException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 * Action Edit Brand.
 */
@Scope("request")
@ParentPackage(value = "CommonInterceptor")
@Results(
    {
    	// On loading default matter detail page
        @Result(name = "success", location = "editBrand.jsp"),
        // After updated success return user detail page with param userId
        @Result(name = "editbrand", type = "redirectAction", location = "editBrand", params = {"brandCode", "${brandCodeToReturn}" }),
        // when edit success redirect to  brandMasterManager page
        /*BOE  Thai.Son 2014/01/17*/
        @Result(name = "brandMasterManager", type = "redirect", location = "brandMasterManager.html"/*,
        		params = {"msgSuccessWhenEditBrand", "${messageParamSuccess}" }*/),
        /*EOE  Thai.Son 2014/01/17*/
        // Return error page when parameter fail
        @Result(name = "error", type = "redirect", location = "error.html?error=param"),
        // Return error page when parameter fail
        @Result(name = "errorNoPermission", type = "redirect", location = "error.html?error=noPermission"),
        // Return error page when error other.
        @Result(name = "errorOther", type = "redirect", location = "error.html"),
	    @Result(name = "previewImage" , type = "json",
        params = {"contentType", "text/html", "root", "imageUpload" , "ignoreHierarchy", "false" }),
        @Result(name = "deleteImage" , type = "json",
        params = {"contentType", "text/html", "root", "resultString" , "ignoreHierarchy", "false" })
    }
)
public class EditBrandAction extends CommonAction implements SessionAware { //BOE implements SessionAware Thai.Son 2014/01/17 EOE  Thai.Son 2014/01/17
    /*BOE Thai.Son 2014/01/17*/
    private Map<String, Object> session;
    /*EOE Thai.Son 2014/01/17*/
    /**
	 * @description serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	private static final String BRAND_CODE_PARAM = "brandCode";
	private static final String MODE_BRAND_UPDATE = "1";
	private static final String MODE_BRAND_INSERT = "0";
	private static final String ACTION_UPDATE = "updateBrand.html";
	private static final String ACTION_INSERT = "insertNewBrand.html";
	private static final String CURRENT_CLASS = "[EditBrandAction]";
	private static final String DEL_FLG_BRAND = "delflg";
	private static final String IMAGE_NAME = "imageName";

    private EditBrandActionForm editBrandActionForm;
	private ImageUpload imageUpload;
	private File userImage;
	private String userImageContentType;
	private String userImageFileName;
	private String resultString;
	/*BOE  thai.son 2014/01/21 */
	private Integer brandCodeToReturn;
	/*EOE  thai.son 2014/01/21 */
	private String messageParamSuccess = "";

	@Autowired
    private BrandService brandService;
    @Autowired
    private UserTmpService userTmpService;
    @Autowired
    private MarumeService marumeService;

	@Override
    @Action("/editBrand")
    public String execute() {
        try {
            //Validate loginUser
            if (!validateLoginUser()) {
                return "errorOther";
            }
            //Check insert status to load init data.
            if (!editBrandActionForm.getInsertStatus()) {
            	editBrandActionForm.setFailMessage("");
            }

            String brandCode = this.getRequest().getParameter(BRAND_CODE_PARAM);
            if (StringUtils.isNotEmpty(brandCode)) {
            	if (NumberUtils.isDigits(brandCode)) {
            	    /*BOE  thai.son 2014/01/21 */
            	    Integer brandCodeInt = Integer.parseInt(brandCode);
            	    if (brandCodeInt != null && brandCodeInt > 0) {
                        EntMstBrand entMstBrandQuery = new EntMstBrand();
                        /*BOE  hoang.ho 2014/01/21 */
//                      entMstBrandQuery.setBrandCode(brandCode);
                        // parse brand code to integer
                        entMstBrandQuery.setBrandCode(brandCodeInt);
                        /*EOE  hoang.ho 2014/01/21 */
                        EntMstBrand entBrand = brandService.selectBrandByBrandCode(entMstBrandQuery);
                        /*BOE  Thai.Son 2014/01/17 */
                        session.put(Constant.SESSION_BRAND_EDIT, entBrand);
                        /*EOE  Thai.Son 2014/01/17*/
                        /*BOE  hoang.ho 2014/01/21 */
//                      entBrand.setBrandCode(brandCode);
                        entBrand.setBrandCode(brandCodeInt);
                        /*EOE  hoang.ho 2014/01/21 */
                        editBrandActionForm.setEntBrand(entBrand);
                        // Mode 1 is Edit Brand
                        editBrandActionForm.setMode(MODE_BRAND_UPDATE);
                        // Set Action Name is update Brand
                        editBrandActionForm.setActionName(ACTION_UPDATE);
            	    }
            	    /*BOE  thai.son 2014/01/21 */
            	} else {
            		return "error";
            	}
            } else {
            	editBrandActionForm.setEntBrand(null);
            	// Mode 0 is New Brand
                editBrandActionForm.setMode(MODE_BRAND_INSERT);
             // Set Action Name is new Brand
                editBrandActionForm.setActionName(ACTION_INSERT);
            }
            //Set list user to common form
            //Thanh -start delete
            List<EntMstFactoryUserTmp> listCommonUser = userTmpService.selectListAllUser();
            this.getCommonForm().setListCommonUser(listCommonUser);
            //Thanh -end delete

            //set list marume to editBrand form
            List<EntMstMarume> listMarume = marumeService.selectListAllMarume();
            editBrandActionForm.setListMarume(listMarume);
            editBrandActionForm.setInsertStatus(false);
        } catch (DataAccessException e) {
            this.getLogger().error(CURRENT_CLASS + " An error to get data in "
                                    + "execute function");
            this.getLogger().error(e.toString());
            return "error";
        } catch (Exception e) {
            this.getLogger().error(CURRENT_CLASS + " An error execute function");
            this.getLogger().error(e.toString());
            return "errorOther";
        }
        return SUCCESS;
    }

	/************************************************************************
	 * <b>Description:</b><br>
	 *  update or insert new brand.
	 *
	 * @author		Tran.Thanh
	 * @date		31 Dec 2013
	 * @return		String
	 ************************************************************************/
	//@SuppressWarnings("unused")
    @Action("/updateOrNewBrand")
	public String updateOrNewBrand() {
		String result = "";
		try {
            //Validate loginUser
            if (!validateLoginUser()) {
                return "errorOther";
            }
            // BOE #6634 No.12 Hoang.Ho 2014/02/18 Cut overflow length of BrandName
            // prepare data of form.
            validateAndPrepareData();
            // EOE #6634 No.12 Hoang.Ho 2014/02/18 Cut overflow length of BrandName

            // default file path
            String filePath = Utils.getRealPath(this.getRequest());

            // Set insertStatus = true for reload page load init data.
            editBrandActionForm.setInsertStatus(true);
            EntMstBrand entBrand = editBrandActionForm.getEntBrand();

            // brandCodeImage to save folder in server image
            String brandCodeImage = "";
            /*BOE  hoang.ho 2014/01/21 */
//            String brandCode = entBrand.getBrandCode();
//            brandCodeToReturn = brandCode;
            Integer brandCodeInt = 0;
            if (entBrand.getBrandCode() != 0) {
                brandCodeInt = entBrand.getBrandCode();
                brandCodeToReturn = brandCodeInt;
            }
            /*EOE  hoang.ho 2014/01/21 */
            entBrand.setTourokuUserId(this.getCommonForm().getLoginId());
            entBrand.setKousinUserId(this.getCommonForm().getLoginId());
            /*BOE  hoang.ho 2014/01/21 */
            // get param from checkbox to check delflg or no
            String delflg = this.getRequest().getParameter(DEL_FLG_BRAND);
            boolean paramValidFlg = (StringUtils.isNotEmpty(delflg)) && (StringUtils.isNumeric(delflg));
            if (paramValidFlg) {
                entBrand.setDelFlg(Integer.parseInt(delflg));
            }
//            if (StringUtils.isNotEmpty(delflg)) {
//            	entBrand.setDelFlg(delflg);
//            }
            /*BOE  thai.son 2014/01/21 */
            // if  brandCode != null  => process for update function
//            if (null ! =brandCode)) {
//            	brandCodeImage = brandCode;
            if (null != brandCodeInt &&  brandCodeInt > 0) {
                 result = updateBrand(entBrand);
            	 brandCodeImage = brandCodeInt + "";
                /*EOE  hoang.ho 2014/01/21 */
            	if (result.equals("updated")) {
            		setMessageParamSuccess(MODE_BRAND_UPDATE);
                	editBrandActionForm.setFailMessage("");
                	/*BOE:check the brand data of the target has not been updated, when updating :Thai.Son 2014/01/17*/
            	} else if (result.equals("errorOthersUpdate")) {
            	   /* attr = (EntMstAttribute) session.get(Constant.SESSION_ATTRIBUTE);
                    // clear session.
                    session.remove(Constant.SESSION_ATTRIBUTE);
                    errorMsg = this.getText("message.error.othersUpdate");
                    errorMsg = String.format(errorMsg, attr.getUpdatedUserId(), attr.getUpdatedOn());*/
            	    EntMstBrand entMstBrand = editBrandActionForm.getEntBrand();
            	    String errorMsg = "";
            	    errorMsg = getText("message.error.othersUpdate");
            	    errorMsg = String.format(errorMsg, entMstBrand.getKousinUserId(), entMstBrand.getKousinDateString());
            	    editBrandActionForm.setFailMessage(errorMsg);
            	    /*EOE:check the brand data of the target has not been updated, when updating :Thai.Son 2014/01/17*/
            	} else {
            /*EOE  thai.son 2014/01/21 */
                	editBrandActionForm.setFailMessage(getText("message.error.updateFail"));
            	}
            // if  brandCode == null  => process for insert function
            } else {
            	int brandCodeGenerate = insertNewBrand(entBrand);
            	if (brandCodeGenerate > 0) {
            		result = "inserted";
            		setMessageParamSuccess(MODE_BRAND_INSERT);
                	editBrandActionForm.setFailMessage("");
            		brandCodeImage = String.valueOf(brandCodeGenerate);
            	} else {
            		result = "errorInsert";
                	editBrandActionForm.setFailMessage(getText("message.success.insertFail"));
				}
            }

            // copy or edit image from tmp folder to real folder
            if (StringUtils.isNotEmpty(brandCodeImage) && StringUtils.isNotEmpty(editBrandActionForm.getEntBrand().getLogo())) {
            	String loginId = this.getCommonForm().getLoginId();

    			// check exist folder tmp
    			File folderTmp = new File(filePath, Constant.FOLDER_BRAND_IMAGE);
    			if (!folderTmp.exists()) {
    				FileUtils.forceMkdir(folderTmp);
    			}

    			// copy file to tmp folder
    			File fileFrom = new File(filePath + Constant.FOLDER_TMP_UPLOAD
    									, loginId + Constant.CHAR_BETWEEN_USER_IMAGE
    										+ editBrandActionForm.getEntBrand().getLogo());
    			File fileToCreate = new File(filePath + Constant.FOLDER_BRAND_IMAGE
    									, Constant.BRAND + brandCodeImage + Constant.DOT + Constant.IMAGE_TYPE_GIF);

    			if (fileFrom.exists()) {
    				FileUtils.copyFile(fileFrom, fileToCreate);
    				FileUtils.forceDelete(fileFrom);
    			}
    		    // upload logo Image again in Database
                brandService.updateBrandLogo(brandCodeImage, Constant.BRAND + brandCodeImage + Constant.DOT + Constant.IMAGE_TYPE_GIF);
            }

            //this case process when delete image in real server
            File fileToDelete = new File(filePath + Constant.FOLDER_BRAND_IMAGE , Constant.BRAND + brandCodeImage + Constant.DOT + Constant.IMAGE_TYPE_GIF);
            /*BOE  hoang.ho 2014/01/21 */
//            if (StringUtils.isNotEmpty(brandCode) && fileToDelete.exists()
//                && StringUtils.isEmpty(editBrandActionForm.getEntBrand().getLogo())) {
            /*BOE  thai.son 2014/01/21 */
            if ((null != brandCodeInt && brandCodeInt > 0) && fileToDelete.exists()
                && StringUtils.isEmpty(editBrandActionForm.getEntBrand().getLogo())) {
            /*EOE  thai.son 2014/01/21 */
                FileUtils.forceDelete(fileToDelete);
            }
            /*EOE  hoang.ho 2014/01/21 */

        } catch (DataAccessException e) {
            this.getLogger().error(CURRENT_CLASS + " An error to get data in "
                                    + "execute function");
            this.getLogger().error(e.toString());
            return "error";
        } catch (Exception e) {
            this.getLogger().error(CURRENT_CLASS + " An error execute function");
            this.getLogger().error(e.toString());
            return "errorOther";
        }
		editBrandActionForm.setInsertStatus(true);

		if (StringUtils.isNotEmpty(messageParamSuccess)) {
		    /*BOE  Thai.Son 2014/01/17*/
		    session.put(Constant.SESSION_MSG_CODE, messageParamSuccess);
		    /*EOE  Thai.Son 2014/01/17*/
			return "brandMasterManager";
		} else {
			return "editbrand";
		}

	}

    /************************************************************************
     * <b>Description:</b><br>
     * Check duplicate brand name by brand name and brand code.
     *
     * @author      hoang.ho
     * @since        2014/02/18
     * @return      String
     ************************************************************************/
    @Action(value = "ajaxCheckExistBrandName", results = { @Result(name = "ajaxCheckExistBrandName", type = "json") })
    public String ajaxCheckExistBrandName() {
        EntMstBrand brand = editBrandActionForm.getEntBrand();
        if (brand != null) {
            boolean hasBrand = brandService.checkExistBrandByCodeName(brand);
            editBrandActionForm.setExistBrand(hasBrand);
        }
        return "ajaxCheckExistBrandName";
    }

    /************************************************************************
     * <b>Description:</b><br>
     *  Prepare data and validate before to save database.
     *  1. Cut brand-name at character No.50 (if any)
     *  2. Cut hyouji_name1 at character No.50 (if any)
     *  3. Cut hyouji_name2 at character No.50 (if any)
     *  4. Cut hyouji_name3 at character No.50 (if any)
     *  5. Cut sort_kana_name at character No.50 (if any)
     *  6. Cut logo at character No.255 (if any)
     *  7. Cut url at character No.255 (if any)
     *  8. Cut rss at character No.255 (if any)
     * @author      hoang.ho
     * @since       2014/02/18
     ************************************************************************/
    private void validateAndPrepareData() {

        // Get data and check null object value.
        EditBrandActionForm form = this.editBrandActionForm;
        if (form == null) {
            return;
        }
        EntMstBrand entBrand = form.getEntBrand();
        if (entBrand == null) {
            return;
        }
        //1. Cut brand-name at character No.50 (if any)
        String brandName = form.getEntBrand().getName();
        form.getEntBrand().setName(Utils.cutStringByLength(brandName, Constant.BRAND_NAME_MAX_LENGTH));

        //2. Cut hyouji_name1 at character No.50 (if any)
        String hyoujiName1 = form.getEntBrand().getHyoujiName1();
        form.getEntBrand().setHyoujiName1(Utils.cutStringByLength(hyoujiName1, Constant.BRAND_HYOUJI_NAME1_LENGTH));

        //3. Cut hyouji_name2 at character No.50 (if any)
        String hyoujiName2 = form.getEntBrand().getHyoujiName2();
        form.getEntBrand().setHyoujiName2(Utils.cutStringByLength(hyoujiName2, Constant.BRAND_HYOUJI_NAME2_LENGTH));

        //4. Cut hyouji_name3 at character No.50 (if any)
        String hyoujiName3 = form.getEntBrand().getHyoujiName3();
        form.getEntBrand().setHyoujiName3(Utils.cutStringByLength(hyoujiName3, Constant.BRAND_HYOUJI_NAME3_LENGTH));

        //5. Cut sort_kana_name at character No.50 (if any)
        String sortKanaName = form.getEntBrand().getSortKanaName();
        form.getEntBrand().setSortKanaName(Utils.cutStringByLength(sortKanaName, Constant.BRAND_SORT_KANA_NAME_LENGTH));

        //6. Cut logo at character No.255 (if any)
        String logo = form.getEntBrand().getLogo();
        form.getEntBrand().setLogo(Utils.cutStringByLength(logo, Constant.BRAND_LOGO_LENGTH));

        //7. Cut url at character No.255 (if any)
        String url = form.getEntBrand().getUrl();
        form.getEntBrand().setUrl(Utils.cutStringByLength(url, Constant.BRAND_URL_LENGTH));

        //8. Cut rss at character No.255 (if any)
        String rss = form.getEntBrand().getRss();
        form.getEntBrand().setRss(Utils.cutStringByLength(rss, Constant.BRAND_RSS_LENGTH));
    }

	/************************************************************************
	 * <b>Description:</b><br>
	 *  previewImage.
	 *
	 * @author		Le.Dinh
	 * @date		2013/01/02
	 * @return		String
	 ************************************************************************/
	@Action("/previewImage")
	public String previewImage() {
		// get real path
		String filePath = Utils.getRealPath(this.getRequest());
		// Get login Id
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String loginId = getLoginId(authentication);
		try {

			imageUpload = new ImageUpload();

			// validation : type only gif and wight = 140 and height = 140
			String checkImage = this.validateImage(userImage, userImageContentType, userImageFileName, Constant.IMAGE_WIDTH, Constant.IMAGE_HEIGHT);
			if (StringUtils.isNotEmpty(checkImage)) {
				imageUpload.setImageError(checkImage);
				return "previewImage";
			}

			// check exist folder tmp
			File folderTmp = new File(filePath, Constant.FOLDER_TMP_UPLOAD);
			if (!folderTmp.exists()) {
				FileUtils.forceMkdir(folderTmp);
			}

			// copy file to tmp folder
			File fileToCreate = new File(filePath + Constant.FOLDER_TMP_UPLOAD,
										 loginId + Constant.CHAR_BETWEEN_USER_IMAGE + this.userImageFileName);
			FileUtils.copyFile(this.userImage, fileToCreate);
		} catch (Exception e) {
            this.getLogger().error(CURRENT_CLASS + " An error when preview image");
			this.getLogger().error(e.toString());
			imageUpload.setImageError(getText("message.error.imageLogoSize") + Constant.HYPHEN + getText("message.error.imageOnlyGif"));
			return "previewImage";
		}

		// set data of image upload for result json
		imageUpload.setImageWidth(Constant.IMAGE_WIDTH);
		imageUpload.setImageHeight(Constant.IMAGE_HEIGHT);
		imageUpload.setImageName(userImageFileName);
		imageUpload.setImageType(userImageContentType);

		// remove all file not use in tmp folder
		File folder = new File(filePath + Constant.FOLDER_TMP_UPLOAD);
		if (folder.exists()) {
			String allFileOfUser = loginId + Constant.CHAR_BETWEEN_USER_IMAGE;
			String currentFileUser = loginId + Constant.CHAR_BETWEEN_USER_IMAGE + userImageFileName;
			Utils.getInstance().removeImageNotUseInTMP(folder, allFileOfUser, currentFileUser);
		}

		return "previewImage";
	}

	/************************************************************************
	 * <b>Description:</b><br>
	 *  delete Image Preview.
	 *
	 * @author		Le.Dinh
	 * @date		2013/01/02
	 * @return		String
	 ************************************************************************/
	@Action("/deleteImagePreview")
	public String deleteImagePreview() {

        // Get login Id
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String loginId = getLoginId(authentication);

		String imageName = this.getRequest().getParameter(IMAGE_NAME);
		String filePath = Utils.getRealPath(this.getRequest());

		File fileToDelete = new File(filePath + Constant.FOLDER_TMP_UPLOAD , loginId + Constant.CHAR_BETWEEN_USER_IMAGE + imageName);

		try {
			if (fileToDelete.exists()) {
				FileUtils.forceDelete(fileToDelete);
			}
		} catch (IOException e) {
            this.getLogger().error(CURRENT_CLASS + " An error when delete preview image");
			this.getLogger().error(e.toString());
		}
		return "deleteImage";
	}

	/************************************************************************
	 * <b>Description:</b><br>
	 *  validateImage only file GIF and width = height = 140.
	 *
	 * @author		Le.Dinh
	 * @date		2013/01/02
	 * @param		file	File
	 * @param		imgType	String
	 * @param		imgName	String
	 * @param		width	int
	 * @param		height	int
	 * @return		boolean
	 * @throws 		Exception exception
	 ************************************************************************/
	private String validateImage(File file, String imgType, String imgName, int width, int height) throws Exception {
		StringBuilder resultErrow = new StringBuilder("");
		boolean isSize = ValidateUtils.checkSizeOfImageFileS(file, width, height);
		boolean isType = ValidateUtils.checkTypeOfImageFiles(imgType, imgName);
		boolean flgToAddSpecialChar = false;
		if (!isSize) {
			resultErrow.append(getText("message.error.imageLogoSize"));
			flgToAddSpecialChar = true;
		}
		if (!isType) {
			if (flgToAddSpecialChar) {
				resultErrow.append("-");
			}
			resultErrow.append(getText("message.error.imageOnlyGif"));
		}
		return resultErrow.toString();
	}

    /************************************************************************
     * <b>Description:</b><br>
     *  update infomation brand.
     *
     * @author		Tran.Thanh
     * @date		31 Dec 2013
     * @param 		entBrand entity brand.
     * @return		String
     ************************************************************************/
    public String updateBrand(EntMstBrand entBrand) {
        /*BOE  thai.son 2014/01/21 */
        /*BOE:check the brand data of the target has not been updated, when updating :Thai.Son 2014/01/17*/
        EntMstBrand entBrandOfThisUserHaveKousinDate = (EntMstBrand) session.get(Constant.SESSION_BRAND_EDIT);
        //session.remove(Constant.SESSION_BRAND_EDIT);
        //entBrand have no Kousin Date:
        /*BOE  hoang.ho 2014/01/21 */
//        String brandCode = entBrand.getBrandCode();
        Integer brandCodeInt = entBrand.getBrandCode();
        EntMstBrand entMstBrandQuery = new EntMstBrand();
//        entMstBrandQuery.setBrandCode(brandCode);
        entMstBrandQuery.setBrandCode(brandCodeInt);
        /*EOE  hoang.ho 2014/01/21 */
        try {
            EntMstBrand entBrandInDBNowHaveKounsinDate = brandService.selectBrandByBrandCode(entMstBrandQuery);
            if (entBrandOfThisUserHaveKousinDate != null && entBrandInDBNowHaveKounsinDate != null) {
                if (!entBrandInDBNowHaveKounsinDate.getKousinDate().equals(entBrandOfThisUserHaveKousinDate.getKousinDate())) {
                    editBrandActionForm.setEntBrand(entBrandInDBNowHaveKounsinDate);
                    return "errorOthersUpdate";
                }
            }
        /*EOE  thai.son 2014/01/21 */
        } catch (SQLException e) {
            this.getLogger().error(CURRENT_CLASS + " An error execute function updateBrand");
            this.getLogger().error(e.toString());
            return "errorUpdate";
        }
        /*EOE:check the brand data of the target has not been updated, when updating :Thai.Son 2014/01/17*/
        try {
            brandService.updateBrand(entBrand);
        } catch (DataAccessException e) {
            this.getLogger().error(CURRENT_CLASS + " An error to updateBrand in"
                                    + "updateBrand function");
            this.getLogger().error(e.toString());
            return "errorUpdate";
        } catch (Exception e) {
            this.getLogger().error(CURRENT_CLASS + " An error execute function updateBrand");
            this.getLogger().error(e.toString());
            return "errorUpdate";
        }
        return "updated";
    }


    /************************************************************************
     * <b>Description:</b><br>
     *  insert new brand.
     *
     * @author		Tran.Thanh
     * @date		31 Dec 2013
     * @param 		entBrand entity brand.
     * @return		int
     ************************************************************************/
    public int insertNewBrand(EntMstBrand entBrand) {
    	int brandCodeGenerate = 0;
    	try {
        	brandCodeGenerate = brandService.insertNewBrand(entBrand);
        } catch (DataAccessException e) {
            this.getLogger().error(CURRENT_CLASS + " An error to insertBrand in"
                                    + "insertBrand function");
            this.getLogger().error(e.toString());
        } catch (Exception e) {
            this.getLogger().error(CURRENT_CLASS + " An error execute function insertBrand");
            this.getLogger().error(e.toString());
        }
        return brandCodeGenerate;
    }

	/**
	 * @return the editBrandActionForm
	 */
	public EditBrandActionForm getEditBrandActionForm() {
		return editBrandActionForm;
	}

	/**
	 * @param editBrandActionForm the editBrandActionForm to set
	 */
	public void setEditBrandActionForm(EditBrandActionForm editBrandActionForm) {
		this.editBrandActionForm = editBrandActionForm;
	}
	/**
	 * @return the userImage
	 */
	public File getUserImage() {
		return userImage;
	}

	/**
	 * @param userImage the userImage to set
	 */
	public void setUserImage(File userImage) {
		this.userImage = userImage;
	}

	/**
	 * @return the userImageContentType
	 */
	public String getUserImageContentType() {
		return userImageContentType;
	}

	/**
	 * @param userImageContentType the userImageContentType to set
	 */
	public void setUserImageContentType(String userImageContentType) {
		this.userImageContentType = userImageContentType;
	}

	/**
	 * @return the userImageFileName
	 */
	public String getUserImageFileName() {
		return userImageFileName;
	}

	/**
	 * @param userImageFileName the userImageFileName to set
	 */
	public void setUserImageFileName(String userImageFileName) {
		this.userImageFileName = userImageFileName;
	}

	/**
	 * @return the resultString
	 */
	public String getResultString() {
		return resultString;
	}

	/**
	 * @param resultString the resultString to set
	 */
	public void setResultString(String resultString) {
		this.resultString = resultString;
	}

	/**
	 * @return the imageUpload
	 */
	public ImageUpload getImageUpload() {
		return imageUpload;
	}

	/**
	 * @param imageUpload the imageUpload to set
	 */
	public void setImageUpload(ImageUpload imageUpload) {
		this.imageUpload = imageUpload;
	}

	public String getMessageParamSuccess() {
		return messageParamSuccess;
	}
	/*BOE  thai.son 2014/01/21 */
	/**
     * @return the brandCodeToReturn
     *//*
    public int getBrandCodeToReturn() {
        return brandCodeToReturn;
    }

    *//**
     * @param brandCodeToReturn the brandCodeToReturn to set
     *//*
    public void setBrandCodeToReturn(int brandCodeToReturn) {
        this.brandCodeToReturn = brandCodeToReturn;
    }*/

    public void setMessageParamSuccess(String messageParamSuccess) {
		this.messageParamSuccess = messageParamSuccess;
	}
	public Integer getBrandCodeToReturn() {
        return brandCodeToReturn;
    }

    public void setBrandCodeToReturn(Integer brandCodeToReturn) {
        this.brandCodeToReturn = brandCodeToReturn;
    }
    /*EOE  thai.son 2014/01/21 */
    /*BOE  Thai.Son 2014/01/17*/
    @Override
    public void setSession(Map<String, Object> session) {
        this.session = session;
    }
	/*EOE  Thai.Son 2014/01/17 */
}
