/**
 * File Name ： CategoryManageAction.java
 * Author ： hoang.ho
 * Version ： 1.0.0
 * Date Created ： 2014/01/07
 * Date Updated ： 2014/01/07
 * Description ： action to manage category list.
*/
package net.webike.RcProductFactory.action;

import java.util.ArrayList;
import java.util.List;

import net.webike.RcProductFactory.action.form.CategoryManageActionForm;
import net.webike.RcProductFactory.entity.EntMstAttribute;
import net.webike.RcProductFactory.entity.EntMstCategory;
import net.webike.RcProductFactory.service.CategoryService;

import org.apache.commons.lang.StringUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;

/**
 * action of manage category list.
 * @author hoang.ho
 * Create date ： 2014/01/07
 */
@Scope("request")
@ParentPackage(value = "CommonInterceptor")
@Results(
    {
        // On loading default matter detail page
        @Result(name = "success", location = "categoryManage.jsp"),
        //Return json
        @Result(name = "list", type = "json"),
        // Return error page when parameter fail
        @Result(name = "error", type = "redirect", location = "error.html?error=param"),
        // Return error page when parameter fail
        @Result(name = "errorNoPermission", type = "redirect", location = "error.html?error=noPermission"),
        // Return error page when error other.
        @Result(name = "errorOther", type = "redirect", location = "error.html")
    }
)
public class CategoryManageAction extends CommonAction {

    /**
     * Used for serialization object.
     */
    private static final long serialVersionUID = 1L;

    @Autowired
    private CategoryManageActionForm categoryManageActionForm;

    @Autowired
    private CategoryService categoryService;

    /**
     * Number product in 1 page.
     */
    private String page;
    /**
     * Page number to get product data.
     */
    private String pageSize;
    /**
     * host address.
     */
    private String hostAddress;
    /**
     * First load page.
     * @return screen mapping name
     */
    @Override
    @Action("/categoryManage")
    public String execute() {
        // 1. Validate authentication
        if (!validateLoginUser()) {
            return "error";
        }
      //Get Request url
        String url = this.getRequest().getRequestURL().toString();

        if (StringUtils.isNotEmpty(url)) {
            //Remove URI
            int idx = url.lastIndexOf("/");
            hostAddress = url.substring(0, idx);
        }
        // 2. Initial data on screen.
        initScreen();
        return "success";
    }

    /**
     * Get category list by search conditions.
     * @return screen mapping name
     */

    @Action("/ajaxGetCategoryList")
    public String ajaxGetCategoryList() {
        // 1. Prepare paging information.
        if (!preparePagination()) {
            return "error";
        }
        // 2. Prepare conditions.
        boolean isSearch = prepareSearchCondition();
        List<EntMstCategory> categoryManageList = null;
        int categoryManageListCount = 0;
        if (isSearch) {
            // 3. Get category list by conditions from form.
            EntMstCategory searchConditions = this.categoryManageActionForm.getEntMstCategory();
            categoryManageList = categoryService.selectCategoryManageList(searchConditions);
            categoryManageListCount = categoryService.selectCategoryManageListCount(searchConditions);
        } else {
            categoryManageList = new ArrayList<EntMstCategory>();
        }

        // 4. Set data into form.
        categoryManageActionForm.setCount(categoryManageListCount);
        categoryManageActionForm.setEntMstCategoryList(categoryManageList);
        return "list";
    }

    /**
     * Initial screen.
     * <br/>
     * 1. Initial attribute drop-down list.
     * 2. Get message insert/update from category edit page (if any)
     */
    private void initScreen() {
        try {
            // 1. Get attribute list.
            List<EntMstAttribute> entMstAttributeList = categoryService.selectAttributeList();
            categoryManageActionForm.setEntMstAttributeList(entMstAttributeList);

            // 2.Get message insert/update from category edit page (if any)
            String msgCode = this.getRequest().getParameter("msgCode");
            if (StringUtils.isNotBlank(msgCode)) {
                if ("1".equals(msgCode)) {
                    categoryManageActionForm.setMessage(this.getText("message.success.insertSuccess"));
                } else if ("0".equals(msgCode)) {
                    categoryManageActionForm.setMessage(this.getText("message.success.updateSuccess"));
                }
            }
        } catch (Exception e) {
            this.getLogger().error("[CategoryManageAction - prepareSearchCondition] error occurs in execute");
            this.getLogger().error(e.toString());
        }
    }

    /**
     * Prepare pagination data.
     * @return boolean
     */
    private boolean preparePagination() {
        try {
            // Check missing params pageSize and pageNumber
            if (StringUtils.isEmpty(pageSize) || StringUtils.isEmpty(page)) {
                // Return page error params
                return false;
            }
            Integer pageSizeInt = Integer.parseInt(pageSize);
            Integer pageInt = Integer.parseInt(page);

            EntMstCategory searchConditions = this.categoryManageActionForm.getEntMstCategory();

            // Put information of paging in search conditions.
            if (searchConditions == null) {
                searchConditions = new EntMstCategory();
                this.categoryManageActionForm.setEntMstCategory(searchConditions);
            }
            searchConditions.setOffset(((pageInt - 1) * pageSizeInt));
            searchConditions.setLimit(pageSizeInt);
            return true;
        } catch (Exception e) {
            this.getLogger().error("[CategoryManageAction - prepareSearchCondition] error occurs in execute");
            this.getLogger().error(e.toString());
            return false;
        }
    }

    /**
     * Prepare search conditions.
     * @return boolean true : search by conditions,
     *                  false : not search with current conditions.
     */
    private boolean prepareSearchCondition() {
        try {
            // Get search conditions from action form.
            EntMstCategory searchConditions = this.categoryManageActionForm.getEntMstCategory();
            boolean filterFlg = true;
            if (null == searchConditions) {
                return true;
            }

            // Check search information had filled or not.
            if (StringUtils.isBlank(searchConditions.getCategoryCode())
                && StringUtils.isBlank(searchConditions.getCategoryName())
                && StringUtils.isBlank(searchConditions.getBunruiCode())
                && StringUtils.isBlank(searchConditions.getBunruiName())
                && StringUtils.isBlank(searchConditions.getCategoryAttributeCountFrom())
                && StringUtils.isBlank(searchConditions.getCategoryAttributeCountTo())
                && StringUtils.isBlank(searchConditions.getCategoryDelFlg())
                && StringUtils.isBlank(searchConditions.getUpdatedFrom())
                && StringUtils.isBlank(searchConditions.getUpdatedTo())
                && StringUtils.isBlank(searchConditions.getAttributeCodes())) {
                filterFlg = false;
            }

            // Get category codes from attribute code
            if (!StringUtils.isBlank(searchConditions.getAttributeCodes())) {
                String categoryCodes = categoryService.selectCategoryCodes(categoryManageActionForm.getEntMstCategory().getAttributeCodes());
                if (StringUtils.isBlank(categoryCodes)) {
                    return false;
                } else {
                    categoryManageActionForm.getEntMstCategory().setCategoryCodes(categoryCodes);
                }
            }

            // Set filter flag if search conditions has value.
            this.categoryManageActionForm.getEntMstCategory().setFilterFlg(filterFlg);
        } catch (Exception e) {
            this.getLogger().error("[CategoryManageAction - prepareSearchCondition] error occurs in execute");
            this.getLogger().error(e.toString());
            return false;
        }
        return true;
    }

    /**
     * @return the categoryManageActionForm
     */
    public CategoryManageActionForm getCategoryManageActionForm() {
        return categoryManageActionForm;
    }

    /**
     * @param categoryManageActionForm the categoryManageActionForm to set
     */
    public void setCategoryManageActionForm(CategoryManageActionForm categoryManageActionForm) {
        this.categoryManageActionForm = categoryManageActionForm;
    }

    /**
     * @return the page
     */
    public String getPage() {
        return page;
    }

    /**
     * @param page the page to set
     */
    public void setPage(String page) {
        this.page = page;
    }

    /**
     * @return the pageSize
     */
    public String getPageSize() {
        return pageSize;
    }

    /**
     * @param pageSize the pageSize to set
     */
    public void setPageSize(String pageSize) {
        this.pageSize = pageSize;
    }

    /**
     * @return the hostAddress
     */
    public String getHostAddress() {
        return hostAddress;
    }

    /**
     * @param hostAddress the hostAddress to set
     */
    public void setHostAddress(String hostAddress) {
        this.hostAddress = hostAddress;
    }

}
