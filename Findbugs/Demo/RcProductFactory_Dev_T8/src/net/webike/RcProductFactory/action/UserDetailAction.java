/************************************************************************
 * File Name    ： UserDetail.java
 * Author        ： Tran.Thanh
 * Version        ： 1.0.0
 * Date Created    ： 2013/10/03
 * Date Updated    ： 2013/10/03
 * Description    ： User detail.
 *
 ***********************************************************************/
package net.webike.RcProductFactory.action;

import java.util.List;

import net.webike.RcProductFactory.action.form.UserDetailActionForm;
import net.webike.RcProductFactory.entity.EntMstFactoryUserTmp;
import net.webike.RcProductFactory.entity.UserAuthority;
//import net.webike.RcProductFactory.service.UserService;
import net.webike.RcProductFactory.service.UserTmpService;
import net.webike.RcProductFactory.util.Constant;
import net.webike.RcProductFactory.util.Utils;
import net.webike.RcProductFactory.util.ValidateUtils;

import org.apache.commons.lang.StringUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Controller;

/**
 * PJ Detail Action Class.
 */
@Controller
@Scope("request")
@ParentPackage(value = "CommonInterceptor")
@Results(
    {
        // On loading default user manage page
        @Result(name = "success", location = "userDetail.jsp", params = {"userId", "${userId}" }),
        // After updated success return user detail page with param userId
        @Result(name = "updated", type = "redirectAction", location = "userDetail", params = {"userId", "${userId}" }),
        // Return error page when parameter fail
        @Result(name = "error", type = "redirect", location = "error.html?error=param"),
        //Return error page when no permission
        @Result(name = "errorPermission", type = "redirect", location = "error.html?error=noPermission"),
        // Return error page when parameter fail
        @Result(name = "errorOther", type = "redirect", location = "error.html")
    }
)
public class UserDetailAction extends CommonAction {
    /**
     * @serialVersionUID
     */
    private static final long serialVersionUID = 1L;
    //delete or active user: delflg.
    private static final String DEL_FLAG_DELETE = "0";
    //flag to check password change or not: flgPassChange.
    private static final String FLAG_PASS_CHANGE = "1";
    private static final String FLAG_PASS_NOT_CHANGE = "0";
    /**
     * @userService userService
     */
    @Autowired
    private UserTmpService userTmpService;

    @Autowired
    private UserDetailActionForm userDetailActionForm;

    // userId of user get from parameter, not userId of user's login
    private String userId;

    @Override
    @Action("/userDetail")
    public String execute() {
        try {
            // Validate loginUser
            if (!validateLoginUser()) {
                return "errorOther";
            }
            // Get authority from commonForm
            //String authority = this.getCommonForm().getLoginAuthority(); // Thanh - oldcode
            List<String> authority2 =  this.getCommonForm().getLoginAuthority(); // Thanh - newcode
            // Set List All User To From
            userId = this.getRequest().getParameter(Constant.PARAM_USER_ID);
            //Check userId parameter
            if (StringUtils.isEmpty(userId)) {
                return "error";
            }
            // Get loginId
            String kousinId = this.getCommonForm().getLoginId();

            userDetailActionForm.setAuthorityTreeMap(UserAuthority.getListTreeMap());
    		//Check permission of current login
    		if (!userId.equals(kousinId)) {
    			//if (!authority.equals(Constant.ADMIN_AUTHORITY)) { // Thanh - oldcode
    			if (!authority2.contains(getText("text.role.userManagerRoleId"))) {
    				return "errorPermission";
    			}
    		}

    		// Check permission of authority
    		//if (authority.equals(Constant.OPERATOR_AUTHORITY)) {// Thanh-oldcode
//    		if (!authority2.contains(Constant.USER_MANAGER_ROLE_ID)) {
//    			TreeMap<String, String> mapTemp = new TreeMap<String, String>();
//    			mapTemp.put(Constant.OPERATOR_AUTHORITY, UserAuthority.getName(Constant.OPERATOR_AUTHORITY));
//    			userDetailActionForm.setAuthorityTreeMap(mapTemp);
//    		}

    		// Select info user by userId
	    	EntMstFactoryUserTmp entUserTmp = userTmpService.selectUserTmpById(userId);
	    	if (entUserTmp == null) {
        		return "error";
        	}

	    	// if update status then set message to null
	        if (!userDetailActionForm.getUpdateStatus()) {
	            userDetailActionForm.setSuccessMessage(null);
	            userDetailActionForm.setFailMessage(null);
	        }
	        userDetailActionForm.setUpdateStatus(false);

	        // Set entUser, action name, submit action, title, tabid to ActionForm
	    	userDetailActionForm.setEntUserTmp(entUserTmp);
	    	userDetailActionForm.setActionName("userDetail");
	    	userDetailActionForm.setSubmitActionName("updateInfoUser.html");
	    	userDetailActionForm.setH1Title(getText("userDetail.label.userDetailH1"));
	    	userDetailActionForm.setDetailTabId("rightTab");
    	} catch (DataAccessException e) {
            this.getLogger().error("[userDetailAction] An error to get data in "
                                    + "execute function");
            this.getLogger().error(e.toString());
            return "error";
        } catch (Exception e) {
            this.getLogger().error("[userDetailAction] An error execute function");
            this.getLogger().error(e.toString());
            return "errorOther";
        }
        return SUCCESS;
    }

    /************************************************************************
     * <b>Description:</b><br>
     *  Change status user. If del_flg of user is 0 then set del_flg is 1.
     *  If del_flg of user is 1 then set del_flg is 0.
     *
     * @author        Tran.Thanh
     * @date        3 Oct 2013
     * @return        String
     ************************************************************************/
    @Action("/changeStatusDetail")
    public String changeStatusDetail() {
        try {
            //Validate loginUser
            if (!validateLoginUser()) {
                return "errorOther";
            }

            // Get userId from requset and check emty
            userId = this.getRequest().getParameter(Constant.PARAM_USER_ID);
            String kousinId = this.getCommonForm().getLoginId();
            String delFlg = this.getRequest().getParameter(Constant.PARAM_DEL_FLAG);
            if (StringUtils.isEmpty(userId) || StringUtils.isEmpty(kousinId)
                                            || StringUtils.isEmpty(delFlg)) {
                return "error";
            }

            // Check have del flg then detele user by userId
            if (DEL_FLAG_DELETE.equals(delFlg)) {
                userTmpService.deleteUserByUserId(userId, kousinId);
                userDetailActionForm.setSuccessMessage(getText("message.success.lockSuccess"));
            } else { // Active user By userId
                userTmpService.activeUserByUSerId(userId, kousinId);
                userDetailActionForm.setSuccessMessage(getText("message.success.activeSuccess"));
            }
        } catch (DataAccessException e) {
            this.getLogger().error("[UserManageAction] An error to changeStatus"
                                    + "changeStatus function");
            this.getLogger().error(e.toString());
            return "error";
        } catch (Exception e) {
            this.getLogger().error("[UserManageAction] An error execute function changeStatus");
            this.getLogger().error(e.toString());
            return "error";
        }
        userDetailActionForm.setUpdateStatus(true);
        return "updated";
    }

    /************************************************************************
     * <b>Description:</b><br>
     *  update User Info.
     *
     * @author        Tran.Thanh
     * @date        4 Oct 2013
     * @return        String
     ************************************************************************/
    @Action("/updateInfoUser")
    public String updateUserInfo() {
        try {
            //Validate loginUser
            if (!this.validateLoginUser()) {
                return "errorOther";
            }
        	EntMstFactoryUserTmp entUserTmp = userDetailActionForm.getEntUserTmp();
        	// Get Api AccessKey
            String apiKey = this.getRequest().getParameter("userRedmineApiKeyHidden");
            if (StringUtils.isEmpty(apiKey)) {
            	entUserTmp.setUserRedmineApiKey(apiKey);
            }

    		// Get param from request
        	entUserTmp.setUpdatedUserId(this.getCommonForm().getLoginId());
	        String flgPassChange = this.getRequest().getParameter(Constant.PARAM_FLAG_PASS_CHANGE);

	        //Check required field
	        if (StringUtils.isEmpty(entUserTmp.getUserId())
	                || StringUtils.isEmpty(entUserTmp.getUserFirstName())
                    || StringUtils.isEmpty(entUserTmp.getUserLastName())
	                || StringUtils.isEmpty(entUserTmp.getUpdatedUserId())) {
	        	return "error";
	        }

	        // If had no change password, set UserPasswd is ""
	        if (flgPassChange.equals(FLAG_PASS_NOT_CHANGE)) {
	        	entUserTmp.setUserPasswd("");
	        }

	        //Validate each fields of ent
	        if (ValidateUtils.validateUserEnt(entUserTmp)) {
	        	//Check permission of current login
	    		if (!entUserTmp.getUserId().equals(entUserTmp.getUpdatedUserId())) {
	    			//if (this.getCommonForm().getLoginAuthority().equals(Constant.OPERATOR_AUTHORITY)) {
	    			if (!this.getCommonForm().getLoginAuthority().contains(getText("text.role.userManagerRoleId"))) {
	    				return "errorPermission";
	    			}
	    		}
	    		// Convert String password to Md5
	    		if (!StringUtils.isEmpty(entUserTmp.getUserPasswd()) && flgPassChange.equals(FLAG_PASS_CHANGE)) {
		        	String passEncryption = Utils.convertStringToMd5(entUserTmp.getUserPasswd());
		        	entUserTmp.setUserPasswd(passEncryption);
		        } else {
		        	entUserTmp.setUserPasswd(null);
		        	entUserTmp.setUserPasswdConfirm(null);
		        }
	    		// Set UserId and Update User Info
	    		userId = entUserTmp.getUserId();
		        userTmpService.updateUserInfo(entUserTmp);
		        userDetailActionForm.setSuccessMessage(getText("message.success.updateSuccess"));
	            userDetailActionForm.setFailMessage(null);
		        userDetailActionForm.setUpdateStatus(true);
            } else {
            	userDetailActionForm.setSuccessMessage(null);
            	userDetailActionForm.setFailMessage(getText("message.error.updateFail"));
                userDetailActionForm.setUpdateStatus(true);
            }
    	} catch (DataAccessException e) {
            this.getLogger().error("[updateInfoUser] An error to update user info "
                                    + "updateInfoUser function");
            this.getLogger().error(e.toString());
            return "error";
        } catch (Exception e) {
            this.getLogger().error("[updateInfoUser] An error to update user info");
            this.getLogger().error(e.toString());
            return "error";
        }
        return "updated";
    }

    /**
     * @return the userService
     */
//    public UserService getUserService() {
//        return userService;
//    }

    /**
     * @param userService the userService to set
     */
//    public void setUserService(UserService userService) {
//        this.userService = userService;
//    }

    /**
     * @return the userDetailActionForm
     */
    public UserDetailActionForm getUserDetailActionForm() {
        return userDetailActionForm;
    }

    /**
     * @param userDetailActionForm the userDetailActionForm to set
     */
    public void setUserDetailActionForm(UserDetailActionForm userDetailActionForm) {
        this.userDetailActionForm = userDetailActionForm;
    }

    /**
     * @return the userId
     */
    public String getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }
}
