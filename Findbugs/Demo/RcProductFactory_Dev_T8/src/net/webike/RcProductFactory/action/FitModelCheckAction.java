/************************************************************************
 * File Name    ： FitModelCheckAction.java
 * Author       ： Nguyen.Chuong
 * Version      ： 1.0.0
 * Date Created ： 2014/001/22
 * Description  ： action for fitModelCheck page.
 *
 ***********************************************************************/
package net.webike.RcProductFactory.action;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import net.webike.RcProductFactory.action.form.FitModelCheckActionForm;
import net.webike.RcProductFactory.entity.EntMstFactoryMatterNew;
import net.webike.RcProductFactory.entity.EntMstMaker;
import net.webike.RcProductFactory.entity.EntMstModel;
import net.webike.RcProductFactory.entity.EntTblFactoryProductFitModel;
import net.webike.RcProductFactory.entity.EntTblFactoryProductNew;
import net.webike.RcProductFactory.service.FitModelService;
import net.webike.RcProductFactory.service.MatterService;
import net.webike.RcProductFactory.service.ModelService;
import net.webike.RcProductFactory.service.ProductService;
import net.webike.RcProductFactory.util.Constant;

import org.apache.commons.lang.StringUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
/**
 * Fit model check action.
 * @author Nguyen.Chuong
 * Date Created ： 2014/01/22
 */
@Scope("request")
@ParentPackage(value = "CommonInterceptor")
@Results(
    {
        // On loading default attribute edit
        @Result(name = "success", location = "fitModelCheck.jsp"),
        // Return error page when error other.
        @Result(name = "errorOther", type = "redirect", location = "error.html"),
        // Return error page when parameter fail
        @Result(name = "errorParams", type = "redirect", location = "error.html?error=param"),
        @Result(name = "errorPermission", type = "redirect", location = "error.html?error=noPermission")
    }
)
public class FitModelCheckAction extends CommonAction {

    private static final long serialVersionUID = 1L;

    @Autowired
    private FitModelCheckActionForm fitModelCheckActionForm;

    @Autowired
    private ProductService productService;
    @Autowired
    private ModelService modelService;
    @Autowired
    private FitModelService fitModelService;
    @Autowired
    private MatterService matterService;
    //private MstFactoryMatterMapper mstFactoryMatterMapper;

    //Get request product id
    private String productId;
    // Number attribute in 1 page
    private String page;
    // Page number to get attribute data
    private String pageSize;
    private String linkToSyouhinPage = "http://www.webike.net/sd/";
    private static final String ERROR_OTHER = "errorOther";
    private static final int SUCCESS_MODE = 1;
    private static final int FAIL_MODE = 0;
    //Param name productId in constant
    private String productIdParamName = Constant.PARAM_PRODUCT_ID;

    @Override
    @Action("/fitModelCheck")
    public String execute() {
        try {
            // Validate loginUser
            if (!validateLoginUser()) {
                return ERROR_OTHER;
            }
            //Check productId isInteger
            EntTblFactoryProductNew entProduct;
            if (StringUtils.isNumeric(productId)) {
                //Get detail of product object by productId
                entProduct = productService.selectEntProductByProductId(Long.parseLong(productId));
                if (null == entProduct) {
                    return "errorParams";
                }
                //Set entProduct init to actionForm
                fitModelCheckActionForm.setProduct(entProduct);
            } else {
                return ERROR_OTHER;
            }

            /* BOE Tran.Thanh 2014/03/24
             * Validate permission authority
             */
            if (this.getCommonForm().getLoginAuthority().contains("LIMITED_USER_ID")) {
    			//form.getProductMatterNo()
            	if (entProduct.getProductMatterNo() != null) {
            	    //BOE Nguyen.Chuong 2014/04/08 modify to call service
//                    EntMstFactoryMatter entMatter = matterService.selectDetailMatterByMatterNo(entProduct.getProductMatterNo().toString());
                    EntMstFactoryMatterNew entMatter = matterService.selectFactoryMatterByMatterCode(entProduct.getProductMatterNo());
                    /*EntMstFactoryMatterNew entMatter = mstFactoryMatterMapper.selectFactoryMatterByMatterCode(entProduct.getProductMatterNo());*/
                    //EOE Nguyen.Chuong 2014/04/08 modify to call service
            		if (entMatter == null) {
            			return "errorOther";
            		} else {
            			if (this.getCommonForm().getLoginId() != null) {
                			if (!entMatter.getMatterChargeUserId().equals(this.getCommonForm().getLoginId())) {
                    			return "errorPermission";
                    		}
                		}
            		}
            	}
    		}
            /* EOE Tran.Thanh 2014/03/24*/

            //Get list image of product.
            fitModelCheckActionForm.setListImage(productService.selectListImageAndThumbByProductId(entProduct));

            //Get list model of product.
            EntTblFactoryProductFitModel entProductFitModel = new EntTblFactoryProductFitModel();
            entProductFitModel.setProductId(Long.parseLong(productId));
            entProductFitModel.setSortField("fitModelMaker");
            entProductFitModel.setSortDir("ASC");
            List<EntTblFactoryProductFitModel> listFitModel = fitModelService.selectListFitModelByProductCode(entProductFitModel);
            fitModelCheckActionForm.setListFitModelOfProduct(listFitModel);
            fitModelCheckActionForm.setListFitModelJsonStr(this.parseObjectToJson(listFitModel));

            } catch (Exception e) {
            this.getLogger().error("[FitModelCheckAction] An error execute function");
            this.getLogger().error(e.toString());
            return ERROR_OTHER;
        }
        return "success";
    }

    /************************************************************************
     * <b>Description:</b><br>
     * Load list all model grid for fitModelCheck.
     *
     * @author      hoang.ho
     * @date        Feb 12, 2014
     * @return      String Json
     ************************************************************************/
    @Action(value = "/ajaxLoadModelList", results = { @Result(name = "list", type = "json") })
    public String ajaxLoadModelList() {
        try {
            // Check missing params pageSize and pageNumber
            if (StringUtils.isEmpty(pageSize) || StringUtils.isEmpty(page)) {
                // Return page error params
                return "list";
            }
            int pageSizeInt = Integer.parseInt(pageSize);
            int pageInt = Integer.parseInt(page);
            //Get Model List and set into form
            List<EntMstModel> entMstModelList = modelService.selectListModel(pageSizeInt, pageInt
                                                                           , fitModelCheckActionForm.getEntMstModel());
            fitModelCheckActionForm.setListMstModel(entMstModelList);

            // Get model list count and set into form
            int total = modelService.selectListModelCount(fitModelCheckActionForm.getEntMstModel());
            fitModelCheckActionForm.setMstModelListCount(total);
        } catch (Exception e) {
            this.getLogger().error("[ajaxLoadModelList] An error execute function");
            this.getLogger().error(e.toString());
            return "errorOther";
        }
        return "list";
    }

    /************************************************************************
     * <b>Description:</b><br>
     * Load list all model grid for fitModelCheck.
     *
     * @author      hoang.ho
     * @date        Feb 12, 2014
     * @return      String Json
     ************************************************************************/
    @Action(value = "/ajaxLoadAutoCompleModels", results = { @Result(name = "list", type = "json") })
    public String ajaxLoadAutoCompleModels() {
        try {
            List<String> autoCompleteList = null;
            autoCompleteList = modelService.selectModelFilterList(fitModelCheckActionForm);
            fitModelCheckActionForm.setAutoCompleteList(autoCompleteList);
        } catch (Exception e) {
            this.getLogger().error("[ajaxLoadModelList] An error execute function");
            this.getLogger().error(e.toString());
            return "errorOther";
        }
        return "list";
    }

    /************************************************************************
     * <b>Description:</b><br>
     *  load list bottom grid for fitModelCheck page with filter.
     *
     * @author      Thai.Son
     * @date        Dec 27, 2013
     * @return      String
     ************************************************************************/
    @Action(value = "/bottomGridFilter", results = { @Result(name = "list", type = "json") })
    public String bottomGridFilter() {
        try {
            // Check missing params pageSize and pageNumber
            if (StringUtils.isEmpty(pageSize) || StringUtils.isEmpty(page)) {
                // Return page error params
                return "list";
            }
            int pageSizeInt = Integer.parseInt(pageSize);
            int pageInt = Integer.parseInt(page);

            //Get listAllBrand for init page
            fitModelCheckActionForm.setListProduct(productService.selectBottomGridByMatterNoAndFilterForFitModelCheck(
                                                                                                      pageSizeInt
                                                                                                    , pageInt
                                                                                                    , fitModelCheckActionForm.getEntTblFactoryProductNew()));
            //Check list of brand
            fitModelCheckActionForm.setCount(productService.selectTotalRecordOfFilterBottomGrid(fitModelCheckActionForm.getEntTblFactoryProductNew()));
        } catch (SQLException sqlEx) {
            this.getLogger().error("[AttributeManagerAction] An error execute function");
            this.getLogger().error(" An error SQLException");
            this.getLogger().error(sqlEx.toString());
            return "errorOther";
        } catch (Exception e) {
            this.getLogger().error("[AttributeManagerAction] An error execute function");
            this.getLogger().error(e.toString());
            return "errorOther";
        }
        return "list";
    }

    /************************************************************************
     * <b>Description:</b><br>
     *  get list fitModel, list image of product by ajax when change row selected product.
     *
     * @author		Nguyen.Chuong
     * @date		Jan 24, 2014
     * @return		String
     ************************************************************************/
    @Action(value = "loadProductAllDataAjax", results = { @Result(name = "list", type = "json") })
    public String loadProductAllDataAjax() {
        try {
            long productIdInt = Long.parseLong(productId);
            //Get list model of product.
            EntTblFactoryProductFitModel entProductFitModel = new EntTblFactoryProductFitModel();
            entProductFitModel.setProductId(productIdInt);
            entProductFitModel.setSortField("fitModelMaker");
            entProductFitModel.setSortDir("ASC");
            List<EntTblFactoryProductFitModel> listFitModel = fitModelService.selectListFitModelByProductCode(entProductFitModel);
            fitModelCheckActionForm.setListFitModelOfProduct(listFitModel);
            fitModelCheckActionForm.setListFitModelJsonStr(this.parseObjectToJson(listFitModel));

            //Get list image of product.
            EntTblFactoryProductNew entProduct = new EntTblFactoryProductNew();
            //Get request productId
            entProduct.setProductId(productIdInt);
            fitModelCheckActionForm.setListImage(productService.selectListImageAndThumbByProductId(entProduct));
        } catch (Exception e) {
            e.getLocalizedMessage();
        }
        return "list";
    }

    /************************************************************************
     * <b>Description:</b><br>
     * process insert, delete fit model data by single product.
     *
     * @return       String
     * @author      Long Vu
     * @date        Jan 23, 2014
     ************************************************************************/
    @Action(value = "/processDataFitModelBySingleProduct", results = { @Result(name = "processDataFitModelBySingleProduct", type = "json") })
    public String processDataFitModelBySingleProduct() {
        boolean result = false;
        try {
            String modelFitListJsonStr = fitModelCheckActionForm.getListModelJsonStr();
            List<EntTblFactoryProductFitModel> list = parseJsonToModelFitList(modelFitListJsonStr);
            fitModelCheckActionForm.setListFitModel(list);
            // Validate loginUser
            if (!validateLoginUser()) {
                return ERROR_OTHER;
            }
            // check product is null or not then set it to list
            if (fitModelCheckActionForm.getProduct() != null) {
                List<EntTblFactoryProductNew> productList = new ArrayList<EntTblFactoryProductNew>();
                productList.add(fitModelCheckActionForm.getProduct());
                fitModelCheckActionForm.setListProduct(productList);
            }
            if (fitModelCheckActionForm.getListFitModel().size() != 0) {
                if (!validateDataInput(false)) {
                    return ERROR_OTHER;
                }
                // check model maker and model model are full match with maker name
                // and model name or not
                checkModelMakerAndModelModel();
            }
            // Get user name login
            String user = this.getCommonForm().getLoginId();
            // process data for fit model
            result = fitModelService.processDataFitModelBySingleProduct(fitModelCheckActionForm.getListFitModel()
                                                                        , fitModelCheckActionForm.getListProduct()
                                                                        , fitModelCheckActionForm.getFactoryMatter()
                                                                        , user);
        } catch (Exception e) {
            fitModelCheckActionForm.setMsgCode(FAIL_MODE);
            return "processDataFitModelBySingleProduct";
        }
        if (result) {
            selectListFitModel();
            // set msgCode to success
            fitModelCheckActionForm.setMsgCode(SUCCESS_MODE);
        } else {
            // set msgCode to fail
            fitModelCheckActionForm.setMsgCode(FAIL_MODE);
        }
        return "processDataFitModelBySingleProduct";
    }

    /************************************************************************
     * <b>Description:</b><br>
     * check the model maker and model model are full match with
     * maker name and model model or not.
     *
     * @author      Long Vu
     * @date        Feb 10, 2014
     ************************************************************************/
    private void checkModelMakerAndModelModel() {
        this.getLogger().debug("start checkModelMakerAndModelModel");
        List<EntMstMaker> makerList = null;
        List<EntMstModel> modelList = null;
        try {
            // get maker list
            makerList = fitModelService.selectMakerList();
            // get model list
            modelList = modelService.selectAllListModel();
            for (EntTblFactoryProductFitModel obj : fitModelCheckActionForm.getListFitModel()) {
                // check if the maker list in DB is null or empty
                if (makerList != null && makerList.size() != 0) {
                    // loop through the maker list
                    for (EntMstMaker maker: makerList) {
                        // check if the fit model maker is full match with maker name
                        // then set model maker no fit flg to 0
                        // else set to 1
                        obj.setFitModelMakerNoFitFlg(1);
                        if (StringUtils.equals(obj.getFitModelMaker().trim(), maker.getMakerName().trim())) {
                            obj.setFitModelMakerNoFitFlg(0);
                            break;
                        }
                    }
                }
                // check if the model list in DB is null or empty
                if (modelList != null && modelList.size() != 0) {
                    // loop through the model list
                    for (EntMstModel model: modelList) {
                        // check if the fit model model is full match with model name
                        // then set model model no fit flg to 0
                        // else set to 1
                        obj.setFitModelModelNoFitFlg(1);
                        if (StringUtils.equals(model.getModelName().trim(), obj.getFitModelModel().trim())) {
                            obj.setFitModelModelNoFitFlg(0);
                            break;
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.getLogger().debug("end checkModelMakerAndModelModel");
    }

    /************************************************************************
     * <b>Description:</b><br>
     * process insert, delete fit model data by multi product.
     *
     * @return      String
     * @author      Long Vu
     * @date        Jan 23, 2014
     ************************************************************************/
    @Action(value = "/processDataFitModelByMultiProduct", results = { @Result(name = "processDataFitModelByMultiProduct", type = "json") })
    public String processDataFitModelByMultiProduct() { //TODO action
        boolean result = false;
        // get product list
        parseProcessInserFitModelByMultiProduct();
        // get fit model in json format
        String modelFitListJsonStr = fitModelCheckActionForm.getListModelJsonStr();
        // parse json to list object
	    List<EntTblFactoryProductFitModel> list = parseJsonToModelFitList(modelFitListJsonStr);
	    // set to form
	    fitModelCheckActionForm.setListFitModel(list);
        try {
            // Validate loginUser
            if (!validateLoginUser()) {
                return ERROR_OTHER;
            }
            // Get user name login
            String user = this.getCommonForm().getLoginId();
            if (fitModelCheckActionForm.getListFitModel().size() != 0) {
                if (!validateDataInput(false)) {
                    return ERROR_OTHER;
                }
                // check model maker and model model are full match with maker name
                // and model name or not
                checkModelMakerAndModelModel();
            }
            result = fitModelService.processDataFitModelByMultiProduct(fitModelCheckActionForm.getListFitModel()
                                                                      , fitModelCheckActionForm.getListProduct()
                                                                      , fitModelCheckActionForm.getFactoryMatter()
                                                                      , user);
        } catch (Exception e) {
            fitModelCheckActionForm.setMsgCode(FAIL_MODE);
            this.getLogger().error("[FitModelCheckAction] An error processDataFitModelByMultiProduct function");
            this.getLogger().error(e.toString());
            return "processDataFitModelByMultiProduct";
        }
        if (result) {
            selectListFitModel();
            fitModelCheckActionForm.setMsgCode(SUCCESS_MODE);
        } else {
            fitModelCheckActionForm.setMsgCode(FAIL_MODE);
        }
        return "processDataFitModelByMultiProduct";
    }

    /************************************************************************
     * <b>Description:</b><br>
     * process insert, delete fit model data by multi product.
     *
     * @return      String
     * @author      Long Vu
     * @date        Jan 23, 2014
     ************************************************************************/
    @Action(value = "/processInserFitModelByMultiProduct", results = { @Result(name = "processInserFitModelByMultiProduct", type = "json") })
    public String processInserFitModelByMultiProduct() {
        boolean result = false;
        parseProcessInserFitModelByMultiProduct();
        parseListFitModelProduct();
        try {
            // Validate loginUser
            if (!validateLoginUser()) {
                return ERROR_OTHER;
            }
            if (fitModelCheckActionForm.getListFitModel() != null && fitModelCheckActionForm.getListProduct() != null) {
                checkDuplicateFitModel();
            }
            // Get user name login
            String user = this.getCommonForm().getLoginId();
            if (!validateDataInput(true)) {
                return ERROR_OTHER;
            }
            // check model maker and model model are full match with maker name
            // and model name or not
            checkModelMakerAndModelModel();
            if (!selectListFitModelWithMaxModelSort()) {
                return ERROR_OTHER;
            }
            result = fitModelService.processInserFitModelByMultiProduct(fitModelCheckActionForm.getListFitModel()
                                                                        , fitModelCheckActionForm.getListProduct()
                                                                        , fitModelCheckActionForm.getFactoryMatter()
                                                                        , user);
        } catch (SQLException e) {
        	this.getLogger().error("[processInserFitModelByMultiProduct] error when excute sql query");
        	this.getLogger().error(e.getStackTrace());
        	//Return update false
        	fitModelCheckActionForm.setMsgCode(FAIL_MODE);
            return "processInserFitModelByMultiProduct";
        } catch (Exception e) {
            fitModelCheckActionForm.setMsgCode(FAIL_MODE);
            return "processInserFitModelByMultiProduct";
        }
        if (result) {
            selectListFitModel();
            fitModelCheckActionForm.setMsgCode(SUCCESS_MODE);
        } else {
            fitModelCheckActionForm.setMsgCode(FAIL_MODE);
        }
        return "processInserFitModelByMultiProduct";
    }

    /************************************************************************
     * <b>Description:</b><br>
     * update product check flg if there is no error for selected product.
     *
     * @return      String
     * @author      Long Vu
     * @date        Jan 23, 2014
     ************************************************************************/
    @Action(value = "/ajaxCheckProductChange", results = { @Result(name = "list", type = "json") })
    public String checkProductChange() {
        EntTblFactoryProductNew product = new EntTblFactoryProductNew();
        try {
            // Validate loginUser
            if (!validateLoginUser()) {
                return ERROR_OTHER;
            }
            // Get user name login
            String user = this.getCommonForm().getLoginId();
            // get product by product id
            if (StringUtils.isNumeric(productId) && !StringUtils.equals("", productId)) {
                product.setProductId(Long.parseLong(productId));
                product.setUpdatedUserId(user);
                fitModelService.updateProductCheckFlgByProductId(product);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "list";
    }

    /************************************************************************
     * <b>Description:</b><br>
     * check duplicate fit model when insert new fit model by product.
     *
     * @author      Long Vu
     * @throws SQLException 
     * @date        Feb 12, 2014
     ************************************************************************/
    private void checkDuplicateFitModel() throws SQLException {
        // new product list to save product list with no duplicate fit model
        //List<EntTblFactoryProductNew> prdListNew = new ArrayList<EntTblFactoryProductNew>();
        // product list from client
        List<EntTblFactoryProductNew> prdList = fitModelCheckActionForm.getListProduct();
        // fit model to save in DB
        EntTblFactoryProductFitModel fitModel = fitModelCheckActionForm.getListFitModel().get(0);
        /* BOE @rcv! Luong.Dai 2014/08/22 move check duplicate fitModel of product to DB
         * Get list product has duplicate model */
        // list fit model will get from DB by product Id
        /*List<EntTblFactoryProductFitModel> fitModelList = null;*/
        //Check duplicate fit model of product. return list product id has duplicate
        List<Long> lstProductId = fitModelService.selectListProductByFitMode(prdList, fitModel);
        //Remove product has duplicate fit model
        Long duplicateProductId = null; 
        for (Long id : lstProductId) {
        	//check productId in lstProductId
        	for (EntTblFactoryProductNew prd: prdList) {
        		duplicateProductId = prd.getProductId();
        		//Check productId has in lstProductId
        		if (duplicateProductId.equals(id)) {
        			//Remove product from list
        			prdList.remove(prd);
        			break;
        		}
        	}
        }
        // loop through product list to check the duplicate fit model
       /* for (EntTblFactoryProductNew prd: prdList) {
            if (prd.getProductId() != null) {
                // get list fit model from DB by product id
                fitModelList = fitModelService.selectListFitModelByProductId(prd.getProductId());
                if (fitModelList != null) {
                    int i = 0;
                     BOE #9407 Luong.Dai 2014/07/03 Fix bug cannot add fit model for product in mode select all 
                    // loop through the fit model to check with the fit model obj from client
                    for (EntTblFactoryProductFitModel fMObj: fitModelList) {
                        i++;
                        // if data is the same then don't save this product to the new product list
                        if (StringUtils.equals(fitModel.getFitModelMaker(), fMObj.getFitModelMaker())
                               && StringUtils.equals(fitModel.getFitModelModel(), fMObj.getFitModelModel())
                               && StringUtils.equals(fitModel.getFitModelStyle(), fMObj.getFitModelStyle())) {
                            break;
                        } else {
                            // if loop through the fit model list from DB
                            // and found no the same fit model
                            // then add this product to the new product list
                            if (i == fitModelList.size()) {
                                prdListNew.add(prd);
                            }
                        }
                    }
                    for (EntTblFactoryProductFitModel fMObj: fitModelList) {
                        // if data is the same then don't save this product to the new product list
                        if (StringUtils.equals(fitModel.getFitModelMaker(), fMObj.getFitModelMaker())
                               && StringUtils.equals(fitModel.getFitModelModel(), fMObj.getFitModelModel())
                               && StringUtils.equals(fitModel.getFitModelStyle(), fMObj.getFitModelStyle())) {
                            break;
                        }
                        i++;
                    }
                    // if loop through the fit model list from DB
                    // and found no the same fit model
                    // then add this product to the new product list
                    if (i == fitModelList.size()) {
                        prdListNew.add(prd);
                    }
                     EOE #9407 Luong.Dai 2014/07/03 Fix bug cannot add fit model for product in mode select all 
                }
            }
        }*/
        // add new product list to form for processing further
        /*fitModelCheckActionForm.setListProduct(prdListNew);*/
        /* EOE @rcv! Luong.Dai 2014/08/22 move check duplicate fitModel of product to DB */
        fitModelCheckActionForm.setListProduct(prdList);
    }

    /************************************************************************
     * <b>Description:</b><br>
     *  parse Process Inser FitModel By MultiProduct From Request.
     *
     * @author		Tran.Thanh
     * @date		27 Jan 2014
     * @return		boolean
     ************************************************************************/
    public boolean parseProcessInserFitModelByMultiProduct() {
    	/*BOE dang.nam 2014/06/06 select all*/
		String checkAll = this.getRequest().getParameter("checkAll");
		//check if select all then get all products without limit ,set to action form
		if (checkAll.equals("true")) {
			try {
				List<EntTblFactoryProductNew> listProduct = productService.selectBottomGridByMatterNoAndFilterForFitModelCheck(0
                                                                                                       , 0
                                                                                                       , fitModelCheckActionForm.getEntTblFactoryProductNew());
				fitModelCheckActionForm.setListProduct(listProduct);
			} catch (SQLException e) {
	            this.getLogger().error(e.toString());
			}
		} else {
		/*EOE dang.nam 2014/06/06 select all*/
	    	//Parese List Product
	        List<EntTblFactoryProductNew> listProduct = new ArrayList<EntTblFactoryProductNew>();
	        //Get List Product Id
	        String arrayProductId = this.getRequest().getParameter("arrayProductId");
			if (StringUtils.isEmpty(arrayProductId)) {
				return false;
			}
			String[] arrayProducts =  arrayProductId.split("_");
			//Get List Product Error Code
			String arrayProductModelErrorFlg = this.getRequest().getParameter("arrayProductModelErrorFlg");
			if (StringUtils.isEmpty(arrayProductModelErrorFlg)) {
				return false;
			}
			String[] arrayProductsErrorFlg = arrayProductModelErrorFlg.split("_");
			//Parse list product to List EntTblFactoryProductNew
			if (arrayProducts.length == arrayProductsErrorFlg.length) {
				for (int i = 0; i < arrayProducts.length; i++) {
					EntTblFactoryProductNew entProduct = new EntTblFactoryProductNew();
					entProduct.setProductId(Long.parseLong(arrayProducts[i]));
					entProduct.setProductModelErrorFlg(Integer.parseInt(arrayProductsErrorFlg[i]));
					listProduct.add(entProduct);
				}
			}
			fitModelCheckActionForm.setListProduct(listProduct);
		}
		return true;
    }

    /************************************************************************
     * <b>Description:</b><br>
     *  parse List FitModel Product.
     *
     * @author		Tran.Thanh
     * @date		27 Jan 2014
     * @return		boolean
     ************************************************************************/
    private boolean parseListFitModelProduct() {
        this.getLogger().debug("start parseListFitModelProduct");
    	//Parse List Fit Model
		List<EntTblFactoryProductFitModel> listFitModel = new ArrayList<EntTblFactoryProductFitModel>();
		EntTblFactoryProductFitModel entProductFitModel = fitModelCheckActionForm.getFitModel();
		listFitModel.add(entProductFitModel);
		fitModelCheckActionForm.setListFitModel(listFitModel);
		this.getLogger().debug("end parseListFitModelProduct");
		return true;
    }

    /************************************************************************
     * <b>Description:</b><br>
     * process insert fit model data.
     *
     * @return      boolean
     * @param       isSingleInsert check if it is single insert
     * @author      Long Vu
     * @date        Jan 23, 2014
     ************************************************************************/
    private boolean validateDataInput(boolean isSingleInsert) {
        this.getLogger().debug("start validateDataInput");
        List<EntTblFactoryProductFitModel> mFList = fitModelCheckActionForm.getListFitModel();
        List<EntTblFactoryProductNew> productList = null;
        boolean result = false;
        int errorNum = 0;
        try {
            // check the product list is null or not
            if (fitModelCheckActionForm.getListProduct() != null && fitModelCheckActionForm.getListProduct().size() != 0) {
                List<Long> pl = new ArrayList<Long>();
                for (EntTblFactoryProductNew obj : fitModelCheckActionForm.getListProduct()) {
                    // add product id to list integer
                    pl.add(obj.getProductId());
                }
                // get product list by product id list to check error flag
                // and count the number of error flg will change
                if (pl != null && pl.size() != 0) {
                    productList = productService.selectProductListByProductIdList(pl);
                }
                if (mFList != null && mFList.size() != 0) {
                    int i = 0;
                    for (EntTblFactoryProductFitModel obj : mFList) {
                        i++;
                        // check if fit model maker and fit model style is over length of error
                        if (obj.getFitModelMaker().length() > Constant.MODEL_MAKER_LENGTH
                                || obj.getFitModelModel().length() > Constant.MODEL_MODEL_LENGTH
                                || obj.getFitModelStyle().length() > Constant.MODEL_STYLE_LENGTH) {
                            result = false;
                            break;
                        /* BOE @rcv! Luong.Dai 2014/08/25: Add check style to check productModelErrorFlg */
                        /*} else if (obj.getFitModelMaker().length() > Constant.MAKER_LENGTH || obj.getFitModelModel().length() > Constant.MODEL_LENGTH) {*/
                        } else if (obj.getFitModelMaker().length() > Constant.MAKER_LENGTH
                        		|| obj.getFitModelModel().length() > Constant.MODEL_LENGTH
                        		|| obj.getFitModelStyle().length() > Constant.STYLE_LENGTH) {
                    	/* EOE @rcv! Luong.Dai 2014/08/25: Add check style to check productModelErrorFlg */
                            // check if fit model maker and fit model style is over length of warning
                            for (EntTblFactoryProductNew prd : productList) {
                                // check if error flg is 0
                                if (prd.getProductModelErrorFlg() == 0) {
                                    // then loop product list then get the match product and set error flg to 1
                                    for (EntTblFactoryProductNew prdObject : fitModelCheckActionForm.getListProduct()) {
                                        if (prdObject.getProductId().equals(prd.getProductId())) {
                                            prdObject.setProductModelErrorFlg(1);
                                            break;
                                        }
                                    }
                                    errorNum++;
                                }
                            }
                            result = true;
                            break;
                        } else {
                            // check if loop through the fit model but found no error
                            if (i == mFList.size()) {
                                // if data is insert new fit model without delete data then no need to do the below process
                                if (!isSingleInsert) {
                                    for (EntTblFactoryProductNew prd : productList) {
                                           // check if error flg is 1
                                           if (prd.getProductModelErrorFlg() == 1) {
                                               // then loop product list then get the match product and set error flg to 0
                                               for (EntTblFactoryProductNew prdObject : fitModelCheckActionForm.getListProduct()) {
                                                   if (prdObject.getProductId().equals(prd.getProductId())) {
                                                       prdObject.setProductModelErrorFlg(0);
                                                       break;
                                                   }
                                               }
                                               errorNum++;
                                           }
                                       }
                                       errorNum = -errorNum;
                                }
                                result = true;
                            }
                        }
                    }
                }
                fitModelCheckActionForm.getFactoryMatter().setMatterFactoryErrorCountModel(errorNum);
            }
        } catch (Exception e) {
            this.getLogger().error("[validateDataInput] An error validateDataInput function");
            this.getLogger().error(e.toString());
            result = false;
            /*return result;*/
        }
        this.getLogger().debug("end validateDataInput");
        return result;
    }

    /************************************************************************
     * <b>Description:</b><br>
     * get fit model list with max fit model sort to prepare data for insert.
     *
     * @return      boolean
     * @author      Long Vu
     * @date        Jan 23, 2014
     ************************************************************************/
    private boolean selectListFitModelWithMaxModelSort() {
        this.getLogger().debug("start selectListFitModelWithMaxModelSort");
        boolean result = false;
        List<Long> prdIdList = new ArrayList<Long>();
        List<EntTblFactoryProductNew> prdList = fitModelCheckActionForm.getListProduct();
        try {
            // get list product by product list id
            if (prdList != null && prdList.size() != 0) {
                for (EntTblFactoryProductNew obj : prdList) {
                    prdIdList.add(obj.getProductId());
                }
            }
            List<EntTblFactoryProductFitModel> fitModelList = fitModelService.selectListFitModelWithMaxModelSort(prdIdList);
            if (fitModelList != null && fitModelList.size() == prdList.size()) {
                for (EntTblFactoryProductNew obj : prdList) {
                    for (EntTblFactoryProductFitModel fitModel : fitModelList) {
                        if (obj.getProductId().equals(fitModel.getProductId())) {
                            // get max sort model then plus one to prevent duplicate key.
                            obj.setModelSort(fitModel.getFitModelSort() + 1);
                            break;
                        }
                    }
                }
                fitModelCheckActionForm.setListProduct(prdList);
                result = true;
            }
        } catch (Exception e) {
            this.getLogger().error("[selectListFitModelWithMaxModelSort] An error selectListFitModelWithMaxModelSort function");
            this.getLogger().error(e.toString());
            result = false;
            return result;
        }
        this.getLogger().debug("end selectListFitModelWithMaxModelSort");
        return result;
    }

    /**
     * Parse Object to Json String.
     * @param obj object will be parse string json.
     * @return String Json
     * @author hoang.ho
     * @since 2014/01/23
     */
    private String parseObjectToJson(Object obj) {
        this.getLogger().debug("start parseObjectToJson");
        if (null == obj) {
            return "";
        }
        Gson gson = new Gson();
        this.getLogger().debug("end parseObjectToJson");
        return gson.toJson(obj);
    }
    /************************************************************************
     * <b>Description:</b><br>
     * change json string to List<EntTblFactoryProductFitModel>.
     * @param jsonStr json string
     * @author hoang.ho
     * @date 2014/01/24
     * @return List<EntTblFactoryProductFitModel>
     ************************************************************************/
    private List<EntTblFactoryProductFitModel> parseJsonToModelFitList(String jsonStr) {
        List<EntTblFactoryProductFitModel> list = null;
        this.getLogger().debug("Start parseJsonToModelFitList");
        try {
            Gson gson = new Gson();
            // convert string json to list.
            list =  gson.fromJson(jsonStr, new TypeToken<List<EntTblFactoryProductFitModel>>() { } .getType());
        } catch (Exception e) {
            this.getLogger().error("[parseJsonToModelFitList] An error execute function");
            this.getLogger().error(e.toString());
        }
        this.getLogger().debug("End parseJsonToModelFitList");
        return list;
    }

    /************************************************************************
     * get list fit model for reloading grid when loading by ajax.
     * @author      Long Vu
     * @date        2014/01/28
     ************************************************************************/
    private void selectListFitModel() {
        this.getLogger().debug("start selectListFitModel");
        // create product object to get list fit model
        EntTblFactoryProductFitModel entProductFitModel = new EntTblFactoryProductFitModel();
        entProductFitModel.setProductId(fitModelCheckActionForm.getProduct().getProductId());
        entProductFitModel.setSortField("fitModelMaker");
        entProductFitModel.setSortDir("ASC");
        // get list fit model
        List<EntTblFactoryProductFitModel> listFitModel = fitModelService.selectListFitModelByProductCode(entProductFitModel);
        fitModelCheckActionForm.setListFitModelOfProduct(listFitModel);
        fitModelCheckActionForm.setListFitModelJsonStr(this.parseObjectToJson(listFitModel));
        this.getLogger().debug("end selectListFitModel");
    }

    /**
     * @return the page
     */
    public String getPage() {
        return page;
    }

    /**
     * @param page the page to set
     */
    public void setPage(String page) {
        this.page = page;
    }

    /**
     * @return the pageSize
     */
    public String getPageSize() {
        return pageSize;
    }

    /**
     * @param pageSize the pageSize to set
     */
    public void setPageSize(String pageSize) {
        this.pageSize = pageSize;
    }

    /**
     * @return the fitModelCheckActionForm
     */
    public FitModelCheckActionForm getFitModelCheckActionForm() {
        return fitModelCheckActionForm;
    }

    /**
     * @param fitModelCheckActionForm the fitModelCheckActionForm to set
     */
    public void setFitModelCheckActionForm(
            FitModelCheckActionForm fitModelCheckActionForm) {
        this.fitModelCheckActionForm = fitModelCheckActionForm;
    }

    /**
     * @return the productId
     */
    public String getProductId() {
        return productId;
    }
    /**
     * @param productId the productId to set
     */
    public void setProductId(String productId) {
        this.productId = productId;
    }
    /**
     * @return the linkToSyouhinPage
     */
    public String getLinkToSyouhinPage() {
        return linkToSyouhinPage;
    }
    /**
     * @param linkToSyouhinPage the linkToSyouhinPage to set
     */
    public void setLinkToSyouhinPage(String linkToSyouhinPage) {
        this.linkToSyouhinPage = linkToSyouhinPage;
    }
//    @Override
//    public void setSession(Map<String, Object> session) {
//        this.session = session;
//    }

    /**
     * @return the productIdParamName
     */
    public String getProductIdParamName() {
        return productIdParamName;
    }

    /**
     * @param productIdParamName the productIdParamName to set
     */
    public void setProductIdParamName(String productIdParamName) {
        this.productIdParamName = productIdParamName;
    }

}
