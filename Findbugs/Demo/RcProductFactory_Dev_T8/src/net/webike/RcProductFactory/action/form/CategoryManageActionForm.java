/************************************************************************
 * File Name	： CategoryManageActionForm.java
 * Author		： hoang.ho
 * Version		： 1.0.0
 * Date Created	： 2014/01/07
 * Date Updated	： 2014/01/07
 * Description	： store data of category list to communicate with Java Server Page.
 *
 ***********************************************************************/
package net.webike.RcProductFactory.action.form;

import java.util.List;

import net.webike.RcProductFactory.entity.EntMstAttribute;
import net.webike.RcProductFactory.entity.EntMstCategory;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * store data of category list to communicate with Java Server Page.
 * @author hoang.ho
 * Date Created	： 2014/01/07
 */
@Component
@Scope("request")
public class CategoryManageActionForm {
    /**
     * Category list.
     */
    private List<EntMstCategory> entMstCategoryList;

    /**
     * Attribute list.
     */
    private List<EntMstAttribute> entMstAttributeList;


    /**
     * Search conditions for get category list.
     */
    private EntMstCategory entMstCategory;

    //Total record of filter
    private int count = 0;

    private String message;

    public List<EntMstCategory> getEntMstCategoryList() {
        return entMstCategoryList;
    }

    public void setEntMstCategoryList(List<EntMstCategory> entMstCategoryList) {
        this.entMstCategoryList = entMstCategoryList;
    }

    public EntMstCategory getEntMstCategory() {
        return entMstCategory;
    }

    public void setEntMstCategory(EntMstCategory entMstCategory) {
        this.entMstCategory = entMstCategory;
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message the message to set
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * @return the count
     */
    public int getCount() {
        return count;
    }

    /**
     * @param count the count to set
     */
    public void setCount(int count) {
        this.count = count;
    }

    /**
     * @return the entMstAttributeList
     */
    public List<EntMstAttribute> getEntMstAttributeList() {
        return entMstAttributeList;
    }

    /**
     * @param entMstAttributeList the entMstAttributeList to set
     */
    public void setEntMstAttributeList(List<EntMstAttribute> entMstAttributeList) {
        this.entMstAttributeList = entMstAttributeList;
    }


}
