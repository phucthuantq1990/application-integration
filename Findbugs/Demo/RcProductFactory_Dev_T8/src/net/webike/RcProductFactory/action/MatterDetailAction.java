/************************************************************************
 * File Name	MatterDetailAction.java
 * Author		Tran.Thanh
 * Version		1.0.0
 * Date Created	2013/09/24
 * Date Updated	2013/09/24
 * Description	Show, Update, Delete  PJ Detail Info.
 *
 ***********************************************************************/
package net.webike.RcProductFactory.action;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.Reader;
import java.net.URLEncoder;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.mail.internet.MimeUtility;
import javax.servlet.http.HttpServletRequest;

import net.webike.RcProductFactory.action.form.MatterDetailActionForm;
import net.webike.RcProductFactory.entity.EntCSVMode1;
import net.webike.RcProductFactory.entity.EntCSVMode3;
import net.webike.RcProductFactory.entity.EntInfoCSVImport;
import net.webike.RcProductFactory.entity.EntMstAttribute;
import net.webike.RcProductFactory.entity.EntMstFactoryMatterNew;
import net.webike.RcProductFactory.entity.EntMstFactoryUserTmp;
import net.webike.RcProductFactory.entity.EntTblFactoryImportCondition;
import net.webike.RcProductFactory.entity.EntTblFactoryProductNew;
import net.webike.RcProductFactory.entity.EntTblTmpCSV;
import net.webike.RcProductFactory.entity.EnumImportCSVMode1Header;
import net.webike.RcProductFactory.entity.EnumRegistrationFlg;
import net.webike.RcProductFactory.service.CategoryService;
import net.webike.RcProductFactory.service.MatterDetailService;
import net.webike.RcProductFactory.service.ProductEditService;
import net.webike.RcProductFactory.service.ProductService;
import net.webike.RcProductFactory.service.SettingService;
import net.webike.RcProductFactory.service.UserTmpService;
import net.webike.RcProductFactory.util.CharsetDetectorUtils;
import net.webike.RcProductFactory.util.Constant;
import net.webike.RcProductFactory.util.CustomException;
import net.webike.RcProductFactory.util.EncodingUtils;
import net.webike.RcProductFactory.util.Utils;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.apache.struts2.interceptor.SessionAware;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import com.fasterxml.jackson.core.JsonParseException;
/**
 * PJ Detail Action Class.
 */
@Controller
@Scope("request")
@ParentPackage(value = "CommonInterceptor")
@Results(
    {
    	// On loading default matter detail page
        @Result(name = "success", location = "matterDetail.jsp"),
        //Return json
        @Result(name = "list", type = "json"),
        @Result(name = "deleteProducts" , type = "json",
        params = {"contentType", "text/html", "root", "statusDelete" , "ignoreHierarchy", "false" }),
        @Result(name = "deleteAndUpdate" , type = "json",
        params = {"contentType", "text/html", "root", "mapResult" , "ignoreHierarchy", "false" }),
        @Result(name = "updateMatter" , type = "json",
        params = {"contentType", "text/html", "root", "statusUpdate" , "ignoreHierarchy", "false" }),
        // Return error page when error other.
        @Result(name = "errorOther", type = "redirect", location = "error.html"),
        @Result(name = "errorPermission", type = "redirect", location = "error.html?error=noPermission"),
        @Result(name = "matterManage", type = "redirect", location = "matterManage.html"),
        @Result(name = "issueDetail", type = "redirect", location = "%{urlIssueRedmine}"),
        @Result(name = "errorDeadlock" , type = "json",
        params = {"contentType", "text/html", "root", "mapResult" , "ignoreHierarchy", "false" })
    }
)
public class MatterDetailAction extends CommonAction implements SessionAware {

	private static final long serialVersionUID = 1L;

    @Autowired
    private MatterDetailService matterDetailService;
    @Autowired
    private UserTmpService userTmpService;
    @Autowired
    private ProductService productService;
    @Autowired
    private CategoryService categoryService;
    @Autowired
    private ProductEditService productEditService;
    @Autowired
    private MatterDetailActionForm matterDetailActionForm;
    @Autowired
    private SettingService settingService;
    private Map<String, Object> session;
    private String page;
    private String pageSize;
    private String statusDelete;
    private String statusUpdate = "";
    private String userUpdate = "";
    private Date updatedOn = null;
    private Map<Object, Object> mapResult;
    private String urlIssueRedmine;

    //Upload and import CSV to DB
    private File fileCSV;
    private String fileCSVContentType;
    private String fileCSVFileName;
    private String importMode;
    private int maxNumberProductExportCSV = Constant.MAX_PRODUCT_EXPORT_CSV;
    /**
     * Used to check if something gets wrong when validating CSV.
     */
    private boolean csvImportValid;

    @Override
    @Action("/matterDetail")
    public String execute() {
    	/* BOE Fix bug #35 Login text doesn't show @rcv!nguyen.hieu 2014/03/11. */
		// Validate login user.
		if (!super.validateLoginUser()) {
			return "errorOther";
		}
		/* EOE Fix bug #35 Login text doesn't show. */
        String matterNo = this.getRequest().getParameter("matterNo");
        //Init message
        initMessage();
        //Get matterDetail
        EntMstFactoryMatterNew matter = matterDetailService.selectFactoryMatterByMatterCode(Long.valueOf(matterNo));

        /* BOE fix authority for by Luong.Dai 2014/03/24 */
        List<String> permission = this.getCommonForm().getLoginAuthority();
        String loginId = this.getCommonForm().getLoginId();
        //If user has role limitedUserId, use can edit only projects that he manager
        if (permission.contains(getText("text.role.roleLimitedUserId"))
        	&& !matter.getMatterChargeUserId().equals(loginId)) {
        	//Return error page no permission
        	return "errorPermission";
        }
		/* EOE fix authority for by Luong.Dai */
        //If matter = null - redirect to matter manage
        if (matter == null) {
            return "matterManage";
        }

        matterDetailActionForm.setEntMstFactoryMatterNew(matter);

        //Get list user
        List<EntMstFactoryUserTmp> listCommonUser = userTmpService.selectListAllUser();
        this.getCommonForm().setListCommonUser(listCommonUser);

        //Get list registration flg
        Map<String, String> listFlg = EnumRegistrationFlg.getHashMapAllFlg();
        matterDetailActionForm.setListRegistrationFlg(listFlg);

        //Get list Attribute
        List<EntMstAttribute> entMstAttributeList = categoryService.selectAttributeList();
        matterDetailActionForm.setEntMstAttributeList(entMstAttributeList);

        /*BOE Tran.Thanh 2014/04/03 : add max Upload Image size and max Folder Matter size */
        String valueMaxSizeFolder = settingService.selectSettingValueBySettingCode(Constant.MAX_IMAGE_FOLDER_MATTER);
        if (StringUtils.isNotEmpty(valueMaxSizeFolder)) {
        	matterDetailActionForm.setMaxUploadFolderMatter(Integer.parseInt(valueMaxSizeFolder));
        }
        String valueMaxSizeImage = settingService.selectSettingValueBySettingCode(Constant.MAX_SIZE_UPLOAD_IMAGE);
        if (StringUtils.isNotEmpty(valueMaxSizeImage)) {
        	matterDetailActionForm.setMaxUploadImage(Integer.parseInt(valueMaxSizeImage));
        }
        /*EOE Tran.Thanh 2014/04/03 : add max Upload Image size and max Folder Matter size */
        return SUCCESS;
    }
    /*
     @Action(value = "/gridCompatibleModel", results = { @Result(name = "list", type = "json") })
    public String gridCompatibleModel() {
        try {
            // Check missing params pageSize and pageNumber
            if (StringUtils.isEmpty(pageSize) || StringUtils.isEmpty(page)) {
                // Return page error params
                return "list";
            }
            int pageSizeInt = Integer.valueOf(pageSize);
            int pageInt = Integer.valueOf(page);

            //Get listAllBrand for init page
            fitModelCheckActionForm.setListProduct(productService.selectBottomGridByMatterNoAndFilterForFitModelCheck(pageSizeInt, pageInt,
            fitModelCheckActionForm.getEntTblFactoryProductNew()));
            //Check list of brand
            fitModelCheckActionForm.setCount(productService.selectTotalRecordOfFilterBottomGrid(fitModelCheckActionForm.getEntTblFactoryProductNew()));
        } catch (SQLException sqlEx) {

            this.getLogger().error(" An error SQLException");
            this.getLogger().error(sqlEx.toString());
            return "errorOther";
        } catch (Exception e) {

            this.getLogger().error(e.toString());
            return "errorOther";
        }
        return "list";
    }
     */

    /************************************************************************
     * <b>Description:</b><br>
     * ...
     * @author      Thai.Son
     * @date        Dec 27, 2013
     * @return      String
     ************************************************************************/
    @Action(value = "/gridMode1", results = { @Result(name = "list", type = "json") })
    public String gridMode1() {
        try {
            // Check missing params pageSize and pageNumber
            if (StringUtils.isEmpty(pageSize) || StringUtils.isEmpty(page)) {
                // Return page error params
                return "list";
            }
            int pageSizeInt = Integer.parseInt(pageSize);
            int pageInt = Integer.parseInt(page);

            matterDetailActionForm.setListEntCSVMode1(matterDetailService.selectListTempDataForGridMode1(matterDetailActionForm.getImportedCSVTableName(),
                pageSizeInt, pageInt, matterDetailActionForm.getEntCSVMode1()));
            matterDetailActionForm.setCount(matterDetailService.selectTotalRecordForGridMode1(matterDetailActionForm.getImportedCSVTableName(),
                matterDetailActionForm.getEntCSVMode1()));

        } catch (Exception e) {
            this.getLogger().error(e.toString());
            return "errorOther";
        }
        return "list";
    }

    private String headerList;
    /************************************************************************
     * <b>Description:</b><br>
     * ...
     * @author      Thai.Son
     * @date        Dec 27, 2013
     * @return      String
     ************************************************************************/
    @Action("/receiveHeaderGridMode1")
    public String receiveHeaderGridMode1() {
        try {
            // Header
            StringBuilder sbHeader = new StringBuilder();
          createHeader(headerList, sbHeader, new StringBuilder(), new EntCSVMode1(), new ArrayList<EntMstAttribute>());
            String listHeader = sbHeader.toString().replaceFirst(",", "");
            matterDetailActionForm.setListHeader(Arrays.asList(listHeader.split(",")));
        } catch (Exception e) {
            this.getLogger().error(e.toString());
            return "errorOther";
        }
        return "list";
    }

    /************************************************************************
     * <b>Description:</b><br>
     *  Get list product.
     *
     * @author		Luong.Dai
     * @date		Jan 18, 2014
     * @return		String
     ************************************************************************/
    @Action("/ajaxGetProductList")
    public String ajaxGetCategoryList() {
        try {
            // Check missing params pageSize and pageNumber
            if (StringUtils.isEmpty(pageSize) || StringUtils.isEmpty(page)) {
                // Return page error params
                return "list";
            }

            int pageSizeInt = Integer.parseInt(pageSize);
            int pageInt = Integer.parseInt(page);
            //Get listAllBrand for init page
            EntTblFactoryProductNew product = matterDetailActionForm.getProduct();
            /*BOE Nguyen.Chuong 2014/04/23 modify sortField and sortDir*/
            if (product.getSortField().equals("showProductSyouhinSysCode")) {
                product.setSortField("productSyouhinSysCode");
            }
            /*EOE Nguyen.Chuong 2014/04/23 modify sortField and sortDir*/
            List<EntTblFactoryProductNew> listProduct = productService.selectListProductByMatterCode(pageInt, pageSizeInt, product);
            //Validate listProduct
            if (listProduct != null) {
                matterDetailActionForm.setListProduct(listProduct);

                initStringToShowInClient();

                //Get total record for pagging
                Integer totalRecord = productService.selectTotalRecordOfFilterProduct(product);
                matterDetailActionForm.setCount(totalRecord);

                //Get number product of matter
                Integer numberProduct = productEditService.selectCountNumberProductOfMatter(product.getMatterNo());
                matterDetailActionForm.setNumberProduct(numberProduct);
            } else {
                matterDetailActionForm.setCount(0);
                matterDetailActionForm.setListProduct(new ArrayList<EntTblFactoryProductNew>());
            }
        } catch (Exception e) {
            this.getLogger().error("[MatterDetailAction] An error execute function");
            this.getLogger().error(e.toString());
            return "errorOther";
        }
        return "list";
    }

    /************************************************************************
     * <b>Description:</b><br>
     *  deleteProducts.
     *
     * @author		Le.Dinh
     * @date		20 Jan 2014
     * @return		String
     ************************************************************************/
    @Action("/deleteProductList")
    public String deleteProducts() {
        if (!validateLoginUser()) {
            return "errorOther";
        }

        mapResult = new HashMap<Object, Object>();
    	String productId = this.getRequest().getParameter("arrayProductId");
    	String updateOn = this.getRequest().getParameter("arrayProductUpdateOn");
    	//BOE Tran.Thanh 2014/06/04 : get Entity Filter From Request For Function SelectAll
		EntTblFactoryProductNew entProductFilter = matterDetailActionForm.getProduct();
		String deleteReleaseProduct = this.getRequest().getParameter("deleteReleaseProduct");
		//EOE Tran.Thanh 2014/06/04 : get Entity Filter From Request For Function SelectAll
    	if (StringUtils.isEmpty(productId) || StringUtils.isEmpty(updateOn)
    			|| entProductFilter == null || StringUtils.isEmpty(deleteReleaseProduct)) {
    	    mapResult.put("statusDelete", "fail");
    		return "deleteAndUpdate";
    	}

    	String[] arrayProducts =  productId.split("_");
    	String[] arrayUpdateOn =  updateOn.split("_");
    	try {
    		/* BOE Get loginId to update product_maintenance_flg when deleting product @rcv!nguyen.hieu 2014/03/11. */
    	    //matterDetailService.deleteProducts(arrayProducts, arrayUpdateOn);
    		String loginId = this.getCommonForm().getLoginId();
    		matterDetailService.deleteProducts(arrayProducts, arrayUpdateOn, loginId, entProductFilter, deleteReleaseProduct);
    	    /* EOE Get loginId to update product_maintenance_flg when deleting product. */
    	    mapResult.put("statusDelete", "success");
		} catch (CustomException e) {
		    mapResult.put("statusDelete", "fail");
		    /* BOE # Luong.Dai comment code add product error to response */
		    /*EntTblFactoryProductNew product = (EntTblFactoryProductNew) e.getObj();

		    if (product != null) {
		        this.setUserUpdate(product.getUpdatedUserId());
		        //Get userName
		        EntMstFactoryUserTmp user = userTmpService.selectUserById(userUpdate);
		        if (user != null) {
		            mapResult.put("userUpdated", user.getUserLastName() + " " + user.getUserFirstName());
		        }
		        this.setUpdatedOn(product.getUpdatedOn());
		        if (this.updatedOn != null) {
		            mapResult.put("updatedOn", Utils.formatDateFull(this.updatedOn));
		        }
		     */
		        //BOE Tran.Thanh : handle exception when had product release
		        mapResult.put("hadProductRelease", "Had product release");
		        //EOE Tran.Thanh : handle exception when had product release
/*		    }*/
	        /* BOE # Luong.Dai comment code add product error to response */

		} catch (Exception e) {
            this.getLogger().error("[MatterDetailAction] An error when delete Products");
            this.getLogger().error(e.toString());
            mapResult.put("statusDelete", "fail");
		}
    	return "deleteAndUpdate";

    }

    /************************************************************************
     * <b>Description:</b><br>
     *  update matter detail.
     *
     * @author		Luong.Dai
     * @date		Jan 20, 2014
     * @return		String
     ************************************************************************/
    @Action("/updateMatterDetail")
    public String updateMatterDetail() {
        Integer updateResult = 0;
        mapResult = new HashMap<Object, Object>();
        try {
            setStatusUpdate("success");

            if (!validateLoginUser()) {
                return "errorOther";
            }

            EntMstFactoryMatterNew matter = matterDetailActionForm.getEntMstFactoryMatterNew();

            if (matter == null) {
                return "errorOther";
            }
            /* BOE fix authority for by Luong.Dai 2014/03/24 */
            List<String> permission = this.getCommonForm().getLoginAuthority();
            String loginId = this.getCommonForm().getLoginId();
            //If user has role limitedUserId, use can edit only projects that he manager
            if (permission.contains(getText("text.role.roleLimitedUserId"))
            	&& !matter.getMatterChargeUserId().equals(loginId)) {
            	//Return error page no permission
            	mapResult.put("statusUpdate", "noPermission");
            } else {
	            //set update user id
	            matter.setUpdatedUserId(this.getCommonForm().getLoginId());
	            Integer updateStatus = matterDetailService.updateMatterDataByMatterCode(matter);
	            if (updateStatus <= 0) {
	                this.setStatusUpdate("fail");
	            }
	            mapResult.put("statusUpdate", "success");
	            updateResult = 1;
            }
        } catch (CustomException e) {
            mapResult.put("statusUpdate", "otherUpdate");
            EntMstFactoryMatterNew matter = (EntMstFactoryMatterNew) e.getObj();

            if (matter != null) {
                this.setUserUpdate(matter.getUpdatedUserId());
                this.setUpdatedOn(matter.getUpdatedOn());
                //Get userName
                EntMstFactoryUserTmp user = userTmpService.selectUserById(userUpdate);
                if (user != null) {
                    mapResult.put("userUpdated", user.getUserLastName() + " " + user.getUserFirstName());
                }
                if (this.updatedOn != null) {
                    mapResult.put("updatedOn", Utils.formatDateFull(this.updatedOn));
                }
            }
            updateResult = 0;
        } catch (SQLException e) {
            this.getLogger().error("[MatterDetailActionrror when excute sql");
            this.getLogger().error(e.toString());
            mapResult.put("statusUpdate", "fail");
            updateResult = 0;
        }

        if (updateResult == 1) {
            session.put("msg", "update");
        }
        return "deleteAndUpdate";
    }

    /************************************************************************
     * <b>Description:</b><br>
     *  linkToDetailIssueRedmine.
     *
     * @author		Le.Dinh
     * @date		23 Jan 2014
     * @return		String
     ************************************************************************/
    @Action("/linkToDetailIssueRedmine")
    public String linkToDetailIssueRedmine() {
        //Validate loginUser
        if (!validateLoginUser()) {
            return "errorOther";
        }
        String issueId = this.getRequest().getParameter("issueId");
        String loginId = this.getCommonForm().getLoginId();
        String passWordCredentials = this.getCommonForm().getPassWordCredentials();
        String redmineIssuesUrl = settingService.selectSettingValueBySettingCode("REDMINE_URL");
        String redmineIssuesUrlNoHTTP = StringUtils.replace(redmineIssuesUrl, "https://", "");
    	urlIssueRedmine = "https://" + loginId + ":" + passWordCredentials + "@" + redmineIssuesUrlNoHTTP + Constant.REDMINE_ISSUES_URL + issueId + "/";
        return "issueDetail";
    }

    /************************************************************************
     * <b>Description:</b><br>
     *  create string for show in client.
     *  product syouhin sys code
     *  product registration flg
     *
     * @author		Luong.Dai
     * @date		Jan 18, 2014		void
     ************************************************************************/
    private void initStringToShowInClient() {
        List<EntTblFactoryProductNew> listProduct = matterDetailActionForm.getListProduct();
        if (listProduct != null) {
            int size = listProduct.size();

            for (int i = 0; i < size; i++) {
                EntTblFactoryProductNew product = listProduct.get(i);
                initSyouhinSysCode(product);
                initRegistrationFlg(product);
            }
        }
    }

    /************************************************************************
     * <b>Description:</b><br>
     *  create string of syouhin sys code to show in client.
     * tbl_factory_product.product_syouhin_sys_code : If value is 0, show the insertjapanese is mistake
     *
     * @author		Luong.Dai
     * @date		Jan 20, 2014
     * @param product		EntTblFactoryProductNew
     ************************************************************************/
    private void initSyouhinSysCode(EntTblFactoryProductNew product) {
        Long syouhinCode = product.getProductSyouhinSysCode();
        if (syouhinCode == null) {
            product.setShowProductSyouhinSysCode(Constant.SYOUHIN_CODE_MISTAKE);
        } else if (syouhinCode == 0) {
            product.setShowProductSyouhinSysCode(Constant.SYOUHIN_CODE_0);
        } else {
            product.setShowProductSyouhinSysCode(syouhinCode.toString());
        }
    }

    /************************************************************************
     * <b>Description:</b><br>
     *  create string of RegistrationFlg to show in clinet.
     *  tbl_factory_product.product_registration_flg
     *  if flag value = 1, show insertjapanese
     *  If value of any one of among the object 10.11.12.13 > 0, show insertjapanese change to red color font the column.
     *  If value of all the object 10.11.12.13 = 0 and flag value is 0, show insertjapanese
     *
     * @author		Luong.Dai
     * @date		Jan 20, 2014
     * @param product		EntTblFactoryProductNew
     ************************************************************************/
    private void initRegistrationFlg(EntTblFactoryProductNew product) {
        if (product.getProductRegistrationFlg() == 1) {
            product.setShowProductRegistrationFlg(EnumRegistrationFlg.getFlgValueByKey("FlgEqual1"));
        } else if (product.getProductGeneralErrorFlg() > 0
                    || product.getProductCategoryErrorFlg() > 0
                    || product.getProductModelErrorFlg() > 0
                    || product.getProductAttributeErrorFlg() > 0) {
                product.setShowProductRegistrationFlg(EnumRegistrationFlg.getFlgValueByKey("Larger0"));
        } else {
            product.setShowProductRegistrationFlg(EnumRegistrationFlg.getFlgValueByKey("FlgEqual0"));
        }
    }

    /************************************************************************
     * <b>Description:</b><br>
     *  Init message for page.
     *
     * @author		Luong.Dai
     * @date		Jan 22, 2014		void
     ************************************************************************/
    private void initMessage() {
        String msg = (String) session.get("msg");
        session.remove("msg");
        if (msg != null) {
            if (msg == "update") {
                matterDetailActionForm.setMessage(this.getText("message.success.updateSuccess"));
            }
        }
    }

    /************************************************************************
     * <b>Description:</b><br>
     * Check import is not working.
     *
     * @author      hoang.ho
     * @date        2014/02/27
     * @return      String
     ************************************************************************/
    @Action("/ajaxCheckImportCondition")
    public String ajaxCheckImportCondition() {
        long matterNo = matterDetailActionForm.getEntMstFactoryMatterNew().getMatterNo();
        boolean validFlg = matterDetailService.checkImportConditionNotWorking(matterNo);
        int svImportStatus = 0;
        if (!validFlg) {
            svImportStatus = 1;
        }
        matterDetailActionForm.setCsvImportStatus(svImportStatus);
        return "list";
    }

    /************************************************************************
     * <b>Description:</b><br>
     *  get uploaded CSV, read and import to DB.
     *
     * @author      Nguyen.Chuong
     * @date        Feb 26, 2014
     * @return      String
     ************************************************************************/
    @SuppressWarnings("unchecked")
	@Action("/importCSV")
    public String importCSV() {
        try {
        	// BOE @rcv!Tran.Thanh Jul 23, 2014 #9956 : delete old error csv
        	deleteErrorCSV();
        	// EOE @rcv!Tran.Thanh Jul 23, 2014 #9956 : delete old error csv

            if (!StringUtils.isNumeric(this.importMode)) {
                return "list";
            }
            String filePath = Utils.getRealPath(this.getRequest());
            String fullFilePath = "";
            // get matter no
            EntMstFactoryMatterNew ent = this.matterDetailActionForm.getEntMstFactoryMatterNew();
            long matterNo = ent.getMatterNo();
            // Get login Id
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            String loginId = getLoginId(authentication);
            String currentDateStr = Utils.formatDate(new Date(), Constant.FORMAT_DATE_PATTERN_YYYYMMDD_HHMMSS);
            // Create table name
            String tableName = String.format(Constant.TABLE_NAME_FORMAT, loginId, currentDateStr);
            String errorTableName = String.format(Constant.ERROR_TABLE_NAME_FORMAT, loginId, currentDateStr);

            String fileName = "";
            //File convert if encode is shift-jis
            String convertFileName = "";
            // check file upload success
            try {
                // check exist folder images/tmp_image
                File folderTmp = new File(filePath + Constant.FOLDER_TMP_UPLOAD);
                if (!folderTmp.exists()) {
                    FileUtils.forceMkdir(folderTmp);
                }
                // copy file to tmp folder
                fileName = loginId + Constant.CHAR_BETWEEN_USER_IMAGE + this.fileCSVFileName;
                //TODO THANH
                fileName = fileName.toLowerCase();
                //Full path of file
                fullFilePath = filePath + Constant.FOLDER_TMP_UPLOAD + "/" + fileName;

                File fileToCreate = new File(filePath + Constant.FOLDER_TMP_UPLOAD, fileName);
                FileUtils.copyFile(this.fileCSV, fileToCreate);

                /*InputStream fileStream = null;
                fileStream = new FileInputStream(fullFilePath);*/
                // BOE @rcv! Nguyen.Chuong Aug 4, 2014 : check file CSV content enter character or number of "," is not valid
                //Check csv result value:
                //resultCheckFile = true => valid encoding or not content enter character or number of "," is valid
                //resultCheckFile = false => invalid encoding or content enter character or number of "," is invalid(not match with header)
                boolean resultCheckFile;
                /* BOE @rcv! Luong.Dai 2014/09/10: Check encode file by CharsetDetectorUtils */
                /*String formatFile = EncodingUtils.readEncodeFile(fileStream);*/
                String formatFile = CharsetDetectorUtils.detectFileCharset(fullFilePath);
                //if (StringUtils.isNotBlank(formatFile)) {
                	//If file encoding is SHIFT_JIS then convert to UTF-8 and check file content "," or enter character.
                	if (formatFile.equals(Constant.SHIFT_JIS_ENCODE)
                		|| formatFile.equals(Constant.MS932_ENCODE)) {
                		/* EOE @rcv! Luong.Dai 2014/09/10: Check encode file by CharsetDetectorUtils */
                		String tempFileName = Constant.UTF_8_ENCODE + Constant.CHAR_BETWEEN_USER_IMAGE + fileName;
                		convertFileName = filePath + Constant.FOLDER_TMP_UPLOAD + "/" + tempFileName;
                		// BOE @rcv! Nguyen.Chuong Sep 4, 2014 : convert file CSV SHiftJis and check valid number of ",". 
//                		resultCheckFile = EncodingUtils.convertFileFromShiftJisToUTF8(fullFilePath, convertFileName);
                		resultCheckFile = convertFileFromShiftJisToUTF8(fullFilePath, convertFileName);
                		// EOE @rcv! Nguyen.Chuong Sep 4, 2014 : convert file CSV SHiftJis and check valid number of ",".
                		if (resultCheckFile) {
                			this.fileCSV = new File(convertFileName);
                			fullFilePath = convertFileName;
                		}
                	} else {
                		//If encoding is different with SHIFT_JIS then check file content "," or enter character.
                		resultCheckFile = checkCSVFileValid(fullFilePath);
                	}
                	//resultCheckFile = false => return invalid CSV error to user.
                	if (!resultCheckFile) {
                		this.getLogger().error("Error when excute import csv: "
                							+ "file encoding is error or content enter charater or number of ',' is not match");
                		//Set error code to show error messgage inport CSV invalid format in UI.
                		matterDetailActionForm.setCsvImportStatus(Integer.parseInt(Constant.CSV_IMPORT_ERROR_CODE_11));
                        return "list";
                	}
                //}
                // EOE @rcv! Nguyen.Chuong Aug 4, 2014 : check file CSV content enter character or number of "," is not valid
            } catch (RuntimeException e) {
                throw e;
            } catch (Exception ex) {
                matterDetailActionForm.setCsvImportStatus(Constant.CSV_UPLOAD_ERROR_MSG_CODE);
                return "list";
            }
            // Check size of file.
            long fileSize = this.fileCSV.length();
            if (fileSize > Constant.CSV_MAX_FILE_SIZE) {
                matterDetailActionForm.setCsvImportStatus(Constant.CSV_SIZE_ERROR_MSG_CODE);
                return "list";
            }

            // get mode user has chosen.
            int mode = Integer.parseInt(this.importMode);
            // get spliter symbol and bounded symbol
            String splitSymbol = matterDetailActionForm.getSplitSymbol();
            String boundedFieldSymbol = matterDetailActionForm.getBoundedFieldSymbol();
            // check header
            Boolean headerValidFlg = matterDetailService.checkHeaderCSVFile(fullFilePath
                                                                          , splitSymbol, boundedFieldSymbol);
            if (!headerValidFlg) {
                matterDetailActionForm.setCsvImportStatus(Constant.CSV_HEADER_NOT_EXIST_ERROR_MSG_CODE);
                return "list";
            }

            //Start load contain of CSV file.
            //BOE @rcv!Luong.Dai 2014/07/14 #9811: Change default status to true
            //Float import to db
            Boolean importCSVStatus = true;
            //EOE @rcv!Luong.Dai 2014/07/14 #9811: Change default status to true
            EntInfoCSVImport outputInfoImportCSV = new EntInfoCSVImport();
            if (mode == 1) {
                // MODEL ALL
                List<String> listCSVHeaderMode1;
                List<EntCSVMode1> listTmpCSVMode1 = null;
                listCSVHeaderMode1 = matterDetailService.readCSVHeaderMode1(fullFilePath
                                                                          , splitSymbol, boundedFieldSymbol);
                /* BOE #7206 Luong.Dai validate required header */
                //Validate required header
                boolean validHeader = matterDetailService.validateRequiredHeaderFieldMode1(listCSVHeaderMode1);
                if (!validHeader) {
                	matterDetailActionForm.setCsvImportStatus(Constant.CSV_HEADER_NOT_EXIST_ERROR_MSG_CODE);
                    return "list";
                }
                /* EOE #7206 Luong.Dai validate required header */
//                listTmpCSVMode1 = matterDetailService.readCSVContainMode1(fullFilePath
//                                                                        , listCSVHeaderMode1, matterNo, errorTableName, outputInfoImportCSV
//                                                                        , splitSymbol, boundedFieldSymbol);
                Map<Object, Object> mapEntityMode1 = matterDetailService.readCSVContainMode1(fullFilePath
					              , listCSVHeaderMode1, matterNo, errorTableName, outputInfoImportCSV
					              , splitSymbol, boundedFieldSymbol
					              , loginId);
                //Get Status Import CSV
                String status = (String) mapEntityMode1.get(Constant.STATUS_READ_CSV_CONTAINS);
                //BOE @rcv!Luong.Dai 2014/07/14 #9811: If wrong data in file csv, errorCSVData = true
                boolean errorCSVData = false;
                //BOE @rcv!Luong.Dai 2014/07/14 #9811: If wrong data in file csv, errorCSVData = true
                //If status has error
                if (status != null) {
                	if (status.equals(Constant.STATUS_READ_CSV_WRONG_HEADER)) {
                		//Read CSV Wrong Header
                    	matterDetailActionForm.setCsvImportStatus(Constant.CSV_WRONG_HEADER_ERROR_MSG_CODE);
                        return "list";
                    } else if (status.equals(Constant.STATUS_READ_CSV_MAXIMUN_25000)) {
                        //Read CSV over 25000 product
                        matterDetailActionForm.setCsvImportStatus(Constant.CSV_MAXIMUM_IMPORT_LENGTH);
                        return "list";
                    //BOE @rcv!Luong.Dai 2014/07/14 #9811: Add status wrong data
                    } else if (status.equals(Constant.STATUS_READ_CSV_MAXIMUN_25000_AFTER_IMPORT)) {
                        //Read CSV over 20000 product after delete,update
                        matterDetailActionForm.setCsvImportStatus(Constant.MAXIMUN_IMPORT_PRODUCT_CSV);
                        return "list";
                    } else if (status.equals(Constant.STATUS_READ_CSV_WRONG_DATA)) {
                       errorCSVData = true;
                    }
                	//EOE @rcv!Luong.Dai 2014/07/14 #9811: Add status wrong data
                } else {
                    //Have no error when reader csv
                    listTmpCSVMode1  = (List<EntCSVMode1>) mapEntityMode1.get(Constant.LIST_CSV_READ_FROM_CSV);
                }
                //BOE @rcv!Luong.Dai 2014/07/14 #9811: Add if condition, check wrong data
                //If wrong data, not insert data to DB
                if (!errorCSVData) {
	                /* BOE #7206 Luong.Dai cannot read header */
	                if (listTmpCSVMode1 == null) {
	                    matterDetailActionForm.setCsvImportStatus(Constant.CSV_WRONG_HEADER_ERROR_MSG_CODE);
	                    return "list";
	                }
	                /* EOE #7206 Luong.Dai cannot read header */
	                // Create table used for insert value CSV into database.
	                //BOE #7864 Tran.Thanh 2014/05/27 : add login id to get temporary table
	                if (!matterDetailService.createTemporaryTable(tableName, mode, loginId)) {
	                //if (!matterDetailService.createTemporaryTable(tableName, mode)) {
	                //EOE #7864 Tran.Thanh 2014/05/27 : add login id to get temporary table
	                	/* BOE #7206 Luong.Dai sent message code to server */
	                    /*return "error";*/
	                	matterDetailActionForm.setCsvImportStatus(Constant.CSV_CANNOT_CREATE_TEMP_TABLE_MSG_CODE);
	                	return "list";
	                	/* EOE #7206 Luong.Dai sent message code to server */
	                }
	                //Insert list product to DB;
	                importCSVStatus = matterDetailService.insertListProductCSVToDB(listTmpCSVMode1, tableName);
	                if (importCSVStatus) {
	                    /*BOE #7950 Tran.Thanh : count new product in list return */
	                    int countNewProduct = 0;
	                    for (EntCSVMode1 entMode1 : listTmpCSVMode1) {
	                        if (entMode1.getImportMode().equals(Constant.NEW_MODE_IMPORT_CSV)) {
	                            countNewProduct++;
	                        }
	                    }
	                    matterDetailActionForm.setCountNewProductInCSVFile(countNewProduct);
	                    /*EOE #7950 Tran.Thanh : count new product in list return */

	                    //Remove invalid header in list header mode 1
	                    listCSVHeaderMode1.removeAll(Arrays.asList(null, ""));
	                    matterDetailActionForm.setListCSVHeaderMode1(listCSVHeaderMode1);
	                } else {
	                    matterDetailService.dropCSVTempTable(1, tableName, errorTableName);
	                }
                } else {
                	//Remove invalid header in list header mode 1
                    listCSVHeaderMode1.removeAll(Arrays.asList(null, ""));
                    matterDetailActionForm.setListCSVHeaderMode1(listCSVHeaderMode1);
                    // BOE @rcv!Tran.Thanh Jul 15, 2014 #9956 : add status import csv
                    matterDetailActionForm.setCsvImportStatus(Constant.ERROR_IMPORT_CSV_FILE);
                    // EOE @rcv!Tran.Thanh Jul 15, 2014 #9956 : add status import csv
                }
                //EOE @rcv!Luong.Dai 2014/07/14 #9811: Add if condition, check wrong data
            } else {
                // MODEL FIT_MODEL, ATTRIBUTE
                List<EntTblTmpCSV> listTmpCSV = null;
                listTmpCSV = matterDetailService.readCSVFile(fullFilePath, mode
                                                           , matterNo, errorTableName
                                                           , outputInfoImportCSV
                                                           , splitSymbol, boundedFieldSymbol);
                // Create table used for insert value CSV into database.
                //BOE #7864 Tran.Thanh 2014/05/27 : add login id to get temporary table
                //if (!matterDetailService.createTemporaryTable(tableName, mode)) {
                if (!matterDetailService.createTemporaryTable(tableName, mode, loginId)) {
                //EOE #7864 Tran.Thanh 2014/05/27 : add login id to get temporary table
                    return "error";
                }
                //Insert list product to DB;
                importCSVStatus = matterDetailService.insertListProductCSVToDB(listTmpCSV, tableName, mode);
            }
            //Remove convert file
            if (StringUtils.isNotBlank(convertFileName)) {
            	File fileToDelete = new File(convertFileName);
        		if (fileToDelete.exists()) {
        			FileUtils.forceDelete(fileToDelete);
        		}
            }
            //Set imported status.
            matterDetailActionForm.setImportCSVStatus(importCSVStatus);
            //Set created table in DB
            matterDetailActionForm.setImportedCSVTableName(tableName);
            //Set csv file name
            matterDetailActionForm.setCsvfileName(this.fileCSVFileName);
            //Set real file name
            matterDetailActionForm.setRealFileName(fileName);
            //Set errorTableName
            matterDetailActionForm.setErrorCSVTableName(errorTableName);
            // Set status of import.
            matterDetailActionForm.setOutputInfoImportCSV(outputInfoImportCSV);
		} catch (JsonParseException e) {
        	this.getLogger().error("Error when excute import csv: catch JsonParseException invalid CSV file: column over max");
            this.getLogger().error(e.toString());
        	//File CSV invalid format
            matterDetailActionForm.setCsvImportStatus(Constant.CSV_INVALID_FORMAT);
            return "list";
        } catch (NullPointerException e) {
        	this.getLogger().error("Error when excute import csv: catch NullPointerException invalid CSV file: data in cell invalid");
            this.getLogger().error(e.toString());
        	//File CSV invalid format
            matterDetailActionForm.setCsvImportStatus(Constant.CSV_INVALID_FORMAT);
            return "list";
        } catch (RuntimeException e) {
        	//BOE @rcv!Luong.Dai 2014/07/09 #9981: Go to error page
            /*throw e;*/
        	this.getLogger().error("Error when excute import csv");
        	return "error";
        	//EOE @rcv!Luong.Dai 2014/07/09 #9981: Go to error page
        } catch (Exception e) {
            // Handle exception
            this.getLogger().error("Error in MatterDetailAction");
            this.getLogger().error("Error at importCSV fucntion when insert data in csv file to DB");
            this.getLogger().error(e.toString());
            return "error";
        }
        return "list";
    }

  
    /************************************************************************
     * <b>Description:</b><br>
     *  load data grid for mode all of import CSV popup after insert to DB.
     *
     * @author		Nguyen.Chuong
     * @date		Feb 25, 2014
     * @return		String json
     ************************************************************************/
    @Action("/loadDataForImportCSV")
    public String loadDataForImportCSV() {
        if (!StringUtils.isNumeric(matterDetailActionForm.getImportedCSVMode())) {
            return "list";
        }
        int mode = Integer.parseInt(matterDetailActionForm.getImportedCSVMode());
        //List ent temp in temp table.
        List<EntTblTmpCSV> listProduct = null;
        int totallistProductInDB;
        int limit = Integer.parseInt(pageSize);
        int offset = (Integer.parseInt(page) - 1) * Integer.parseInt(pageSize);
        listProduct = matterDetailService.selectListTmpDataInDB(matterDetailActionForm.getImportedCSVTableName()
                                                              , matterDetailActionForm.getEntTblTmpCSVFilter()
                                                              , limit
                                                              , offset
                                                              , mode);
        totallistProductInDB = matterDetailService.selectTotalListImportedCSVFilter(matterDetailActionForm.getImportedCSVTableName()
                                                                                  , matterDetailActionForm.getEntTblTmpCSVFilter()
                                                                                  , mode);
        if (listProduct == null) {
            listProduct = new ArrayList<EntTblTmpCSV>();
        }
        //Set list all product and attribute to action form
        matterDetailActionForm.setListImportedProduct(listProduct);
        matterDetailActionForm.setListImportedProductSize(totallistProductInDB);
        return "list";
    };

    /**
     * exportCSV mode 3.
     * @throws IOException e
     */
    @Action("/exportCSVMode3")
    public void exportCSVMode3() throws IOException {
        PrintWriter out = null;
        try {
            //BOE Nguyen.Chuong 2014/03/14 export CSV all page
            // type of file
            ServletActionContext.getResponse().setContentType("application/csv");
//            String matterNo = this.getRequest().getParameter("matterNo");
            String time = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
            // file name
            String fileName = time + "_" + matterDetailActionForm.getProduct().getMatterNo() + ".csv";
            /*ServletActionContext.getResponse().setHeader("content-disposition", "fileName=" + URLEncoder.encode(fileName, Constant.UTF_8_ENCODE));*/
            ServletActionContext.getResponse().setHeader("content-disposition", "fileName=" + URLEncoder.encode(fileName, Constant.SHIFT_JIS_ENCODE));
            // encoding for file in shift-jis
            ServletActionContext.getResponse().setCharacterEncoding(Constant.SHIFT_JIS_ENCODE);
            /*ServletActionContext.getResponse().setCharacterEncoding(Constant.UTF_8_ENCODE);*/
            // write file
            out = ServletActionContext.getResponse().getWriter();
            StringBuilder sbHeader = new StringBuilder();
            //BOE #7206 Nguyen.Chuong 2014/05/13: change load text header from properties to Enum.
//            sbHeader.append(getText("updateMode") + ",");
//            sbHeader.append(getText("identificationNumber") + ",");
//            sbHeader.append(getText("systemProductCode") + ",");
//            sbHeader.append(getText("atributeName") + ",");
//            sbHeader.append(getText("administrativeName") + ",");
//            sbHeader.append(getText("displayName"));
            sbHeader.append(EnumImportCSVMode1Header.getHeaderJPByHeaderEN("importMode") + ",");
            sbHeader.append(EnumImportCSVMode1Header.getHeaderJPByHeaderEN("productId") + ",");
            sbHeader.append(EnumImportCSVMode1Header.getHeaderJPByHeaderEN("productSyouhinSysCode") + ",");
            sbHeader.append(EnumImportCSVMode1Header.getHeaderJPByHeaderEN("attributeName") + ",");
            sbHeader.append(EnumImportCSVMode1Header.getHeaderJPByHeaderEN("attributeValue") + ",");
            sbHeader.append(EnumImportCSVMode1Header.getHeaderJPByHeaderEN("attributeDisplay") + ",");
            //BOE #7206 Nguyen.Chuong 2014/05/13: change load text header from properties to Enum.
            // Header
            out.print(sbHeader.toString() + "\r\n");
            // data
//            String productIds = this.getRequest().getParameter("productIds");
//            String attributeCodes = this.getRequest().getParameter("attributeCodes");
//            if (StringUtils.isBlank(productIds)) {
//                return;
//            }
            List<String> productIdList = Collections.emptyList();
            Boolean exportAll = matterDetailActionForm.getExportAll();
            if (exportAll) {
                // Get all product IDs.
                EntTblFactoryProductNew product = matterDetailActionForm.getProduct();
                if (StringUtils.isEmpty(product.getSortDir()) || StringUtils.isEmpty(product.getSortField())) {
                    product.setSortField("productId");
                    product.setSortDir("ASC");
                }
                productIdList = productService.selectProductIdListByFilterForExportCSV(product);
            } else {
                String productIds = matterDetailActionForm.getProductIds();
                if (StringUtils.isBlank(productIds)) {
                    return;
                }
                productIdList = Arrays.asList(productIds.split(","));
            }
            String attributeCodes = matterDetailActionForm.getAttributeCodes();
            List<String> attributeCodeList = null;
            if (StringUtils.isNotBlank(attributeCodes)) {
                attributeCodeList = Arrays.asList(attributeCodes.split(","));
            }
            List<EntCSVMode3> data = new ArrayList<EntCSVMode3>();
            // Select info of maximum 10000 products each time to prevent connection timeout with MySQL
            for (int i = 0; i <= productIdList.size() / Constant.INT_10000; i++) {
                int from = i * Constant.INT_10000;
                int to = (i + 1) * Constant.INT_10000;
                if (to > productIdList.size()) {
                    to = productIdList.size();
                }
                List<EntCSVMode3> tmp = productService.selectProductAttributeByProductIds(productIdList.subList(from, to)
                                                                                        , attributeCodeList);
                data.addAll(tmp);
            }
            StringBuilder sbData;
//            List<EntMstAttribute> attributeList = productService.selectAttributeList();
//            data = removeDeletedAttribute(data, attributeList);
            for (EntCSVMode3 ent : data) {
                sbData = new StringBuilder();
                //BOE #7841 Nguyen.Chuong 2014/05/13 add default mode when export CSV.
                sbData.append(Constant.DEFAULT_MODE_EXPORT_CSV + ",");
                //BOE #7841 Nguyen.Chuong 2014/05/13 add default mode when export CSV.
                sbData.append("P" + ent.getProductId() + ",");
                sbData.append(ent.getProductSyouhinSysCode() + ",");
//                sbData.append(productService.selectAttributeNameByAttributeCode(attributeList, ent.getAttributeCode1()) + ",");
                sbData.append(ent.getAttributeName() + ",");
                sbData.append(ent.getAttributeValues1() + ",");
                sbData.append(ent.getAttributeDisplay1());
                sbData.append("\r\n");
                out.print(sbData.toString());
            }
            //EOE Nguyen.Chuong 2014/03/14
        } catch (SQLException e) {
            this.getLogger().error(e.toString());
        }  finally {
            if (out != null) {
                try {
                    out.flush();
                    out.close();
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            }
        }
    }

//    /**
//     * removeDeletedAttribute.
//     * @param dataList List<EntCSVMode3>
//     * @param attrList List<EntMstAttribute>
//     * @return List<EntCSVMode3>
//     */
//    private List<EntCSVMode3> removeDeletedAttribute(List<EntCSVMode3> dataList, List<EntMstAttribute> attrList) {
//        List<EntCSVMode3> result = new ArrayList<EntCSVMode3>();
//        for (EntCSVMode3 ent : dataList) {
//            if (isAttributeCodeExist(attrList, ent.getAttributeCode1() + "")) {
//                result.add(ent);
//            }
//        }
//        return result;
//    }

//    /**
//     * isAttributeCodeExist.
//     * @param attrList List<EntMstAttribute>
//     * @param attrCode String
//     * @return boolean
//     */
//    private boolean isAttributeCodeExist(List<EntMstAttribute> attrList, String attrCode) {
//        for (EntMstAttribute ent : attrList) {
//            if (ent.getAttributeCode().equals(attrCode)) {
//                return true;
//            }
//        }
//        return false;
//    }

    /**
     * exportCSV mode 2.
     * @throws IOException e
     */
    @Action("/exportCSVMode2")
    public void exportCSVMode2() throws IOException {
        PrintWriter out = null;
        try {
            //BOE Nguyen.Chuong 2014/03/14 export CSV all page
            // type of file
            ServletActionContext.getResponse().setContentType("application/csv");
//            String matterNo = this.getRequest().getParameter("matterNo");
            String time = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
            // file name
            String fileName = time + "_" + matterDetailActionForm.getProduct().getMatterNo() + ".csv";
            /*ServletActionContext.getResponse().setHeader("content-disposition", "fileName=" + URLEncoder.encode(fileName, Constant.UTF_8_ENCODE));*/
            ServletActionContext.getResponse().setHeader("content-disposition", "fileName=" + URLEncoder.encode(fileName, Constant.SHIFT_JIS_ENCODE));
            // encoding for file in shift-jis
            ServletActionContext.getResponse().setCharacterEncoding(Constant.SHIFT_JIS_ENCODE);
            /*ServletActionContext.getResponse().setCharacterEncoding(Constant.UTF_8_ENCODE);*/
            // write file
            out = ServletActionContext.getResponse().getWriter();
            StringBuilder sbHeader = new StringBuilder();
            //BOE #7206 Nguyen.Chuong 2014/05/13: change load text header from properties to Enum.
//            sbHeader.append(getText("updateMode") + ",");
//            sbHeader.append(getText("identificationNumber") + ",");
//            sbHeader.append(getText("systemProductCode") + ",");
//            sbHeader.append(getText("maker") + ",");
//            sbHeader.append(getText("carModel") + ",");
//            sbHeader.append(getText("bikeModel"));
            sbHeader.append(EnumImportCSVMode1Header.getHeaderJPByHeaderEN("importMode")            + ","); //更新モード
            sbHeader.append(EnumImportCSVMode1Header.getHeaderJPByHeaderEN("productId")            + ","); //識別番号
            sbHeader.append(EnumImportCSVMode1Header.getHeaderJPByHeaderEN("productSyouhinSysCode") + ","); //システム品番
            sbHeader.append(EnumImportCSVMode1Header.getHeaderJPByHeaderEN("fitModelMaker")         + ","); //メーカー
            sbHeader.append(EnumImportCSVMode1Header.getHeaderJPByHeaderEN("fitModelModel")         + ","); //車種
            sbHeader.append(EnumImportCSVMode1Header.getHeaderJPByHeaderEN("fitModelStyle")         + ","); //年式・型式
            //EOE #7206 Nguyen.Chuong 2014/05/13: change load text header from properties to Enum.
            // Header
            out.print(sbHeader.toString() + "\r\n");
            // data
//            String productIds = this.getRequest().getParameter("productIds");
//            if (StringUtils.isBlank(productIds)) {
//                return;
//            }
//            List<String> productIdList = Arrays.asList(productIds.split(","));
            List<String> productIdList = Collections.emptyList();
            Boolean exportAll = matterDetailActionForm.getExportAll();
            if (exportAll) {
                // Get all product IDs.
                EntTblFactoryProductNew product = matterDetailActionForm.getProduct();
                if (StringUtils.isEmpty(product.getSortDir()) || StringUtils.isEmpty(product.getSortField())) {
                    product.setSortField("productId");
                    product.setSortDir("ASC");
                }
//              product.setMatterNo(Integer.parseInt(matterNo));
                productIdList = productService.selectProductIdListByFilterForExportCSV(product);
            } else {
                String productIds = matterDetailActionForm.getProductIds();
                if (StringUtils.isBlank(productIds)) {
                    return;
                }
                productIdList = Arrays.asList(productIds.split(","));
            }
            List<EntCSVMode3> data = new ArrayList<EntCSVMode3>();
            // Select info of maximum 10000 products each time to prevent connection timeout with MySQL
            for (int i = 0; i <= productIdList.size() / Constant.INT_10000; i++) {
                int from = i * Constant.INT_10000;
                int to = (i + 1) * Constant.INT_10000;
                if (to > productIdList.size()) {
                    to = productIdList.size();
                }
                List<EntCSVMode3> tmp = productService.selectProductFitModelByProductIds(productIdList.subList(from, to));
                data.addAll(tmp);
            }
            StringBuilder sbData;
            for (EntCSVMode3 ent : data) {
                sbData = new StringBuilder();
                //BOE #7841 Nguyen.Chuong 2014/05/13 add default mode when export CSV.
                sbData.append(Constant.DEFAULT_MODE_EXPORT_CSV + ",");
                //EOE #7841 Nguyen.Chuong 2014/05/13 add default mode when export CSV.
                sbData.append("P" + ent.getProductId() + ",");
                sbData.append(ent.getProductSyouhinSysCode() + ",");
                sbData.append(ent.getAttributeValues1() + ",");
                sbData.append(ent.getAttributeValues2() + ",");
                sbData.append(ent.getAttributeValues3());
                sbData.append("\r\n");
                out.print(sbData.toString());
            }
            //EOE Nguyen.Chuong 2014/03/14
        } catch (SQLException e) {
            this.getLogger().error(e.toString());
        } finally {
            if (out != null) {
                try {
                    out.flush();
                    out.close();
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            }
        }
    }

    /************************************************************************
     * <b>Description:</b><br>
     *  process data of csv with database.
     *
     * @author      ho.hoang
     * @date        Feb 26, 2014
     * @return      String
     ************************************************************************/
    @Action("/reflectCSVData")
    public String reflectCSVData() {
        try {
            if (!StringUtils.isNumeric(this.importMode)) {
                return "list";
            }
           // get mode user has chosen.
            // get spliter symbol and bounded symbol
            String splitSymbol = matterDetailActionForm.getSplitSymbol();
            String boundedFieldSymbol = matterDetailActionForm.getBoundedFieldSymbol();
            // String get realFileName
            String realFileName = matterDetailActionForm.getRealFileName();
            // String get Csv file name
            String csvfileName = matterDetailActionForm.getCsvfileName();
            // String get table name
            String importedCSVTableName = matterDetailActionForm.getImportedCSVTableName();
            // String get table name
            String errorCSVTableName = matterDetailActionForm.getErrorCSVTableName();
            // get list Product
            //BOE #7882 Tran.Thanh : get matterNo from request
            Long matterNo = matterDetailActionForm.getEntMstFactoryMatterNew().getMatterNo();
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            String loginId = getLoginId(authentication);
            //EOE #7882 Tran.Thanh : get matterNo from request
            int mode = Integer.parseInt(this.importMode);
            if (mode == 1) {
                EntCSVMode1 entCSVMode1 = new EntCSVMode1();
                entCSVMode1.setSortField("productId");
                entCSVMode1.setSortDir("ASC");
                //List product to import CSV
                List<List<EntCSVMode1>> listProductToImport = new ArrayList<List<EntCSVMode1>>();
                //Total record
                Integer totalProduct = matterDetailService.selectTotalRecordForGridMode1(importedCSVTableName, entCSVMode1);

            	int offset = 1;
            	for (int i = 0; i < totalProduct;) {
            		int limit = Constant.MAX_PRODUCT_IMPORT_CSV_MODE_1;
            		List<EntCSVMode1> dataList = matterDetailService.selectListTempDataForGridMode1(importedCSVTableName
            		                                                                              , limit
            		                                                                              , offset
            		                                                                              , entCSVMode1);
            		if (dataList != null) {
            			listProductToImport.add(dataList);
            			i += limit;
            			offset++;
            		}
            	}
                /*List<EntCSVMode1> dataList = matterDetailService.selectListTempDataForGridMode1(importedCSVTableName, Integer.MAX_VALUE, 1, entCSVMode1);*/
                /*EntTblFactoryImportCondition importCondition =  createImportCondition(dataList.size(), realFileName, csvfileName);*/
                EntTblFactoryImportCondition importCondition =  createImportCondition(totalProduct, realFileName, csvfileName);
                //BOE Tran.Thanh 2014/05/15 : update table numbering and set true productId to list
                /*int result = matterDetailService.updateNumbering(dataList, matterDetailActionForm.getCountNewProductInCSVFile());*/
                int result = matterDetailService.updateNumbering(listProductToImport, matterDetailActionForm.getCountNewProductInCSVFile());
                if (result > 0) {
                	/*matterDetailService.importCSVDataMode1(dataList, mode, importCondition, importedCSVTableName,
                            errorCSVTableName, splitSymbol, boundedFieldSymbol);*/
                	matterDetailService.importCSVDataMode1(listProductToImport, mode, importCondition, importedCSVTableName,
                            errorCSVTableName, splitSymbol, boundedFieldSymbol, matterNo, loginId);
                }
                //BOE Tran.Thanh 2014/05/15 : add process for mode insert
            } else {
                List<EntTblTmpCSV> entTblTmpCSVList = matterDetailService.selectAllListTmpDataInDB(importedCSVTableName, mode);
                // create import condition object
                EntTblFactoryImportCondition importCondition =  createImportCondition(entTblTmpCSVList.size(), realFileName, csvfileName);
                matterDetailService.processReflectCSVData(entTblTmpCSVList, mode, importCondition, importedCSVTableName, errorCSVTableName
                , splitSymbol, boundedFieldSymbol);
            }
            //process reflect csv data.
        } catch (Exception e) {
            // Handle exception
            this.getLogger().error("Error in MatterDetailAction");
            this.getLogger().error("Error at importCSV fucntion when insert data in csv file to DB");
            this.getLogger().error(e.toString());
            //BOE #7854 Tran.Thanh 2014/05/19 : add logic when exception Deadlock
            if (e.getClass().toString().contains("Deadlock")) {
            	//Delete table temp
            	// String get table name
            	//String importedCSVTableName = matterDetailActionForm.getImportedCSVTableName();
            	//matterDetailService.dropTableTemp(importedCSVTableName);
            	mapResult = new HashMap<Object, Object>();
            	mapResult.put("messageErrorDeadlock", getText("message.error.deadlock"));
            	return "errorDeadlock";
        	 //EOE #7854 Tran.Thanh 2014/05/19 : add logic when exception Deadlock
            } else {
            	return "error";
            }
        }
        return "list";
    }
    /************************************************************************
     * <b>Description:</b><br>
     *  drop temporary data when cancel process.
     *
     * @author      ho.hoang
     * @date        Feb 26, 2014
     * @return      String
     ************************************************************************/
    @Action("/cancelImportCSVData")
    public String cancelImportCSVData() {
        try {
            if (!StringUtils.isNumeric(this.importMode)) {
                return "list";
            }
           // get mode user has chosen.
            int mode = Integer.parseInt(this.importMode);
            // String get table name
            String importedCSVTableName = matterDetailActionForm.getImportedCSVTableName();
            // String get table name
            String errorCSVTableName = matterDetailActionForm.getErrorCSVTableName();
            //If imported table name is null then return error.
            if (StringUtils.isNotEmpty(importedCSVTableName)) {
             // process reflect csv data.
                matterDetailService.dropCSVTempTable(mode, importedCSVTableName, errorCSVTableName);
                return "list";
            }
            return "error";
        } catch (Exception e) {
            // Handle exception
            this.getLogger().error("Error in MatterDetailAction");
            this.getLogger().error("Error at importCSV fucntion when insert data in csv file to DB");
            this.getLogger().error(e.toString());
            return "error";
        }
    }
    /**
     * exportCSV mode 1.
     * @throws IOException e
     */
    @Action("/exportCSVMode1")
    public void exportCSVMode1() throws IOException {
        PrintWriter out = null;
        try {
            // type of file
            ServletActionContext.getResponse().setContentType("application/csv");
            String time = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
            // file name
            String fileName = time + "_" + matterDetailActionForm.getProduct().getMatterNo() + ".csv";
            /*ServletActionContext.getResponse().setHeader("content-disposition", "fileName=" + URLEncoder.encode(fileName, Constant.UTF_8_ENCODE));*/
            ServletActionContext.getResponse().setHeader("content-disposition", "fileName=" + URLEncoder.encode(fileName, Constant.SHIFT_JIS_ENCODE));
            /* BOE #7206 Luong.Dai convert to UTF-8 */
            // encoding for file in shift-jis
            ServletActionContext.getResponse().setCharacterEncoding(Constant.SHIFT_JIS_ENCODE);
            //ServletActionContext.getResponse().setCharacterEncoding(Constant.UTF_8_ENCODE);
            /* EOE #7206 Luong.Dai convert to UTF-8 */
            // get headers
            String headerString = matterDetailActionForm.getHeaderList();
            out = ServletActionContext.getResponse().getWriter();
            /* BOE Fix bug #9 cannot export all CSV file @rcv!nguyen.hieu 2014/03/10. */
            // data
            /*
            String productIds = this.getRequest().getParameter("productIds");
            if (StringUtils.isBlank(productIds)) {
                return;
            }
            List<String> productIdList = Arrays.asList(productIds.split(","));
            */
            List<String> productIdList = Collections.emptyList();
            Boolean exportAll = matterDetailActionForm.getExportAll();
            if (exportAll) {
            	// Get all product IDs.
            	EntTblFactoryProductNew product = matterDetailActionForm.getProduct();
            	if (StringUtils.isEmpty(product.getSortDir()) || StringUtils.isEmpty(product.getSortField())) {
            	    product.setSortField("productId");
            	    product.setSortDir("ASC");
            	}
//            	product.setMatterNo(Integer.parseInt(matterNo));
            	productIdList = productService.selectProductIdListByFilterForExportCSV(product);
            } else {
                String productIds = matterDetailActionForm.getProductIds();
                if (StringUtils.isBlank(productIds)) {
                    return;
                }
                productIdList = Arrays.asList(productIds.split(","));
            }
            /* EOE Fix bug #9 cannot export all CSV file. */
            List<EntCSVMode1> data = new ArrayList<EntCSVMode1>();
            List<EntCSVMode1> tmp;
            // Select info of maximum 2000 products each time to prevent connection timeout with MySQL
            // This query select data in multiple tables, so that limit at 2000 per select
            for (int i = 0; i <= productIdList.size() / Constant.INT_2000; i++) {
                int from = i * Constant.INT_2000;
                int to = (i + 1) * Constant.INT_2000;
                if (to > productIdList.size()) {
                    to = productIdList.size();
                }
                tmp = productService.selectProductInfoExportCSVMode1SingleTable(productIdList.subList(from, to));
                data.addAll(tmp);
            }
            String attributeCodes = matterDetailActionForm.getAttributeCodes();
            List<String> attributeCodeList = null;
            List<EntMstAttribute> attributeNameList = new ArrayList<EntMstAttribute>();
            if (StringUtils.isNotBlank(attributeCodes)) {
                attributeCodeList = Arrays.asList(attributeCodes.split(","));
                List<EntCSVMode3> attributeDataList = new ArrayList<EntCSVMode3>();
                List<EntCSVMode3> tmp3;
                // Select info of maximum 5000 products each time to prevent connection timeout with MySQL
                // This query select data in few tables, so that limit at 5000 per select
                for (int i = 0; i <= productIdList.size() / Constant.INT_5000; i++) {
                    int from = i * Constant.INT_5000;
                    int to = (i + 1) * Constant.INT_5000;
                    if (to > productIdList.size()) {
                        to = productIdList.size();
                    }
                    tmp3 = productService.selectProductAttributeByProductIds(productIdList.subList(from, to),
                        attributeCodeList);
                    attributeDataList.addAll(tmp3);
                }
                // set attribute code for each field
                attributeNameList = productService.selectAttributeByCodes(attributeCodeList);
                productService.appendAttributeCode(data, attributeNameList);
                // set attribute data for each field
                productService.appendAttributeData(data, attributeDataList);
            }
            if (data.size() > 0) {
                if (headerString.contains("supportedBike")) {
                    List<EntCSVMode1> subList = new ArrayList<EntCSVMode1>();
                    // Select info of maximum 5000 products each time to prevent connection timeout with MySQL
                    // This query select data in few tables, so that limit at 5000 per select
                    for (int i = 0; i <= productIdList.size() / Constant.INT_5000; i++) {
                        int from = i * Constant.INT_5000;
                        int to = (i + 1) * Constant.INT_5000;
                        if (to > productIdList.size()) {
                            to = productIdList.size();
                        }
                        tmp = productService.selectProductCompatibleModelByProductIds(productIdList.subList(from, to));
                        subList.addAll(tmp);
                    }
                    productService.appendDataByFieldName(data, subList, "compatibleModel");
                }
                if (headerString.contains("customerConfirmationItem")) {
                    List<EntCSVMode1> subList = new ArrayList<EntCSVMode1>();
                    // Select info of maximum 5000 products each time to prevent connection timeout with MySQL
                    // This query select data in few tables, so that limit at 5000 per select
                    for (int i = 0; i <= productIdList.size() / Constant.INT_5000; i++) {
                        int from = i * Constant.INT_5000;
                        int to = (i + 1) * Constant.INT_5000;
                        if (to > productIdList.size()) {
                            to = productIdList.size();
                        }
                        tmp = productService.selectProductGuestInputInfoByProductIds(productIdList.subList(from, to));
                        subList.addAll(tmp);
                    }
                    productService.appendDataByFieldName(data, subList, "customerConfirmationItem");
                }
                if (headerString.contains("link")) {
                    List<EntCSVMode1> subList = new ArrayList<EntCSVMode1>();
                    // Select info of maximum 5000 products each time to prevent connection timeout with MySQL
                    // This query select data in few tables, so that limit at 5000 per select
                    for (int i = 0; i <= productIdList.size() / Constant.INT_5000; i++) {
                        int from = i * Constant.INT_5000;
                        int to = (i + 1) * Constant.INT_5000;
                        if (to > productIdList.size()) {
                            to = productIdList.size();
                        }
                        tmp = productService.selectProductLinkInfoByProductIds(productIdList.subList(from, to));
                        subList.addAll(tmp);
                    }
                    productService.appendDataByFieldName(data, subList, "link");
                }
                if (headerString.contains("video")) {
                    List<EntCSVMode1> subList = new ArrayList<EntCSVMode1>();
                    // Select info of maximum 5000 products each time to prevent connection timeout with MySQL
                    // This query select data in few tables, so that limit at 5000 per select
                    for (int i = 0; i <= productIdList.size() / Constant.INT_5000; i++) {
                        int from = i * Constant.INT_5000;
                        int to = (i + 1) * Constant.INT_5000;
                        if (to > productIdList.size()) {
                            to = productIdList.size();
                        }
                        tmp = productService.selectProductVideoInfoByProductIds(productIdList.subList(from, to));
                        subList.addAll(tmp);
                    }
                    productService.appendDataByFieldName(data, subList, "video");
                }
                if (headerString.contains("image")) {
                    List<EntCSVMode1> subList = new ArrayList<EntCSVMode1>();
                    // Select info of maximum 5000 products each time to prevent connection timeout with MySQL
                    // This query select data in few tables, so that limit at 5000 per select
                    for (int i = 0; i <= productIdList.size() / Constant.INT_5000; i++) {
                        int from = i * Constant.INT_5000;
                        int to = (i + 1) * Constant.INT_5000;
                        if (to > productIdList.size()) {
                            to = productIdList.size();
                        }
                        tmp = productService.selectProductImageInfoByProductIds(productIdList.subList(from, to));
                        subList.addAll(tmp);
                    }
                    productService.appendImageSelectData(data, subList, "image");
                }
                if (headerString.contains("option")) {
                    List<EntCSVMode1> subList = new ArrayList<EntCSVMode1>();
                    // Select info of maximum 5000 products each time to prevent connection timeout with MySQL
                    // This query select data in few tables, so that limit at 5000 per select
                    for (int i = 0; i <= productIdList.size() / Constant.INT_5000; i++) {
                        int from = i * Constant.INT_5000;
                        int to = (i + 1) * Constant.INT_5000;
                        if (to > productIdList.size()) {
                            to = productIdList.size();
                        }
                        tmp = productService.selectProductSelectInfoByProductIds(productIdList.subList(from, to));
                        subList.addAll(tmp);
                    }
                    productService.appendImageSelectData(data, subList, "option");
                }
            }

            // Header
            StringBuilder sbHeader = new StringBuilder();
            createHeader(headerString, sbHeader, new StringBuilder(), new EntCSVMode1(), attributeNameList);
            out.print(sbHeader.toString().replaceFirst(",", "") + "\r\n");
            StringBuilder sbData;
            for (EntCSVMode1 ent : data) {
                sbData = new StringBuilder();
                createHeader(headerString, new StringBuilder(), sbData, ent, attributeNameList);
                out.print(sbData.toString().replaceFirst(",", "") + "\r\n");
            }
        } catch (SQLException e) {
            this.getLogger().error(e.toString());
		} finally {
            if (out != null) {
                try {
                    out.flush();
                    out.close();
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            }
        }
    }

    /************************************************************************
     * <b>Description:</b><br>
     *  download error csv file.
     *
     * @author		Tran.Thanh
     * @date		Jul 10, 2014
     * @throws 		IOException		void
     ************************************************************************/
    @Action("/downloadErrorCSV")
    public void downloadErrorCSV() throws IOException {
        //PrintWriter out = null;
        OutputStream outputStream = null;
        InputStream inputStream = null;
        try {
            // set response type
            ServletActionContext.getResponse().setContentType("application/octet-stream");

            // get file name from request
            String fileName = this.getRequest().getParameter("pathErrorCSV");
            fileName = fileName.replace(Constant.CSV_FILE_EXTENSION, "");

            //Get login Id and set to fileName
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            String loginId = getLoginId(authentication);
            fileName = loginId + Constant.CHAR_BETWEEN_USER_IMAGE + fileName + Constant.ERROR + Constant.CSV_FILE_EXTENSION;

            /*ServletActionContext.getResponse().setHeader("content-disposition", "fileName=" + URLEncoder.encode(fileName, Constant.UTF_8_ENCODE));*/
            // BOE @rcv!Tran.Thanh Jul 31, 2014 #9956 : Add encode fileName
            HttpServletRequest request = ServletActionContext.getRequest();
            String userAgent = request.getHeader("user-agent");
            boolean isInternetExplorer = (userAgent.indexOf("Trident") > -1);
            boolean isChome = (userAgent.indexOf("Chrome") > -1);
            boolean isSafari = (userAgent.indexOf("Safari") > -1);

            if (isInternetExplorer) {
            	ServletActionContext.getResponse().setHeader("Content-disposition", "attachment;"
            		+ "filename=\"" + URLEncoder.encode(fileName, "utf-8") + "\"");
            } else if (isChome) {
            	ServletActionContext.getResponse().addHeader("Content-Disposition", "attachment;"
            		+ "filename=\"" + MimeUtility.encodeWord(fileName, "utf-8", "Q") + "\"");
            } else if (isSafari) {
            	ServletActionContext.getResponse().setHeader("Content-disposition", "attachment;"
            		+ "filename=\"" + new String(fileName.getBytes("UTF-8"), "ISO8859-1") + "\"");
            } else {
            	ServletActionContext.getResponse().addHeader("Content-Disposition", "attachment; "
                 + "filename=\"" + MimeUtility.encodeWord(fileName, "utf-8", "Q") + "\"");
            }
            // EOE @rcv!Tran.Thanh Jul 31, 2014 #9956 : Add encode fileName

            /*ServletActionContext.getResponse().addHeader("Content-Disposition", "attachment;fileName="
            		+ URLEncoder.encode(fileName, Constant.UTF_8_ENCODE));*/

            // encoding for file in shift-jis
            //ServletActionContext.getResponse().setCharacterEncoding(Constant.SHIFT_JIS_ENCODE);
            //ServletActionContext.getResponse().setCharacterEncoding(Constant.UTF_8_ENCODE);
            // get headers

            //set full path at server
            String fullFilePath = Utils.getRealPath(this.getRequest()) + Constant.FOLDER_TMP_UPLOAD + "/"
            				+ loginId + Constant.ERROR + Constant.CSV_FILE_EXTENSION;

            //Create new file and set to output stream
            outputStream = ServletActionContext.getResponse().getOutputStream();

            //File file = new File(Utils.getRealPath(this.getRequest()) + Constant.FOLDER_TMP_UPLOAD, fileName);
            File file = new File(fullFilePath);
            if (file.exists()) {
            	inputStream = new FileInputStream(file);
            	// set input stream to output stream
            	int read = 0;
                byte[] outputByte = new byte[Constant.FILE_READER_BUFFER];
                while ((read = inputStream.read(outputByte, 0, Constant.FILE_READER_BUFFER)) != -1) {
                    outputStream.write(outputByte, 0, read);
                }
            }

        } catch (IOException e) {
        	this.getLogger().error("[MatterDetailAction] An error when download error csv file on Server ");
            this.getLogger().error(e.toString());
		} finally {
            if (outputStream != null) {
                try {
                	outputStream.flush();
                	outputStream.close();
                } catch (Exception e2) {
                	this.getLogger().error("[MatterDetailAction] An error when download error csv file on Server ");
                    e2.printStackTrace();
                }
            }
            if (inputStream != null) {
            	// close input stream
                inputStream.close();
            }
        }
    }

    /************************************************************************
     * <b>Description:</b><br>
     *  Delete error csv by full path.
     *
     * @author		Tran.Thanh
     * @date		Jul 14, 2014		void
     ************************************************************************/
    @Action("/deleteErrorCSV")
    public void deleteErrorCSV() {
        //PrintWriter out = null;
        try {
            //Delete file by fileName
            deleteFile();
        } catch (IOException e) {
        	this.getLogger().error("[MatterDetailAction] An error when delete error csv file on Server ");
            this.getLogger().error(e.toString());
		}
    }

    /************************************************************************
     * <b>Description:</b><br>
     *  delete file error (loginId_error.csv).
     *
     * @author		Tran.Thanh
     * @date		Jul 14, 2014
     * @throws 		IOException		void
     ************************************************************************/
    private void deleteFile() throws IOException {
		//Get login Id and set to fileName
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String loginId = getLoginId(authentication);
        String fileName = loginId + Constant.ERROR + Constant.CSV_FILE_EXTENSION;

        //set full path at server
        String fullFilePath = Utils.getRealPath(this.getRequest()) + Constant.FOLDER_TMP_UPLOAD + "/";

        //Create new file and set to output stream
        File fileToDelete = new File(fullFilePath, fileName);
		if (fileToDelete.exists()) {
			FileUtils.forceDelete(fileToDelete);
		}
    }

    /**
     * createHeader.
     * @param headerString String
     * @param sbHeader StringBuilder
     * @param sbData StringBuilder
     * @param attributeNameList List<EntMstAttribute>
     * @param data EntCSVMode1
     */
    private void createHeader(String headerString, StringBuilder sbHeader, StringBuilder sbData, EntCSVMode1 data, List<EntMstAttribute> attributeNameList) {
        if (headerString.contains("updateMode")) {
            //BOE #7841 Nguyen.Chuong 2014/05/13 add default mode when export CSV
            sbHeader.append("," + EnumImportCSVMode1Header.getHeaderJPByHeaderEN("importMode"));
            sbData.append(Constant.DEFAULT_MODE_EXPORT_CSV  + ",");
            //EOE #7841 Nguyen.Chuong 2014/05/13 add default mode when export CSV
        }
        sbHeader.append("," + EnumImportCSVMode1Header.getHeaderJPByHeaderEN("productId"));
        sbData.append("," + "P" + data.getProductId());
        sbHeader.append("," + EnumImportCSVMode1Header.getHeaderJPByHeaderEN("productSyouhinSysCode"));
        sbData.append("," + data.getProductSyouhinSysCodeNotNull());
        if (headerString.contains("brandCode")) {
            sbHeader.append("," + EnumImportCSVMode1Header.getHeaderJPByHeaderEN("productBrandCode"));
            sbData.append("," + data.getProductBrandCodeNotNull());
        }
        if (headerString.contains("brandName")) {
            sbHeader.append("," + EnumImportCSVMode1Header.getHeaderJPByHeaderEN("productBrandName"));
            sbData.append("," + data.getProductBrandName());
        }
        if (headerString.contains("itemCode2")) {
            sbHeader.append("," + EnumImportCSVMode1Header.getHeaderJPByHeaderEN("productCategoryCode"));
            sbData.append("," + data.getProductCategoryCodeNotNull());
        }
        if (headerString.contains("itemName")) {
            sbHeader.append("," + EnumImportCSVMode1Header.getHeaderJPByHeaderEN("productCategoryName"));
            sbData.append("," + data.getProductCategoryName());
        }
        if (headerString.contains("productName")) {
            sbHeader.append("," + EnumImportCSVMode1Header.getHeaderJPByHeaderEN("productName"));
            sbData.append("," + data.getProductName());
        }
        if (headerString.contains("makerStockNumber")) {
            sbHeader.append("," + EnumImportCSVMode1Header.getHeaderJPByHeaderEN("productCode"));
            sbData.append("," + data.getProductCode());
        }
        if (headerString.contains("notStockNumber")) {
            sbHeader.append("," + EnumImportCSVMode1Header.getHeaderJPByHeaderEN("productWebikeCodeFlg"));
            sbData.append("," + data.getProductWebikeCodeFlgNotNull());
        }
        if (headerString.contains("jan")) {
            sbHeader.append("," + EnumImportCSVMode1Header.getHeaderJPByHeaderEN("productEanCode"));
            sbData.append("," + data.getProductEanCode());
        }
        if (headerString.contains("listPrice")) {
            sbHeader.append("," + EnumImportCSVMode1Header.getHeaderJPByHeaderEN("productProperPrice"));
            sbData.append("," + data.getProductProperPriceNotNull());
        }
        if (headerString.contains("division")) {
            sbHeader.append("," + EnumImportCSVMode1Header.getHeaderJPByHeaderEN("supplierPricePrice"));
            sbData.append("," + data.getSupplierPricePriceNotNull());
        }
        if (headerString.contains("groupCode")) {
            sbHeader.append("," + EnumImportCSVMode1Header.getHeaderJPByHeaderEN("productGroupCode"));
            sbData.append("," + data.getProductGroupCodeNotNull());
        }
        if (headerString.contains("dateOfIssue")) {
            sbHeader.append("," + EnumImportCSVMode1Header.getHeaderJPByHeaderEN("productSupplierReleaseDate"));
            sbData.append("," + data.getProductSupplierReleaseDate());
        }
        if (headerString.contains("openPrice")) {
            sbHeader.append("," + EnumImportCSVMode1Header.getHeaderJPByHeaderEN("productOpenPriceFlg"));
            sbData.append("," + data.getProductOpenPriceFlgNotNull());
        }
        if (headerString.contains("salePrice")) {
            sbHeader.append("," + EnumImportCSVMode1Header.getHeaderJPByHeaderEN("productProperSellingFlg"));
            sbData.append("," + data.getProductProperSellingFlgNotNull());
        }
        if (headerString.contains("buildToOrderManufacturing")) {
            sbHeader.append("," + EnumImportCSVMode1Header.getHeaderJPByHeaderEN("productOrderProductFlg"));
            sbData.append("," + data.getProductOrderProductFlgNotNull());
        }
        if (headerString.contains("nonReturnable")) {
            sbHeader.append("," + EnumImportCSVMode1Header.getHeaderJPByHeaderEN("productNoReturnableFlg"));
            sbData.append("," + data.getProductNoReturnableFlgNotNull());
        }
        if (headerString.contains("preferenceImage")) {
            sbHeader.append("," + EnumImportCSVMode1Header.getHeaderJPByHeaderEN("productAmbiguousImageFlg"));
            sbData.append("," + data.getProductAmbiguousImageFlgNotNull());
        }
        //BOE Nguyen.Chuong 2014/04/03: format to duplicate quote symbol (") to fix CSV export error when data contain quote symbol (")
//        if (headerString.contains("summary")) {
//            sbHeader.append("," + getText("summary")); sbData.append(",\"" + data.getDescriptionSummary() + "\"");
//        }
//        if (headerString.contains("note")) {
//            sbHeader.append("," + getText("note")); sbData.append(",\"" + data.getDescriptionRemarks() + "\"");
//        }
//        if (headerString.contains("caution")) {
//            sbHeader.append("," + getText("caution")); sbData.append(",\"" + data.getDescriptionCaution() + "\"");
//        }
//        if (headerString.contains("explanation")) {
//            sbHeader.append("," + getText("explanation")); sbData.append(",\"" + data.getDescriptionSentence() + "\"");
//        }
        if (headerString.contains("summary")) {
            sbHeader.append("," + EnumImportCSVMode1Header.getHeaderJPByHeaderEN("descriptionSummary"));
          //BOE #9607 Phan. Tinh 2014/07/01 delete ["] when export csv
            //sbData.append(",\"" + Utils.dobuleQuoteChar(data.getDescriptionSummary()) + "\"");
            //BOE @rcv!Luong.Dai 2014/07/09 #9811: Remove double quote char when write csv  
            /*sbData.append("," + Utils.dobuleQuoteChar(data.getDescriptionSummary()));*/
            sbData.append("," + data.getDescriptionSummary());
            //EOE @rcv!Luong.Dai 2014/07/09 #9811: Remove double quote char when write csv
          //EOE #9607 Phan. Tinh 2014/07/01
        }
        if (headerString.contains("note")) {
            sbHeader.append("," + EnumImportCSVMode1Header.getHeaderJPByHeaderEN("descriptionRemarks"));
          //BOE #9607 Phan. Tinh 2014/07/01 delete ["] when export csv
            //sbData.append(",\"" + Utils.dobuleQuoteChar(data.getDescriptionRemarks()) + "\"");
            //BOE @rcv!Luong.Dai 2014/07/09 #9811: Remove double quote char when write csv  
            /*sbData.append("," + Utils.dobuleQuoteChar(data.getDescriptionRemarks()));*/
            sbData.append("," + data.getDescriptionRemarks());
            //EOE @rcv!Luong.Dai 2014/07/09 #9811: Remove double quote char when write csv
          //EOE #9607 Phan. Tinh 2014/07/01
        }
        if (headerString.contains("caution")) {
            sbHeader.append("," + EnumImportCSVMode1Header.getHeaderJPByHeaderEN("descriptionCaution"));
          //BOE #9607 Phan. Tinh 2014/07/01 delete ["] when export csv
           //sbData.append(",\"" + Utils.dobuleQuoteChar(data.getDescriptionCaution()) + "\"");
            //BOE @rcv!Luong.Dai 2014/07/09 #9811: Remove double quote char when write csv  
            /*sbData.append("," + Utils.dobuleQuoteChar(data.getDescriptionCaution()));*/
            sbData.append("," + data.getDescriptionCaution());
            //EOE @rcv!Luong.Dai 2014/07/09 #9811: Remove double quote char when write csv
          //EOE #9607 Phan. Tinh 2014/07/01
        }
        if (headerString.contains("explanation")) {
            sbHeader.append("," + EnumImportCSVMode1Header.getHeaderJPByHeaderEN("descriptionSentence"));
          //BOE #9607 Phan. Tinh 2014/07/01 delete ["] when export csv
            //sbData.append(",\"" + Utils.dobuleQuoteChar(data.getDescriptionSentence()) + "\"");
            //BOE @rcv!Luong.Dai 2014/07/09 #9811: Remove double quote char when write csv  
            /*sbData.append("," + Utils.dobuleQuoteChar(data.getDescriptionSentence()));*/
            sbData.append("," + data.getDescriptionSentence());
            //EOE @rcv!Luong.Dai 2014/07/09 #9811: Remove double quote char when write csv
          //EOE #9607 Phan. Tinh 2014/07/01
        }
        //EOE Nguyen.Chuong 2014/04/03
        if (headerString.contains("supplierCode1")) {
            sbHeader.append("," + EnumImportCSVMode1Header.getHeaderJPByHeaderEN("siireCode1"));
            sbData.append("," + data.getSiireCode1());
        }
        if (headerString.contains("supplierName1")) {
            sbHeader.append("," + EnumImportCSVMode1Header.getHeaderJPByHeaderEN("siireName1"));
            sbData.append("," + data.getSiireName1());
        }
        if (headerString.contains("supplierCode1")) {
            sbHeader.append("," + EnumImportCSVMode1Header.getHeaderJPByHeaderEN("noukiCode1"));
            sbData.append("," + data.getNoukiCode1());
        }
        if (headerString.contains("supplierName1")) {
            sbHeader.append("," + EnumImportCSVMode1Header.getHeaderJPByHeaderEN("noukiName1"));
            sbData.append("," + data.getNoukiName1());
        }
        if (headerString.contains("supplierCode2")) {
            sbHeader.append("," + EnumImportCSVMode1Header.getHeaderJPByHeaderEN("siireCode2"));
            sbData.append("," + data.getSiireCode2());
        }
        if (headerString.contains("supplierName2")) {
            sbHeader.append("," + EnumImportCSVMode1Header.getHeaderJPByHeaderEN("siireName2"));
            sbData.append("," + data.getSiireName2());
        }
        if (headerString.contains("supplierCode2")) {
            sbHeader.append("," + EnumImportCSVMode1Header.getHeaderJPByHeaderEN("noukiCode2"));
            sbData.append("," + data.getNoukiCode2());
        }
        if (headerString.contains("supplierName2")) {
            sbHeader.append("," + EnumImportCSVMode1Header.getHeaderJPByHeaderEN("noukiName2"));
            sbData.append("," + data.getNoukiName2());
        }
        if (headerString.contains("supportedBike")) {
            sbHeader.append("," + EnumImportCSVMode1Header.getHeaderJPByHeaderEN("compatibleModel"));
            sbData.append("," + data.getcompatibleModelNotNull());
        }
        // image
        for (int i = 1; i <= Constant.PRODUCT_MAX_IMAGE_LENGTH; i++) {
            // check to identify image1 vs image10
            if (i == 1 && StringUtils.countMatches(headerString, "image1") == 1 && StringUtils.countMatches(headerString, "image10") == 1) {
                continue;
            }
            if (headerString.contains("image" + i)) {
                sbHeader.append("," + EnumImportCSVMode1Header.getHeaderJPByHeaderEN("productImageThumbnailPath" + i));
                sbHeader.append("," + EnumImportCSVMode1Header.getHeaderJPByHeaderEN("productImageDetailPath" + i));
                try {
                    sbData.append("," + BeanUtils.getProperty(data, "productImageThumbnailPath" + i + "NotNull"));
                    sbData.append("," + BeanUtils.getProperty(data, "productImageDetailPath" + i + "NotNull"));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        // select option
        for (int i = 1; i <= Constant.PRODUCT_MAX_OPTION_LENGTH; i++) {
            if (i == 1 && StringUtils.countMatches(headerString, "option1") == 1 && StringUtils.countMatches(headerString, "option10") == 1) {
                continue;
            }
            if (headerString.contains("option" + i)) {
                sbHeader.append("," + EnumImportCSVMode1Header.getHeaderJPByHeaderEN("selectCode" + i));
                sbHeader.append("," + EnumImportCSVMode1Header.getHeaderJPByHeaderEN("selectName" + i));
                try {
                    sbData.append("," + BeanUtils.getProperty(data, "selectCode" + i + "Str"));
                    sbData.append("," + BeanUtils.getProperty(data, "selectName" + i + "NotNull"));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        if (headerString.contains("customerConfirmationItem")) {
            sbHeader.append("," + EnumImportCSVMode1Header.getHeaderJPByHeaderEN("customersConfirmationItem"));
            sbData.append("," + data.getCustomersConfirmationItemNotNull());
        }
        if (headerString.contains("link")) {
            sbHeader.append("," + EnumImportCSVMode1Header.getHeaderJPByHeaderEN("link"));
            sbData.append("," + data.getLinkNotNull());
        }
        if (headerString.contains("video")) {
            sbHeader.append("," + EnumImportCSVMode1Header.getHeaderJPByHeaderEN("animation"));
            sbData.append("," + data.getAnimationNotNull());
        }
        // attribute
        for (int i = 0; i < attributeNameList.size(); i++) {
            sbHeader.append("," + attributeNameList.get(i).getAttributeName() + "_" + EnumImportCSVMode1Header.getHeaderJPByHeaderEN("attributeValue"));
            sbHeader.append("," + attributeNameList.get(i).getAttributeName() + "_" + EnumImportCSVMode1Header.getHeaderJPByHeaderEN("attributeDisplay"));
            try {
                sbData.append("," + BeanUtils.getProperty(data, "attributevalues" + (i + 1) + "NotNull"));
                sbData.append("," + BeanUtils.getProperty(data, "attributeDisplay" + (i + 1) + "NotNull"));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Create import conditions.
     * @since   2014/03/03
     * @author Hoang.Ho
     * @param size number of records
     * @param realFileName real file name of user
     * @param csvFileName csv file name.
     * @return EntTblFactoryImportCondition
     */
    private EntTblFactoryImportCondition createImportCondition(int size, String realFileName, String csvFileName) {

        // Get login Id
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String loginId = getLoginId(authentication);
        EntTblFactoryImportCondition importCondition = new EntTblFactoryImportCondition();
        EntMstFactoryMatterNew ent = this.matterDetailActionForm.getEntMstFactoryMatterNew();
        if (ent == null) {
            return importCondition;
        }
        importCondition.setImportConditionImportType("Product");
        importCondition.setImportConditionMatterNo(ent.getMatterNo());
        importCondition.setImportConditionFileName(realFileName);
        importCondition.setImportConditionOriginalName(csvFileName);
        importCondition.setImportConditionMaxCount(size);
        importCondition.setImportConditionExeCount(0);
        importCondition.setImportConditionStatus(0);
        importCondition.setUpdatedUserId(loginId);
        importCondition.setCreatedUserId(loginId);
        return importCondition;
    }
//    /************************************************************************
//     * <b>Description:</b><br>
//     *  loop to add each attribute from list attribute to ent product.
//     *
//     * @author		Nguyen.Chuong
//     * @date		Feb 26, 2014
//     * @param       listAttributes
//     * @return		entProductCSV with attribute
//     ************************************************************************/
//    public EntCSVMode3 addAttributeToEntProduct(List<EntMstAttribute> listAttributes) {
//        EntCSVMode3 entProductCSV = new EntCSVMode3();
//        EntMstAttribute entMstAttribute = new EntMstAttribute();
//        for (int i = 1; i <= listAttributes.size(); i++) {
//            entMstAttribute = listAttributes.get(i);
//            switch (i) {
//            case 1:
//                entProductCSV.setAttributeCode1(Integer.getInteger(entMstAttribute.getAttributeCode()));
//                entProductCSV.setAttributeValues1(entMstAttribute.getAttributeValues());
//                entProductCSV.setAttributeDisplay1(entMstAttribute.getAttributeDisplay());
//                break;
//            case 2:
//                entProductCSV.setAttributeCode2(Integer.getInteger(entMstAttribute.getAttributeCode()));
//                entProductCSV.setAttributeValues2(entMstAttribute.getAttributeValues());
//                entProductCSV.setAttributeDisplay2(entMstAttribute.getAttributeDisplay());
//                break;
//            case 3:
//                entProductCSV.setAttributeCode3(Integer.getInteger(entMstAttribute.getAttributeCode()));
//                entProductCSV.setAttributeValues3(entMstAttribute.getAttributeValues());
//                entProductCSV.setAttributeDisplay3(entMstAttribute.getAttributeDisplay());
//                break;
//            case 4:
//                entProductCSV.setAttributeCode4(Integer.getInteger(entMstAttribute.getAttributeCode()));
//                entProductCSV.setAttributeValues4(entMstAttribute.getAttributeValues());
//                entProductCSV.setAttributeDisplay4(entMstAttribute.getAttributeDisplay());
//                break;
//            case 5:
//                entProductCSV.setAttributeCode5(Integer.getInteger(entMstAttribute.getAttributeCode()));
//                entProductCSV.setAttributeValues5(entMstAttribute.getAttributeValues());
//                entProductCSV.setAttributeDisplay5(entMstAttribute.getAttributeDisplay());
//                break;
//            case 6:
//                entProductCSV.setAttributeCode6(Integer.getInteger(entMstAttribute.getAttributeCode()));
//                entProductCSV.setAttributeValues6(entMstAttribute.getAttributeValues());
//                entProductCSV.setAttributeDisplay6(entMstAttribute.getAttributeDisplay());
//                break;
//            case 7:
//                entProductCSV.setAttributeCode7(Integer.getInteger(entMstAttribute.getAttributeCode()));
//                entProductCSV.setAttributeValues7(entMstAttribute.getAttributeValues());
//                entProductCSV.setAttributeDisplay7(entMstAttribute.getAttributeDisplay());
//                break;
//            case 8:
//                entProductCSV.setAttributeCode8(Integer.getInteger(entMstAttribute.getAttributeCode()));
//                entProductCSV.setAttributeValues8(entMstAttribute.getAttributeValues());
//                entProductCSV.setAttributeDisplay8(entMstAttribute.getAttributeDisplay());
//                break;
//            case 9:
//                entProductCSV.setAttributeCode9(Integer.getInteger(entMstAttribute.getAttributeCode()));
//                entProductCSV.setAttributeValues9(entMstAttribute.getAttributeValues());
//                entProductCSV.setAttributeDisplay9(entMstAttribute.getAttributeDisplay());
//                break;
//            case 10:
//                entProductCSV.setAttributeCode10(Integer.getInteger(entMstAttribute.getAttributeCode()));
//                entProductCSV.setAttributeValues10(entMstAttribute.getAttributeValues());
//                entProductCSV.setAttributeDisplay10(entMstAttribute.getAttributeDisplay());
//                break;
//            case 11:
//                entProductCSV.setAttributeCode11(Integer.getInteger(entMstAttribute.getAttributeCode()));
//                entProductCSV.setAttributeValues11(entMstAttribute.getAttributeValues());
//                entProductCSV.setAttributeDisplay11(entMstAttribute.getAttributeDisplay());
//                break;
//            case 12:
//                entProductCSV.setAttributeCode12(Integer.getInteger(entMstAttribute.getAttributeCode()));
//                entProductCSV.setAttributeValues12(entMstAttribute.getAttributeValues());
//                entProductCSV.setAttributeDisplay12(entMstAttribute.getAttributeDisplay());
//                break;
//            case 13:
//                entProductCSV.setAttributeCode13(Integer.getInteger(entMstAttribute.getAttributeCode()));
//                entProductCSV.setAttributeValues13(entMstAttribute.getAttributeValues());
//                entProductCSV.setAttributeDisplay13(entMstAttribute.getAttributeDisplay());
//                break;
//            case 14:
//                entProductCSV.setAttributeCode14(Integer.getInteger(entMstAttribute.getAttributeCode()));
//                entProductCSV.setAttributeValues14(entMstAttribute.getAttributeValues());
//                entProductCSV.setAttributeDisplay14(entMstAttribute.getAttributeDisplay());
//                break;
//            case 15:
//                entProductCSV.setAttributeCode15(Integer.getInteger(entMstAttribute.getAttributeCode()));
//                entProductCSV.setAttributeValues15(entMstAttribute.getAttributeValues());
//                entProductCSV.setAttributeDisplay15(entMstAttribute.getAttributeDisplay());
//                break;
//            case 16:
//                entProductCSV.setAttributeCode16(Integer.getInteger(entMstAttribute.getAttributeCode()));
//                entProductCSV.setAttributeValues16(entMstAttribute.getAttributeValues());
//                entProductCSV.setAttributeDisplay16(entMstAttribute.getAttributeDisplay());
//                break;
//            case 17:
//                entProductCSV.setAttributeCode17(Integer.getInteger(entMstAttribute.getAttributeCode()));
//                entProductCSV.setAttributeValues17(entMstAttribute.getAttributeValues());
//                entProductCSV.setAttributeDisplay17(entMstAttribute.getAttributeDisplay());
//                break;
//            case 18:
//                entProductCSV.setAttributeCode18(Integer.getInteger(entMstAttribute.getAttributeCode()));
//                entProductCSV.setAttributeValues18(entMstAttribute.getAttributeValues());
//                entProductCSV.setAttributeDisplay18(entMstAttribute.getAttributeDisplay());
//                break;
//            case 19:
//                entProductCSV.setAttributeCode19(Integer.getInteger(entMstAttribute.getAttributeCode()));
//                entProductCSV.setAttributeValues19(entMstAttribute.getAttributeValues());
//                entProductCSV.setAttributeDisplay19(entMstAttribute.getAttributeDisplay());
//                break;
//            case 20:
//                entProductCSV.setAttributeCode20(Integer.getInteger(entMstAttribute.getAttributeCode()));
//                entProductCSV.setAttributeValues20(entMstAttribute.getAttributeValues());
//                entProductCSV.setAttributeDisplay20(entMstAttribute.getAttributeDisplay());
//                break;
//            case 21:
//                entProductCSV.setAttributeCode21(Integer.getInteger(entMstAttribute.getAttributeCode()));
//                entProductCSV.setAttributeValues21(entMstAttribute.getAttributeValues());
//                entProductCSV.setAttributeDisplay21(entMstAttribute.getAttributeDisplay());
//                break;
//            case 22:
//                entProductCSV.setAttributeCode22(Integer.getInteger(entMstAttribute.getAttributeCode()));
//                entProductCSV.setAttributeValues22(entMstAttribute.getAttributeValues());
//                entProductCSV.setAttributeDisplay22(entMstAttribute.getAttributeDisplay());
//                break;
//            case 23:
//                entProductCSV.setAttributeCode23(Integer.getInteger(entMstAttribute.getAttributeCode()));
//                entProductCSV.setAttributeValues23(entMstAttribute.getAttributeValues());
//                entProductCSV.setAttributeDisplay23(entMstAttribute.getAttributeDisplay());
//                break;
//            case 24:
//                entProductCSV.setAttributeCode24(Integer.getInteger(entMstAttribute.getAttributeCode()));
//                entProductCSV.setAttributeValues24(entMstAttribute.getAttributeValues());
//                entProductCSV.setAttributeDisplay24(entMstAttribute.getAttributeDisplay());
//                break;
//            case 25:
//                entProductCSV.setAttributeCode25(Integer.getInteger(entMstAttribute.getAttributeCode()));
//                entProductCSV.setAttributeValues25(entMstAttribute.getAttributeValues());
//                entProductCSV.setAttributeDisplay25(entMstAttribute.getAttributeDisplay());
//                break;
//            case 26:
//                entProductCSV.setAttributeCode26(Integer.getInteger(entMstAttribute.getAttributeCode()));
//                entProductCSV.setAttributeValues26(entMstAttribute.getAttributeValues());
//                entProductCSV.setAttributeDisplay26(entMstAttribute.getAttributeDisplay());
//                break;
//            case 27:
//                entProductCSV.setAttributeCode27(Integer.getInteger(entMstAttribute.getAttributeCode()));
//                entProductCSV.setAttributeValues27(entMstAttribute.getAttributeValues());
//                entProductCSV.setAttributeDisplay27(entMstAttribute.getAttributeDisplay());
//                break;
//            case 28:
//                entProductCSV.setAttributeCode28(Integer.getInteger(entMstAttribute.getAttributeCode()));
//                entProductCSV.setAttributeValues28(entMstAttribute.getAttributeValues());
//                entProductCSV.setAttributeDisplay28(entMstAttribute.getAttributeDisplay());
//                break;
//            case 29:
//                entProductCSV.setAttributeCode29(Integer.getInteger(entMstAttribute.getAttributeCode()));
//                entProductCSV.setAttributeValues29(entMstAttribute.getAttributeValues());
//                entProductCSV.setAttributeDisplay29(entMstAttribute.getAttributeDisplay());
//                break;
//            default:
//                entProductCSV.setAttributeCode30(Integer.getInteger(entMstAttribute.getAttributeCode()));
//                entProductCSV.setAttributeValues30(entMstAttribute.getAttributeValues());
//                entProductCSV.setAttributeDisplay30(entMstAttribute.getAttributeDisplay());
//                break;
//            }
//        }
//        return entProductCSV;
//    }

    /**
     * Validate CSV mode 1.
     * @author nguyen.hieu
     * @date 2014-03-10
     * @return Action name.
     */
    @Action(value = "/validateCsvMode1", results = { @Result(name = "list", type = "json") })
    public String validateCsvMode1() {
        try {
			// Validate loginUser
			if (!super.validateLoginUser()) {
				return "errorOther";
			}

			// Get user name login
			String loginId = this.getCommonForm().getLoginId();

			// Get matter no.
			String matterNo = this.getRequest().getParameter("matterNo");

			// Create param entity.
	    	EntTblFactoryImportCondition ent = new EntTblFactoryImportCondition();
	    	ent.setUpdatedUserId(loginId);
	    	ent.setImportConditionMatterNo(Long.parseLong(matterNo));

            // Start validation.
            csvImportValid = productService.validateCsvData(ent);
        } catch (Exception e) {
            this.getLogger().error(e.toString());
            return "errorOther";
        }
        return "list";
    }

    /**
     * @return the matterDetailActionForm
     */
    public MatterDetailActionForm getMatterDetailActionForm() {
        return matterDetailActionForm;
    }

    /**
     * @param matterDetailActionForm the matterDetailActionForm to set
     */
    public void setMatterDetailActionForm(MatterDetailActionForm matterDetailActionForm) {
        this.matterDetailActionForm = matterDetailActionForm;
    }

    /**
     * @return the page
     */
    public String getPage() {
        return page;
    }

    /**
     * @param page the page to set
     */
    public void setPage(String page) {
        this.page = page;
    }

    /**
     * @return the pageSize
     */
    public String getPageSize() {
        return pageSize;
    }

    /**
     * @param pageSize the pageSize to set
     */
    public void setPageSize(String pageSize) {
        this.pageSize = pageSize;
    }

	/**
	 * @return the statusDelete
	 */
	public String getStatusDelete() {
		return statusDelete;
	}

	/**
	 * @param statusDelete the statusDelete to set
	 */
	public void setStatusDelete(String statusDelete) {
		this.statusDelete = statusDelete;
	}

    /**
     * @return the statusUpdate
     */
    public String getStatusUpdate() {
        return statusUpdate;
    }

    /**
     * @param statusUpdate the statusUpdate to set
     */
    public void setStatusUpdate(String statusUpdate) {
        this.statusUpdate = statusUpdate;
    }

    /**
     * @return the userUpdate
     */
    public String getUserUpdate() {
        return userUpdate;
    }

    /**
     * @param userUpdate the userUpdate to set
     */
    public void setUserUpdate(String userUpdate) {
        this.userUpdate = userUpdate;
    }

    /**
     * @return the updatedOn
     */
    public Date getUpdatedOn() {
    	if (this.updatedOn != null) {
            return (Date) this.updatedOn.clone();
        }
        return null;
    }

    /**
     * @param updatedOn the updatedOn to set
     */
    public void setUpdatedOn(Date updatedOn) {
        if (updatedOn != null) {
            this.updatedOn = (Date) updatedOn.clone();
        } else {
            this.updatedOn = null;
        }
    }

    /**
     * @return the mapResult
     */
    public Map<Object, Object> getMapResult() {
        return mapResult;
    }

    /**
     * @param mapResult the mapResult to set
     */
    public void setMapResult(Map<Object, Object> mapResult) {
        this.mapResult = mapResult;
    }

    /* (non-Javadoc)
     * @see org.apache.struts2.interceptor.SessionAware#setSession(java.util.Map)
     */
    @Override
    public void setSession(Map<String, Object> session) {
        this.session = session;
    }

	/**
	 * @return the urlIssueRedmine
	 */
	public String getUrlIssueRedmine() {
		return urlIssueRedmine;
	}

	/**
	 * @param urlIssueRedmine the urlIssueRedmine to set
	 */
	public void setUrlIssueRedmine(String urlIssueRedmine) {
		this.urlIssueRedmine = urlIssueRedmine;
	}
    /**
     * @return the fileCSV
     */
    public File getFileCSV() {
        return fileCSV;
    }
    /**
     * @param fileCSV the fileCSV to set
     */
    public void setFileCSV(File fileCSV) {
        this.fileCSV = fileCSV;
    }
    /**
     * @return the fileCSVContentType
     */
    public String getFileCSVContentType() {
        return fileCSVContentType;
    }
    /**
     * @param fileCSVContentType the fileCSVContentType to set
     */
    public void setFileCSVContentType(String fileCSVContentType) {
        this.fileCSVContentType = fileCSVContentType;
    }
    /**
     * @return the fileCSVFileName
     */
    public String getFileCSVFileName() {
        return fileCSVFileName;
    }
    /**
     * @param fileCSVFileName the fileCSVFileName to set
     */
    public void setFileCSVFileName(String fileCSVFileName) {
        this.fileCSVFileName = fileCSVFileName;
    }
    /**
     * @return the importMode
     */
    public String getImportMode() {
        return importMode;
    }
    /**
     * @param importMode the importMode to set
     */
    public void setImportMode(String importMode) {
        this.importMode = importMode;
    }
    public String getHeaderList() {
        return headerList;
    }
    public void setHeaderList(String headerList) {
        this.headerList = headerList;
    }

    public void setCsvImportValid(boolean csvImportValid) {
		this.csvImportValid = csvImportValid;
	}

    public boolean isCsvImportValid() {
		return csvImportValid;
	}

    /**
     * @return the maxNumberProductExportCSV
     */
    public int getMaxNumberProductExportCSV() {
        return maxNumberProductExportCSV;
    }

    /**
     * @param maxNumberProductExportCSV the maxNumberProductExportCSV to set
     */
    public void setMaxNumberProductExportCSV(int maxNumberProductExportCSV) {
        this.maxNumberProductExportCSV = maxNumberProductExportCSV;
    }

	
	/************************************************************************
	 * <b>Description:</b><br>
	 *  Loop all row in CSV to check CSV content enter character or not enough "," char.
	 *
	 * @author		Nguyen.Chuong
	 * @date		2014/08/04
	 * @param 		fileConvertName		file to read
	 * @return		boolean				Convert result
	 ************************************************************************/
	public static boolean checkCSVFileValid(String fileConvertName) {
		int numberOfComma = -1;
		BufferedReader bufferReader = null;
        InputStream fileStream = null;
        Reader readerDecoder = null;
        // type of file
		try {
            //Current readed line
            String line = "";
            fileStream = new FileInputStream(fileConvertName);
            readerDecoder = new InputStreamReader(fileStream, Constant.UTF_8_ENCODE);
            bufferReader = new BufferedReader(readerDecoder);
            // read file line by line
            int numberOfCommanInLine = -1;
            while ((line = bufferReader.readLine()) != null) {
            	//Count the number of "," to check number of column. Add any String to ent of line for get corrent number "," in line.
            	numberOfCommanInLine = (line + "|||").split(",").length - 1;
//        		if (line.endsWith(",")) {
//        			numberOfCommanInLine++;
//        		}
        		//Check number of comma in each rows.
        		//First row is header then get number of comma in header.
            	if (numberOfComma < 0) {
            		numberOfComma = numberOfCommanInLine;
            	} else if (numberOfComma != numberOfCommanInLine) {
            		//If number of comma in next row diferent with first row is error.
                    //Close bufer
                    if (bufferReader != null) {
        				bufferReader.close();
        			}
                    if (readerDecoder != null) {
                    	readerDecoder.close();
                    }
                    if (fileStream != null) {
                    	fileStream.close();
                    }
        			return false;
            	}
            }
            //Close bufer
            if (bufferReader != null) {
				bufferReader.close();
			}
            if (readerDecoder != null) {
            	readerDecoder.close();
            }
            if (fileStream != null) {
            	fileStream.close();
            }
            return true;
		} catch (IOException ex) {
			return false;
		}
	}
	
	/************************************************************************
	 * <b>Description:</b><br>
	 *  Convert file from shift-jis to utf-8 and check the format of CSV invalid ",".
	 *
	 * @author		Nguyen.Chuong
	 * @date		Sep 4, 2014
	 * @param 		fileConvertName		File to convert
	 * @param		fileResultName		File result
	 * @return		boolean				Convert result
	 ************************************************************************/
	public static boolean convertFileFromShiftJisToUTF8(String fileConvertName, String fileResultName) {
		int numberOfComma = -1;
		boolean result = false;
		BufferedReader bufferReader = null;
        InputStream fileStream = null;
        Reader readerDecoder = null;

        PrintWriter out = null;
        // type of file
		try {
			File fileResult = new File(fileResultName);
			//Set encode of file result if UTF-8
            out = new PrintWriter(fileResult, Constant.UTF_8_ENCODE);
            //Current readed line
            String line = "";
            String lineUtf8 = "";

            fileStream = new FileInputStream(fileConvertName);
            /* BOE @rcv! Luong.Dai 2014/09/06: Change encode from sjis to ms932 */
            /*readerDecoder = new InputStreamReader(fileStream, Constant.SHIFT_JIS_ENCODE);*/
            readerDecoder = new InputStreamReader(fileStream, Constant.MS932_ENCODE);
            /* EOE @rcv! Luong.Dai 2014/09/06: Change encode from sjis to ms932 */
            bufferReader = new BufferedReader(readerDecoder);
            // read file line by line
            int numberOfCommanInLine = -1;
            while ((line = bufferReader.readLine()) != null) {
            	//Count the number of "," to check number of column. Add any String to ent of line for get corrent number "," in line.
            	numberOfCommanInLine = (line + "|||").split(",").length - 1;
//        		if (line.endsWith(",")) {
//        			numberOfCommanInLine++;
//        		}
        		//Check number of comma in each rows.
        		//First row is header then get number of comma in header.
            	if (numberOfComma < 0) {
            		numberOfComma = numberOfCommanInLine;
            	} else if (numberOfComma != numberOfCommanInLine) {
            		//If number of comma in next row diferent with first row is error.
                    //Close bufer
                    if (bufferReader != null) {
        				bufferReader.close();
        			}
                    if (readerDecoder != null) {
                    	readerDecoder.close();
                    }
                    if (fileStream != null) {
                    	fileStream.close();
                    }
                    if (out != null) {
                         out.close();
                    }
        			return false;
            	}
            	//Convert to UTF-8
            	lineUtf8 = EncodingUtils.sjisToUtf8(line);
            	//Print line with encode UTF-8
             	out.print(lineUtf8);
             	out.print("\n");
            }
            //Close bufer
            if (bufferReader != null) {
				bufferReader.close();
			}
            if (readerDecoder != null) {
            	readerDecoder.close();
            }
            if (fileStream != null) {
            	fileStream.close();
            }
            if (out != null) {
                 out.flush();
                 out.close();
            }
            result = true;
		} catch (IOException ex) {
			result = false;
		}
		return result;
	}
}
