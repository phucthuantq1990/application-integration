/************************************************************************
 * File Name	： BrandMasterManagerActionForm.java
 * Author		： Nguyen.Chuong
 * Version		： 1.0.0
 * Date Created	： 2013/12/27
 * Date Updated	： 2013/12/27
 * Description	： contain data Brand Master Manager page.
 *
 ***********************************************************************/
package net.webike.RcProductFactory.action.form;

import java.util.List;

import net.webike.RcProductFactory.entity.EntMstBrand;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * BrandMasterManagerActionForm.
 */
@Component
@Scope("session")
public class BrandMasterManagerActionForm {
    //List of brand.
    private List<EntMstBrand> listBrand;
    //Ent to filter.
    private EntMstBrand entMstBrand;
    /**
     * @return the listBrand
     */
    public List<EntMstBrand> getListBrand() {
        return listBrand;
    }

    /**
     * @param listBrand the listBrand to set
     */
    public void setListBrand(List<EntMstBrand> listBrand) {
        this.listBrand = listBrand;
    }

    /**
     * @return the entMstBrand
     */
    public EntMstBrand getEntMstBrand() {
        return entMstBrand;
    }

    /**
     * @param entMstBrand the entMstBrand to set
     */
    public void setEntMstBrand(EntMstBrand entMstBrand) {
        this.entMstBrand = entMstBrand;
    }
}
