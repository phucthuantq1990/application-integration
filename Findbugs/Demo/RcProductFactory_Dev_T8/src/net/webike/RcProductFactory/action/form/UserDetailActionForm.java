/************************************************************************
 * File Name	： UserDetailActionForm.java
 * Author		： Tran.Thanh
 * Version		： 1.0.0
 * Date Created	： 2013/10/03
 * Date Updated	： 2013/10/03
 * Description	： contain data UserDetail page.
 *
 ***********************************************************************/
package net.webike.RcProductFactory.action.form;

import java.util.TreeMap;
import net.webike.RcProductFactory.entity.EntMstFactoryUserTmp;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * UserManageActionForm.
 */
@Component
@Scope("session")
public class UserDetailActionForm {

    private EntMstFactoryUserTmp entUserTmp;

    /**---------------------------------------------------------------------------.
     * @description hashmap contains all values of authority.
     */
    private TreeMap<String, String> authorityTreeMap;

    /**---------------------------------------------------------------------------.
     * @description hashmap contains all values of status.
     */
    private TreeMap<String, String> statusTreeMap;

	/**
     * called actionName.
     */
	private String actionName;

	/**
	 * action name for submit form in JSP.
	 */
	private String submitActionName;

	/**
	 * value for tag H1 in JSP.
	 */
	private String h1Title;

	/**
	 * Id of detail tag: if userDetail page: rightTab, if newUser page: centerTab.
	 */
	private String detailTabId;

	/**
	 * Status of insert action: insert success: true, insert fail: false.
	 */
	private Boolean insertStatus = true;

	/**
	 * Status of update action: update success: true, update fail: false.
	 */
	private Boolean updateStatus = false;

	/**
	 * Message of insert action.
	 */
	private String successMessage;

	/**
     * Message of insert action.
     */
    private String failMessage;

	/**
	 * @return the actionName
	 */
	public String getActionName() {
		return actionName;
	}

	/**
	 * @param actionName the actionName to set
	 */
	public void setActionName(String actionName) {
		this.actionName = actionName;
	}

    /**
     * @return the h1Title
     */
    public String getH1Title() {
        return h1Title;
    }

    /**
     * @param h1Title the h1Title to set
     */
    public void setH1Title(String h1Title) {
        this.h1Title = h1Title;
    }

    /**
     * @return the submitActionName
     */
    public String getSubmitActionName() {
        return submitActionName;
    }

    /**
     * @param submitActionName the submitActionName to set
     */
    public void setSubmitActionName(String submitActionName) {
        this.submitActionName = submitActionName;
    }

    /**
     * @return the detailTabId
     */
    public String getDetailTabId() {
        return detailTabId;
    }

    /**
     * @param detailTabId the detailTabId to set
     */
    public void setDetailTabId(String detailTabId) {
        this.detailTabId = detailTabId;
    }

    /**
     * @return the insertStatus
     */
    public Boolean getInsertStatus() {
        return insertStatus;
    }

    /**
     * @param insertStatus the insertStatus to set
     */
    public void setInsertStatus(Boolean insertStatus) {
        this.insertStatus = insertStatus;
    }

    /**
     * @return the successMessage
     */
    public String getSuccessMessage() {
        return successMessage;
    }

    /**
     * @param successMessage the successMessage to set
     */
    public void setSuccessMessage(String successMessage) {
        this.successMessage = successMessage;
    }

    /**
     * @return the failMessage
     */
    public String getFailMessage() {
        return failMessage;
    }

    /**
     * @param failMessage the failMessage to set
     */
    public void setFailMessage(String failMessage) {
        this.failMessage = failMessage;
    }

    /**
     * @return the authorityTreeMap
     */
    public TreeMap<String, String> getAuthorityTreeMap() {
        return authorityTreeMap;
    }

    /**
     * @param authorityTreeMap the authorityTreeMap to set
     */
    public void setAuthorityTreeMap(TreeMap<String, String> authorityTreeMap) {
        this.authorityTreeMap = authorityTreeMap;
    }

    /**
     * @return the statusTreeMap
     */
    public TreeMap<String, String> getStatusTreeMap() {
        return statusTreeMap;
    }

    /**
     * @param statusTreeMap the statusTreeMap to set
     */
    public void setStatusTreeMap(TreeMap<String, String> statusTreeMap) {
        this.statusTreeMap = statusTreeMap;
    }
    /**
     * @return the updateStatus
     */
    public Boolean getUpdateStatus() {
        return updateStatus;
    }

    /**
     * @param updateStatus the updateStatus to set
     */
    public void setUpdateStatus(Boolean updateStatus) {
        this.updateStatus = updateStatus;
    }

	/**
	 * @return the entUserTmp
	 */
	public EntMstFactoryUserTmp getEntUserTmp() {
		return entUserTmp;
	}

	/**
	 * @param entUserTmp the entUserTmp to set
	 */
	public void setEntUserTmp(EntMstFactoryUserTmp entUserTmp) {
		this.entUserTmp = entUserTmp;
	}
}
