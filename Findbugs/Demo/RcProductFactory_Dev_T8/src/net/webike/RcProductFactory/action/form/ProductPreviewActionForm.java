/************************************************************************
  * file name	： ProductManageActionForm.java
 * author		： Luong.Dai
 * version		： 1.0.0
 * date created	： 13 Jun 2014
 * date updated	： 13 Jun 2014
 * description	： form for product preview page
 *
 ***********************************************************************/
package net.webike.RcProductFactory.action.form;

import java.util.List;
import java.util.Map;

import net.webike.RcProductFactory.entity.EntMstAttribute;
import net.webike.RcProductFactory.entity.EntTblFactoryCalibrationRequestIndividual;
import net.webike.RcProductFactory.entity.EntTblFactoryCalibrationRequestWhole;
import net.webike.RcProductFactory.entity.EntTblFactoryProductDescription;
import net.webike.RcProductFactory.entity.EntTblFactoryProductGeneral;
import net.webike.RcProductFactory.entity.EntTblFactoryProductGuestInput;
import net.webike.RcProductFactory.entity.EntTblFactoryProductLink;
import net.webike.RcProductFactory.entity.EntTblFactoryProductNew;
import net.webike.RcProductFactory.entity.EntTblFactoryProductSupplier;
import net.webike.RcProductFactory.entity.EntTblFactoryProductVideo;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * form for product manage page.
 */
@Component
@Scope("request")
public class ProductPreviewActionForm {
	/* BOE Variable by Tran.Thanh */
	/* Start Block 2 */
	private int totalProductInMatter = 0;
	private int indexProductInMatter = 0;
	private int indexGroupInMatter = 0;
	private int totalGroupInMatter = 0;

	private EntTblFactoryProductGeneral entProductGeneral;
	/* End Block 2 */
	/* Start Block 4 */
	private String factoryProductImageDetailJsonStr;
	/* End Block 4 */
	/* Start Block 5 */
	private List<EntTblFactoryProductSupplier> listSupplier;
	/* End Block 5 */
	/* Start Block 10 */
	private EntTblFactoryProductDescription entDescription;
	/* End Block 10 */
	/* EOE Variable by Tran.Thanh */

	/* BOE Variable by Dai.Luong */
	private List<EntTblFactoryProductGuestInput> lstGuestInput;
	private List<EntTblFactoryProductLink> lstLink;
	private Map<String, Object> mapFitModel;
	private Map<Integer, Object> mapSelectDisplay;
	/* EOE Variable by Dai.Luong */

	/* BOE Variable by Nguyen.Chuong */
	private Long matterNo;
	private Long productId;
	private Long nextProductId;
	private Long previousProductId;
	//List video of product data.
	private List<EntTblFactoryProductVideo> listVideo;
	//List attribute of product get from DB.
	private List<EntMstAttribute> listAttribute;
	/* EOE Variable by Nguyen.Chuong */

	/* BOE Getter, Setter by Tran.Thanh */

	/* BOE Variable by @rcv!dau.phuong */
	private List<EntTblFactoryCalibrationRequestIndividual> listCalibrationRequest;
	private EntTblFactoryCalibrationRequestIndividual entTblFactoryCalibrationRequestIndividual;
	private List<EntTblFactoryCalibrationRequestIndividual> listCalibrationType;
	private EntTblFactoryProductNew entStatusPopUpCheck;
	/* EOE Variable by @rcv!dau.phuong */
	// BOE @rcv!Nguyen.Chuong Jul 17, 2014 #9811 : add variable for tab 1, 3 in popup note
	//Entity to content data of tab all and tab note
	private EntTblFactoryCalibrationRequestWhole entTblCalibrationRequestWhole;
	//tab number when call save action
	private int tabNo;
	// EOE @rcv!Nguyen.Chuong Jul 17, 2014 #9811 : add variable for tab 1, 3 in popup note
	private String lstCalibrationTypeJson;
	//Role of user
	private List<String> roleUser = null;
	/**
	 * @return the totalProductInMatter
	 */
	public int getTotalProductInMatter() {
		return totalProductInMatter;
	}

	/**
	 * @param totalProductInMatter the totalProductInMatter to set
	 */
	public void setTotalProductInMatter(int totalProductInMatter) {
		this.totalProductInMatter = totalProductInMatter;
	}

	/**
	 * @return the indexProductInMatter
	 */
	public int getIndexProductInMatter() {
		return indexProductInMatter;
	}

	/**
	 * @param indexProductInMatter the indexProductInMatter to set
	 */
	public void setIndexProductInMatter(int indexProductInMatter) {
		this.indexProductInMatter = indexProductInMatter;
	}

	/**
	 * @return the indexGroupInMatter
	 */
	public int getIndexGroupInMatter() {
		return indexGroupInMatter;
	}

	/**
	 * @param indexGroupInMatter the indexGroupInMatter to set
	 */
	public void setIndexGroupInMatter(int indexGroupInMatter) {
		this.indexGroupInMatter = indexGroupInMatter;
	}

	/**
	 * @return the totalGroupInMatter
	 */
	public int getTotalGroupInMatter() {
		return totalGroupInMatter;
	}

	/**
	 * @param totalGroupInMatter the totalGroupInMatter to set
	 */
	public void setTotalGroupInMatter(int totalGroupInMatter) {
		this.totalGroupInMatter = totalGroupInMatter;
	}

	/**
	 * @return the entProductGeneral
	 */
	public EntTblFactoryProductGeneral getEntProductGeneral() {
		return entProductGeneral;
	}

	/**
	 * @param entProductGeneral the entProductGeneral to set
	 */
	public void setEntProductGeneral(EntTblFactoryProductGeneral entProductGeneral) {
		this.entProductGeneral = entProductGeneral;
	}

	/**
	 * @return the listSupplier
	 */
	public List<EntTblFactoryProductSupplier> getListSupplier() {
		return listSupplier;
	}

	/**
	 * @param listSupplier the listSupplier to set
	 */
	public void setListSupplier(List<EntTblFactoryProductSupplier> listSupplier) {
		this.listSupplier = listSupplier;
	}

	/**
	 * @return the factoryProductImageDetailJsonStr
	 */
	public String getFactoryProductImageDetailJsonStr() {
		return factoryProductImageDetailJsonStr;
	}

	/**
	 * @param factoryProductImageDetailJsonStr the factoryProductImageDetailJsonStr to set
	 */
	public void setFactoryProductImageDetailJsonStr(
			String factoryProductImageDetailJsonStr) {
		this.factoryProductImageDetailJsonStr = factoryProductImageDetailJsonStr;
	}

	/**
	 * @return the entDescription
	 */
	public EntTblFactoryProductDescription getEntDescription() {
		return entDescription;
	}

	/**
	 * @param entDescription the entDescription to set
	 */
	public void setEntDescription(EntTblFactoryProductDescription entDescription) {
		this.entDescription = entDescription;
	}
	/* EOE Getter, Setter by Tran.Thanh */

	/* BOE Getter, Setter by Dai.Luong */
	/**
	 * @return the lstGuestInput
	 */
	public List<EntTblFactoryProductGuestInput> getLstGuestInput() {
		return lstGuestInput;
	}

	/**
	 * @param lstGuestInput the lstGuestInput to set
	 */
	public void setLstGuestInput(List<EntTblFactoryProductGuestInput> lstGuestInput) {
		this.lstGuestInput = lstGuestInput;
	}

	/**
	 * @return the lstLink
	 */
	public List<EntTblFactoryProductLink> getLstLink() {
		return lstLink;
	}

	/**
	 * @param lstLink the lstLink to set
	 */
	public void setLstLink(List<EntTblFactoryProductLink> lstLink) {
		this.lstLink = lstLink;
	}

	/**
	 * @return the mapFitModel
	 */
	public Map<String, Object> getMapFitModel() {
		return mapFitModel;
	}

	/**
	 * @param mapFitModel the mapFitModel to set
	 */
	public void setMapFitModel(Map<String, Object> mapFitModel) {
		this.mapFitModel = mapFitModel;
	}

	/**
	 * @return the mapSelectDisplay
	 */
	public Map<Integer, Object> getMapSelectDisplay() {
		return mapSelectDisplay;
	}

	/**
	 * @param mapSelectDisplay the mapSelectDisplay to set
	 */
	public void setMapSelectDisplay(Map<Integer, Object> mapSelectDisplay) {
		this.mapSelectDisplay = mapSelectDisplay;
	}

	/* EOE Getter, Setter by Dai.Luong */

	/* BOE Getter, Setter by Nguyen.Chuong */
    /**
     * @return the matterNo
     */
    public Long getMatterNo() {
        return matterNo;
    }

    /**
     * @param matterNo the matterNo to set
     */
    public void setMatterNo(Long matterNo) {
        this.matterNo = matterNo;
    }
    /**
     * @return the nextProductId
     */
    public Long getNextProductId() {
        return nextProductId;
    }

    /**
     * @param nextProductId the nextProductId to set
     */
    public void setNextProductId(Long nextProductId) {
        this.nextProductId = nextProductId;
    }

    /**
     * @return the previousProductId
     */
    public Long getPreviousProductId() {
        return previousProductId;
    }

    /**
     * @param previousProductId the previousProductId to set
     */
    public void setPreviousProductId(Long previousProductId) {
        this.previousProductId = previousProductId;
    }

    /**
     * @return the productId
     */
    public Long getProductId() {
        return productId;
    }

    /**
     * @param productId the productId to set
     */
    public void setProductId(Long productId) {
        this.productId = productId;
    }

    /**
     * @return the listVideo
     */
    public List<EntTblFactoryProductVideo> getListVideo() {
        return listVideo;
    }

    /**
     * @param listVideo the listVideo to set
     */
    public void setListVideo(List<EntTblFactoryProductVideo> listVideo) {
        this.listVideo = listVideo;
    }
	/* EOE Getter, Setter by Nguyen.Chuong */

    /**
     * @return the listAttribute
     */
    public List<EntMstAttribute> getListAttribute() {
        return listAttribute;
    }

    /**
     * @param listAttribute the listAttribute to set
     */
    public void setListAttribute(List<EntMstAttribute> listAttribute) {
        this.listAttribute = listAttribute;
    }

    /**
     * @return the listCalibrationRequest
     */
    public List<EntTblFactoryCalibrationRequestIndividual> getListCalibrationRequest() {
        return listCalibrationRequest;
    }

    /**
     * @param listCalibrationRequest the listCalibrationRequest to set
     */
    public void setListCalibrationRequest(List<EntTblFactoryCalibrationRequestIndividual> listCalibrationRequest) {
        this.listCalibrationRequest = listCalibrationRequest;
    }

    /**
     * @return the entTblFactoryCalibrationRequestIndividual
     */
    public EntTblFactoryCalibrationRequestIndividual getEntTblFactoryCalibrationRequestIndividual() {
        return entTblFactoryCalibrationRequestIndividual;
    }

    /**
     * @param entTblFactoryCalibrationRequestIndividual the entTblFactoryCalibrationRequestIndividual to set
     */
    public void setEntTblFactoryCalibrationRequestIndividual(EntTblFactoryCalibrationRequestIndividual entTblFactoryCalibrationRequestIndividual) {
        this.entTblFactoryCalibrationRequestIndividual = entTblFactoryCalibrationRequestIndividual;
    }

    /**
     * @return the listCalibrationType
     */
    public List<EntTblFactoryCalibrationRequestIndividual> getListCalibrationType() {
        return listCalibrationType;
    }

    /**
     * @param listCalibrationType the listCalibrationType to set
     */
    public void setListCalibrationType(List<EntTblFactoryCalibrationRequestIndividual> listCalibrationType) {
        this.listCalibrationType = listCalibrationType;
    }

    /**
     * @return the entStatusPopUpCheck
     */
    public EntTblFactoryProductNew getEntStatusPopUpCheck() {
        return entStatusPopUpCheck;
    }

    /**
     * @param entStatusPopUpCheck the entStatusPopUpCheck to set
     */
    public void setEntStatusPopUpCheck(EntTblFactoryProductNew entStatusPopUpCheck) {
        this.entStatusPopUpCheck = entStatusPopUpCheck;
    }

    /**
     * @return the entTblCalibrationRequestWhole
     */
    public EntTblFactoryCalibrationRequestWhole getEntTblCalibrationRequestWhole() {
        return entTblCalibrationRequestWhole;
    }

    /**
     * @param entTblCalibrationRequestWhole the entTblCalibrationRequestWhole to set
     */
    public void setEntTblCalibrationRequestWhole(
            EntTblFactoryCalibrationRequestWhole entTblCalibrationRequestWhole) {
        this.entTblCalibrationRequestWhole = entTblCalibrationRequestWhole;
    }

    /**
     * @return the tabNo
     */
    public int getTabNo() {
        return tabNo;
    }

    /**
     * @param tabNo the tabNo to set
     */
    public void setTabNo(int tabNo) {
        this.tabNo = tabNo;
    }

    /**
     * @return the lstCalibrationTypeJson
     */
    public String getLstCalibrationTypeJson() {
        return lstCalibrationTypeJson;
    }

    /**
     * @param lstCalibrationTypeJson the lstCalibrationTypeJson to set
     */
    public void setLstCalibrationTypeJson(String lstCalibrationTypeJson) {
        this.lstCalibrationTypeJson = lstCalibrationTypeJson;
    }

	/**
	 * @return the roleUser
	 */
	public List<String> getRoleUser() {
		return roleUser;
	}

	/**
	 * @param roleUser the roleUser to set
	 */
	public void setRoleUser(List<String> roleUser) {
		this.roleUser = roleUser;
	}

}
