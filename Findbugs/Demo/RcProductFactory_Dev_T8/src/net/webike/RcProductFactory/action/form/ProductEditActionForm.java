/************************************************************************
 * file name	： ProductManageActionForm.java
 * author		： Le.Dinh
 * version		： 1.0.0
 * date created	： 11 Feb 2014
 * date updated	： 11 Feb 2014
 * description	： form for product manage page
 *
 ***********************************************************************/
package net.webike.RcProductFactory.action.form;

import java.util.List;
import java.util.Map;

import net.webike.RcProductFactory.entity.EntMstAttribute;
import net.webike.RcProductFactory.entity.EntMstAttributeGroup;
import net.webike.RcProductFactory.entity.EntMstBrand;
import net.webike.RcProductFactory.entity.EntMstCategory;
import net.webike.RcProductFactory.entity.EntMstFactoryMatter;
import net.webike.RcProductFactory.entity.EntMstLinkReason;
import net.webike.RcProductFactory.entity.EntMstNouki;
import net.webike.RcProductFactory.entity.EntMstProductSupplierStatus;
import net.webike.RcProductFactory.entity.EntMstSiire;
/* BOE global process by  Thanh */
import net.webike.RcProductFactory.entity.EntMstSyouhinSearch;
import net.webike.RcProductFactory.entity.EntTblBrandConditionRule;
import net.webike.RcProductFactory.entity.EntTblFactoryProductDescription;
import net.webike.RcProductFactory.entity.EntTblFactoryProductFitModel;
import net.webike.RcProductFactory.entity.EntTblFactoryProductGeneral;
import net.webike.RcProductFactory.entity.EntTblFactoryProductImage;
import net.webike.RcProductFactory.entity.EntTblFactoryProductLink;
import net.webike.RcProductFactory.entity.EntTblFactoryProductNew;
import net.webike.RcProductFactory.entity.EntTblFactoryProductSelect;
import net.webike.RcProductFactory.entity.EntTblFactoryProductSupplier;
import net.webike.RcProductFactory.entity.EntTblFactoryProductVideo;
import net.webike.RcProductFactory.entity.ImageUploadKenDo;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
/* EOE global process by  Thanh */

/* BOE global process by  Dai */
/* EOE global process by  Dai */

/* BOE global process by  Hoang */
/* EOE global process by  Hoang */

/**
 * form for product manage page.
 */
@Component
@Scope("request")
public class ProductEditActionForm {

	/* BOE Block1 */
	/* BOE process by  Hoang */
	private EntTblFactoryProductNew product;
	private String msgString = "";
	private Long productMatterNo;
	private Integer flgToGetPriceBlock9;
	//Mode for page: Mode = E is Edit, Mode = N if New
	private String mode = "N";
	private Long productIdDupliCate;
    /* EOE process by  Hoang */

    /* BOE process by  Thanh */
	// return message code. 0: fail, 1: success
    private Integer msgCode;
	// jSon date has change Block 6
	private String jSonVideoDs = "";
	// jSon product
	private String productJson = "";
    /* EOE process by  Thanh */

    /* BOE process by  Dai */
    /* EOE process by  Dai */

    /* BOE process by  Hoang */
    /* EOE process by  Hoang */
	/* EOE Block1 */

	/* BOE Block2 */
	//List image of product
    private List<EntTblFactoryProductImage> listImage;
    private String factoryProductImageDetailJsonStr;
    private String urlProductFactoryPath = "";
    private Integer maxUploadImage;
    private Integer maxUploadFolderMatter;
	/* EOE Block2 */

	/* BOE Block3 */
    private List<EntMstBrand> brandList;
    private EntMstBrand brandInfo;
    private Map<String, String> mapProductConditionFlg;
	/* EOE Block3 */

	/* BOE Block4 */
    private Long productId;
    private List<EntMstAttribute> listAttribute;
    private List<EntMstAttribute> listAllAttribute;
    //List attributeGroup get by attributeCode
    private List<EntMstAttributeGroup> listAttrGroup;
	/* EOE Block4 */

	/* BOE Block4 */
	/* EOE Block4 */

	/* BOE Block5 */
    private List<EntTblFactoryProductLink> listLink;
    private String  factoryProductLinkJsonStr = "";

    private List<EntMstLinkReason> listMstLinkReason;

    private List<EntMstSyouhinSearch> listSyouhinSearch;
    private String  listSyouhinSearchJsonStr = "";
    // syouhinSearch - Entity for query.
    private EntMstSyouhinSearch syouhinSearch;
	/* EOE Block5 */

	/* BOE Block6 */
    private List<EntTblFactoryProductVideo> listFactoryProductVideo;
    private String  factoryProductVideoJsonStr = "";
	/* EOE Block6 */

	/* BOE Block7 */
    private EntTblFactoryProductDescription productDescription;
	/* EOE Block7 */

	/* BOE Block8 */
    private String lstProductGuestInputJson;
	/* EOE Block8 */

	/* BOE Block9 */
    private EntTblFactoryProductNew productPrice;
    private String defaultFlagString;
    private Integer marumeCode;
    private Integer brandMarumeCode;
    private List<EntTblFactoryProductNew> listProductSameGroup;
    private Long minProperPrice = 0L;
    private Long maxProperPrice = 0L;
    private Long groupCode;
    private String listSelectJson;
    private List<EntTblFactoryProductSelect> listProductSelect;
    private EntTblBrandConditionRule rule;
    private String listGridGroupCodeStr;
    private String listProductSelectStr;
	/* EOE Block9 */

	/* BOE Block10 */
    private Integer categoryCode;
    private List<EntMstCategory> listCategory;
	/* EOE Block10 */

	/* BOE Block11 */
    private List<EntTblFactoryProductFitModel> listFitModel;
    private String listFitModelJsonStr;
    private List<EntTblFactoryProductNew> listProductFitModel;
    private EntMstFactoryMatter factoryMatter;
	/* EOE Block11 */

	/* BOE Block12 */
    private List<EntTblFactoryProductSupplier> listFactoryProductSupplier;
    private String  factoryProductSupplierJsonStr;
    private List<EntMstSiire> listAllEntMstSiire;
    private List<EntMstNouki> listAllEntMstNouki;
	/* EOE Block12 */

	/* BOE Block13 */
	private EntTblFactoryProductGeneral factoryProductGeneral;
	private List<EntMstProductSupplierStatus> listProductSupplierStatus;
	/* EOE Block13 */

	/* BOE Block14 */
	private List<ImageUploadKenDo> listImageSentenceJson;
	private ImageUploadKenDo imageUploadKenDo;
	private String thumbnailUrl;
	/* EOE Block14 */

	/* BOE Block15 */
	private EntTblFactoryProductNew productSearch;
	private List<EntTblFactoryProductNew> listProduct;
	private Integer totalListProduct;
	private Map<String, String> lstRegistrationFlg;
	private Integer totalProductOfMatter;
	/*BOE Nguyen.Chuong 2014/04/23: load page, pagesize and sort from matterDetail.*/
	private Integer page;
	private Integer pageSize;
	//flg to check matterDetail filter.
	private Boolean searchFromMatterDetailFlg;
	/*EOE Nguyen.Chuong 2014/04/23: load page, pagesize and sort from matterDetail.*/
	//Number product release when select all block 15
	private Integer numberReleaseProduct;
	//Status when delete data
	private Integer deleteStatus;
	/* EOE Block15 */


	////////// get set code ////////////

	/* BOE Block1 */
	/**
	 * @return the msgString
	 */
	public String getMsgString() {
		return msgString;
	}

	/**
	 * @param msgString the msgString to set
	 */
	public void setMsgString(String msgString) {
		this.msgString = msgString;
	}
    /**
     * @return the product
     */
    public EntTblFactoryProductNew getProduct() {
        return product;
    }

    /**
     * @param product the product to set
     */
    public void setProduct(EntTblFactoryProductNew product) {
        this.product = product;
    }

    /**
     * @return the mode
     */
    public String getMode() {
        return mode;
    }

    /**
     * @param mode the mode to set
     */
    public void setMode(String mode) {
        this.mode = mode;
    }

    /**
	 * @return the productJson
	 */
	public String getProductJson() {
		return productJson;
	}

	/**
	 * @param productJson the productJson to set
	 */
	public void setProductJson(String productJson) {
		this.productJson = productJson;
	}
	/* EOE Block1 */

	/* BOE Block2 */
	/**
	 * @return the listImage
	 */
	public List<EntTblFactoryProductImage> getListImage() {
		return listImage;
	}

	/**
	 * @param listImage the listImage to set
	 */
	public void setListImage(List<EntTblFactoryProductImage> listImage) {
		this.listImage = listImage;
	}

	/**
	 * @return the factoryProductImageDetailJsonStr
	 */
	public String getFactoryProductImageDetailJsonStr() {
		return factoryProductImageDetailJsonStr;
	}

	/**
	 * @param factoryProductImageDetailJsonStr the factoryProductImageDetailJsonStr to set
	 */
	public void setFactoryProductImageDetailJsonStr(
			String factoryProductImageDetailJsonStr) {
		this.factoryProductImageDetailJsonStr = factoryProductImageDetailJsonStr;
	}

	/**
	 * @return the urlProductFactoryPath
	 */
	public String getUrlProductFactoryPath() {
		return urlProductFactoryPath;
	}

	/**
	 * @param urlProductFactoryPath the urlProductFactoryPath to set
	 */
	public void setUrlProductFactoryPath(String urlProductFactoryPath) {
		this.urlProductFactoryPath = urlProductFactoryPath;
	}

	/**
	 * @return the maxUploadImage
	 */
	public Integer getMaxUploadImage() {
		return maxUploadImage;
	}

	/**
	 * @param maxUploadImage the maxUploadImage to set
	 */
	public void setMaxUploadImage(Integer maxUploadImage) {
		this.maxUploadImage = maxUploadImage;
	}

	/**
	 * @return the maxUploadFolderMatter
	 */
	public Integer getMaxUploadFolderMatter() {
		return maxUploadFolderMatter;
	}

	/**
	 * @param maxUploadFolderMatter the maxUploadFolderMatter to set
	 */
	public void setMaxUploadFolderMatter(Integer maxUploadFolderMatter) {
		this.maxUploadFolderMatter = maxUploadFolderMatter;
	}
	/* EOE Block2 */

	/* BOE Block3 */
	/**
	 * @return the brandInfo
	 */
	public EntMstBrand getBrandInfo() {
		return brandInfo;
	}

	/**
	 * @param brandInfo the brandInfo to set
	 */
	public void setBrandInfo(EntMstBrand brandInfo) {
		this.brandInfo = brandInfo;
	}
	/**
	 * @return the brandList
	 */
	public List<EntMstBrand> getBrandList() {
		return brandList;
	}

	/**
	 * @param brandList the brandList to set
	 */
	public void setBrandList(List<EntMstBrand> brandList) {
		this.brandList = brandList;
	}
	/**
	 * @return the mapProductConditionFlg
	 */
	public Map<String, String> getMapProductConditionFlg() {
		return mapProductConditionFlg;
	}

	/**
	 * @param mapProductConditionFlg the mapProductConditionFlg to set
	 */
	public void setMapProductConditionFlg(Map<String, String> mapProductConditionFlg) {
		this.mapProductConditionFlg = mapProductConditionFlg;
	}
	/* EOE Block3 */

	/* BOE Block4 */
	/* EOE Block4 */

	/* BOE Block4 */
    /**
     * @return the productId
     */
    public Long getProductId() {
        return productId;
    }

    /**
     * @param productId the productId to set
     */
    public void setProductId(Long productId) {
        this.productId = productId;
    }

    /**
     * @return the listAttribute
     */
    public List<EntMstAttribute> getListAttribute() {
        return listAttribute;
    }

    /**
     * @param listAttribute the listAttribute to set
     */
    public void setListAttribute(List<EntMstAttribute> listAttribute) {
        this.listAttribute = listAttribute;
    }

    /**
     * @return the listAllAttribute
     */
    public List<EntMstAttribute> getListAllAttribute() {
        return listAllAttribute;
    }

    /**
     * @param listAllAttribute the listAllAttribute to set
     */
    public void setListAllAttribute(List<EntMstAttribute> listAllAttribute) {
        this.listAllAttribute = listAllAttribute;
    }

    /**
     * @return the listAttrGroup
     */
    public List<EntMstAttributeGroup> getListAttrGroup() {
        return listAttrGroup;
    }

    /**
     * @param listAttrGroup the listAttrGroup to set
     */
    public void setListAttrGroup(List<EntMstAttributeGroup> listAttrGroup) {
        this.listAttrGroup = listAttrGroup;
    }
	/* EOE Block4 */

	/* BOE Block5 */
    /**
	 * @return the listLink
	 */
	public List<EntTblFactoryProductLink> getListLink() {
		return listLink;
	}

	/**
	 * @param listLink the listLink to set
	 */
	public void setListLink(List<EntTblFactoryProductLink> listLink) {
		this.listLink = listLink;
	}

	/**
	 * @return the factoryProductLinkJsonStr
	 */
	public String getFactoryProductLinkJsonStr() {
		return factoryProductLinkJsonStr;
	}

	/**
	 * @param factoryProductLinkJsonStr the factoryProductLinkJsonStr to set
	 */
	public void setFactoryProductLinkJsonStr(String factoryProductLinkJsonStr) {
		this.factoryProductLinkJsonStr = factoryProductLinkJsonStr;
	}

	/**
	 * @return the listMstLinkReason
	 */
	public List<EntMstLinkReason> getListMstLinkReason() {
		return listMstLinkReason;
	}

	/**
	 * @param listMstLinkReason the listMstLinkReason to set
	 */
	public void setListMstLinkReason(List<EntMstLinkReason> listMstLinkReason) {
		this.listMstLinkReason = listMstLinkReason;
	}

	/**
	 * @return the listSyouhinSearch
	 */
	public List<EntMstSyouhinSearch> getListSyouhinSearch() {
		return listSyouhinSearch;
	}

	/**
	 * @param listSyouhinSearch the listSyouhinSearch to set
	 */
	public void setListSyouhinSearch(List<EntMstSyouhinSearch> listSyouhinSearch) {
		this.listSyouhinSearch = listSyouhinSearch;
	}

	/**
	 * @return the listSyouhinSearchJsonStr
	 */
	public String getListSyouhinSearchJsonStr() {
		return listSyouhinSearchJsonStr;
	}

	/**
	 * @param listSyouhinSearchJsonStr the listSyouhinSearchJsonStr to set
	 */
	public void setListSyouhinSearchJsonStr(String listSyouhinSearchJsonStr) {
		this.listSyouhinSearchJsonStr = listSyouhinSearchJsonStr;
	}

	/**
	 * @return the syouhinSearch
	 */
	public EntMstSyouhinSearch getSyouhinSearch() {
		return syouhinSearch;
	}

	/**
	 * @param syouhinSearch the syouhinSearch to set
	 */
	public void setSyouhinSearch(EntMstSyouhinSearch syouhinSearch) {
		this.syouhinSearch = syouhinSearch;
	}
	/* EOE Block5 */

	/* BOE Block6 */
	/**
	 * @return the factoryProductVideoJsonStr
	 */
	public String getFactoryProductVideoJsonStr() {
		return factoryProductVideoJsonStr;
	}

	/**
	 * @param factoryProductVideoJsonStr the factoryProductVideoJsonStr to set
	 */
	public void setFactoryProductVideoJsonStr(String factoryProductVideoJsonStr) {
		this.factoryProductVideoJsonStr = factoryProductVideoJsonStr;
	}

	/**
	 * @return the listFactoryProductVideo
	 */
	public List<EntTblFactoryProductVideo> getListFactoryProductVideo() {
		return listFactoryProductVideo;
	}

	/**
	 * @param listFactoryProductVideo the listFactoryProductVideo to set
	 */
	public void setListFactoryProductVideo(
			List<EntTblFactoryProductVideo> listFactoryProductVideo) {
		this.listFactoryProductVideo = listFactoryProductVideo;
	}

	/**
	 * @return the jSonVideoDs
	 */
	public String getjSonVideoDs() {
		return jSonVideoDs;
	}

	/**
	 * @param jSonVideoDs the jSonVideoDs to set
	 */
	public void setjSonVideoDs(String jSonVideoDs) {
		this.jSonVideoDs = jSonVideoDs;
	}

	/**
	 * @return the msgCode
	 */
	public Integer getMsgCode() {
		return msgCode;
	}

	/**
	 * @param msgCode the msgCode to set
	 */
	public void setMsgCode(Integer msgCode) {
		this.msgCode = msgCode;
	}
	/* EOE Block6 */

	/* BOE Block7 */
    /**
	 * @return the productDescription
	 */
	public EntTblFactoryProductDescription getProductDescription() {
		return productDescription;
	}

	/**
	 * @param productDescription the productDescription to set
	 */
	public void setProductDescription(
			EntTblFactoryProductDescription productDescription) {
		this.productDescription = productDescription;
	}
	/* EOE Block7 */

	/* BOE Block8 */
    /**
     * @return the lstProductGuestInputJson
     */
    public String getLstProductGuestInputJson() {
        return lstProductGuestInputJson;
    }

    /**
     * @param lstProductGuestInputJson the lstProductGuestInputJson to set
     */
    public void setLstProductGuestInputJson(String lstProductGuestInputJson) {
        this.lstProductGuestInputJson = lstProductGuestInputJson;
    }
	/* EOE Block8 */

	/* BOE Block9 */
    /**
     * @return the productPrice
     */
    public EntTblFactoryProductNew getProductPrice() {
        return productPrice;
    }

    /**
     * @param productPrice the productPrice to set
     */
    public void setProductPrice(EntTblFactoryProductNew productPrice) {
        this.productPrice = productPrice;
    }

    /**
     * @return the marumeCode
     */
    public Integer getMarumeCode() {
        return marumeCode;
    }

    /**
     * @param marumeCode the marumeCode to set
     */
    public void setMarumeCode(Integer marumeCode) {
        this.marumeCode = marumeCode;
    }

    /**
     * @return the defaultFlagString
     */
    public String getDefaultFlagString() {
        return defaultFlagString;
    }

    /**
     * @param defaultFlagString the defaultFlagString to set
     */
    public void setDefaultFlagString(String defaultFlagString) {
        this.defaultFlagString = defaultFlagString;
    }

    /**
     * @return the listProductSameGroup
     */
    public List<EntTblFactoryProductNew> getListProductSameGroup() {
        return listProductSameGroup;
    }

    /**
     * @param listProductSameGroup the listProductSameGroup to set
     */
    public void setListProductSameGroup(List<EntTblFactoryProductNew> listProductSameGroup) {
        this.listProductSameGroup = listProductSameGroup;
    }

    /**
     * @return the minProperPrice
     */
    public Long getMinProperPrice() {
        return minProperPrice;
    }

    /**
     * @param minProperPrice the minProperPrice to set
     */
    public void setMinProperPrice(Long minProperPrice) {
        this.minProperPrice = minProperPrice;
    }

    /**
     * @return the maxProperPrice
     */
    public Long getMaxProperPrice() {
        return maxProperPrice;
    }

    /**
     * @param maxProperPrice the maxProperPrice to set
     */
    public void setMaxProperPrice(Long maxProperPrice) {
        this.maxProperPrice = maxProperPrice;
    }

    /**
     * @return the groupCode
     */
    public Long getGroupCode() {
        return groupCode;
    }

    /**
     * @param groupCode the groupCode to set
     */
    public void setGroupCode(Long groupCode) {
        this.groupCode = groupCode;
    }

    /**
     * @return the listSelectJson
     */
    public String getListSelectJson() {
        return listSelectJson;
    }

    /**
     * @param listSelectJson the listSelectJson to set
     */
    public void setListSelectJson(String listSelectJson) {
        this.listSelectJson = listSelectJson;
    }

    /**
     * @return the listProductSelect
     */
    public List<EntTblFactoryProductSelect> getListProductSelect() {
        return listProductSelect;
    }

    /**
     * @param listProductSelect the listProductSelect to set
     */
    public void setListProductSelect(List<EntTblFactoryProductSelect> listProductSelect) {
        this.listProductSelect = listProductSelect;
    }

    /**
     * @return the rule
     */
    public EntTblBrandConditionRule getRule() {
        return rule;
    }

    /**
     * @param rule the rule to set
     */
    public void setRule(EntTblBrandConditionRule rule) {
        this.rule = rule;
    }

    /**
     * @return the brandMarumeCode
     */
    public Integer getBrandMarumeCode() {
        return brandMarumeCode;
    }

    /**
     * @param brandMarumeCode the brandMarumeCode to set
     */
    public void setBrandMarumeCode(Integer brandMarumeCode) {
        this.brandMarumeCode = brandMarumeCode;
    }

    /**
     * @return the listProductSelectStr
     */
    public String getListProductSelectStr() {
        return listProductSelectStr;
    }

    /**
     * @param listProductSelectStr the listProductSelectStr to set
     */
    public void setListProductSelectStr(String listProductSelectStr) {
        this.listProductSelectStr = listProductSelectStr;
    }
    /* EOE Block9 */

    /* BOE Block10 */
    /**
     * @return the categoryCode
     */
    public Integer getCategoryCode() {
        return categoryCode;
    }

	/**
     * @param categoryCode the categoryCode to set
     */
    public void setCategoryCode(Integer categoryCode) {
        this.categoryCode = categoryCode;
    }

    /**
     * @return the listCategory
     */
    public List<EntMstCategory> getListCategory() {
        return listCategory;
    }

    /**
     * @param listCategory the listCategory to set
     */
    public void setListCategory(List<EntMstCategory> listCategory) {
        this.listCategory = listCategory;
    }
	/* EOE Block10 */

	/* BOE Block11 */
	/**
	 * @return the listGridGroupCodeStr
	 */
	public String getListGridGroupCodeStr() {
		return listGridGroupCodeStr;
	}

	/**
	 * @param listGridGroupCodeStr the listGridGroupCodeStr to set
	 */
	public void setListGridGroupCodeStr(String listGridGroupCodeStr) {
		this.listGridGroupCodeStr = listGridGroupCodeStr;
	}
	/**
	 * @return the factoryMatter
	 */
	public EntMstFactoryMatter getFactoryMatter() {
		return factoryMatter;
	}

	/**
	 * @param factoryMatter the factoryMatter to set
	 */
	public void setFactoryMatter(EntMstFactoryMatter factoryMatter) {
		this.factoryMatter = factoryMatter;
	}
	/**
	 * @return the listProductFitModel
	 */
	public List<EntTblFactoryProductNew> getListProductFitModel() {
		return listProductFitModel;
	}

	/**
	 * @param listProductFitModel the listProductFitModel to set
	 */
	public void setListProductFitModel(List<EntTblFactoryProductNew> listProductFitModel) {
		this.listProductFitModel = listProductFitModel;
	}
	/**
	 * @return the listFitModel
	 */
	public List<EntTblFactoryProductFitModel> getListFitModel() {
		return listFitModel;
	}

	/**
	 * @param listFitModel the listFitModel to set
	 */
	public void setListFitModel(List<EntTblFactoryProductFitModel> listFitModel) {
		this.listFitModel = listFitModel;
	}
	/**
	 * @return the listFitModelJsonStr
	 */
	public String getListFitModelJsonStr() {
		return listFitModelJsonStr;
	}

	/**
	 * @param listFitModelJsonStr the listFitModelJsonStr to set
	 */
	public void setListFitModelJsonStr(String listFitModelJsonStr) {
		this.listFitModelJsonStr = listFitModelJsonStr;
	}
	/* EOE Block11 */

	/* BOE Block12 */
	/**
	 * @return the listFactoryProductSupplier
	 */
	public List<EntTblFactoryProductSupplier> getListFactoryProductSupplier() {
		return listFactoryProductSupplier;
	}

	/**
	 * @param listFactoryProductSupplier the listFactoryProductSupplier to set
	 */
	public void setListFactoryProductSupplier(
			List<EntTblFactoryProductSupplier> listFactoryProductSupplier) {
		this.listFactoryProductSupplier = listFactoryProductSupplier;
	}
	/**
	 * @return the factoryProductSupplierJsonStr
	 */
	public String getFactoryProductSupplierJsonStr() {
		return factoryProductSupplierJsonStr;
	}

	/**
	 * @param factoryProductSupplierJsonStr the factoryProductSupplierJsonStr to set
	 */
	public void setFactoryProductSupplierJsonStr(
			String factoryProductSupplierJsonStr) {
		this.factoryProductSupplierJsonStr = factoryProductSupplierJsonStr;
	}
	/**
	 * @return the listAllEntMstSiire
	 */
	public List<EntMstSiire> getListAllEntMstSiire() {
		return listAllEntMstSiire;
	}

	/**
	 * @param listAllEntMstSiire the listAllEntMstSiire to set
	 */
	public void setListAllEntMstSiire(List<EntMstSiire> listAllEntMstSiire) {
		this.listAllEntMstSiire = listAllEntMstSiire;
	}
	/**
	 * @return the listAllEntMstNouki
	 */
	public List<EntMstNouki> getListAllEntMstNouki() {
		return listAllEntMstNouki;
	}

	/**
	 * @param listAllEntMstNouki the listAllEntMstNouki to set
	 */
	public void setListAllEntMstNouki(List<EntMstNouki> listAllEntMstNouki) {
		this.listAllEntMstNouki = listAllEntMstNouki;
	}
	/* EOE Block12 */

	/* BOE Block13 */
	/**
	 * @return the listProductSupplierStatus
	 */
	public List<EntMstProductSupplierStatus> getListProductSupplierStatus() {
		return listProductSupplierStatus;
	}

	/**
	 * @param listProductSupplierStatus the listProductSupplierStatus to set
	 */
	public void setListProductSupplierStatus(
			List<EntMstProductSupplierStatus> listProductSupplierStatus) {
		this.listProductSupplierStatus = listProductSupplierStatus;
	}
	/**
	 * @return the factoryProductGeneral
	 */
	public EntTblFactoryProductGeneral getFactoryProductGeneral() {
		return factoryProductGeneral;
	}

	/**
	 * @param factoryProductGeneral the factoryProductGeneral to set
	 */
	public void setFactoryProductGeneral(EntTblFactoryProductGeneral factoryProductGeneral) {
		this.factoryProductGeneral = factoryProductGeneral;
	}
	/* EOE Block13 */

	/* BOE Block14 */
	/**
	 * @return the listImageSentenceJson
	 */
	public List<ImageUploadKenDo> getListImageSentenceJson() {
		return listImageSentenceJson;
	}

	/**
	 * @param listImageSentenceJson the listImageSentenceJson to set
	 */
	public void setListImageSentenceJson(List<ImageUploadKenDo> listImageSentenceJson) {
		this.listImageSentenceJson = listImageSentenceJson;
	}
	/**
	 * @return the thumbnailUrl
	 */
	public String getThumbnailUrl() {
		return thumbnailUrl;
	}

	/**
	 * @param thumbnailUrl the thumbnailUrl to set
	 */
	public void setThumbnailUrl(String thumbnailUrl) {
		this.thumbnailUrl = thumbnailUrl;
	}
	/**
	 * @return the imageUploadKenDo
	 */
	public ImageUploadKenDo getImageUploadKenDo() {
		return imageUploadKenDo;
	}

	/**
	 * @param imageUploadKenDo the imageUploadKenDo to set
	 */
	public void setImageUploadKenDo(ImageUploadKenDo imageUploadKenDo) {
		this.imageUploadKenDo = imageUploadKenDo;
	}
	/* EOE Block14 */

	/* BOE Block15 */
	/**
     * @return the listProduct
     */
    public List<EntTblFactoryProductNew> getListProduct() {
        return listProduct;
    }

    /**
     * @param listProduct the listProduct to set
     */
    public void setListProduct(List<EntTblFactoryProductNew> listProduct) {
        this.listProduct = listProduct;
    }

    /**
     * @return the totalListProduct
     */
    public Integer getTotalListProduct() {
        return totalListProduct;
    }

    /**
     * @param totalListProduct the totalListProduct to set
     */
    public void setTotalListProduct(Integer totalListProduct) {
        this.totalListProduct = totalListProduct;
    }

    /**
     * @return the lstRegistrationFlg
     */
    public Map<String, String> getLstRegistrationFlg() {
        return lstRegistrationFlg;
    }

    /**
     * @param lstRegistrationFlg the lstRegistrationFlg to set
     */
    public void setLstRegistrationFlg(Map<String, String> lstRegistrationFlg) {
        this.lstRegistrationFlg = lstRegistrationFlg;
    }

    /**
     * @return the productSearch
     */
    public EntTblFactoryProductNew getProductSearch() {
        return productSearch;
    }

    /**
     * @param productSearch the productSearch to set
     */
    public void setProductSearch(EntTblFactoryProductNew productSearch) {
        this.productSearch = productSearch;
    }

    /**
     * @return the totalProductOfMatter
     */
    public Integer getTotalProductOfMatter() {
        return totalProductOfMatter;
    }

    /**
     * @param totalProductOfMatter the totalProductOfMatter to set
     */
    public void setTotalProductOfMatter(Integer totalProductOfMatter) {
        this.totalProductOfMatter = totalProductOfMatter;
    }

	/**
	 * @return the numberReleaseProduct
	 */
	public Integer getNumberReleaseProduct() {
		return numberReleaseProduct;
	}

	/**
	 * @param numberReleaseProduct the numberReleaseProduct to set
	 */
	public void setNumberReleaseProduct(Integer numberReleaseProduct) {
		this.numberReleaseProduct = numberReleaseProduct;
	}

	/**
	 * @return the deleteStatus
	 */
	public Integer getDeleteStatus() {
		return deleteStatus;
	}

	/**
	 * @param deleteStatus the deleteStatus to set
	 */
	public void setDeleteStatus(Integer deleteStatus) {
		this.deleteStatus = deleteStatus;
	}
    /* EOE Block15 */

	/**
	 * @return the productMatterNo
	 */
	public Long getProductMatterNo() {
		return productMatterNo;
	}

	/**
	 * @param productMatterNo the productMatterNo to set
	 */
	public void setProductMatterNo(Long productMatterNo) {
		this.productMatterNo = productMatterNo;
	}

	/**
	 * @return the productIdDupliCate
	 */
	public Long getProductIdDupliCate() {
		return productIdDupliCate;
	}

	/**
	 * @param productIdDupliCate the productIdDupliCate to set
	 */
	public void setProductIdDupliCate(Long productIdDupliCate) {
		this.productIdDupliCate = productIdDupliCate;
	}

	/**
	 * @return the flgToGetPriceBlock9
	 */
	public Integer getFlgToGetPriceBlock9() {
		return flgToGetPriceBlock9;
	}

	/**
	 * @param flgToGetPriceBlock9 the flgToGetPriceBlock9 to set
	 */
	public void setFlgToGetPriceBlock9(Integer flgToGetPriceBlock9) {
		this.flgToGetPriceBlock9 = flgToGetPriceBlock9;
	}

    /**
     * @return the page
     */
    public Integer getPage() {
        return page;
    }

    /**
     * @param page the page to set
     */
    public void setPage(Integer page) {
        this.page = page;
    }

    /**
     * @return the pageSize
     */
    public Integer getPageSize() {
        return pageSize;
    }

    /**
     * @param pageSize the pageSize to set
     */
    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    /**
     * @return the searchFromMatterDetailFlg
     */
    public Boolean getSearchFromMatterDetailFlg() {
        return searchFromMatterDetailFlg;
    }

    /**
     * @param searchFromMatterDetailFlg the searchFromMatterDetailFlg to set
     */
    public void setSearchFromMatterDetailFlg(Boolean searchFromMatterDetailFlg) {
        this.searchFromMatterDetailFlg = searchFromMatterDetailFlg;
    }
}
