/************************************************************************
 * File Name    ： JSonAction.java
 * Author        ： Nguyen.Chuong
 * Version        ： 1.0.0
 * Date Created    ： 2013/09/24
 * Date Updated    ： 2013/09/24
 * Description    ： JSonAction for get json from ajax.
 *
 ***********************************************************************/
package net.webike.RcProductFactory.action;

import java.util.ArrayList;
import java.util.List;

import net.webike.RcProductFactory.entity.EntCategory;
import net.webike.RcProductFactory.entity.EntMstBunrui;
import net.webike.RcProductFactory.entity.EntMstBunruikaisou;
import net.webike.RcProductFactory.entity.EntMstFactoryMatter;
import net.webike.RcProductFactory.entity.EntTblFactoryProductImage;
import net.webike.RcProductFactory.entity.EntTblFactoryProductModel;
import net.webike.RcProductFactory.entity.EntTblFactoryProductNew;
import net.webike.RcProductFactory.entity.EntTreeView;
import net.webike.RcProductFactory.service.BunruiService;
import net.webike.RcProductFactory.service.MatterService;
import net.webike.RcProductFactory.service.ProductService;
import net.webike.RcProductFactory.service.UserTmpService;
import net.webike.RcProductFactory.util.Constant;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.apache.struts2.json.annotations.JSON;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 * JSonAction for get json from ajax.
 */
@Controller
@Scope("request")
@ParentPackage(value = "json-default")
@Results(
    {
        @Result(name = "list", type = "json"),
        // Return error page when parameter fail
        @Result(name = "errorOther", type = "redirect", location = "error.html")
    }
)
public class JSonAction extends CommonAction {

    /**
     * @serialVersionUID SerialVersion
     */
    private static final long serialVersionUID = 1L;

    /**
     * Matter Service.
     */
    @Autowired
    private MatterService matterService;

    @Autowired
    private BunruiService bunruiService;

    @Autowired
    private UserTmpService userTmpService;

    @Autowired
    private ProductService productService;
    /**
     * Boolean to check userId existed or not.
     */
    private boolean validUserId;

    /**
     * List of EntMstFactoryMatter.
     */
    private List<EntMstFactoryMatter> mainBodyList;

    /**
     * List of EntMstBunruikaisou.
     */
    private List<EntMstBunruikaisou> bunruikaisouList;

    /**
     * List of EntMstBunrui.
     */
    private List<EntMstBunrui> bunruiList;

    /**
     * Category TreeView.
     */
    private List<EntCategory> category;

    /**
     * List TreeView.
     */
    private List<EntTreeView> treeView;
    /* BOE move to userMangeAction @rcv!Nguyen.Chuong 2014/01/13 [6115]*/
/*    *//**
     * List of EntMstFactoryUser.
     *//*
    private List<EntMstFactoryUser> userManageBodyList;*/
    /* EOE move to userMangeAction @rcv!Nguyen.Chuong 2014/01/13 [6115]*/
    /**
     * Get list product for productInfo page.
     */
    private List<EntTblFactoryProductImage> listImage;
    private List<EntTblFactoryProductModel> listModel;

    @Override
    public String execute() {
        return "list";
    }

    /************************************************************************
     * <b>Description:</b><br>
     *  mainMenuLoadJson action for load main content ot mainMenu page by Ajax.
     *
     * @author        Nguyen.Chuong
     * @date        Sep 11, 2013
     * @return      String
     * @throws      Exception
     ************************************************************************/
    @Action("/mainMenuLoadJson")
    public String mainMenuLoadJson() {
        // Get parameter from request
        String status = this.getRequest().getParameter(Constant.PARAM_STATUS);
        String user = this.getRequest().getParameter(Constant.PARAM_USER_ID);

        // Get list Matter and set to MainBody
        List<EntMstFactoryMatter> list = matterService.selectListMatterByStatusAndUser(status, user);
        this.setMainBodyList(list);

        return "list";
    }
    /* BOE move to userMangeAction @rcv!Nguyen.Chuong 2014/01/13 [6115]*/
/*    *//************************************************************************
     * <b>Description:</b><br>
     *  userManageLoadJson action for load main content after filter userManage page by Ajax.
     *
     * @author        Tran.Thanh
     * @date        2 Oct 2013
     * @return        String
     ************************************************************************//*
    @Action("/userManageLoadJson")
    public String userManageLoadJson() {
        //Validate loginUser
        if (!super.validateLoginUser()) {
            return "errorOther";
        }
        // Get parameter from request
        EntMstFactoryUser entUser = new EntMstFactoryUser();
        entUser.setUserId(this.getRequest().getParameter(Constant.PARAM_USER_ID));
        entUser.setUserName(this.getRequest().getParameter(Constant.PARAM_USER_NAME));
        entUser.setUserAuthority(this.getRequest().getParameter(Constant.PARAM_USER_AUTHORITY));
        entUser.setDelFlg(this.getRequest().getParameter(Constant.PARAM_DEL_FLAG));

        // Get list of Users and set to UserManageBody
        List<EntMstFactoryUser> listUser = userService.selectFilterUser(entUser);
        this.setUserManageBodyList(listUser);
        return "list";
    }*/
    /* EOE move to userMangeAction @rcv!Nguyen.Chuong 2014/01/13 [6115]*/

    /************************************************************************
     * <b>Description:</b><br>
     *  Action for check that userId is existed in DB or not.
     *
     * @author      Nguyen.Chuong
     * @date        Sep 11, 2013
     * @return      String json
     ************************************************************************/
    @Action("/checkExistedUserId")
    public String checkExistedUserId() {
        String userId = this.getRequest().getParameter(Constant.PARAM_USER_ID);
        //Check userId is existed.
        Boolean user =  userTmpService.selectUserIdExistedOrNot(userId);
        if (user) {
            //UserId existed.
            this.setValidUserId(true);
            return "list";
        }
        //UserId not existed.
        this.setValidUserId(false);
        return "list";
    }

    /************************************************************************
     * <b>Description:</b><br>
     *  get product info when click into row:
     *  + list image and thumb.
     *  + list model.
     *  + product information.
     *
     * @author      Nguyen.Chuong
     * @date        Oct 31, 2013
     * @return      String
     ************************************************************************/
    @Action("/selectProductDataAjax")
    public String selectProductDataAjax() {
        EntTblFactoryProductNew entProduct = new EntTblFactoryProductNew();
        //Get request productCode
        entProduct.setProductCode(this.getRequest().getParameter(Constant.PARAM_PRODUCT_CODE));
        entProduct.setProductId(Long.parseLong(this.getRequest().getParameter(Constant.PARAM_PRODUCT_CODE)));
        //Get list image of product.
        this.setListImage(productService.selectListImageAndThumbByProductId(entProduct));
        //Get list model of product.
        this.setListModel(productService.selectProductModelByProductCode(entProduct));
        return "list";
    }

    /************************************************************************
     * <b>Description:</b><br>
     *  Get TreeView Data.
     *
     * @author        Tran.Thanh
     * @date        23 Oct 2013
     * @return        String
     ************************************************************************/
    @Action("/selectTreeViewJson")
    public String selectTreeViewJson() {
        try {
        List<EntCategory> listEntCategory = null;
        // Get list bunrui from DB
        listEntCategory = bunruiService.selectCategory();

        // List bunrui code
        List<Integer> level1 = new ArrayList<Integer>();
        List<Integer> level2 = new ArrayList<Integer>();
        List<Integer> level3 = new ArrayList<Integer>();
        List<Integer> level4 = new ArrayList<Integer>();
        List<Integer> level5 = new ArrayList<Integer>();

        // List bunrui name
        List<String> level1String = new ArrayList<String>();
        List<String> level2String = new ArrayList<String>();
        List<String> level3String = new ArrayList<String>();
        List<String> level4String = new ArrayList<String>();
        List<String> level5String = new ArrayList<String>();

        // Set list bunrui which get from db to list bunrui code and list bunrui name
        for (int i = 0; i < listEntCategory.size(); i++) {
        	// Check each row of list bunrui, If have level 1 node then set to list bunrui code level1 and list bunrui name level1
            if (!listEntCategory.get(i).getLevel1Code().isEmpty()
                    && !level1.contains(Integer.parseInt(listEntCategory.get(i).getLevel1Code()))) {
                level1.add(Integer.parseInt(listEntCategory.get(i).getLevel1Code()));
                level1String.add(listEntCategory.get(i).getLevel1Name());
            }
            // Check each row of list bunrui, If have level 2 node then set to list bunrui code level2 and list bunrui name level2
            if (!listEntCategory.get(i).getLevel2Code().isEmpty()
                    && !level2.contains(Integer.parseInt(listEntCategory.get(i).getLevel2Code() + listEntCategory.get(i).getLevel1Code()))) {
                level2.add(Integer.parseInt(listEntCategory.get(i).getLevel2Code() + listEntCategory.get(i).getLevel1Code()));
                level2String.add(listEntCategory.get(i).getLevel2Name());
            }
            // Check each row of list bunrui, If have level 3 node then set to list bunrui code level3 and list bunrui name level3
            if (!listEntCategory.get(i).getLevel3Code().isEmpty()
                    && !level3.contains(Integer.parseInt(listEntCategory.get(i).getLevel3Code() + listEntCategory.get(i).getLevel2Code()))) {
                level3.add(Integer.parseInt(listEntCategory.get(i).getLevel3Code() + listEntCategory.get(i).getLevel2Code()));
                level3String.add(listEntCategory.get(i).getLevel3Name());
            }
            // Check each row of list bunrui, If have level 4 node then set to list bunrui code level4 and list bunrui name level4
            if (!listEntCategory.get(i).getLevel4Code().isEmpty()
                    && !level4.contains(Integer.parseInt(listEntCategory.get(i).getLevel4Code() + listEntCategory.get(i).getLevel3Code()))) {
                level4.add(Integer.parseInt(listEntCategory.get(i).getLevel4Code() + listEntCategory.get(i).getLevel3Code()));
                level4String.add(listEntCategory.get(i).getLevel4Name());
            }
            // Check each row of list bunrui, If have level 5 node then set to list bunrui code level5 and list bunrui name level5
            if (!listEntCategory.get(i).getLevel5Code().isEmpty()
                    && !level5.contains(Integer.parseInt(listEntCategory.get(i).getLevel5Code() + listEntCategory.get(i).getLevel4Code()))) {
                level5.add(Integer.parseInt(listEntCategory.get(i).getLevel5Code() + listEntCategory.get(i).getLevel4Code()));
                level5String.add(listEntCategory.get(i).getLevel5Name());
            }
        }

        // Create result tree
        List<EntTreeView> result = new ArrayList<EntTreeView>();

        // Set list bunrui code to listCode
        List<List<Integer>> listCode = new ArrayList<List<Integer>>();
        listCode.add(level1);
        listCode.add(level2);
        listCode.add(level3);
        listCode.add(level4);
        listCode.add(level5);

        // Set lit bunrui name to listDetail
        List<List<String>> listDetail = new ArrayList<List<String>>();
        listDetail.add(level1String);
        listDetail.add(level2String);
        listDetail.add(level3String);
        listDetail.add(level4String);
        listDetail.add(level5String);

        // Create result bunrui tree with listCode and listDetail
        result = fillTree(listCode, listDetail);

        // Set list TreeView to result Json
        this.setTreeView(result);
        } catch (Exception e) {
            this.getLogger().error(e.toString());
            e.printStackTrace();
        }
        return "list";
    }

    /************************************************************************
     * <b>Description:</b><br>
     *  Fill TreeView Category.
     *
     * @author		Tran.Thanh
     * @date		5 Nov 2013
     * @param       listCode list of level code.
     * @param       listDetail list of level name.
     * @return		List<EntTreeView>
     ************************************************************************/
    public List<EntTreeView> fillTree(List<List<Integer>> listCode
    								, List<List<String>> listDetail) {
        List<EntTreeView> result = new ArrayList<EntTreeView>();
        // Get list level of node to bunrui code
        List<Integer> level1 = listCode.get(0);
        List<Integer> level2 = listCode.get(1);
        List<Integer> level3 = listCode.get(2);
        List<Integer> level4 = listCode.get(Constant.LEVEL_3);
        List<Integer> level5 = listCode.get(Constant.LEVEL_4);
        // Get list level of node to bunrui name
        List<String> level1String = listDetail.get(0);
        List<String> level2String = listDetail.get(1);
        List<String> level3String = listDetail.get(2);
        List<String> level4String = listDetail.get(Constant.LEVEL_3);
        List<String> level5String = listDetail.get(Constant.LEVEL_4);

        for (int q = 0; q < level1.size(); q++) { // Get level 1 of node and fill to TreeView
		    EntTreeView node = new EntTreeView();
		    node.setId(level1.get(q).toString());
		    node.setData(level1String.get(q).toString());
		    node.setChildren(null);
		    EntTreeView entTreeTemp = new EntTreeView();
		    entTreeTemp.setId(level1.get(q).toString());
		    node.setAttr(entTreeTemp);
		    result.add(node);
		}
		for (int w = 0; w < level2.size(); w++) { // Get level 2 of node and fill to TreeView
		    EntTreeView node = new EntTreeView();
		    node.setId(level2.get(w).toString().substring(0, Constant.LIMIT_OF_CATEGORY_CODE_4));
		    node.setData(level2String.get(w).toString());
		    node.setChildren(null);
		    EntTreeView entTreeTemp = new EntTreeView();
		    entTreeTemp.setId(level2.get(w).toString().substring(0, Constant.LIMIT_OF_CATEGORY_CODE_4));
		    node.setAttr(entTreeTemp);
		    String parentId = level2.get(w).toString().substring(Constant.LIMIT_OF_CATEGORY_CODE_4, Constant.LIMIT_OF_CATEGORY_CODE_8);
		    for (int i = 0; i < result.size(); i++) {
		        if (result.get(i).getId().equals(parentId)) {
		            List<EntTreeView> listTemp = new ArrayList<EntTreeView>();
		            if (result.get(i).getChildren() == null) {
		                List<EntTreeView> x = new ArrayList<EntTreeView>();
		                x.add(node);
		                listTemp = x;
		            } else {
		                listTemp = result.get(i).getChildren();
		                listTemp.add(node);
		            }
		            result.get(i).setChildren(listTemp);
		        }
		    }
		}
		for (int e = 0; e < level3.size(); e++) { // Get level 3 of node and fill to TreeView
		    EntTreeView node = new EntTreeView();
		    node.setId(level3.get(e).toString().substring(0, Constant.LIMIT_OF_CATEGORY_CODE_4));
		    node.setData(level3String.get(e).toString());
		    node.setChildren(null);
		    EntTreeView entTreeTemp = new EntTreeView();
		    entTreeTemp.setId(level3.get(e).toString().substring(0, Constant.LIMIT_OF_CATEGORY_CODE_4));
		    node.setAttr(entTreeTemp);
		    String parentId = level3.get(e).toString().substring(Constant.LIMIT_OF_CATEGORY_CODE_4, Constant.LIMIT_OF_CATEGORY_CODE_8);
		    for (int i = 0; i < result.size(); i++) {
		        for (int j = 0; j < result.get(i).getChildren().size(); j++) {
		            if (result.get(i).getChildren().get(j).getId().equals(parentId)) {
		                List<EntTreeView> listTemp = new ArrayList<EntTreeView>();
		                if (result.get(i).getChildren().get(j).getChildren() == null) {
		                    List<EntTreeView> x = new ArrayList<EntTreeView>();
		                    x.add(node);
		                    listTemp = x;
		                } else {
		                    listTemp = result.get(i).getChildren().get(j).getChildren();
		                    listTemp.add(node);
		                }
		                result.get(i).getChildren().get(j).setChildren(listTemp);
		            }
		        }
		    }
		}
		int check = 0;
		for (int r = 0; r < level4.size(); r++) { // Get level 4 of node and fill to TreeView
		    check = 0;
		    EntTreeView node = new EntTreeView();
		    node.setId(level4.get(r).toString().substring(0, Constant.LIMIT_OF_CATEGORY_CODE_4));
		    node.setData(level4String.get(r).toString());
		    node.setChildren(null);
		    EntTreeView entTreeTemp = new EntTreeView();
		    entTreeTemp.setId(level4.get(r).toString().substring(0, Constant.LIMIT_OF_CATEGORY_CODE_4));
		    node.setAttr(entTreeTemp);
		    String parentId = level4.get(r).toString().substring(Constant.LIMIT_OF_CATEGORY_CODE_4, Constant.LIMIT_OF_CATEGORY_CODE_8);
		    for (int i = 0; i < result.size() && check != 1; i++) {
		        for (int j = 0; j < result.get(i).getChildren().size() && check != 1; j++) {
		            if (result.get(i).getChildren().get(j).getChildren() != null) {
		                for (int k = 0; k < result.get(i).getChildren().get(j).getChildren().size() && check != 1; k++) {
		                    if (result.get(i).getChildren().get(j).getChildren().get(k).getId() != null
		                            && result.get(i).getChildren().get(j).getChildren().get(k).getId().equals(parentId)) {
		                        List<EntTreeView> listTemp = new ArrayList<EntTreeView>();
		                        if (result.get(i).getChildren().get(j).getChildren().get(k).getChildren() == null) {
		                            List<EntTreeView> x = new ArrayList<EntTreeView>();
		                            x.add(node);
		                            listTemp = x;
		                        } else {
		                            listTemp = result.get(i).getChildren().get(j).getChildren().get(k).getChildren();
		                            listTemp.add(node);
		                        }
		                        result.get(i).getChildren().get(j).getChildren().get(k).setChildren(listTemp);
		                        check = 1;
		                        break;
		                    }
		                }
		            }
		        }
		    }
		}
		for (int r = 0; r < level5.size(); r++) { // Get level 5 of node and fill to TreeView
		    check = 0;
		    EntTreeView node = new EntTreeView();
		    node.setId(level5.get(r).toString().substring(0, Constant.LIMIT_OF_CATEGORY_CODE_4));
		    node.setData(level5String.get(r).toString());
		    node.setChildren(null);
		    EntTreeView entTreeTemp = new EntTreeView();
		    entTreeTemp.setId(level5.get(r).toString().substring(0, Constant.LIMIT_OF_CATEGORY_CODE_4));
		    node.setAttr(entTreeTemp);
		    String parentId = level5.get(r).toString().substring(Constant.LIMIT_OF_CATEGORY_CODE_4, Constant.LIMIT_OF_CATEGORY_CODE_8);
		    for (int i = 0; i < result.size() && check != 1; i++) {
		        for (int j = 0; j < result.get(i).getChildren().size() && check != 1; j++) {
		            if (result.get(i).getChildren().get(j).getChildren() != null) {
		                for (int k = 0; k < result.get(i).getChildren().get(j).getChildren().size() && check != 1; k++) {
		                    if (result.get(i).getChildren().get(j).getChildren().get(k).getChildren() != null) {
		                        for (int l = 0; l < result.get(i).getChildren().get(j).getChildren().get(k).getChildren().size() && check != 1; l++) {
		                            if (result.get(i).getChildren().get(j).getChildren().get(k).getChildren().get(l).getId() != null
		                                && result.get(i).getChildren().get(j).getChildren().get(k).getChildren().get(l).getId().equals(parentId)) {
		                                List<EntTreeView> listTemp = new ArrayList<EntTreeView>();
		                                if (result.get(i).getChildren().get(j).getChildren().get(k).getChildren().get(l).getChildren() == null) {
		                                    List<EntTreeView> x = new ArrayList<EntTreeView>();
		                                    x.add(node);
		                                    listTemp = x;
		                                } else {
		                                    listTemp = result.get(i).getChildren().get(j).getChildren().get(k).getChildren().get(l).getChildren();
		                                    listTemp.add(node);
		                                }
		                                result.get(i).getChildren().get(j).getChildren().get(k).getChildren().get(l).setChildren(listTemp);
		                                check = 1;
		                                break;
		                            }
		                        }
		                    }
		                }
		            }
		        }
		    }
		}
        return result;
    }

    /**
     * @return the mainBodyList
     */
    @JSON(name = "mainBodyList")
    public List<EntMstFactoryMatter> getMainBodyList() {
        return mainBodyList;
    }

    /**
     * @param mainBodyList the mainBodyList to set
     */
    public void setMainBodyList(List<EntMstFactoryMatter> mainBodyList) {
        this.mainBodyList = mainBodyList;
    }

    /**
     * @return the matterService
     */
    public MatterService getMatterService() {
        return matterService;
    }

    /**
     * @param matterService the matterService to set
     */
    public void setMatterService(MatterService matterService) {
        this.matterService = matterService;
    }
    /* BOE move to userMangeAction @rcv!Nguyen.Chuong 2014/01/13 [6115]*/
/*    *//**
     * @return the UserManageBodyList
     *//*
    @JSON(name = "userManageBodyList")
    public List<EntMstFactoryUser> getUserManageBodyList() {
        return userManageBodyList;
    }

    *//**
     * @param userManageBodyList the userManageBodyList to set
     *//*
    public void setUserManageBodyList(List<EntMstFactoryUser> userManageBodyList) {
        this.userManageBodyList = userManageBodyList;
    }*/
    /* EOE move to userMangeAction @rcv!Nguyen.Chuong 2014/01/13 [6115]*/

    /**
     * @return the validUserId
     */
    @JSON(name = "validUserId")
    public boolean getValidUserId() {
        return validUserId;
    }

    /**
     * @param validUserId the validUserId to set
     */
    public void setValidUserId(boolean validUserId) {
        this.validUserId = validUserId;
    }

    /**
     * @return the bunruiService
     */
    public BunruiService getBunruiService() {
        return bunruiService;
    }

    /**
     * @param bunruiService the bunruiService to set
     */
    public void setBunruiService(BunruiService bunruiService) {
        this.bunruiService = bunruiService;
    }

    /**
     * @return the bunruikaisouList
     */
    @JSON(name = "bunruikaisouList")
    public List<EntMstBunruikaisou> getBunruikaisouList() {
        return bunruikaisouList;
    }

    /**
     * @param bunruikaisouList the bunruikaisouList to set
     */
    public void setBunruikaisouList(List<EntMstBunruikaisou> bunruikaisouList) {
        this.bunruikaisouList = bunruikaisouList;
    }

    /**
     * @return the bunruiList
     */
    @JSON(name = "bunruiList")
    public List<EntMstBunrui> getBunruiList() {
        return bunruiList;
    }

    /**
     * @param bunruiList the bunruiList to set
     */
    public void setBunruiList(List<EntMstBunrui> bunruiList) {
        this.bunruiList = bunruiList;
    }

    /**
     * @return the category
     */
    @JSON(name = "category")
    public List<EntCategory> getCategory() {
        return category;
    }

    /**
     * @param category the category to set
     */
    public void setCategory(List<EntCategory> category) {
        this.category = category;
    }

    /**
     * @return the listImage
     */
    @JSON(name = "listImage")
    public List<EntTblFactoryProductImage> getListImage() {
        return listImage;
    }

    /**
     * @param listImage the listImage to set
     */
    public void setListImage(List<EntTblFactoryProductImage> listImage) {
        this.listImage = listImage;
    }

    /**
     * @return the listModel
     */
    @JSON(name = "listModel")
    public List<EntTblFactoryProductModel> getListModel() {
        return listModel;
    }

    /**
     * @param listModel the listModel to set
     */
    public void setListModel(List<EntTblFactoryProductModel> listModel) {
        this.listModel = listModel;
    }

    /**
     * @return the treeView
     */
    @JSON(name = "treeView")
    public List<EntTreeView> getTreeView() {
        return treeView;
    }

    /**
     * @param treeView the treeView to set
     */
    public void setTreeView(List<EntTreeView> treeView) {
        this.treeView = treeView;
    }

}
