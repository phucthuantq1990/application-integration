/************************************************************************
 * File Name	： CategoryEditActionForm.java
 * Author		： hoang.ho
 * Version		： 1.0.0
 * Date Created	： 2014/01/07
 * Date Updated	： 2014/01/07
 * Description	： store data of category to communicate with Java Server Page .
 *
 ***********************************************************************/
package net.webike.RcProductFactory.action.form;

import java.util.List;

import net.webike.RcProductFactory.entity.EntMstAttribute;
import net.webike.RcProductFactory.entity.EntMstBunrui;
import net.webike.RcProductFactory.entity.EntMstCategory;
import net.webike.RcProductFactory.entity.EntTblCategoryAttribute;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * store data of category to communicate with Java Server Page.
 * @author hoang.ho
 */
@Component
@Scope("request")
public class CategoryEditActionForm {

    private EntMstCategory category;
    private EntMstBunrui bunrui;
    private List<EntMstBunrui> lstBunrui;
    private EntTblCategoryAttribute catAttribute;
    private EntTblCategoryAttribute catAttributeToCreateMessageUpdated;
    private List<EntTblCategoryAttribute> catAttributeList;
    private List<EntTblCategoryAttribute> catAttributeUpdateList;
    private List<EntTblCategoryAttribute> catAttributeInsertList;
    private String deletedCodeList;
    /** to check insert or update. 1: insert, 0: update*/
    private int insertMode;
    private String originalAttributeCodeList;

    //List attribute to search attribute result
    private List<EntMstAttribute> lstAttribute;
    //Message
    private String message = "";
    // Message type. 0: error, 1: success
    private String msgType;

    //Exists category in DB
    private boolean existsCategory = false;
    /**
     * @return the category
     */
    public EntMstCategory getCategory() {
        return category;
    }

    /**
     * @param category the category to set
     */
    public void setCategory(EntMstCategory category) {
        this.category = category;
    }

    /**
     * @return the bunrui
     */
    public EntMstBunrui getBunrui() {
        return bunrui;
    }

    /**
     * @param bunrui the bunrui to set
     */
    public void setBunrui(EntMstBunrui bunrui) {
        this.bunrui = bunrui;
    }

    /**
     * @return the insertMode
     */
    public int getInsertMode() {
        return insertMode;
    }

    /**
     * @param insertMode the insertMode to set
     */
    public void setInsertMode(int insertMode) {
        this.insertMode = insertMode;
    }

    /**
     * @return the catAttribute
     */
    public EntTblCategoryAttribute getCatAttribute() {
        return catAttribute;
    }

    /**
     * @param catAttribute the catAttribute to set
     */
    public void setCatAttribute(EntTblCategoryAttribute catAttribute) {
        this.catAttribute = catAttribute;
    }

    /**
     * @return the catAttributeList
     */
    public List<EntTblCategoryAttribute> getCatAttributeList() {
        return catAttributeList;
    }

    /**
     * @param catAttributeList the catAttributeList to set
     */
    public void setCatAttributeList(List<EntTblCategoryAttribute> catAttributeList) {
        this.catAttributeList = catAttributeList;
    }

    /**
     * @return the deletedCodeList
     */
    public String getDeletedCodeList() {
        return deletedCodeList;
    }

    /**
     * @param deletedCodeList the deletedCodeList to set
     */
    public void setDeletedCodeList(String deletedCodeList) {
        this.deletedCodeList = deletedCodeList;
    }

    /**
     * @return the lstBunrui
     */
    public List<EntMstBunrui> getLstBunrui() {
        return lstBunrui;
    }

    /**
     * @param lstBunrui the lstBunrui to set
     */
    public void setLstBunrui(List<EntMstBunrui> lstBunrui) {
        this.lstBunrui = lstBunrui;
    }

    /**
     * @return the lstAttribute
     */
    public List<EntMstAttribute> getLstAttribute() {
        return lstAttribute;
    }

    /**
     * @param lstAttribute the lstAttribute to set
     */
    public void setLstAttribute(List<EntMstAttribute> lstAttribute) {
        this.lstAttribute = lstAttribute;
    }

    /**
     * @return the originalAttributeCodeList
     */
    public String getOriginalAttributeCodeList() {
        return originalAttributeCodeList;
    }

    /**
     * @param originalAttributeCodeList the originalAttributeCodeList to set
     */
    public void setOriginalAttributeCodeList(String originalAttributeCodeList) {
        this.originalAttributeCodeList = originalAttributeCodeList;
    }

    /**
     * @return the catAttributeUpdateList
     */
    public List<EntTblCategoryAttribute> getCatAttributeUpdateList() {
        return catAttributeUpdateList;
    }

    /**
     * @param catAttributeUpdateList the catAttributeUpdateList to set
     */
    public void setCatAttributeUpdateList(List<EntTblCategoryAttribute> catAttributeUpdateList) {
        this.catAttributeUpdateList = catAttributeUpdateList;
    }

    /**
     * @return the catAttributeInsertList
     */
    public List<EntTblCategoryAttribute> getCatAttributeInsertList() {
        return catAttributeInsertList;
    }

    /**
     * @param catAttributeInsertList the catAttributeInsertList to set
     */
    public void setCatAttributeInsertList(List<EntTblCategoryAttribute> catAttributeInsertList) {
        this.catAttributeInsertList = catAttributeInsertList;
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message the message to set
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * @return the msgType
     */
    public String getMsgType() {
        return msgType;
    }

    /**
     * @param msgType the msgType to set
     */
    public void setMsgType(String msgType) {
        this.msgType = msgType;
    }

    /**
     * @return the existsCategory
     */
    public boolean isExistsCategory() {
        return existsCategory;
    }

    /**
     * @param existsCategory the existsCategory to set
     */
    public void setExistsCategory(boolean existsCategory) {
        this.existsCategory = existsCategory;
    }

    public EntTblCategoryAttribute getCatAttributeToCreateMessageUpdated() {
        return catAttributeToCreateMessageUpdated;
    }

    public void setCatAttributeToCreateMessageUpdated(EntTblCategoryAttribute catAttributeToCreateMessageUpdated) {
        this.catAttributeToCreateMessageUpdated = catAttributeToCreateMessageUpdated;
    }
}
