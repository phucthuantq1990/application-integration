/************************************************************************
 * file name	： ProductPreviewAction.java
 * author		： Nguyen.Chuong
 * version		： 1.0.0
 * date created	： 2014/06/11
 * date updated	： 2014/06/11
 * description	： action to process productPreview page to preview data of product in matter.
 *
 ***********************************************************************/
package net.webike.RcProductFactory.action;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import net.webike.RcProductFactory.action.form.ProductPreviewActionForm;
import net.webike.RcProductFactory.entity.EntMstAttribute;
import net.webike.RcProductFactory.entity.EntMstFactoryMatterNew;
import net.webike.RcProductFactory.entity.EntMstFactoryUserTmp;
import net.webike.RcProductFactory.entity.EntMstMaker;
import net.webike.RcProductFactory.entity.EntMstModel;
import net.webike.RcProductFactory.entity.EntTblFactoryCalibrationRequestIndividual;
import net.webike.RcProductFactory.entity.EntTblFactoryCalibrationRequestWhole;
import net.webike.RcProductFactory.entity.EntTblFactoryProductDescription;
import net.webike.RcProductFactory.entity.EntTblFactoryProductFitModel;
import net.webike.RcProductFactory.entity.EntTblFactoryProductGeneral;
import net.webike.RcProductFactory.entity.EntTblFactoryProductGuestInput;
import net.webike.RcProductFactory.entity.EntTblFactoryProductImage;
import net.webike.RcProductFactory.entity.EntTblFactoryProductLink;
import net.webike.RcProductFactory.entity.EntTblFactoryProductNew;
import net.webike.RcProductFactory.entity.EntTblFactoryProductSelect;
import net.webike.RcProductFactory.entity.EntTblFactoryProductSupplier;
import net.webike.RcProductFactory.entity.EntTblFactoryProductVideo;
import net.webike.RcProductFactory.service.CalibrationService;
import net.webike.RcProductFactory.service.FitModelService;
import net.webike.RcProductFactory.service.MatterService;
import net.webike.RcProductFactory.service.ModelService;
import net.webike.RcProductFactory.service.ProductEditService;
import net.webike.RcProductFactory.service.ProductPreviewService;
import net.webike.RcProductFactory.service.ProductService;
import net.webike.RcProductFactory.service.UserTmpService;
import net.webike.RcProductFactory.util.Constant;
import net.webike.RcProductFactory.util.Utils;

import org.apache.commons.lang.StringUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 * Action to process productPreview page to preview data of product in matter.
 * Link from matterDetail by click from first link column in bottom grid.
 */
@ParentPackage(value = "CommonInterceptor")
@Results({
        //Process success then show productPreview page.
		@Result(name = "success", location = "productPreview.jsp"),
		//Return json
        @Result(name = "list", type = "json"),
        //Get list fit model.
  		@Result(name = "selectDisplay" , type = "json",
              params = {"contentType", "text/html", "root", "productPreviewActionForm.mapSelectDisplay" , "ignoreHierarchy", "true" }),
        //Get next, previous productId
        @Result(name = "productId", type = "json",
            params = {"contentType", "text/html", "root", "productPreviewActionForm" , "ignoreHierarchy", "true" }),
		//Get list guest input.
		@Result(name = "guestInput" , type = "json",
            params = {"contentType", "text/html", "root", "productPreviewActionForm.lstGuestInput" , "ignoreHierarchy", "true" }),
        @Result(name = "listImage" , type = "json",
            params = {"contentType", "text/html", "root", "productPreviewActionForm.factoryProductImageDetailJsonStr" , "ignoreHierarchy", "true" }),
        @Result(name = "listDescription" , type = "json",
        	params = {"contentType", "text/html", "root", "productPreviewActionForm.entDescription" , "ignoreHierarchy", "true" }),
            //Get list fit model.
  		@Result(name = "fitModel" , type = "json",
              params = {"contentType", "text/html", "root", "productPreviewActionForm.mapFitModel" , "ignoreHierarchy", "true" }),
        //Get list link.
      	@Result(name = "link" , type = "json",
              params = {"contentType", "text/html", "root", "productPreviewActionForm.lstLink" , "ignoreHierarchy", "true" }),
		//Error other case redirect to error page.
        @Result(name = "errorOther", type = "redirect", location = "error.html"),
        // Return error page when parameter fail
        @Result(name = "errorParams", type = "redirect", location = "error.html?error=param"),
        //Return error page when permission param.
        @Result(name = "errorPermission", type = "redirect", location = "error.html?error=noPermission"),
        @Result(name = "form" , type = "json",
            params = {"contentType", "text/html", "root", "productPreviewActionForm" , "ignoreHierarchy", "true" }),
        //Return list video of product from DB.
        @Result(name = "video" , type = "json",
            params = {"contentType", "text/html", "root", "productPreviewActionForm.listVideo" , "ignoreHierarchy", "true" }),
		//Return list attribute of product from DB.
        @Result(name = "attribute" , type = "json",
            params = {"contentType", "text/html", "root", "productPreviewActionForm.listAttribute" , "ignoreHierarchy", "true" }),
        @Result(name = "calibrationRequest" , type = "json",
            params = {"contentType", "text/html", "root", "productPreviewActionForm.listCalibrationRequest" , "ignoreHierarchy", "true" }),
        @Result(name = "statusPopUpCheck" , type = "json",
            params = {"contentType", "text/html", "root", "productPreviewActionForm.entStatusPopUpCheck" , "ignoreHierarchy", "true" }),
        @Result(name = "updateStatusPopUpCheck" , type = "json",
            params = {"contentType", "text/html", "root", "productId" , "ignoreHierarchy", "true" }),
        @Result(name = "changeCorrespondRequest" , type = "json",
            params = {"contentType", "text/html", "root", "" , "ignoreHierarchy", "true" }),
        @Result(name = "updateContentRequest" , type = "json",
            params = {"contentType", "text/html", "root", "" , "ignoreHierarchy", "true" }),
        @Result(name = "deleteSuccess" , type = "json",
        params = {"contentType", "text/html", "root", "responeMapAjaxDeleData" , "ignoreHierarchy", "true" })
		})
public class ProductPreviewAction extends CommonAction {
	/* serialVersionUID. */
	private static final long serialVersionUID = -7501314786361633145L;

	/* BOE Autowired, Global Variable by Tran.Thanh */
	@Autowired
	private ProductService productService;

	@Autowired
	private MatterService matterService;

//	@Autowired
//	private SettingService settingService;
	/* EOE Autowired, Global Variable by Tran.Thanh */

	/* BOE Autowired, Global Variable by Dai.Luong */
	@Autowired
	private ProductPreviewService productPreviewService;

	@Autowired
	private ProductPreviewActionForm productPreviewActionForm;
	/* EOE Autowired, Global Variable by Dai.Luong */
	/* BOE Autowired by @rcv!dau.phuong */
	@Autowired
    private UserTmpService userTmpService;
	/* EOE Autowired by @rcv!dau.phuong */
	/* BOE Autowired, Global Variable by Nguyen.Chuong */
	@Autowired
	private ProductEditService productEditService;
    @Autowired
    private CalibrationService calibrationService;
	private String productId;
	/* EOE Autowired, Global Variable by Nguyen.Chuong */
	// BOE @rcv!Luong.Tuong 2014/07/21  Global Variable
	@Autowired
	private ModelService modelService;
	@Autowired
	private FitModelService fitModelService;
	// EOE @rcv!Luong.Tuong 2014/07/21  Global Variable
	@Override
	@Action("/productPreview")
	public String execute() throws SQLException {
	    //Validate permission.
        if (!validateLoginUser()) {
            return "errorPermission";
        }

        //BOE Tran.Thanh 2014/06/24 : Add validate user has permission to process product of matter
        Long matterNo = productPreviewService.selectMatterNoByProductId(Long.valueOf(productId));
		if (null != matterNo) {
			if (this.getCommonForm().getLoginAuthority().contains("LIMITED_USER_ID")) {
				EntMstFactoryMatterNew matter = matterService.selectFactoryMatterByMatterCode(matterNo);
				Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		        String loginId = getLoginId(authentication);
				if (!matter.getMatterChargeUserId().equals(loginId)) {
					return "errorPermission";
				}
			}
		}
		//EOE Tran.Thanh 2014/06/24 : Add validate user has permission to process product of matter

        /* BOE @rcv!dau.phuong */

        //BOE @rcv!dau.phuong 2014/07/16 : get list user for request popup
        List<EntMstFactoryUserTmp> lstUser = userTmpService.selectAllUser();
        this.getCommonForm().setListCommonUser(lstUser);
        //EOE @rcv!dau.phuong 2014/07/16 : get list user for request popup

        //BOE @rcv!dau.phuong 2014/07/16 : get list calibration type for grid request
        List<EntTblFactoryCalibrationRequestIndividual> lstCalibrationType = productPreviewService.selectCalibrationType();
        productPreviewActionForm.setListCalibrationType(lstCalibrationType);
        //EOE @rcv!dau.phuong 2014/07/16 : get list calibration type for grid request

        //BOE @rcv!dau.phuong 2014/07/25: set json calibration type for edit data in grid
        String lstCalibrationTypeJson;
        if (lstCalibrationType == null) {
            lstCalibrationTypeJson = "[]";
        } else {
            lstCalibrationTypeJson = Utils.parseObjectToJson(lstCalibrationType);
        }
        productPreviewActionForm.setLstCalibrationTypeJson(lstCalibrationTypeJson);
        //EOE @rcv!dau.phuong 2014/07/25: set json calibration type for edit data in grid

        /* EOE @rcv!dau.phuong */

        //Check required field
        if (StringUtils.isNotEmpty(productId) && StringUtils.isNumeric(productId)) {
            productPreviewActionForm.setProductId(Long.valueOf(productId));
            return SUCCESS;
        }
        //Return to error param when don't productId param.
		return "errorParams";
	}

	/* BOE Function Tran.Thanh */
	/* Start block 2 */
	/************************************************************************
	 * <b>Description:</b><br>
	 *  Load data Block 2.
	 *
	 * @author		Tran.Thanh
	 * @date		Jun 13, 2014
	 * @return		data of block 2
	 * @throws 		SQLException		String
	 ************************************************************************/
	@Action("/loadDataBlock2")
    public String loadDataBlock2() throws SQLException {
		try {
			if (StringUtils.isNotEmpty(productId) && StringUtils.isNumeric(productId)) {
				// BOE @rcv!Tran.Thanh Jul 30, 2014 #9956 : Get Role of User and set to ActionForm
				Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		        List<String> loginAuthority = new ArrayList<String>();
		        for (GrantedAuthority grantedAuthority : authentication.getAuthorities()) {
		        	loginAuthority.add(grantedAuthority.getAuthority());
		        }
		        productPreviewActionForm.setRoleUser(loginAuthority);
				// EOE @rcv!Tran.Thanh Jul 30, 2014 #9956 : Get Role of User and set to ActionForm

	            productPreviewActionForm.setProductId(Long.valueOf(productId));
    	        //get matterNo by product by
				Long matterNo = productPreviewService.selectMatterNoByProductId(Long.valueOf(productId));
				if (null != matterNo) {
    				productPreviewActionForm.setMatterNo(matterNo);
    				//get total product in matter and set to form
    				productPreviewActionForm.setTotalProductInMatter(productPreviewService.countTotalProductInMatter(matterNo));
    				//get index of product in matter and set to form
    				productPreviewActionForm.setIndexProductInMatter(productPreviewService.indexProductInMatter(Long.valueOf(productId), matterNo));
    				//Get list all group > 0 in matter.
    				List<EntTblFactoryProductGeneral> listGroupInMatter = productPreviewService.selectListGroupByMatterNo(matterNo);
    				//get total group in matter and set to form
    				productPreviewActionForm.setTotalGroupInMatter(listGroupInMatter.size());
    				//get index of product in group and set to form
    				productPreviewActionForm.setIndexGroupInMatter(productPreviewService.indexGroupInMatter(Long.valueOf(productId)
    				                                                                                       , listGroupInMatter));
    				//get data of product general by productId
    				EntTblFactoryProductGeneral entProductGeneral = productPreviewService.selectProductGeneralByProductId(Long.valueOf(productId));
    				//Get list siire by productId
    				List<EntTblFactoryProductSupplier> listSupplier = productPreviewService.selectListSupplierByProductId(Long.valueOf(productId));
    				productPreviewActionForm.setListSupplier(listSupplier);
    				//Set product condition if null data
    				if (null == entProductGeneral.getProductOpenPriceFlg()) {
    					entProductGeneral.setProductOpenPriceFlg(0);
    				}
    				if (null == entProductGeneral.getProductProperSellingFlg()) {
    					entProductGeneral.setProductProperSellingFlg(0);
    				}
    				if (null == entProductGeneral.getProductOrderProductFlg()) {
    					entProductGeneral.setProductOrderProductFlg(0);
    				}
    				if (null == entProductGeneral.getProductNoReturnableFlg()) {
    					entProductGeneral.setProductNoReturnableFlg(0);
    				}
    				if (null == entProductGeneral.getProductAmbiguousImageFlg()) {
    					entProductGeneral.setProductAmbiguousImageFlg(0);
    				}
    				// BOE @rcv!Luong.Tuong 2014/07/15 #9955 : check duplicate jan code and product code
    				if (null != entProductGeneral.getProductCode() && !"".equals(entProductGeneral.getProductCode())) {
    					//if the query return exactly 1 record, it is mean product code is valid(true)
    					if (productPreviewService.countCheckInvalidDataInMatter(matterNo, entProductGeneral.getProductCode(), null) == 1) {
    						entProductGeneral.setValidProductCode(true);
    					} else {
    						//Set ProductCode flag to false (mean invalid) in case return many rows.
    						entProductGeneral.setValidProductCode(false);
    					}
    				}
    				if (null != entProductGeneral.getProductEanCode() && !"".equals(entProductGeneral.getProductEanCode())) {
    					//if the query return exactly 1 record, it is mean jan code is valid(true)
    					if (productPreviewService.countCheckInvalidDataInMatter(matterNo, null, entProductGeneral.getProductEanCode()) == 1) {
    						entProductGeneral.setValidJanCode(true);
    					} else {
    					//Set JAN flag to false (mean invalid) in case return many rows.
    						entProductGeneral.setValidJanCode(false);
    					}
    				} else {
    					entProductGeneral.setValidJanCode(true);
    				}
    				// EOE @rcv!Luong.Tuong 2014/07/15 #9955 : check duplicate jan code and product code
    				//Set product condition to form
    				productPreviewActionForm.setEntProductGeneral(entProductGeneral);
                    // BOE @rcv!Nguyen.Chuong Jul 17, 2014 #9955 : Get calibration request whole value and memo
                    EntTblFactoryCalibrationRequestWhole entCalibrationRequestWhole = calibrationService.selectCalibrationRequestWhole(matterNo);
                    productPreviewActionForm.setEntTblCalibrationRequestWhole(entCalibrationRequestWhole);
                    // EOE @rcv!Nguyen.Chuong Jul 17, 2014 #9811 : Get calibration request whole value and memo
				} else {
                    productPreviewActionForm = null;
                }
	        }
		} catch (NumberFormatException e) {
			this.getLogger().error("[loadDataBlock2] Cannot get data of selected product!");
		} catch (Exception ex) {
			this.getLogger().error("[loadDataBlock2] Error when excute action loadDataBlock2!");
			this.getLogger().error(ex.toString());
		}
		//ServletActionContext.getResponse().setStatus(400);
		return "form";
	}

	/************************************************************************
	 * <b>Description:</b><br>
	 *  get list image and thumb by productId.
	 *
	 * @author		Tran.Thanh
	 * @date		Jun 16, 2014
	 * @return		String jSon formated
	 ************************************************************************/
	@Action("/loadDataBlock4")
	public String loadDataBlock4() {
		List<EntTblFactoryProductImage> listImageAndThumb = new ArrayList<EntTblFactoryProductImage>();
		String productImageDetailJsonStr = "[]";

		if (StringUtils.isNotEmpty(productId) && StringUtils.isNumeric(productId)) {
			//Get list image of product.
			EntTblFactoryProductNew entProduct = new EntTblFactoryProductNew();
			entProduct.setProductId(Long.valueOf(productId));

			listImageAndThumb = productService.selectListImageAndThumbByProductId(entProduct);
			// BOE @rcv!Nguyen.Chuong 2014/07/29 #9955 : Verify thumb nail and exists image
			if (listImageAndThumb.size() > 0) {
				EntTblFactoryProductImage factoryProImg = listImageAndThumb.get(0);
				//verify thumb nail of first image is 140x140
				String thumbnailURL = factoryProImg.getProductThumb();
				//Check link thumbnail content HTTP to define tmp or server.
	        	if (thumbnailURL.indexOf("http") < 0) {
	        		//Create link content tmp to get correct URL.
	        		thumbnailURL = this.getRequest().getRequestURL().toString().replace(this.getRequest().getServletPath(), "/")
	        					 + thumbnailURL;
	        		//Replace incorrect char when create link.
	        		thumbnailURL = thumbnailURL.replaceAll("/./", "/");
	        	}
				factoryProImg.setValidThumbnailSize(Utils.getInstance().validateThumbnailSize(thumbnailURL));
				//Loop to check image is exist
				Iterator<EntTblFactoryProductImage> listImageAndThumbItr = listImageAndThumb.iterator();
				 while (listImageAndThumbItr.hasNext()) {
					 factoryProImg = listImageAndThumbItr.next();
					 //verify exists image
					 factoryProImg.setExistsImage(Utils.getInstance().isExistsImage(factoryProImg.getProductImage()));
				}
			}
			//EOE @rcv!Nguyen.Chuong 2014/07/29 #9955 : Verify thumb nail and exists image
			while (listImageAndThumb.size() < Constant.PRODUCT_MAX_IMAGE_LENGTH) {
				EntTblFactoryProductImage entNoImage = new EntTblFactoryProductImage();
				entNoImage.setProductImage(Constant.NO_IMAGE_URL);
				entNoImage.setProductImageDetailPath(Constant.NO_IMAGE_URL);
				entNoImage.setProductImageThumbnailPath(Constant.NO_IMAGE_URL);
				//BOE @rcv!Luong.Tuong 2014/07/15 #9955 : 	Verify thumbnail
				//True mean flag is valid.
				entNoImage.setValidThumbnailSize(true);
				entNoImage.setExistsImage(true);
				//EOE @rcv!Luong.Tuong 2014/07/15 #9955 : 	Verify thumbnail
				entNoImage.setProductThumb(Constant.NO_IMAGE_URL);
				entNoImage.setImageId("");
				listImageAndThumb.add(entNoImage);
			}
			productImageDetailJsonStr = Utils.parseObjectToJson(listImageAndThumb);
		}

		productImageDetailJsonStr = Utils.parseObjectToJson(listImageAndThumb);
		productPreviewActionForm.setFactoryProductImageDetailJsonStr(productImageDetailJsonStr);
		return "listImage";
	}

	/************************************************************************
	 * <b>Description:</b><br>
	 *  get description by product id.
	 *
	 * @author		Tran.Thanh
	 * @date		Jun 16, 2014
	 * @return		description of product.
	 * @throws 		SQLException		String
	 ************************************************************************/
	@Action("/loadDataBlock10")
	public String loadDataBlock10() throws SQLException {
		EntTblFactoryProductDescription entDescription = new EntTblFactoryProductDescription();

		if (StringUtils.isNotEmpty(productId) && StringUtils.isNumeric(productId)) {
			entDescription = productPreviewService.selectProductDescriptionByProductId(Long.valueOf(productId));
		}
		// BOE @rcv!Nguyen.Chuong Jul 23, 2014 #9955 : validate HTML.
		if (null != entDescription) {
		    if (!Utils.isValidHtml(entDescription.getDescriptionRemarks())) {
		        entDescription.setDescriptionRemarksErrorFlg(false);
		    }
		    if (!Utils.isValidHtml(entDescription.getDescriptionSummary())) {
                entDescription.setDescriptionSummaryErrorFlg(false);
            }
		    if (!Utils.isValidHtml(entDescription.getDescriptionSentence())) {
                entDescription.setDescriptionSentenceErrorFlg(false);
            }
		    if (!Utils.isValidHtml(entDescription.getDescriptionCaution())) {
                entDescription.setDescriptionCautionErrorFlg(false);
            }
		}
		//EOE @rcv!Nguyen.Chuong Jul 23, 2014 #9955 : validate HTML.
		productPreviewActionForm.setEntDescription(entDescription);

		return "listDescription";
	}
	/* EOE Function Tran.Thanh */

	/* BOE Function Dai.Luong */
	/* Start block 7 */
	/************************************************************************
	 * <b>Description:</b><br>
	 *  .
	 *
	 * @author		Luong.Dai
	 * @date		Jun 14, 2014
	 * @return		String
	 ************************************************************************/
	@Action("/loadListSelectOfProduct")
	public String loadListSelectOfProduct() {
		//Get productId from request
		String groupCode 	= this.getRequest().getParameter("groupCode");
		String brandCode 	= this.getRequest().getParameter("brandCode");
		String matterNo 	= this.getRequest().getParameter("matterNo");

		if (StringUtils.isBlank(productId)
				|| !StringUtils.isNumeric(productId)
				|| productId.length() > Constant.PRODUCT_ID_MAX_LENGTH_INPUT) {
			//Validate productId is blank, not a number and has length > 18
			return ERROR;
		}

		if (StringUtils.isBlank(groupCode)
				|| !StringUtils.isNumeric(groupCode)
				|| groupCode.length() > Constant.PRODUCT_GROUP_CODE_MAX_LENGTH) {
			//Validate productId is blank, not a number and has length > 18
			return ERROR;
		}

		if (StringUtils.isBlank(brandCode)
				|| !StringUtils.isNumeric(brandCode)
				|| brandCode.length() > Constant.PRODUCT_BRAND_CODE_MAX_LENGTH_INPUT) {
			//Validate productId is blank, not a number and has length > 18
			return ERROR;
		}

		if (StringUtils.isBlank(matterNo)
				|| !StringUtils.isNumeric(matterNo)
				|| matterNo.length() > Constant.MATTER_NO_MAX_LENGTH_INPUT) {
			//Validate productId is blank, not a number and has length > 18
			return ERROR;
		}

		//Get list select code by productId]
		List<EntTblFactoryProductSelect> lstSelect = productPreviewService.selectListSelectCodeByProductId(Long.valueOf(productId));
		//Select list product id same group
		List<Long> lstProductId = productPreviewService.selectListProductIdSameGroup(Long.valueOf(groupCode)
		                                                                            , Integer.valueOf(brandCode)
		                                                                            , Long.valueOf(matterNo));
		if (lstProductId == null || lstProductId.size() == 0) {
			return ERROR;
		}
		//Select list option value
		Map<Integer, Object> mapSelectValue = productPreviewService.selectListSelectDisplayValue(lstSelect, lstProductId);
		productPreviewActionForm.setMapSelectDisplay(mapSelectValue);
		return "selectDisplay";
	}

	/************************************************************************
	 * <b>Description:</b><br>
	 *  load product id, proper price and supplier price of product.
	 *
	 * @author		Luong.Dai
	 * @date		Jun 16, 2014
	 * @return		String
	 ************************************************************************/
	@Action("/loadProductDataWhenChangeOption")
	public String loadProductDataWhenChangeOption() {
		String lstSelectParam = this.getRequest().getParameter("selectValue");
		String brandCode = this.getRequest().getParameter("brandCode");
		String groupCode = this.getRequest().getParameter("groupCode");
		String matterNo = this.getRequest().getParameter("matterNo");

		if (lstSelectParam.isEmpty()) {
			//Cannot get list select
			return ERROR;
		}

		if (StringUtils.isBlank(productId)
				|| !StringUtils.isNumeric(productId)
				|| productId.length() > Constant.PRODUCT_ID_MAX_LENGTH_INPUT) {
			//Validate productId is blank, not a number and has length > 18
			return ERROR;
		}

		if (StringUtils.isBlank(groupCode)
				|| !StringUtils.isNumeric(groupCode)
				|| groupCode.length() > Constant.PRODUCT_GROUP_CODE_MAX_LENGTH) {
			//Validate productId is blank, not a number and has length > 18
			return ERROR;
		}

		if (StringUtils.isBlank(brandCode)
				|| !StringUtils.isNumeric(brandCode)
				|| brandCode.length() > Constant.PRODUCT_BRAND_CODE_MAX_LENGTH_INPUT) {
			//Validate productId is blank, not a number and has length > 18
			return ERROR;
		}

		if (StringUtils.isBlank(matterNo)
				|| !StringUtils.isNumeric(matterNo)
				|| matterNo.length() > Constant.MATTER_NO_MAX_LENGTH_INPUT) {
			//Validate productId is blank, not a number and has length > 18
			return ERROR;
		}
		//parse list select from lstSelect
		List<EntTblFactoryProductSelect> lstSelect = parseListSelectFormParma(lstSelectParam);
		if (lstSelect == null || lstSelect.size() == 0) {
			return ERROR;
		}
		//Get list product_id same group
		List<Long> lstProductId = productPreviewService.selectListProductIdSameGroup(Long.valueOf(groupCode)
                                                                                   , Integer.valueOf(brandCode)
                                                                                   , Long.valueOf(matterNo));
		Long idResult = productPreviewService.findProductByListSelectedOption(lstSelect, lstProductId);
		if (idResult != null) {
			//get proper price and supplier price
			EntTblFactoryProductGeneral productGeneral = productPreviewService.selectProductPriceByProductId(idResult.toString());
			productPreviewActionForm.setEntProductGeneral(productGeneral);
		}
		return "form";
	}
	/* End block 7 */

	/************************************************************************
	 * <b>Description:</b><br>
	 *  parse list select from string.
	 *  Format string: select code _ select display : select code _ select display....
	 *
	 * @author		Luong.Dai
	 * @date		Jun 16, 2014
	 * @param 		lstSelectParam						String list select
	 * @return		List<EntTblFactoryProductSelect>	list ent result
	 ************************************************************************/
	private List<EntTblFactoryProductSelect> parseListSelectFormParma(String lstSelectParam) {
		List<EntTblFactoryProductSelect> lstSelect = new ArrayList<EntTblFactoryProductSelect>();
		//Parse list to list select
		List<String> listSelectInfo = new ArrayList<String>();
		int i = 0;
		int newIndex = 0;
		int oldIndex = 0;
		while ((i = (lstSelectParam.indexOf(Constant.SPLIT_SELECT_INFO, i + 1))) > 0) {
            newIndex = i;
            listSelectInfo.add(lstSelectParam.substring(oldIndex, newIndex));
            oldIndex = i + 1;
        }
		//Add last group.
		listSelectInfo.add(lstSelectParam.substring(oldIndex, lstSelectParam.length()));
		if (listSelectInfo == null || listSelectInfo.size() == 0) {
			return null;
		}
		for (String selectInfo : listSelectInfo) {
			//Split select code and select name
			String[] select = selectInfo.split(Constant.SPLIT_SELECT_CODE_SELECT_NAME);

			//Set to entity
			EntTblFactoryProductSelect entSelect = new EntTblFactoryProductSelect();
			entSelect.setSelectCode(Integer.valueOf(select[0]));
			entSelect.setProductSelectDisplay(select[1]);
			//Add entity to list
			lstSelect.add(entSelect);
		}
		return lstSelect;
	}

	/* Start block 8 */
	/************************************************************************
	 * <b>Description:</b><br>
	 *  Get list guest input of product by product id.
	 *
	 * @author		Luong.Dai
	 * @date		Jun 13, 2014
	 * @return		String
	 ************************************************************************/
	@Action("/selectGuestInputDescription")
	public String selectGuestInputDescription() {
		if (StringUtils.isBlank(productId)
				|| !StringUtils.isNumeric(productId)
				|| productId.length() > Constant.PRODUCT_ID_MAX_LENGTH_INPUT) {
			//Validate productId is blank, not a number and has length > 18
			return ERROR;
		}
		List<EntTblFactoryProductGuestInput> lstGuestInput = productPreviewService.selectListGuestInputOfProduct(Long.valueOf(productId));
		productPreviewActionForm.setLstGuestInput(lstGuestInput);
		return "guestInput";
	}
	/* End block 8 */

	/* Start block 9 */
	/************************************************************************
	 * <b>Description:</b><br>
	 *  Select list fit model of product.
	 *
	 * @author		Luong.Dai
	 * @date		Jun 13, 2014
	 * @return		String
	 ************************************************************************/
	@Action("/selectFitModelOfProduct")
	public String selectListFitModel() {
		if (StringUtils.isBlank(productId)
				|| !StringUtils.isNumeric(productId)
				|| productId.length() > Constant.PRODUCT_ID_MAX_LENGTH_INPUT) {
			//Validate productId is blank, not a number and has length > 18
			return ERROR;
		}
		//Get list fitmodel from DB
		List<EntTblFactoryProductFitModel> lstFitModel = productPreviewService.selectListFitModelOfProduct(Long.valueOf(productId));
		//BOE @rcv!Luong.Tuong 2014/07/15 #9955 : Check master table
        Iterator<EntTblFactoryProductFitModel> lstFitModelStr = lstFitModel.iterator();
        //Declare Map for store model name
        Map<String, String> entMstModelNameMap = new HashMap<String, String>();
        //Declare Map for store maker name
        Map<String, String> entMstMakerNameMap = new HashMap<String, String>();
        //Get all model name from table master
        List<EntMstModel> entMstModelList = modelService.selectAllListModel();
        Iterator<EntMstModel> entMstModelListItr = entMstModelList.iterator();
        while (entMstModelListItr.hasNext()) {
            EntMstModel entMstModel = entMstModelListItr.next();
            //transfer to Map
            entMstModelNameMap.put(entMstModel.getModelName(), entMstModel.getModelName());
        }
        //Get all maker name from table master
        List<EntMstMaker> entMstMakerList = fitModelService.selectMakerList();
        Iterator<EntMstMaker> entMstMakerListItr = entMstMakerList.iterator();
        while (entMstMakerListItr.hasNext()) {
            EntMstMaker entMstMaker = entMstMakerListItr.next();
            //transfert to Map
            entMstMakerNameMap.put(entMstMaker.getMakerName(), entMstMaker.getMakerName());
        }
        //Through List Model Fit and get check with Set, if exist data is valid else invalid
        while (lstFitModelStr.hasNext()) {
            EntTblFactoryProductFitModel tblFactoryProdFitModel = lstFitModelStr.next();
            String resultModelName = entMstModelNameMap.get(tblFactoryProdFitModel.getFitModelModel());
            if (StringUtils.isNotBlank(resultModelName)) {
                //True is mean valid, default is false(invalid).
                tblFactoryProdFitModel.setValidModelName(true);
            }
            String resultMakerName = entMstMakerNameMap.get(tblFactoryProdFitModel.getFitModelMaker());
            if (StringUtils.isNotBlank(resultMakerName)) {
                //True is mean valid, default is false(invalid).
                tblFactoryProdFitModel.setValidMakerName(true);
            }
        }
        //EOE @rcv!Luong.Tuong 2014/07/15 #9955 : Check master table

		//Merge list fit model
		Map<String, Object> mapFitModel = mergeListFitModelByMaker(lstFitModel);
		productPreviewActionForm.setMapFitModel(mapFitModel);
		return "fitModel";
	}
	/* End block 9 */

	/* Start block 11 */
	/************************************************************************
	 * <b>Description:</b><br>
	 *  Select list link of product.
	 *
	 * @author		Luong.Dai
	 * @date		Jun 13, 2014
	 * @return		String
	 ************************************************************************/
	@Action("/selectLinkOfProduct")
	public String selectLinkOfProduct() {
		if (StringUtils.isBlank(productId)
				|| !StringUtils.isNumeric(productId)
				|| productId.length() > Constant.PRODUCT_ID_MAX_LENGTH_INPUT) {
			//Validate productId is blank, not a number and has length > 18
			return ERROR;
		}
		List<EntTblFactoryProductLink> lstLink = productPreviewService.selectLinkOfProduct(Long.valueOf(productId));
		productPreviewActionForm.setLstLink(lstLink);
		return "link";
	}
	/* End block 11 */

	/************************************************************************
	 * <b>Description:</b><br>
	 *  Merge list fit model by maker.
	 *
	 * @author		Luong.Dai
	 * @date		Jun 13, 2014
	 * @param 		lstFitModel			list fit model to merge
	 * @return		Map<String,Object>	map result
	 ************************************************************************/
	@SuppressWarnings("unchecked")
	private Map<String, Object> mergeListFitModelByMaker(List<EntTblFactoryProductFitModel> lstFitModel) {
		Map<String, Object> mapFitModel = new HashMap<String, Object>();

		int size = lstFitModel.size();
		for (int i = 0; i < size; i++) {
			EntTblFactoryProductFitModel fitModel = lstFitModel.get(i);
			if (StringUtils.isBlank(fitModel.getMakerLogo())) {
				//If maker don't have logo,
				fitModel.setMakerLogo(Constant.NO_LOGO_OF_MAKER);
			}

			//Find model in map result
			List<EntTblFactoryProductFitModel> lst = (List<EntTblFactoryProductFitModel>) mapFitModel.get(fitModel.getMakerLogo());
			if (lst == null) {
				//maker not exist in map
				lst = new ArrayList<EntTblFactoryProductFitModel>();
				//Add current fit model
				mapFitModel.put(fitModel.getMakerLogo(), lst);
			}
			lst.add(fitModel);
		}

		return mapFitModel;
	}
	/* EOE Function Dai.Luong */

	/* BOE Function Nguyen.Chuong */
	/************************************************************************
	 * <b>Description:</b><br>
	 *  #7203 Load product Id of next product and previous product.
	 *
	 * @author		Nguyen.Chuong
	 * @date		Jun 13, 2014
	 * @return		String
	 ************************************************************************/
	@Action("/loadChangeProductIdBtn")
	public String loadChangeProductIdBtn() {
	    String matterNo = this.getRequest().getParameter(Constant.PARAM_MATTER_ID);
	    String changeProductMode = this.getRequest().getParameter(Constant.PARAM_CHANGE_PRODUCT_MODE);

	    if (StringUtils.isNotEmpty(productId) && StringUtils.isNumeric(productId)
	            && StringUtils.isNotEmpty(matterNo) && StringUtils.isNumeric(matterNo)) {
	        //Get next product Id.
	        productPreviewActionForm.setNextProductId(productPreviewService.getNextOrPreviousProductId(Long.valueOf(productId)
                	                                                                   , Long.valueOf(matterNo)
                	                                                                   , changeProductMode
                	                                                                   , "ASC"));
	        //Get previous product Id.
            productPreviewActionForm.setPreviousProductId(productPreviewService.getNextOrPreviousProductId(Long.valueOf(productId)
                                                                                       , Long.valueOf(matterNo)
                                                                                       , changeProductMode
                                                                                       , "DESC"));
	    }
	    return "productId";
	}
	/************************************************************************
	 * <b>Description:</b><br>
	 *  Load list data video of product from rc_product_factory.tbl_factory_product_video.
	 *
	 * @author		Nguyen.Chuong
	 * @date		Jun 16, 2014
	 * @return		String
	 ************************************************************************/
	@Action("/loadVideoData")
	public String loadVideoData() {
	    //Validate productId
	    if (StringUtils.isNotEmpty(productId)
	            && StringUtils.isNumeric(productId)
	            && productId.length() < Constant.PRODUCT_ID_MAX_LENGTH_INPUT) {
	        try {
	            //Get list video from DB by productId
                List<EntTblFactoryProductVideo> listVideo = productEditService.selectVideoByProductId(Long.valueOf(productId));
                productPreviewActionForm.setListVideo(listVideo);
                return "video";
            } catch (Exception e) {
                this.getLogger().error("[ProductPreviesAction] An error get list video of product from DB");
                this.getLogger().error(e.toString());
                return "errorOther";
            }
	    }
	    //return error if productId is not valid
	    return "errorOther";
	};

	/************************************************************************
	 * <b>Description:</b><br>
	 *  Load list attribute of product from DB by productID and by categoryCode.
	 *
	 * @author		Nguyen.Chuong
	 * @date		Jun 16, 2014
	 * @return		String
	 ************************************************************************/
	@Action("/loadDataAttribute")
	public String loadDataAttribut() {
	    EntTblFactoryProductNew product = new EntTblFactoryProductNew();
	    //Validate productId
	    if (StringUtils.isNotEmpty(productId)
                && StringUtils.isNumeric(productId)
                && productId.length() < Constant.PRODUCT_ID_MAX_LENGTH_INPUT) {
	        product.setProductId(Long.valueOf(productId));
	        //Get category param.
	        String categoryCode = this.getRequest().getParameter("categoryCode");
	        product.setCategoryCode(categoryCode);
	        try {
	            //Get list attribute from DB by productId and by categoryCode.
                List<EntMstAttribute> listAttribute = productEditService.selectListAttributeOfProduct(product);
                productPreviewActionForm.setListAttribute(listAttribute);
                return "attribute";
            } catch (Exception e) {
                this.getLogger().error("[ProductPreviesAction] An error get list attribute of product from DB");
                this.getLogger().error(e.toString());
                return "errorOther";
            }
	    }
	    //return error if productId is not valid
	    return "errorOther";
	}
	/* EOE Function Nguyen.Chuong */

	//BOE @rcv!dau.phuong 2014/07/15 : get data for request popup
	/************************************************************************
     * <b>Description:</b><br>
     *  Load data for grid in popup edit.
     *
     * @author      Dau.Phuong
     * @date        July 15, 2014
     * @return      String
     ************************************************************************/
    @Action("/loadDataForGridPopUp")
    public String loadDataForGridPopUp() {
        EntTblFactoryCalibrationRequestIndividual calibrationRequest = productPreviewActionForm.getEntTblFactoryCalibrationRequestIndividual();
            try {
                if (StringUtils.isNotEmpty(productId) && StringUtils.isNumeric(productId)) {
                    //Get list request by productId and other filter param (if have)
                    List<EntTblFactoryCalibrationRequestIndividual> listCalibrationRequest = productPreviewService.selectCalibrationRequestIndividual(
                                                                                                                   calibrationRequest, Long.valueOf(productId));
                    productPreviewActionForm.setListCalibrationRequest(listCalibrationRequest);
                }
                return "calibrationRequest";
            } catch (Exception e) {
                this.getLogger().error("[ProductPreviesAction] [Action loadDataForGridPopUp] Cannot get data for pop up");
                this.getLogger().error(e.toString());
                return "errorOther";
            }
    }
    /************************************************************************
     * <b>Description:</b><br>
     *  Load status of popup check.
     *
     * @author      Dau.Phuong
     * @date        July 15, 2014
     * @return      String
     ************************************************************************/
    @Action("/loadCheckPopUp")
    public String loadCheckPopUp() {
        EntTblFactoryProductNew result = null;
        try {
            if (StringUtils.isNotEmpty(productId) && StringUtils.isNumeric(productId)) {
                productPreviewActionForm.setProductId(Long.valueOf(productId));
                result = productPreviewService.selectStatusCheckPreviewProduct(Long.valueOf(productId));
                productPreviewActionForm.setEntStatusPopUpCheck(result);
            }
            return "statusPopUpCheck";
        } catch (Exception e) {
            this.getLogger().error("[ProductPreviesAction] [Action loadCheckPopUp] Cannot get data of status popup check!");
            return "errorOther";
        }
    }
    /************************************************************************
     * <b>Description:</b><br>
     *  Update status of popup check.
     *
     * @author      Dau.Phuong
     * @date        July 17, 2014
     * @return      String
     ************************************************************************/
    @Action("/updateStatusPopUpCheck")
    public String updateStatusPopUpCheck() {
        String status1 = this.getRequest().getParameter("status1");
        String status2 = this.getRequest().getParameter("status2");
        String oldStatus1 = this.getRequest().getParameter("oldStatus1");
        String oldStatus2 = this.getRequest().getParameter("oldStatus2");
        String user = this.getRequest().getParameter("loginId");
        EntTblFactoryProductNew param = new EntTblFactoryProductNew();
        try {
            if (StringUtils.isNotEmpty(productId) && StringUtils.isNumeric(productId)) {
                param.setProductId(Long.valueOf(productId));
                if (StringUtils.isNotBlank(status1)) {
                    if (StringUtils.isNotBlank(oldStatus1) && StringUtils.equals(status1, oldStatus1)) {
                        param.setProductCheck1Flg(null);
                    } else {
                        param.setProductCheck1Flg(Integer.valueOf(status1));
                    }
                }
                if (StringUtils.isNotBlank(status2)) {
                    if (StringUtils.isNotBlank(oldStatus2) && StringUtils.equals(status2, oldStatus2)) {
                        param.setProductCheck2Flg(null);
                    } else {
                        param.setProductCheck2Flg(Integer.valueOf(status2));
                    }
                }
                if (StringUtils.isNotBlank(user)) {
                    param.setUpdatedUserId(user);
                }
                productPreviewService.updateStatusPopUpCheck(param);
            }
            return "updateStatusPopUpCheck";
        } catch (Exception e) {
            this.getLogger().error("[ProductPreviesAction] [Action updateStatusPopUpCheck] Cannot update status of check popup!");
            return "errorOther";
        }
    }

    /************************************************************************
     * <b>Description:</b><br>
     *  Change status of request.
     *
     * @author      Dau.Phuong
     * @date        July 18, 2014
     * @return      String
     ************************************************************************/
    @Action("/changeCorrespondRequest")
    public String changeCorresPondRequest() {
        String id = this.getRequest().getParameter(Constant.CALIBRATION_INDIVIDUAL_CODE);
        String statusRequest = this.getRequest().getParameter("statusRequest");
        String user = this.getRequest().getParameter("loginId");
        EntTblFactoryCalibrationRequestIndividual param = new EntTblFactoryCalibrationRequestIndividual();
        try {
            if (StringUtils.isNotBlank(id)) {
                param.setCalibrationRequestIndividualCode(Long.valueOf(id));
            }
            if (StringUtils.isNotBlank(statusRequest)) {
                param.setCalibrationRequestIndividualFinishFlg(Integer.valueOf(statusRequest));
            }
            if (StringUtils.isNotBlank(user)) {
                param.setCalibrationRequestIndividualFinishUserId(user);
            }
            productPreviewService.changeCorrespondRequest(param);
            return "changeCorrespondRequest";
        } catch (Exception e) {
            this.getLogger().error("[ProductPreviesAction] [Action changeCorrespondRequest] Cannot change status of request!");
            return "errorOther";
        }
    }

    /************************************************************************
     * <b>Description:</b><br>
     *  Update content of project.
     *
     * @author      Dau.Phuong
     * @date        July 21, 2014
     * @return      String
     ************************************************************************/
    @Action("/updateContentRequest")
    public String updateContentRequest() {
        String id = this.getRequest().getParameter(Constant.CALIBRATION_INDIVIDUAL_CODE);
        String editType = this.getRequest().getParameter("editType");
        String editValue = this.getRequest().getParameter("editValue");
        String user = this.getRequest().getParameter("loginId");
        EntTblFactoryCalibrationRequestIndividual param = new EntTblFactoryCalibrationRequestIndividual();
        try {
            if (StringUtils.isNotBlank(id)) {
                param.setCalibrationRequestIndividualCode(Long.valueOf(id));
            }
            if (StringUtils.isNotBlank(editType)) {
                param.setCalibrationRequestIndividualTypeCode(Integer.valueOf(editType));
            }
            if (StringUtils.isNotBlank(editValue)) {
                param.setCalibrationRequestIndividualValue(editValue);
            }
            if (StringUtils.isNotBlank(user)) {
                param.setCalibrationRequestIndividualFinishUserId(user);
            }
            productPreviewService.updateContentRequest(param);
            return "updateContentRequest";
        } catch (Exception e) {
            this.getLogger().error("[ProductPreviesAction] [Action updateContentRequest] Cannot change content of request!");
            return "errorOther";
        }
    }
    //EOE @rcv!dau.phuong 2014/07/15 : get data for request popup
    /************************************************************************
     * <b>Description:</b><br>
     *  save tab all in popup value.
     *
     * @author		Nguyen.Chuong
     * @date		Jul 17, 2014
     * @return		String
     ************************************************************************/
    @Action("/saveTabAllOrNotePopup")
    public String saveTabAllOrNotePopup() {
        EntTblFactoryCalibrationRequestWhole ent = productPreviewActionForm.getEntTblCalibrationRequestWhole();
        if (productPreviewActionForm.getTabNo() == 0) {
            return "error";
        }
        if (null == ent) {
            return "error";
        } else if (null == ent.getCalibrationRequestWholeMatterNo() || StringUtils.isEmpty(ent.getCalibrationRequestWholeValueUserId())) {
            return "error";
        }
        ent = calibrationService.saveTabAllPopup(ent, productPreviewActionForm.getTabNo());
        productPreviewActionForm.setEntTblCalibrationRequestWhole(ent);
        return "form";
    }

    /************************************************************************
     * <b>Description:</b><br>
     *  Delete selected data on popup.
     *
     * @author      Luong.Tuong
     * @date        July 21, 2014
     * @return      String
     ************************************************************************/
    @Action("/deleteValuePopUpCheck")
    public String deleteValuePopUpCheck() {
        //Validate user login
        if (!validateLoginUser()) {
            return "errorOther";
        }
         String idCalibrationArray = this.getRequest().getParameter("idCalibrationArray");
         if (StringUtils.isNotEmpty(idCalibrationArray)) {
             //Incase: Multiple selected rows, the params array will have character(,) else 1 row selected.
             if (idCalibrationArray.contains(",")) {
                 //Case: Multiple rows selected
                 String[] idCalibrationHanle = idCalibrationArray.split(",");
                 for (String idCalibrationElement : idCalibrationHanle) {
                     if (StringUtils.isBlank(idCalibrationElement) || !StringUtils.isNumeric(idCalibrationElement)) {
                         return "errorOther";
                     }
                     //Loop for get product id and delete one by one
                     EntTblFactoryCalibrationRequestIndividual calibrationRequestIndividualParams = new EntTblFactoryCalibrationRequestIndividual();
                     calibrationRequestIndividualParams.setCalibrationRequestIndividualCode(Long.parseLong(idCalibrationElement));
                     try {
                          productPreviewService.deleteCalibrationRequestIndividual(calibrationRequestIndividualParams);
                     } catch (SQLException e) {
                         this.getLogger().error("Cannot delete Calibration Request Individual ");
                         return "error";
                     }
                 }
             } else {
                 //Case, just 1 row has been selected.
                 EntTblFactoryCalibrationRequestIndividual calibrationRequestIndividualParams = new EntTblFactoryCalibrationRequestIndividual();
                 calibrationRequestIndividualParams.setCalibrationRequestIndividualCode(Long.parseLong(idCalibrationArray));
                 try {
                      productPreviewService.deleteCalibrationRequestIndividual(calibrationRequestIndividualParams);
                 } catch (Exception e) {
                     this.getLogger().error("Cannot delete Calibration Request Individual");
                     return "error";
                 }
             }
         }
         return "deleteSuccess";
    }

    /************************************************************************
     * <b>Description:</b><br>
     *  save tab all in popup value.
     *
     * @author      Nguyen.Chuong
     * @date        Jul 17, 2014
     * @return      String
     ************************************************************************/
    @Action("/insertNewRowPopupToDB")
    public String insertNewRowPopupToDB() {
        //Get insert ent value;
        EntTblFactoryCalibrationRequestIndividual ent = productPreviewActionForm.getEntTblFactoryCalibrationRequestIndividual();
        if (null == ent) {
            return "error";
        }
        //Insert and get new insert individual code into DB.
        Long newIndividualCode = calibrationService.insertNewCalibrationRequestIndividual(ent);
        ent.setCalibrationRequestIndividualCode(newIndividualCode);
        productPreviewActionForm.setEntTblFactoryCalibrationRequestIndividual(ent);
        return "form";
    }

    /************************************************************************
     * <b>Description:</b><br>
     *  count number product check 1, check 2 and total product of matter.
     *
     * @author		Luong.Dai
     * @date		Jul 30, 2014
     * @return		String
     ************************************************************************/
    @Action("/countCheckedProductInMatter")
    public String countCheckedProductInMatter() {
        try {
        	//Check matterNo, if matterNo is null, return error
        	if (productPreviewActionForm.getMatterNo() == null) {
        		return ERROR;
        	}
        	//Count checked product
        	EntTblFactoryProductNew product = productPreviewService.countTotalProductCheckInMatter(productPreviewActionForm.getMatterNo());
        	productPreviewActionForm.setEntStatusPopUpCheck(product);
        } catch (SQLException e) {
        	this.getLogger().error("Cannot count product checked in matter");
        	return ERROR;
        }
    	 
        return "form";
    }

	/**
	 * @return the productPreviewActionForm
	 */
	public ProductPreviewActionForm getProductPreviewActionForm() {
		return productPreviewActionForm;
	}

	/**
	 * @param productPreviewActionForm the productPreviewActionForm to set
	 */
	public void setProductPreviewActionForm(
			ProductPreviewActionForm productPreviewActionForm) {
		this.productPreviewActionForm = productPreviewActionForm;
	}

    /**
     * @return the productId
     */
    public String getProductId() {
        return productId;
    }

    /**
     * @param productId the productId to set
     */
    public void setProductId(String productId) {
        this.productId = productId;
    }
}
