/************************************************************************
 * File Name    ： AttributeEditAction.java
 * Author       ： Long Vu
 * Version      ： 1.0.0
 * Date Created ： 2014/01/15
 * Date Updated ： 2014/01/15
 * Description  ： attribute edit action.
 *
 ***********************************************************************/
package net.webike.RcProductFactory.action;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import net.webike.RcProductFactory.action.form.AttributeActionForm;
import net.webike.RcProductFactory.entity.EntMstAttribute;
import net.webike.RcProductFactory.entity.EntMstAttributeGroup;
import net.webike.RcProductFactory.entity.EnumAttributeType;
import net.webike.RcProductFactory.service.AttributeService;
import net.webike.RcProductFactory.util.Constant;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.apache.struts2.interceptor.SessionAware;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
/**
 * attribute edit action.
 * @author Long Vu
 * Date Created ： 2014/01/15
 */
@Scope("request")
@ParentPackage(value = "CommonInterceptor")
@Results(
    {
        // On loading default attribute edit
        @Result(name = "success", location = "attributeEdit.jsp"),
        // Return to attribute manage page.
        @Result(name = "successToManage", location = "attributeManage.jsp"),
        @Result(name = "attributeManage", type = "redirect", location = "attributeManage.html"),
        // Return error page when error other.
        @Result(name = "errorOther", type = "redirect", location = "error.html"),
        // Return to attribute edit in edit mode with error msg.
        @Result(name = "errorUpdate", type = "redirectAction", location = "attributeEdit.html",
                    params = {"attributeCode", "${attributeActionForm.attribute.attributeCode}" }),
        // Return to attribute edit in insert mode with error msg.
        @Result(name = "errorInsert", type = "redirectAction", location = "attributeEdit.html"),
        // Return error page when parameter fail
        @Result(name = "errorParams", type = "redirect", location = "error.html?error=param")
    }
)
public class AttributeEditAction extends CommonAction implements SessionAware {

    private static final long serialVersionUID = 1L;

    // session for store old list of attribute and attributeGroup.
    private Map<String, Object> session;
    // Constant
    private static final String ATTR_CODE_PARAM = "attributeCode";
    private static final int INSERT_MODE = 1;
    private static final int UPDATE_MODE = 0;
    private static final String ERROR_OTHER = "errorOther";
    private static final String ERROR_PARAMS = "errorParams";
    private static final String ERROR_UPDATE = "errorUpdate";
    private static final String ERROR_INSERT = "errorInsert";
    private static final String OTHER_UPDATE = "otherUpdate";
    private static final String ATTRIBUTE_MANAGE = "attributeManage";
    private static final String DUPLICATED_NAME = "duplicatedName";
    private static final int DUPLICATED_MODE = 3;

	// Host address
    private String hostAddress;

    // check attribute name if exist or not.
    private boolean existAttrName;
    @Autowired
    private AttributeActionForm attributeActionForm;

    @Autowired
    private AttributeService attributeService;

    // Number attribute in 1 page
    private String page;
    // Page number to get attribute data
    private String pageSize;

    /************************************************************************
     * <b>Description:</b><br>
     * default load page.
     *
     * @author      Long Vu
     * @date        Jan 15, 2014
     * @return      String
     ************************************************************************/
    @Override
    @Action("/attributeEdit")
    public String execute() {
        try {
            // Validate loginUser
            if (!validateLoginUser()) {
                return ERROR_OTHER;
            }
            initErrorMess();
            //Get insert mode or update mode
            //If update mode, return attribute code, else return -1
            //Return null wrong pramas
            Integer attributeCode = selectAttributeCode();

            //validate attributeCode
            if (null == attributeCode) {
                //Wrong param
                return ERROR_PARAMS;
            } else if (attributeCode == -1) {
                // Insert mode
                attributeActionForm.setInsertMode(INSERT_MODE);
                //Load init data for insert mode
                //Load enum attributeType
                boolean loadResult = initDataForInsertMode();
                if (!loadResult) {
                    //Wrong param
                    return ERROR_PARAMS;
                }
            } else {
                // Update mode
                attributeActionForm.setInsertMode(UPDATE_MODE);
                //Load init data for update mode
                //Load Attribute by code
                //Load Attriubte group list by code
                boolean loadResult = initDataForUpdateMode(attributeCode);
                if (!loadResult) {
                    //Wrong param
                    return ERROR_PARAMS;
                }
                //Save attribute, list attributeGroup to session
                saveInitDataToSession();
            }
        } catch (Exception e) {
            this.getLogger().error("[AttributeEditAction] An error insert or update function");
            this.getLogger().error(e.toString());
            return ERROR_OTHER;
        }

        return SUCCESS;
    }

    /************************************************************************
     * to get error msg when error occur.
     * @author      Long Vu
     * @date        2014/01/16
     ************************************************************************/
    private void initErrorMess() {
        EntMstAttribute attr = null;
        Integer msg = (Integer) session.get(Constant.SESSION_MSG_CODE);
        session.remove(Constant.SESSION_MSG_CODE);
        // get msg code to display msg.
        if (msg != null) {
            String errorMsg = "";
            if (msg == 1) {
                // check if the mode is insert.
                errorMsg = this.getText("message.success.insertFail");
            } else if (msg == 0) {
                // if the mode is edit.
                errorMsg = this.getText("message.error.updateFail");
            } else if (msg == 2) {
                // if mode is edit and data is updated by other person.
                // get user update and update time from session.
                attr = (EntMstAttribute) session.get(Constant.SESSION_ATTRIBUTE);
                // clear session.
                session.remove(Constant.SESSION_ATTRIBUTE);
                errorMsg = this.getText("message.error.othersUpdate");

                errorMsg = String.format(errorMsg, attr.getUpdatedUserId(), attr.getUpdatedOn());
            } else if (msg == DUPLICATED_MODE) {
                // get message for duplicate attribute name.
                errorMsg = this.getText("message.error.duplicateAttrName");
            }
            attributeActionForm.setUpdatedAttr(attr);
            this.getCommonForm().setErrorMessage(errorMsg);
        }
    }

    /************************************************************************
     * Check duplicate attribute name.
     * @author      hoang.ho
     * @date        2014/01/15
     * @return      String
     ************************************************************************/
    @Action(value = "/loadAjaxAttributeName", results = { @Result(name = "loadAjaxAttributeName", type = "json") })
    public String loadAjaxAttributeName() {

        // Get parameters from ajax request.
        String attrName = this.getRequest().getParameter("attrName");
        String attrCode = this.getRequest().getParameter("attrCode");

        // Prepare attribute parameter for check.
        EntMstAttribute attribute = new EntMstAttribute();
        attribute.setAttributeCode(attrCode);
        attribute.setAttributeName(attrName);

        // prepare insert mode for check.
        int isInsertMode = 0;
        if (StringUtils.isBlank(attrCode)) {
            isInsertMode = 1;
        }

        // Set result check to form and return into client.
        boolean isExistName = attributeService.checkAttrExistedName(attribute, isInsertMode);
        this.setExistAttrName(isExistName);

        return "loadAjaxAttributeName";
    }

    /************************************************************************
	 * <b>Description:</b><br>
	 *  load init for attributeManage page.
	 *
	 * @author		Nguyen.Chuong
	 * @date		Jan 15, 2014
	 * @return		String
	 ************************************************************************/
	@Action("/attributeManage")
    public String loadInitManagePage() {
        try {
            //Validate loginUser
            if (!validateLoginUser()) {
                return "errorOther";
            }
            //Get Request url
            String url = this.getRequest().getRequestURL().toString();

            if (StringUtils.isNotEmpty(url)) {
                //Remove URI
                int idx = url.lastIndexOf("/");
                setHostAddress(url.substring(0, idx));
            }
            Map<String, String> listType = EnumAttributeType.getHashMapAllType();
            attributeActionForm.setListType(listType);
            //Load message from edit
            Integer msgCode = (Integer) session.get(Constant.SESSION_MSG_CODE);
            session.remove(Constant.SESSION_MSG_CODE);
            if (msgCode != null) {
                if (msgCode == 1) {
                    attributeActionForm.setErrorMsg(this.getText("message.success.insertSuccess"));
                } else if (msgCode == 0) {
                    attributeActionForm.setErrorMsg(this.getText("message.success.updateSuccess"));
                }
            }
        } catch (Exception e) {
            this.getLogger().error("[AttributeManagerAction] An error execute function");
            this.getLogger().error(e.toString());
            return "errorOther";
        }
        return "successToManage";
    }

    /************************************************************************
     * <b>Description:</b><br>
     *  filter action for get json data of attributeManage page.
     *
     * @author		Nguyen.Chuong
     * @date		Jan 16, 2014
     * @return		String
     ************************************************************************/
	@Action(value = "attributerManagerFilter", results = { @Result(name = "list", type = "json") })
    public String loadFilterManagePage() {
        try {
            int pageSizeInt = Integer.parseInt(pageSize);
            int pageInt = Integer.parseInt(page);
            // Check missing params pageSize and pageNumber
            if (StringUtils.isEmpty(pageSize) || StringUtils.isEmpty(page)) {
                // Return page error params
                return "list";
            }

            //Get listAllBrand for init page
            attributeActionForm.setListAttribute(attributeService.selectListAttributeWithFilter(pageSizeInt, pageInt, attributeActionForm.getAttribute()));

            //Check list of brand
            attributeActionForm.setCount(attributeService.selectTotalRecordOfFilter(attributeActionForm.getAttribute()));
        } catch (SQLException sqlEx) {
            this.getLogger().error("[AttributeManagerAction] An error execute function");
            this.getLogger().error(" An error SQLException");
            this.getLogger().error(sqlEx.toString());
            return "errorOther";
        } catch (Exception e) {
            this.getLogger().error("[AttributeManagerAction] An error execute function");
            this.getLogger().error(e.toString());
            return "errorOther";
        }
        return "list";
    }


    /************************************************************************
     * <b>Description:</b><br>
     *  Load init data for mode insert.
     *  Load list attribute type
     *
     * @author		Luong.Dai
     * @date		Jan 15, 2014
     * @return		boolean
     ************************************************************************/
    private boolean initDataForInsertMode() {
        this.getLogger().debug("Start initDataForInsertMode");
        // Load list attribute type
        Map<String, String> listType = EnumAttributeType.getHashMapAllType();
        attributeActionForm.setListType(listType);
        this.getLogger().debug("End initDataForInsertMode");
        return true;
    }

    /************************************************************************
     * <b>Description:</b><br>
     *  Load init data for update mode.
     *  Load attribute value.
     *  Load attribute group list
     *
     * @author		Luong.Dai
     * @date		Jan 15, 2014
     * @param attributeCode		void
     * @return Boolean
     ************************************************************************/
    private boolean initDataForUpdateMode(Integer attributeCode) {
        this.getLogger().debug("Start initDataForUpdateModeMode");
        EntMstAttribute attribute = attributeService.selectAttributeByCode(attributeCode);
        //Validate attribute result
        if (attribute != null) {
            attributeActionForm.setAttribute(attribute);

            //Load attribute group list
            List<EntMstAttributeGroup> lstAttributeGroup = attributeService.selectAttrGroupByCode(attributeCode);

            attributeActionForm.setAttributeGroupList(lstAttributeGroup);
            Gson gson = new Gson();
            String jsonLstAttributeGroup = gson.toJson(lstAttributeGroup);
            attributeActionForm.setJsonLstAttributeGroup(jsonLstAttributeGroup);
            this.getLogger().debug("End initDataForUpdateModeMode");
            return true;
        } else {
            this.getLogger().debug("End initDataForUpdateModeMode");
            //Param attribute code is wrong
            return false;
        }
    }

    /************************************************************************
     * <b>Description:</b><br>
     *  Save attribute, attributeGroup to session.
     *
     * @author      Luong.Dai
     * @date        Jan 15, 2014        void
     ************************************************************************/
    private void saveInitDataToSession() {
        this.getLogger().debug("Start saveInitDataToSession");
        session.put(Constant.SESSION_ATTRIBUTE, attributeActionForm.getAttribute());
        session.put(Constant.SESSION_ATTRIBUTE_GROUP_LIST, attributeActionForm.getAttributeGroupList());
        this.getLogger().debug("End saveInitDataToSession");
    }

    /************************************************************************
     * <b>Description:</b><br>
     * Get insert mode or update mode.
     * If update mode, return attribute code, else return -1
     *
     * @author		Luong.Dai
     * @date		Jan 15, 2014
     * @return		int
     ************************************************************************/
    private Integer selectAttributeCode() {
        this.getLogger().debug("Start selectAttributeCode");
        //Result
        Integer attrCode = null;

        String sAttrCode = this.getRequest().getParameter(ATTR_CODE_PARAM);
        // Validate attrCode from params
        if (StringUtils.isNotEmpty(sAttrCode)) {
            if (NumberUtils.isDigits(sAttrCode)) {
                //params is a number
                attrCode = Integer.valueOf(sAttrCode);
            } else {
                this.getLogger().debug("End selectAttributeCode");
                //params not a number
                return null;
            }
        } else {
            //Insert mode
            attrCode = Integer.valueOf(-1);
        }
        this.getLogger().debug("End selectAttributeCode");
        return attrCode;
    }

    /************************************************************************
     * <b>Description:</b><br>
     * to process insert or update attribute.
     *
     * @author      Long Vu
     * @date        Jan 15, 2014
     * @return      String
     ************************************************************************/
    @Action("/insertOrUpdateAttribute")
    public String insertOrUpdateAttribute() {
        int insertMode = attributeActionForm.getInsertMode();
        try {
            // Validate loginUser
            if (!validateLoginUser()) {
                return ERROR_OTHER;
            }
            // Validate data get from server before process.
            if (!validateDataInput()) {
                return ERROR_OTHER;
            }
            // prepare data for process.
            prepareData();

            if (checkExistedName()) {
                throw new RuntimeException(DUPLICATED_NAME);
            }
            // Get user name login
            String user = this.getCommonForm().getLoginId();
            // add user to form.
            attributeActionForm.setUserId(user);
            if (insertMode == 1) {
                // process for insert.
                boolean insertStatus = processInsertAttribute();

                if (insertStatus) {
                    //Return to attribute manager
                    session.put(Constant.SESSION_MSG_CODE, INSERT_MODE);
                    return ATTRIBUTE_MANAGE;
                }
            } else {
                // process for update.
                processUpdateAttribute();
            }
        } catch (Exception e) {
            // get error msg type.
            String msgReturn = e.getLocalizedMessage();
            if (StringUtils.equals(msgReturn, ERROR_UPDATE)) {
                session.put(Constant.SESSION_MSG_CODE, UPDATE_MODE);
                return ERROR_UPDATE;
            } else if (StringUtils.equals(msgReturn, ERROR_INSERT)) {
                session.put(Constant.SESSION_MSG_CODE, INSERT_MODE);
                return ERROR_INSERT;
            } else if (StringUtils.equals(msgReturn, OTHER_UPDATE)) {
                session.put(Constant.SESSION_MSG_CODE, 2);
                session.put(Constant.SESSION_ATTRIBUTE, attributeActionForm.getUpdatedAttr());
                return ERROR_UPDATE;
            } else if (StringUtils.equals(msgReturn, DUPLICATED_NAME)) {
                if (insertMode == 1) {
                    session.put(Constant.SESSION_MSG_CODE, DUPLICATED_MODE);
                    return ERROR_INSERT;
                } else {
                    session.put(Constant.SESSION_MSG_CODE, DUPLICATED_MODE);
                    return ERROR_UPDATE;
                }
            } else {
                return ERROR_OTHER;
            }
        }
        if (insertMode == 1) {
            session.put(Constant.SESSION_MSG_CODE, INSERT_MODE);
        } else {
            session.put(Constant.SESSION_MSG_CODE, UPDATE_MODE);
        }
        return "attributeManage";
    }

    /************************************************************************
     * <b>Description:</b><br>
     * check name is exist or note.
     *
     * @author      Long Vu
     * @date        Jan 15, 2014
     * @return      boolean
     ************************************************************************/
    private boolean checkExistedName() {
        this.getLogger().debug("Start checkExistedName");
        boolean result = false;
        int insertMode = attributeActionForm.getInsertMode();
        try {
            result = attributeService.checkAttrExistedName(attributeActionForm.getAttribute(), insertMode);
        } catch (Exception e) {
            this.getLogger().error("[AttributeEditAction] An error occure in checkExistedName function");
            this.getLogger().error(e.toString());
        }
        this.getLogger().debug("End checkExistedName");
        return result;
    }

    /************************************************************************
     * <b>Description:</b><br>
     * check the input data is valid or not.
     * Check null attributeName and attribute_length_limit.
     *
     * @author      Long Vu
     * @date        Jan 15, 2014
     * @return      boolean
     ************************************************************************/
    private boolean validateDataInput() {
        this.getLogger().debug("Start validateDataInput");
        // get attribute object to validate field.
        EntMstAttribute attribute = attributeActionForm.getAttribute();
        String attrName = attribute.getAttributeName();
        String attrType = attribute.getAttributeType();
        String attrLengthLimit = attribute.getAttributeLengthLimit();
        // BOE #6634 No.40 Hoang.Ho 2014/02/18 Validate length
        String attrRegEx =  attribute.getAttributeRegexp();
        // EOE #6634 No.40 Hoang.Ho 2014/02/18 Validate length
        String attrShowFlg =  attribute.getAttributeShowFlg();
        String attrDelFlg =  attribute.getAttributeDelFlg();

        // BOE #6634 No.40 Hoang.Ho 2014/02/18 Cut overflow length of Attribute Name and attrRegEx
        // if name or length limit is null or empty then return false.
        // else cut attribute-name at character = 256.
        if (StringUtils.isEmpty(attrName)) {
            return false;
        } else {
            int length = attrName.length();
            int subLength = length;
            if (length > Constant.ATTRIBUTE_NAME_LENGTH) {
                subLength = Constant.ATTRIBUTE_NAME_LENGTH;
            }
            attrName = attrName.substring(0, subLength);
            // set into form.
            attribute.setAttributeName(attrName);
        }
        // cut attribute-regex.expression at character = 256.
        if (!StringUtils.isEmpty(attrRegEx)) {
            int length = attrRegEx.length();
            int subLength = length;
            if (length > Constant.ATTRIBUTE_REGEXP_LENGTH) {
                subLength = Constant.ATTRIBUTE_REGEXP_LENGTH;
            }
            attrRegEx = attrRegEx.substring(0, subLength);
            // set into form.
            attribute.setAttributeRegexp(attrRegEx);
        }
        // EOE #6634 No.40 Hoang.Ho 2014/02/18 Cut overflow length of Attribute Name  and attrRegEx

        //type = FLAG: lengthLimit = 0
        if (StringUtils.isNotEmpty(attrType)) {
            // In case of attribute type is not flag and attributeLengthLimit is null or empty.
            // the return false.
            if (!attrType.trim().equals(Constant.TYPE_FLAG)
            && StringUtils.isEmpty(attrLengthLimit)) {
                return false;
            } else {
                // In case of attribute type is not flag and attributeLengthLimit is not number.
                if (!attrType.trim().equals(Constant.TYPE_FLAG)
                && !StringUtils.isNumeric(attrLengthLimit)) {
                    return false;
                }
            }
        } else {
            return false;
        }

        // check if show flag is null then set it to 0.
        if (StringUtils.isEmpty(attrShowFlg)) {
            attribute.setAttributeShowFlg("0");
        }
        // check if del Flag is null then set it to 0.
        if (StringUtils.isEmpty(attrDelFlg)) {
            attribute.setAttributeDelFlg("0");
        }

        this.getLogger().debug("End validateDataInput");
        return true;
    }
    /************************************************************************
     * <b>Description:</b><br>
     * prepare data from form.
     * @author      hoang.ho
     * @date        2014/01/16
     * @return      boolean
     ************************************************************************/
    private boolean prepareData() {
        this.getLogger().debug("Start prepareData");
        try {
            // get json data of list attributes group
            String jsonLstAttributeGroup =  attributeActionForm.getJsonLstAttributeGroup();

            Gson gson = new Gson();
            // convert string json to list.
            List<EntMstAttributeGroup> attributeGroupList = gson.fromJson(
                jsonLstAttributeGroup, new TypeToken<List<EntMstAttributeGroup>>() { } .getType());

            attributeActionForm.setAttributeGroupList(attributeGroupList);
        } catch (Exception e) {
            this.getLogger().error("[AttributeEditAction - prepareData] An error execute function");
            this.getLogger().error(e.toString());
        }
        this.getLogger().debug("End prepareData");
        return true;
    }

    /************************************************************************
     * <b>Description:</b><br>
     * to process insert attribute.
     *
     * @author      Long Vu
     * @date        Jan 15, 2014
     * @return      boolean
     ************************************************************************/
    private boolean processInsertAttribute() {
        this.getLogger().debug("Start processInsertAttribute");
        boolean result = false;
        result = attributeService.insertNewAttribute(attributeActionForm.getUserId()
                                                                    , attributeActionForm.getAttribute()
                                                                    , attributeActionForm.getAttributeGroupList());
        this.getLogger().debug("End processInsertAttribute");
        return result;
    }

    /************************************************************************
     * <b>Description:</b><br>
     * to process update attribute.
     *
     * @author      Long Vu
     * @date        Jan 15, 2014
     ************************************************************************/
    private void processUpdateAttribute() {
        this.getLogger().debug("Start processUpdateAttribute");
        EntMstAttribute attr;
        attr = checkEdited();
        if (attr != null) {
            attributeActionForm.setUpdatedAttr(attr);
            throw new RuntimeException(OTHER_UPDATE);
        }
        processDataForUpdate();
        attributeService.processUpdateDataAttrGroup(attributeActionForm.getAttribute()
                                                   , attributeActionForm.getDeletedCodeList()
                                                   , attributeActionForm.getAttrGroupInsertdList()
                                                   , attributeActionForm.getAttrGroupUpdatedList()
                                                   , attributeActionForm.getUserId());
        this.getLogger().debug("End processUpdateAttribute");
    }

    /************************************************************************
     * <b>Description:</b><br>
     * to get data list for insert, udpate, delete.
     *
     * @author      Long Vu
     * @date        Jan 15, 2014
     ************************************************************************/
    @SuppressWarnings("unchecked")
    private void processDataForUpdate() {
        this.getLogger().debug("Start processDataForUpdate");
        List<EntMstAttributeGroup> updatedList = new ArrayList<EntMstAttributeGroup>();
        List<EntMstAttributeGroup> insertedList = new ArrayList<EntMstAttributeGroup>();
        StringBuilder sb = new StringBuilder();
        List<EntMstAttributeGroup> oldAttrGroupList = (List<EntMstAttributeGroup>) session.get(Constant.SESSION_ATTRIBUTE_GROUP_LIST);
        // remove session after get data.
        session.remove(Constant.SESSION_ATTRIBUTE_GROUP_LIST);
        int i;
        if (attributeActionForm.getAttributeGroupList() != null) {
            for (EntMstAttributeGroup currentObj : attributeActionForm.getAttributeGroupList()) {
                i = 0;
                // check if the old is empty then set new list to insert.
                if (oldAttrGroupList.size() == 0) {
                    insertedList.add(currentObj);
                }
                for (EntMstAttributeGroup oldObj : oldAttrGroupList) {
                    i++;
                    // check for data update.
                    if (currentObj.getAttributeGroupCode().equals(oldObj.getAttributeGroupCode())) {
                        updatedList.add(currentObj);
                        break;
                    }
                    // check for data insert.
                    if (i == oldAttrGroupList.size()) {
                        if (!currentObj.getAttributeGroupCode().equals(oldObj.getAttributeGroupCode())) {
                            insertedList.add(currentObj);
                        }
                    }
                }
            }
        }
        // check for data delete.
        for (EntMstAttributeGroup oldObj : oldAttrGroupList) {
            i = 0;
            // check if updated list size is 0 then set oldlist to delete.
            if (updatedList.size() == 0) {
                sb.append(oldObj.getAttributeGroupCode());
                sb.append(",");
            }
            for (EntMstAttributeGroup updateObj : updatedList) {
                i++;
                if (oldObj.getAttributeGroupCode().equals(updateObj.getAttributeGroupCode())) {
                    break;
                }
                if (i == updatedList.size()) {
                    if (!oldObj.getAttributeGroupCode().equals(updateObj.getAttributeGroupCode())) {
                        sb.append(oldObj.getAttributeGroupCode());
                        sb.append(",");
                    }
                }
            }
        }
        if (sb.toString().length() > 0) {
            // remove "," at the last character
            attributeActionForm.setDeletedCodeList(sb.toString().substring(0, sb.toString().length() - 1));
        }
        attributeActionForm.setAttrGroupInsertdList(insertedList);
        attributeActionForm.setAttrGroupUpdatedList(updatedList);
        this.getLogger().debug("End processDataForUpdate");
    }

    /************************************************************************
     * <b>Description:</b><br>
     * to check if there is someone has edited before.
     *
     * @author      Long Vu
     * @date        Jan 15, 2014
     * @return      EntMstAttribute
     ************************************************************************/
    private EntMstAttribute checkEdited() {
        this.getLogger().debug("Start checkEdited");
        int attrCode;
        if (NumberUtils.isDigits(attributeActionForm.getAttribute().getAttributeCode())) {
            attrCode = Integer.parseInt(attributeActionForm.getAttribute().getAttributeCode());
            EntMstAttribute attr = attributeService.selectAttributeByCode(attrCode);
            EntMstAttribute oldAttr = (EntMstAttribute) session.get(Constant.SESSION_ATTRIBUTE);
            // check if different update id or update date then return different object.
            if (!StringUtils.equals(attr.getUpdatedUserId(), oldAttr.getUpdatedUserId())
                || !StringUtils.equals(attr.getUpdatedOn(), oldAttr.getUpdatedOn())) {
                return attr;
            }
        } else {
            try {
                throw new Exception(ERROR_OTHER);
            } catch (Exception e) {
                this.getLogger().error("[AttributeEditAction] An error occur in checkEdited function");
            }
        }
        this.getLogger().debug("End checkEdited");
        return null;
    }

    /**
     * @return the attributeActionForm
     */
    public AttributeActionForm getAttributeActionForm() {
        return attributeActionForm;
    }

    /**
     * @param attributeActionForm the attributeActionForm to set
     */
    public void setAttributeActionForm(AttributeActionForm attributeActionForm) {
        this.attributeActionForm = attributeActionForm;
    }

    @Override
    public void setSession(Map<String, Object> session) {
        this.session = session;
    }

    /**
     * @return the hostAddress
     */
    public String getHostAddress() {
        return hostAddress;
    }

    /**
     * @param hostAddress the hostAddress to set
     */
    public void setHostAddress(String hostAddress) {
        this.hostAddress = hostAddress;
    }

    /**
     * @return the existAttrName
     */
    public boolean isExistAttrName() {
        return existAttrName;
    }

    /**
     * @param existAttrName the existAttrName to set
     */
    public void setExistAttrName(boolean existAttrName) {
        this.existAttrName = existAttrName;
    }

    /**
     * @return the page
     */
    public String getPage() {
        return page;
    }

    /**
     * @param page the page to set
     */
    public void setPage(String page) {
        this.page = page;
    }

    /**
     * @return the pageSize
     */
    public String getPageSize() {
        return pageSize;
    }

    /**
     * @param pageSize the pageSize to set
     */
    public void setPageSize(String pageSize) {
        this.pageSize = pageSize;
    }
}
