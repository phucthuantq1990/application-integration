/************************************************************************
 * file name	： ProductEditAction.java
 * author		： Le.Dinh
 * version		： 1.0.0
 * date created	： 11 Feb 2014
 * date updated	： 11 Feb 2014
 * description	： process for all product
 *
 ***********************************************************************/
package net.webike.RcProductFactory.action;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Type;
import java.net.URLDecoder;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.webike.RcProductFactory.action.form.ProductEditActionForm;
import net.webike.RcProductFactory.entity.EntMstAttribute;
import net.webike.RcProductFactory.entity.EntMstAttributeGroup;
import net.webike.RcProductFactory.entity.EntMstBrand;
import net.webike.RcProductFactory.entity.EntMstCategory;
import net.webike.RcProductFactory.entity.EntMstFactoryMatter;
import net.webike.RcProductFactory.entity.EntMstFactoryMatterNew;
import net.webike.RcProductFactory.entity.EntMstFactoryUserTmp;
import net.webike.RcProductFactory.entity.EntMstLinkReason;
import net.webike.RcProductFactory.entity.EntMstMaker;
import net.webike.RcProductFactory.entity.EntMstModel;
import net.webike.RcProductFactory.entity.EntMstNouki;
import net.webike.RcProductFactory.entity.EntMstProductSupplierStatus;
import net.webike.RcProductFactory.entity.EntMstSiire;
import net.webike.RcProductFactory.entity.EntMstSyouhinSearch;
import net.webike.RcProductFactory.entity.EntMstSyouhinSelect;
import net.webike.RcProductFactory.entity.EntReplaceElement;
import net.webike.RcProductFactory.entity.EntTblBrandConditionRule;
import net.webike.RcProductFactory.entity.EntTblFactoryProductDescription;
import net.webike.RcProductFactory.entity.EntTblFactoryProductFitModel;
import net.webike.RcProductFactory.entity.EntTblFactoryProductGeneral;
import net.webike.RcProductFactory.entity.EntTblFactoryProductGuestInput;
import net.webike.RcProductFactory.entity.EntTblFactoryProductImage;
import net.webike.RcProductFactory.entity.EntTblFactoryProductLink;
import net.webike.RcProductFactory.entity.EntTblFactoryProductNew;
import net.webike.RcProductFactory.entity.EntTblFactoryProductSelect;
import net.webike.RcProductFactory.entity.EntTblFactoryProductSupplier;
import net.webike.RcProductFactory.entity.EntTblFactoryProductVideo;
import net.webike.RcProductFactory.entity.EnumProductConditionFlg;
import net.webike.RcProductFactory.entity.EnumRegistrationFlg;
import net.webike.RcProductFactory.entity.ImageUploadKenDo;
import net.webike.RcProductFactory.service.BrandService;
import net.webike.RcProductFactory.service.FitModelService;
import net.webike.RcProductFactory.service.MatterService;
import net.webike.RcProductFactory.service.ModelService;
import net.webike.RcProductFactory.service.ProductEditService;
import net.webike.RcProductFactory.service.ProductService;
import net.webike.RcProductFactory.service.SettingService;
import net.webike.RcProductFactory.service.UserTmpService;
import net.webike.RcProductFactory.util.Constant;
import net.webike.RcProductFactory.util.Utils;

import org.apache.commons.lang.StringUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.apache.struts2.dispatcher.multipart.MultiPartRequestWrapper;
import org.apache.struts2.interceptor.SessionAware;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.transaction.UnexpectedRollbackException;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

/* BOE global process by  Dai */
/* EOE global process by  Dai */

/* BOE global process by  Hoang */
/* EOE global process by  Hoang */

/**
 * class process all for products.
 */
@ParentPackage(value = "CommonInterceptor")
@Results({
		@Result(name = "success", location = "productEdit.jsp"),
	    @Result(name = "deleteSuccess" , type = "json",
        params = {"contentType", "text/html", "root", "responeMapAjaxDeleData" , "ignoreHierarchy", "true" }),
		//Return json
        @Result(name = "list", type = "json"),
        @Result(name = "errorOther", type = "redirect", location = "error.html"),
        @Result(name = "errorPermission", type = "redirect", location = "error.html?error=noPermission"),
	    @Result(name = "readImage" , type = "json",
        params = {"contentType", "text/html", "root", "form.listImageSentenceJson" , "ignoreHierarchy", "true" }),
	    @Result(name = "thumbnailUrl" , type = "json",
        params = {"contentType", "text/html", "root", "form.thumbnailUrl" , "ignoreHierarchy", "true" }),
	    @Result(name = "upLoadImage" , type = "json",
        params = {"contentType", "text/html", "root", "form.imageUploadKenDo" , "ignoreHierarchy", "true" }),
	    @Result(name = "brandInfo" , type = "json",
        params = {"contentType", "text/html", "root", "form.brandInfo" , "ignoreHierarchy", "true" }),
		@Result(name = "processSaveData" , type = "json",
        params = {"contentType", "text/html", "root", "responeMapAjaxSaveData" , "ignoreHierarchy", "true" }),
        //BOE #7363 Tran.Thanh : return after save process
        @Result(name = "savePopupBlock15Success" , type = "json",
        params = {"contentType", "text/html", "root", "form.msgCode" , "ignoreHierarchy", "true" }),
        //EOE #7363 Tran.Thanh : return after save process
        @Result(name = "countReleaseProduct" , type = "json",
        params = {"contentType", "text/html", "root", "form.numberReleaseProduct" , "ignoreHierarchy", "true" }),
		})
public class ProductEditAction extends CommonAction  implements SessionAware {
	/* serialVersionUID. */
	private static final long serialVersionUID = -7501314786361633145L;

	private static final int LIST_FIT_MODEL = 1;
	private static final int LIST_PRODUCT_FIT_MODEL = 2;
	private static final int FACTORY_MATTER = 3;

	/* static variable. */
	/* BOE global process by  Dinh */
    // session for store old list of category_attributes.
    private Map<String, Object> session;
	/* EOE global process by  Dinh */

	/* BOE global process by  Thanh */
	/* EOE global process by  Thanh */

	/* BOE global process by  Dai */
	/* EOE global process by  Dai */

	/* services. */
	@Autowired
	private ProductEditService productEditService;
	/* BOE global process by  Dinh */
    @Autowired
    private FitModelService fitModelService;
    @Autowired
    private BrandService brandService;
    @Autowired
    private ModelService modelService;
	/* EOE global process by  Dinh */

	/* BOE global process by  Thanh */
	@Autowired
	private ProductService productService;
	@Autowired
	private MatterService matterService;
	//private MstFactoryMatterMapper mstFactoryMatterMapper;

	@Autowired
	private SettingService settingService;
    @Autowired
    private UserTmpService userTmpService;
	/* EOE global process by  Thanh */

	/* BOE global process by  Dai */
	/* EOE global process by  Dai */

	/* result variable */
	@Autowired
	private ProductEditActionForm form;
	/* BOE global process by  Dinh */
	private File file;
	private String fileFileName;
	private Map<Object, Object> responeMapAjaxSaveData;
	private Map<Object, Object> responeMapAjaxDeleData;
	/* EOE global process by  Dinh */

	/* BOE global process by  Thanh */
	/* EOE global process by  Thanh */

	/* BOE global process by  Dai */
	/* EOE global process by  Dai */

	@Override
	@Action("/productEdit")
	public String execute() {
        if (!validateLoginUser()) {
            return "errorOther";
        }
        try {
        Boolean initResult = initProductAndMode();
        if (!initResult) {
            //Cannot load init, show error page
            return "errorOther";
        }
        // Check permission of authority
        if (this.getCommonForm().getLoginAuthority().contains("LIMITED_USER_ID")) {
			//form.getProductMatterNo()
        	if (form.getProductMatterNo() != null) {
        	    //BOE Nguyen.Chuong 2014/04/08 modify to call service
        		//EntMstFactoryMatter entMatter = matterService.selectDetailMatterByMatterNo(form.getProductMatterNo().toString());
        		EntMstFactoryMatterNew entMatter = matterService.selectFactoryMatterByMatterCode(form.getProductMatterNo());
        		//EntMstFactoryMatterNew entMatter = mstFactoryMatterMapper.selectFactoryMatterByMatterCode(form.getProductMatterNo());
                //EOE Nguyen.Chuong 2014/04/08 modify to call service
        		if (entMatter == null) {
        			return "errorOther";
        		} else {
        			if (this.getCommonForm().getLoginId() != null) {
            			if (!entMatter.getMatterChargeUserId().equals(this.getCommonForm().getLoginId())) {
                			return "errorPermission";
                		}
            		}
        		}
        	}
		}
        Long productId = form.getProductId();
        /* BOE global process by  Dinh */
        // set MsgString
        //BOE Tran.Thanh 2014/05/10 : remove function checkMsgString to fixbug
//        checkMsgString();
        //BOE Tran.Thanh 2014/05/10 : remove function checkMsgString to fixbug

		/* EOE global process by  Dinh */

		/* BOE global process by  Thanh */
		/* EOE global process by  Thanh */

		/* BOE global process by  Dai */
		/* EOE global process by  Dai */

		/* BOE Block1 */
		/* BOE global process by  Dinh */
		initListAllProductSameGroud(productId);
		/* EOE global process by  Dinh */

		/* BOE global process by  Thanh */
		/* EOE global process by  Thanh */

		/* BOE global process by  Dai */
		/* EOE global process by  Dai */

		/* BOE global process by  Hoang */
		/* EOE global process by  Hoang */
		/* EOE Block1 */

		/* BOE Block2 */
		initListImageThumb(productId);
		/* EOE Block2 */

		/* BOE Block3 */
		initListUserInformation(productId);
		/* EOE Block3 */

		/* BOE Block4 */
		loadInitAttribute();
		/* EOE Block4 */

		/* BOE Block4 */
		/* EOE Block4 */

		/* BOE Block5 */
		loadInitProductLink(productId);
		loadInitMstLinkReason();
		/* EOE Block5 */

		/* BOE Block6 */
		initProductVideo(productId);
		/* EOE Block6 */

		/* BOE Block7 */
		loadInitProductDescription(productId);
		/* EOE Block7 */

		/* BOE Block8 */
		//Load init data for block 8
		loadInitProductGuestInputInfo(productId);
		/* EOE Block8 */

		/* BOE Block9 */
		loadInitProductPriceInfo(productId);
		/* EOE Block9 */

		/* BOE Block10 */
	    //Load init for block 10
        loadInitCategory(productId);
		/* EOE Block10 */

		/* BOE Block11 */
        initFitModel(productId);
		/* EOE Block11 */

		/* BOE Block12 */
        initSupplier(productId);
		/* EOE Block12 */

		/* BOE Block13 */
		initReleaseDateAndMakerSituation(productId);
		/* EOE Block13 */

		/* BOE Block14 */
		/* EOE Block14 */

		/* BOE Block15 */
		loadInitForGridProduct();
		/* EOE Block15 */

		/*BOE Nguyen.Chuong #7205 2014/04/17: load list user to create user filter in main grid*/
        //Get list user
        List<EntMstFactoryUserTmp> listCommonUser = userTmpService.selectListAllUser();
        this.getCommonForm().setListCommonUser(listCommonUser);
        /*EOE Nguyen.Chuong #7205 2014/04/17: load list user to create user filter in main grid*/
        } catch (SQLException e) {
            this.getLogger().error("[ProductEditAction] Error when query SQL at execute()");
        }
		return SUCCESS;
	}

	/* BOE Block1 */
	/************************************************************************
	 * <b>Description:</b><br>
	 *  initListAllProductSameGroud.
	 * @param		productId	Long
	 * @author		Le.Dinh
	 * @date		21 Feb 2014
	 * @throws		SQLException	SQLException
	 ************************************************************************/
	void initListAllProductSameGroud(Long productId) throws SQLException {
		List<EntTblFactoryProductNew> listProductSameGroup;
		EntTblFactoryProductGeneral productGen = null;
		session.remove("listProductSameGroup");
		if (productId != null && productId > 0) {
			productGen = productEditService.selectProductGeneralValueByProductId(String.valueOf(productId));
			listProductSameGroup = productEditService.selectListProductSameGroupNoSelect(productGen);
			 // set setListProductSameGroup to session to proccess for button Delete product (block1)
			session.put("listProductSameGroup", listProductSameGroup);
		}
	}
	/* BOE global process by  Dinh */
	/************************************************************************
	 * <b>Description:</b><br>
	 *  getAjaxInforBrandByBrandCode.
	 *
	 * @author		Le.Dinh
	 * @date		20 Feb 2014
	 * @return		String
	 * @throws 		SQLException SQLException
	 ************************************************************************/
	@Action("/productDelete")
	public String productDelete() throws SQLException {
	    //Validate user login
        if (!validateLoginUser()) {
            return "errorOther";
        }
        String productMatterNo = this.getRequest().getParameter("matterNo");
        if (StringUtils.isBlank(productMatterNo) || !StringUtils.isNumeric(productMatterNo)) {
            return "errorOther";
        }
        Long matterNo = Long.parseLong(productMatterNo);
	    String loginId = this.getCommonForm().getLoginId();
		@SuppressWarnings("unchecked")
		List<EntTblFactoryProductNew> listProductSameGroup = (List<EntTblFactoryProductNew>) session.get("listProductSameGroup");

		boolean flgResultDelete = productEditService.deleteProducts(listProductSameGroup, matterNo, loginId);
		//initResponeMapAjaxDeleData
		initResponeMapAjaxDeleteData(flgResultDelete);
		return "deleteSuccess";
	}
	/************************************************************************
	 * <b>Description:</b><br>
	 *  initResponeMapAjaxDeleData.
	 * @author		Le.Dinh
	 * @date		6 Mar 2014
	 * @param 		flgResultDelete	boolean
	 ************************************************************************/
	private void initResponeMapAjaxDeleteData(boolean flgResultDelete) {
		String msgCode = "0";
		String msgString = getText("message.error.deleteFail");
		if (flgResultDelete) {
			msgCode = "1";
			msgString = getText("message.error.deleteSuccess");
		}
		responeMapAjaxDeleData = new HashMap<Object, Object>();
		responeMapAjaxDeleData.put(Constant.SESSION_MSG_CODE, msgCode);
		responeMapAjaxDeleData.put(Constant.MSG_STRING, msgString);
	}
	/* EOE global process by  Dinh */

	/* BOE global process by  Thanh */
	/* EOE global process by  Thanh */

	/* BOE global process by  Dai */
	/* EOE global process by  Dai */

	/* BOE global process by  Hoang */
	/* EOE global process by  Hoang */

	/************************************************************************
	 * <b>Description:</b><br>
	 *  init mode for block 1.
	 *  Load product, set mode (edit, new) for block.
	 *
	 * @author		Luong.Dai
	 * @date		Feb 20, 2014
	 * @return		Integer
	 ************************************************************************/
	private boolean initProductAndMode() {
	    //when load init fail, result = false;
        Boolean result = true;
        /* BOE global process by  Dinh */
        Long productIdFinal = (long) 0;
        String productIdRequest = this.getRequest().getParameter("productId");
        String productIdDuplicate = this.getRequest().getParameter("productIdDuplicate");
        String productMatterNo = this.getRequest().getParameter("matterNo");

        // if productIdRequest && productIdDuplicate is Blank
        if (StringUtils.isBlank(productIdRequest) && StringUtils.isBlank(productIdDuplicate)) {
            productIdFinal = (long) -1;
            //Set mode new
            form.setMode(Constant.NEW_MODE);
        }

        // Mode is new => when productIdRequest is blank  && productIdDuplicate is not blank
        if (StringUtils.isBlank(productIdRequest) && StringUtils.isNotBlank(productIdDuplicate)) {
        	productIdFinal = Long.parseLong(productIdDuplicate);
            //Set mode new
            form.setMode(Constant.NEW_MODE);
            form.setProductIdDupliCate(productIdFinal);
        }

        // Mode is edit = when productIdRequest is not blank
        if (StringUtils.isNotBlank(productIdRequest)) {
        	productIdFinal =  Long.parseLong(productIdRequest);
            //Set mode edit
            form.setMode(Constant.EDIT_MODE);
        }

        EntTblFactoryProductNew product = new EntTblFactoryProductNew();
        if (productIdFinal != null && productIdFinal > 0) {
            //Get product form DB
            product = productEditService.checkExistProduct(productIdFinal);
        }

        if (product == null) {
            //Product not exists in DB, return error page
            result = false;
        } else {
            // set matter No
            if (product.getProductMatterNo() == null || product.getProductMatterNo() < 0) {
                if (StringUtils.isEmpty(productMatterNo)) {
                    return false;
                } else {
                    form.setProductMatterNo(Long.valueOf(productMatterNo));
                    product.setProductMatterNo(Long.valueOf(productMatterNo));
                }
            } else {
                form.setProductMatterNo(product.getProductMatterNo());
            }
            //Product exists in DB, set value to form
            form.setProduct(product);
            form.setProductJson(Utils.parseObjectToJson(product)); // Add by Tran.Thanh 20140224
            //Set productId to form
            form.setProductId(productIdFinal);
        }

        return result;
	}

	/************************************************************************
	 * <b>Description:</b><br>
	 *  validate Block 3.
	 *
	 * @author		Tran.Thanh
	 * @date		3 Mar 2014
	 * @param 		form ProductEditActionForm
	 * @return		int
	 ************************************************************************/
	int validateBlock3(ProductEditActionForm form) {
		int errorCheckFlg = 1;
		Integer brandCode = form.getFactoryProductGeneral().getProductBrandCode();
        String brandCodeStr = "";
        if (brandCode != null) {
        	brandCodeStr = brandCode.toString();
        }
        if (brandCodeStr.length() > Constant.PRODUCT_BRAND_CODE_WARNING_LENGTH
        	|| form.getFactoryProductGeneral().getProductName().length() > Constant.PRODUCT_NAME_WARNING_LENGTH
        	|| form.getFactoryProductGeneral().getProductCode().length() > Constant.PRODUCT_CODE_WARNING_LENGTH
        	|| form.getFactoryProductGeneral().getProductEanCode().length() > Constant.PRODUCT_EAN_CODE_WARNING_LENGTH
        	|| form.getProductDescription().getDescriptionSummary().length() > Constant.DESCRIPTION_SUMMARY_WARNING_LENGTH
        	|| form.getProductDescription().getDescriptionCaution().length() > Constant.DESCRIPTION_CAUTION_WARNING_LENGTH
        	|| form.getProductDescription().getDescriptionRemarks().length() > Constant.DESCRIPTION_REMARKS_WARNING_LENGTH) {
        	form.setMsgCode(0);
        	return -1;
        } else if (brandCodeStr.length() == Constant.PRODUCT_EDIT_PRODUCT_NULL
        		|| form.getFactoryProductGeneral().getProductName().length() == Constant.PRODUCT_EDIT_PRODUCT_NULL
        		/*|| form.getFactoryProductGeneral().getProductCode().length() == Constant.PRODUCT_EDIT_PRODUCT_NULL
        		|| form.getFactoryProductGeneral().getProductEanCode().length() == Constant.PRODUCT_EDIT_PRODUCT_NULL*/) {
        	form.setMsgCode(0);
        	return -1;
        } else if (((brandCodeStr.length() > Constant.PRODUCT_BRAND_CODE_MAX_LENGTH_INPUT)
        		&& (brandCodeStr.length() <= Constant.PRODUCT_BRAND_CODE_WARNING_LENGTH))
        	|| ((form.getFactoryProductGeneral().getProductName().length() > Constant.PRODUCT_NAME_MAX_LENGTH_INPUT)
        		&& (form.getFactoryProductGeneral().getProductName().length() <= Constant.PRODUCT_NAME_WARNING_LENGTH))
        	|| ((form.getFactoryProductGeneral().getProductCode().length() > Constant.PRODUCT_CODE_LENGTH)
            	&& (form.getFactoryProductGeneral().getProductCode().length() <= Constant.PRODUCT_CODE_WARNING_LENGTH))
            || ((form.getFactoryProductGeneral().getProductEanCode().length() > Constant.PRODUCT_EAN_CODE_LENGTH)
                && (form.getFactoryProductGeneral().getProductEanCode().length() <= Constant.PRODUCT_EAN_CODE_WARNING_LENGTH))
            || ((form.getProductDescription().getDescriptionSummary().length() > Constant.DESCRIPTION_SUMMARY_LENGTH)
                && (form.getProductDescription().getDescriptionSummary().length() <= Constant.DESCRIPTION_SUMMARY_WARNING_LENGTH))
            || ((form.getProductDescription().getDescriptionCaution().length() > Constant.DESCRIPTION_CAUTION_LENGTH)
                && (form.getProductDescription().getDescriptionCaution().length() <= Constant.DESCRIPTION_CAUTION_WARNING_LENGTH))
            || ((form.getProductDescription().getDescriptionRemarks().length() > Constant.DESCRIPTION_REMARKS_LENGTH)
                && (form.getProductDescription().getDescriptionRemarks().length() <= Constant.DESCRIPTION_REMARKS_WARNING_LENGTH))) {
        	//productGeneralErrorFlg = 1;
        	errorCheckFlg = 0;
        }
        if (errorCheckFlg == 0) {
			return 0;
		} else {
			return 1;
		}
	}

	/************************************************************************
	 * <b>Description:</b><br>
	 *  validate Block 5.
	 *
	 * @author		Tran.Thanh
	 * @date		3 Mar 2014
	 * @param 		entTblFactoryProductLinkList List<EntTblFactoryProductLink>
	 * @return		int
	 ************************************************************************/
	int validateBlock5(List<EntTblFactoryProductLink> entTblFactoryProductLinkList) {
		int errorCheckFlg = 1;
		// Validate before Save Process
        for (EntTblFactoryProductLink entProductLink: entTblFactoryProductLinkList) {
        	if (entProductLink.getProductLinkTitle().length() > Constant.PRODUCT_LINK_TITLE_WARNING_LENGTH
        		|| entProductLink.getProductLinkUrl().length() > Constant.PRODUCT_LINK_URL_WARNING_LENGTH) {
        		return -1; // return when input out of range in DataBase
        	} else if (entProductLink.getProductLinkTitle().length() == Constant.PRODUCT_EDIT_PRODUCT_NULL
        			|| entProductLink.getProductLinkUrl().length() == Constant.PRODUCT_EDIT_PRODUCT_NULL) {
        		return -1; // return when input is null
        	} else if (((entProductLink.getProductLinkTitle().length() > Constant.PRODUCT_LINK_TITLE_LENGTH)
        			&& (entProductLink.getProductLinkTitle().length() <= Constant.PRODUCT_LINK_TITLE_WARNING_LENGTH))
            		|| ((entProductLink.getProductLinkUrl().length() > Constant.PRODUCT_LINK_URL_LENGTH)
            		&& (entProductLink.getProductLinkUrl().length() <= Constant.PRODUCT_LINK_URL_WARNING_LENGTH))) {
        		errorCheckFlg = 0;
        	}
        }
        if (errorCheckFlg == 0) {
			return 0;
		} else {
			return 1;
		}
	}

	/************************************************************************
	 * <b>Description:</b><br>
	 *  validate BLock 6.
	 *
	 * @author		Tran.Thanh
	 * @date		3 Mar 2014
	 * @param 		entTblFactoryProductVideoList List<EntTblFactoryProductVideo>
	 * @return		int
	 ************************************************************************/
	int validateBLock6(List<EntTblFactoryProductVideo> entTblFactoryProductVideoList) {
		int errorCheckFlg = 1;
		// Validate before Save Process
        for (EntTblFactoryProductVideo entProductVideo: entTblFactoryProductVideoList) {
        	if (entProductVideo.getProductVideoTitle().length() > Constant.PRODUCT_VIDEO_TITLE_WARNING_LENGTH
        		|| entProductVideo.getProductVideoUrl().length() > Constant.PRODUCT_VIDEO_URL_WARNING_LENGTH
        		|| entProductVideo.getProductVideoDescription().length() > Constant.PRODUCT_VIDEO_DESCRIPTION_WARNING_LENGTH) {
        		return -1; // return when input out of range in DataBase
        	} else if (entProductVideo.getProductVideoTitle().length() == Constant.PRODUCT_EDIT_PRODUCT_NULL
        			|| entProductVideo.getProductVideoUrl().length() == Constant.PRODUCT_EDIT_PRODUCT_NULL
        			|| entProductVideo.getProductVideoDescription().length() == Constant.PRODUCT_EDIT_PRODUCT_NULL) {
        		return -1; // return when input is null
        	} else if (((entProductVideo.getProductVideoTitle().length() > Constant.PRODUCT_VIDEO_TITLE_LENGTH)
        			&& (entProductVideo.getProductVideoTitle().length() <= Constant.PRODUCT_VIDEO_TITLE_WARNING_LENGTH))
            		|| ((entProductVideo.getProductVideoUrl().length() > Constant.PRODUCT_VIDEO_URL_LENGTH)
            		&& (entProductVideo.getProductVideoUrl().length() <= Constant.PRODUCT_VIDEO_URL_WARNING_LENGTH))
            		|| ((entProductVideo.getProductVideoDescription().length() > Constant.PRODUCT_VIDEO_DESCRIPTION_LENGTH)
                    && (entProductVideo.getProductVideoDescription().length() <= Constant.PRODUCT_VIDEO_DESCRIPTION_WARNING_LENGTH))) {
        		errorCheckFlg = 0;
        	}
        }
        if (errorCheckFlg == 0) {
			return 0;
		} else {
			return 1;
		}
	}

	/************************************************************************
	 * <b>Description:</b><br>
	 *  validate Block 8.
	 *
	 * @author		Tran.Thanh
	 * @date		3 Mar 2014
	 * @param 		entTblFactoryProductGuestInputList List<EntTblFactoryProductGuestInput>
	 * @return		int
	 ************************************************************************/
	int validateBlock8(List<EntTblFactoryProductGuestInput> entTblFactoryProductGuestInputList) {
		int errorCheckFlg = 1;
		for (EntTblFactoryProductGuestInput entProductGuestInput: entTblFactoryProductGuestInputList) {
        	if (entProductGuestInput.getProductGuestInputTitle().length() > Constant.PRODUCT_GUEST_INPUT_TITLE_WARNING_LENGTH
        		|| entProductGuestInput.getProductGuestInputDescription().length() > Constant.PRODUCT_GUEST_INPUT_DESCRIPTION_WARNING_LENGTH) {
        		return -1; // return when input out of range in DataBase
        	} else if (entProductGuestInput.getProductGuestInputTitle().length() == Constant.PRODUCT_EDIT_PRODUCT_NULL
        			|| entProductGuestInput.getProductGuestInputDescription().length() == Constant.PRODUCT_EDIT_PRODUCT_NULL) {
        		return -1; // return when input is null
        	} else if (((entProductGuestInput.getProductGuestInputTitle().length() > Constant.PRODUCT_GUEST_INPUT_TITLE_LENGTH)
        			&& (entProductGuestInput.getProductGuestInputTitle().length() <= Constant.PRODUCT_GUEST_INPUT_TITLE_WARNING_LENGTH))
            		|| ((entProductGuestInput.getProductGuestInputDescription().length() > Constant.PRODUCT_GUEST_INPUT_DESCRIPTION_LENGTH)
                    && (entProductGuestInput.getProductGuestInputDescription().length() <= Constant.PRODUCT_GUEST_INPUT_DESCRIPTION_WARNING_LENGTH))) {
        		errorCheckFlg = 0;
        	}
        }
		if (errorCheckFlg == 0) {
			return 0;
		} else {
			return 1;
		}
	}

	/************************************************************************
	 * <b>Description:</b><br>
	 *  validateBlock10.
	 *
	 * @author		Tran.Thanh
	 * @date		3 Mar 2014
	 * @param 		form ProductEditActionForm
	 * @return		int
	 ************************************************************************/
	int validateBlock10(ProductEditActionForm form) {
		int errorCheckFlg = 1;
		String categoryCodeStr = "";
        if (form.getFactoryProductGeneral().getProductCategoryCode() != null) {
        	categoryCodeStr = form.getFactoryProductGeneral().getProductCategoryCode().toString();
        }
        if (categoryCodeStr.length() > Constant.PRODUCT_CATEGORY_CODE_WARNING_LENGTH) {
        	form.setMsgCode(0);
        	return -1;
        } else if (categoryCodeStr.length() == Constant.PRODUCT_EDIT_PRODUCT_NULL) {
        	form.setMsgCode(0);
        	return -1;
        } else if (categoryCodeStr.length() > Constant.PRODUCT_CATEGORY_CODE_MAX_LENGTH_INPUT
        		&& categoryCodeStr.length() <= Constant.PRODUCT_CATEGORY_CODE_WARNING_LENGTH) {
        	errorCheckFlg = 0;
        }
        if (errorCheckFlg == 0) {
			return 0;
		} else {
			return 1;
		}
	}

	/************************************************************************
	 * <b>Description:</b><br>
	 *  validateBlock12.
	 *
	 * @author		Tran.Thanh
	 * @date		5 Mar 2014
	 * @param 		listFactoryProductSupplier List<EntTblFactoryProductSupplier>
	 * @return		int
	 ************************************************************************/
	int validateBlock12(List<EntTblFactoryProductSupplier> listFactoryProductSupplier) {
		int errorCheckFlg = 1;
		for (EntTblFactoryProductSupplier entProductSupplier: listFactoryProductSupplier) {
			if (entProductSupplier.getSiireCode().length() > Constant.SIIRE_CODE_WARNING_LENGTH
        		|| entProductSupplier.getNoukiCode().length() > Constant.NOUKI_CODE_WARNING_LENGTH
        		|| entProductSupplier.getSupplierSyouhinSiireOrder().toString().length() > Constant.SUPPLIER_SYOUHIN_SIIRE_ORDER_WARNING_LENGTH) {
        		return -1; // return when input out of range in DataBase
        	} else if (entProductSupplier.getSiireCode().length() == Constant.PRODUCT_EDIT_PRODUCT_NULL
        			|| entProductSupplier.getNoukiCode().length() == Constant.PRODUCT_EDIT_PRODUCT_NULL
        			|| entProductSupplier.getSupplierSyouhinSiireOrder().toString().length() == Constant.PRODUCT_EDIT_PRODUCT_NULL) {
        		return -1; // return when input is null
        	} else if (((entProductSupplier.getSiireCode().length() > Constant.SIIRE_CODE_LENGTH)
        			&& (entProductSupplier.getSiireCode().length() <= Constant.SIIRE_CODE_WARNING_LENGTH))
            		|| ((entProductSupplier.getNoukiCode().length() > Constant.NOUKI_CODE_LENGTH)
                    && (entProductSupplier.getNoukiCode().length() <= Constant.NOUKI_CODE_WARNING_LENGTH))
                    || ((entProductSupplier.getSupplierSyouhinSiireOrder().toString().length() > Constant.SUPPLIER_SYOUHIN_SIIRE_ORDER_LENGTH)
                    && (entProductSupplier.getSupplierSyouhinSiireOrder().toString().length() <= Constant.SUPPLIER_SYOUHIN_SIIRE_ORDER_WARNING_LENGTH))) {
        		errorCheckFlg = 0;
        	}
		}
        if (errorCheckFlg == 0) {
			return 0;
		} else {
			return 1;
		}
	}

	/************************************************************************
	 * <b>Description:</b><br>
	 *   process to save Data productEdit page.
	 *
	 * @author		Tran.Thanh
	 * @date		21 Feb 2014
	 * @return		list
	 ************************************************************************/
	@SuppressWarnings("unchecked")
    @Action("/processSaveDataProductEdit")
	public String processSaveDataProductEdit() {
		boolean flgResultSave = false;
        int productGeneralErrorFlg = 0; // 0 : no error, 1 : have error
        int productCategoryErrorFlg = 0; // 0 : no error, 1 : have error
		Map<Integer, Object> mapProduct = new HashMap<Integer , Object>();
		EntTblFactoryProductNew entProduct = new EntTblFactoryProductNew();

		try {
        if (!validateLoginUser()) {
            return "errorOther";
        }
		// initEntTblFactoryProductNew
		entProduct = this.initEntTblFactoryProductNew();
		//initMsgCode
		initMsgCode(false);
		// initMsgString
		initMsgString();
		// initResponeMapAjaxSaveData
		initResponeMapAjaxSaveData(entProduct);

		// jSon Block 2
        List<EntTblFactoryProductImage> entTblFactoryProductImageList = this.initEntTblFactoryProductImageList();
        mapProduct.put(Constant.PRODUCT_EDIT_IMAGE_BLOCK, entTblFactoryProductImageList);
		// jSon Block 3
        form.getFactoryProductGeneral().setProductDelFlg(false);
        int resultValidateBlock3 = validateBlock3(form);
        if (resultValidateBlock3 == -1) {
        	return "processSaveData";
        } else if (resultValidateBlock3 ==  0) {
        	productGeneralErrorFlg = 1;
        }
        mapProduct.put(Constant.PRODUCT_EDIT_GENARAL_BLOCK, form);
		// jSon Block 4
        List<EntMstAttribute> listAttribute =  this.initListAttribute();
        mapProduct.put(Constant.PRODUCT_EDIT_ATRIBUTE_BLOCK, listAttribute);
		// jSon Block 5
        List<EntTblFactoryProductLink> entTblFactoryProductLinkList = this.initEntTblFactoryProductLinkList();
        int resultValidateBlock5 = validateBlock5(entTblFactoryProductLinkList);
        if (resultValidateBlock5 == -1) {
        	return "processSaveData";
        } else if (resultValidateBlock5 ==  0) {
        	productGeneralErrorFlg = 1;
        }
        int levelLink = Constant.PRODUCT_EDIT_LINK_BLOCK;
        mapProduct.put(levelLink, entTblFactoryProductLinkList);
        // jSonVideo Block 6
        List<EntTblFactoryProductVideo> entTblFactoryProductVideoList = this.initEntTblFactoryProductVideoList();
        int resultValidateBlock6 = validateBLock6(entTblFactoryProductVideoList);
        if (resultValidateBlock6 == -1) {
        	return "processSaveData";
        } else if (resultValidateBlock6 ==  0) {
        	productGeneralErrorFlg = 1;
        }
        int level = Constant.PRODUCT_EDIT_VIDEO_BLOCK;
        mapProduct.put(level, entTblFactoryProductVideoList);
        List<EntTblFactoryProductGuestInput> entTblFactoryProductGuestInputList = this.initEntTblFactoryProductGuestInputList();
        // Validate before Save Process
        int resultValidateBlock8 = validateBlock8(entTblFactoryProductGuestInputList);
        if (resultValidateBlock8 == -1) {
        	return "processSaveData";
        } else if (resultValidateBlock8 ==  0) {
        	productGeneralErrorFlg = 1;
        }
        int levelGuest = Constant.PRODUCT_EDIT_GUEST_BLOCK;
        mapProduct.put(levelGuest, entTblFactoryProductGuestInputList);
        //Move block 9 to bottom by Luong.Dai
        // jSon Block 10
        // Validate before process save
        int resultValidateBlock10 = validateBlock10(form);
        if (resultValidateBlock10 == -1) {
        	return "processSaveData";
        } else if (resultValidateBlock10 ==  0) {
        	productCategoryErrorFlg = 1;
        }
        // jSon Block 11
        Map<Object, Object> mapFitModel = this.initMapFitModel(entProduct);
        mapProduct.put(Constant.PRODUCT_EDIT_FIT_MODEL_BLOCK, mapFitModel);

        // jSon Block 12
        List<EntTblFactoryProductSupplier> listFactoryProductSupplier =  this.initListFactoryProductSupplier();
        // Validate before Save Process
        int resultValidateBlock12 = validateBlock12(listFactoryProductSupplier);
        if (resultValidateBlock12 == -1) {
        	return "processSaveData";
        } else if (resultValidateBlock12 ==  0) {
        	productGeneralErrorFlg = 1;
        }
        mapProduct.put(Constant.PRODUCT_EDIT_SUPPLIER_STATUS_BLOCK, listFactoryProductSupplier);

        // jSon Block 9
        Map<String, Object> productSameGroupData =  this.initListProductSameGroup();
        //Validate current product value: result = -1: validate fail, 0: warning, 1: success
        Integer validateResult = validateProductValueOFGroupProduct(
                                                    (List<EntTblFactoryProductNew>) productSameGroupData.get("ListProduct"));
        if (validateResult == 0) {
            productGeneralErrorFlg = 1;
        }

        mapProduct.put(Constant.PRODUCT_EDIT_SUPPLIER_BLOCK, productSameGroupData);

        ArrayList<Integer> errorFlg = new ArrayList<Integer>();
        errorFlg.add(productGeneralErrorFlg);
        errorFlg.add(productCategoryErrorFlg);

        // Excute Process Save Product
		flgResultSave = productEditService.processSaveDataProductEdit(mapProduct, entProduct, errorFlg, form);

		} catch (Exception e) {
            this.getLogger().error("[ProductEditAction] An error execute function");
            this.getLogger().error(e.toString());
		} finally {
			//initMsgCode
			initMsgCode(flgResultSave);
			// initMsgString
			initMsgString();
			// initResponeMapAjaxSaveData
			initResponeMapAjaxSaveData(entProduct);
		}
		return "processSaveData";
	}
	/************************************************************************
	 * <b>Description:</b><br>
	 *  initMsgCode.
	 * @author		Le.Dinh
	 * @date		5 Mar 2014
	 * @param 		flgResultSave	boolean
	 ************************************************************************/
	private void initMsgCode(boolean flgResultSave) {
		form.setMsgCode(0);
		if (flgResultSave) {
			form.setMsgCode(1);
		}
	}
	/************************************************************************
	 * <b>Description:</b><br>
	 *  initResponeMapAjaxSaveData.
	 * @author		Le.Dinh
	 * @date		5 Mar 2014
	 * @param 		entProduct	EntTblFactoryProductNew
	 ************************************************************************/
	private void initResponeMapAjaxSaveData(EntTblFactoryProductNew entProduct) {
		responeMapAjaxSaveData = new HashMap<Object, Object>();
		responeMapAjaxSaveData.put(Constant.PARAM_PRODUCT_ID, entProduct.getProductId());
		responeMapAjaxSaveData.put(Constant.SESSION_MSG_CODE, form.getMsgCode());
		responeMapAjaxSaveData.put(Constant.MSG_STRING, 	  form.getMsgString());
	}
	/************************************************************************
	 * <b>Description:</b><br>
	 *  checkMsgString.
	 * @author		Le.Dinh
	 * @date		5 Mar 2014
	 ************************************************************************/
	//BOE Tran.Thanh 2018/05/10 : remove checkMsgString
	/*private void checkMsgString() {
		String msgString = (String) session.get(Constant.SESSION_MSG_CODE);
		form.setMsgString(msgString);
		session.remove(Constant.SESSION_MSG_CODE);
	}*/
	//EOE Tran.Thanh 2018/05/10 : remove checkMsgString

	/************************************************************************
	 * <b>Description:</b><br>
	 *  initMsgString.
	 * @author		Le.Dinh
	 * @date		5 Mar 2014
	 ************************************************************************/
	private void initMsgString() {
		// set msgString in case Update or Duplicate
		if (form.getMsgCode() == 1) {
			if (Constant.EDIT_MODE.equals(form.getMode())) {
				form.setMsgString(getText("message.success.updateSuccess"));
			} else {
				form.setMsgString(getText("message.success.insertSuccess"));
			}
			//BOE Tran.Thanh 2014/05/10 : Remove session msgCode
			//session.put(Constant.SESSION_MSG_CODE, form.getMsgString());
			//EOE Tran.Thanh 2014/05/10 : Remove session msgCode
		} else {
			if (Constant.EDIT_MODE.equals(form.getMode())) {
				form.setMsgString(getText("message.error.updateFail"));
			} else {
				form.setMsgString(getText("message.success.insertFail"));
			}
		}
	}

	/************************************************************************
	 * <b>Description:</b><br>
	 *  initEntTblFactoryProductImageList.
	 *
	 * @author		Le.Dinh
	 * @date		4 Mar 2014
	 * @return		List<EntTblFactoryProductImage>
	 ************************************************************************/
	@SuppressWarnings("unchecked")
	private List<EntTblFactoryProductImage> initEntTblFactoryProductImageList() {
		List<EntTblFactoryProductImage> entTblFactoryProductImageList = null;
		String listImageJson = this.getRequest().getParameter("jSonImageDs");
		Type listTypeImage = new TypeToken<List<EntTblFactoryProductImage>>() { } .getType();
        // convert string json to list.
        entTblFactoryProductImageList = Utils.parseJsonStringToList(listImageJson, listTypeImage);

        if (entTblFactoryProductImageList == null || entTblFactoryProductImageList.size() < 1) {
        	entTblFactoryProductImageList = new ArrayList<EntTblFactoryProductImage>();
        }

        return	entTblFactoryProductImageList;
	}

	/************************************************************************
	 * <b>Description:</b><br>
	 *  initListAttribute.
	 * @author		Le.Dinh
	 * @date		4 Mar 2014
	 * @return		List<EntMstAttribute>
	 ************************************************************************/
	@SuppressWarnings("unchecked")
	private List<EntMstAttribute>  initListAttribute() {
		List<EntMstAttribute> listAttribute  = null;
        String gridAttributeStr = this.getRequest().getParameter("gridAttributeStr");
        Type listTypeAttribute = new TypeToken<List<EntMstAttribute>>() { } .getType();
        listAttribute =  Utils.parseJsonStringToList(gridAttributeStr, listTypeAttribute);

        if (listAttribute == null || listAttribute.size() < 1) {
        	listAttribute = new ArrayList<EntMstAttribute>();
        }

        return listAttribute;
	}

	/************************************************************************
	 * <b>Description:</b><br>
	 *  initEntTblFactoryProductLinkList.
	 *
	 * @author		Le.Dinh
	 * @date		4 Mar 2014
	 * @return		List<EntTblFactoryProductLink>
	 ************************************************************************/
	@SuppressWarnings("unchecked")
	private List<EntTblFactoryProductLink> initEntTblFactoryProductLinkList() {
		List<EntTblFactoryProductLink> entTblFactoryProductLinkList = null;
		String listLinkJson = this.getRequest().getParameter("jSonLinkDs");
		Type listTypeLink = new TypeToken<List<EntTblFactoryProductLink>>() { } .getType();
        entTblFactoryProductLinkList = Utils.parseJsonStringToList(listLinkJson, listTypeLink);

        if (entTblFactoryProductLinkList == null || entTblFactoryProductLinkList.size() < 1) {
        	entTblFactoryProductLinkList = new ArrayList<EntTblFactoryProductLink>();
        }

        return entTblFactoryProductLinkList;
	}

	/************************************************************************
	 * <b>Description:</b><br>
	 *  initEntTblFactoryProductVideoList.
	 *
	 * @author		Le.Dinh
	 * @date		4 Mar 2014
	 * @return		List<EntTblFactoryProductVideo>
	 ************************************************************************/
	@SuppressWarnings("unchecked")
	private List<EntTblFactoryProductVideo> initEntTblFactoryProductVideoList() {

		List<EntTblFactoryProductVideo> entTblFactoryProductVideoList = null;

		String listVideoJson = this.getRequest().getParameter("jSonVideoDs");
		Type listType = new TypeToken<List<EntTblFactoryProductVideo>>() { } .getType();
        // convert string json to list.
        entTblFactoryProductVideoList = Utils.parseJsonStringToList(listVideoJson, listType);

        if (entTblFactoryProductVideoList == null || entTblFactoryProductVideoList.size() < 1) {
        	entTblFactoryProductVideoList = new ArrayList<EntTblFactoryProductVideo>();
        }

        return entTblFactoryProductVideoList;
	}

	/************************************************************************
	 * <b>Description:</b><br>
	 *  initEntTblFactoryProductGuestInputList.
	 * @author		Le.Dinh
	 * @date		4 Mar 2014
	 * @return		List<EntTblFactoryProductGuestInput>
	 ************************************************************************/
	@SuppressWarnings("unchecked")
	private List<EntTblFactoryProductGuestInput> initEntTblFactoryProductGuestInputList() {
		List<EntTblFactoryProductGuestInput> entTblFactoryProductGuestInputList = null;
		String listGuestJson = this.getRequest().getParameter("jSonGuestDs");
		Type listTypeGuest = new TypeToken<List<EntTblFactoryProductGuestInput>>() { } .getType();
        // convert string json to list.
        entTblFactoryProductGuestInputList = Utils.parseJsonStringToList(listGuestJson, listTypeGuest);

        if (entTblFactoryProductGuestInputList == null || entTblFactoryProductGuestInputList.size() < 1) {
        	entTblFactoryProductGuestInputList = new ArrayList<EntTblFactoryProductGuestInput>();
        }

        return entTblFactoryProductGuestInputList;
	}

	/************************************************************************
	 * <b>Description:</b><br>
	 *  initListProductSameGroup.
	 * @author		Le.Dinh
	 * @date		4 Mar 2014
	 * @return		List<EntTblFactoryProductNew>
	 * @throws		SQLException		Exception
	 ************************************************************************/
	@SuppressWarnings("unchecked")
	private Map<String, Object> initListProductSameGroup() throws SQLException {
	    Type listTypeGroupCode = new TypeToken<List<EntTblFactoryProductNew>>() { } .getType();
        List<EntTblFactoryProductNew> listProductSameGroup =  Utils.parseJsonStringToListWithDateFormat(form.getListGridGroupCodeStr(), listTypeGroupCode);
        // List select
        Type listTypeSelect = new TypeToken<List<EntTblFactoryProductSelect>>() { } .getType();
        List<EntTblFactoryProductSelect> listProductSelect =  Utils.parseJsonStringToListWithDateFormat(form.getListProductSelectStr(), listTypeSelect);
        // Map has value to sent to service
        Map<String, Object> mapValueGroupProduct = new HashMap<String , Object>();
        mapValueGroupProduct.put("ListProduct", listProductSameGroup);
        mapValueGroupProduct.put("ListSelect", listProductSelect);
        //Set productGeneral value
        mapValueGroupProduct.put("ProductGeneral", form.getFactoryProductGeneral());
        //Set flag edit single product or group product
        mapValueGroupProduct.put("editGroupFlg", form.getFlgToGetPriceBlock9());

        return mapValueGroupProduct;

	}

	/************************************************************************
     * <b>Description:</b><br>
     *  Validate value of product.
     *
     * @author		Luong.Dai
     * @date		Mar 5, 2014
     * @param       listProductSameGroup    List<EntTblFactoryProductNew>
     * @return		Integer     : 0     : warning
     *                          : 1     : success
     ************************************************************************/
    private Integer validateProductValueOFGroupProduct(List<EntTblFactoryProductNew> listProductSameGroup) {
        Integer result = 1;
        //Validate required field
        if (listProductSameGroup != null) {
            int size = listProductSameGroup.size();
            if (size > 0) {
                EntTblFactoryProductNew product = listProductSameGroup.get(0);
                Boolean flag = validateMaxLengthFieldProduct(product);
                if (!flag) {
                    //Has field wrong max length
                    result = 0;
                }
            }
        }

        return result;
    }

    /************************************************************************
     * <b>Description:</b><br>
     *  Validate max length of productField.
     *  Validate productCode, productEANCode.
     *
     * @author		Luong.Dai
     * @date		Mar 6, 2014
     * @param       product     EntTblFactoryProductNew
     * @return		Boolean
     ************************************************************************/
    private Boolean validateMaxLengthFieldProduct(EntTblFactoryProductNew product) {
        Boolean result = true;

        if (StringUtils.isNotEmpty(product.getProductCode())) {
            if (product.getProductCode().length() > Constant.PRODUCT_MAX_PRODUCT_CODE_LENGTH) {
                result = false;
            }
        }

        if (StringUtils.isNotEmpty(product.getProductEanCode())) {
            if (product.getProductEanCode().length() > Constant.PRODUCT_MAX_PRODUCT_EAN_CODE_LENGTH) {
                result = false;
            }
        }

        return result;
    }

    /************************************************************************
	 * <b>Description:</b><br>
	 *  initMapFitModel.
	 * @author		Le.Dinh
	 * @date		4 Mar 2014
	 * @param 		entProduct	EntTblFactoryProductNew
	 * @return		Map<Object,Object>
	 ************************************************************************/
	@SuppressWarnings("unchecked")
	private Map<Object, Object> initMapFitModel(EntTblFactoryProductNew entProduct) {
        Map<Object, Object> mapFitModel = new HashMap<Object , Object>();

        Type listTypeFitModel = new TypeToken<List<EntTblFactoryProductFitModel>>() { } .getType();
        List<EntTblFactoryProductFitModel> listFitModel =  Utils.parseJsonStringToList(form.getListFitModelJsonStr(), listTypeFitModel);
        // set list Fit Model to form
        form.setListFitModel(listFitModel);
        // setListProductFitModel
        List<EntTblFactoryProductNew> listProductFitModel = new ArrayList<EntTblFactoryProductNew>();
        listProductFitModel.add(entProduct);
        form.setListProductFitModel(listProductFitModel);
        // validate
        validateDataInputFitModel(false);
        // checkModelMakerAndModelModel
        checkModelMakerAndModelModel();

        mapFitModel.put(LIST_FIT_MODEL, form.getListFitModel());
        mapFitModel.put(LIST_PRODUCT_FIT_MODEL, form.getListProductFitModel());
        //entProductModelFlg
        form.getFactoryMatter().setMatterNo(entProduct.getProductMatterNo());
        mapFitModel.put(FACTORY_MATTER, form.getFactoryMatter());

        return	mapFitModel;
	}

	/************************************************************************
	 * <b>Description:</b><br>
	 *  initListFactoryProductSupplier.
	 * @author		Le.Dinh
	 * @date		4 Mar 2014
	 * @return		List<EntTblFactoryProductSupplier>
	 ************************************************************************/
	@SuppressWarnings("unchecked")
	private List<EntTblFactoryProductSupplier> initListFactoryProductSupplier() {
		List<EntTblFactoryProductSupplier> listFactoryProductSupplier = null;
        Type listTypeSupplier = new TypeToken<List<EntTblFactoryProductSupplier>>() { } .getType();
        listFactoryProductSupplier =  Utils.parseJsonStringToList(form.getFactoryProductSupplierJsonStr(), listTypeSupplier);

        if (listFactoryProductSupplier == null || listFactoryProductSupplier.size() < 1) {
        	listFactoryProductSupplier = new ArrayList<EntTblFactoryProductSupplier>();
        }

        return listFactoryProductSupplier;
	}
	/************************************************************************
	 * <b>Description:</b><br>
	 *  initEntTblFactoryProductNewList.
	 * @author		Le.Dinh
	 * @date		4 Mar 2014
	 * @return		List<EntTblFactoryProductNew>
	 ************************************************************************/
	@SuppressWarnings("unchecked")
	private List<EntTblFactoryProductNew> initEntTblFactoryProductNewList() {
		List<EntTblFactoryProductNew> entTblFactoryProductNewList =  null;
 		String listProductJson = this.getRequest().getParameter("gridProductStr");
 		Type listTypeProduct = new TypeToken<List<EntTblFactoryProductNew>>() { } .getType();
        // convert string json to list.
 		entTblFactoryProductNewList = Utils.parseJsonStringToListWithDateFormat(listProductJson, listTypeProduct);

        if (entTblFactoryProductNewList == null || entTblFactoryProductNewList.size() < 1) {
        	entTblFactoryProductNewList = new ArrayList<EntTblFactoryProductNew>();
        }

 		return entTblFactoryProductNewList;
	}


	/************************************************************************
	 * <b>Description:</b><br>
	 *  initEntTblFactoryProductNew.
	 *
	 * @author		Le.Dinh
	 * @date		4 Mar 2014
	 * @return		entProduct	EntTblFactoryProductNew
	 * @throws 		SQLException	SQLException
	 ************************************************************************/
	private EntTblFactoryProductNew initEntTblFactoryProductNew() throws SQLException {
		EntTblFactoryProductNew entProduct = null;

        String loginId = this.getCommonForm().getLoginId();
		String entProductJson = this.getRequest().getParameter("productJson");
		String flgMode = this.getRequest().getParameter("flgMode");
		Gson gsonProduct = new Gson();
        // convert string json to list.
        entProduct = gsonProduct.fromJson(entProductJson, new TypeToken<EntTblFactoryProductNew>() { } .getType());

        // set Mode is Edit or New ( N => new , E  => Edit)
        form.setMode(flgMode);
		if (Constant.NEW_MODE.equals(flgMode)) {
			// get new ProductId by  select max Numbering to create new productId
			Long productIdNew  = productEditService.selectMaxProductIdPlusOne();
			// set new properties for entProduct
			entProduct.setProductId(productIdNew);
			entProduct.setProductModelErrorFlg(0);
			entProduct.setProductSyouhinSysCode((long) 0);

			//BOE Nguyen.Chuong 2014/03/28 check productWebikeCodeFlg to format productCode in factoryProductGeneral
			//If checkBox infoProductWebikeCodeFlg is checked.
			if (1 == form.getFactoryProductGeneral().getProductWebikeCodeFlg()) {
			    String productCodeFormated = String.format(Constant.PRODUCT_CODE_FORMAT_JAVA
			                                             , form.getFactoryProductGeneral().getProductBrandCode()
			                                             , productIdNew);
			    form.getFactoryProductGeneral().setProductCode(productCodeFormated);
			}
			//EOE Nguyen.Chuong 2014/03/28
		}
        // set Login id for EntTblFactoryProductNew
        entProduct.setUpdatedUserId(loginId);
		return	entProduct;
	}

	/* EOE Block1 */

	/* BOE Block2 */
	/************************************************************************
	 * <b>Description:</b><br>
	 *  get list image thumb.
	 *
	 * @author		Tran.Thanh
	 * @date		14 Feb 2014
	 * @param       productId Long
	 ************************************************************************/
	private void initListImageThumb(Long productId) {

		List<EntTblFactoryProductImage> listImageAndThumb = new ArrayList<EntTblFactoryProductImage>();
		String productImageDetailJsonStr = "[]";

		if (productId != null && productId > 0) {
			//Get list image of product.
			EntTblFactoryProductNew entProduct = new EntTblFactoryProductNew();
			entProduct.setProductId(productId);

			listImageAndThumb = productService.selectListImageAndThumbByProductId(entProduct);
			productImageDetailJsonStr = Utils.parseObjectToJson(listImageAndThumb);
		}

		productImageDetailJsonStr = Utils.parseObjectToJson(listImageAndThumb);

        form.setListImage(listImageAndThumb);
        form.setFactoryProductImageDetailJsonStr(productImageDetailJsonStr);
        form.setUrlProductFactoryPath(productEditService.selectUrlPathFromSettingTable());

        /*BOE Tran.Thanh 2014/04/03 : add max Upload Image size and max Folder Matter size */
        String valueMaxSizeFolder = settingService.selectSettingValueBySettingCode(Constant.MAX_IMAGE_FOLDER_MATTER);
        if (StringUtils.isNotEmpty(valueMaxSizeFolder)) {
        	form.setMaxUploadFolderMatter(Integer.parseInt(valueMaxSizeFolder));
        }
        String valueMaxSizeImage = settingService.selectSettingValueBySettingCode(Constant.MAX_SIZE_UPLOAD_IMAGE);
        if (StringUtils.isNotEmpty(valueMaxSizeImage)) {
        	form.setMaxUploadImage(Integer.parseInt(valueMaxSizeImage));
        }
        /*EOE Tran.Thanh 2014/04/03 : add max Upload Image size and max Folder Matter size */
	}
	/* EOE Block2 */

	/* BOE Block3 */
	/************************************************************************
	 * <b>Description:</b><br>
	 *  initListUserInformation.
	 * @author		Le.Dinh
	 * @date		19 Feb 2014
	 * @param 		productId	Long
	 * @throws		SQLException	SQLException
	 ************************************************************************/
	private void initListUserInformation(Long productId) throws SQLException {
		// Get brand list.
		List<EntMstBrand> brandList = brandService.selectBrandList();
		form.setBrandList(brandList);

		// get List EnumProductConditionFlg
		Map<String, String> mapProductConditionFlg = EnumProductConditionFlg.getHashMapAllFlg();
		form.setMapProductConditionFlg(mapProductConditionFlg);

	}
	/************************************************************************
	 * <b>Description:</b><br>
	 *  getAjaxInforBrandByBrandCode.
	 *
	 * @author		Le.Dinh
	 * @date		20 Feb 2014
	 * @return		String
	 * @throws 		SQLException SQLException
	 ************************************************************************/
	@Action("/loadtAjaxInforBrandByBrandCode")
	public String loadtAjaxInforBrandByBrandCode() throws SQLException {
		String brandCode  = this.getRequest().getParameter("brandCode");

        EntMstBrand entMstBrandQuery = new EntMstBrand();
        entMstBrandQuery.setBrandCode(Integer.parseInt(brandCode));

        /* BOE Fix brandCode validation @rcv!nguyen.hieu 2014/03/14. */
		//EntMstBrand brandInfo = brandService.selectBrandByBrandCode(entMstBrandQuery);
		EntMstBrand brandInfo = brandService.selectBrandByBrandCodeNotDeleted(entMstBrandQuery);
		/* EOE Fix brandCode validation. */
		form.setBrandInfo(brandInfo);

		return "brandInfo";
	}
	/* EOE Block3 */

	/* BOE Block4 */
	/************************************************************************
	 * <b>Description:</b><br>
	 *  select list attribute of product.
	 *
	 * @author		Luong.Dai
	 * @date		Feb 12, 2014
	 * @return		String
	 ************************************************************************/
	@Action("/selectListAttributeOfProduct")
	public String selectListAttributeOfProduct() {
	    try {
    	    //Get categoryCode in params
    	    String categoryCode = this.getRequest().getParameter("categoryCode");
    	    //Get productId
    	    String productIdParam = this.getRequest().getParameter("productId");
    	    //Set params
    	    EntTblFactoryProductNew product = new EntTblFactoryProductNew();
    	    if (StringUtils.isNotEmpty(productIdParam)) {
    	        product.setProductId(Long.parseLong(productIdParam));
    	    }
    	    product.setCategoryCode(categoryCode);
    	    //Get list attribute
    	    List<EntMstAttribute> lstAttribute = productEditService.selectListAttributeOfProduct(product);
    	    form.setListAttribute(lstAttribute);
	    } catch (SQLException e) {
	        this.getLogger().error("[selectListAttributeOfProduct] Error when query in DB");
	    }

	    return "list";
	}

	/************************************************************************
	 * <b>Description:</b><br>
	 *  selec list attribute group by attribte code.
	 *
	 * @author		Luong.Dai
	 * @date		Feb 15, 2014
	 * @return		String
	 ************************************************************************/
	@Action("/selectListAttributeGroupBySelectedAttributeCode")
    public String selectListAttributeGroupBySelectedAttributeCode() {
        try {
            //Get attributeCode in params
            String attributeCode = this.getRequest().getParameter("attributeCode");
            //Validate categoryCode
            if (StringUtils.isEmpty(attributeCode)) {
                return "list";
            }
            //Get list attribute_group
            List<EntMstAttributeGroup> lstAttributeGroup = productEditService.selectListAttributeGroupByAttributeCode(attributeCode);
            form.setListAttrGroup(lstAttributeGroup);
        } catch (Exception e) {
            this.getLogger().error("[selectListAttributeGroupBySelectedAttributeCode] Error when query in DB");
        }

        return "list";
    }

	/************************************************************************
	 * <b>Description:</b><br>
	 *  Load init for block 4: Attribute.
	 *  Load list attribute name.
	 *  Load list attribute type from enum
	 *
	 * @author		Luong.Dai
	 * @date		Feb 13, 2014		void
	 ************************************************************************/
	private void loadInitAttribute() {
	    //Get all list attribute
	    List<EntMstAttribute> listAllAttribute = productEditService.selectAllListAttribute();

	    //Set list to action form
	    this.form.setListAllAttribute(listAllAttribute);
	}
	/* EOE Block4 */

	/* BOE Block4 */
	/* EOE Block4 */

	/* BOE Block5 */
	/************************************************************************
	 * <b>Description:</b><br>
	 *  get data for block 5 -Product Link.
	 *
	 * @author		Tran.Thanh
	 * @date		18 Feb 2014
	 * @param 		productId Long
	 * @throws SQLException		void
	 ************************************************************************/
	private void loadInitProductLink(Long productId) throws SQLException {

		List<EntTblFactoryProductLink> listLink  = new ArrayList<EntTblFactoryProductLink>();
		String productLinkJsonStr = "[]";

		if (productId != null && productId > 0) {
			listLink = productEditService.selectLinkByProductId(productId);
			productLinkJsonStr = Utils.parseObjectToJson(listLink);
		}

		form.setListLink(listLink);
		form.setFactoryProductLinkJsonStr(productLinkJsonStr);
	}

	/************************************************************************
	 * <b>Description:</b><br>
	 *  get all link reason name in rc_syouhin.mst_link_reason.
	 *
	 * @author		Tran.Thanh
	 * @date		18 Feb 2014
	 * @throws SQLException		void
	 ************************************************************************/
	private void loadInitMstLinkReason() throws SQLException {
		List<EntMstLinkReason> listLinkReason = productEditService.selectListAllLinkReason();
		form.setListMstLinkReason(listLinkReason);
	}

	/************************************************************************
	 * <b>Description:</b><br>
	 *  filter from VIEW webikesh.mst_syouhin_search.
	 *
	 * @author		Tran.Thanh
	 * @date		20 Feb 2014
	 * @return		String
	 ************************************************************************/
	@Action("/selectSyouhinSearch")
    public String selectSyouhinSearch() {
        try {
        	EntMstSyouhinSearch entQuery = form.getSyouhinSearch();
        	if (!entQuery.getSyouhinBunruiCode().isEmpty()) {
        		String [] s2 = entQuery.getSyouhinBunruiCode().split("\n");
            	if (s2 != null) {
            		List<Integer> listBunrui = new ArrayList<Integer>();
                	for (String results : s2) {
                		Integer temp = Integer.parseInt(results);
                		listBunrui.add(temp);
                    }
                	entQuery.setSyouhinBunruiList(listBunrui);
            	}
        	}
        	List<EntMstSyouhinSearch> listSyouhinSearch = productEditService.selectSyouhinSearch(entQuery);
        	if (listSyouhinSearch != null && listSyouhinSearch.size() > 0) {
    			form.setListSyouhinSearch(listSyouhinSearch);
    			form.setListSyouhinSearchJsonStr(Utils.parseObjectToJson(listSyouhinSearch));
    		}
        } catch (Exception e) {
            this.getLogger().error("[selectSyouhinAllSearch] Error when query in DB : " + e.toString());
        }

        return "list";
    }
	/* EOE Block5 */

	/* BOE Block6 */
	/************************************************************************
	 * <b>Description:</b><br>
	 *  init Video Block.
	 *
	 * @author		Tran.Thanh
	 * @date		13 Feb 2014
	 * @param 		productId productId.
	 * @throws 		SQLException		void
	 ************************************************************************/
	private void initProductVideo(Long productId) throws SQLException {

		List<EntTblFactoryProductVideo> listFactoryProductVideo = new ArrayList<EntTblFactoryProductVideo>();
		String productVideoJsonStr = "[]";

		if (productId != null && productId > 0) {
			listFactoryProductVideo = productEditService.selectVideoByProductId(productId);
			productVideoJsonStr = Utils.parseObjectToJson(listFactoryProductVideo);
		}

		form.setListFactoryProductVideo(listFactoryProductVideo);
		form.setFactoryProductVideoJsonStr(productVideoJsonStr);

	}
	/* EOE Block6 */

	/* BOE Block7 */
	/************************************************************************
	 * <b>Description:</b><br>
	 *  load Init Product Description.
	 *
	 * @author		Tran.Thanh
	 * @date		13 Feb 2014
	 * @param 		productId Long
	 * @throws 		SQLException	SQLException
	 ************************************************************************/
	private void loadInitProductDescription(Long productId) throws SQLException {

		EntTblFactoryProductDescription entProductDescription = new EntTblFactoryProductDescription();

		if (productId != null && productId > 0) {
			//Get list category
		    entProductDescription = productEditService.selectRemarksByProductId(productId);
		}

	   form.setProductDescription(entProductDescription);

	}
	/* EOE Block7 */

	/* BOE Block8 */
	/************************************************************************
	 * <b>Description:</b><br>
	 *  Load init for block 8.
	 *  Load list productGuestInput by productId.
	 *
	 * @author		Luong.Dai
	 * @date		Feb 18, 2014
	 * @param       productId	Long
	 * @throws      SQLException Integer
	 ************************************************************************/
	private void loadInitProductGuestInputInfo(Long productId) throws SQLException {
	    //Load list productGuestInput
	    List<EntTblFactoryProductGuestInput> listProductGuestInput = productEditService.selectListProductGuestInputByProductId(productId);

	    //Create json string from list
	    String jsonListProductGuestInput = Utils.parseObjectToJson(listProductGuestInput);
	    form.setLstProductGuestInputJson(jsonListProductGuestInput);
	}
	/* EOE Block8 */

	/* BOE Block9 */
	/************************************************************************
	 * <b>Description:</b><br>
	 *  Load init price of product.
	 *  Get price infor.
	 *  Get default flag.
	 *  Get list select from mst_syouhin_select
	 *
	 * @author		Luong.Dai
	 * @date		Feb 18, 2014
	 * @param       productId Long
	 * @throws      SQLException Integer
	 ************************************************************************/
	private void loadInitProductPriceInfo(Long productId) throws SQLException {
	    EntTblFactoryProductNew product = productEditService.selectProductProperAndSupplierPriceByProductId(productId);
	    form.setProductPrice(product);

	    //Get default flag
	    EntTblBrandConditionRule defaultRule = productEditService.selectDefaultFlagByProductId(product);

	    Integer siireMarumeCode = productEditService.selectSiireMarumeCodeByProductId(productId);
	    form.setMarumeCode(siireMarumeCode);

	    //Create string of default and set to form
	    if (defaultRule != null) {
	        String defaultFlagString = createStringDefaultFlag(defaultRule);
	        form.setDefaultFlagString(defaultFlagString);
	        //Set marume code to form
            form.setBrandMarumeCode(defaultRule.getBrandMarumeCode());
	    } else {
	        form.setDefaultFlagString("");
            form.setBrandMarumeCode(1);
	    }
	    //Load list select for popup
	    List<EntMstSyouhinSelect> listSelect = productEditService.selectListSyouhinSelect();
	    if (listSelect != null) {
	        form.setListSelectJson(Utils.parseObjectToJson(listSelect));
	    } else {
	        form.setListSelectJson("[]");
	    }
	}

	/************************************************************************
	 * <b>Description:</b><br>
	 *  create string of default flag.
	 *  return conditionRultBrandCode_conditionRultSort_conditionRultCategoryCode
	 * @author		Luong.Dai
	 * @date		Feb 18, 2014
	 * @param       rule EntTblBrandConditionRule
	 * @return		String
	 ************************************************************************/
	private String createStringDefaultFlag(EntTblBrandConditionRule rule) {
	    return rule.getConditionRuleBrandCode() + "_" + rule.getConditionRuleSort() + "_" + rule.getConditionRuleCategoryCode();
	}

	/************************************************************************
	 * <b>Description:</b><br>
	 *  Select price flag by entered brand code and properPrice.
	 *
	 * @author		Luong.Dai
	 * @date		Feb 18, 2014
	 * @return		String
	 * @throws      SQLException Integer
	 ************************************************************************/
	@Action("/selectAjaxPriceFlag")
	public String selectAjaxPriceFlag() throws SQLException {
	    if (form.getProductSearch() == null) {
	        return "list";
	    }

	    //Get new rule
	    EntTblBrandConditionRule rule = productEditService.selectPriceFlagByBrandCodeAndProperPrice(form.getProductSearch());
	    if (rule == null) {
	    	rule = new EntTblBrandConditionRule();
	    }

	    if (rule != null) {
	        if (rule.getConditionRuleBrandCode() != null) {
	            form.setDefaultFlagString(createStringDefaultFlag(rule));
	        }
	        //Set marume to form
	        form.setBrandMarumeCode(rule.getBrandMarumeCode());
	    }
	    form.setRule(rule);
	    return "list";
	}

	/************************************************************************
	 * <b>Description:</b><br>
	 *  select list select by productId.
	 *
	 * @author		Luong.Dai
	 * @date		Feb 21, 2014
	 * @return		String
	 ************************************************************************/
	@Action("/selectListSelectOfProduct")
	public String selectListSelectOfProductAjax() {
	    try {
	        String productId = this.getRequest().getParameter("productId");
	        if (StringUtils.isEmpty(productId)) {
	            //Create empty data and set to form
	            List<EntTblFactoryProductSelect> listSelect = new ArrayList<EntTblFactoryProductSelect>();
	            form.setListProductSelect(listSelect);
	            return "list";
	        }
	        //Select current list select of product
	        List<EntTblFactoryProductSelect> listSelect = selectListSelectOfProduct(productId);
	        form.setListProductSelect(listSelect);
	    } catch (SQLException e) {
	        this.getLogger().error("[selectListSelectOfProduct] Error when query DB");
	    }
	    return "list";
	}

	/************************************************************************
	 * <b>Description:</b><br>
	 *  select list select and product group.
	 *
	 * @author		Luong.Dai
	 * @date		Feb 19, 2014
	 * @return      String
	 * @throws      SQLException Integer
	 ************************************************************************/
	@Action("/selectListSelectProductGroup")
	public String selectListSelectProductGroup() throws SQLException {
	    String productId 				= this.getRequest().getParameter("productId");
        String productSelectCodeList 	= this.getRequest().getParameter("productSelectCodeList");
        String productGroupCode 		= this.getRequest().getParameter("productGroupCode");
        String productBrandCode 		= this.getRequest().getParameter("productBrandCode");
        String productMatterNo 			= this.getRequest().getParameter("productMatterNo");
        String duplicateProductId 		= this.getRequest().getParameter("hiddenProductIdDuplicate");

        //Get group code to form
        if (StringUtils.isEmpty(productGroupCode)) {
        	productGroupCode = "0";
        }
	    form.setGroupCode(Long.valueOf(productGroupCode));

        List<EntTblFactoryProductNew> listProduct = null;
	    if (StringUtils.isEmpty(productId)
	            || StringUtils.isEmpty(productBrandCode)
	            || StringUtils.isEmpty(productMatterNo)
	            || StringUtils.isEmpty(productGroupCode)) {
	        //Create empty data and set to form
	        List<EntTblFactoryProductSelect> listSelect = new ArrayList<EntTblFactoryProductSelect>();
	        listProduct = new ArrayList<EntTblFactoryProductNew>();
	        form.setListProductSelect(listSelect);
            form.setListProductSameGroup(listProduct);
	    } else {
    	    //Ent has value to query DB
    	    EntTblFactoryProductGeneral productGen = new EntTblFactoryProductGeneral();
    	    productGen.setProductId(Long.valueOf(productId));
    	    productGen.setProductBrandCode(Integer.valueOf(productBrandCode));
    	    productGen.setProductGroupCode(Long.valueOf(productGroupCode));
    	    productGen.setProductMatterNo(Long.valueOf(productMatterNo));

    	    listProduct = selectListProductInfoOfGroupProduct(productGen, productSelectCodeList);
    	    form.setListProductSameGroup(listProduct);
	    }
	    //Check mode: duplicate mode, new mode and edit mode
	    if (StringUtils.isNotBlank(duplicateProductId)) {
	        //Duplicate mode, add new product in top of list, copy data of root product
	        initListGroupProductForDuplicateMode(duplicateProductId, listProduct, productSelectCodeList);
	    } else if (productId.equals("-1")) {
	        //New mode with matterNo, add new product in top of list
	        EntTblFactoryProductNew product = new EntTblFactoryProductNew();
	        //New list select of product
	        List<EntTblFactoryProductSelect> select = initListSelectForProduct(productSelectCodeList);
	        product.setProductId(-1L);
	        product.setIdRow("-1");
	        product.setLstSelectOfProduct(select);
	        if (listProduct != null) {
	            //Add product to top
	            listProduct.add(0, product);
	        }
	    } else {
	        //Edit mode
	        // set first index alway is current productId
	        if (listProduct.size() > 1) {
	            for (EntTblFactoryProductNew obj: listProduct) {
	                if (obj.getProductId().equals(Long.parseLong(productId))) {
	                    //  indexOfCurrentProduct
	                    int indexOfCurrentProduct = listProduct.indexOf(obj);
	                    // obj Fist Index
	                    EntTblFactoryProductNew objFistIndex = listProduct.get(0);

	                    // set ObjectFirst to ObjectCurrent
	                    listProduct.set(indexOfCurrentProduct, objFistIndex);
	                    // set ObjectCurrent to ObjectFirst
	                    listProduct.set(0, obj);
	                    break;
	                }
	            }
	        }

	    }
	    return "list";
	}

	/************************************************************************
	 * <b>Description:</b><br>
	 *  Select information of list product in group.
	 *
	 * @author		Luong.Dai
	 * @date		Apr 1, 2014
	 * @param 		entQuery	Entity has query params
	 * @param		productSelectCodeList List option code of group
	 * @return		List<EntTblFactoryProductNew>	List product in group
	 * @throws		SQLException Exception when queyr DB
	 ************************************************************************/
	private List<EntTblFactoryProductNew> selectListProductInfoOfGroupProduct(
	                                    EntTblFactoryProductGeneral entQuery
									  , String productSelectCodeList) throws SQLException {
		List<EntTblFactoryProductNew> listProduct = null;

		//Select current list select of product
	    List<EntTblFactoryProductSelect> listSelect = new ArrayList<EntTblFactoryProductSelect>();
	    if (StringUtils.isNotEmpty(productSelectCodeList)) {
	        listSelect = productEditService.selectListSelectByCodeList(productSelectCodeList);
	    }

	    //Get list product of same group
	    listProduct = productEditService.selectListProductSameGroup(entQuery, listSelect);
	    //Select min and max properPrice in listProduct
    	selectMinAndMaxProperPrice(listProduct);

		return listProduct;
	}

	/************************************************************************
	 * <b>Description:</b><br>
	 *  Get option value of list product.
	 *
	 * @author		Luong.Dai
	 * @date		Feb 19, 2014
	 * @return      String
	 * @throws      SQLException Integer
	 ************************************************************************/
	@Action("/selectListOptionValueOfProduct")
	public String selectListOptionValueOfProduct() throws SQLException {
	    String listProductId = this.getRequest().getParameter("listProductId");
        String listSelectCode = this.getRequest().getParameter("listSelect");

        //Get group code to form
        if (StringUtils.isEmpty(listProductId)
        	|| StringUtils.isEmpty(listSelectCode)) {
        	List<EntTblFactoryProductNew> listProduct = new ArrayList<EntTblFactoryProductNew>();
            //Set listProduct to form
            form.setListProductSameGroup(listProduct);
        	return "list";
        }

        //Select current list select of product
	    List<EntTblFactoryProductSelect> listItemSelect =  productEditService.selectListSelectByCodeList(listSelectCode);
        List<EntTblFactoryProductNew> listProduct = productEditService.selectListOptionValueOfProduct(listProductId, listItemSelect);
        //Set listProduct to form
        form.setListProductSameGroup(listProduct);
        form.setListProductSelect(listItemSelect);
	    return "list";
	}

	/************************************************************************
	 * <b>Description:</b><br>
	 *  Create list select from string list select.
	 *
	 * @author		Luong.Dai
	 * @date		Mar 17, 2014
	 * @param 		productSelectCodeList	String
	 * @return		List<EntTblFactoryProductSelect>
	 ************************************************************************/
	private List<EntTblFactoryProductSelect> initListSelectForProduct(String productSelectCodeList) {
		List<EntTblFactoryProductSelect> lstSelect = new ArrayList<EntTblFactoryProductSelect>();

		if (StringUtils.isNotEmpty(productSelectCodeList)) {
			String[] listCode = productSelectCodeList.split(",");
			for (String selectCode : listCode) {
				EntTblFactoryProductSelect select = new EntTblFactoryProductSelect();
				select.setSelectCode(Integer.valueOf(selectCode));
				select.setProductSelectDisplay("");
				lstSelect.add(select);
			}
		}
		return lstSelect;
	}

	/************************************************************************
     * <b>Description:</b><br>
     *  List product of duplicate mode.
     *  Add new product in top of list.
     *  Copy data of current product to top product.
     *
     * @author		Luong.Dai
     * @date		Mar 14, 2014
     * @param       productId       String
     * @param       listProduct		ListProduct
     * @param		productSelectCodeList	list option code of product
     * @throws		SQLException	Exception when query DB
     ************************************************************************/
    private void initListGroupProductForDuplicateMode(String productId
    												, List<EntTblFactoryProductNew> listProduct
    												, String productSelectCodeList) throws SQLException {
        Long id = Long.valueOf(productId);
        if (listProduct != null) {
            //Get current product
            EntTblFactoryProductNew currentProduct = findProductInListProductByProduct(id, listProduct);
            //New product
            EntTblFactoryProductNew product = new EntTblFactoryProductNew();
            if (currentProduct == null) {
            	//Duplicate product not has in list
            	//Get product from DB
            	EntTblFactoryProductGeneral productGen = new EntTblFactoryProductGeneral();
            	productGen.setProductId(id);
            	productGen.setProductGroupCode(0L);
            	List<EntTblFactoryProductNew> listNewProduct = selectListProductInfoOfGroupProduct(productGen, productSelectCodeList);
            	currentProduct = findProductInListProductByProduct(id, listNewProduct);
            }

            //Clone product
            if (currentProduct != null) {
            	product = currentProduct.clone();
            }
            //Add product to top of list
            if (product != null) {
                //Set productId new
                product.setProductId(-1L);
                product.setIdRow("-1");
                listProduct.add(0, product);
            }
        }
    }

    /************************************************************************
     * <b>Description:</b><br>
     *  Find product in list product by productId.
     *
     * @author		Luong.Dai
     * @date		Apr 1, 2014
     * @param 		productId 		id of product
     * @param 		listProduct		List product to find
     * @return		EntTblFactoryProductNew		Product value
     ************************************************************************/
    private EntTblFactoryProductNew findProductInListProductByProduct(Long productId, List<EntTblFactoryProductNew> listProduct) {
    	EntTblFactoryProductNew productResult = null;

    	//Size of list product
         for (EntTblFactoryProductNew product : listProduct) {
             if (product.getProductId().equals(productId)) {
            	 productResult = product;
                 break;
             }
         }
    	return productResult;
    }

    /************************************************************************
	 * <b>Description:</b><br>
	 *  Select min and max proper price.
	 *
	 * @author		Luong.Dai
	 * @date		Feb 19, 2014
	 * @param listProcduct		void
	 ************************************************************************/
	private void selectMinAndMaxProperPrice(List<EntTblFactoryProductNew> listProcduct) {
	    long min = Long.MAX_VALUE;
	    long max = -1;

	    int size = listProcduct.size();
	    for (int i = 0; i < size; i++) {
	        EntTblFactoryProductNew product = listProcduct.get(i);
	        //Check properPrice < min or > max value
	        if (product.getProductProperPrice() < min) {
	            min = product.getProductProperPrice();
	        }
	        if (product.getProductProperPrice() > max) {
                max = product.getProductProperPrice();
            }
	    }

	    if (min <= max) {
	        form.setMinProperPrice(min);
	        form.setMaxProperPrice(max);
	    }
	}

	/************************************************************************
	 * <b>Description:</b><br>
	 *  select list select of product.
	 *
	 * @author		Luong.Dai
	 * @date		Feb 20, 2014
	 * @param       productId String
	 * @return		List<EntTblFactoryProductSelect>
	 * @throws      SQLException Exception
	 ************************************************************************/
	List<EntTblFactoryProductSelect> selectListSelectOfProduct(String productId) throws SQLException {
	    EntTblFactoryProductSelect select = new EntTblFactoryProductSelect();
	    //Set params for query DB
	    select.setProductId(Long.parseLong(productId));
	    select.setLimitListSelect(Constant.LIMIT_SELECT);

	    List<EntTblFactoryProductSelect> listSelect = productEditService.selectListSelectCodeByProductId(select);
	    return listSelect;
	}
	/* EOE Block9 */

	/* BOE Block10 */
	/************************************************************************
	 * <b>Description:</b><br>
	 *  Load init for Block 10 - Category.
	 *  Load category_code of product.
	 *
	 * @author		Luong.Dai
	 * @date		Feb 12, 2014
	 * @param productId		Long
	 * @throws SQLException Integer
	 ************************************************************************/
	private void loadInitCategory(Long productId) throws SQLException {
	    //Get categoryCode by productId
	    Integer categoryCode = productEditService.selectCategoryCodeOfProduct(productId);
	    form.setCategoryCode(categoryCode);

	    //Get list category
	    List<EntMstCategory> lstCategory = productEditService.selectListCategory();

	    form.setListCategory(lstCategory);
	}
	/* EOE Block10 */

	/* BOE Block11 */
	/************************************************************************
	 * <b>Description:</b><br>
	 *  initFitModel.
	 *
	 * @author		Le.Dinh
	 * @date		17 Feb 2014
	 * @param 		productId	Long
	 ************************************************************************/
	private void initFitModel(Long productId) {
		String listFitModelJsonStr = "[]";

		if (productId != null && productId > 0) {
	        //Get list model of product.
	        EntTblFactoryProductFitModel entProductFitModel = new EntTblFactoryProductFitModel();
	        entProductFitModel.setProductId(productId);
	        entProductFitModel.setSortField(Constant.FIT_MODEL_SORT_FIELD);
	        entProductFitModel.setSortDir("ASC");

	        List<EntTblFactoryProductFitModel> listFitModel = fitModelService.selectListFitModelByProductCode(entProductFitModel);
	        listFitModelJsonStr = Utils.parseObjectToJson(listFitModel);
		}

        form.setListFitModelJsonStr(listFitModelJsonStr);
	}
    /************************************************************************
     * <b>Description:</b><br>
     * process insert fit model data.
     *
     * @return      boolean
     * @param       isSingleInsert check if it is single insert
     * @author      Le.Dinh
     * @date        Jan 23, 2014
     ************************************************************************/
    private boolean validateDataInputFitModel(boolean isSingleInsert) {
        this.getLogger().debug("start validateDataInput");
        List<EntTblFactoryProductFitModel> mFList = form.getListFitModel();
        List<EntTblFactoryProductNew> productList = null;
        boolean result = false;
        int errorNum = 0;
        try {
            // check the product list is null or not
            if (form.getListProductFitModel() != null && form.getListProductFitModel().size() > 0) {
                List<Long> pl = new ArrayList<Long>();
                for (EntTblFactoryProductNew obj : form.getListProductFitModel()) {
                    // add product id to list integer
                    pl.add(obj.getProductId());
                }
                // get product list by product id list to check error flag
                // and count the number of error flg will change
                if (pl != null && pl.size() != 0) {
                    productList = productService.selectProductListByProductIdList(pl);
                }
                if (mFList != null && mFList.size() != 0) {
                    int i = 0;
                    for (EntTblFactoryProductFitModel obj : mFList) {
                        i++;
                        // check if fit model maker and fit model style is over length of error
                        if (obj.getFitModelMaker().length() > Constant.MODEL_MAKER_LENGTH
                                || obj.getFitModelModel().length() > Constant.MODEL_MODEL_LENGTH
                                || obj.getFitModelStyle().length() > Constant.MODEL_STYLE_LENGTH) {
                            result = false;
                            break;
                        } else if (obj.getFitModelMaker().length() > Constant.MAKER_LENGTH || obj.getFitModelModel().length() > Constant.MODEL_LENGTH) {
                            // check if fit model maker and fit model style is over length of warning
                            for (EntTblFactoryProductNew prd : productList) {
                                // check if error flg is 0
                                if (prd.getProductModelErrorFlg() == 0) {
                                    // then loop product list then get the match product and set error flg to 1
                                    for (EntTblFactoryProductNew prdObject : form.getListProductFitModel()) {
                                        if (prdObject.getProductId().equals(prd.getProductId())) {
                                            prdObject.setProductModelErrorFlg(1);
                                            break;
                                        }
                                    }
                                    errorNum++;
                                }
                            }
                            result = true;
                            break;
                        } else {
                            // check if loop through the fit model but found no error
                            if (i == mFList.size()) {
                                // if data is insert new fit model without delete data then no need to do the below process
                                if (!isSingleInsert) {
                                    for (EntTblFactoryProductNew prd : productList) {
                                           // check if error flg is 1
                                           if (prd.getProductModelErrorFlg() == 1) {
                                               // then loop product list then get the match product and set error flg to 0
                                               for (EntTblFactoryProductNew prdObject : form.getListProductFitModel()) {
                                                   if (prdObject.getProductId().equals(prd.getProductId())) {
                                                       prdObject.setProductModelErrorFlg(0);
                                                       break;
                                                   }
                                               }
                                               errorNum++;
                                           }
                                       }
                                       errorNum = -errorNum;
                                }
                                result = true;
                            }
                        }
                    }
                }
                EntMstFactoryMatter entMstFactoryMatter = new EntMstFactoryMatter();
                entMstFactoryMatter.setMatterFactoryErrorCountModel(errorNum);
                form.setFactoryMatter(entMstFactoryMatter);
            }
        } catch (Exception e) {
            this.getLogger().error("[validateDataInput] An error validateDataInput function");
            this.getLogger().error(e.toString());
            result = false;
            return result;
        }
        this.getLogger().debug("end validateDataInput");
        return result;
    }
    /************************************************************************
     * <b>Description:</b><br>
     * check the model maker and model model are full match with
     * maker name and model model or not.
     *
     * @author      Le.Dinh
     * @date        Feb 10, 2014
     ************************************************************************/
    private void checkModelMakerAndModelModel() {
        this.getLogger().debug("start checkModelMakerAndModelModel");
        List<EntMstMaker> makerList = null;
        List<EntMstModel> modelList = null;
        try {
            // get maker list
            makerList = fitModelService.selectMakerList();
            // get model list
            modelList = modelService.selectAllListModel();
            for (EntTblFactoryProductFitModel obj : form.getListFitModel()) {
                // check if the maker list in DB is null or empty
                if (makerList != null && makerList.size() != 0) {
                    // loop through the maker list
                    for (EntMstMaker maker: makerList) {
                        // check if the fit model maker is full match with maker name
                        // then set model maker no fit flg to 0
                        // else set to 1
                        obj.setFitModelMakerNoFitFlg(1);
                        if (StringUtils.equals(obj.getFitModelMaker().trim(), maker.getMakerName().trim())) {
                            obj.setFitModelMakerNoFitFlg(0);
                            break;
                        }
                    }
                }
                // check if the model list in DB is null or empty
                if (modelList != null && modelList.size() != 0) {
                    // loop through the model list
                    for (EntMstModel model: modelList) {
                        // check if the fit model model is full match with model name
                        // then set model model no fit flg to 0
                        // else set to 1
                        obj.setFitModelModelNoFitFlg(1);
                        if (StringUtils.equals(model.getModelName().trim(), obj.getFitModelModel().trim())) {
                            obj.setFitModelModelNoFitFlg(0);
                            break;
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.getLogger().debug("end checkModelMakerAndModelModel");
    }
	/* EOE Block11 */

	/* BOE Block12 */
	/************************************************************************
	 * <b>Description:</b><br>
	 *  initSupplier for block 12.
	 * @author		Le.Dinh
	 * @date		12 Feb 2014
	 * @param 		productId		Long
	 * @throws		SQLException	SQLException
	 ************************************************************************/
	private void initSupplier(Long productId) throws SQLException {
		List<EntTblFactoryProductSupplier> listFactoryProductSupplier = new ArrayList<EntTblFactoryProductSupplier>();
		String productSupplierJsonStr = "[]";

		// init listFactoryProductSupplier for grid 12
		if (productId != null && productId > 0) {
			listFactoryProductSupplier = productEditService.selectListSupplierByProductId(productId);
			productSupplierJsonStr = Utils.parseObjectToJson(listFactoryProductSupplier);
		}

		form.setListFactoryProductSupplier(listFactoryProductSupplier);
		form.setFactoryProductSupplierJsonStr(productSupplierJsonStr);

		// init list siire and list nouki for dialog
		List<EntMstSiire> listSiire = productEditService.selectListAllSiire();
		form.setListAllEntMstSiire(listSiire);

		List<EntMstNouki> listNouki = productEditService.selectListAllNouki();
		form.setListAllEntMstNouki(listNouki);
	}

	/* BOE #7205 Luong.Dai 2014/04/17 Add load only list siire of product */
	/************************************************************************
	 * <b>Description:</b><br>
	 *  Load list siire of product by productId.
	 *
	 * @author		Luong.Dai
	 * @date		Apr 17, 2014
	 * @param 		productId		id of product
	 * @throws 		SQLException	exception
	 ************************************************************************/
	private void loadListSiireOfproduct(Long productId) throws SQLException {
		String productSupplierJsonStr = "[]";

		// init listFactoryProductSupplier for grid 12
		if (productId != null && productId > 0) {
			List<EntTblFactoryProductSupplier> listFactoryProductSupplier = productEditService.selectListSupplierByProductId(productId);
			if (listFactoryProductSupplier != null) {
				productSupplierJsonStr = Utils.parseObjectToJson(listFactoryProductSupplier);
			}
		}

		form.setFactoryProductSupplierJsonStr(productSupplierJsonStr);
	}
	/* EOE #7205 Luong.Dai 2014/04/17 Add load only list siire of product */
	/* EOE Block12 */

	/* BOE Block13 */
	/************************************************************************
	 * <b>Description:</b><br>
	 *  initReleaseDateAndMakerSituation.
	 * @param		productId	Long
	 * @author		Le.Dinh
	 * @date		12 Feb 2014
	 * @throws		SQLException	SQLException
	 ************************************************************************/
	private void initReleaseDateAndMakerSituation(Long productId) throws SQLException {

		EntTblFactoryProductGeneral entTblFactoryProductGeneral = new EntTblFactoryProductGeneral();

		if (productId != null && productId > 0) {
			entTblFactoryProductGeneral = productEditService.selectProductFactoryProductGeneralByProductid(productId);
		}

		form.setFactoryProductGeneral(entTblFactoryProductGeneral);

		List<EntMstProductSupplierStatus> listProductSupplierStatus = productEditService.selectListAllSupplierStatus();
		form.setListProductSupplierStatus(listProductSupplierStatus);
	}
	/* EOE Block13 */

	/* BOE Block14 */
	/************************************************************************
	 * <b>Description:</b><br>
	 *  readImage.
	 * @author		Le.Dinh
	 * @date		2013/01/02
	 * @return		String
	 * @throws 		IOException IOException
	 ************************************************************************/
	@Action("/readImageDescriptionSentence")
	public String readImageDescriptionSentence() throws IOException {
		List<ImageUploadKenDo> listImageUploadKenDo = null;

		String matterNo = this.getRequest().getParameter("matterNo");
		String tmpFolder = Constant.FOLDER_TMP_IMAGES_PRODUCT_SENTENCE;
		tmpFolder += "/";
		tmpFolder += matterNo;

		listImageUploadKenDo = Utils.readImageDescriptionSentence(this.getRequest(), tmpFolder);

		form.setListImageSentenceJson(listImageUploadKenDo);
		return "readImage";
	}
	/************************************************************************
	 * <b>Description:</b><br>
	 *  upLoadImageDescriptionSentence.
	 *
	 * @author		Le.Dinh
	 * @date		2013/01/02
	 * @return		String
	 * @throws 		IOException IOException
	 ************************************************************************/
	@Action("/upLoadImageDescriptionSentence")
	public String upLoadImageDescriptionSentence() throws IOException {
		try {
			ImageUploadKenDo imageUpload = null;

			//String s = new String(URLDecoder.decode(this.getRequest().getQueryString(), "utf-8"));

			String matterNo = this.getRequest().getParameter("matterNo");
			/*BOE Tran.Thanh 2014/04/04 : get Image Name from request */

			/*EOE Tran.Thanh 2014/04/04 : get Image Name from request */
			String tmpFolder = Constant.FOLDER_TMP_IMAGES_PRODUCT_SENTENCE;
			tmpFolder += "/";
			tmpFolder += matterNo;

			// BOE @rcv!#xxx Nguyen.Chuong Aug 8, 2014 : set file upload and file name
			MultiPartRequestWrapper wrapper = (MultiPartRequestWrapper) this.getRequest();
			this.setFile(wrapper.getFiles("file")[0]);
			String imageName = wrapper.getFileNames("file")[0];
			// EOE @rcv!#xxx Nguyen.Chuong Aug 8, 2014 : set file upload and file name

			/*BOE Tran.Thanh 2014/04/03 : Change get maxSize Folder from DB */
			String valueMaxSizeFolder = settingService.selectSettingValueBySettingCode(Constant.MAX_IMAGE_FOLDER_MATTER);
			if (StringUtils.isNotEmpty(valueMaxSizeFolder)) {
				int maxSize = Integer.parseInt(valueMaxSizeFolder);
				/*BOE Tran.Thanh 2014/04/04 : get Image Name from request */
				fileFileName = imageName;
				int maxFileSizeCanUpload = Integer.parseInt(settingService.selectSettingValueBySettingCode(Constant.MAX_SIZE_UPLOAD_IMAGE));
				/*EOE Tran.Thanh 2014/04/04 : get Image Name from request */
				imageUpload = Utils.upLoadImageDescriptionSentence(this.getRequest(), tmpFolder,
												fileFileName, file, maxSize, maxFileSizeCanUpload);
				form.setImageUploadKenDo(imageUpload);
			}
			/*EOE Tran.Thanh 2014/04/03 : Change get maxSize Folder from DB */
		} catch (Exception e) {
			this.getLogger().error("[upLoadImageDescriptionSentence] error : " + e.toString());
		}
		return "upLoadImage";
	}
	/************************************************************************
	 * <b>Description:</b><br>
	 *  deleteImageDescriptionSentence.
	 * @author		Le.Dinh
	 * @date		2013/01/02
	 * @return		String
	 * @throws IOException IOException
	 ************************************************************************/
	@Action("/deleteImageDescriptionSentence")
	public String deleteImageDescriptionSentence() throws IOException {
		String fileNameToDelete = this.getRequest().getParameter("name");

		String matterNo = this.getRequest().getParameter("matterNo");
		String tmpFolder = Constant.FOLDER_TMP_IMAGES_PRODUCT_SENTENCE;
		tmpFolder += "/";
		tmpFolder += matterNo;

		Utils.deleteImageDescriptionSentence(this.getRequest(), tmpFolder, fileNameToDelete);
		return "upLoadImage";
	}

	/************************************************************************
	 * <b>Description:</b><br>
	 *  rename File Tmp Upload.
	 *
	 * @author		Tran.Thanh
	 * @date		Aug 25, 2014
	 * @return		true if rename success
	 * @throws IOException		String
	 ************************************************************************/
	@Action("/renameFileUpload")
	public String renameFileUpload() throws IOException {
		ImageUploadKenDo imageUpload = null;

		String imageNameTmp = URLDecoder.decode(this.getRequest().getParameter("imageNameTmp"), Constant.UTF_8_ENCODE);
		String imageName = URLDecoder.decode(this.getRequest().getParameter("imageName"), Constant.UTF_8_ENCODE);

		this.getLogger().error("imageNameTmp : " + imageNameTmp);
		this.getLogger().error("imageName : " + imageName);

		//imageName = URLEncoder.encode(imageName, Constant.UTF_8_ENCODE);

		String matterNo = this.getRequest().getParameter("matterNo");
		String tmpFolder = Constant.FOLDER_TMP_IMAGES_PRODUCT_SENTENCE;
		tmpFolder += "/";
		tmpFolder += matterNo;

		imageUpload = Utils.renameFileUpload(this.getRequest(), tmpFolder, imageNameTmp, imageName);

		if (imageUpload != null) {
			form.setImageUploadKenDo(imageUpload);
		}
		return "upLoadImage";
	}
	/* EOE Block14 */

	/* BOE Block15 */
	/************************************************************************
	 * <b>Description:</b><br>
	 *  Load init for block 15: Grid product.
	 *  Load comboBox matter_registration_flg.
	 *
	 * @author		Luong.Dai
	 * @date		Feb 15, 2014		void
	 ************************************************************************/
	private void loadInitForGridProduct() {
	  //Get list registration flg
        Map<String, String> listFlg = EnumRegistrationFlg.getHashMapAllFlg();
        form.setLstRegistrationFlg(listFlg);
	}
	/************************************************************************
	 * <b>Description:</b><br>
	 *  select list product for block 15.
	 *
	 * @author		Luong.Dai
	 * @date		Feb 14, 2014
	 * @return		String
	 ************************************************************************/
	@Action("/selectListProductAtProductEdit")
    public String selectListProductAtProductEdit() {
        try {
            //Get page and pageSize from params
            String page = this.getRequest().getParameter(Constant.PARAM_PAGE_NUMBER);
            String pageSize = this.getRequest().getParameter(Constant.PARAM_PAGE_SIZE);
            //If page and pageSize in actionForm is not null => param page sent from matterDetail. Get page map with matterDetail.
            if (null != form.getPage() && null != form.getPageSize()) {
                page = form.getPage().toString();
                pageSize = form.getPageSize().toString();
            }
            // Check missing params pageSize and pageNumber
            if (StringUtils.isEmpty(pageSize) || StringUtils.isEmpty(page)) {
                // Return page error params
                return "list";
            }
            //Parse page and pageSize to integer
            int pageSizeInt = Integer.parseInt(pageSize);
            int pageInt = Integer.parseInt(page);
            //Get list product for block 15
            List<EntTblFactoryProductNew> listProduct = productEditService.selectAllListProduct(pageInt, pageSizeInt, form.getProductSearch());
            form.setListProduct(listProduct);
            //Get total product for pagging in list 15
            Integer totalProduct = productEditService.selectCountListProduct(form.getProductSearch());
            form.setTotalListProduct(totalProduct);
            //Get total product of matter
            Integer totalProductOfMatter = productEditService.selectCountNumberProductOfMatter(form.getProductSearch().getProductMatterNo());
            form.setTotalProductOfMatter(totalProductOfMatter);
        } catch (SQLException e) {
            this.getLogger().error("[selectListProductAtProductEdit] Error when query in DB");
        }

        return "list";
    }

	/************************************************************************
	 * <b>Description:</b><br>
	 *  Save data of block 15 to DB.
	 *
	 * @author		Luong.Dai
	 * @date		Mar 12, 2014
	 * @return		String
	 ************************************************************************/
	@Action("/saveListProductDataAtProductEdit")
    public String saveListProductDataAtProductEdit() {
        try {
            //Validate user login
            if (!validateLoginUser()) {
                return "errorOther";
            }
            //Get list product
            List<EntTblFactoryProductNew> listTblFactoryProductNew = this.initEntTblFactoryProductNewList();
            String matterNo = this.getRequest().getParameter("matterNo");
            String loginId = this.getCommonForm().getLoginId();
            //Save data to DB
            Boolean result = productEditService.saveDataOfListProductAtProductEdit(listTblFactoryProductNew, loginId, matterNo);
            //Init message
            //initMsgCode
            initMsgCode(result);
            // initMsgString
            initMsgString();
            //Set data to map
            responeMapAjaxSaveData = new HashMap<Object, Object>();
            responeMapAjaxSaveData.put(Constant.SESSION_MSG_CODE, form.getMsgCode());
            responeMapAjaxSaveData.put(Constant.MSG_STRING,       form.getMsgString());
        } catch (Exception e) {
            this.getLogger().error("[saveListProductDataAtProductEdit] Error when excute function");
        }

        return "processSaveData";
    }
	/* BOE #7205 Luong.Dai load data of product when click in grid block 15 */
	/************************************************************************
	 * <b>Description:</b><br>
	 *  Load data for selected product in block 15.
	 *
	 * @author		Luong.Dai
	 * @date		Apr 16, 2014
	 * @return		String
	 * @throws		SQLException exception
	 ************************************************************************/
	@Action("/loadDataOfSelectedProduct")
    public String loadDataOfSelectedProduct() throws SQLException {
		try {
			String productIdParam = this.getRequest().getParameter("productId");
			Long productId = Long.valueOf(productIdParam);
			//Load product by productId
			initProductDataForSelectedProduct(productId);
			if (form.getProduct() != null) {
				//Load image
				initListImageThumb(Long.valueOf(productIdParam));
				//Load link
				loadInitProductLink(productId);
				//Load list video
				initProductVideo(productId);
				//Description remarks
				loadInitProductDescription(productId);
				/* BOE Luong.Dai fix bug null pointer when load description */
				if (form.getProductDescription() == null) {
					form.setProductDescription(new EntTblFactoryProductDescription());
				}
				/* EOE Luong.Dai fix bug null pointer when load description */
				//Load guest input
				loadInitProductGuestInputInfo(productId);
				//Load releaseDate and situation
				initReleaseDateAndMakerSituation(productId);
				//Load list siire of product
				loadListSiireOfproduct(productId);
				//Load fit model
				initFitModel(productId);
				//Load categoryCode
				loadInitCategory(productId);
				//Load price for block 9
				loadInitProductPriceInfo(productId);
			}
		} catch (NumberFormatException e) {
			this.getLogger().error("[loadDataOfSelectedProduct] Cannot get data of selected product!");
		}
		return "list";
	}

	/************************************************************************
	 * <b>Description:</b><br>
	 *  init product data.
	 *
	 * @author		Luong.Dai
	 * @date		Apr 16, 2014
	 * @param 		productId	productId
	 ************************************************************************/
	private void initProductDataForSelectedProduct(Long productId) {
		//Set mode
		form.setMode(Constant.EDIT_MODE);
		//Get product data
		EntTblFactoryProductNew product = productEditService.checkExistProduct(productId);

        if (product != null) {
            //Product exists in DB, set value to form
            form.setProduct(product);
            form.setProductJson(Utils.parseObjectToJson(product)); // Add by Tran.Thanh 20140224
            //Set productId to form
            form.setProductId(productId);
            //Set productMatterNo
            form.setProductMatterNo(product.getProductMatterNo());
        }
	}
	/* EOE #7205 Luong.Dai load data of product when click in grid block 15 */

	/* BOE #7363 Tran.Thanh save data popup block 15 to DB */
	/************************************************************************
	 * <b>Description:</b><br>
	 *  saveProductValueToDB.
	 *
	 * @author		Tran.Thanh
	 * @date		Apr 18, 2014
	 * @return		boolean process SaveData
	 * @throws SQLException		String
	 ************************************************************************/
	@Action("/saveProductValueToDB")
	public String saveProductValueToDB() throws SQLException {
        try {
            //Validate user login
            if (!validateLoginUser()) {
                return "errorOther";
            }
            String listProductId = this.getRequest().getParameter("listProductId");
            List<String> lstProductId = parseJsonProductIdBlock15ToListString(listProductId);

            //Check checkbox selectall is checked
            EntTblFactoryProductNew entProductSearch = null;
            if (lstProductId.get(0).contains("on")) {
            	// Get Filter
            	entProductSearch = form.getProductSearch();
            	lstProductId.remove(0);
            }

            //convert ListString To ListLong
            List<Long> lstlongProductId = new ArrayList<Long>();
            for (String productId : lstProductId) {
            	lstlongProductId.add(Long.parseLong(productId));
            }

			String strJson = this.getRequest().getParameter("jsonObj");
			List<EntReplaceElement> listObjPopupBlock15 = parseJsonPopupBlock15ToEnt(strJson);

			// Validate and count error flag before Save Proces

			// Get update user id
			String updateUserId = "";
			if (this.getCommonForm().getLoginId() != null) {
				updateUserId = this.getCommonForm().getLoginId();
			}

			boolean res = productEditService.saveDataPopupBlock15(listObjPopupBlock15
																, lstlongProductId
																, entProductSearch
																, updateUserId);
			//Init message
            //initMsgCode
            initMsgCode(res);
            // initMsgString
            initMsgString();
            //Set data to map
            responeMapAjaxSaveData = new HashMap<Object, Object>();
            responeMapAjaxSaveData.put(Constant.SESSION_MSG_CODE, form.getMsgCode());
            responeMapAjaxSaveData.put(Constant.MSG_STRING,       form.getMsgString());
        } catch (UnexpectedRollbackException rtEx) {
        	this.getLogger().error("[saveProductValueToDB] Error when excute function");
            this.getLogger().error(rtEx.toString());
            //initMsgCode
            initMsgCode(false);
            // initMsgString
            initMsgString();
            //Set data to map
            responeMapAjaxSaveData = new HashMap<Object, Object>();
            responeMapAjaxSaveData.put(Constant.SESSION_MSG_CODE, form.getMsgCode());
            responeMapAjaxSaveData.put(Constant.MSG_STRING,       "timeOutException");
        } catch (Exception e) {
            this.getLogger().error("[saveProductValueToDB] Error when excute function");
            this.getLogger().error(e.toString());
            //initMsgCode
            initMsgCode(false);
            // initMsgString
            initMsgString();
            //Set data to map
            responeMapAjaxSaveData = new HashMap<Object, Object>();
            responeMapAjaxSaveData.put(Constant.SESSION_MSG_CODE, form.getMsgCode());
            responeMapAjaxSaveData.put(Constant.MSG_STRING,       form.getMsgString());
        }
		return "processSaveData";
	}

	/************************************************************************
	 * <b>Description:</b><br>
	 *  parse Json Popup Block 15 To EntReplaceElement.
	 *
	 * @author		Tran.Thanh
	 * @date		Apr 21, 2014
	 * @param 		jsonString String json popup block 15
	 * @return		List<EntReplaceElement>
	 ************************************************************************/
	@SuppressWarnings("unchecked")
	private List<EntReplaceElement> parseJsonPopupBlock15ToEnt(String jsonString) {
		List<EntReplaceElement> res = null;
		Type listEntPopupBlock15 = new TypeToken<List<EntReplaceElement>>() { } .getType();
		 // convert string json to list.
		res = Utils.parseJsonStringToList(jsonString, listEntPopupBlock15);
		if (res == null || res.size() < 1) {
			res = new ArrayList<EntReplaceElement>();
        }
		return res;
	}

	/************************************************************************
	 * <b>Description:</b><br>
	 *  parse JsonProductId Block15 To List String.
	 *
	 * @author		Tran.Thanh
	 * @date		Apr 21, 2014
	 * @param 		jsonString json productId
	 * @return		List<String> listString productId
	 ************************************************************************/
	@SuppressWarnings("unchecked")
	private List<String> parseJsonProductIdBlock15ToListString(String jsonString) {
		List<String> res = null;
		Type listString = new TypeToken<List<String>>() { } .getType();
		 // convert string json to list.
		res = Utils.parseJsonStringToList(jsonString, listString);
		if (res == null || res.size() < 1) {
			res = new ArrayList<String>();
        }
		return res;
	}
	/* BOE #7363 Tran.Thanh save data popup block 15 to DB */

	/************************************************************************
	 * <b>Description:</b><br>
	 * Count release product of grid 15.
	 *
	 * @author		Luong.Dai
	 * @date		May 12, 2014
	 * @return		String
	 ************************************************************************/
	@Action("/checkReleaseProductOfMatter")
	public String checkReleaseProductOfMatter() {
		//Count number release product by filter
		Integer numberReleaseProduct = productEditService.countNumberReleasseProduct(form.getProductSearch());
		form.setNumberReleaseProduct(numberReleaseProduct);
		return "countReleaseProduct";
	}

	/************************************************************************
	 * <b>Description:</b><br>
	 *  Delete all product not release with filter.
	 *
	 * @author		Luong.Dai
	 * @date		May 12, 2014
	 * @return		String
	 ************************************************************************/
	@Action("/deleteAllProductNotReleasedWithFilter")
	public String deleteAllProductNotReleasedWithFilter() {
		//object respone to client
		responeMapAjaxDeleData = new HashMap<Object, Object>();
		try {
			List<Long> lstProductId = productEditService.selectAllProductIdNotReleaseWithFilter(form.getProductSearch());

			if (lstProductId.size() == 0) {
				//No product to delete
				responeMapAjaxDeleData.put(Constant.DELETE_STATUS, Constant.NO_PRODUCT_TO_DELETE);
				return "deleteSuccess";
			}

			//get login ID
			Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
	        String loginId = getLoginId(authentication);

	        //Get list product syouhin system code
			List<Long> lstSyouhinSysCode = productEditService.selectListSyouhinSysCode(lstProductId);

	        //Count error of product
			EntMstFactoryMatterNew entMatter = productEditService.countErrorOfListProduct(lstProductId
			                                                                            , form.getProductSearch().getProductMatterNo()
			                                                                            , loginId);

			//Delete all list product
			boolean result = productEditService.deleteAllProductNotReleasedWithFilter(lstProductId
			                                                                        , entMatter
			                                                                        , lstSyouhinSysCode
			                                                                        , loginId);

			if (result) {
				//Success
				responeMapAjaxDeleData.put(Constant.DELETE_STATUS, Constant.DELETE_SUCCESS);
			} else {
				//Cannot delete
				responeMapAjaxDeleData.put(Constant.DELETE_STATUS, Constant.DELETE_FAIL);
			}
		} catch (SQLException e) {
			//Log bug
			this.getLogger().error("[deleteAllProductNotReleasedWithFilter] Error when excute function");
            this.getLogger().error(e.toString());

            //Set status for client
			responeMapAjaxDeleData.put(Constant.DELETE_STATUS, Constant.DELETE_FAIL);
		}

		return "deleteSuccess";
	}
	/* EOE Block15 */

	/**
	 * @return the form
	 */
	public ProductEditActionForm getForm() {
		return form;
	}

	/**
	 * @param form
	 *            the form to set
	 */
	public void setForm(ProductEditActionForm form) {
		this.form = form;
	}

	/**
	 * @return the file
	 */
	public File getFile() {
		return file;
	}

	/**
	 * @param file the file to set
	 */
	public void setFile(File file) {
		this.file = file;
	}

	/**
	 * @return the fileFileName
	 */
	public String getFileFileName() {
		return fileFileName;
	}

	/**
	 * @param fileFileName the fileFileName to set
	 */
	public void setFileFileName(String fileFileName) {
		this.fileFileName = fileFileName;
	}

	@Override
	public void setSession(Map<String, Object> session) {
		this.session = session;
	}

	/**
	 * @return the responeMapAjaxSaveData
	 */
	public Map<Object, Object> getResponeMapAjaxSaveData() {
		return responeMapAjaxSaveData;
	}

	/**
	 * @param responeMapAjaxSaveData the responeMapAjaxSaveData to set
	 */
	public void setResponeMapAjaxSaveData(Map<Object, Object> responeMapAjaxSaveData) {
		this.responeMapAjaxSaveData = responeMapAjaxSaveData;
	}

	/**
	 * @return the responeMapAjaxDeleData
	 */
	public Map<Object, Object> getResponeMapAjaxDeleData() {
		return responeMapAjaxDeleData;
	}
}
