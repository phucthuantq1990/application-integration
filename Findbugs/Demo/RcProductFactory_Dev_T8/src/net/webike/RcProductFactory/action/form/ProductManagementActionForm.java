/************************************************************************
 * File Name	： ProductManagementActionForm.java
 * Author		： Nguyen.Chuong
 * Version		： 1.0.0
 * Date Created	： 2014/02/14
 * Date Updated	： 2014/02/14
 * Description	： store data of productManagement page.
 ***********************************************************************/
package net.webike.RcProductFactory.action.form;

import java.util.List;
import java.util.Map;

import net.webike.RcProductFactory.entity.EntMstAttribute;
import net.webike.RcProductFactory.entity.EntMstAttributeGroup;
import net.webike.RcProductFactory.entity.EntMstBrand;
import net.webike.RcProductFactory.entity.EntMstCategory;
import net.webike.RcProductFactory.entity.EntMstFactoryMatter;
import net.webike.RcProductFactory.entity.EntMstFactoryUserTmp;
import net.webike.RcProductFactory.entity.EntMstNouki;
import net.webike.RcProductFactory.entity.EntMstProduct;
import net.webike.RcProductFactory.entity.EntMstProductSupplierStatus;
import net.webike.RcProductFactory.entity.EntMstSiire;
import net.webike.RcProductFactory.entity.EntMstSyouhinSelect;
import net.webike.RcProductFactory.entity.EntTblFactoryImportCondition;
import net.webike.RcProductFactory.entity.EntTblProductGuestInput;
import net.webike.RcProductFactory.entity.EntTblProductLink;
import net.webike.RcProductFactory.entity.EntTblProductVideo;
import net.webike.RcProductFactory.entity.EntTblSyouhinFitModel;
import net.webike.RcProductFactory.entity.EntTblSyouhinImage;
import net.webike.RcProductFactory.util.Constant;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * store data of product management.
 */
@Component
@Scope("request")
public class ProductManagementActionForm {
    private List<EntMstProduct> prdList;
    private EntMstProduct entMstProduct;
    /**
     * Attribute list.
     */
    private List<EntMstAttribute> entMstAttributeList;

    private List<EntMstSiire> listSiireNouki;
    private EntMstFactoryMatter entMstFactoryMatter;
    //Link no image from constant
    private String noImageURL = Constant.NO_IMAGE_URL;
    private List<EntTblSyouhinImage> listSyouhinImage;
    private List<EntTblSyouhinFitModel> listSyouhinFitModel;
    private List<EntMstAttribute> listAttributeAllType;
    private List<EntTblProductLink> listLinkReason;
    private List<EntTblProductVideo> listVideo;
    private List<EntTblProductGuestInput> listGuestInput;
    // list to store only productId (syouhinSysCode)
    private List<Long> prdIdList;
    private List<String> fullNameList;
    private List<String> productStatusNameList;
    private List<EntMstBrand> brandList;
    private List<EntMstCategory> categoryList;
    private List<EntMstSiire> siireList;
    private List<EntMstSyouhinSelect> syouhinSelectList;
    private List<EntMstNouki> noukiList;
    private List<EntMstAttribute> attributeList;
    private List<EntMstProductSupplierStatus> productSupplierStatusList;
    private List<EntMstFactoryUserTmp> userList;
//    private List<EnumVariousFlag> variousFlagList;
    private List<Map<String, String>> variousFlagList;
    private List<EntMstAttributeGroup> attributeGroupList;

    private List<ProductDeleteFlag> productDeleteFlagList;
    private List<EntTblFactoryImportCondition> entTblFactoryImportConditionList;
    private EntTblFactoryImportCondition entTblFactoryImportCondition;
    // matter code
    private long matterNo;
    // to check matterNo is from DB or not
    // 1: from DB, 0: not from DB
    private int isEnteredMatterNo;

    // page number
    private int pageSize;
    // number of data in 1 page
    private int page;
    // total record of grid
    private int totalRecord;
    // flag to check the product maintenance validate mode
    private int productValidateMode;
    private int maintenanceFlg;
    private String prdIdStringList;
    private String errorMode;

    private String headers;
    private String syouhinSysCodes;
    private String attributeCodes;
    private Boolean allCheckBoxValue;
    /** -1 : ignore check, 0 : all products are OK - 1 : some products is OK - 2 : no product is OK. */
    private int checkStatus;

    /**
     * Product delete flag (mst_product.product_del_flg).
     * @author		nguyen.hieu
     * @date		2014-02-20
     */
    public static final class ProductDeleteFlag {
    	private int value;
    	private String name;

    	/**
    	 * Construct ProductDeleteFlag.
    	 * @param value Value
    	 * @param name Name
    	 */
    	public ProductDeleteFlag(final int value, final String name) {
    		this.value = value;
    		this.name = name;
    	}

    	public String getName() {
			return name;
		}

    	public int getValue() {
			return value;
		}
    }

	/**
     * @return the entMstProduct
     */
    public EntMstProduct getEntMstProduct() {
        return entMstProduct;
    }
    /**
     * @param entMstProduct the entMstProduct to set
     */
    public void setEntMstProduct(EntMstProduct entMstProduct) {
        this.entMstProduct = entMstProduct;
    }
    /**
     * @return the listSiireNouki
     */
    public List<EntMstSiire> getListSiireNouki() {
        return listSiireNouki;
    }
    /**
     * @param listSiireNouki the listSiireNouki to set
     */
    public void setListSiireNouki(List<EntMstSiire> listSiireNouki) {
        this.listSiireNouki = listSiireNouki;
    }
    /**
     * @return the listSyouhinImage
     */
    public List<EntTblSyouhinImage> getListSyouhinImage() {
        return listSyouhinImage;
    }
    /**
     * @param listSyouhinImage the listSyouhinImage to set
     */
    public void setListSyouhinImage(List<EntTblSyouhinImage> listSyouhinImage) {
        this.listSyouhinImage = listSyouhinImage;
    }
    /**
     * @return the pageSize
     */
    public int getPageSize() {
        return pageSize;
    }
    /**
     * @param pageSize the pageSize to set
     */
    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }
    /**
     * @return the page
     */
    public int getPage() {
        return page;
    }
    /**
     * @param page the page to set
     */
    public void setPage(int page) {
        this.page = page;
    }
    /**
     * @return the totalRecord
     */
    public int getTotalRecord() {
        return totalRecord;
    }
    /**
     * @param totalRecord the totalRecord to set
     */
    public void setTotalRecord(int totalRecord) {
        this.totalRecord = totalRecord;
    }
    /**
     * @return the prdList
     */
    public List<EntMstProduct> getPrdList() {
        return prdList;
    }
    /**
     * @param prdList the prdList to set
     */
    public void setPrdList(List<EntMstProduct> prdList) {
        this.prdList = prdList;
    }
    /**
     * @return the listSyouhinFitModel
     */
    public List<EntTblSyouhinFitModel> getListSyouhinFitModel() {
        return listSyouhinFitModel;
    }
    /**
     * @param listSyouhinFitModel the listSyouhinFitModel to set
     */
    public void setListSyouhinFitModel(List<EntTblSyouhinFitModel> listSyouhinFitModel) {
        this.listSyouhinFitModel = listSyouhinFitModel;
    }
    /**
     * @return the listAttributeAllType
     */
    public List<EntMstAttribute> getListAttributeAllType() {
        return listAttributeAllType;
    }
    /**
     * @param listAttributeAllType the listAttributeAllType to set
     */
    public void setListAttributeAllType(List<EntMstAttribute> listAttributeAllType) {
        this.listAttributeAllType = listAttributeAllType;
    }
    /**
     * @return the listVideo
     */
    public List<EntTblProductVideo> getListVideo() {
        return listVideo;
    }
    /**
     * @param listVideo the listVideo to set
     */
    public void setListVideo(List<EntTblProductVideo> listVideo) {
        this.listVideo = listVideo;
    }
    /**
     * @return the noImageURL
     */
    public String getNoImageURL() {
        return noImageURL;
    }
    /**
     * @param noImageURL the noImageURL to set
     */
    public void setNoImageURL(String noImageURL) {
        this.noImageURL = noImageURL;
    }
    /**
     * @return the entMstFactoryMatter
     */
    public EntMstFactoryMatter getEntMstFactoryMatter() {
        return entMstFactoryMatter;
    }
    /**
     * @param entMstFactoryMatter the entMstFactoryMatter to set
     */
    public void setEntMstFactoryMatter(EntMstFactoryMatter entMstFactoryMatter) {
        this.entMstFactoryMatter = entMstFactoryMatter;
    }
    /**
     * @return the prdIdList
     */
    public List<Long> getPrdIdList() {
        return prdIdList;
    }
    /**
     * @param prdIdList the prdIdList to set
     */
    public void setPrdIdList(List<Long> prdIdList) {
        this.prdIdList = prdIdList;
    }
    /**
     * @return the fullNameList
     */
    public List<String> getFullNameList() {
        return fullNameList;
    }
    /**
     * @param fullNameList the fullNameList to set
     */
    public void setFullNameList(List<String> fullNameList) {
        this.fullNameList = fullNameList;
    }
    /**
     * @return the productStatusNameList
     */
    public List<String> getProductStatusNameList() {
        return productStatusNameList;
    }
    /**
     * @param productStatusNameList the productStatusNameList to set
     */
    public void setProductStatusNameList(List<String> productStatusNameList) {
        this.productStatusNameList = productStatusNameList;
    }
    /**
     * @return the brandList
     */
    public List<EntMstBrand> getBrandList() {
        return brandList;
    }
    /**
     * @param brandList the brandList to set
     */
    public void setBrandList(List<EntMstBrand> brandList) {
        this.brandList = brandList;
    }
    /**
     * @return the categoryList
     */
    public List<EntMstCategory> getCategoryList() {
        return categoryList;
    }
    /**
     * @param categoryList the categoryList to set
     */
    public void setCategoryList(List<EntMstCategory> categoryList) {
        this.categoryList = categoryList;
    }
    /**
     * @return the siireList
     */
    public List<EntMstSiire> getSiireList() {
        return siireList;
    }
    /**
     * @param siireList the siireList to set
     */
    public void setSiireList(List<EntMstSiire> siireList) {
        this.siireList = siireList;
    }
    /**
     * @return the syouhinSelectList
     */
    public List<EntMstSyouhinSelect> getSyouhinSelectList() {
        return syouhinSelectList;
    }
    /**
     * @param syouhinSelectList the syouhinSelectList to set
     */
    public void setSyouhinSelectList(List<EntMstSyouhinSelect> syouhinSelectList) {
        this.syouhinSelectList = syouhinSelectList;
    }
    /**
     * @return the noukiList
     */
    public List<EntMstNouki> getNoukiList() {
        return noukiList;
    }
    /**
     * @param noukiList the noukiList to set
     */
    public void setNoukiList(List<EntMstNouki> noukiList) {
        this.noukiList = noukiList;
    }
    /**
     * @return the attributeList
     */
    public List<EntMstAttribute> getAttributeList() {
        return attributeList;
    }
    /**
     * @param attributeList the attributeList to set
     */
    public void setAttributeList(List<EntMstAttribute> attributeList) {
        this.attributeList = attributeList;
    }
	/**
	 * @return the productSupplierStatusList
	 */
	public List<EntMstProductSupplierStatus> getProductSupplierStatusList() {
		return productSupplierStatusList;
	}
	/**
	 * @param productSupplierStatusList the productSupplierStatusList to set
	 */
	public void setProductSupplierStatusList(
			List<EntMstProductSupplierStatus> productSupplierStatusList) {
		this.productSupplierStatusList = productSupplierStatusList;
	}

	/**
	 * @return the userList
	 */
	public List<EntMstFactoryUserTmp> getUserList() {
		return userList;
	}
	/**
	 * @param userList the userList to set
	 */
	public void setUserList(List<EntMstFactoryUserTmp> userList) {
		this.userList = userList;
	}
	/**
	 * @return the productDeleteFlagList
	 */
	public List<ProductDeleteFlag> getProductDeleteFlagList() {
		return productDeleteFlagList;
	}
	/**
	 * @param productDeleteFlagList the productDeleteFlagList to set
	 */
	public void setProductDeleteFlagList(
			List<ProductDeleteFlag> productDeleteFlagList) {
		this.productDeleteFlagList = productDeleteFlagList;
	}
    /**
     * @return the listLinkReason
     */
    public List<EntTblProductLink> getListLinkReason() {
        return listLinkReason;
    }
    /**
     * @param listLinkReason the listLinkReason to set
     */
    public void setListLinkReason(List<EntTblProductLink> listLinkReason) {
        this.listLinkReason = listLinkReason;
    }
    /**
     * @return the listGuestInput
     */
    public List<EntTblProductGuestInput> getListGuestInput() {
        return listGuestInput;
    }
    /**
     * @param listGuestInput the listGuestInput to set
     */
    public void setListGuestInput(List<EntTblProductGuestInput> listGuestInput) {
        this.listGuestInput = listGuestInput;
    }

	/**
	 * @param variousFlagList the variousFlagList to set
	 */
	public void setVariousFlagList(List<Map<String, String>> variousFlagList) {
		this.variousFlagList = variousFlagList;
	}

	/**
	 * @return the attributeGroupList
	 */
	public List<EntMstAttributeGroup> getAttributeGroupList() {
		return attributeGroupList;
	}
	/**
	 * @param attributeGroupList the attributeGroupList to set
	 */
	public void setAttributeGroupList(List<EntMstAttributeGroup> attributeGroupList) {
		this.attributeGroupList = attributeGroupList;
	}

	/**
	 * @return the variousFlagList
	 */
	public List<Map<String, String>> getVariousFlagList() {
		return variousFlagList;
	}
    /**
     * @return the matterNo
     */
    public long getMatterNo() {
        return matterNo;
    }
    /**
     * @param matterNo the matterNo to set
     */
    public void setMatterNo(long matterNo) {
        this.matterNo = matterNo;
    }
    /**
     * @return the isEnteredMatterNo
     */
    public int getIsEnteredMatterNo() {
        return isEnteredMatterNo;
    }
    /**
     * @param isEnteredMatterNo the isEnteredMatterNo to set
     */
    public void setIsEnteredMatterNo(int isEnteredMatterNo) {
        this.isEnteredMatterNo = isEnteredMatterNo;
    }
    /**
     * @return the entTblFactoryImportConditionList
     */
    public List<EntTblFactoryImportCondition> getEntTblFactoryImportConditionList() {
        return entTblFactoryImportConditionList;
    }
    /**
     * @param entTblFactoryImportConditionList the entTblFactoryImportConditionList to set
     */
    public void setEntTblFactoryImportConditionList(List<EntTblFactoryImportCondition> entTblFactoryImportConditionList) {
        this.entTblFactoryImportConditionList = entTblFactoryImportConditionList;
    }
    /**
     * @return the entTblFactoryImportCondition
     */
    public EntTblFactoryImportCondition getEntTblFactoryImportCondition() {
        return entTblFactoryImportCondition;
    }
    /**
     * @param entTblFactoryImportCondition the entTblFactoryImportCondition to set
     */
    public void setEntTblFactoryImportCondition(EntTblFactoryImportCondition entTblFactoryImportCondition) {
        this.entTblFactoryImportCondition = entTblFactoryImportCondition;
    }
    public List<EntMstAttribute> getEntMstAttributeList() {
        return entMstAttributeList;
    }
    public void setEntMstAttributeList(List<EntMstAttribute> entMstAttributeList) {
        this.entMstAttributeList = entMstAttributeList;
    }
    /**
     * @return the productValidateMode
     */
    public int getProductValidateMode() {
        return productValidateMode;
    }
    /**
     * @param productValidateMode the productValidateMode to set
     */
    public void setProductValidateMode(int productValidateMode) {
        this.productValidateMode = productValidateMode;
    }
    /**
     * @return the maintenanceFlg
     */
    public int getMaintenanceFlg() {
        return maintenanceFlg;
    }
    /**
     * @param maintenanceFlg the maintenanceFlg to set
     */
    public void setMaintenanceFlg(int maintenanceFlg) {
        this.maintenanceFlg = maintenanceFlg;
    }
    /**
     * @return the prdIdStringList
     */
    public String getPrdIdStringList() {
        return prdIdStringList;
    }
    /**
     * @param prdIdStringList the prdIdStringList to set
     */
    public void setPrdIdStringList(String prdIdStringList) {
        this.prdIdStringList = prdIdStringList;
    }
    /**
     * @return the errorMode
     */
    public String getErrorMode() {
        return errorMode;
    }
    /**
     * @param errorMode the errorMode to set
     */
    public void setErrorMode(String errorMode) {
        this.errorMode = errorMode;
    }
    /**
     * @return the headers
     */
    public String getHeaders() {
        return headers;
    }
    /**
     * @param headers the headers to set
     */
    public void setHeaders(String headers) {
        this.headers = headers;
    }
    /**
     * @return the syouhinSysCodes
     */
    public String getSyouhinSysCodes() {
        return syouhinSysCodes;
    }
    /**
     * @param syouhinSysCodes the syouhinSysCodes to set
     */
    public void setSyouhinSysCodes(String syouhinSysCodes) {
        this.syouhinSysCodes = syouhinSysCodes;
    }
    /**
     * @return the attributeCodes
     */
    public String getAttributeCodes() {
        return attributeCodes;
    }
    /**
     * @param attributeCodes the attributeCodes to set
     */
    public void setAttributeCodes(String attributeCodes) {
        this.attributeCodes = attributeCodes;
    }
    /**
     * @return the allCheckBoxValue
     */
    public Boolean getAllCheckBoxValue() {
        return allCheckBoxValue;
    }
    /**
     * @param allCheckBoxValue the allCheckBoxValue to set
     */
    public void setAllCheckBoxValue(Boolean allCheckBoxValue) {
        this.allCheckBoxValue = allCheckBoxValue;
    }
    /**
     * @return the checkStatus
     */
    public int getCheckStatus() {
        return checkStatus;
    }
    /**
     * @param checkStatus the checkStatus to set
     */
    public void setCheckStatus(int checkStatus) {
        this.checkStatus = checkStatus;
    }
}
