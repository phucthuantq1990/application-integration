/************************************************************************
 * File Name	： CategoryEditAction.java
 * Author		： hoang.ho
 * Version		： 1.0.0
 * Date Created	： 2014/01/07
 * Date Updated	： 2014/01/07
 * Description	： action to edit category list.
 *
 ***********************************************************************/
package net.webike.RcProductFactory.action;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import net.webike.RcProductFactory.action.form.CategoryEditActionForm;
import net.webike.RcProductFactory.entity.EntMstAttribute;
import net.webike.RcProductFactory.entity.EntMstBunrui;
import net.webike.RcProductFactory.entity.EntMstCategory;
import net.webike.RcProductFactory.entity.EntTblCategoryAttribute;
import net.webike.RcProductFactory.service.CategoryService;
import net.webike.RcProductFactory.util.Constant;
import net.webike.RcProductFactory.util.Utils;

import org.apache.commons.lang.StringUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.apache.struts2.interceptor.SessionAware;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;

import com.opensymphony.xwork2.ModelDriven;

/**
 * action of edit category list.
 * @author hoang.ho
 * Date Created	： 2014/01/07
 */
@Scope("request")
@ParentPackage(value = "CommonInterceptor")
@Results(
    {
        // On loading default matter detail page
        @Result(name = "success", location = "categoryEdit.jsp"),
        // Return error page when error other.
        @Result(name = "errorOther", type = "redirect", location = "error.html"),
        @Result(name = "updateError", type = "redirectAction", location = "categoryEdit.html",
                params = {"insertMode", "${insertModeParam}", "categoryCode", "${categoryCodeParam}",
                          "msgCode", "${messageParam}" }),
        @Result(name = "insertError", type = "redirectAction", location = "categoryEdit.html",
                params = {"msgCode", "${messageParam}" }),
        @Result(name = "categoryManage", type = "redirectAction", location = "categoryManage.html",
                params = {"msgCode", "${messageParam}" })
    }
)
public class CategoryEditAction extends CommonAction implements ModelDriven<CategoryEditActionForm>, SessionAware {

    private static final long serialVersionUID = 1L;

    @Autowired
    private CategoryEditActionForm form;

    @Autowired
    private CategoryService categoryService;

    /** these params use to pass parm to url when return from insert mode.*/
    private String insertModeParam;
    private String categoryCodeParam;
    private String messageParam;

    // session for store old list of category_attributes.
    private Map<String, Object> session;

    /************************************************************************
     * <b>Description:</b><br>
     * default load page.
     *
     * @author      Long Vu
     * @date        Jan 07, 2014
     * @return      String
     ************************************************************************/
    @Override
    @Action("/categoryEdit")
    public String execute() {
        try {
            //Validate loginUser
            if (!validateLoginUser()) {
                return "errorOther";
            }
            initMessage();
            initInsertMode();
            if (form.getInsertMode() == 0) {
                int categoryCode = initCategoryCode();
                if (categoryCode == 0) {
                    return "errorOther";
                }
                EntMstCategory category = categoryService.selectCategoryByCode(categoryCode);
                List<EntTblCategoryAttribute> catAttributeList = categoryService.selectCatAttributeByCode(categoryCode);
                //First list get from database
                session.put(Constant.SESSION_CATEGORY_EDIT_ATTR_LIST, catAttributeList);
                createOriginalAttribute(catAttributeList);
                form.setCategory(category);
                form.setCatAttributeList(catAttributeList);
                initOriginalAttributeCodeList();
            }
        } catch (Exception e) {
            this.getLogger().error("[CategoryEditAction] error occurs in execute");
            this.getLogger().error(e.toString());
            return "errorOther";
        }
        return SUCCESS;
    }
    /************************************************************************
     * <b>Description:</b><br>
     *  Prepare data and validate before to save database.
     *  1. Cut category name-name at character No.255 (if any)
     * @author      hoang.ho
     * @since       2014/02/19
     ************************************************************************/
    private void validateAndPrepareData() {
        // Get data and check null object value.
        if (form == null) {
            return;
        }
        EntMstCategory entCate = form.getCategory();
        if (entCate == null) {
            return;
        }
        //1. Cut brand-name at character No.255 (if any)
        String cateName = form.getCategory().getCategoryName();
        form.getCategory().setCategoryName(Utils.cutStringByLength(cateName, Constant.CATEGORY_NAME_LENGTH));
    }
    /************************************************************************
     * <b>Description:</b><br>
     * init message.
     *
     * @author      Long Vu
     * @date        Jan 13, 2014
     ************************************************************************/
    private void initMessage() {
        String msg = this.getRequest().getParameter("msgCode");
        String errorMsg = "";
        if (StringUtils.equals("1", msg)) {
            errorMsg = this.getText("message.success.insertFail");
        } else if (StringUtils.equals("0", msg)) {
            errorMsg = this.getText("message.error.updateFail");
        } else if (StringUtils.equals("2", msg)) {
            //BOE THAI.SON
            // if mode is edit and data is updated by other person.
            // get user update and update time from session.
            EntTblCategoryAttribute catAttributeToCreateMessageUpdated = (EntTblCategoryAttribute)
            session.get(Constant.SESSION_CAT_ATTRIBUTE_TO_CREATE_MESSAGE_UPDATED);
            // clear session.
            session.remove(Constant.SESSION_CAT_ATTRIBUTE_TO_CREATE_MESSAGE_UPDATED);
            errorMsg = this.getText("message.error.othersUpdate");
            if (catAttributeToCreateMessageUpdated != null && catAttributeToCreateMessageUpdated.getUpdatedUserFlg() != null) {
                String userName = categoryService.getUserNameByUserIdInMstFactoryUser(catAttributeToCreateMessageUpdated.getUpdatedUserFlg());
                errorMsg = String.format(errorMsg, userName, catAttributeToCreateMessageUpdated.getUpdatedOn());
            }
            //EOE THAI.SON
        }
        form.setMessage(errorMsg);
    }

    /************************************************************************
     * <b>Description:</b><br>
     * get categoryCode.
     *
     * @author      Long Vu
     * @date        Jan 13, 2014
     * @return      int
     ************************************************************************/
    private int initCategoryCode() {
        int result = 0;
        String code = this.getRequest().getParameter("categoryCode");
        if (StringUtils.isNumeric(code)) {
            result = Integer.parseInt(code);
        }

        return result;
    }

    /************************************************************************
     * <b>Description:</b><br>
     *  create original attribute.
     *
     * @author		Luong.Dai
     * @date		Jan 10, 2014
     * @param catAttributeList		void
     ************************************************************************/
    private void createOriginalAttribute(List<EntTblCategoryAttribute> catAttributeList) {
        int length = catAttributeList.size();

        for (int i = 0; i < length; i++) {
            EntTblCategoryAttribute catAttr = catAttributeList.get(i);

            String sOriginal = catAttr.getAttributeCode() + ";"
                            + catAttr.getRequiredFlg() + ";"
                            + catAttr.getSort();

            catAttr.setOriginalAttribute(sOriginal);
        }
    }

    /************************************************************************
     * <b>Description:</b><br>
     * get ordiginal attribute code list to compare for updating or not.
     *
     * @author      Long Vu
     * @date        Jan 07, 2014
     ************************************************************************/
    private void initOriginalAttributeCodeList() {
        StringBuffer buf = new StringBuffer();
//        String result = "";
        if (this.form.getCatAttributeList() != null && this.form.getCatAttributeList().size() != 0) {
            for (EntTblCategoryAttribute obj : this.form.getCatAttributeList()) {
                buf.append(obj.getAttributeCode());
                buf.append(",");
//                result += obj.getAttributeCode() + ",";
            }
        }
//        form.setOriginalAttributeCodeList(result);
        form.setOriginalAttributeCodeList(buf.toString());
    }

    /************************************************************************
     * <b>Description:</b><br>
     * get the insert mode to check the return to browser is insert or delete.
     *
     * @author      Long Vu
     * @date        Jan 07, 2014
     ************************************************************************/
    private void initInsertMode() {
        String insertMode = this.getRequest().getParameter("insertMode");
        if (StringUtils.equals(insertMode, "0")) {
            form.setInsertMode(0);
        } else {
            form.setInsertMode(1);
        }
    }

    /************************************************************************
     * <b>Description:</b><br>
     * to process insert or update category.
     *
     * @author      Long Vu
     * @date        Jan 07, 2014
     * @return      String
     ************************************************************************/
    @Action("/insertOrUpdateBunrui")
    public String insertOrUpdateBunrui() {
        try {
            if (!validateLoginUser()) {
                return "errorOther";
            }

            // BOE #6634 No.24 Hoang.Ho 2014/02/19
            validateAndPrepareData();
            // EOE #6634 No.24 Hoang.Ho 2014/02/19

            // Validate data get from server before process.
            if (!validateDataInput()) {
                if ("0".equals(form.getMsgType())) {
                    return "success";
                } else {
                    return "errorOther";
                }
            }
            // Get user name login
            String user = this.getCommonForm().getLoginId();
            form.getCategory().setLoginUserId(user);
            if (form.getCategory().getCategoryDelFlg() == null) {
                form.getCategory().setCategoryDelFlg("0");
            }
            if (form.getInsertMode() == 1) {
                processInsert();
            } else {
                processUpdate();
            }
        } catch (Exception e) {
            String msgReturn = e.getLocalizedMessage();
            if (StringUtils.equals(msgReturn, "update")) {
                this.setMessageParam("0");
                this.insertModeParam = "0";
                this.categoryCodeParam = form.getCategory().getCategoryCode();
                return "updateError";
            } else if (StringUtils.equals(msgReturn, "insert")) {
                this.setMessageParam("1");
                return "insertError";
            } else if (StringUtils.equals(msgReturn, "othersUpdate")) {
                //BOE THAI.SON
                session.put(Constant.SESSION_CAT_ATTRIBUTE_TO_CREATE_MESSAGE_UPDATED, form.getCatAttributeToCreateMessageUpdated());
                //EOE THAI.SON
                this.setMessageParam("2");
                this.insertModeParam = "0";
                this.categoryCodeParam = form.getCategory().getCategoryCode();
                return "updateError";
            } else {
                return "errorOther";
            }
        }
        if (form.getInsertMode() == 0) {
            this.setMessageParam("0");
        } else {
            this.setMessageParam("1");
        }
        return "categoryManage";
    }

    /**
     * Validate data get from server before process.
     * @return boolean
     */
    private boolean validateDataInput() {
        try {
            EntMstCategory category = form.getCategory();
            if (category == null) {
                return false;
            }

            // Check category name.
            if (StringUtils.isBlank(category.getCategoryName())) {
                return false;
            }

            String bunruiCode = category.getBunruiCode();
            // check bunrui code has value.
            if (StringUtils.isBlank(bunruiCode)) {
                return false;
            }
            // check bunrui code has value. exist or not.
            EntMstBunrui bunrui = categoryService.getBunrui(bunruiCode);

            // is not exist, return false.
            if (bunrui == null) {
                form.setMessage(getText("message.error.bunruiNotExists"));
                form.setMsgType("0");
                return false;
            }

            // if bunrui name from client is blank. set bunrui name.
            if (StringUtils.isBlank(category.getBunruiName())) {
                category.setBunruiName(bunrui.getBunruiName());
            }

            } catch (Exception ex) {
                this.getLogger().error("[CategoryEditAction] error occurs in execute");
                this.getLogger().error(ex.toString());
                return false;
            }
        return true;
    }

    /************************************************************************
     * <b>Description:</b><br>
     * to process insert data.
     *
     * @author      Long Vu
     * @date        Jan 07, 2014
     * @return      boolean
     ************************************************************************/
    private boolean processInsert() {
        boolean result = false;
        initInsertList();
        if (form.getCatAttributeInsertList() != null && form.getCatAttributeInsertList().size() != 0) {
            form.getCategory().setCategoryAttributeCount(String.valueOf(form.getCatAttributeInsertList().size()));
        } else {
            form.getCategory().setCategoryAttributeCount("0");
        }
        result = categoryService.processInsertDataCatAttribute(form);
        return result;
    }

    /************************************************************************
     * <b>Description:</b><br>
     * get insert category_attribute list.
     *
     * @author      Long Vu
     * @date        Jan 13, 2014
     ************************************************************************/
    private void initInsertList() {
        if (form.getCatAttributeList() != null && form.getCatAttributeList().size() != 0) {
            List<EntTblCategoryAttribute> list = new ArrayList<EntTblCategoryAttribute>();
            for (EntTblCategoryAttribute obj : form.getCatAttributeList()) {
                if (StringUtils.equals(obj.getDelFlg(), "0")) {
                    list.add(obj);
                }
            }
            form.setCatAttributeInsertList(list);
        }
    }

    /************************************************************************
     * <b>Description:</b><br>
     * to process update data.
     *
     * @author      Long Vu
     * @date        Jan 07, 2014
     * @return      boolean
     ************************************************************************/
    private boolean processUpdate() {
        boolean result = false;
        // Check attribute list has not modified with database.
        @SuppressWarnings("unchecked")
        List<EntTblCategoryAttribute> catAttributeList = ((List<EntTblCategoryAttribute>) session.get(Constant.SESSION_CATEGORY_EDIT_ATTR_LIST));
        /*BOE  thai.son 2014/01/21 */
        //session.remove(Constant.SESSION_CATEGORY_EDIT_ATTR_LIST);
        /*EOE  thai.son 2014/01/21 */
        String categoryCode = form.getCategory().getCategoryCode();
        //BOE THAI SON EOE
        if (!categoryService.checkAttrHasNotModified(catAttributeList, categoryCode, form)) {
            throw new RuntimeException("othersUpdate");
        }
        //EOE THAI SON
        // to separete delete, insert, and update process
        checkProcessMode();
        result = categoryService.processUpdateDataCatAttribute(form);
        return result;
    }

    /************************************************************************
     * <b>Description:</b><br>
     * to make data condition for delete, update and insert.
     *
     * @author      Long Vu
     * @date        Jan 08, 2014
     ************************************************************************/
    private void checkProcessMode() {
        String[] originalList = form.getOriginalAttributeCodeList().split(",");
        List<EntTblCategoryAttribute> updatedList = new ArrayList<EntTblCategoryAttribute>();
        List<EntTblCategoryAttribute> insertedList = new ArrayList<EntTblCategoryAttribute>();
        StringBuffer buf = new StringBuffer();
        String deletedList = "";
        // create data for insert and update
        if (form.getCatAttributeList() != null) {
            for (EntTblCategoryAttribute obj : form.getCatAttributeList()) {
                if (StringUtils.equals(obj.getDelFlg(), "1")) {
                    // create data for delete
                    buf.append(obj.getAttributeCode());
                    buf.append(",");
    //                deletedList += obj.getAttributeCode() + ",";
                } else {
                    int i = 0;
                    for (String st : originalList) {
                        i++;
                        // if the code from client match the orginal code then add it to update mode
                        if (StringUtils.equals(st.toString().trim(), obj.getAttributeCode())) {
                            updatedList.add(obj);
                            break;
                        }
                        // if the code from client does not match the orginal code then add it to insert mode
                        if (i == originalList.length) {
                            if (!StringUtils.equals(st.toString().trim(), obj.getAttributeCode())) {
                                insertedList.add(obj);
                            }
                        }
                    }
                }
            }
        }
        deletedList = buf.toString();
        form.setCatAttributeInsertList(insertedList);
        form.setCatAttributeUpdateList(updatedList);
        int catAttributeCoutn = insertedList.size() + updatedList.size();
        form.getCategory().setCategoryAttributeCount(String.valueOf(catAttributeCoutn));
        if (deletedList.length() > 0) {
            // remove "," at the last character
            form.setDeletedCodeList(deletedList.substring(0, deletedList.length() - 1));
        }
    }

    /************************************************************************
     * <b>Description:</b><br>
     * to process get Bunrui and check the input bunruiCode exist or not.
     *
     * @author      Long Vu
     * @date        Jan 08, 2014
     * @return      String
     ************************************************************************/
    @Action(value = "loadAjaxBunruiName", results = { @Result(name = "loadAjaxBunruiName", type = "json") })
    public String loadAjaxBunruiName() {
        String bunruiCode = form.getBunrui().getBunruiCode();
        EntMstBunrui bunrui = categoryService.getBunrui(bunruiCode);
        form.setBunrui(bunrui);
        return "loadAjaxBunruiName";
    }

    /************************************************************************
     * <b>Description:</b><br>
     *  check duplicate category by categoryName.
     *
     * @author		Luong.Dai
     * @date		Jan 13, 2014
     * @return		String
     ************************************************************************/
    @Action(value = "loadAjaxCategoryName", results = { @Result(name = "loadAjaxCategoryName", type = "json") })
    public String loadAjaxCategoryName() {
        String categoryName = this.getRequest().getParameter("categoryName");
        String categoryCode = this.getRequest().getParameter("categoryCode");

        EntMstCategory category = new EntMstCategory();
        category.setCategoryName(categoryName);
        category.setCategoryCode(categoryCode);

        boolean hasCategory = categoryService.checkExistsCategoryByCategoryName(category);
        form.setExistsCategory(hasCategory);
        return "loadAjaxCategoryName";
    }

    /************************************************************************
     * <b>Description:</b><br>
     *  search and get list bunrui by bunrui name.
     *
     * @author		Luong.Dai
     * @date		Jan 8, 2014
     * @return		String
     ************************************************************************/
    @Action(value = "searchAjaxBunruiName", results = { @Result(name = "searchAjaxBunruiName", type = "json") })
    public String searchAjaxBunruiName() {
        String bunruiName = this.getRequest().getParameter("bunruiName");
        List<EntMstBunrui> lstBunrui = categoryService.getListBunrui(bunruiName);
        form.setLstBunrui(lstBunrui);
        return "searchAjaxBunruiName";
    }

    /************************************************************************
     * <b>Description:</b><br>
     *  search and get list attribute by attribute name.
     *
     * @author		Luong.Dai
     * @date		Jan 9, 2014
     * @return		String
     ************************************************************************/
    @Action(value = "searchAjaxAttributeName", results = { @Result(name = "searchAjaxAttributeName", type = "json") })
    public String searchAjaxAttributeName() {
        //Get attribute name from request
        String attrName = this.getRequest().getParameter("attrName");
        List<EntMstAttribute> lstAttr = categoryService.selectListAttribute(attrName);
        form.setLstAttribute(lstAttr);
        return "searchAjaxAttributeName";
    }

    /**
     * @return the form
     */
    public CategoryEditActionForm getForm() {
        return form;
    }

    /**
     * @param form the form to set
     */
    public void setForm(CategoryEditActionForm form) {
        this.form = form;
    }

    @Override
    public CategoryEditActionForm getModel() {
        return this.form;
    }

    /**
     * @return the insertModeParam
     */
    public String getInsertModeParam() {
        return insertModeParam;
    }

    /**
     * @param insertModeParam the insertModeParam to set
     */
    public void setInsertModeParam(String insertModeParam) {
        this.insertModeParam = insertModeParam;
    }

    /**
     * @return the categoryCodeParam
     */
    public String getCategoryCodeParam() {
        return categoryCodeParam;
    }

    /**
     * @param categoryCodeParam the categoryCodeParam to set
     */
    public void setCategoryCodeParam(String categoryCodeParam) {
        this.categoryCodeParam = categoryCodeParam;
    }

    @Override
    public void setSession(Map<String, Object> session) {
        this.session = session;
    }

    /**
     * @return the messageParam
     */
    public String getMessageParam() {
        return messageParam;
    }

    /**
     * @param messageParam the messageParam to set
     */
    public void setMessageParam(String messageParam) {
        this.messageParam = messageParam;
    }
}
