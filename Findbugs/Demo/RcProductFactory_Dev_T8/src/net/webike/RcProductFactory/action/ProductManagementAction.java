/**
 * File Name ProductManagementAction.java
 * Author Nguyen.Chuong
 * Version 1.0.0
 * Date Created 2014/02/14
 * Date Updated 2014/02/14
 * Description action to manage product list.
*/
package net.webike.RcProductFactory.action;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import net.webike.RcProductFactory.action.form.ProductManagementActionForm;
import net.webike.RcProductFactory.action.form.ProductManagementActionForm.ProductDeleteFlag;
import net.webike.RcProductFactory.entity.EntCSVMode1;
import net.webike.RcProductFactory.entity.EntCSVMode3;
import net.webike.RcProductFactory.entity.EntMstAttribute;
import net.webike.RcProductFactory.entity.EntMstAttributeGroup;
import net.webike.RcProductFactory.entity.EntMstBrand;
import net.webike.RcProductFactory.entity.EntMstCategory;
import net.webike.RcProductFactory.entity.EntMstFactoryMatter;
import net.webike.RcProductFactory.entity.EntMstFactoryUserTmp;
import net.webike.RcProductFactory.entity.EntMstNouki;
import net.webike.RcProductFactory.entity.EntMstProduct;
import net.webike.RcProductFactory.entity.EntMstProductSupplierStatus;
import net.webike.RcProductFactory.entity.EntMstSiire;
import net.webike.RcProductFactory.entity.EntMstSyouhinSelect;
import net.webike.RcProductFactory.entity.EntTblFactoryImportCondition;
import net.webike.RcProductFactory.entity.EntTblFactoryProductNew;
import net.webike.RcProductFactory.entity.EntTblProductGuestInput;
import net.webike.RcProductFactory.entity.EntTblProductLink;
import net.webike.RcProductFactory.entity.EntTblProductVideo;
import net.webike.RcProductFactory.entity.EntTblSyouhinFitModel;
import net.webike.RcProductFactory.entity.EntTblSyouhinImage;
import net.webike.RcProductFactory.entity.EnumVariousFlag;
import net.webike.RcProductFactory.service.BrandService;
import net.webike.RcProductFactory.service.CategoryService;
import net.webike.RcProductFactory.service.ProductEditService;
import net.webike.RcProductFactory.service.ProductManagementService;
import net.webike.RcProductFactory.service.SyouhinSelectService;
import net.webike.RcProductFactory.service.UserTmpService;
import net.webike.RcProductFactory.util.Constant;
import net.webike.RcProductFactory.util.Utils;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;

/**
 * action of manage product list.
 */
@Scope("request")
@ParentPackage(value = "CommonInterceptor")
@Results(
    {
        // On loading default matter detail page
        @Result(name = "success", location = "productManagement.jsp"),
        //Return json
        @Result(name = "list", type = "json"),
        // Return error page when parameter fail
        @Result(name = "error", type = "redirect", location = "error.html?error=param"),
        // Return error page when parameter fail
        @Result(name = "errorNoPermission", type = "redirect", location = "error.html?error=noPermission"),
        // Return error page when error other.
        @Result(name = "errorOther", type = "redirect", location = "error.html")
    }
)
public class ProductManagementAction extends CommonAction {

    /**
     * Used for serialization object.
     */
    private static final long serialVersionUID = 1L;

    @Autowired
    private ProductManagementActionForm productManagementActionForm;

    @Autowired
    private ProductManagementService productManagementService;

//    @Autowired
//    private UserTmpService userTmpService;
    /**
     * host address.
     */
    private String hostAddress;
    private int page;
    private int pageSize;
    //GroupCodeParam to search only by group code when click on preview popup
    private String groupCodeParam;
    //BrandCodeParam to search only by group code when click on preview popup
    private String brandCodeParam;
    private String brandNameParam;

    /* Loading detail search data. */
    @Autowired
    private BrandService brandService;
    @Autowired
    private CategoryService categoryService;
    @Autowired
    private ProductEditService productEditService;
    @Autowired
    private SyouhinSelectService syouhinSelectService;
    @Autowired
    private UserTmpService userService;
//    @Autowired
//    private MatterService matterService;

    private static final String IMPORT_CONDITION_IMPORT_TYPE = "Maintenance";
    private static final String IMPORT_CONDITION_FILE_NAME = Utils.getProp("withoutFile");
    private static final String IMPORT_CONDITION_ORIGINAL_NAME = Utils.getProp("productMaster");
    private static final int NO_3 = 3;
    //max product for maintenace action
    private static final int NO_4 = 4;
    private static final int NO_5 = 5;
    //Some of products are disable.
    private static final int NO_6 = 6;

    /* Used for detail search. it contains various flags (multiSelect). */
    private String variousFlag;

    /* Used for detail search. it contains multiSearch criteria (textArea). */
    private String multiSearchCriteria;

    /* Used to store syouhinSysCode list after detail search's been done. */
    private String syouhinSysCodeListStr;

    private int maxNumberProductMaintenanceAction = Constant.MAX_PRODUCT_MAINTENANCE_ACTION;
    private int maxNumberProductExportCSV = Constant.MAX_PRODUCT_EXPORT_CSV;

    /**
     * First load page.
     * @return productManagement page.
     */
    @Action("/productManagement")
    public String execute() {
        // 1. Validate authentication
        if (!validateLoginUser()) {
            return "error";
        }
        try {
            List<EntMstAttribute> entMstAttributeList = categoryService.selectAttributeList();
            productManagementActionForm.setEntMstAttributeList(entMstAttributeList);
            productManagementActionForm.setFullNameList(productManagementService.selectAllFullname());
            productManagementActionForm.setProductStatusNameList(productManagementService.selectAllProductStatusName());
            productManagementActionForm.setProductSupplierStatusList(productEditService.selectListAllSupplierStatus());
            //Get brand name from param brandCode.
            if (null != this.getBrandCodeParam()) {
                //if param brandCode is number then go get brand name
                if (StringUtils.isNumeric(this.getBrandCodeParam())) {
                    EntMstBrand entBrand = brandService.selectMstBrandByBrandCode(Integer.parseInt(this.getBrandCodeParam()));
                    this.setBrandNameParam(entBrand.getName());
                } else {
                    //BrandCode is not number then return error param.
                    return "error";
                }
            }
            //Check param group code
            if (null != this.getGroupCodeParam() && !StringUtils.isNumeric(this.getBrandCodeParam())) {
                //GroupCode is not number then return error param.
                return "error";
            }
            //BOE @rcv! Luong.Dai 2014/08/08 #10740: change load list value to load init
            //Get brand list
            List<EntMstBrand> brandList = brandService.selectBrandList();
            productManagementActionForm.setBrandList(brandList);
            // Get category list.
            List<EntMstCategory> categoryList = categoryService.selectCategoryList();
            productManagementActionForm.setCategoryList(categoryList);
            // Get siire list.
            List<EntMstSiire> siireList = productManagementService.selectListAllSiire();
            productManagementActionForm.setSiireList(siireList);
          //BOE @rcv! Luong.Dai 2014/08/08 #10740: change load list value to load init
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "success";
    }

    /************************************************************************
     * <b>Description:</b><br>
     * load product manage list for grid, paging, sort and filter.
     *
     * @return      String
     * @author      Long Vu
     * @date        Feb 15, 2014
     ************************************************************************/
    @Action(value = "/ajaxLoadProductManageList", results = { @Result(name = "list", type = "json") })
    public String loadProductMangeList() {
        List<EntMstProduct> prdList = null;
        int total = 0;
        try {
            // 1. Validate authentication
            if (!validateLoginUser()) {
                return "error";
            }
            total = productManagementService.selectTotalRecordOfGrid(productManagementActionForm.getEntMstProduct());
            // get product manage list with paging, sort and filter
            if (total != 0) {
                prdList = productManagementService.selectProductList(pageSize , page, productManagementActionForm.getEntMstProduct());
            } else {
                prdList = new ArrayList<EntMstProduct>();
            }
            productManagementActionForm.setPrdList(prdList);
            // get total record of product manage list
            productManagementActionForm.setTotalRecord(total);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "list";
    }

    /************************************************************************
     * <b>Description:</b><br>
     * load matter by matter no.
     *
     * @return      String
     * @author      Long Vu
     * @date        Feb 17, 2014
     ************************************************************************/
    @Action(value = "/ajaxLoadMatterByMatterNo", results = { @Result(name = "list", type = "json") })
    public String loadMatterByMatterNo() {
        EntMstFactoryMatter matter = null;
        try {
            // 1. Validate authentication
            if (!validateLoginUser()) {
                return "error";
            }
            matter = productManagementService.selectMatterByMatterNo(productManagementActionForm.getEntMstFactoryMatter().getMatterNo());
            productManagementActionForm.setEntMstFactoryMatter(matter);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "list";
    }

    /************************************************************************
     * <b>Description:</b><br>
     * update product del fgl.
     *
     * @return      String
     * @author      Long Vu
     * @date        Feb 17, 2014
     ************************************************************************/
    @Action(value = "/ajaxUpdateProductDelFlg", results = { @Result(name = "list", type = "json") })
    public String updateProductDelFlg() {
        boolean result = false;
        try {
            // 1. Validate authentication
            if (!validateLoginUser()) { return "error"; }
            //BOE Nguyen.Chuong 2014/03/26 prepare condition for entMstProduct.
            EntMstProduct product = productManagementActionForm.getEntMstProduct();
            if (null != product) {
                product = prepareEntProductCondition(product);
                productManagementActionForm.setEntMstProduct(product);
            }
            //EOE Nguyen.Chuong 2014/03/26
            // get product list
            obtainPrdIdList();
            if (productManagementActionForm.getAllCheckBoxValue()) {
                List<Long> syouhinSysCodeList = productManagementService.selectSyouhinSysCodeListByStatus(productManagementActionForm.getPrdIdList(), 0);
                if (syouhinSysCodeList != null) {
                    if (syouhinSysCodeList.size() == 0) {
                        // no product can be edited
                        productManagementActionForm.setCheckStatus(2);
                        // return to show message
                        return "list";
                    } else if (syouhinSysCodeList.size() < productManagementActionForm.getPrdIdList().size()) {
                        if (productManagementActionForm.getCheckStatus() == -1) {
                            productManagementActionForm.setCheckStatus(0);
                            // update delete status of these filtered records
                            productManagementActionForm.setPrdIdList(syouhinSysCodeList);
                        } else {
                            // some products can be edited
                            productManagementActionForm.setCheckStatus(1);
                            // return to show message
                            return "list";
                        }
                    } else if (syouhinSysCodeList.size() == productManagementActionForm.getPrdIdList().size()) {
                        // all products can be edited
                        productManagementActionForm.setCheckStatus(0);
                        // set filtered
                        productManagementActionForm.setPrdIdList(syouhinSysCodeList);
                    }
                }
            }
            String user = this.getCommonForm().getLoginId();
            productManagementActionForm.getEntMstProduct().setUpdatedUserId(user);
            // enable and disable product
            // if delFlg == 1 then disable
            // else enable
            result = productManagementService.updateProductDelFlg(productManagementActionForm.getPrdIdList(), productManagementActionForm.getEntMstProduct());
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (result) {
            return "list";
        } else {
            return "error";
        }
    }

    /************************************************************************
     * <b>Description:</b><br>
     * update product supplier status code.
     *
     * @return      String
     * @author      Long Vu
     * @date        Feb 17, 2014
     ************************************************************************/
    @Action(value = "/ajaxUpdateProductSupplierStatusCode", results = { @Result(name = "list", type = "json") })
    public String updateProductSupplierStatusCode() {
        boolean result = false;
        try {
            // 1. Validate authentication
            if (!validateLoginUser()) { return "error"; }
            //BOE Nguyen.Chuong 2014/03/26 prepare condition for entMstProduct.
            EntMstProduct product = productManagementActionForm.getEntMstProduct();
            if (null != product) {
                product = prepareEntProductCondition(product);
                productManagementActionForm.setEntMstProduct(product);
            }
            //EOE Nguyen.Chuong 2014/03/26
            // get product list
            obtainPrdIdList();
            if (productManagementActionForm.getAllCheckBoxValue()) {
                List<Long> syouhinSysCodeList = null;
                List<Long> prdIdList = productManagementActionForm.getPrdIdList();
                if (prdIdList != null && prdIdList.size() > 0) {
                    syouhinSysCodeList = productManagementService.selectSyouhinSysCodeListByStatus(prdIdList, 1);
                } else {
                    syouhinSysCodeList = new ArrayList<Long>();
                }
                if (syouhinSysCodeList != null) {
                    if (syouhinSysCodeList.size() == 0) {
                        // no product can be edited
                        productManagementActionForm.setCheckStatus(2);
                        // return to show message
                        return "list";
                    } else if (syouhinSysCodeList.size() < productManagementActionForm.getPrdIdList().size()) {
                        if (productManagementActionForm.getCheckStatus() == -1) {
                            productManagementActionForm.setCheckStatus(0);
                            // set filtered
                            productManagementActionForm.setPrdIdList(syouhinSysCodeList);
                        } else {
                            // some products can be edited
                            productManagementActionForm.setCheckStatus(1);
                            // return to show message
                            return "list";
                        }
                    } else if (syouhinSysCodeList.size() == productManagementActionForm.getPrdIdList().size()) {
                        // all products can be edited
                        productManagementActionForm.setCheckStatus(0);
                        // set filtered
                        productManagementActionForm.setPrdIdList(syouhinSysCodeList);
                    }
                }
            }
            String user = this.getCommonForm().getLoginId();
            productManagementActionForm.getEntMstProduct().setUpdatedUserId(user);
            // update product supplier status code
            result = productManagementService.updateProductSupplierStatusCode(productManagementActionForm.getPrdIdList()
                                                                            , productManagementActionForm.getEntMstProduct());
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (result) {
            return "list";
        } else {
            return "error";
        }
    }

    /************************************************************************
     * <b>Description:</b><br>
     * process data select in grid then copy data to another DB.
     *
     * @return      String
     * @author      Long Vu
     * @date        Mar 04, 2014
     ************************************************************************/
    @Action(value = "/ajaxCopyDataThrough2DB", results = { @Result(name = "list", type = "json") })
    public String copyDataThrough2DB() {
        boolean result = false;
        try {
            // 1. Validate authentication
            if (!validateLoginUser()) {
                return "error";
            }
            //BOE Nguyen.Chuong 2014/03/26 prepare condition for entMstProduct.
            EntMstProduct product = productManagementActionForm.getEntMstProduct();
            if (null != product) {
                product = prepareEntProductCondition(product);
                productManagementActionForm.setEntMstProduct(product);
            }
            //EOE Nguyen.Chuong 2014/03/26
            String user = this.getCommonForm().getLoginId();
            // get product list
            obtainPrdIdListForMaintenance();
            //If check length of productId list is over max maintenace.
            if (NO_4 == productManagementActionForm.getProductValidateMode()) {
                //Number of product to maintenance greater than max then return to show error.
                return "list";
            }
            int productValidateMode = productManagementActionForm.getProductValidateMode();
            // get matter no
            long matterNo = productManagementActionForm.getMatterNo();
            // productValidateMode == 0 mean we are in check import condition mode
            if (productValidateMode == 0) {
                // check import condition
                result = checkImportCondition(matterNo);
                if (!result) {
                    // return error here then show popup in view
                    ServletActionContext.getResponse().setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                    // set mode error for showing popup
                    productManagementActionForm.setErrorMode("importConditionError");
                    return "list";
                } else {
                    // if pass check import condition then set productValidateMode to 1
                    productValidateMode = 1;
                }
            }
            // productValidateMode == 0 mean we are in check maintenance mode
            if (productValidateMode == 1) {
                // check maintenance mode
                int maintenanceMode;
                maintenanceMode = checkMaintenanceMode(productManagementActionForm.getPrdIdList());
                productManagementActionForm.setMaintenanceFlg(maintenanceMode);
                switch (maintenanceMode) {
                case 1:// return 1 mean all product are can't maintenance.
                    // return error here then show popup in view
                    ServletActionContext.getResponse().setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                    // set mode error for showing popup
                    productManagementActionForm.setErrorMode("maintenanceError");
                    return "list";
                case 2:// return 2 mean some products are can maintenance.
                    // show popup confirm
                    productManagementActionForm.setProductValidateMode(productValidateMode);
                    return "list";
                case NO_4://Return 4 then max product to maintenace is over max.
                    //Number of product to maintenance greater than max.
                    productManagementActionForm.setProductValidateMode(NO_4);
                    return "list";
                //BOE Nguyen.Chuong 2014/04/04 check productList contain some product disabled.
                case NO_6://Return 6 then some of product is disabled.
                    productManagementActionForm.setProductValidateMode(NO_6);
                    return "list";
                //EOE Nguyen.Chuon 2014/04/04
                default://Continues to maintence process.
                    // set mode to 2 for process next
                    productValidateMode = 2;
                    break;
                }
            }
            // productValidateMode == 0 mean we are pass check maintenance mode (final mode)
            if (productValidateMode == 2) {
                productManagementActionForm.setProductValidateMode(productValidateMode);
                // check matter no enter or not
                //If matterNo is not enter then new one Ent Matter and set to actionForm.
                //If matter is entered then Ent matter in action form will be null.
                if (matterNo == -1) {
                    // create new matter
                    productManagementActionForm.setEntMstFactoryMatter(createMatterObject(matterNo, user));
                }
                // check product group
                //Check max list product id from UI request.
                if (productManagementActionForm.getPrdIdList().size() > Constant.MAX_PRODUCT_MAINTENANCE_ACTION) {
                    //Number of product to maintenance greater than max.
                    productManagementActionForm.setProductValidateMode(NO_4);
                    return "list";
                }
                checkProductGroup();
                //iF GROUP CODE is 0 then no maintenance and return
                if (productManagementActionForm.getPrdList().size() <= 0) {
                    productManagementActionForm.setProductValidateMode(NO_5);
                    return "list";
                }
                //Check max product maintenance.
                if (productManagementActionForm.getPrdList().size() > Constant.MAX_PRODUCT_MAINTENANCE_ACTION) {
                    //Number of product to maintenance greater than max.
                    productManagementActionForm.setProductValidateMode(NO_4);
                    return "list";
                }
                // create import condition object
                createImportConditonObject(matterNo);
                // copy data
                result = productManagementService.processUpdateMaintenanceProduct(user
                                                                                , matterNo
                                                                                , productManagementActionForm.getPrdList()
                                                                                , productManagementActionForm.getEntMstFactoryMatter()
                                                                                , productManagementActionForm.getEntTblFactoryImportCondition());
                //if matterNo = -1 then user not input matterNo => get max in DB
                if (matterNo == -1) {
                    // create new matter
                    productManagementActionForm.setMatterNo(productManagementService.selectMaxMatterNo());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            productManagementActionForm.setProductValidateMode(NO_3);
            return "list";
        }
        if (result) {
           return "list";
        } else {
            return "error";
        }
    }

    /************************************************************************
     * <b>Description:</b><br>
     * check maintenance mode.
     *
     * @author      Long Vu
     * @param       prdIdList List<Long>
     * @return      int
     * @date        Feb 24, 2014
     ************************************************************************/
    private int checkMaintenanceMode(List<Long> prdIdList) {
        this.getLogger().debug("start checkMaintenanceMode");
        int result = 1;
        List<Long> listPrdId = new ArrayList<Long>();
        try {
            //BOE Nguyen.Chuong 2014/03/19: change process to make code faster
            // get product list by product id list
//            List<EntMstProduct> prdList = productManagementService.selectproductListByPrdIdList(prdIdList);
            List<Long> prdSyouhinList = productManagementService.selectproductSyouhinNotMaintenacedByPrdIdList(prdIdList);
            // check maintenance mode
            if (prdSyouhinList != null) {
//                int errorCount = 0;
                StringBuilder prdIdStringList = new StringBuilder();
//                for (EntMstProduct prd: prdList) {
//                    if (prd.getProductMaintenanceFlg() == 1) {
//                        errorCount++;
//                    } else {
//                        prdIdStringList.append(prd.getSyouhinSysCode() + "_");
//                        listPrdId.add(prd.getSyouhinSysCode());
//                    }
//                }
                for (Long syouhinCode: prdSyouhinList) {
                    prdIdStringList.append(syouhinCode + "_");
                    listPrdId.add(syouhinCode);
                }
                // set product id String list to form to send it to client
                productManagementActionForm.setPrdIdStringList(prdIdStringList.toString());
//                if (errorCount == prdList.size()) {
//                    // return 1 mean all product are can't maintenance
//                    result = 1;
//                } else if (errorCount != 0) {
//                    // return 2 mean some products are can maintenance
//                    result = 2;
//                } else {
//                    // set product id list to form for processing next
//                    productManagementActionForm.setPrdIdList(listPrdId);
//                    // return 3 mean all products are can maintenance
//                    result = NO_3;
//                }
                //Get count of disabled product
                int countDisaleProduct = productManagementService.selectCountProductSyouhinDisabledByPrdIdList(prdIdList);
                if (prdSyouhinList.size() == 0) {
                    // return 1 mean all product are can't maintenance
                    result = 1;
                } else if (countDisaleProduct > 0) {
                    // return 6 mean some of products are disable.
                    result = NO_6;
                } else if (prdIdList.size() != prdSyouhinList.size()) {
                    // return 2 mean some products are can maintenance
                    result = 2;
                } else {
                    // set product id list to form for processing next
                    productManagementActionForm.setPrdIdList(listPrdId);
                    // return 3 mean all products are can maintenance
                    result = NO_3;
                }
                //EOE Nguyen.Chuong 2014/03/19: change process to make code faster
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.getLogger().debug("end checkMaintenanceMode");
        return result;
    }

    /************************************************************************
     * <b>Description:</b><br>
     * create new matter object.
     *
     * @author      Long Vu
     * @param       matterNo long
     * @param       user login user id
     * @return      EntMstFactoryMatter
     * @date        Feb 24, 2014
     ************************************************************************/
    private EntMstFactoryMatter createMatterObject(long matterNo, String user) {
        this.getLogger().debug("start createMatterObject");
        EntMstFactoryMatter matter = new EntMstFactoryMatter();
        matter.setMatterNo(matterNo);
        // create matter name by user name
        /* BOE set matterName = last name + first name by Luong.Dai at 2014/03/24 */
        /*matter.setMatterName(user);*/
        matter.setMatterName(this.getText("maintenanceBy")
        		+ this.getCommonForm().getLoginLastName() + " " + this.getCommonForm().getLoginFirstName());
        /* BOE set matterName = last name + first name by Luong.Dai at 2014/03/24 */
        // create matter charge user id by execution user
        matter.setMatterChargeUserId(user);
        matter.setCreatedUserId(user);
        matter.setUpdatedUserId(user);
        this.getLogger().debug("end createMatterObject");
        return matter;
    }

    /************************************************************************
     * <b>Description:</b><br>
     * check product group with given product list.
     *
     * @author      Long Vu
     * @date        Feb 21, 2014
     ************************************************************************/
    private void checkProductGroup() {
        this.getLogger().debug("start checkProductGroup");
        List<EntMstProduct> productList = null;
        try {
            // get product list with brand code and group code
            productList = productManagementService.selectProductBrdCodeAndGrpCodeListWithoutGroup0(productManagementActionForm.getPrdIdList());
            if (productList != null) {
                // get product list by brand code and group code
                productList = productManagementService.selectProductListByBrdCodeAndGrpCode(productList);
            }
            //Get productList in groupCode 0
            List<EntMstProduct> productListGroup0 = null;
            productListGroup0 = productManagementService.selectSyouhinSysCodeOnlyGroup0ByListSyouhin(productManagementActionForm.getPrdIdList());
            if (null != productListGroup0) {
                productList.addAll(productListGroup0);
            }
            // set product list to form
            productManagementActionForm.setPrdList(productList);
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.getLogger().debug("start checkProductGroup");
    }

    /************************************************************************
     * <b>Description:</b><br>
     * create import condition object for preparing data to update after.
     *
     * @author      Long Vu
     * @param       matterNo long
     * @date        Feb 20, 2014
     ************************************************************************/
    private void createImportConditonObject(long matterNo) {
        this.getLogger().debug("start createImportConditonObject");
        EntTblFactoryImportCondition obj = new EntTblFactoryImportCondition();
        obj.setImportConditionImportType(IMPORT_CONDITION_IMPORT_TYPE);
        obj.setImportConditionMatterNo(matterNo);
        obj.setImportConditionFileName(IMPORT_CONDITION_FILE_NAME);
        obj.setImportConditionOriginalName(IMPORT_CONDITION_ORIGINAL_NAME);
        obj.setImportConditionExeCount(0);
        obj.setImportConditionMaxCount(productManagementActionForm.getPrdList().size());
        obj.setImportConditionStatus(0);
        productManagementActionForm.setEntTblFactoryImportCondition(obj);
        this.getLogger().debug("start createImportConditonObject");
    }

    /************************************************************************
     * <b>Description:</b><br>
     * check import condition status with given matter no.
     *
     * @return      boolean
     * @param       matterNo long
     * @author      Long Vu
     * @date        Feb 21, 2014
     ************************************************************************/
    private boolean checkImportCondition(long matterNo) {
        this.getLogger().debug("start checkImportCondition");
        boolean result = false;
        List<EntTblFactoryImportCondition> importList = null;
        try {
            // get import condition list
            importList = productManagementService.selectImportConditionListByMatterNo(matterNo);
            if (importList != null && importList.size() != 0) {
                for (EntTblFactoryImportCondition obj: importList) {
                    // check the return is valid import list or not
                    // 0: valid, 1: invalid
                    if (obj.getImportConditionStatus() == 0) {
                        this.getLogger().debug("end checkImportCondition");
                        return result;
                    }
                }
                result = true;
                this.getLogger().debug("end checkImportCondition");
                return result;
            } else {
                // when import condition list is null then return true
                // and create new import condition like no enter matter no
                result = true;
                this.getLogger().debug("end checkImportCondition");
                return result;
            }
        } catch (Exception e) {
            e.printStackTrace();
            this.getLogger().debug("end checkImportCondition");
            return result;
        }
    }

    /*BOE Nguyen.Chuong process for load data of preview product popup*/
    /************************************************************************
     * <b>Description:</b><br>
     *  Load init data for popup review.
     *
     * @author      Nguyen.Chuong
     * @date        Feb 17, 2014
     * @return      String  review popup page.
     ************************************************************************/
    @Action(value = "/loadInitDataPreviewPopup", results = { @Result(name = "initReviewPopup", type = "json") })
    public String loadInitDataPreviewPopup() {
        // 1. Validate authentication
        if (!validateLoginUser()) {
            return "error";
        }
        //Get request syouhinCode
        String syouhinSysCode = this.getRequest().getParameter(Constant.PARAM_SYOUHIN_SYS_CODE);
        if (StringUtils.isEmpty(syouhinSysCode) || !StringUtils.isNumeric(syouhinSysCode)) {
            return "errorOther";
        }
        long syouhinSysCodeInt = Long.parseLong(syouhinSysCode);
        //Get data for entMstProduct and set to actionForm.
        EntMstProduct entMstProduct = productManagementService.selectProductDataForProductReviewPopupBySyouhinCode(syouhinSysCodeInt);
        productManagementActionForm.setEntMstProduct(entMstProduct);
        //Get list siire and Nouki and set to actionForm.
        List<EntMstSiire> listSiireNouki = productManagementService.selectListSiireNoukiBySyouhinSysCode(syouhinSysCodeInt);
        productManagementActionForm.setListSiireNouki(listSiireNouki);
        //Get list image syouhin and set to acitonForm
        List<EntTblSyouhinImage> listSyouhinImage = productManagementService.selectListImageBySyouhinSysCode(syouhinSysCodeInt);
        productManagementActionForm.setListSyouhinImage(listSyouhinImage);
        return "initReviewPopup";
    }

    /************************************************************************
     * <b>Description:</b><br>
     *  load product data in detail mode of productManagement's preview popup.
     *
     * @author      Nguyen.Chuong
     * @date        Feb 15, 2014
     * @return      String
     ************************************************************************/
    @Action(value = "/loadDetailDataPreviewPopup", results = { @Result(name = "detailReviewPopup", type = "json") })
    public String loadDetailDataPreviewPopup() {
        // 1. Validate authentication
        if (!validateLoginUser()) {
            return "error";
        }
        //Get request syouhinCode
        String syouhinSysCode = this.getRequest().getParameter(Constant.PARAM_SYOUHIN_SYS_CODE);
        if (StringUtils.isEmpty(syouhinSysCode) || !StringUtils.isNumeric(syouhinSysCode)) {
            return "errorOther";
        }
        long syouhinSysCodeLong = Long.parseLong(syouhinSysCode);
        //Get fit model list and set to actionForm.
        List<EntTblSyouhinFitModel> listSyouhinFitModel = productManagementService.selectListSyouhinFitModelBySyouhinSysCode(syouhinSysCodeLong);
        productManagementActionForm.setListSyouhinFitModel(listSyouhinFitModel);
        //Get list attribute all type and set to actionForm.
        List<EntMstAttribute> listAttributeAllType = productManagementService.selectListAttributeAllTypeBySyouhinSysCode(syouhinSysCodeLong);
        productManagementActionForm.setListAttributeAllType(listAttributeAllType);
        //Get list link reason
        List<EntTblProductLink> listLinkReason = productManagementService.selectLinkReasonBySyouhinSysCode(syouhinSysCodeLong);
        productManagementActionForm.setListLinkReason(listLinkReason);
        //Get list video and set to actionForm.
        List<EntTblProductVideo> listVideo = productManagementService.selectLinkVideoBySyouhinSysCode(syouhinSysCodeLong);
        productManagementActionForm.setListVideo(listVideo);
        //Get list guest input
        List<EntTblProductGuestInput> listGuestInput = productManagementService.selectListProductGuestInputBySyouhinSysCode(syouhinSysCodeLong);
        productManagementActionForm.setListGuestInput(listGuestInput);
        return "detailReviewPopup";
    }
    /*EOE Nguyen.Chuong process for load data of preview product popup*/

    /************************************************************************
     * <b>Description:</b><br>
     * get product id list from client.
     *
     * @author      Long Vu
     * @date        Feb 18, 2014
     ************************************************************************/
    private void obtainPrdIdList() {
        this.getLogger().debug("start obtainPrdIdList");
        List<Long> productIdList = new ArrayList<Long>();
        //BOE Nguyen.Chuong 2014/03/19 get list all from DB
        //If check box checkAll is checked => get list productId from DB
        if (productManagementActionForm.getAllCheckBoxValue()) {
            // Used when filtering after detail search done.
          /*  if (syouhinSysCodeListStr != null && !StringUtils.equals("", syouhinSysCodeListStr.trim())) {
                Gson gson = new Gson();
                Type type = new TypeToken<List<Integer>>() { } .getType();
                List<Integer> syouhinSysCodeListInt = gson.fromJson(syouhinSysCodeListStr, type);
//                productManagementActionForm.getEntMstProduct().setSyouhinSysCodeList(syouhinSysCodeListInt);
            }*/
            List<String> listProducIdString = productManagementService.selectAllProductsForExportCSV(productManagementActionForm.getEntMstProduct());
            //Loop to parse list String to int
            productIdList = productManagementService.convertListStringToListLong(listProducIdString);
            productManagementActionForm.setPrdIdList(productIdList);
            this.getLogger().debug("end obtainPrdIdList");
            return;
        }
        //EOE Nguyen.Chuong 2014/03/19
        // get syouhin sys code array string have normal group
        String stringProductId = this.getRequest().getParameter("arraySyouhinSysCode");
        // split syouhinSysCode array string to element
        String[] arrayProductId = stringProductId.split("_");
        // add element to productIdList (integer list)
        if (arrayProductId != null && arrayProductId.length != 0) {
            for (String obj: arrayProductId) {
                productIdList.add(Long.valueOf(obj));
            }
        }
        productManagementActionForm.setPrdIdList(productIdList);
        this.getLogger().debug("end obtainPrdIdList");
    }

    /************************************************************************
     * <b>Description:</b><br>
     * get product id list from client only for maintenace mode.
     *
     * @author      Long Vu
     * @date        Feb 18, 2014
     ************************************************************************/
    private void obtainPrdIdListForMaintenance() {
        this.getLogger().debug("start obtainPrdIdList");
        List<Long> productIdList = new ArrayList<Long>();
        //If check box checkAll is checked => get list productId from DB
        if (productManagementActionForm.getAllCheckBoxValue()) {

            //Check count of list product maintenanced over max or not.
            //Get EntMstProduct param from actionForm.
            EntMstProduct entProductTmp = productManagementActionForm.getEntMstProduct();
            /* BOE #9414 Luong.Dai 2014/07/02 Fix bug filter by maintenance flag */
            /*//Set maintenanceFlg to get count of list maintenaced product.
            entProductTmp.setProductMaintenanceFlg(0);
            int countProductNotMaintenance = 0;
            try {
                countProductNotMaintenance = productManagementService.selectDetailSearchProductListCount(entProductTmp);
            } catch (SQLException e) {
                this.getLogger().error("[ProductManagementAction] An error obtainPrdIdListForMaintenance function");
                this.getLogger().error(e.toString());
                return;
            }*/
            int countProductNotMaintenance = 0;
            //Check filter by maintenance flag
            if (entProductTmp.getProductMaintenanceFlg() != 1) {
            	//Set maintenanceFlg to get count of list maintenaced product.
            	int currentMaintenanceFlg = entProductTmp.getProductMaintenanceFlg();
            	entProductTmp.setProductMaintenanceFlg(0);
                try {
                    countProductNotMaintenance = productManagementService.selectDetailSearchProductListCount(entProductTmp);
                } catch (SQLException e) {
                    this.getLogger().error("[ProductManagementAction] An error obtainPrdIdListForMaintenance function");
                    this.getLogger().error(e.toString());
                    return;
                }
                //Reset maintenanceFlg by filter
                entProductTmp.setProductMaintenanceFlg(currentMaintenanceFlg);
            }
            /* EOE #9414 Luong.Dai 2014/07/02 Fix bug filter by maintenance flag */
            //if list greater than MAX_PRODUCT_MAINTENANCE_ACTION then return No_4: over max maintence.
            if (countProductNotMaintenance > Constant.MAX_PRODUCT_MAINTENANCE_ACTION) {
                productManagementActionForm.setProductValidateMode(NO_4);
                return;
            }
            //if count is smaller then max then get list productID
            List<String> listProducIdString = productManagementService.selectAllProductsForExportCSV(productManagementActionForm.getEntMstProduct());
            //Loop to parse list String to int
            productIdList = productManagementService.convertListStringToListLong(listProducIdString);
            productManagementActionForm.setPrdIdList(productIdList);
            this.getLogger().debug("end obtainPrdIdList");
            return;
        }
        // get syouhin sys code array string have normal group
        String stringProductId = this.getRequest().getParameter("arraySyouhinSysCode");
        // split syouhinSysCode array string to element
        String[] arrayProductId = stringProductId.split("_");
        // add element to productIdList (integer list)
        if (arrayProductId != null && arrayProductId.length != 0) {
            for (String obj: arrayProductId) {
                productIdList.add(Long.valueOf(obj));
            }
        }
        productManagementActionForm.setPrdIdList(productIdList);
        this.getLogger().debug("end obtainPrdIdList");
    }

    /**
     * Load attribute group.
     * @author      nguyen.hieu
     * @date        2014-02-20
     * @return      Action name.
     */
    @Action(value = "/loadAttributeGroup", results = { @Result(name = "loadAttributeGroup", type = "json") })
    public String loadAttributeGroup() {
        try {
            // Get attributeCode param.
            String attributeCode = getRequest().getParameter("attributeCode");
            if (attributeCode == null) {
                return "error";
            }
            int intAttributeCode = Integer.parseInt(attributeCode);

            // Get attributeGroup list.
            List<EntMstAttributeGroup> attributeGroupList = productManagementService.selectAttrGroupListByAttributeCode(intAttributeCode);
            productManagementActionForm.setAttributeGroupList(attributeGroupList);

        } catch (NumberFormatException e) {
            this.getLogger().error("[ProductManagementAction] An error execute function");
            this.getLogger().error(e.toString());
            return "errorOther";

        } catch (Exception e) {
            this.getLogger().error("[ProductManagementAction] An error execute function");
            this.getLogger().error(e.toString());
            return "errorOther";
        }

        return "loadAttributeGroup";
    }

    /**
     * Load brand list.
     * @author      nguyen.hieu
     * @date        2014-02-20
     * @return      Action name.
     */
    @Action(value = "/loadBrandList", results = { @Result(name = "loadBrandList", type = "json") })
    public String loadBrandList() {
        try {
            // Get brand list.
            List<EntMstBrand> brandList = brandService.selectBrandList();
            productManagementActionForm.setBrandList(brandList);

        } catch (NumberFormatException e) {
            this.getLogger().error("[ProductManagementAction] An error execute function");
            this.getLogger().error(e.toString());
            return "errorOther";

        } catch (Exception e) {
            this.getLogger().error("[ProductManagementAction] An error execute function");
            this.getLogger().error(e.toString());
            return "errorOther";
        }

        return "loadBrandList";
    }

    /**
     * Load category list.
     * @author      nguyen.hieu
     * @date        2014-02-20
     * @return      Action name.
     */
    @Action(value = "/loadCategoryList", results = { @Result(name = "loadCategoryList", type = "json") })
    public String loadCategoryList() {
        try {
            // Get category list.
            List<EntMstCategory> categoryList = categoryService.selectCategoryList();
            productManagementActionForm.setCategoryList(categoryList);

        } catch (NumberFormatException e) {
            this.getLogger().error("[ProductManagementAction] An error execute function");
            this.getLogger().error(e.toString());
            return "errorOther";

        } catch (Exception e) {
            this.getLogger().error("[ProductManagementAction] An error execute function");
            this.getLogger().error(e.toString());
            return "errorOther";
        }

        return "loadCategoryList";
    }

    /**
     * Load siire list.
     * @author      nguyen.hieu
     * @date        2014-02-20
     * @return      Action name.
     */
    @Action(value = "/loadSiireList", results = { @Result(name = "loadSiireList", type = "json") })
    public String loadSiireList() {
        try {
            // Get siire list.
            List<EntMstSiire> siireList = productManagementService.selectListAllSiire();
            productManagementActionForm.setSiireList(siireList);

        } catch (NumberFormatException e) {
            this.getLogger().error("[ProductManagementAction] An error execute function");
            this.getLogger().error(e.toString());
            return "errorOther";

        } catch (Exception e) {
            this.getLogger().error("[ProductManagementAction] An error execute function");
            this.getLogger().error(e.toString());
            return "errorOther";
        }

        return "loadSiireList";
    }

    /**
     * Load syouhin select list.
     * @author      nguyen.hieu
     * @date        2014-02-20
     * @return      Action name.
     */
    @Action(value = "/loadSyouhinSelectList", results = { @Result(name = "loadSyouhinSelectList", type = "json") })
    public String loadSyouhinSelectList() {
        try {
            // Get syouhin select list.
            List<EntMstSyouhinSelect> syouhinSelectList = syouhinSelectService.selectSyouhinSelect();
            productManagementActionForm.setSyouhinSelectList(syouhinSelectList);

        } catch (NumberFormatException e) {
            this.getLogger().error("[ProductManagementAction] An error execute function");
            this.getLogger().error(e.toString());
            return "errorOther";

        } catch (Exception e) {
            this.getLogger().error("[ProductManagementAction] An error execute function");
            this.getLogger().error(e.toString());
            return "errorOther";
        }

        return "loadSyouhinSelectList";
    }

    /**
     * Load nouki list.
     * @author      nguyen.hieu
     * @date        2014-02-20
     * @return      Action name.
     */
    @Action(value = "/loadNoukiList", results = { @Result(name = "loadNoukiList", type = "json") })
    public String loadNoukiList() {
        try {
            // Get nouki list.
            List<EntMstNouki> noukiList = productManagementService.selectNoukiList();
            productManagementActionForm.setNoukiList(noukiList);

        } catch (NumberFormatException e) {
            this.getLogger().error("[ProductManagementAction] An error execute function");
            this.getLogger().error(e.toString());
            return "errorOther";

        } catch (Exception e) {
            this.getLogger().error("[ProductManagementAction] An error execute function");
            this.getLogger().error(e.toString());
            return "errorOther";
        }

        return "loadNoukiList";
    }

    /**
     * Load attribute list.
     * @author      nguyen.hieu
     * @date        2014-02-20
     * @return      Action name.
     */
    @Action(value = "/loadAttributeList", results = { @Result(name = "loadAttributeList", type = "json") })
    public String loadAttributeList() {
        try {
            // Get attribute list.
            List<EntMstAttribute> attributeList = productManagementService.selectAttributeList();
            productManagementActionForm.setAttributeList(attributeList);

        } catch (NumberFormatException e) {
            this.getLogger().error("[ProductManagementAction] An error execute function");
            this.getLogger().error(e.toString());
            return "errorOther";

        } catch (Exception e) {
            this.getLogger().error("[ProductManagementAction] An error execute function");
            this.getLogger().error(e.toString());
            return "errorOther";
        }

        return "loadAttributeList";
    }

    /**
     * Load product supplier status list.
     * @author      nguyen.hieu
     * @date        2014-02-20
     * @return      Action name.
     */
    @Action(value = "/loadProductSupplierStatusList", results = { @Result(name = "loadProductSupplierStatusList", type = "json") })
    public String loadProductSupplierStatusList() {
        try {
            // Get product supplier status list.
            List<EntMstProductSupplierStatus> productSupplierStatusList = productEditService.selectListAllSupplierStatus();
            productManagementActionForm.setProductSupplierStatusList(productSupplierStatusList);

        } catch (NumberFormatException e) {
            this.getLogger().error("[ProductManagementAction] An error execute function");
            this.getLogger().error(e.toString());
            return "errorOther";

        } catch (Exception e) {
            this.getLogger().error("[ProductManagementAction] An error execute function");
            this.getLogger().error(e.toString());
            return "errorOther";
        }

        return "loadProductSupplierStatusList";
    }

    /**
     * Load user list.
     * @author      nguyen.hieu
     * @date        2014-02-20
     * @return      Action name.
     */
    @Action(value = "/loadUserList", results = { @Result(name = "loadUserList", type = "json") })
    public String loadUserList() {
        // Validate login user.
        if (!validateLoginUser()) {
            return "error";
        }

        try {
            // Get user list.
            List<EntMstFactoryUserTmp> userList = userService.selectAllUser();
            productManagementActionForm.setUserList(userList);

        } catch (NumberFormatException e) {
            this.getLogger().error("[ProductManagementAction] An error execute function");
            this.getLogger().error(e.toString());
            return "errorOther";

        } catch (Exception e) {
            this.getLogger().error("[ProductManagementAction] An error execute function");
            this.getLogger().error(e.toString());
            return "errorOther";
        }

        return "loadUserList";
    }

    /**
     * Load product delete flag list.
     * @author      nguyen.hieu
     * @date        2014-02-20
     * @return      Action name.
     */
    @Action(value = "/loadProductDeleteFlagList", results = { @Result(name = "loadProductDeleteFlagList", type = "json") })
    public String loadProductDeleteFlagList() {
        try {
            // Get product delete flag list.
            List<ProductDeleteFlag> productDeleteFlagList =
                    new ArrayList<ProductDeleteFlag>();
            productDeleteFlagList.add(new ProductDeleteFlag(0, getText("valid")));
            productDeleteFlagList.add(new ProductDeleteFlag(1, getText("invalid")));
            productManagementActionForm.setProductDeleteFlagList(productDeleteFlagList);

        } catch (NumberFormatException e) {
            this.getLogger().error("[ProductManagementAction] An error execute function");
            this.getLogger().error(e.toString());
            return "errorOther";

        } catch (Exception e) {
            this.getLogger().error("[ProductManagementAction] An error execute function");
            this.getLogger().error(e.toString());
            return "errorOther";
        }

        return "loadProductDeleteFlagList";
    }

    /**
     * Load various flag list.
     * @author      nguyen.hieu
     * @date        2014-02-20
     * @return      Action name.
     */
    @Action(value = "/loadVariousFlagList", results = { @Result(name = "loadVariousFlagList", type = "json") })
    public String loadVariousFlagList() {
        try {
            // Get various flag list.
            List<Map<String, String>> variousFlagList = EnumVariousFlag.getEnumList();
            productManagementActionForm.setVariousFlagList(variousFlagList);

        } catch (NumberFormatException e) {
            this.getLogger().error("[ProductManagementAction] An error execute function");
            this.getLogger().error(e.toString());
            return "errorOther";

        } catch (Exception e) {
            this.getLogger().error("[ProductManagementAction] An error execute function");
            this.getLogger().error(e.toString());
            return "errorOther";
        }

        return "loadVariousFlagList";
    }

    /**
     * Get detail search product list.
     * @return Action name.
     */
    @Action(value = "/loadDetailSearchProductList", results = { @Result(name = "loadDetailSearchProductList", type = "json") })
    public String loadDetailSearchProductList() {
        try {
            // Validate login.
            if (!validateLoginUser()) {
                return "error";
            }
            EntMstProduct product = productManagementActionForm.getEntMstProduct();
            //BOE Nguyen.Chuong 2014/03/26: move to function for use in other action
//            if (product.getDetailSearchFlag() == 2) {
//                // Get various flag then set to form.
//                String []parts = variousFlag.split(",");
//                for (String part : parts) {
//                    if (!part.equals("")) {
//                        // Convert it to integer to compare.
//                        int partInt = Integer.parseInt(part);
//                        if (partInt == EnumVariousFlag.OPEN_PRICE.getKey()) {
//                            productManagementActionForm.getEntMstProduct().setProductOpenPriceFlg(true);
//                        } else if (partInt == EnumVariousFlag.NOT_RETURNED_OR_EXCHANGED.getKey()) {
//                            productManagementActionForm.getEntMstProduct().setProductNoReturnableFlg(true);
//                        } else if (partInt == EnumVariousFlag.BUILD_TO_ORDER_MANUFACTURING.getKey()) {
//                            productManagementActionForm.getEntMstProduct().setProductOrderProductFlg(true);
//                        } else if (partInt == EnumVariousFlag.PREFERENCE_IMAGE.getKey()) {
//                            productManagementActionForm.getEntMstProduct().setProductAmbigousImageFlg(true);
//                        } else if (partInt == EnumVariousFlag.SALE_PRICE.getKey()) {
//                            productManagementActionForm.getEntMstProduct().setProductProperSellingFlg(true);
//                        }
//                    }
//                }
//                // Get multiSearch criteria then set to form.
//                String []criteria = multiSearchCriteria.split(",");
//                List<String> multiSearchCriteriaList = new ArrayList<String>();
//                for (String criterion : criteria) {
//                    if (!criterion.equals("")) {
//                        multiSearchCriteriaList.add(criterion);
//                    }
//                }
//                productManagementActionForm.getEntMstProduct().setMultiSearchCriteriaList(multiSearchCriteriaList);
//            }
            if (null != product) {
                product = prepareEntProductCondition(product);
                productManagementActionForm.setEntMstProduct(product);
            }
            //EOE Nguyen.Chuong 2014/03/2
            int productListCount = 0;
            List<EntMstProduct> productList = null;
            productListCount = productManagementService.selectDetailSearchProductListCount(product);

            if (productListCount > 0) {
                productList = productManagementService.selectDetailSearchProductList(pageSize , page, product);
            } else {
                productList = new ArrayList<EntMstProduct>();
            }
            //BOE @rcv! Luong.Dai 2014/08/06 #10742: Get matter info of maintenance product
            for (EntMstProduct mstProduct : productList) {
            	//Check product maintenance
            	if (mstProduct.getProductMaintenanceFlg() == 1) {
            		List<EntTblFactoryProductNew> listMatterBySyouhinSysCode = 
            				productManagementService.selectMatterDetailByProductSyouhinSysCode(mstProduct.getSyouhinSysCode());
            		EntTblFactoryProductNew matterInfo = null;
            		//Check many matter has product with syouhinSysCode
            		if (listMatterBySyouhinSysCode.size() > 0) {
            			mstProduct.setHasMaintenanceProduct(true);
            			//Get product to show in tooltip: min product id of min matterNo
            			matterInfo = selectMatterMaintenanceProduct(listMatterBySyouhinSysCode);
            		} else {
            			mstProduct.setHasMaintenanceProduct(false);
            		}
            		mstProduct.setMatterInfo(matterInfo);
            	}
            }
            //EOE @rcv! Luong.Dai 2014/08/06 #10742: Get matter info of maintenance product
            // Get search detail product list count.
            productManagementActionForm.setTotalRecord(productListCount);
            // Get search detail product list.
            productManagementActionForm.setPrdList(productList);
        } catch (Exception e) {
            this.getLogger().error("[ProductManagementAction] An error execute function");
            this.getLogger().error(e.toString());
            return "errorOther";
        }
        return "loadDetailSearchProductList";
    }

    /************************************************************************
     * <b>Description:</b><br>
     *  Find min matter not release from list matter info.
     *  Find min product in min matter
     *
     * @author		Luong.Dai
     * @date		Aug 8, 2014
     * @param 		listMatterBySyouhinSysCode		List matter info to check
     * @return		EntTblFactoryProductNew			Entity result
     ************************************************************************/
    private EntTblFactoryProductNew selectMatterMaintenanceProduct(List<EntTblFactoryProductNew> listMatterBySyouhinSysCode) {
    	EntTblFactoryProductNew matterInfo = null;

    	//Count number matter maintenance product
    	Long countMatter = 0L;
    	//Count number product in matter maintenance product
    	Long countProduct = 0L;
    	//Matter has product maintenance product
    	Long currentMatter = -1L;

    	for (EntTblFactoryProductNew product : listMatterBySyouhinSysCode) {
    		//skip product release and matter release
    		if (product.getMatterRegistrationFlg() == 1
    				|| product.getProductRegistrationFlg() == 1) {
    			continue;
    		}
    		//Get first product (min matter, min product in matter)
    		if (matterInfo == null) {
    			matterInfo = product;
    		}
    		//Check first matter. first matter is matter has min matterNo
    		if (currentMatter < 0) {
    			currentMatter = product.getProductMatterNo();
    			//Update count matter and product
    			countMatter++;
    			countProduct++;
    		} else if (!currentMatter.equals(product.getProductMatterNo())) {
    			//Has more matter maintenance product.
    			//Update count matter and stop this loop
    			countMatter++;
    			break;
    		} else {
    			//count number product in current matter
    			countProduct++;
    		}
    	}
    	//has matter maintenance product
    	if (matterInfo != null) {
    		matterInfo.setAllMatterCount(countMatter);
    		matterInfo.setAllProductCount(countProduct);
    	}

    	return matterInfo;
    }

    /**
     * exportCSV mode 3.
     * @throws IOException e
     */
    @Action("/prdMngExportCSVMode3")
    public void exportCSVMode3() throws IOException {
      //Create printWriter to write data
        PrintWriter out = null;
        try {
            //BOE Nguyen.Chuong 2014/03/26 prepare condition for entMstProduct.
            EntMstProduct product = productManagementActionForm.getEntMstProduct();
            if (null != product) {
                product = prepareEntProductCondition(product);
                productManagementActionForm.setEntMstProduct(product);
            }
            //EOE Nguyen.Chuong 2014/03/26
            // type of file
            ServletActionContext.getResponse().setContentType("application/csv");
            // file name
            String fileName = "csv_Mode3_" + System.currentTimeMillis() + ".csv";
            ServletActionContext.getResponse().setHeader("content-disposition", "fileName=" + fileName);
            // encoding for file in shift-jis
            ServletActionContext.getResponse().setCharacterEncoding("shift-jis");
            // write file
            out = ServletActionContext.getResponse().getWriter();
            StringBuilder sbHeader = new StringBuilder();
            sbHeader.append(getText("updateMode") + ",");
            sbHeader.append(getText("systemProductCode") + ",");
            sbHeader.append(getText("atributeName") + ",");
            sbHeader.append(getText("administrativeName") + ",");
            sbHeader.append(getText("displayName"));
            // Header
            out.print(sbHeader.toString() + "\r\n");

            //Get list syouhin_sys_code to export: if checkAll = true => get list from DB, if checlAll = false => get list from Client param.
            List<String> syouhinSysCodeList;
            //if checkAll = true
            if (productManagementActionForm.getAllCheckBoxValue()) {
               /* // Used when filtering after detail search done.
                if (syouhinSysCodeListStr != null && !StringUtils.equals("", syouhinSysCodeListStr.trim())) {
                    Gson gson = new Gson();
                    Type type = new TypeToken<List<Integer>>() { } .getType();
                    List<Integer> syouhinSysCodeListInt = gson.fromJson(syouhinSysCodeListStr, type);
//                    productManagementActionForm.getEntMstProduct().setSyouhinSysCodeList(syouhinSysCodeListInt);
                }*/
                syouhinSysCodeList = productManagementService.selectAllProductsForExportCSV(productManagementActionForm.getEntMstProduct());
            } else {
                //if checlAll = false
                // String syouhin_sys_code match with detail search
                String syouhinSysCodes = productManagementActionForm.getSyouhinSysCodes();
                if (StringUtils.isBlank(syouhinSysCodes)) {
                    return;
                }
                syouhinSysCodeList = Arrays.asList(syouhinSysCodes.split(","));
            }
            if (syouhinSysCodeList.size() <= 0) {
                return;
            }
            //Get attribute Code
            String attributeCodes = productManagementActionForm.getAttributeCodes();
            List<String> attributeCodeList = null;
            if (StringUtils.isNotBlank(attributeCodes)) {
                attributeCodeList = Arrays.asList(attributeCodes.split(","));
            }
            //list data to export.
            List<EntCSVMode3> data = new ArrayList<EntCSVMode3>();
            // Select info of maximum 10000 products each time to prevent connection timeout with MySQL
            for (int i = 0; i <= syouhinSysCodeList.size() / Constant.INT_10000; i++) {
                int from = i * Constant.INT_10000;
                int to = (i + 1) * Constant.INT_10000;
                if (to > syouhinSysCodeList.size()) {
                    to = syouhinSysCodeList.size();
                }
                List<EntCSVMode3> tmp = productManagementService.selectProductAttributeBySyouhinSysCodeList(syouhinSysCodeList.subList(from, to)
                                                                                                          , attributeCodeList);
                data.addAll(tmp);
            }
            StringBuilder sbData;
            for (EntCSVMode3 ent : data) {
                sbData = new StringBuilder();
                sbData.append("" + ",");
                sbData.append(ent.getProductSyouhinSysCode() + ",");
                sbData.append(ent.getAttributeName() + ",");
                sbData.append(ent.getAttributeValues1() + ",");
                sbData.append(ent.getAttributeDisplay1());
                sbData.append("\r\n");
                out.print(sbData.toString());
            }
        } finally {
            if (out != null) {
                try {
                    out.flush();
                    out.close();
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            }
        }
    }

    /**
     * exportCSV mode 2.
     * @throws IOException e
     */
    @Action("/prdMngExportCSVMode2")
    public void exportCSVMode2() throws IOException {
        PrintWriter out = null;
        try {
            //BOE Nguyen.Chuong 2014/03/26 prepare condition for entMstProduct.
            EntMstProduct product = productManagementActionForm.getEntMstProduct();
            if (null != product) {
                product = prepareEntProductCondition(product);
                productManagementActionForm.setEntMstProduct(product);
            }
            //EOE Nguyen.Chuong 2014/03/26
            // type of file
            ServletActionContext.getResponse().setContentType("application/csv");
            // file name
            String fileName = "csv_Mode2_" + System.currentTimeMillis() + ".csv";
            ServletActionContext.getResponse().setHeader("content-disposition", "fileName=" + fileName);
            // encoding for file in shift-jis
            ServletActionContext.getResponse().setCharacterEncoding("shift-jis");
            // write file
            out = ServletActionContext.getResponse().getWriter();
            StringBuilder sbHeader = new StringBuilder();
            sbHeader.append(getText("updateMode") + ",");
            sbHeader.append(getText("systemProductCode") + ",");
            sbHeader.append(getText("maker") + ",");
            sbHeader.append(getText("carModel") + ",");
            sbHeader.append(getText("bikeModel"));
            // Header
            out.print(sbHeader.toString() + "\r\n");

          //Get list syouhin_sys_code to export: if checkAll = true => get list from DB, if checlAll = false => get list from Client param.
            List<String> syouhinSysCodeList;
            //if checkAll = true
            if (productManagementActionForm.getAllCheckBoxValue()) {
                // Used when filtering after detail search done.
               /* if (syouhinSysCodeListStr != null && !StringUtils.equals("", syouhinSysCodeListStr.trim())) {
                    Gson gson = new Gson();
                    Type type = new TypeToken<List<Integer>>() { } .getType();
                    List<Integer> syouhinSysCodeListInt = gson.fromJson(syouhinSysCodeListStr, type);
//                    productManagementActionForm.getEntMstProduct().setSyouhinSysCodeList(syouhinSysCodeListInt);
                }*/
                syouhinSysCodeList = productManagementService.selectAllProductsForExportCSV(productManagementActionForm.getEntMstProduct());
            } else {
                //if checlAll = false
                // String syouhin_sys_code match with detail search
                String syouhinSysCodes = productManagementActionForm.getSyouhinSysCodes();
                if (StringUtils.isBlank(syouhinSysCodes)) {
                    return;
                }
                syouhinSysCodeList = Arrays.asList(syouhinSysCodes.split(","));
            }
            if (syouhinSysCodeList.size() <= 0) {
                return;
            }
            List<EntCSVMode3> data = new ArrayList<EntCSVMode3>();
            // Select info of maximum 10000 products each time to prevent connection timeout with MySQL
            for (int i = 0; i <= syouhinSysCodeList.size() / Constant.INT_10000; i++) {
                int from = i * Constant.INT_10000;
                int to = (i + 1) * Constant.INT_10000;
                if (to > syouhinSysCodeList.size()) {
                    to = syouhinSysCodeList.size();
                }
                List<EntCSVMode3> tmp = productManagementService.selectSyouhinFitModelBySyouhinSysCodeList(syouhinSysCodeList.subList(from, to));
                data.addAll(tmp);
            }
            StringBuilder sbData;
            for (EntCSVMode3 ent : data) {
                sbData = new StringBuilder();
                sbData.append("" + ",");
                sbData.append(ent.getAttributeValues1() + ",");
                sbData.append(ent.getAttributeValues2() + ",");
                sbData.append(ent.getAttributeValues3() + ",");
                sbData.append(ent.getAttributeValues4());
                sbData.append("\r\n");
                out.print(sbData.toString());
            }
        } finally {
            if (out != null) {
                try {
                    out.flush();
                    out.close();
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            }
        }
    }

    /**
     * exportCSV mode 1.
     * @throws IOException e
     */
    @Action("/prdMngExportCSVMode1")
    public void exportCSVMode1() throws IOException {
        //Create printWriter to write data
        PrintWriter out = null;
        try {
            //BOE Nguyen.Chuong 2014/03/26 prepare condition for entMstProduct.
            EntMstProduct product = productManagementActionForm.getEntMstProduct();
            if (null != product) {
                product = prepareEntProductCondition(product);
                productManagementActionForm.setEntMstProduct(product);
            }
            //EOE Nguyen.Chuong 2014/03/26
            // type of file
            ServletActionContext.getResponse().setContentType("application/csv");
            // file name
            String fileName = "csv_Mode1_" + System.currentTimeMillis() + ".csv";
            ServletActionContext.getResponse().setHeader("content-disposition", "fileName=" + fileName);
            // encoding for file in shift-jis
            ServletActionContext.getResponse().setCharacterEncoding("shift-jis");
            // get headers
            String headerString = productManagementActionForm.getHeaders();
            //Set writer
            out = ServletActionContext.getResponse().getWriter();
            //Get list syouhin_sys_code to export: if checkAll = true => get list from DB, if checlAll = false => get list from Client param.
            List<String> syouhinSysCodeList;
            //if checkAll = true
            if (productManagementActionForm.getAllCheckBoxValue()) {
                // Used when filtering after detail search done.
                /*if (syouhinSysCodeListStr != null && !StringUtils.equals("", syouhinSysCodeListStr.trim())) {
                    Gson gson = new Gson();
                    Type type = new TypeToken<List<Integer>>() { } .getType();
                    List<Integer> syouhinSysCodeListInt = gson.fromJson(syouhinSysCodeListStr, type);
//                    productManagementActionForm.getEntMstProduct().setSyouhinSysCodeList(syouhinSysCodeListInt);
                }*/
                syouhinSysCodeList = productManagementService.selectAllProductsForExportCSV(productManagementActionForm.getEntMstProduct());
            } else {
                //if checlAll = false
                // String syouhin_sys_code match with detail search
                String syouhinSysCodes = productManagementActionForm.getSyouhinSysCodes();
                if (StringUtils.isBlank(syouhinSysCodes)) {
                    return;
                }
                syouhinSysCodeList = Arrays.asList(syouhinSysCodes.split(","));
            }
            if (syouhinSysCodeList.size() <= 0) {
                return;
            }
            //list data to export.
            List<EntCSVMode1> data = new ArrayList<EntCSVMode1>();
            List<EntCSVMode1> tmp;
            // Select info of maximum 2000 products each time to prevent connection timeout with MySQL
            // This query select data in multiple tables, so that limit at 2000 per select
            for (int i = 0; i <= syouhinSysCodeList.size() / Constant.INT_2000; i++) {
                int from = i * Constant.INT_2000;
                int to = (i + 1) * Constant.INT_2000;
                if (to > syouhinSysCodeList.size()) {
                    to = syouhinSysCodeList.size();
                }
                tmp = productManagementService.selectSyouhinInfoExportCSVMode1SingleTable(syouhinSysCodeList.subList(from, to));
                data.addAll(tmp);
            }
            String attributeCodes = productManagementActionForm.getAttributeCodes();
            List<String> attributeCodeList = null;
            List<EntMstAttribute> attributeNameList = new ArrayList<EntMstAttribute>();
            if (StringUtils.isNotBlank(attributeCodes)) {
                attributeCodeList = Arrays.asList(attributeCodes.split(","));
                List<EntCSVMode3> attributeDataList = new ArrayList<EntCSVMode3>();
                List<EntCSVMode3> tmp3;
                // Select info of maximum 5000 products each time to prevent connection timeout with MySQL
                // This query select data in few tables, so that limit at 5000 per select
                for (int i = 0; i <= syouhinSysCodeList.size() / Constant.INT_5000; i++) {
                    int from = i * Constant.INT_5000;
                    int to = (i + 1) * Constant.INT_5000;
                    if (to > syouhinSysCodeList.size()) {
                        to = syouhinSysCodeList.size();
                    }
                    tmp3 = productManagementService.selectProductAttributeBySyouhinSysCodeList(syouhinSysCodeList.subList(from, to),
                    attributeCodeList);
                    attributeDataList.addAll(tmp3);
                }
                // set attribute code for each field
                attributeNameList = productManagementService.selectAttributeByCodes(attributeCodeList);
                productManagementService.appendAttributeCode(data, attributeNameList);
                // set attribute data for each field
                productManagementService.appendAttributeData(data, attributeDataList);
            }
            if (data.size() > 0) {
                if (headerString.contains("supportedBike")) {
                    List<EntCSVMode1> subList = new ArrayList<EntCSVMode1>();
                    // Select info of maximum 5000 products each time to prevent connection timeout with MySQL
                    // This query select data in few tables, so that limit at 5000 per select
                    for (int i = 0; i <= syouhinSysCodeList.size() / Constant.INT_5000; i++) {
                        int from = i * Constant.INT_5000;
                        int to = (i + 1) * Constant.INT_5000;
                        if (to > syouhinSysCodeList.size()) {
                            to = syouhinSysCodeList.size();
                        }
                        tmp = productManagementService.selectProductFitModelBySyouhinSysCodeList(syouhinSysCodeList.subList(from, to));
                        subList.addAll(tmp);
                    }
                    productManagementService.appendDataByFieldName(data, subList, "compatibleModel");
                }
                if (headerString.contains("customerConfirmationItem")) {
                    List<EntCSVMode1> subList = new ArrayList<EntCSVMode1>();
                    // Select info of maximum 5000 products each time to prevent connection timeout with MySQL
                    // This query select data in few tables, so that limit at 5000 per select
                    for (int i = 0; i <= syouhinSysCodeList.size() / Constant.INT_5000; i++) {
                        int from = i * Constant.INT_5000;
                        int to = (i + 1) * Constant.INT_5000;
                        if (to > syouhinSysCodeList.size()) {
                            to = syouhinSysCodeList.size();
                        }
                        tmp = productManagementService.selectProductGuestInputInfoBySyouhinSysCodeList(syouhinSysCodeList.subList(from, to));
                        subList.addAll(tmp);
                    }
                    productManagementService.appendDataByFieldName(data, subList, "customerConfirmationItem");
                }
                if (headerString.contains("link")) {
                    List<EntCSVMode1> subList = new ArrayList<EntCSVMode1>();
                    // Select info of maximum 5000 products each time to prevent connection timeout with MySQL
                    // This query select data in few tables, so that limit at 5000 per select
                    for (int i = 0; i <= syouhinSysCodeList.size() / Constant.INT_5000; i++) {
                        int from = i * Constant.INT_5000;
                        int to = (i + 1) * Constant.INT_5000;
                        if (to > syouhinSysCodeList.size()) {
                            to = syouhinSysCodeList.size();
                        }
                        tmp = productManagementService.selectProductLinkInfoBySyouhinSysCodeList(syouhinSysCodeList.subList(from, to));
                        subList.addAll(tmp);
                    }
                    productManagementService.appendDataByFieldName(data, subList, "link");
                }
                if (headerString.contains("video")) {
                    List<EntCSVMode1> subList = new ArrayList<EntCSVMode1>();
                    // Select info of maximum 5000 products each time to prevent connection timeout with MySQL
                    // This query select data in few tables, so that limit at 5000 per select
                    for (int i = 0; i <= syouhinSysCodeList.size() / Constant.INT_5000; i++) {
                        int from = i * Constant.INT_5000;
                        int to = (i + 1) * Constant.INT_5000;
                        if (to > syouhinSysCodeList.size()) {
                            to = syouhinSysCodeList.size();
                        }
                        tmp = productManagementService.selectProductVideoInfoBySyouhinSysCodeList(syouhinSysCodeList.subList(from, to));
                        subList.addAll(tmp);
                    }
                    productManagementService.appendDataByFieldName(data, subList, "video");
                }
                if (headerString.contains("image")) {
                    List<EntCSVMode1> subList = new ArrayList<EntCSVMode1>();
                    // Select info of maximum 5000 products each time to prevent connection timeout with MySQL
                    // This query select data in few tables, so that limit at 5000 per select
                    for (int i = 0; i <= syouhinSysCodeList.size() / Constant.INT_5000; i++) {
                        int from = i * Constant.INT_5000;
                        int to = (i + 1) * Constant.INT_5000;
                        if (to > syouhinSysCodeList.size()) {
                            to = syouhinSysCodeList.size();
                        }
                        tmp = productManagementService.selectProductImageInfoBySyouhinSysCodeList(syouhinSysCodeList.subList(from, to));
                        subList.addAll(tmp);
                    }
                    productManagementService.appendImageSelectData(data, subList, "image");
                }
                if (headerString.contains("option")) {
                    List<EntCSVMode1> subList = new ArrayList<EntCSVMode1>();
                    // Select info of maximum 5000 products each time to prevent connection timeout with MySQL
                    // This query select data in few tables, so that limit at 5000 per select
                    for (int i = 0; i <= syouhinSysCodeList.size() / Constant.INT_5000; i++) {
                        int from = i * Constant.INT_5000;
                        int to = (i + 1) * Constant.INT_5000;
                        if (to > syouhinSysCodeList.size()) {
                            to = syouhinSysCodeList.size();
                        }
                        tmp = productManagementService.selectProductSelectInfoBySyouhinSysCodeList(syouhinSysCodeList.subList(from, to));
                        subList.addAll(tmp);
                    }
                    productManagementService.appendImageSelectData(data, subList, "option");
                }
            }
            // Header
            StringBuilder sbHeader = new StringBuilder();
            createHeader(headerString, sbHeader, new StringBuilder(), new EntCSVMode1(), attributeNameList);
            out.print(sbHeader.toString().replaceFirst(",", "") + "\r\n");
            StringBuilder sbData;
            for (EntCSVMode1 ent : data) {
                sbData = new StringBuilder();
                createHeader(headerString, new StringBuilder(), sbData, ent, attributeNameList);
                out.print(sbData.toString().replaceFirst(",", "") +  "\r\n");
            }
        } finally {
            if (out != null) {
                try {
                    out.flush();
                    out.close();
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            }
        }
    }

    /**
     * createHeader.
     * @param headerString String
     * @param sbHeader StringBuilder
     * @param sbData StringBuilder
     * @param attributeNameList List<EntMstAttribute>
     * @param data EntCSVMode1
     */
    private void createHeader(String headerString, StringBuilder sbHeader, StringBuilder sbData, EntCSVMode1 data, List<EntMstAttribute> attributeNameList) {
        if (headerString.contains("updateMode")) {
            sbHeader.append("," + getText("updateMode"));
            sbData.append(",");
        }
        sbHeader.append("," + getText("systemProductCode")); sbData.append("," + data.getProductSyouhinSysCode());
        if (headerString.contains("brandCode")) {
            sbHeader.append("," + getText("brandCode")); sbData.append("," + data.getProductBrandCode());
        }
        if (headerString.contains("brandName")) {
            sbHeader.append("," + getText("brandName")); sbData.append("," + data.getProductBrandName());
        }
        if (headerString.contains("itemCode2")) {
            sbHeader.append("," + getText("itemCode2")); sbData.append("," + data.getProductCategoryCode());
        }
        if (headerString.contains("itemName")) {
            sbHeader.append("," + getText("itemName")); sbData.append("," + data.getProductCategoryName());
        }
        if (headerString.contains("productName")) {
            sbHeader.append("," + getText("productName2")); sbData.append("," + data.getProductName());
        }
        if (headerString.contains("makerStockNumber")) {
            sbHeader.append("," + getText("makerStockNumber")); sbData.append("," + data.getProductCode());
        }
        if (headerString.contains("notStockNumber")) {
            sbHeader.append("," + getText("notStockNumber")); sbData.append("," + data.getProductWebikeCodeFlg());
        }
        if (headerString.contains("jan")) {
            sbHeader.append("," + getText("jan")); sbData.append("," + data.getProductEanCode());
        }
        if (headerString.contains("listPrice")) {
            sbHeader.append("," + getText("listPrice")); sbData.append("," + data.getProductProperPrice());
        }
        if (headerString.contains("division")) {
            sbHeader.append("," + getText("division")); sbData.append("," + data.getSupplierPricePrice());
        }
        if (headerString.contains("groupCode")) {
            sbHeader.append("," + getText("groupCode")); sbData.append("," + data.getProductGroupCode());
        }
        if (headerString.contains("dateOfIssue")) {
            sbHeader.append("," + getText("dateOfIssue")); sbData.append("," + data.getProductSupplierReleaseDate());
        }
        if (headerString.contains("openPrice")) {
            sbHeader.append("," + getText("openPrice")); sbData.append("," + data.getProductOpenPriceFlg());
        }
        if (headerString.contains("salePrice")) {
            sbHeader.append("," + getText("salePrice")); sbData.append("," + data.getProductProperSellingFlg());
        }
        if (headerString.contains("buildToOrderManufacturing")) {
            sbHeader.append("," + getText("buildToOrderManufacturing")); sbData.append("," + data.getProductOrderProductFlg());
        }
        if (headerString.contains("nonReturnable")) {
            sbHeader.append("," + getText("nonReturnable")); sbData.append("," + data.getProductNoReturnableFlg());
        }
        if (headerString.contains("preferenceImage")) {
            sbHeader.append("," + getText("preferenceImage")); sbData.append("," + data.getProductAmbiguousImageFlg());
        }
        //BOE Nguyen.Chuong 2014/04/03: format to duplicate quote symbol (") to fix CSV export error when data contain quote symbol (")
//        if (headerString.contains("summary")) {
//            sbHeader.append("," + getText("summary")); sbData.append(",\"" + data.getDescriptionSummary() + "\"");
//        }
//        if (headerString.contains("note")) {
//            sbHeader.append("," + getText("note")); sbData.append(",\"" + data.getDescriptionRemarks() + "\"");
//        }
//        if (headerString.contains("caution")) {
//            sbHeader.append("," + getText("caution")); sbData.append(",\"" + data.getDescriptionCaution() + "\"");
//        }
//        if (headerString.contains("explanation")) {
//            sbHeader.append("," + getText("explanation")); sbData.append(",\"" + data.getDescriptionSentence() + "\"");
//        }
        if (headerString.contains("summary")) {
        	//BOE #9607 Phan. Tinh 2014/07/01 delete ["] when export csv
            //sbHeader.append("," + getText("summary")); sbData.append(",\"" + Utils.dobuleQuoteChar(data.getDescriptionSummary()) + "\"");
            sbHeader.append("," + getText("summary")); sbData.append("," + Utils.dobuleQuoteChar(data.getDescriptionSummary()));
          //EOE #9607 Phan. Tinh 2014/07/01
        }
        if (headerString.contains("note")) {
        	//BOE #9607 Phan. Tinh 2014/07/01 delete ["] when export csv
            //sbHeader.append("," + getText("note")); sbData.append(",\"" + Utils.dobuleQuoteChar(data.getDescriptionRemarks()) + "\"");
            sbHeader.append("," + getText("note")); sbData.append("," + Utils.dobuleQuoteChar(data.getDescriptionRemarks()));
          //EOE #9607 Phan. Tinh 2014/07/01
        }
        if (headerString.contains("caution")) {
        	//BOE #9607 Phan. Tinh 2014/07/01 delete ["] when export csv
            //sbHeader.append("," + getText("caution")); sbData.append(",\"" + Utils.dobuleQuoteChar(data.getDescriptionCaution()) + "\"");
            sbHeader.append("," + getText("caution")); sbData.append("," + Utils.dobuleQuoteChar(data.getDescriptionCaution()));
          //EOE #9607 Phan. Tinh 2014/07/01
        }
        if (headerString.contains("explanation")) {
        	//BOE #9607 Phan. Tinh 2014/07/01 delete ["] when export csv
            //sbHeader.append("," + getText("explanation")); sbData.append(",\"" + Utils.dobuleQuoteChar(data.getDescriptionSentence()) + "\"");
            sbHeader.append("," + getText("explanation")); sbData.append("," + Utils.dobuleQuoteChar(data.getDescriptionSentence()));
          //EOE #9607 Phan. Tinh 2014/07/01
        }
        //EOE Nguyen.Chuong 2014/04/03
        if (headerString.contains("supplierCode1")) {
            sbHeader.append("," + getText("supplierCode1")); sbData.append("," + data.getSiireCode1());
        }
        if (headerString.contains("supplierName1")) {
            sbHeader.append("," + getText("supplierName1")); sbData.append("," + data.getSiireName1());
        }
        if (headerString.contains("supplierCode1")) {
            sbHeader.append("," + getText("deliveryTimeCode1")); sbData.append("," + data.getNoukiCode1());
        }
        if (headerString.contains("supplierName1")) {
            sbHeader.append("," + getText("deliveryTimeName1")); sbData.append("," + data.getNoukiName1());
        }
        if (headerString.contains("supplierCode2")) {
            sbHeader.append("," + getText("supplierCode2")); sbData.append("," + data.getSiireCode2());
        }
        if (headerString.contains("supplierName2")) {
            sbHeader.append("," + getText("supplierName2")); sbData.append("," + data.getSiireName2());
        }
        if (headerString.contains("supplierCode2")) {
            sbHeader.append("," + getText("deliveryTimeCode2")); sbData.append("," + data.getNoukiCode2());
        }
        if (headerString.contains("supplierName2")) {
            sbHeader.append("," + getText("deliveryTimeName2")); sbData.append("," + data.getNoukiName2());
        }
        if (headerString.contains("supportedBike")) {
            sbHeader.append("," + getText("compatibleModel")); sbData.append("," + data.getcompatibleModelNotNull());
        }
        // image
        for (int i = 1; i <= Constant.PRODUCT_MAX_IMAGE_LENGTH; i++) {
            // check to identify image1 vs image10
            if (i == 1 && StringUtils.countMatches(headerString, "image1") == 1 && StringUtils.countMatches(headerString, "image10") == 1) {
                continue;
            }
            if (headerString.contains("image" + i)) {
                sbHeader.append("," + getText("thumbnail") + i);
                sbHeader.append("," + getText("detailedImage") + i);
                try {
                    sbData.append("," + BeanUtils.getProperty(data, "productImageThumbnailPath" + i + "NotNull"));
                    sbData.append("," + BeanUtils.getProperty(data, "productImageDetailPath" + i + "NotNull"));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        // select option
        for (int i = 1; i <= Constant.PRODUCT_MAX_OPTION_LENGTH; i++) {
            if (i == 1 && StringUtils.countMatches(headerString, "option1") == 1 && StringUtils.countMatches(headerString, "option10") == 1) {
                continue;
            }
            if (headerString.contains("option" + i)) {
                sbHeader.append("," + getText("lookupCode") + i);
                sbHeader.append("," + getText("optionName") + i);
                try {
                    sbData.append("," + BeanUtils.getProperty(data, "selectCode" + i + "Str"));
                    sbData.append("," + BeanUtils.getProperty(data, "selectName" + i + "NotNull"));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        if (headerString.contains("customerConfirmationItem")) {
            sbHeader.append("," + getText("customerConfirmationItem")); sbData.append("," + data.getCustomersConfirmationItemNotNull());
        }
        if (headerString.contains("link")) {
            sbHeader.append("," + getText("link")); sbData.append("," + data.getLinkNotNull());
        }
        if (headerString.contains("video")) {
            sbHeader.append("," + getText("video")); sbData.append("," + data.getAnimationNotNull());
        }
        // attribute
        for (int i = 0; i < attributeNameList.size(); i++) {
            sbHeader.append("," + attributeNameList.get(i).getAttributeName() + "_" + getText("administrativeName"));
            sbHeader.append("," + attributeNameList.get(i).getAttributeName() + "_" + getText("displayName"));
            try {
                sbData.append("," + BeanUtils.getProperty(data, "attributevalues" + (i + 1) + "NotNull"));
                sbData.append("," + BeanUtils.getProperty(data, "attributeDisplay" + (i + 1) + "NotNull"));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /************************************************************************
     * <b>Description:</b><br>
     *  prepare EntMstProduct with more condition of search.
     *
     * @author		Nguyen.Chuong
     * @date		Mar 26, 2014
     * @param       product EntMstProduct in actionForm
     * @return		EntMstProduct
     ************************************************************************/
    public EntMstProduct prepareEntProductCondition(EntMstProduct product) {
        if (product.getDetailSearchFlag() == 2) {
            // Get various flag then set to form.
            String []parts = variousFlag.split(",");
            for (String part : parts) {
                if (!part.equals("")) {
                    // Convert it to integer to compare.
                    int partInt = Integer.parseInt(part);
                    if (partInt == EnumVariousFlag.OPEN_PRICE.getKey()) {
                        product.setProductOpenPriceFlg(true);
                    } else if (partInt == EnumVariousFlag.NOT_RETURNED_OR_EXCHANGED.getKey()) {
                        product.setProductNoReturnableFlg(true);
                    } else if (partInt == EnumVariousFlag.BUILD_TO_ORDER_MANUFACTURING.getKey()) {
                        product.setProductOrderProductFlg(true);
                    } else if (partInt == EnumVariousFlag.PREFERENCE_IMAGE.getKey()) {
                        product.setProductAmbigousImageFlg(true);
                    } else if (partInt == EnumVariousFlag.SALE_PRICE.getKey()) {
                        product.setProductProperSellingFlg(true);
                    }
                }
            }
            // Get multiSearch criteria then set to form.
            String []criteria = multiSearchCriteria.split(",");
            List<String> multiSearchCriteriaList = new ArrayList<String>();
            for (String criterion : criteria) {
                if (!criterion.equals("")) {
                    multiSearchCriteriaList.add(criterion);
                }
            }
            product.setMultiSearchCriteriaList(multiSearchCriteriaList);
        }
        return product;
    }

    /**
     * @return the productManagementActionForm
     */
    public ProductManagementActionForm getProductManagementActionForm() {
        return productManagementActionForm;
    }

    /**
     * @param productManagementActionForm the productManagementActionForm to set
     */
    public void setProductManagementActionForm(
            ProductManagementActionForm productManagementActionForm) {
        this.productManagementActionForm = productManagementActionForm;
    }

    /**
     * @return the hostAddress
     */
    public String getHostAddress() {
        return hostAddress;
    }

    /**
     * @param hostAddress the hostAddress to set
     */
    public void setHostAddress(String hostAddress) {
        this.hostAddress = hostAddress;
    }

    /**
     * @return the page
     */
    public int getPage() {
        return page;
    }

    /**
     * @param page the page to set
     */
    public void setPage(int page) {
        this.page = page;
    }

    /**
     * @return the pageSize
     */
    public int getPageSize() {
        return pageSize;
    }

    /**
     * @param pageSize the pageSize to set
     */
    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    /**
     * @return the groupCodeParam
     */
    public String getGroupCodeParam() {
        return groupCodeParam;
    }

    /**
     * @param groupCodeParam the groupCodeParam to set
     */
    public void setGroupCodeParam(String groupCodeParam) {
        this.groupCodeParam = groupCodeParam;
    }

    public void setVariousFlag(String variousFlag) {
		this.variousFlag = variousFlag;
	}
    public String getVariousFlag() {
		return variousFlag;
	}

    public String getMultiSearchCriteria() {
		return multiSearchCriteria;
	}
    public void setMultiSearchCriteria(String multiSearchCriteria) {
		this.multiSearchCriteria = multiSearchCriteria;
	}

	public void setSyouhinSysCodeListStr(String syouhinSysCodeListStr) {
		this.syouhinSysCodeListStr = syouhinSysCodeListStr;
	}

    public String getSyouhinSysCodeListStr() {
		return syouhinSysCodeListStr;
	}

    /**
     * @return the maxNumberProductMaintenanceAction
     */
    public int getMaxNumberProductMaintenanceAction() {
        return maxNumberProductMaintenanceAction;
    }

    /**
     * @param maxNumberProductMaintenanceAction the maxNumberProductMaintenanceAction to set
     */
    public void setMaxNumberProductMaintenanceAction(
            int maxNumberProductMaintenanceAction) {
        this.maxNumberProductMaintenanceAction = maxNumberProductMaintenanceAction;
    }

    /**
     * @return the maxNumberProductExportCSV
     */
    public int getMaxNumberProductExportCSV() {
        return maxNumberProductExportCSV;
    }

    /**
     * @param maxNumberProductExportCSV the maxNumberProductExportCSV to set
     */
    public void setMaxNumberProductExportCSV(int maxNumberProductExportCSV) {
        this.maxNumberProductExportCSV = maxNumberProductExportCSV;
    }

    /**
     * @return the brandCodeParam
     */
    public String getBrandCodeParam() {
        return brandCodeParam;
    }

    /**
     * @param brandCodeParam the brandCodeParam to set
     */
    public void setBrandCodeParam(String brandCodeParam) {
        this.brandCodeParam = brandCodeParam;
    }

    /**
     * @return the brandNameParam
     */
    public String getBrandNameParam() {
        return brandNameParam;
    }

    /**
     * @param brandNameParam the brandNameParam to set
     */
    public void setBrandNameParam(String brandNameParam) {
        this.brandNameParam = brandNameParam;
    }
}
