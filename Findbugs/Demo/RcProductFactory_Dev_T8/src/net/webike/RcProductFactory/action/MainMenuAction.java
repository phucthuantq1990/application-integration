/************************************************************************
 * File Name	： MainMenuAction.java
 * Author		： Nguyen.Chuong
 * Version		： 1.0.0
 * Date Created	： 2013/09/24
 * Date Updated	： 2013/09/24
 * Description	： Show, Filter Matter Info.
 *
 ***********************************************************************/
package net.webike.RcProductFactory.action;

import net.webike.RcProductFactory.action.form.MainMenuActionForm;
import net.webike.RcProductFactory.service.MatterService;
import net.webike.RcProductFactory.util.Constant;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Controller;

/**
 * MainMenu action.
 */
@Controller
@Scope("request")
@ParentPackage(value = "CommonInterceptor")
@Results(
    {
    	// On loading default matter detail page
        @Result(name = "success", location = "mainMenu.jsp"),
        // Return error page
        @Result(name = "errorOther", type = "redirect", location = "error.html")
    }
)
public class MainMenuAction extends CommonAction {

    /**
	 * @serialVersionUID SerialVersion
	 */
	private static final long serialVersionUID = 1L;

	/**
     * Main Menu Action Form.
     */
    @Autowired
    private MainMenuActionForm mainMenuActionForm;

    /**
     * Matter Service.
     */
    @Autowired
    private MatterService matterService;


    /**
     * Status PJ, default is '00'.
     */
    private String status = Constant.MATTER_STATUS_DEFAULT;

    /**
     * String Login Id.
     */
    private String user;

    @Override
    @Action("/mainMenu")
    public String execute() {
        try {
            //Validate loginUser
            if (!validateLoginUser()) {
                return "errorOther";
            }
            //Get common list status and user for 2 combobox in UI.
            getCommonListStatusMatterAndUser();
            //Get list of matter and set to form
            user = this.getCommonForm().getLoginId();
            mainMenuActionForm.setListMatter(matterService.selectListMatterByStatusAndUser(status, user));
        } catch (DataAccessException e) {
            this.getLogger().error("[MainMenuAction] An error to get data in "
                                    + "execute function");
            this.getLogger().error(e.toString());
            return "errorOther";
        } catch (Exception e) {
            this.getLogger().error("[MainMenuAction] An error execute function");
            this.getLogger().error(e.toString());
            return "errorOther";
        }
        return SUCCESS;
    }

    /**
     * @return the mainMenuActionForm
     */
    public MainMenuActionForm getMainMenuActionForm() {
        return mainMenuActionForm;
    }

    /**
     * @param mainMenuActionForm the mainMenuActionForm to set
     */
    public void setMainMenuActionForm(MainMenuActionForm mainMenuActionForm) {
        this.mainMenuActionForm = mainMenuActionForm;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return the user
     */
    public String getUser() {
        return user;
    }

    /**
     * @param user the user to set
     */
    public void setUser(String user) {
        this.user = user;
    }

    /**
     * @return the matterService
     */
    public MatterService getMatterService() {
        return matterService;
    }

    /**
     * @param matterService the matterService to set
     */
    public void setMatterService(MatterService matterService) {
        this.matterService = matterService;
    }
}
