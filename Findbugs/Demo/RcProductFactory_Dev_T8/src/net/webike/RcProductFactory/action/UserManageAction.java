/************************************************************************
 * File Name	： UserManage.java
 * Author		： Tran.Thanh
 * Version		： 1.0.0
 * Date Created	： 2013/10/02
 * Date Updated	： 2013/10/02
 * Description	： User manage for admin.
 *
 ***********************************************************************/
package net.webike.RcProductFactory.action;

import java.util.List;

import net.webike.RcProductFactory.action.form.UserManageActionForm;
import net.webike.RcProductFactory.entity.EntMstFactoryUserTmp;
import net.webike.RcProductFactory.entity.UserStatus;
import net.webike.RcProductFactory.service.UserTmpService;
import net.webike.RcProductFactory.util.Constant;

import org.apache.commons.lang.StringUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.apache.struts2.json.annotations.JSON;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataAccessException;

/**
 * PJ Detail Action Class.
 */
@Scope("request")
@ParentPackage(value = "CommonInterceptor")
@Results(
    {
    	// On loading default user manage page
        @Result(name = "success", location = "userManage.jsp"),
        // After change status success return main user manage page
        @Result(name = "userManage", type = "redirectAction", location = "userManage"),
        // After change status success return detail user page with param
        @Result(name = "userDetail", type = "redirectAction", location = "userDetail", params = {"userId", "${id}" }),
        /* BOE move from userMangeAction to load jSon in file @rcv!Nguyen.Chuong 2014/01/13 [6115]*/
        //Return json after filter.
        @Result(name = "list", type = "json"),
        /* EOE move from userMangeAction to load jSon in file @rcv!Nguyen.Chuong 2014/01/13 [6115]*/
        // Return error page when parameter fail
        @Result(name = "error", type = "redirect", location = "error.html?error=param"),
        // Return error page when parameter fail
        @Result(name = "errorOther", type = "redirect", location = "error.html")
    }
)
public class UserManageAction extends CommonAction {

	/**
	 * @serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
    //delete or active user: delflg.
    private static final String DEL_FLAG_DELETE = "0";
    /**
     * @userService userService
     */
    @Autowired
    private UserTmpService userTmprvice;

	@Autowired
    private UserManageActionForm userManageActionForm;

	/* BOE move from userMangeAction to load jSon when search in file @rcv!Nguyen.Chuong 2014/01/13 [6115]*/
	/**
     * List of EntMstFactoryUser.
     */
    private List<EntMstFactoryUserTmp> userManageBodyList;
    /* EOE move from userMangeAction to load jSon when search in file @rcv!Nguyen.Chuong 2014/01/13 [6115]*/
	@Override
    @Action("/userManage")
    public String execute() {
    	try {
    	    //Validate loginUser
    	    if (!validateLoginUser()) {
    	        return "errorOther";
    	    }
	    	//Set List All User using query
    		EntMstFactoryUserTmp entUser = new EntMstFactoryUserTmp();
    		entUser.setUserId("");
	    	entUser.setUserName("");
//	    	entUser.setUserAuthority("");
	    	entUser.setUserDelFlg("0");
	    	List<EntMstFactoryUserTmp> allUser =  userTmprvice.selectFilterUser(entUser);
	    	/* BOE login with new process: login by redmine @rcv!Nguyen.Chuong 2014/01/13 [6115]*/
//	    	userManageActionForm.setAuthorityTreeMap(UserAuthority.getListTreeMap());
	    	/* EOE login with new process: login by redmine @rcv!Nguyen.Chuong 2014/01/13 [6115]*/
	    	userManageActionForm.setStatusTreeMap(UserStatus.getListTreeMap());
	    	userManageActionForm.setListAllUser(allUser);
    	} catch (DataAccessException e) {
            this.getLogger().error("[UserManageAction] An error to get data in "
                                    + "execute function");
            this.getLogger().error(e.toString());
            return "error";
        } catch (Exception e) {
            this.getLogger().error("[UserManageAction] An error execute function");
            this.getLogger().error(e.toString());
            return "error";
        }
    	return SUCCESS;
    }

	/************************************************************************
	 * <b>Description:</b><br>
	 *  Change status user. If del_flg of user is 0 then set del_flg is 1.
	 *  If del_flg of user is 1 then set del_flg is 0.
	 *
	 * @author		Tran.Thanh
	 * @date		3 Oct 2013
	 * @return		String
	 ************************************************************************/
	@Action("/changeStatus")
    public String changeStatus() {
    	try {
    	    //Validate loginUser
    	    if (!validateLoginUser()) {
                return "errorOther";
            }
    		// Get param for change status
    		String userId = this.getRequest().getParameter(Constant.PARAM_USER_ID);
	        String kousinId = this.getCommonForm().getLoginId();
	        String delFlg = this.getRequest().getParameter(Constant.PARAM_DEL_FLAG);

	        if (StringUtils.isEmpty(userId) || StringUtils.isEmpty(kousinId)
	        								|| StringUtils.isEmpty(delFlg)) {
	        	return "error";
	        }
	        if (delFlg.equals(DEL_FLAG_DELETE)) {
	            userTmprvice.deleteUserByUserId(userId, kousinId);
	        } else {
	            userTmprvice.activeUserByUSerId(userId, kousinId);
	        }
    	} catch (DataAccessException e) {
            this.getLogger().error("[UserManageAction] An error to changeStatus user "
                                    + "deleteMatter function");
            this.getLogger().error(e.toString());
            return "error";
        } catch (Exception e) {
            this.getLogger().error("[UserManageAction] An error execute function changeStatus user");
            this.getLogger().error(e.toString());
            return "error";
        }
    	return "success";
    }

	/* BOE move from userMangeAction to load jSon in file @rcv!Nguyen.Chuong 2014/01/13 [6115]*/
	/************************************************************************
     * <b>Description:</b><br>
     *  userManageLoadJson action for load main content after filter userManage page by Ajax.
     *
     * @author        Tran.Thanh
     * @date        2 Oct 2013
     * @return        String
     ************************************************************************/
    @Action("/userManageLoadJson")
    public String userManageLoadJson() {
        //Validate loginUser
        if (!validateLoginUser()) {
            return "errorOther";
        }
        // Get parameter from request
        EntMstFactoryUserTmp entUser = new EntMstFactoryUserTmp();
        entUser.setUserId(this.getRequest().getParameter(Constant.PARAM_USER_ID));
        entUser.setUserName(this.getRequest().getParameter(Constant.PARAM_USER_NAME));
//        entUser.setUserAuthority(this.getRequest().getParameter(Constant.PARAM_USER_AUTHORITY));
        entUser.setUserDelFlg(this.getRequest().getParameter(Constant.PARAM_DEL_FLAG));

        // Get list of Users and set to UserManageBody
        List<EntMstFactoryUserTmp> listUser = userTmprvice.selectFilterUser(entUser);
        this.setUserManageBodyList(listUser);
        return "list";
    }
    /* EOE move from userMangeAction to load jSon in file @rcv!Nguyen.Chuong 2014/01/13 [6115]*/
    /**
	 * @return the userManageActionForm
	 */
	public UserManageActionForm getUserManageActionForm() {
		return userManageActionForm;
	}

	/**
	 * @param userManageActionForm the userManageActionForm to set
	 */
	public void setUserManageActionForm(UserManageActionForm userManageActionForm) {
		this.userManageActionForm = userManageActionForm;
	}

	/**
	 * @return the userService
	 */
	public UserTmpService getUserTmpService() {
		return userTmprvice;
	}

	/**
	 * @param userService the userService to set
	 */
	public void setUserService(UserTmpService userService) {
		this.userTmprvice = userService;
	}
	/* BOE move from userMangeAction to load jSon in file @rcv!Nguyen.Chuong 2014/01/13 [6115]*/
    /**
     * @return the UserManageBodyList
     */
    @JSON(name = "userManageBodyList")
    public List<EntMstFactoryUserTmp> getUserManageBodyList() {
        return userManageBodyList;
    }

    /**
     * @param userManageBodyList the userManageBodyList to set
     */
    public void setUserManageBodyList(List<EntMstFactoryUserTmp> userManageBodyList) {
        this.userManageBodyList = userManageBodyList;
    }
    /* EOE move from userMangeAction to load jSon in file @rcv!Nguyen.Chuong 2014/01/13 [6115]*/
}
