/************************************************************************
 * File Name    ： CommonAction.java
 * Author       ： Nguyen.Chuong
 * Version      ： 1.0.0
 * Date Created ： 2013/09/24
 * Date Updated ： 2013/09/24
 * Description  ： Common action to content all common function.
 *
 ***********************************************************************/
package net.webike.RcProductFactory.action;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import net.webike.RcProductFactory.action.form.CommonForm;
import net.webike.RcProductFactory.entity.EntMstFactoryUserTmp;
import net.webike.RcProductFactory.service.UserTmpService;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.interceptor.annotations.After;
import com.opensymphony.xwork2.interceptor.annotations.Before;

/**
 * Common action class.
 */
@Controller
@Scope("request")
public class CommonAction extends ActionSupport implements ServletRequestAware {
    /**
     * @serialVersionUID serialVersion
     */
    private static final long serialVersionUID = 1L;

    @Autowired
    private CommonForm commonForm;

    @Autowired
    private UserTmpService userTmpService;

    @Autowired
//    private MatterService matterService;

    private HttpServletRequest request;

    private Logger logger;

    private boolean jUnitMode;

    /**
     * Constructor.
     */
    protected CommonAction() {
        this.logger = Logger.getLogger(this.getClass());
    }

    /**
     * Before action logic.
     * @throws Exception excep
     */
    @Before
    public void before() throws Exception {
        // Check authentication
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication == null) {
            this.logger.warn("authentication is null");
            return;
        }
        if (!authentication.isAuthenticated()) {
            this.logger.warn("not authentication");
            return;
        }
        List<String> loginAuthority = new ArrayList<String>();
        for (GrantedAuthority grantedAuthority : authentication.getAuthorities()) {
        	loginAuthority.add(grantedAuthority.getAuthority());
        }
        this.getCommonForm().setLoginAuthority(loginAuthority);
    }

    /**
     * Common action logic.
     * @throws Exception excep
     */
    @After
    public void after() throws Exception {
    }

    /**
     * Common action logic.
     * @return SUCCESS
     * @throws Exception excep
     */
    @Override
    public String execute() throws Exception {
        return SUCCESS;
    }

    /************************************************************************
     * <b>Description:</b><br>
     *  get text from properties file for jsp.
     *
     * @author      Nguyen.Chuong
     * @date        2013/10/02
     * @param       aTextName String
     * @return      String
     ************************************************************************/
    @Override
    public String getText(String aTextName) {
        if (!jUnitMode) {
            return super.getText(aTextName);
        } else {
            return "Junit"; //CONST_JUNIT_MODE;
        }
    }

    /************************************************************************
     * <b>Description:</b><br>
     *  null is returned when not logged in.
     *
     * @author      Nguyen.Chuong
     * @date        24 Sep 2013
     * @param       authentication Auth data
     * @return      userID String
     ************************************************************************/
    public String getLoginId(Authentication authentication) {
        // Get authentication
        Object p = authentication.getPrincipal();

        // Check authentication instance of User Details
        if (p instanceof UserDetails) {
          return ((UserDetails) p).getUsername();
        }

        return p.toString();
    }

    /************************************************************************
     * <b>Description:</b><br>
     *  getPassWordCredentials.
     *
     * @author		Le.Dinh
     * @date		24 Jan 2014
     * @param 		authentication	Authentication
     * @return		String
     ************************************************************************/
    public String getPassWordCredentials(Authentication authentication) {
        // Get authentication
        Object p = authentication.getCredentials();
        return p.toString();

    }

    /************************************************************************
     * <b>Description:</b><br>
     *  get logined authority from authentication object.
     *
     * @author      Nguyen.Chuong
     * @date        Oct 16, 2013
     * @param       authentication Authentication
     * @return      String  authority
     ************************************************************************/
//    private String getLoginAuthority(Authentication authentication) {
//        // Get authentication
//        Object p = authentication.getPrincipal();
//        String autho = "";
//        // Check authentication instance of User Details
//        if (p instanceof UserDetails) {
//            @SuppressWarnings("rawtypes")Collection a = ((UserDetails) p).getAuthorities();
//            StringBuffer buffer = new StringBuffer();
//            for (@SuppressWarnings("rawtypes")Iterator iterator = a.iterator(); iterator.hasNext();) {
//                Object object = (Object) iterator.next();
//                buffer.append(object.toString());
//              }
//            autho = buffer.toString();
//            return autho;
//        }
//        return autho;
//    }

    /************************************************************************
     * <b>Description:</b><br>
     *  Validate login user.
     *
     * @author      Nguyen.Chuong
     * @date        Oct 16, 2013
     * @return      boolean
     ************************************************************************/
    public boolean validateLoginUser() {
        // Get login Id
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String loginId = getLoginId(authentication);
        String passWordCredentials = getPassWordCredentials(authentication);
        //return false if loginId is null
        if (StringUtils.isEmpty(loginId) || StringUtils.isEmpty(passWordCredentials)) {
        	SecurityContextHolder.clearContext();
            return false;
        }

        EntMstFactoryUserTmp entUserTmp =  userTmpService.selectUserById(loginId);
        //Thanh - Get Role in redmine-
        //Check login user is exsited in DB
        if (null == entUserTmp) {
            SecurityContextHolder.clearContext();
            return false;
        } else {
            //Set normal info to commonForm.
        	commonForm.setLoginLastName(entUserTmp.getUserLastName());
        	commonForm.setLoginFirstName(entUserTmp.getUserFirstName());
        	commonForm.setPassWordCredentials(passWordCredentials);
            //this.getCommonForm().setLoginAuthority(entUser.getUserAuthority()); old_code
            List<String> loginAuthority = new ArrayList<String>();
            for (GrantedAuthority grantedAuthority : authentication.getAuthorities()) {
            	loginAuthority.add(grantedAuthority.getAuthority());
            }
            this.getCommonForm().setLoginAuthority(loginAuthority);
            commonForm.setApiAccessKey(entUserTmp.getUserRedmineApiKey());
        }
        this.logger.debug("Logging ID:" + loginId);
        commonForm.setLoginId(loginId);
        return true;
    }

    /************************************************************************
     * <b>Description:</b><br>
     *  Get common list matter Status and list user.
     *
     * @author      Nguyen.Chuong
     * @date        Oct 18, 2013
     ************************************************************************/
    public void getCommonListStatusMatterAndUser() {
     // Get login Id
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        //Get all matter status
        //commonForm.setListCommonStatus(matterService.selectAllMatterStatus());
        commonForm.setListCommonStatus(null);

        //Get list user
        List<EntMstFactoryUserTmp> listUser = new ArrayList<EntMstFactoryUserTmp>();

        //Check Login as admin.
        List<String> permission = this.getCommonForm().getLoginAuthority();
        if (permission.contains(getText("text.role.userManagerRoleId"))) {
            listUser = userTmpService.selectAllUser();
        } else { //Login as operator.
            listUser.add(userTmpService.selectUserById(getLoginId(authentication)));
        }
        commonForm.setListCommonUser(listUser);
    }
    /************************************************************************
     * <b>Description:</b><br>
     *  Format number to decimal format.
     *
     * @author      Luong.Dai
     * @date        Jan 23, 2014
     * @param args double
     * @return      String
     ************************************************************************/
    public String putComma(double args) {
        DecimalFormat decimalFormat1 = new DecimalFormat("###,###.##");
        String outputdata = decimalFormat1.format(args);
        return outputdata;
    }

    /************************************************************************
     * <b>Description:</b><br>
     * check request is ajax or not.
     *
     * @author      Long Vu
     * @date        Feb 27, 2014
     * @param       request request
     * @return      boolean
     ************************************************************************/
    protected boolean isAjax(HttpServletRequest request) {
        return "XMLHttpRequest".equals(request.getHeader("X-Requested-With"));
    }

    /*
     * @see org.apache.struts2.interceptor.ServletRequestAware#setServletRequest(javax.servlet.http.HttpServletRequest)
     */
    @Override
    public void setServletRequest(HttpServletRequest arg0) {
        this.request = arg0;
    }

    /**
     * Getter CommonForm.
     * @return commonForm
     */
    public CommonForm getCommonForm() {
        return commonForm;
    }

    /**
     * Setter CommonForm.
     * @param commonForm commonForm
     */
    public void setCommonForm(CommonForm commonForm) {
        this.commonForm = commonForm;
    }

    /**
     * Getter Request.
     * @return request
     */
    protected HttpServletRequest getRequest() {
        return request;
    }

    /**
     * Setter Request.
     * @param request request
     */
    protected void setRequest(HttpServletRequest request) {
        this.request = request;
    }

    /**
     * Getter Logger.
     * @return logger
     */
    protected Logger getLogger() {
        return logger;
    }

    /**
     * Setter.
     * @param logger logger
     */
    protected void setLogger(Logger logger) {
        this.logger = logger;
    }

    /**
     * @return the jUnitMode
     */
    public boolean isjUnitMode() {
        return jUnitMode;
    }

    /**
     * @param jUnitMode the jUnitMode to set
     */
    public void setjUnitMode(boolean jUnitMode) {
        this.jUnitMode = jUnitMode;
    }
}
