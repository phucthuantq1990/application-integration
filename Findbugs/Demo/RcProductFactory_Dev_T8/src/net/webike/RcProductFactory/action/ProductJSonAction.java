/************************************************************************
 * File Name	： JSonAction.java
 * Author		： Nguyen.Chuong
 * Version		： 1.0.0
 * Date Created	： 2013/09/24
 * Date Updated	： 2013/09/24
 * Description	： JSonAction for get json from ajax.
 *
 ***********************************************************************/
package net.webike.RcProductFactory.action;

import java.util.ArrayList;
import java.util.List;

import net.webike.RcProductFactory.entity.EntMstFactoryMatter;
import net.webike.RcProductFactory.entity.EntProduct;
import net.webike.RcProductFactory.entity.EntTblFactoryProductImage;
import net.webike.RcProductFactory.entity.EntTblFactoryProductModel;
import net.webike.RcProductFactory.service.MatterService;
import net.webike.RcProductFactory.service.ProductService;
import net.webike.RcProductFactory.util.Constant;

import org.apache.commons.lang.StringUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 * JSonAction for get json from ajax.
 */
@Controller
@Scope("request")
@ParentPackage(value = "json-default")
@Results(
    {
        @Result(name = "list", type = "json"),
        @Result(name = "update", type = "json"),
        @Result(name = "error", type = "redirect", location = "error.html?error=${error}"),
        // Return error page when parameter fail
        @Result(name = "errorOther", type = "redirect", location = "error.html")
    }
)
public class ProductJSonAction extends CommonAction {

    /**
	 * @serialVersionUID SerialVersion
	 */
	private static final long serialVersionUID = 1L;

    @Autowired
    private ProductService productService;

    @Autowired
    private MatterService matterService;

    private int iTotalRecords;
    private int iTotalDisplayRecords;
    private int iDisplayLength = Constant.LIMIT_OF_PRODUCT_TABLE;
    /**---------------------------------------------------------------------------
     * Get list product for productInfo page.
     */
    private List<EntProduct> listProduct;
    private int count;

    /**
     * ------------------------------------------------------------------------
     * --- Update bunrui code for productInfo page.
     */
    private Boolean updateResult = false;
    //Count total error product
    private int totalProductError = 0;
    //Count all product of matter.
    private int totalAllProduct = 0;
    //Count all finished product.
    private int totalCheckedProduct = 0;

    private List<EntTblFactoryProductImage> listImage;
    private List<EntTblFactoryProductModel> listModel;

    private String error;

    /************************************************************************
     * <b>Description:</b><br>
     *  get list product filter.
     *
     * @author      Luong.Dai
     * @date        Nov 12, 2013
     * @return      String
     ************************************************************************/
    @Action("/getListProductFilter")
    public String selectListProductFilter() {
        //MatterNo
        String matterNo = this.getRequest().getParameter(Constant.PARAM_MATTER_ID);

        // Check missing params
        if (StringUtils.isEmpty(matterNo)) {
            // Return page error params
        	listProduct = new ArrayList<EntProduct>();
            count = -1;
            return "list";
        }

        // Get matter detail by matterNo
        EntMstFactoryMatter matterDetail = matterService.selectDetailMatterByMatterNo(matterNo);
        // Check entMatter not null
        if (null == matterDetail) {
            // Error params
            listProduct = new ArrayList<EntProduct>();
            count = -1;

            return "list";
        }

        //Get entproduct param
        EntProduct entProduct = new EntProduct();
        entProduct.setMatterNo(matterNo);
        //Get filter value
        entProduct.setSysProductCode(this.getRequest().getParameter(Constant.PARAM_SYSTEM_PRODUCT_CODE));
        entProduct.setManufactoryCode(this.getRequest().getParameter(Constant.PARAM_PRODUCT_MANUFACTORY_CODE));
        entProduct.setBunruiCode(this.getRequest().getParameter(Constant.PARAM_PRODUCT_BUNRUI_CODE));
        entProduct.setProductStatusCode(this.getRequest().getParameter(Constant.PARAM_PRODUCT_STATUS_CODE));

        // Number product in 1 page
        String sPageSize = this.getRequest().getParameter(Constant.PARAM_PAGE_SIZE);
        // Page number to get product data
        String sPageNumber = this.getRequest().getParameter(Constant.PARAM_PAGE_NUMBER);

        // Check missing params pageSize and pageNumber
        if (StringUtils.isEmpty(sPageSize) || StringUtils.isEmpty(sPageNumber)) {
            // Return page error params
        	listProduct = new ArrayList<EntProduct>();
            count = -1;
            return "list";
        }

        int pageSize = Integer.parseInt(sPageSize);
        int pageNumber = Integer.parseInt(sPageNumber);

        //Get productList with filter
        listProduct = productService.selectProductListByMatterNo(entProduct, pageNumber, pageSize);

        //Get count product of matter.
        EntMstFactoryMatter entMatter = productService.countProductOfMatter(entProduct);

        // Count total product for search params
        count = entMatter.getTotalAllProduct();

        return "list";
    }

    /************************************************************************
     * <b>Description:</b><br>
     *  Update bunrui code.
     *
     * @author		Luong.Dai
     * @date		Nov 6, 2013
     * @return		String
     ************************************************************************/
    @Action("/updateBunruiCode")
    public String updateBunruiCode() {
        // Validate user
        if (!validateLoginUser()) {
            return "errorOther";
        }

        // Get params from request and set to entity
        EntProduct entProduct = createParams();

        //return to error page if entProduct is null
        if (entProduct == null) {
            // Return to error parameter page
            error = Constant.ERROR_ERROR_PARAM;
            return "error";
        }

        // Update bunrui code
        productService.updateBunruiCode(entProduct);
        // Update status for product
        productService.updateProductStatusCode(entProduct);

        // Count product error
        EntProduct entForCount = entProduct.cloneMatterNo();
        EntMstFactoryMatter entFactoryMatter = productService.countProductOfMatter(entForCount);
        //Total error product
        totalProductError = entFactoryMatter.getTotalErrorProduct();
        //Count all product of matter.
        totalAllProduct = entFactoryMatter.getTotalAllProduct();
        //Count all finished product.
        totalCheckedProduct = entFactoryMatter.getTotalCheckedProduct();

        updateResult = true;

        return "update";
    }

    /************************************************************************
     * <b>Description:</b><br>
     *  Release product.
     *
     * @author		Nguyen.Chuong
     * @date		Nov 20, 2013
     * @return		String
     ************************************************************************/
    @Action("/releaseProduct")
    public String releaseProduct() {
        //Validate user
        if (!validateLoginUser()) {
            return "errorOther";
        }
        // Get userId
        String userId = this.getCommonForm().getLoginId();
        // Get matterNo from request parameter
        String matterNo = this.getRequest().getParameter(Constant.PARAM_MATTER_ID);
        //Update matter to DB: change status of matter to released and set kousin userID, kousin date to now()
        updateResult = matterService.releaseMatter(userId, matterNo);
        return "update";
    }

    /************************************************************************
     * <b>Description:</b><br>
     *  replace your description for this method here.
     *
     * @author		Luong.Dai
     * @date		Nov 21, 2013
     * @return		String
     ************************************************************************/
    @Action("/countNumberForMatter")
    public String countNumberForMatter() {
        String matterNo = this.getRequest().getParameter(Constant.PARAM_MATTER_ID);

        // Check matterNo is empty
        if (StringUtils.isEmpty(matterNo)) {
            error = Constant.ERROR_ERROR_PARAM;

            return "list";
        }

        EntProduct entProduct = new EntProduct();
        entProduct.setMatterNo(matterNo);

        // Count total product
        EntMstFactoryMatter entFactoryMatter = productService.countProductOfMatter(entProduct);

        //Total error product
        totalProductError = entFactoryMatter.getTotalErrorProduct();
        //Count all product of matter.
        totalAllProduct = entFactoryMatter.getTotalAllProduct();
        //Count all finished product.
        totalCheckedProduct = entFactoryMatter.getTotalCheckedProduct();


        return "list";
    }

    /************************************************************************
     * <b>Description:</b><br>
     * Create parameter for update.
     *
     * @author Luong.Dai
     * @date Nov 6, 2013
     * @return EntProduct
     ************************************************************************/
    private EntProduct createParams() {
        EntProduct entProduct = new EntProduct();

        // Get userId
        String user = this.getCommonForm().getLoginId();

        // Get bunruiCode from request pramas
        String bunruiCode = this.getRequest().getParameter(Constant.PARAM_PRODUCT_BUNRUI_CODE);

        // Get productCode from request params
        String productCode = this.getRequest().getParameter(Constant.PARAM_PRODUCT_CODE);

        // Get matter_no from request params
        String matterNo = this.getRequest().getParameter(Constant.PARAM_MATTER_ID);

        //Check required field
        if (StringUtils.isEmpty(bunruiCode)
                || StringUtils.isEmpty(productCode)
                || StringUtils.isEmpty(matterNo)) {
            return null;
        }

        // Set to entProduct
        entProduct.setProductCode(productCode);
        entProduct.setBunruiCode(bunruiCode);
        entProduct.setUserName(user);
        entProduct.setMatterNo(matterNo);
        // Set product_status_code
        entProduct.setProductStatusCode(Constant.PRODUCT_CHECKED_STATUS_CODE);

        return entProduct;
    }

    @Override
    public String execute() {
        return "list";
    }

    /**
     * @return the listImage
     */
    public List<EntTblFactoryProductImage> getListImage() {
        return listImage;
    }

    /**
     * @param listImage the listImage to set
     */
    public void setListImage(List<EntTblFactoryProductImage> listImage) {
        this.listImage = listImage;
    }

    /**
     * @return the listModel
     */
    public List<EntTblFactoryProductModel> getListModel() {
        return listModel;
    }

    /**
     * @param listModel the listModel to set
     */
    public void setListModel(List<EntTblFactoryProductModel> listModel) {
        this.listModel = listModel;
    }

    /**
     * @return the iTotalRecords
     */
    public int getiTotalRecords() {
        return iTotalRecords;
    }

    /**
     * @param iTotalRecords the iTotalRecords to set
     */
    public void setiTotalRecords(int iTotalRecords) {
        this.iTotalRecords = iTotalRecords;
    }

    /**
     * @return the iTotalDisplayRecords
     */
    public int getiTotalDisplayRecords() {
        return iTotalDisplayRecords;
    }

    /**
     * @param iTotalDisplayRecords the iTotalDisplayRecords to set
     */
    public void setiTotalDisplayRecords(int iTotalDisplayRecords) {
        this.iTotalDisplayRecords = iTotalDisplayRecords;
    }

    /**
     * @return the iDisplayLength
     */
    public int getiDisplayLength() {
        return iDisplayLength;
    }

    /**
     * @param iDisplayLength the iDisplayLength to set
     */
    public void setiDisplayLength(int iDisplayLength) {
        this.iDisplayLength = iDisplayLength;
    }

    /**
     * @return the updateResult
     */
    public Boolean getUpdateResult() {
        return updateResult;
    }

    /**
     * @param updateResult
     *            Boolean
     */
    public void setUpdateResult(Boolean updateResult) {
        this.updateResult = updateResult;
    }

    /**
     * @return the totalProductError
     */
    public int getTotalProductError() {
        return totalProductError;
    }

    /**
     * @param totalProductError
     *            int
     */
    public void setTotalProductError(int totalProductError) {
       this.totalProductError = totalProductError;
    }

    /**
     * @return the totalAllProduct
     */
    public int getTotalAllProduct() {
        return totalAllProduct;
    }

    /**
     * @param totalAllProduct the totalAllProduct to set
     */
    public void setTotalAllProduct(int totalAllProduct) {
        this.totalAllProduct = totalAllProduct;
    }

    /**
     * @return the totalCheckedProduct
     */
    public int getTotalCheckedProduct() {
        return totalCheckedProduct;
    }

    /**
     * @param totalCheckedProduct the totalCheckedProduct to set
     */
    public void setTotalCheckedProduct(int totalCheckedProduct) {
        this.totalCheckedProduct = totalCheckedProduct;
    }

    /**
     * @return the listProduct
     */
    public List<EntProduct> getListProduct() {
        return listProduct;
    }

    /**
     * @param listProduct the listProduct to set
     */
    public void setListProduct(List<EntProduct> listProduct) {
        this.listProduct = listProduct;
    }

    /**
     * @return the count
     */
    public int getCount() {
        return count;
    }

    /**
     * @param count the count to set
     */
    public void setCount(int count) {
        this.count = count;
    }

    /**
     * @return the productService
     */
    public ProductService getProductService() {
        return productService;
    }

    /**
     * @param productService the productService to set
     */
    public void setProductService(ProductService productService) {
        this.productService = productService;
    }

    /**
     * @return the matterService
     */
    public MatterService getMatterService() {
        return matterService;
    }

    /**
     * @param matterService the matterService to set
     */
    public void setMatterService(MatterService matterService) {
        this.matterService = matterService;
    }

    /**
     * @return the error
     */
    public String getError() {
        return error;
    }

    /**
     * @param error the error to set
     */
    public void setError(String error) {
        this.error = error;
    }
}
