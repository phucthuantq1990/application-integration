/************************************************************************
 * File Name	： MatterDetailActionForm.java
 * Author		： Tran.Thanh
 * Version		： 1.0.0
 * Date Created	： 2013/09/24
 * Date Updated	： 2013/09/24
 * Description	： contain data matter detail page.
 *
 ***********************************************************************/
package net.webike.RcProductFactory.action.form;

import java.util.List;
import java.util.Map;

import net.webike.RcProductFactory.entity.EntCSVMode1;
import net.webike.RcProductFactory.entity.EntCSVProductFitModel;
import net.webike.RcProductFactory.entity.EntInfoCSVImport;
import net.webike.RcProductFactory.entity.EntMstAttribute;
import net.webike.RcProductFactory.entity.EntMstFactoryMatterNew;
import net.webike.RcProductFactory.entity.EntTblFactoryProductNew;
import net.webike.RcProductFactory.entity.EntTblTmpCSV;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * MainMenuActionForm.
 */
@Component
@Scope("request")
public class MatterDetailActionForm {
    //sontt
    private List<String> listHeader;
    private EntCSVMode1 entCSVMode1;
    //
    private List<EntCSVMode1> listEntCSVMode1;
	private EntMstFactoryMatterNew entMstFactoryMatterNew;
	private List<EntMstAttribute> entMstAttributeList;

    /* BOE add attribute by Luong.Dai 2014/01/18 #B07 */
	private EntTblFactoryProductNew product;
	private List<EntTblFactoryProductNew> listProduct;
	//Total record in grid
	private Integer count;
	//List registration_flg enum.
    private Map<String, String> listRegistrationFlg;
    //Message
    private String message;
    /* EOE add attribute by Luong.Dai 2014/01/18 #B07 */
    /* BOE add attribute by Luong.Dai 2014/03/11 #B08 */
    private Integer numberProduct;
    /* EOE add attribute by Luong.Dai 2014/03/11 #B08 */

    /** csv Import Status. */
    private Integer csvImportStatus;

    private List<EntCSVProductFitModel> entCSVProductFitModelList;
    private Integer entCSVProductFitModelCount;
    private EntCSVProductFitModel entCSVProductFitModel;
    /* BOE [6716] add attribute by Nguyen.Chuong 2014/02/26 */
//    //List header get from inported CSV
//    private List<String> listHeaderCSV;
//    //List product for popup CSV mode 3: attribute
//    private List<EntCSVMode3> listEntCSVMode3;
    //List all product with each attribute
    private List<EntTblTmpCSV> listImportedProduct;
    private int listImportedProductSize;
    private List<String> listCSVHeaderMode1;
    //Status to inport data from csv to temp DB table: true: success, false: fail
    private Boolean importCSVStatus;
    private String importedCSVTableName;
    private EntTblTmpCSV entTblTmpCSVFilter;
    private String importedCSVMode;
    private String splitSymbol;
    private String boundedFieldSymbol;
    /* EOE [6716] add attribute by Nguyen.Chuong 2014/02/26 */
    /* BOE [6716] hoang.ho 2014/03/04 */
    private String realFileName;
    private String csvfileName;
    private String errorCSVTableName;
    private EntInfoCSVImport outputInfoImportCSV;
    /* EOE [6716] hoang.ho 2014/03/04 */

    /*BOE Nguyen.Chuong 2014/03/14: export all page.*/
    private Boolean exportAll;
    private String productIds;
    private String attributeCodes;
    private String headerList;
    /* EOE Nguyen.Chuong 2014/03/14 */

    /*BOE Tran.Thanh 2014/04/04: Fix bug maxSize upload.*/
    private Integer maxUploadImage;
    private Integer maxUploadFolderMatter;
    /*BOE Tran.Thanh 2014/04/04: Fix bug maxSize upload.*/

    /*BOE #7905 Tran.Thanh 2014/05/16: add number for count new product in csv file */
    private Integer countNewProductInCSVFile = 0;
    /*BOE #7905 Tran.Thanh 2014/05/16: add number for count new product in csv file */

	/**
	 * @return the entMstFactoryMatterNew
	 */
	public EntMstFactoryMatterNew getEntMstFactoryMatterNew() {
		return entMstFactoryMatterNew;
	}

	/**
	 * @param entMstFactoryMatterNew the entMstFactoryMatterNew to set
	 */
	public void setEntMstFactoryMatterNew(EntMstFactoryMatterNew entMstFactoryMatterNew) {
		this.entMstFactoryMatterNew = entMstFactoryMatterNew;
	}

    /**
     * @return the listProduct
     */
    public List<EntTblFactoryProductNew> getListProduct() {
        return listProduct;
    }

    /**
     * @param listProduct the listProduct to set
     */
    public void setListProduct(List<EntTblFactoryProductNew> listProduct) {
        this.listProduct = listProduct;
    }

    /**
     * @return the product
     */
    public EntTblFactoryProductNew getProduct() {
        return product;
    }

    /**
     * @param product the product to set
     */
    public void setProduct(EntTblFactoryProductNew product) {
        this.product = product;
    }

    /**
     * @return the count
     */
    public Integer getCount() {
        return count;
    }

    /**
     * @param count the count to set
     */
    public void setCount(Integer count) {
        this.count = count;
    }

    /**
     * @return the listRegistrationFlg
     */
    public Map<String, String> getListRegistrationFlg() {
        return listRegistrationFlg;
    }

    /**
     * @param listRegistrationFlg the listRegistrationFlg to set
     */
    public void setListRegistrationFlg(Map<String, String> listRegistrationFlg) {
        this.listRegistrationFlg = listRegistrationFlg;
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message the message to set
     */
    public void setMessage(String message) {
        this.message = message;
    }

    public List<EntMstAttribute> getEntMstAttributeList() {
        return entMstAttributeList;
    }

    public void setEntMstAttributeList(List<EntMstAttribute> entMstAttributeList) {
        this.entMstAttributeList = entMstAttributeList;
    }

    /**
     * @return the importCSVStatus
     */
    public Boolean getImportCSVStatus() {
        return importCSVStatus;
    }

    /**
     * @param importCSVStatus the importCSVStatus to set
     */
    public void setImportCSVStatus(Boolean importCSVStatus) {
        this.importCSVStatus = importCSVStatus;
    }

    /**
     * @return the entCSVProductFitModelList
     */
    public List<EntCSVProductFitModel> getEntCSVProductFitModelList() {
        return entCSVProductFitModelList;
    }

    /**
     * @param entCSVProductFitModelList the entCSVProductFitModelList to set
     */
    public void setEntCSVProductFitModelList(List<EntCSVProductFitModel> entCSVProductFitModelList) {
        this.entCSVProductFitModelList = entCSVProductFitModelList;
    }

    /**
     * @return the entCSVProductFitModel
     */
    public EntCSVProductFitModel getEntCSVProductFitModel() {
        return entCSVProductFitModel;
    }

    /**
     * @param entCSVProductFitModel the entCSVProductFitModel to set
     */
    public void setEntCSVProductFitModel(EntCSVProductFitModel entCSVProductFitModel) {
        this.entCSVProductFitModel = entCSVProductFitModel;
    }

    /**
     * @return the entCSVProductFitModelCount
     */
    public Integer getEntCSVProductFitModelCount() {
        return entCSVProductFitModelCount;
    }

    /**
     * @param entCSVProductFitModelCount the entCSVProductFitModelCount to set
     */
    public void setEntCSVProductFitModelCount(Integer entCSVProductFitModelCount) {
        this.entCSVProductFitModelCount = entCSVProductFitModelCount;
    }


    public List<EntCSVMode1> getListEntCSVMode1() {
        return listEntCSVMode1;
    }

    public void setListEntCSVMode1(List<EntCSVMode1> listEntCSVMode1) {
        this.listEntCSVMode1 = listEntCSVMode1;
    }

    /**
     * @return the importedCSVTableName
     */
    public String getImportedCSVTableName() {
        return importedCSVTableName;
    }

    /**
     * @param importedCSVTableName the importedCSVTableName to set
     */
    public void setImportedCSVTableName(String importedCSVTableName) {
        this.importedCSVTableName = importedCSVTableName;
    }

    /**
     * @return the csvImportStatus
     */
    public Integer getCsvImportStatus() {
        return csvImportStatus;
    }

    /**
     * @param csvImportStatus the csvImportStatus to set
     */
    public void setCsvImportStatus(Integer csvImportStatus) {
        this.csvImportStatus = csvImportStatus;
    }
    public List<String> getListHeader() {
        return listHeader;
    }

    public void setListHeader(List<String> listHeader) {
        this.listHeader = listHeader;
    }

    /**
     * @return the listImportedProduct
     */
    public List<EntTblTmpCSV> getListImportedProduct() {
        return listImportedProduct;
    }

    /**
     * @param listImportedProduct the listImportedProduct to set
     */
    public void setListImportedProduct(List<EntTblTmpCSV> listImportedProduct) {
        this.listImportedProduct = listImportedProduct;
    }

    /**
     * @return the listImportedProductSize
     */
    public int getListImportedProductSize() {
        return listImportedProductSize;
    }

    /**
     * @param listImportedProductSize the listImportedProductSize to set
     */
    public void setListImportedProductSize(int listImportedProductSize) {
        this.listImportedProductSize = listImportedProductSize;
    }

    /**
     * @return the entTblTmpCSVFilter
     */
    public EntTblTmpCSV getEntTblTmpCSVFilter() {
        return entTblTmpCSVFilter;
    }

    /**
     * @param entTblTmpCSVFilter the entTblTmpCSVFilter to set
     */
    public void setEntTblTmpCSVFilter(EntTblTmpCSV entTblTmpCSVFilter) {
        this.entTblTmpCSVFilter = entTblTmpCSVFilter;
    }
    /**
     * @return the realFileName
     */
    public String getRealFileName() {
        return realFileName;
    }

    /**
     * @param realFileName the realFileName to set
     */
    public void setRealFileName(String realFileName) {
        this.realFileName = realFileName;
    }

    /**
     * @return the csvfileName
     */
    public String getCsvfileName() {
        return csvfileName;
    }

    /**
     * @param csvfileName the csvfileName to set
     */
    public void setCsvfileName(String csvfileName) {
        this.csvfileName = csvfileName;
    }

    /**
     * @return the errorCSVTableName
     */
    public String getErrorCSVTableName() {
        return errorCSVTableName;
    }

    /**
     * @param errorCSVTableName the errorCSVTableName to set
     */
    public void setErrorCSVTableName(String errorCSVTableName) {
        this.errorCSVTableName = errorCSVTableName;
    }
    /**
     * @return the outputInfoImportCSV
     */
    public EntInfoCSVImport getOutputInfoImportCSV() {
        return outputInfoImportCSV;
    }

    /**
     * @param outputInfoImportCSV the outputInfoImportCSV to set
     */
    public void setOutputInfoImportCSV(EntInfoCSVImport outputInfoImportCSV) {
        this.outputInfoImportCSV = outputInfoImportCSV;
    }

    /**
     * @return the importedCSVMode
     */
    public String getImportedCSVMode() {
        return importedCSVMode;
    }

    /**
     * @param importedCSVMode the importedCSVMode to set
     */
    public void setImportedCSVMode(String importedCSVMode) {
        this.importedCSVMode = importedCSVMode;
    }

    public EntCSVMode1 getEntCSVMode1() {
        return entCSVMode1;
    }

    public void setEntCSVMode1(EntCSVMode1 entCSVMode1) {
        this.entCSVMode1 = entCSVMode1;
    }

    /**
     * @return the listCSVHeaderMode1
     */
    public List<String> getListCSVHeaderMode1() {
        return listCSVHeaderMode1;
    }

    /**
     * @param listCSVHeaderMode1 the listCSVHeaderMode1 to set
     */
    public void setListCSVHeaderMode1(List<String> listCSVHeaderMode1) {
        this.listCSVHeaderMode1 = listCSVHeaderMode1;
    }

    /**
     * @return the splitSymbol
     */
    public String getSplitSymbol() {
        return splitSymbol;
    }

    /**
     * @param splitSymbol the splitSymbol to set
     */
    public void setSplitSymbol(String splitSymbol) {
        this.splitSymbol = splitSymbol;
    }

    /**
     * @return the boundedFieldSymbol
     */
    public String getBoundedFieldSymbol() {
        return boundedFieldSymbol;
    }

    /**
     * @param boundedFieldSymbol the boundedFieldSymbol to set
     */
    public void setBoundedFieldSymbol(String boundedFieldSymbol) {
        this.boundedFieldSymbol = boundedFieldSymbol;
    }

    /**
     * @return the numberProduct
     */
    public Integer getNumberProduct() {
        return numberProduct;
    }

    /**
     * @param numberProduct the numberProduct to set
     */
    public void setNumberProduct(Integer numberProduct) {
        this.numberProduct = numberProduct;
    }

    /**
     * @return the exportAll
     */
    public Boolean getExportAll() {
        return exportAll;
    }

    /**
     * @param exportAll the exportAll to set
     */
    public void setExportAll(Boolean exportAll) {
        this.exportAll = exportAll;
    }

    /**
     * @return the productIds
     */
    public String getProductIds() {
        return productIds;
    }

    /**
     * @param productIds the productIds to set
     */
    public void setProductIds(String productIds) {
        this.productIds = productIds;
    }

    /**
     * @return the attributeCodes
     */
    public String getAttributeCodes() {
        return attributeCodes;
    }

    /**
     * @param attributeCodes the attributeCodes to set
     */
    public void setAttributeCodes(String attributeCodes) {
        this.attributeCodes = attributeCodes;
    }

    /**
     * @return the headerList
     */
    public String getHeaderList() {
        return headerList;
    }

    /**
     * @param headerList the headerList to set
     */
    public void setHeaderList(String headerList) {
        this.headerList = headerList;
    }

	/**
	 * @return the maxUploadImage
	 */
	public Integer getMaxUploadImage() {
		return maxUploadImage;
	}

	/**
	 * @param maxUploadImage the maxUploadImage to set
	 */
	public void setMaxUploadImage(Integer maxUploadImage) {
		this.maxUploadImage = maxUploadImage;
	}

	/**
	 * @return the maxUploadFolderMatter
	 */
	public Integer getMaxUploadFolderMatter() {
		return maxUploadFolderMatter;
	}

	/**
	 * @param maxUploadFolderMatter the maxUploadFolderMatter to set
	 */
	public void setMaxUploadFolderMatter(Integer maxUploadFolderMatter) {
		this.maxUploadFolderMatter = maxUploadFolderMatter;
	}

	/**
	 * @return the countNewProductInCSVFile
	 */
	public Integer getCountNewProductInCSVFile() {
		return countNewProductInCSVFile;
	}

	/**
	 * @param countNewProductInCSVFile the countNewProductInCSVFile to set
	 */
	public void setCountNewProductInCSVFile(Integer countNewProductInCSVFile) {
		this.countNewProductInCSVFile = countNewProductInCSVFile;
	}

}
