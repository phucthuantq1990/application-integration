/************************************************************************
 * File Name	： MainMenuActionForm.java
 * Author		： Nguyen.Chuong
 * Version		： 1.0.0
 * Date Created	： 2013/09/24
 * Date Updated	： 2013/09/24
 * Description	： contain data main menu page.
 *
 ***********************************************************************/
package net.webike.RcProductFactory.action.form;

import java.util.List;

import net.webike.RcProductFactory.entity.EntMstFactoryMatter;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * MainMenuActionForm.
 */
@Component
@Scope("session")
public class MainMenuActionForm {

    /**
     * message error.
     */
    private String message;

    /**
     * EntMstFactoryMatter.
     */
    private List<EntMstFactoryMatter> listMatter;

    /**
     * @return message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message セットする message
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * @return the listMatter
     */
    public List<EntMstFactoryMatter> getListMatter() {
        return listMatter;
    }

    /**
     * @param listMatter the listMatter to set
     */
    public void setListMatter(List<EntMstFactoryMatter> listMatter) {
        this.listMatter = listMatter;
    }
}
