/************************************************************************
 * File Name        ：  NewMatterAction.java
 * Author           ： Luong.Dai
 * Version          ： 1.0.0
 * Date Created     ： 2013/11/06
 * Date Updated     ： 2013/11/07
 * Description      ： New matter in database
 *
 ***********************************************************************/

package net.webike.RcProductFactory.action;

import java.util.List;

import net.webike.RcProductFactory.action.form.NewMasterActionForm;
import net.webike.RcProductFactory.entity.EntMstFactoryMatter;
import net.webike.RcProductFactory.entity.EntMstFactoryUserTmp;
import net.webike.RcProductFactory.service.MatterService;
import net.webike.RcProductFactory.service.UserTmpService;
//import net.webike.RcProductFactory.service.UserService;
import net.webike.RcProductFactory.util.Constant;

import org.apache.commons.lang.StringUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 * NewMatterAction Class.
 */
@Controller
@Scope("request")
@ParentPackage(value = "CommonInterceptor")
@Results({
        // On loading default newUser page
        @Result(name = "success", location = "newMatter.jsp"),
        //Insert success
        @Result(name = "insertSuccess", type = "redirect", location = "mainMenu.html"),
        //Error page.
        @Result(name = "errorInvalidParam", type = "redirect", location = "error.html?error=param"),
        @Result(name = "errorOther", type = "redirect", location = "error.html") })
public class NewMatterAction extends CommonAction {
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Autowired
    private NewMasterActionForm form;

    @Autowired
    private UserTmpService userTmpService;

    @Autowired
    private MatterService matterService;

    @Override
    @Action("/newMatter")
    public String execute() throws Exception {
        // Validate loginUser
        if (!super.validateLoginUser()) {
            return "errorOther";
        }

        // Get list user
        //Thanh - start old code
        List<EntMstFactoryUserTmp> lstUser = userTmpService.selectAllUser();
        //Thanh - End old Code

        //form.setListAdminTreeMap(convertEntToTree(lstUser));
        this.userNameValueOfListEnt(lstUser);
        form.setListAdmin(lstUser);

        return "success";
    }

    /************************************************************************
     * <b>Description:</b><br>
     *  Add new matter to database.
     *
     * @author		Luong.Dai
     * @date		Nov 7, 2013
     * @return      String
     * @throws Exception		String
     ************************************************************************/
    @Action("/addNewMatter")
    public String addNewMatter() throws Exception {
        try {
            // Validate loginUser
            if (!super.validateLoginUser()) {
                return "errorOther";
            }

            // Check null or empty params matterName and changeUser
            // If params is null, return to error page
            if (StringUtils.isEmpty(this.getRequest().getParameter("matterName").trim())
                    || StringUtils.isEmpty(this.getRequest().getParameter("changeUser").trim())) {
                return "errorInvalidParam";
            }

            // Get params and set value into entFactoryMatter entity
            EntMstFactoryMatter entFactoryMatter = createInsertParams();

            // Insert value to database
            matterService.insertNewMatter(entFactoryMatter);
        } catch (Exception e) {
            //Catch error insert false.
            this.getLogger().error("[NewMatterAction] An error to insert new matter at"
                                    + "addNewMatter() function");
            this.getLogger().error(e.toString());
            e.printStackTrace();
            return "insertSuccess";
        }
        return "insertSuccess";
    }

    /************************************************************************
     * <b>Description:</b><br>
     *  Get params from request and set value to entity.
     *
     * @author		Luong.Dai
     * @date		Nov 7, 2013
     * @return		EntMstFactoryMatter
     ************************************************************************/
    private EntMstFactoryMatter createInsertParams() {
        // Get user name login
        String user = this.getCommonForm().getLoginId();

        // get max matter_no
        int nextMatterNo = matterService.selectMaxMatterNo();

        // Set value to entity
        EntMstFactoryMatter factoryMatter = new EntMstFactoryMatter();
        factoryMatter.setMatterNo(nextMatterNo + 1);
        factoryMatter.setMatterName(this.getRequest().getParameter("matterName"));
        factoryMatter.setMatterKindCode(Constant.MATTER_KIND_CODE);
        factoryMatter.setMatterStatusCode(Constant.MATTER_STATUS_CODE);
        factoryMatter.setMatterChargeUserId(this.getRequest().getParameter("changeUser"));
        factoryMatter.setMatterMessage(Constant.MATTER_MESSAGE);
        factoryMatter.setDelFlg(Constant.DEL_FLG);
        factoryMatter.setTourokuUserId(user);
        factoryMatter.setKousinUserId(user);

        return factoryMatter;
    }

    /************************************************************************
     * <b>Description:</b><br>
     * combine last name and first name to create userName. Format:lastName" "firstName
     *
     * @author		Luong.Dai
     * @date		Nov 15, 2013
     * @param listUser List<EntMstFactoryUser>
     ************************************************************************/
    private void userNameValueOfListEnt(List<EntMstFactoryUserTmp> listUser) {
        int size = listUser.size();

        for (int i = 0; i < size; i++) {
            EntMstFactoryUserTmp entUser = listUser.get(i);

            // userName = [last name][1 space 1byte][first name]
            entUser.setUserName(entUser.getUserLastName() + " " + entUser.getUserFirstName());
        }
    }

    /**
     * @return the form
     */
    public NewMasterActionForm getForm() {
        return form;
    }

    /**
     * @param form the form to set
     *
     */
    public void setForm(NewMasterActionForm form) {
        this.form = form;
    }
}
