/************************************************************************
 * File Name	： CalibrationService.java
 * Author		： Nguyen.Chuong
 * Version		： 1.0.0
 * Date Created	： 2014/07/17
 * Description	： Service to process logic flow of calibration object.
 *
 ***********************************************************************/
package net.webike.RcProductFactory.service;

import net.webike.RcProductFactory.entity.EntTblFactoryCalibrationRequestIndividual;
import net.webike.RcProductFactory.entity.EntTblFactoryCalibrationRequestWhole;

/**
 * Service to process logic flow of CalibrationService object.
 */
public interface CalibrationService {

    /************************************************************************
     * <b>Description:</b><br>
     *  Get calibrasion request whole value data and whole memo data of matter.
     *
     * @author		Nguyen.Chuong
     * @date		Jul 17, 2014
     * @param       matterNo Long
     * @return		EntTblFactoryCalibrationRequestWhole
     ************************************************************************/
    EntTblFactoryCalibrationRequestWhole selectCalibrationRequestWhole(Long matterNo);

    /************************************************************************
     * <b>Description:</b><br>
     *  save tab all value of popup.
     *
     * @author		Nguyen.Chuong
     * @date		Jul 17, 2014
     * @param       ent EntTblFactoryCalibrationRequestWhole of tab 1 content matterNo, value, user update id
     * @param       tabNo integer to show which tab is save:<br>
     * + 1: tab all.<br>
     * + 3: tab note.
     * @return EntTblFactoryCalibrationRequestWhole content insert time.
     ************************************************************************/
    EntTblFactoryCalibrationRequestWhole saveTabAllPopup(EntTblFactoryCalibrationRequestWhole ent, int tabNo);

    /************************************************************************
     * <b>Description:</b><br>
     *  insert new calibration request individal into DB rc_product_factory.tbl_factory_calibration_request_individual.
     *
     * @author      Nguyen.Chuong
     * @date        Jul 21, 2014
     * @param       ent EntTblFactoryCalibrationRequestWhole content data.
     * @return      Long = id of new insert individual.
     ************************************************************************/
    Long insertNewCalibrationRequestIndividual(EntTblFactoryCalibrationRequestIndividual ent);
}
