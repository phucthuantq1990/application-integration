/************************************************************************
 * File Name	： ProductManagementService.java
 * Author		： Nguyen.Chuong
 * Version		： 1.0.0
 * Date Created	： 2014/02/14
 * Date Updated	： 2014/02/14
 * Description	： Service to get product data from database for productManagement page..
 *
 ***********************************************************************/
package net.webike.RcProductFactory.service;

import java.sql.SQLException;
import java.util.List;

import net.webike.RcProductFactory.entity.EntCSVMode1;
import net.webike.RcProductFactory.entity.EntCSVMode3;
import net.webike.RcProductFactory.entity.EntMstAttribute;
import net.webike.RcProductFactory.entity.EntMstAttributeGroup;
import net.webike.RcProductFactory.entity.EntMstFactoryMatter;
import net.webike.RcProductFactory.entity.EntMstNouki;
import net.webike.RcProductFactory.entity.EntMstProduct;
import net.webike.RcProductFactory.entity.EntMstSiire;
import net.webike.RcProductFactory.entity.EntTblFactoryImportCondition;
import net.webike.RcProductFactory.entity.EntTblFactoryProductNew;
import net.webike.RcProductFactory.entity.EntTblProductGuestInput;
import net.webike.RcProductFactory.entity.EntTblProductLink;
import net.webike.RcProductFactory.entity.EntTblProductVideo;
import net.webike.RcProductFactory.entity.EntTblSyouhinFitModel;
import net.webike.RcProductFactory.entity.EntTblSyouhinImage;

/**
 * Service to get Product data from database.
 */
public interface ProductManagementService {

    /************************************************************************
     * <b>Description:</b><br>
     * load product manage list for grid, paging, sort and filter.
     *
     * @return      List<EntMstProduct>
     * @param       limit page number
     * @param       offset number data in 1 page
     * @param       product product object condition
     * @author      Long Vu
     * @date        Feb 15, 2014
     ************************************************************************/
    List<EntMstProduct> selectProductList(int limit, int offset, EntMstProduct product);

    /************************************************************************
     * <b>Description:</b><br>
     * get total record of product manage list.
     *
     * @return      List<EntMstProduct>
     * @param       product product object condition
     * @author      Long Vu
     * @date        Feb 15, 2014
     ************************************************************************/
    int selectTotalRecordOfGrid(EntMstProduct product);

    /************************************************************************
     * <b>Description:</b><br>
     *  Select data for load init data of productManagement popup review by syouhin_sys_code.
     *
     * @author      Nguyen.Chuong
     * @date        Feb 15, 2014
     * @param       syouhinSysCode long
     * @return      EntMstProduct data from db
     ************************************************************************/
    EntMstProduct selectProductDataForProductReviewPopupBySyouhinCode(long syouhinSysCode);

    /************************************************************************
     * <b>Description:</b><br>
     *  get list data siire and nouki by syouhin_sys_code.
     *
     * @author		Nguyen.Chuong
     * @date		Feb 15, 2014
     * @param       syouhinSysCode Long
     * @return		List<EntMstSiire> list siire and nouki
     ************************************************************************/
    List<EntMstSiire> selectListSiireNoukiBySyouhinSysCode(Long syouhinSysCode);

    /************************************************************************
     * <b>Description:</b><br>
     *  select list image by syouhinSysCode from tbl_syouhin_image in rc_syouhin.
     *
     * @author      Nguyen.Chuong
     * @date        Feb 15, 2014
     * @param       syouhinSysCode Long
     * @return      List<EntTblSyouhinImage>
     ************************************************************************/
    List<EntTblSyouhinImage> selectListImageBySyouhinSysCode(Long syouhinSysCode);

    /************************************************************************
     * <b>Description:</b><br>
     *  select list syouhinFitModel by syouhinSysCode.
     * @author      Long Vu
     * @date        Feb 12, 2014
     * @param       syouhinSysCode Long
     * @return      List<EntTblSyouhinFitModel>
     ************************************************************************/
    List<EntTblSyouhinFitModel> selectListSyouhinFitModelBySyouhinSysCode(Long syouhinSysCode);

    /************************************************************************
     * <b>Description:</b><br>
     *  Select all attribute type of product by syouhinSysCode.
     *  Select from db:
     *  - tbl_product_attribute_flag
     *  - tbl_product_attribute_float
     *  - tbl_product_attribute_group
     *  - tbl_product_attribute_integer
     *  - tbl_product_attribute_string
     *
     * @author		Nguyen.Chuong
     * @date		Feb 17, 2014
     * @param       syouhinSysCode Long
     * @return		List<EntMstAttribute>
     ************************************************************************/
    List<EntMstAttribute> selectListAttributeAllTypeBySyouhinSysCode(Long syouhinSysCode);

    /************************************************************************
     * <b>Description:</b><br>
     *  select list video link from rc_syouhin.tbl_product_video by syouhinSysCode.
     *
     * @author      Nguyen.Chuong
     * @date        Feb 17, 2014
     * @param       syouhinSysCode Long
     * @return      List<EntTblProductVideo>
     ************************************************************************/
    List<EntTblProductVideo> selectLinkVideoBySyouhinSysCode(Long syouhinSysCode);

    /************************************************************************
     * <b>Description:</b><br>
     * load matter by matter no.
     *
     * @return      String
     * @param       matterNo matter no
     * @author      Long Vu
     * @date        Feb 17, 2014
     ************************************************************************/
    EntMstFactoryMatter selectMatterByMatterNo(long matterNo);

    /************************************************************************
     * <b>Description:</b><br>
     * update product del fgl.
     *
     * @return      String
     * @param       prdList product list
     * @param       entMstProduct delele flag
     * @author      Long Vu
     * @date        Feb 17, 2014
     ************************************************************************/
    boolean updateProductDelFlg(List<Long> prdList, EntMstProduct entMstProduct);

    /************************************************************************
     * <b>Description:</b><br>
     * update product supplier status code.
     *
     * @return      String
     * @param       prdIdList product id list
     * @param       entMstProduct product
     * @author      Long Vu
     * @date        Feb 17, 2014
     ************************************************************************/
    boolean updateProductSupplierStatusCode(List<Long> prdIdList, EntMstProduct entMstProduct);

    /************************************************************************
     * <b>Description:</b><br>
     * get all full name of user in DB.
     *
     * @return      List<String>
     * @author      Long Vu
     * @date        Feb 18, 2014
     ************************************************************************/
    List<String> selectAllFullname();

    /**
     * Select nouki list.
     * @author      nguyen.hieu
     * @date		2014-02-17
     * @return		Nouki list.
     * @throws SQLException SQLException.
     */
    List<EntMstNouki> selectNoukiList() throws SQLException;

    /**
     * Select attribute list.
     * @author      nguyen.hieu
     * @since		2014-02-17
     * @return		Attribute list.
     * @throws SQLException SQLException.
     */
    List<EntMstAttribute> selectAttributeList() throws SQLException;

    /************************************************************************
     * <b>Description:</b><br>
     * get product status name.
     *
     * @return      List<String>
     * @author      Long Vu
     * @date        Feb 18, 2014
     ************************************************************************/
    List<String> selectAllProductStatusName();

    /************************************************************************
     * <b>Description:</b><br>
     *  select link reason by syouhin_sys_code for rc_syouhin.tbl_product_link.
     *
     * @author      Nguyen.Chuong
     * @date        Feb 19, 2014
     * @param       syouhinSysCode Long
     * @return      List<EntTblFactoryProductLink>
     ************************************************************************/
    List<EntTblProductLink> selectLinkReasonBySyouhinSysCode(Long syouhinSysCode);

    /************************************************************************
     * <b>Description:</b><br>
     *  Select list productGuestInput by syouhin_sys_code.
     *
     * @author      Luong.Dai
     * @date        Feb 19, 2014
     * @param       syouhinSysCode Long
     * @return      List<EntTblProductGuestInput>
     ************************************************************************/
    List<EntTblProductGuestInput> selectListProductGuestInputBySyouhinSysCode(Long syouhinSysCode);

	/**
	 * Get attributeGroup list by attribute code.
     * @author      nguyen.hieu
     * @date        2014-02-19
	 * @param		attributeCode Attribute code.
	 * @return		Attribute group list.
	 * @throws		SQLException SQLException
	 */
    List<EntMstAttributeGroup> selectAttrGroupListByAttributeCode(int attributeCode) throws SQLException;

    /************************************************************************
     * <b>Description:</b><br>
     * get import conditon list by matter no.
     *
     * @return      List<EntTblFactoryImportCondition>
     * @param       matterNo matter no
     * @author      Long Vu
     * @date        Feb 21, 2014
     ************************************************************************/
    List<EntTblFactoryImportCondition> selectImportConditionListByMatterNo(long matterNo);

    /************************************************************************
     * <b>Description:</b><br>
     * get product brand code and product group code list.
     *
     * @author      Long Vu
     * @param       prdIdList product id list
     * @return      List<EntMstProduct>
     * @date        Feb 21, 2014
     ************************************************************************/
    List<EntMstProduct> selectProductBrdCodeAndGrpCodeListWithoutGroup0(List<Long> prdIdList);

    /************************************************************************
     * <b>Description:</b><br>
     * filter only syouhin_sys_code group 0 by list syouhin_sys_code.
     *
     * @author      Nguyen.Chuong
     * @param       prdIdList product id list
     * @return      List<EntMstProduct>
     * @date        2014/03/17
     ************************************************************************/
    List<EntMstProduct> selectSyouhinSysCodeOnlyGroup0ByListSyouhin(List<Long> prdIdList);

    /************************************************************************
     * <b>Description:</b><br>
     * get product list for updating data.
     *
     * @author      Long Vu
     * @param       productList product list
     * @return      List<EntMstProduct>
     * @date        Feb 21, 2014
     ************************************************************************/
    List<EntMstProduct> selectProductListByBrdCodeAndGrpCode(List<EntMstProduct> productList);
    /**
     * Select detail search product list.
     * @author		nguyen.hieu
     * @date		2014-02-21
     * @param		limit Limit.
     * @param		offset Offset.
     * @param		product Product
     * @return		Detail search product list.
     * @throws		SQLException SQLException.
     */
    List<EntMstProduct> selectDetailSearchProductList(int limit, int offset, EntMstProduct product) throws SQLException;

    /**
     * Select detail search product list.
     * @author		nguyen.hieu
     * @date		2014-02-21
     * @param		product Product
     * @return		Detail search product list count.
     * @throws		SQLException SQLException.
     */
    int selectDetailSearchProductListCount(EntMstProduct product) throws SQLException;

    /************************************************************************
     * <b>Description:</b><br>
     * process data select in grid then copy data to another DB.
     *
     * @return      boolean
     * @param       user user login id
     * @param       matterNo matter number
     * @param       prdList product list
     * @param       entMstFactoryMatter matter object
     * @param       entTblFactoryImportCondition import conditin object
     * @author      Long Vu
     * @date        Feb 20, 2014
     ************************************************************************/
    boolean processUpdateMaintenanceProduct(String user, long matterNo, List<EntMstProduct> prdList, EntMstFactoryMatter entMstFactoryMatter,
    EntTblFactoryImportCondition entTblFactoryImportCondition);

    /************************************************************************
     * <b>Description:</b><br>
     * Select syouhin fit model by syouhin sys code list.
     *
     * @author      Doan.Chuong
     * @param       syouhinSysCodeList List<String>
     * @return      List<EntCSVMode3>
     * @date        March 3, 2014
     ************************************************************************/
    List<EntCSVMode3> selectSyouhinFitModelBySyouhinSysCodeList(List<String> syouhinSysCodeList);

    /************************************************************************
     * <b>Description:</b><br>
     *  Select syouhin and all of its attribute.
     *
     * @author      Doan.Chuong
     * @date        March 3, 2014
     * @param       syouhinSysCodeList List<String>
     * @param       attributeCodeList List<String>
     * @return      List<EntCSVMode3>
     ************************************************************************/
    List<EntCSVMode3> selectProductAttributeBySyouhinSysCodeList(List<String> syouhinSysCodeList, List<String> attributeCodeList);

    /************************************************************************
     * <b>Description:</b><br>
     *  Select product's info for export CSV mode 1 (tables with 1-1 mapping).
     *
     * @author      Doan.Chuong
     * @date        March 3, 2014
     * @param       syouhinSysCodeList List<String>
     * @return      List<EntCSVMode1>
     ************************************************************************/
    List<EntCSVMode1> selectSyouhinInfoExportCSVMode1SingleTable(List<String> syouhinSysCodeList);

    /************************************************************************
     * <b>Description:</b><br>
     *  Select attributes by list of code.
     *
     * @author      Doan.Chuong
     * @date        March 3, 2014
     * @param       attributeCodeList List<String>
     * @return      EntMstAttribute
     ************************************************************************/
    List<EntMstAttribute> selectAttributeByCodes(List<String> attributeCodeList);

    /************************************************************************
     * <b>Description:</b><br>
     *  Set attribute code to list data.
     *
     * @author      Doan.Chuong
     * @date        March 3, 2014
     * @param       data List<EntCSVMode1>
     * @param       attributeList List<EntMstAttribute>
     ************************************************************************/
    void appendAttributeCode(List<EntCSVMode1> data, List<EntMstAttribute> attributeList);

    /************************************************************************
     * <b>Description:</b><br>
     *  Set attribute data to list data.
     *
     * @author      Doan.Chuong
     * @date        March 3, 2014
     * @param       data List<EntCSVMode1>
     * @param       attributeDataList List<EntCSVMode1>
     ************************************************************************/
    void appendAttributeData(List<EntCSVMode1> data, List<EntCSVMode3> attributeDataList);

    /************************************************************************
     * <b>Description:</b><br>
     *  Select product fit model for export CSV Mode 1.
     *
     * @author      Doan.Chuong
     * @date        March 3, 2014
     * @param       syouhinSysCodeList List<String>
     * @return      List<EntCSVMode1>
     ************************************************************************/
    List<EntCSVMode1> selectProductFitModelBySyouhinSysCodeList(List<String> syouhinSysCodeList);

    /************************************************************************
     * <b>Description:</b><br>
     *  Select product image info for export CSV Mode 1.
     *
     * @author      Doan.Chuong
     * @date        March 3, 2014
     * @param       syouhinSysCodeList List<String>
     * @return      List<EntCSVMode1>
     ************************************************************************/
    List<EntCSVMode1> selectProductImageInfoBySyouhinSysCodeList(List<String> syouhinSysCodeList);

    /************************************************************************
     * <b>Description:</b><br>
     *  Select product select info for export CSV Mode 1.
     *
     * @author      Doan.Chuong
     * @date        March 3, 2014
     * @param       syouhinSysCodeList List<String>
     * @return      List<EntCSVMode1>
     ************************************************************************/
    List<EntCSVMode1> selectProductSelectInfoBySyouhinSysCodeList(List<String> syouhinSysCodeList);

    /************************************************************************
     * <b>Description:</b><br>
     *  Select product guest input for export CSV Mode 1.
     *
     * @author      Doan.Chuong
     * @date        March 3, 2014
     * @param       syouhinSysCodeList List<String>
     * @return      List<EntCSVMode1>
     ************************************************************************/
    List<EntCSVMode1> selectProductGuestInputInfoBySyouhinSysCodeList(List<String> syouhinSysCodeList);

    /************************************************************************
     * <b>Description:</b><br>
     *  Select product link info for export CSV Mode 1.
     *
     * @author      Doan.Chuong
     * @date        March 3, 2014
     * @param       syouhinSysCodeList List<String>
     * @return      List<EntCSVMode1>
     ************************************************************************/
    List<EntCSVMode1> selectProductLinkInfoBySyouhinSysCodeList(List<String> syouhinSysCodeList);

    /************************************************************************
     * <b>Description:</b><br>
     *  Select product video info for export CSV Mode 1.
     *
     * @author      Doan.Chuong
     * @date        March 3, 2014
     * @param       syouhinSysCodeList List<String>
     * @return      List<EntCSVMode1>
     ************************************************************************/
    List<EntCSVMode1> selectProductVideoInfoBySyouhinSysCodeList(List<String> syouhinSysCodeList);

    /************************************************************************
     * <b>Description:</b><br>
     *  Append data of one field from subList to mainList by product id.
     *
     * @author      Doan.Chuong
     * @date        March 3, 2014
     * @param       mainList List<EntCSVMode1>
     * @param       subList List<EntCSVMode1>
     * @param       fieldName String
     ************************************************************************/
    void appendDataByFieldName(List<EntCSVMode1> mainList, List<EntCSVMode1> subList, String fieldName);

    /************************************************************************
     * <b>Description:</b><br>
     *  Append data image or select info from subList to mainList by product id.
     *
     * @author      Doan.Chuong
     * @date        March 3, 2014
     * @param       mainList List<EntCSVMode1>
     * @param       subList List<EntCSVMode1>
     * @param       fieldName String
     ************************************************************************/
    void appendImageSelectData(List<EntCSVMode1> mainList, List<EntCSVMode1> subList, String fieldName);

    /************************************************************************
     * <b>Description:</b><br>
     * select product list.
     *
     * @author      Long Vu
     * @date        March 3, 2014
     * @param       prdIdList List<prdIdList>
     * @return      List<EntMstProduct>
     ************************************************************************/
    List<EntMstProduct> selectproductListByPrdIdList(List<Long> prdIdList);

    /************************************************************************
     * <b>Description:</b><br>
     *  select count list product syouhin_sys_code by list product syouhin_sys_code with maintenance_flg = 0.
     *
     * @author      Nguyen.Chuong
     * @date        Mar 25, 2014
     * @param       prdIdList list<String>
     * @return      int
     ************************************************************************/
    int selectCountProductSyouhinNotMaintenacedByPrdIdList(List<Long> prdIdList);
    /************************************************************************
     * <b>Description:</b><br>
     *  select list product syouhin_sys_code by list product syouhin_sys_code with maintenance_flg = 0.
     *
     * @author      Nguyen.Chuong
     * @date        Mar 19, 2014
     * @param       prdIdList List<Long>
     * @return      List<Long>
     ************************************************************************/
    List<Long> selectproductSyouhinNotMaintenacedByPrdIdList(List<Long> prdIdList);

    /************************************************************************
     * <b>Description:</b><br>
     *  count list product syouhin_sys_code disabled(product_del_flg = 1).
     *
     * @author      Nguyen.Chuong
     * @date        Apr 4, 2014
     * @param       prdIdList List<Long>
     * @return      int
     ************************************************************************/
    int selectCountProductSyouhinDisabledByPrdIdList(List<Long> prdIdList);

    /************************************************************************
     * <b>Description:</b><br>
     *  select list all syouhin_sys_code from DB for export CSV all check.
     *
     * @author      Nguyen.Chuong
     * @date        Mar 13, 2014
     * @param       entMstProduct EntMstProduct contain filter on server
     * @return      List<String>
     ************************************************************************/
    List<String> selectAllProductsForExportCSV(EntMstProduct entMstProduct);

    /************************************************************************
     * <b>Description:</b><br>
     *  Select max matter no in DB.
     *
     * @author		Nguyen.Chuong
     * @date		Mar 15, 2014
     * @return		int
     ************************************************************************/
    int selectMaxMatterNo();

    /************************************************************************
     * <b>Description:</b><br>
     * Select syouhin sys code list by status type (0:delete, 1:edit, 2:upload).
     *
     * @return      List<Long>
     * @param       syouhinSysCodeList List<Long>
     * @param       statusType int
     * @author      Doan.Chuong
     * @date        2014/03/19
     ************************************************************************/
    List<Long> selectSyouhinSysCodeListByStatus(List<Long> syouhinSysCodeList, int statusType);

    /************************************************************************
     * <b>Description:</b><br>
     *  convert list<String> which String is integer to list<integer>. Remove string is not integer.
     *
     * @author		Nguyen.Chuong
     * @date		Mar 19, 2014
     * @param       listString List<String> string contain only Long
     * @return		List<Long>
     ************************************************************************/
    List<Long> convertListStringToListLong(List<String> listString);

    /************************************************************************
     * <b>Description:</b><br>
     *  selectListAllSiire.
     *
     * @author      Nguyen.Chuong
     * @date        2014/03/28
     * @return      List<EntMstSiire>
     * @throws      SQLException    SQLException
     ************************************************************************/
    List<EntMstSiire> selectListAllSiire() throws SQLException;

    /************************************************************************
     * <b>Description:</b><br>
     * select matter info of product by syouhin sys code.
     *
     * @author		Luong.Dai
     * @date		Aug 6, 2014
     * @param 		syouhinSysCode					Syouhin sys code
     * @return 		List<EntTblFactoryProductNew>	List matter info
     * @throws 		SQLException					Exception when excute
     ************************************************************************/
    List<EntTblFactoryProductNew> selectMatterDetailByProductSyouhinSysCode(Long syouhinSysCode) throws SQLException;
}
