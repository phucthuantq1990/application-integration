/**
 * BrandServiceImpl
 */
package net.webike.RcProductFactory.service;

import java.util.List;

import net.webike.RcProductFactory.entity.EntMstMarume;
import net.webike.RcProductFactory.mapper.MstMarumeMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Tran.Thanh
 * BunruiServiceImpl implements BunruiService
 */
@Service("MarumeServiceImpl")
@Transactional
public class MarumeServiceImpl implements MarumeService {
    @Autowired
    private MstMarumeMapper mstMarumeMapper;

	@Override
	public List<EntMstMarume> selectListAllMarume() {
		List<EntMstMarume> listMarume = mstMarumeMapper.selectListAllMarume();
        return listMarume;
	}
}
