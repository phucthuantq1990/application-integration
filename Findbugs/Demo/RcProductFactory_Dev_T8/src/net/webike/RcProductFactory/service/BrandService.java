/************************************************************************
 * File Name	： BrandService.java
 * Author		： Nguyen.Chuong
 * Version		： 1.0.0
 * Date Created	： 2013/12/27
 * Date Updated	： 2013/12/27
 * Description	： Service to process with brand object.
 *
 ***********************************************************************/
package net.webike.RcProductFactory.service;

import java.sql.SQLException;
import java.util.List;

import net.webike.RcProductFactory.entity.EntMstBrand;


/**
 * Service to process with brand object.
 */
public interface BrandService {

    /************************************************************************
     * <b>Description:</b><br>
     *  Select all brand from mst_brand.
     *
     * @author      Nguyen.Chuong
     * @date        Oct 29, 2013
     * @return      List<EntProduct>
     * @param       pageSize int number record in 1 page
     * @param       pageNumber int current number page
     * @param       entMstBrand content filter value
     * @throws      SQLException error xml
     ************************************************************************/
    List<EntMstBrand> selectListBrand(int pageSize, int pageNumber, EntMstBrand entMstBrand) throws SQLException;

    /************************************************************************
    * <b>Description:</b><br>
     *  Count total record of filter.
     *
     * @author      Luong.Dai
     * @date        Dec 30, 2013
     * @param entMstBrand content filter value
     * @return      int: total record
     * @throws      SQLException error xml
     ************************************************************************/
    int selectTotalRecordOfFilter(EntMstBrand entMstBrand) throws SQLException;

    /************************************************************************
     * <b>Description:</b><br>
     *  select brand by brand_code.
     *
     * @author		Tran.Thanh
     * @date		30 Dec 2013
     * @param 		entMstBrand contain brand_code.
     * @return		EntMstBrand
     * @throws      SQLException error xml
     ************************************************************************/
    EntMstBrand selectBrandByBrandCode(EntMstBrand entMstBrand) throws SQLException;

    /************************************************************************
     * <b>Description:</b><br>
     *  update brand info.
     *
     * @author		Tran.Thanh
     * @date		30 Dec 2013
     * @param 		entMstBrand		EntMstBrand
     * @throws      SQLException error xml
     ************************************************************************/
    void updateBrand(EntMstBrand entMstBrand) throws SQLException;

    /************************************************************************
     * <b>Description:</b><br>
     *  insert new brand.
     *
     * @author		Tran.Thanh
     * @date		30 Dec 2013
     * @param 		entMstBrand entity brand to insert.
     * @return		int
     * @throws      SQLException error xml
     ************************************************************************/
    int insertNewBrand(EntMstBrand entMstBrand) throws SQLException;

    /************************************************************************
     * <b>Description:</b><br>
     *  update brand logo by brand_code.
     *
     * @author		Tran.Thanh
     * @date		3 Jan 2014
     * @param 		brandCode		String
     * @param 		logo			String
     * @throws      SQLException error xml
     ************************************************************************/
    void updateBrandLogo(String brandCode, String logo) throws SQLException;

    /**
     * Select brand list.
     * @author      nguyen.hieu
     * @date		2014/02/17
     * @return		Brand list.
     * @throws		SQLException SQLException.
     */
    List<EntMstBrand> selectBrandList() throws SQLException;

    /************************************************************************
     * <b>Description:</b><br>
     * Check duplicate brand name by brand name and brand code.
     * @param brand store brand name and brand code.
     * @author      hoang.ho
     * @since        2014/02/18
     * @return      String
     ************************************************************************/
    boolean checkExistBrandByCodeName(EntMstBrand brand);

    /**
     * Select all brand list.
     * @author      nguyen.hieu
     * @date		2014-03-05
     * @return		Brand list.
     * @throws		SQLException SQLException.
     */
    List<EntMstBrand> selectAllBrandList() throws SQLException;

    /**
     * Select undeleted brand by brandCode.
     * @author nguyen.hieu
     * @date 2014-03-14
     * @param entMstBrand Brand.
     * @return EntMstBrand
     * @throws SQLException SQLException.
     */
    EntMstBrand selectBrandByBrandCodeNotDeleted(EntMstBrand entMstBrand) throws SQLException;

    /************************************************************************
     * <b>Description:</b><br>
     *  select brand by integer brand code.
     *
     * @author      Nguyen.Chuong
     * @date        2014/03/19
     * @param       brandCode int.
     * @return      EntMstBrand
     ************************************************************************/
    EntMstBrand selectMstBrandByBrandCode(int brandCode);
}
