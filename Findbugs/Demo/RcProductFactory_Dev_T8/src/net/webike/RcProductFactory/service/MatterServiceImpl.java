/**
 * MatterServiceImpl
 */
package net.webike.RcProductFactory.service;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import net.webike.RcProductFactory.entity.EntMstFactoryMatter;
import net.webike.RcProductFactory.entity.EntMstFactoryMatterNew;
import net.webike.RcProductFactory.entity.EntMstFactoryMatterStatus;
import net.webike.RcProductFactory.entity.EntProduct;
import net.webike.RcProductFactory.entity.EntTblFactoryProductNew;
import net.webike.RcProductFactory.mapper.MstFactoryMatterMapper;
import net.webike.RcProductFactory.mapper.MstFactoryMatterStatusMapper;
import net.webike.RcProductFactory.mapper.MstModelMapper;
import net.webike.RcProductFactory.mapper.MstProductMapper;
import net.webike.RcProductFactory.mapper.TblFactoryProductMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Nguyen.Chuong
 * MainMenuServiceImpl implements MainMenuService
 */
@Service("matterServiceImpl")
@Transactional
public class MatterServiceImpl implements MatterService {

    @Autowired
    private MstFactoryMatterStatusMapper matterStatusMapper;

    @Autowired
    private MstFactoryMatterMapper matterMapper;

    @Autowired
    private ProductService productService;

    @Autowired
    private MstProductMapper mstProductMapper;

    @Autowired
    private TblFactoryProductMapper tblFactoryProductMapper;
    
    @Autowired
    private MstModelMapper mstModelMapper;

    @Override
    public List<EntMstFactoryMatterStatus> selectAllMatterStatus() {
        List<EntMstFactoryMatterStatus> listStatus = matterStatusMapper.selectAllMatterStatus();
        return listStatus;
    }

    @Override
    public List<EntMstFactoryMatter> selectListMatterByStatusAndUser(String status, String user) {
        List<EntMstFactoryMatter> listUser = matterMapper.selectMatter(status, user);
        return listUser;
    }

    @Override
    public EntMstFactoryMatter selectDetailMatterByMatterNo(String matterNo) {
        EntMstFactoryMatter factoryMatter = matterMapper.getDetailPJByMatterNo(matterNo);
        return factoryMatter;
    }

    @Override
    public void deleteMatter(String userId, String matterNo) {
        matterMapper.deleteMatterByMatterNo(userId, matterNo);
    }

    @Override
    public boolean updateDetailMatter(EntMstFactoryMatter entMatter) {
        if (null == entMatter) {
            return false;
        }
        matterMapper.updateDetailMatter(entMatter);
        return true;
    }

    @Override
    public void insertNewMatter(EntMstFactoryMatter entFactoryMatter) {
        if (entFactoryMatter != null) {
            matterMapper.insertNewMatter(entFactoryMatter);
        }
    }

    @Override
    public int selectMaxMatterNo() {
        return matterMapper.selectMaxMatterNo();
    }

    @Override
    public boolean releaseMatter(String userId, String matterNo) {
        EntProduct entProduct = new EntProduct();
        entProduct.setMatterNo(matterNo);
        EntMstFactoryMatter entMstFactoryMatter = productService.countProductOfMatter(entProduct);
        if (entMstFactoryMatter.getTotalErrorProduct() > 0) {
            return false;
        }
        matterMapper.releaseMatter(userId, matterNo);
        return true;
    }

    @Override
    public void insertNewMatterNew(EntMstFactoryMatterNew entFactoryMatterNew) {
    	matterMapper.insertNewMatterNew(entFactoryMatterNew);
    }

    //BOE #7208 Tran.Thanh 2014/05/08 : change function "void deleteNewMatterNewList" to "boolean deleteNewMatterNewList"
    /*
    @Override
    @Transactional
    public void deleteNewMatterNewList(List<Long> matterNoList, String updatedUserId, List<String> keyList, String matterChargeUserId) throws SQLException {
    	// Get matterList from database for checking.
    	List<EntMstFactoryMatterNew> matterList = matterMapper.selectMatterListByMatterNo(matterNoList);

    	// Compare array size.
    	if (keyList.size() != matterList.size()) {
    		throw new IllegalArgumentException();
    	}

    	// Init date format.
    	DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    	// Compare updated time.
//    	int index = 0;
    	int matterNoListLength = 0;
    	long matterNoFromOutside = 0;
    	// BOE #6634 No.53 Hoang.Ho 2014/02/19 Fix bug delete when choose multiple brand.
    	List<Long> deletedMatterList = new ArrayList<Long>();
    	for (EntMstFactoryMatterNew matterFromDb : matterList) {
    		// Get matterNo from database.
    		long matterNoFromDb = matterFromDb.getMatterNo();
    		matterNoListLength = matterNoList.size();
//    		for (Integer matterNoFromOutside : matterNoList) {
    		for (int matterNoIndex = 0; matterNoIndex < matterNoListLength; matterNoIndex++) {
    		    // get current matterNo from client.
    		    matterNoFromOutside = matterNoList.get(matterNoIndex);
    		    // Compare with database
    			if (matterNoFromDb == matterNoFromOutside) {
    				// Get updatedTime from database.
    				String updatedTimeFromDb = formatter.format(matterFromDb.getUpdatedOn());

    				// Get updatedTime from outside.
    				String updatedTimeFromOutside = keyList.get(matterNoIndex);

    				// Get delete flag from database.
    				int delFlag = matterFromDb.getMatterDelFlg();
    				int regisFlag = matterFromDb.getMatterRegistrationFlg();

    				// Delete matter only if it hasn't been deleted yet and not been released yet.
    				if (delFlag == 0 && regisFlag == 0) {
        				// Compare.
        				if (!updatedTimeFromDb.equals(updatedTimeFromOutside)) {
        					throw new ExclusionException(matterNoFromDb, matterFromDb.getMatterName(),
        							matterFromDb.getUpdatedUserId(), matterFromDb.getUpdatedOn());
        				}

        				// Store deleted matter.
        				deletedMatterList.add(matterNoFromDb);
        				break;
    				}
    			}
    		}
//    		++index;
    	}

    	for (Long deletedMatterNo : deletedMatterList) {
    		// BOE Tran.Thanh : Fix bug No.64 - Set maintenance flag = 0 when deleted matter in mst_product 2014/18/03
    		List<EntTblFactoryProductNew> listEntProduct = tblFactoryProductMapper.selectEntProductByMatterNo(deletedMatterNo);
    		List<String> listSyouhinSysCode = new ArrayList<String>();
    		for (EntTblFactoryProductNew product : listEntProduct) {
    			listSyouhinSysCode.add(product.getProductSyouhinSysCode().toString());
    		}
    		if (listSyouhinSysCode.size() > 0) {
    			mstProductMapper.updateProductMaintenanceFlgToZeroBySyouhinSysCode(listSyouhinSysCode, updatedUserId);
    		}
    		// EOE Tran.Thanh : Fix bug No.64 - Set maintenance flag = 0 when deleted matter in mst_product 2014/18/03

    		// Delete matter.
    		matterMapper.deleteMatterNew(deletedMatterNo, updatedUserId, matterChargeUserId);

    		// Delete unreleased products infos (product_general).
    		matterMapper.deleteUnreleasedProductInfos(deletedMatterNo);

    		// Delete unreleased products related to the matter (product).
    		matterMapper.deleteUnreleasedProducts(deletedMatterNo);
    	}
    }*/

    @Override
    @Transactional
    public boolean deleteNewMatterNewList(List<Long> matterNoList, String updatedUserId, String matterChargeUserId, String popupConfirm) throws SQLException {
    	// Get matterList from database for checking.
    	List<EntMstFactoryMatterNew> matterList = matterMapper.selectMatterListByMatterNo(matterNoList);

    	List<Long> deletedMatterList = new ArrayList<Long>();

    	// BOE #6634 No.53 Hoang.Ho 2014/02/19 Fix bug delete when choose multiple brand.
    	for (EntMstFactoryMatterNew matterFromDb : matterList) {
    		// Get matterNo from database.
    		long matterNoFromDb = matterFromDb.getMatterNo();
    		// Get delete flag from database.
			int delFlag = matterFromDb.getMatterDelFlg();
			int regisFlag = matterFromDb.getMatterRegistrationFlg();

			// Delete matter only if it hasn't been deleted yet and not been released yet.
			if (delFlag == 0 && regisFlag == 0) {
				// Store deleted matter.
				deletedMatterList.add(matterNoFromDb);
				/*BOE dang.nam delete all matters*/
				//break;
				/*EOE dang.nam delete all matters*/
			} else if (regisFlag != 0) { /*BOE dang.nam 16/06/2014 check when registration flag != 0*/
				if (popupConfirm.equals("false")) {
					return false;
				}
				// throw new CustomException(matterFromDb);
			}
			/*EOE dang.nam 16/06/2014 check when registration flag != 0*/
    	}

    	if (deletedMatterList.isEmpty()) {
    		return false;
    	} else {
    		for (Long deletedMatterNo : deletedMatterList) {
        		// BOE Tran.Thanh : Fix bug No.64 - Set maintenance flag = 0 when deleted matter in mst_product 2014/18/03
        		List<EntTblFactoryProductNew> listEntProduct = tblFactoryProductMapper.selectEntProductByMatterNo(deletedMatterNo);
        		/*BOE #7331 Nguyen.Chuong 2014/01/07 remove confirm when delete matter contain released product.*/
        		List<String> listSyouhinSysCode = new ArrayList<String>();
        		for (EntTblFactoryProductNew product : listEntProduct) {
        		    /*BOE #7208 remove code check product release in matter when select all.*/
//        			/*BOE dang.nam 17/06/2014 check when registration flag != 0*/
//        			if ((product.getProductRegistrationFlg() == 1) && (popupConfirm.equals("false")))
//        			{
//        				return false;
//        			}
//        			/*EOE dang.nam 17/06/2014 check when registration flag != 0*/
        		    /*EOE #7208 remove code check product release in matter when select all.*/
        			listSyouhinSysCode.add(product.getProductSyouhinSysCode().toString());
        		}
        		if (listSyouhinSysCode.size() > 0) {
        			mstProductMapper.updateProductMaintenanceFlgToZeroBySyouhinSysCode(listSyouhinSysCode, updatedUserId);
        		}
        		/*EOE #7331 Nguyen.Chuong 2014/01/07 remove confirm when delete matter contain released product.*/
        		// EOE Tran.Thanh : Fix bug No.64 - Set maintenance flag = 0 when deleted matter in mst_product 2014/18/03
        		// Delete matter.
        		matterMapper.deleteMatterNew(deletedMatterNo, updatedUserId, matterChargeUserId);

        		// Delete unreleased products infos (product_general).
//        		matterMapper.deleteUnreleasedProductInfos(deletedMatterNo);

        		// Delete unreleased products related to the matter (product).
        		matterMapper.deleteUnreleasedProducts(deletedMatterNo);
        	}
    	}

    	return true;
    }
    //EOE #7208 Tran.Thanh 2014/05/08 : change function "void deleteNewMatterNewList" to "boolean deleteNewMatterNewList"
    @Override
    public EntMstFactoryMatterNew selectFactoryMatterByMatterCode(Long matterNo) {
        return matterMapper.selectFactoryMatterByMatterCode(matterNo);

    }

}
