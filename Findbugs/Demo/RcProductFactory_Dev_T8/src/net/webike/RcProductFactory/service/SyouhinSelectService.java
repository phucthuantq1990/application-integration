package net.webike.RcProductFactory.service;

import java.sql.SQLException;
import java.util.List;

import net.webike.RcProductFactory.entity.EntMstSyouhinSelect;

/**
 * @author nguyen.hieu
 *
 */
public interface SyouhinSelectService {
	/**
	 * Get syouhin select list.
     * @author      nguyen.hieu
     * @since       2014/02/17
	 * @return		Syouhin select.
	 * @throws		SQLException SQLException.
	 */
    List<EntMstSyouhinSelect> selectSyouhinSelect() throws SQLException;
}
