/**
 * BrandServiceImpl
 */
package net.webike.RcProductFactory.service;

import java.sql.SQLException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.webike.RcProductFactory.entity.EntMstBrand;
import net.webike.RcProductFactory.mapper.MstBrandMapper;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * @author Tran.Thanh
 * BunruiServiceImpl implements BunruiService
 */
@Service("brandService")
public class BrandServiceImpl implements BrandService {
    @Autowired
    private MstBrandMapper mstBrandMapper;

    @Override
    public List<EntMstBrand> selectListBrand(int limit, int offset, EntMstBrand entMstBrand) throws SQLException {
        List<EntMstBrand> listBrand = null;
        //Get limit and offset to load data.
        offset = (offset - 1) * limit;
        listBrand = mstBrandMapper.selectListBrand(limit, offset, entMstBrand);
        return listBrand;
    }

    @Override
    public int selectTotalRecordOfFilter(EntMstBrand entMstBrand) throws SQLException {
        return mstBrandMapper.selectTotalRecordOfFilter(entMstBrand);
    }

	@Override
	public EntMstBrand selectBrandByBrandCode(EntMstBrand entMstBrand) throws SQLException {
		EntMstBrand entBrand = mstBrandMapper.selectBrandByBrandCode(entMstBrand);
		return entBrand;
	}

	@Override
	@Transactional
	public void updateBrand(EntMstBrand entMstBrand) throws SQLException {
		mstBrandMapper.updateBrand(entMstBrand);
	}

	@Override
	@Transactional
	public int insertNewBrand(EntMstBrand entMstBrand) throws SQLException {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("returnedId", 0);
		mstBrandMapper.insertNewBrand(entMstBrand, map);
		// get key auto generate
		int key  = (Integer) map.get("returnedId");
		if (key > 0) {
			return key;
		} else {
			return 0;
		}
	}

	@Override
	@Transactional
	public void updateBrandLogo(String brandCode, String logo) throws SQLException {
		if (StringUtils.isNotBlank(brandCode)) {
			mstBrandMapper.updateBrandLogo(brandCode, logo);
		}
	}

	@Override
	public List<EntMstBrand> selectBrandList() throws SQLException {
		List<EntMstBrand> brandList = mstBrandMapper.selectBrandList();
		if (brandList != null) {
			return brandList;
		}
		return Collections.emptyList();
	}

    @Override
    public boolean checkExistBrandByCodeName(EntMstBrand brand) {
        EntMstBrand result = mstBrandMapper.selectBrandByNameAndCode(brand);
        return result != null;
    }

	@Override
	public List<EntMstBrand> selectAllBrandList() throws SQLException {
		List<EntMstBrand> brandList = mstBrandMapper.selectAllBrandList();
		if (brandList != null) {
			return brandList;
		}
		return Collections.emptyList();
	}

    /**
     * Select undeleted brand by brandCode.
     * @author nguyen.hieu
     * @date 2014-03-14
     * @param entMstBrand Brand.
     * @return EntMstBrand
     * @throws SQLException SQLException.
     */
    public EntMstBrand selectBrandByBrandCodeNotDeleted(EntMstBrand entMstBrand) throws SQLException {
		EntMstBrand entBrand = mstBrandMapper.selectBrandByBrandCodeNotDeleted(entMstBrand);
		if (entBrand != null) {
			return entBrand;
		}
		return new EntMstBrand();
    }

    @Override
    public EntMstBrand selectMstBrandByBrandCode(int brandCode) {
        EntMstBrand entBrand = mstBrandMapper.selectMstBrandByBrandCode(brandCode);
        return entBrand;
    }
}
