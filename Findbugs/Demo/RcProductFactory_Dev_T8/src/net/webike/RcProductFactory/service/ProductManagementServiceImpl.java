/**
 * ProductManagementServiceImpl
 */
package net.webike.RcProductFactory.service;

import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.webike.RcProductFactory.entity.EntCSVMode1;
import net.webike.RcProductFactory.entity.EntCSVMode3;
import net.webike.RcProductFactory.entity.EntMstAttribute;
import net.webike.RcProductFactory.entity.EntMstAttributeGroup;
import net.webike.RcProductFactory.entity.EntMstFactoryMatter;
import net.webike.RcProductFactory.entity.EntMstFactoryMatterNew;
import net.webike.RcProductFactory.entity.EntMstNouki;
import net.webike.RcProductFactory.entity.EntMstProduct;
import net.webike.RcProductFactory.entity.EntMstSiire;
import net.webike.RcProductFactory.entity.EntTblFactoryImportCondition;
import net.webike.RcProductFactory.entity.EntTblFactoryProductGuestInput;
import net.webike.RcProductFactory.entity.EntTblFactoryProductImage;
import net.webike.RcProductFactory.entity.EntTblFactoryProductLink;
import net.webike.RcProductFactory.entity.EntTblFactoryProductNew;
import net.webike.RcProductFactory.entity.EntTblFactoryProductSelect;
import net.webike.RcProductFactory.entity.EntTblFactoryProductSupplier;
import net.webike.RcProductFactory.entity.EntTblFactoryProductVideo;
import net.webike.RcProductFactory.entity.EntTblProductGuestInput;
import net.webike.RcProductFactory.entity.EntTblProductLink;
import net.webike.RcProductFactory.entity.EntTblProductVideo;
import net.webike.RcProductFactory.entity.EntTblSyouhinFitModel;
import net.webike.RcProductFactory.entity.EntTblSyouhinImage;
import net.webike.RcProductFactory.entity.EntTblTmpCSV;
import net.webike.RcProductFactory.entity.MstProductSqlCondition;
import net.webike.RcProductFactory.mapper.MstAttributeGroupMapper;
import net.webike.RcProductFactory.mapper.MstAttributeMapper;
import net.webike.RcProductFactory.mapper.MstFactoryMatterMapper;
import net.webike.RcProductFactory.mapper.MstFactoryUserTmpMapper;
import net.webike.RcProductFactory.mapper.MstNoukiMapper;
import net.webike.RcProductFactory.mapper.MstProductMapper;
import net.webike.RcProductFactory.mapper.MstProductStatusMapper;
import net.webike.RcProductFactory.mapper.MstSiireMapper;
import net.webike.RcProductFactory.mapper.TblFactoryImportConditionMapper;
import net.webike.RcProductFactory.mapper.TblFactoryProductConditionMapper;
import net.webike.RcProductFactory.mapper.TblFactoryProductDescriptionMapper;
import net.webike.RcProductFactory.mapper.TblFactoryProductGeneralMapper;
import net.webike.RcProductFactory.mapper.TblFactoryProductGuestInputMapper;
import net.webike.RcProductFactory.mapper.TblFactoryProductImageMapper;
import net.webike.RcProductFactory.mapper.TblFactoryProductLinkMapper;
import net.webike.RcProductFactory.mapper.TblFactoryProductMapper;
import net.webike.RcProductFactory.mapper.TblFactoryProductModelMapper;
import net.webike.RcProductFactory.mapper.TblFactoryProductNumberingMapper;
import net.webike.RcProductFactory.mapper.TblFactoryProductSelectMapper;
import net.webike.RcProductFactory.mapper.TblFactoryProductSupplierMapper;
import net.webike.RcProductFactory.mapper.TblFactoryProductSupplierPriceMapper;
import net.webike.RcProductFactory.mapper.TblFactoryProductVideoMapper;
import net.webike.RcProductFactory.mapper.TblProductAttributeMapper;
import net.webike.RcProductFactory.mapper.TblProductGuestInputMapper;
import net.webike.RcProductFactory.mapper.TblProductLinkMapper;
import net.webike.RcProductFactory.mapper.TblProductVideoMapper;
import net.webike.RcProductFactory.mapper.TblSyouhinFitModelMapper;
import net.webike.RcProductFactory.mapper.TblSyouhinImageMapper;
import net.webike.RcProductFactory.util.Constant;

import org.apache.commons.beanutils.BeanComparator;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionStatus;

/**
 * @author Nguyen.Chuong
 * ProductManagementServiceImpl implements ProductManagementService
 */
@Service("productManagementService")
public class ProductManagementServiceImpl extends BaseService implements ProductManagementService {
    @Autowired
    private MstProductMapper mstProductMapper;
    @Autowired
    private MstSiireMapper mstSiireMapper;
    @Autowired
    private TblSyouhinImageMapper tblSyouhinImageMapper;
    @Autowired
    private MstNoukiMapper mstNoukiMapper;
    @Autowired
    private MstAttributeMapper mstAttributeMapper;
    @Autowired
    private MstFactoryMatterMapper mstFactoryMatterMapper;
    @Autowired
    private TblSyouhinFitModelMapper tblSyouhinFitModelMapper;
    @Autowired
    private TblProductAttributeMapper tblProductAttributeMapper;
    @Autowired
    private TblProductVideoMapper tblProductVideoMapper;
    @Autowired
    private MstFactoryUserTmpMapper mstFactoryUserTmpMapper;
    @Autowired
    private MstProductStatusMapper mstProductStatusMapper;
    @Autowired
    private TblProductLinkMapper tblProductLinkMapper;
    @Autowired
    private TblProductGuestInputMapper tblProductGuestInputMapper;
    @Autowired
    private MstAttributeGroupMapper mstAttributeGroupMapper;
    @Autowired
    private TblFactoryImportConditionMapper tblFactoryImportConditionMapper;
    @Autowired
    private TblFactoryProductMapper tblFactoryProductMapper;
    @Autowired
    private TblFactoryProductGeneralMapper tblFactoryProductGeneralMapper;
    @Autowired
    private TblFactoryProductConditionMapper tblFactoryProductConditionMapper;
    @Autowired
    private TblFactoryProductDescriptionMapper tblFactoryProductDescriptionMapper;
    @Autowired
    private TblFactoryProductGuestInputMapper tblFactoryProductGuestInputMapper;
    @Autowired
    private TblFactoryProductLinkMapper tblFactoryProductLinkMapper;
    @Autowired
    private TblFactoryProductVideoMapper tblFactoryProductVideoMapper;
    @Autowired
    private TblFactoryProductSupplierPriceMapper tblFactoryProductSupplierPriceMapper;
    @Autowired
    private TblFactoryProductImageMapper tblFactoryProductImageMapper;
    @Autowired
    private TblFactoryProductSelectMapper tblFactoryProductSelectMapper;
    @Autowired
    private TblFactoryProductSupplierMapper tblFactoryProductSupplierMapper;
    @Autowired
    private TblFactoryProductModelMapper tblFactoryProductModelMapper;
    @Autowired
    private TblFactoryProductNumberingMapper tblFactoryProductNumberingMapper;

    @Override
    public List<EntMstProduct> selectProductList(int limit, int offset, EntMstProduct product) {
        List<EntMstProduct> result = null;
        try {
            offset = (offset - 1) * limit;

            // BOE hoang.ho 2014/03/12 Tuning sql for product management
            // Get products id list.
            List<String> productIdList = mstProductMapper.selectProductIdList(limit, offset, product);
            if (productIdList != null && productIdList.size() > 0) {
                // convert to string
                String prodsIdStr = StringUtils.join(productIdList.toArray(), ",");
                // set to product
                product.setProductIdList(prodsIdStr);

                result = mstProductMapper.selectProductList(limit, offset, product);
            }

            // EOE hoang.ho 2014/03/12 Tuning sql for product management
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public int selectTotalRecordOfGrid(EntMstProduct product) {
        int total = 0;
        try {
            total = mstProductMapper.selectTotalRecordOfGrid(product);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return total;
    }

    @Override
    public EntMstFactoryMatter selectMatterByMatterNo(long matterNo) {
        EntMstFactoryMatter result = null;
        try {
            result = mstFactoryMatterMapper.selectMatterByMatterNo(matterNo);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public boolean updateProductDelFlg(List<Long> productList, EntMstProduct prdObj) {
        boolean result = false;
        int updatedNum = 0;
        // create transaction.
        TransactionStatus status = null;
        try {
            // start transaction.
            status =  this.startTransaction();
            if (productList != null && productList.size() != 0) {
                updatedNum = mstProductMapper.updateProductDelFlg(productList, prdObj);
            }
        } catch (Exception e) {
            e.printStackTrace();
            this.rollback(status);
        }
        if (updatedNum > 0) {
            result = true;
            this.commit(status);
        } else {
            this.rollback(status);
        }
        return result;
    }

    @Override
    public boolean updateProductSupplierStatusCode(List<Long> prdIdList, EntMstProduct entMstProduct) {
        boolean result = false;
        int updatedNum = 0;
        // create transaction.
        TransactionStatus status = null;
        try {
            // start transaction.
            status =  this.startTransaction();
            if (prdIdList != null && prdIdList.size() != 0) {
                updatedNum = mstProductMapper.updateProductSupplierStatusCode(prdIdList, entMstProduct);
            }
        } catch (Exception e) {
            e.printStackTrace();
            this.rollback(status);
        }
        if (updatedNum > 0) {
            result = true;
            this.commit(status);
        } else {
            this.rollback(status);
        }
        return result;
    }

    @Override
    public List<String> selectAllFullname() {
        List<String> result = null;
        try {
            result = mstFactoryUserTmpMapper.selectAllFullname();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public List<String> selectAllProductStatusName() {
        List<String> result = null;
        try {
            result = mstProductStatusMapper.selectAllProductStatusName();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public List<EntMstProduct> selectProductListByBrdCodeAndGrpCode(List<EntMstProduct> productList) {
        List<EntMstProduct> result = new ArrayList<EntMstProduct>();
        List<EntMstProduct> prdList = null;
        try {
            if (productList != null) {
                for (EntMstProduct prd: productList) {
                    // get product list by product
                    prdList = mstProductMapper.selectProductListByBrdCodeAndGrpCode(prd);
                    if (prdList != null) {
                        // if the return list is not null
                        // then add to result
                        result.addAll(prdList);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public List<EntTblFactoryImportCondition> selectImportConditionListByMatterNo(long matterNo) {
        List<EntTblFactoryImportCondition> result = null;
        try {
            result = tblFactoryImportConditionMapper.selectImportConditionListByMatterNo(matterNo);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public List<EntMstProduct> selectProductBrdCodeAndGrpCodeListWithoutGroup0(List<Long> prdIdList) {
        List<EntMstProduct> result = null;
        try {
            result = mstProductMapper.selectProductBrdCodeAndGrpCodeListWithoutGroup0(prdIdList);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public List<EntMstProduct> selectSyouhinSysCodeOnlyGroup0ByListSyouhin(List<Long> prdIdList) {
        List<EntMstProduct> result = null;
        try {
            result = mstProductMapper.selectSyouhinSysCodeOnlyGroup0ByListSyouhin(prdIdList);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    @SuppressWarnings("unchecked")
    @Override
    public boolean processUpdateMaintenanceProduct(String user, long matterNo, List<EntMstProduct> prdList
                                                  , EntMstFactoryMatter entMstFactoryMatter
                                                  , EntTblFactoryImportCondition entTblFactoryImportCondition) {
        boolean result = false;
        // map to get last insert key in tbl_factory_matter table
        Map<String, Long> matterMap = new HashMap<String, Long>();
        // map to get last insert key in tbl_factory_import_condition
        Map<String, Integer> importMap = new HashMap<String, Integer>();
        int rowsAffected = 1;
        long productId;
        int importConditionCode;
        // create transaction.
        TransactionStatus status = null;
        try {
            // start transaction.
            status = this.startTransaction();
            if (entMstFactoryMatter != null) {
                matterMap.put("matterId", (long) 0);
                // insert new matter to DB
                entMstFactoryMatter.setTotalAllProduct(prdList.size());
                rowsAffected = mstFactoryMatterMapper.insertNewMatterObj(entMstFactoryMatter, matterMap);
                // get last insert key
                matterNo = matterMap.get("matterId");
            } else {
                //update matter count
                EntMstFactoryMatterNew entMatter =  mstFactoryMatterMapper.selectFactoryMatterByMatterCode(matterNo);
                int countOfMatter = entMatter.getMatterFactoryCount();
                if (null != prdList) {
                    countOfMatter = countOfMatter + prdList.size();
                }
                mstFactoryMatterMapper.updateCountProductOfMatter(matterNo, countOfMatter, user);
            }
            if (rowsAffected > 0) {
                importMap.put("importConditionCode", 0);
                // reset matter no
                entTblFactoryImportCondition.setImportConditionMatterNo(matterNo);
                entTblFactoryImportCondition.setCreatedUserId(user);
                entTblFactoryImportCondition.setUpdatedUserId(user);
                // insert import condition to DB
                rowsAffected = tblFactoryImportConditionMapper.insertImportCondition(entTblFactoryImportCondition, importMap);
                if (rowsAffected > 0) {
                    // get last insert key
                    importConditionCode = importMap.get("importConditionCode");
                    // set import condition code to object for updating
                    entTblFactoryImportCondition.setImportConditionCode(importConditionCode);
                    // process copy data through DB
                    if (prdList != null) {
                        BeanComparator bc = new BeanComparator("syouhinSysCode");
                        Collections.sort(prdList, bc);
                        // default import condition exe count
                        int numberOfRecordOneTimeInsert = Constant.NUMBER_OF_RECORD_INSERT_ONE_TIME;
                        int from;
                        int to;
                        // get max product id and plus 1 to prevent duplicate key
                        productId = tblFactoryProductNumberingMapper.selectMaxProductIdPlusOne();
                        rowsAffected = tblFactoryProductNumberingMapper.updateTblFactoryProductNumbering(productId - 1 + prdList.size());
                        if (rowsAffected <= 0) {
                            this.rollback(status);
                            return false;
                        }
                        List<EntMstProduct> tempList;
                        for (int i = 0; i <= prdList.size() / numberOfRecordOneTimeInsert; i++) {
                            from = i * numberOfRecordOneTimeInsert;
                            to = (i + 1) * numberOfRecordOneTimeInsert;
                            tempList = getRangeFromList(prdList, from, to, user, productId + (i * numberOfRecordOneTimeInsert));
                            if (!insertData(tempList, matterNo, importConditionCode)) {
                                this.rollback(status);
                                return false;
                            }
                        }
                        rowsAffected = tblFactoryImportConditionMapper.updateImportConditionStatus(entTblFactoryImportCondition);
                        if (rowsAffected <= 0) {
                            this.rollback(status);
                            return false;
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            // rollback when occurring error
            this.rollback(status);
            throw new RuntimeException();
        }
        if (rowsAffected > 0) {
            // commit when no error occur
            this.commit(status);
            result = true;
        } else {
            // rollback when occurring error
            this.rollback(status);
        }
        return result;
    }

    /**
     * insertData.
     * @param dataList List<EntMstProduct>
     * @param matterNo long
     * @param importConditionCode long
     * @return boolean
     */
    private boolean insertData(List<EntMstProduct> dataList, long matterNo, long importConditionCode) {
        if (dataList.size() == 0) { return true; }
        try {
            List<String> syouhinSysCodeList = new ArrayList<String>();
            for (EntMstProduct ent : dataList) {
                syouhinSysCodeList.add(ent.getSyouhinSysCode().toString());
            }
            String updateUserId = dataList.get(0).getUpdatedUserId();
            long firstProductId = dataList.get(0).getProductId();
            // copy data tbl_factory_product
            tblFactoryProductMapper.insertMultipleNewFactoryProduct(matterNo, dataList);
            // copy data tbl_factory_product_general
            tblFactoryProductGeneralMapper.insertMultipleNewFactoryProductGeneral(syouhinSysCodeList, firstProductId - 1, updateUserId);
            // copy data tbl_factory_product_attribute_flag
            List<EntTblTmpCSV> listAttribute = mstAttributeMapper.selectAttributeFlagBySyouhinSysCodeList(syouhinSysCodeList);
            if (listAttribute != null && listAttribute.size() > 0) {
                setProductIdForAttrList(listAttribute, dataList);
                mstAttributeMapper.insertMultipleAttributeFlag(listAttribute, updateUserId);
            }
            // copy data tbl_factory_product_attribute_float
            listAttribute = mstAttributeMapper.selectAttributeFloatBySyouhinSysCodeList(syouhinSysCodeList);
            if (listAttribute != null && listAttribute.size() > 0) {
                setProductIdForAttrList(listAttribute, dataList);
                mstAttributeMapper.insertMultipleAttributeFloat(listAttribute, updateUserId);
            }
            // copy data tbl_factory_product_attribute_group
            listAttribute = mstAttributeMapper.selectAttributeGroupBySyouhinSysCodeList(syouhinSysCodeList);
            if (listAttribute != null && listAttribute.size() > 0) {
                setProductIdForAttrList(listAttribute, dataList);
                mstAttributeMapper.insertMultipleAttributeGroup(listAttribute, updateUserId);
            }
            // copy data tbl_factory_product_attribute_integer
            listAttribute = mstAttributeMapper.selectAttributeIntegerBySyouhinSysCodeList(syouhinSysCodeList);
            if (listAttribute != null && listAttribute.size() > 0) {
                setProductIdForAttrList(listAttribute, dataList);
                mstAttributeMapper.insertMultipleAttributeInteger(listAttribute, updateUserId);
            }
            // copy data tbl_factory_product_attribute_string
            listAttribute = mstAttributeMapper.selectAttributeStringBySyouhinSysCodeList(syouhinSysCodeList);
            if (listAttribute != null && listAttribute.size() > 0) {
                setProductIdForAttrList(listAttribute, dataList);
                mstAttributeMapper.insertMultipleAttributeString(listAttribute, updateUserId);
            }
            // copy data tbl_factory_product_condition
            tblFactoryProductConditionMapper.insertMultipleNewFactoryProductCondition(syouhinSysCodeList, firstProductId - 1, updateUserId);
            // copy data tbl_factory_product_description
            tblFactoryProductDescriptionMapper.insertMultipleNewFactoryProductDescription(syouhinSysCodeList, firstProductId - 1, updateUserId);
            // copy data tbl_factory_product_guest_input
            List<EntTblFactoryProductGuestInput> guestInputList = tblFactoryProductGuestInputMapper.selectProductGuestInputBySyouhinSysCodeList(
                                                                                                                                            syouhinSysCodeList);
            if (guestInputList != null && guestInputList.size() > 0) {
                setProductIdForGuestInputList(guestInputList, dataList, updateUserId);
                tblFactoryProductGuestInputMapper.insertMultipleProductGuestInput(guestInputList);
            }
            // copy data tbl_factory_product_link
            List<EntTblFactoryProductLink> linkList = tblFactoryProductLinkMapper.selectProductLinkBySyouhinSysCodeList(syouhinSysCodeList);
            if (linkList != null && linkList.size() > 0) {
                setProductIdForLinkList(linkList, dataList, updateUserId);
                tblFactoryProductLinkMapper.insertMultipleProductLink(linkList);
            }
            // copy data tbl_factory_product_video
            List<EntTblFactoryProductVideo> videoList = tblFactoryProductVideoMapper.selectProductVideoBySyouhinSysCodeList(syouhinSysCodeList);
            if (videoList != null && videoList.size() > 0) {
                setProductIdForVideoList(videoList, dataList, updateUserId);
                tblFactoryProductVideoMapper.insertMultipleProductVideo(videoList);
            }
            // copy data tbl_factory_product_supplier_price
            tblFactoryProductSupplierPriceMapper.insertMultipleNewFactoryProductSupplierPrice(syouhinSysCodeList, firstProductId - 1, updateUserId);
            // copy data tbl_factory_product_fit_model
            List<EntTblTmpCSV> fitModelList = tblFactoryProductModelMapper.selectSyouhinFitModelBySyouhinSysCodeList2(syouhinSysCodeList);
            if (fitModelList != null && fitModelList.size() > 0) {
                setProductIdFitModelList(fitModelList, dataList);
                tblFactoryProductModelMapper.insertMultipleFitModel(fitModelList, updateUserId);
            }
            // copy data tbl_factory_product_image
            List<EntTblFactoryProductImage> imageList = tblFactoryProductImageMapper.selectProductImageBySyouhinSysCodeList(syouhinSysCodeList);
            if (imageList != null && imageList.size() > 0) {
                setProductIdForImageList(imageList, dataList, updateUserId);
                tblFactoryProductImageMapper.insertMultipleProductImage(imageList);
            }
            // copy data tbl_factory_product_select
            List<EntTblFactoryProductSelect> selectList = tblFactoryProductSelectMapper.selectProductSelectBySyouhinSysCodeList(syouhinSysCodeList);
            if (selectList != null && selectList.size() > 0) {
                setProductIdForSelectList(selectList, dataList, updateUserId);
                tblFactoryProductSelectMapper.insertMultipleProductSelect(selectList);
            }
            // copy data tbl_factory_product_supplier
            List<EntTblFactoryProductSupplier> supplierList = tblFactoryProductSupplierMapper.selectProductSupplierBySyouhinSysCodeList(syouhinSysCodeList);
            if (supplierList != null && supplierList.size() > 0) {
                setProductIdForSupplierList(supplierList, dataList, updateUserId);
                tblFactoryProductSupplierMapper.insertMultipleProductSupplier(supplierList);
            }
            // udpate mst_product.product_maintenance_flg = 1
            int rowsAffected = mstProductMapper.updateProductMaintenanceFlgBySyouhinSysCodeList(syouhinSysCodeList, updateUserId);
            // count up tbl_factory_import_condition.import_condition_exe_count
            if (rowsAffected == dataList.size()) {
                EntTblFactoryImportCondition entTblFactoryImportCondition = new EntTblFactoryImportCondition();
                entTblFactoryImportCondition.setImportConditionCode(importConditionCode);
                // set exe count to object to udpating
                entTblFactoryImportCondition.setImportConditionExeCount(rowsAffected);
                entTblFactoryImportCondition.setUpdatedUserId(updateUserId);
                rowsAffected = tblFactoryImportConditionMapper.updateImportConditionExeCount(entTblFactoryImportCondition);
                // when one product update fail then break loop and return error
                if (rowsAffected <= 0) { return false; }
            } else {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    /**
     * setProductIdForSupplierList.
     * @param supplierList List<EntTblFactoryProductSupplier>
     * @param productList List<EntMstProduct>
     * @param user String
     */
    private void setProductIdForSupplierList(List<EntTblFactoryProductSupplier> supplierList, List<EntMstProduct> productList, String user) {
        for (EntTblFactoryProductSupplier ent : supplierList) {
            ent.setProductId(getProductIdBySyouhinSysCode(ent.getSyouhinSysCode(), productList));
            ent.setUpdatedUserId(user);
        }
    }

    /**
     * setProductIdForSelectList.
     * @param selectList List<EntTblFactoryProductSelect>
     * @param productList List<EntMstProduct>
     * @param user String
     */
    private void setProductIdForSelectList(List<EntTblFactoryProductSelect> selectList, List<EntMstProduct> productList, String user) {
        for (EntTblFactoryProductSelect ent : selectList) {
            ent.setProductId(getProductIdBySyouhinSysCode(ent.getSyouhinSysCode(), productList));
            ent.setUpdatedUserId(user);
        }
    }

    /**
     * setProductIdForImageList.
     * @param imageList List<EntTblFactoryProductImage>
     * @param productList List<EntMstProduct>
     * @param user String
     */
    private void setProductIdForImageList(List<EntTblFactoryProductImage> imageList, List<EntMstProduct> productList, String user) {
        for (EntTblFactoryProductImage ent : imageList) {
            ent.setProductId(getProductIdBySyouhinSysCode(ent.getSyouhinSysCode(), productList));
            ent.setUpdatetedUserId(user);
        }
    }

    /**
     * setProductIdFitModelList.
     * @param fitModelList List<EntTblTmpCSV>
     * @param productList List<EntMstProduct>
     */
    private void setProductIdFitModelList(List<EntTblTmpCSV> fitModelList, List<EntMstProduct> productList) {
        long oldSyouhinSysCode = -1;
        int sortValue = 0;
        for (EntTblTmpCSV ent : fitModelList) {
            if (ent.getProductSyouhinSysCode() != oldSyouhinSysCode) {
                oldSyouhinSysCode = ent.getProductSyouhinSysCode();
                sortValue = 0;
            }
            ent.setProductId(getProductIdBySyouhinSysCode(ent.getProductSyouhinSysCode(), productList));
            ent.setFitModelSort(sortValue);
            sortValue++;
        }
    }

    /**
     * setProductIdForVideoList.
     * @param videoList List<EntTblFactoryProductVideo>
     * @param productList List<EntMstProduct>
     * @param user String
     */
    private void setProductIdForVideoList(List<EntTblFactoryProductVideo> videoList, List<EntMstProduct> productList, String user) {
        for (EntTblFactoryProductVideo ent : videoList) {
            ent.setProductId(getProductIdBySyouhinSysCode(ent.getSyouhinSysCode(), productList));
            ent.setUpdatedUserId(user);
        }
    }

    /**
     * setProductIdForLinkList.
     * @param linkList List<EntTblFactoryProductLink>
     * @param productList List<EntMstProduct>
     * @param user String
     */
    private void setProductIdForLinkList(List<EntTblFactoryProductLink> linkList, List<EntMstProduct> productList, String user) {
        for (EntTblFactoryProductLink ent : linkList) {
            ent.setProductId(getProductIdBySyouhinSysCode(ent.getSyouhinSysCode(), productList));
            ent.setUpdatedUserId(user);
        }
    }

    /**
     * setProductIdForAttrList.
     * @param attrList List<EntTblTmpCSV>
     * @param productList List<EntMstProduct>
     */
    private void setProductIdForAttrList(List<EntTblTmpCSV> attrList, List<EntMstProduct> productList) {
        for (EntTblTmpCSV ent : attrList) {
            ent.setProductId(getProductIdBySyouhinSysCode(ent.getProductSyouhinSysCode(), productList));
        }
    }

    /**
     * setProductIdForGuestInputList.
     * @param guestInputList List<EntTblFactoryProductGuestInput>
     * @param productList List<EntMstProduct>
     * @param user String
     */
    private void setProductIdForGuestInputList(List<EntTblFactoryProductGuestInput> guestInputList, List<EntMstProduct> productList, String user) {
        for (EntTblFactoryProductGuestInput ent : guestInputList) {
            ent.setProductId(getProductIdBySyouhinSysCode(ent.getSyouhinSysCode(), productList));
            ent.setUpdatedUserId(user);
        }
    }

    /**
     * getProductIdBySyouhinSysCode.
     * @param syouhinSysCode long
     * @param productList List<EntMstProduct>
     * @return long
     */
    private long getProductIdBySyouhinSysCode(long syouhinSysCode, List<EntMstProduct> productList) {
        for (EntMstProduct ent : productList) {
            if (ent.getSyouhinSysCode() == syouhinSysCode) {
                return ent.getProductId();
            }
        }
        return -1;
    }

    /**
     * getRangeFromList.
     * @param dataList List<EntMstProduct>
     * @param from int
     * @param to int
     * @param user String
     * @param productIdStart int
     * @return List<EntMstProduct>
     */
    private List<EntMstProduct> getRangeFromList(List<EntMstProduct> dataList, int from, int to, String user, long productIdStart) {
        List<EntMstProduct> result;
        if (to > dataList.size()) {
            to = dataList.size();
        }
        result = dataList.subList(from, to);
        for (int i = 0; i < result.size(); i++) {
            result.get(i).setProductId(productIdStart + i);
            result.get(i).setCreatedUserId(user);
            result.get(i).setUpdatedUserId(user);
        }
        return result;
    }

    @Override
    public EntMstProduct selectProductDataForProductReviewPopupBySyouhinCode(long syouhinSysCode) {
        EntMstProduct entMstProduct = mstProductMapper.selectProductDataForProductReviewPopupBySyouhinCode(syouhinSysCode);

        // percentage of SikiriPrice / ProperPrice.
        float pricePercentage = entMstProduct.getSikiriPrice() * Constant.PERCENT / entMstProduct.getProductProperPrice();
        // format to get 2 digits after decimal point.
        DecimalFormat df = new DecimalFormat("#.##");
        entMstProduct.setSikiriProperPricePercentage(String.valueOf(df.format(pricePercentage)) + "%");

        //Format product_supplier_release_date to String week part type
//        entMstProduct.setProductSupplierReleaseDateStr(Utils.formatDateToJapanWeekPartDate(entMstProduct.getProductSupplierReleaseDate()));
        return entMstProduct;
    }

    @Override
    public List<EntMstSiire> selectListSiireNoukiBySyouhinSysCode(Long syouhinSysCode) {
        List<EntMstSiire> listSiire = mstSiireMapper.selectListSiireNoukiBySyouhinSysCode(syouhinSysCode);
        return listSiire;
    }

    @Override
    public List<EntTblSyouhinImage> selectListImageBySyouhinSysCode(Long syouhinSysCode) {
        List<EntTblSyouhinImage> listImage = tblSyouhinImageMapper.selectListImageBySyouhinSysCode(syouhinSysCode);
        String imageServer = Constant.IMAGE_SERVER_2;
        for (EntTblSyouhinImage entImageTmp : listImage) {
            entImageTmp.setImagePath(imageServer + entImageTmp.getImage());
            entImageTmp.setThumbnailPath(imageServer + entImageTmp.getThumbnail());
        }
        return listImage;
    }

    @Override
    public List<EntTblSyouhinFitModel> selectListSyouhinFitModelBySyouhinSysCode(Long syouhinSysCode) {
        List<EntTblSyouhinFitModel> listSyouhinFitModel = tblSyouhinFitModelMapper.selectListSyouhinFitModelBySyouhinSysCode(syouhinSysCode);
        return listSyouhinFitModel;
    }

    @Override
    public List<EntMstAttribute> selectListAttributeAllTypeBySyouhinSysCode(Long syouhinSysCode) {
        List<EntMstAttribute> listAttributeAllType = new ArrayList<EntMstAttribute>();
        List<EntMstAttribute> listTmp = null;
        //Get flg attribute
        listTmp = tblProductAttributeMapper.selectListAttributeFlgBySyouhinCode(syouhinSysCode);
        for (EntMstAttribute ent : listTmp) {
            ent.setAttributeDisplay("flg");
        }
        listAttributeAllType.addAll(listTmp);
        //Get Float attribute
        listAttributeAllType.addAll(tblProductAttributeMapper.selectListAttributeFloatBySyouhinCode(syouhinSysCode));
        //Get group attribute
        listAttributeAllType.addAll(tblProductAttributeMapper.selectListAttributeGroupBySyouhinCode(syouhinSysCode));
        //Get Integer attribute
        listAttributeAllType.addAll(tblProductAttributeMapper.selectListAttributeIntegerBySyouhinCode(syouhinSysCode));
        //Get String attribute
        listTmp = tblProductAttributeMapper.selectListAttributeStringBySyouhinCode(syouhinSysCode);
        for (EntMstAttribute ent : listTmp) {
            ent.setAttributeDisplay("string");
        }
        listAttributeAllType.addAll(listTmp);
        return listAttributeAllType;
    }

    @Override
    public List<EntTblProductVideo> selectLinkVideoBySyouhinSysCode(Long syouhinSysCode) {
        List<EntTblProductVideo> listVideo = tblProductVideoMapper.selectLinkVideoBySyouhinSysCode(syouhinSysCode);
        return listVideo;
    }

    @Override
    public List<EntMstNouki> selectNoukiList() throws SQLException {
    	List<EntMstNouki> attributeList = mstNoukiMapper.selectNoukiList();
    	if (attributeList != null) {
    		return attributeList;
    	}
    	return Collections.emptyList();
    }

    @Override
    public List<EntMstAttribute> selectAttributeList() throws SQLException {
    	List<EntMstAttribute> attributeList = mstAttributeMapper.selectAttributeList();
    	if (attributeList != null) {
    		return attributeList;
    	}
    	return Collections.emptyList();
    }

    @Override
    public List<EntMstAttributeGroup> selectAttrGroupListByAttributeCode(int attributeCode) throws SQLException {
    	List<EntMstAttributeGroup> attributeGroupList = mstAttributeGroupMapper.selectListAttrGroupByCode(attributeCode);
    	if (attributeGroupList != null) {
    		return attributeGroupList;
    	}
    	return Collections.emptyList();
    }

    @Override
    public List<EntTblProductLink> selectLinkReasonBySyouhinSysCode(Long syouhinSysCode) {
        List<EntTblProductLink> listLinkReason = tblProductLinkMapper.selectLinkReasonBySyouhinSysCode(syouhinSysCode);
        return listLinkReason;
    }

    @Override
    public List<EntTblProductGuestInput> selectListProductGuestInputBySyouhinSysCode(Long syouhinSysCode) {
        List<EntTblProductGuestInput> listGuestInput = tblProductGuestInputMapper.selectListProductGuestInputBySyouhinSysCode(syouhinSysCode);
        return listGuestInput;
    }

    @Override
    public List<EntMstProduct> selectDetailSearchProductList(int limit, int offset, EntMstProduct product)  throws SQLException {
        offset = (offset - 1) * limit;
        List<String> productIdList = null;
        List<EntMstProduct> productList = null;
        // Check if detail search is used.
        if (product.getDetailSearchFlag() == 2) {
            // Model details search
            MstProductSqlCondition sqlCondition = new MstProductSqlCondition(product);
            productIdList = mstProductMapper.selectDetailSearchProductList(limit, offset, product, sqlCondition);
        } else {
            // Mode simple search.
            productIdList = mstProductMapper.selectProductIdList(limit, offset, product);
        }
        if (productIdList != null && productIdList.size() > 0) {
            // convert to string
            String prodsIdStr = StringUtils.join(productIdList.toArray(), ",");
            // set to product
            product.setProductIdList(prodsIdStr);

            productList = mstProductMapper.selectProductList(limit, offset, product);
        }
        if (productList != null) {
            return productList;
        }
        return Collections.emptyList();
    }

    @Override
    public int selectDetailSearchProductListCount(EntMstProduct product) throws SQLException {
		int total = 0;
		// Check if detail search is used.
        if (product.getDetailSearchFlag() == 2) {
            // Model details search
            MstProductSqlCondition sqlCondition = new MstProductSqlCondition(product);
            total = mstProductMapper.selectDetailSearchProductListCount(product, sqlCondition);
        } else {
            // Mode simple search.
            total = mstProductMapper.selectTotalRecordOfGrid(product);
        }
		return total;
    }

    @Override
    public List<EntCSVMode3> selectSyouhinFitModelBySyouhinSysCodeList(List<String> syouhinSysCodeList) {
        List<EntCSVMode3> result = mstProductMapper.selectSyouhinFitModelBySyouhinSysCodeList(syouhinSysCodeList);
        if (result == null) {
            result = new ArrayList<EntCSVMode3>();
        }
        return result;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<EntCSVMode3> selectProductAttributeBySyouhinSysCodeList(List<String> syouhinSysCodeList, List<String> attributeCodeList) {
        List<EntCSVMode3> result = mstProductMapper.selectProductAttributeBySyouhinSysCodeList(syouhinSysCodeList, attributeCodeList);
        if (result == null) {
            result = new ArrayList<EntCSVMode3>();
        } else {
            BeanComparator bc = new BeanComparator("productSyouhinSysCode");
            Collections.sort(result, bc);
        }
        return result;
    }

    @Override
    public List<EntCSVMode1> selectSyouhinInfoExportCSVMode1SingleTable(List<String> syouhinSysCodeList) {
        List<EntCSVMode1> result = mstProductMapper.selectSyouhinInfoExportCSVMode1SingleTable(syouhinSysCodeList);
        if (result == null) {
            result = new ArrayList<EntCSVMode1>();
        }
        return result;
    }

    @Override
    public List<EntMstAttribute> selectAttributeByCodes(List<String> attributeCodeList) {
        List<EntMstAttribute> result = mstAttributeMapper.selectAttributeByCodes(attributeCodeList);
        if (result == null) {
            result = new ArrayList<EntMstAttribute>();
        }
        return result;
    }

    @Override
    public void appendAttributeCode(List<EntCSVMode1> data, List<EntMstAttribute> attributeList) {
        for (EntCSVMode1 ent : data) {
            for (int i = 1; i <= attributeList.size(); i++) {
                try {
                    BeanUtils.setProperty(ent, "attributeCode" + i, Integer.parseInt(attributeList.get(i - 1).getAttributeCode()));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void appendAttributeData(List<EntCSVMode1> data, List<EntCSVMode3> attributeDataList) {
        for (EntCSVMode1 ent : data) {
            for (EntCSVMode3 attr : attributeDataList) {
                if (ent.getProductSyouhinSysCode() == attr.getProductSyouhinSysCode()) {
                    int index = getAttributeIndex(attr.getAttributeCode1(), ent);
                    if (index != -1) {
                        try {
                            BeanUtils.setProperty(ent, "attributevalues" + index, attr.getAttributeValues1());
                            BeanUtils.setProperty(ent, "attributeDisplay" + index, attr.getAttributeDisplay1());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
    }

    /**
     * Find index of attribute that contain search value.
     * @param attributeCode int
     * @param ent EntCSVMode1
     * @return int
     */
    private int getAttributeIndex(int attributeCode, EntCSVMode1 ent) {
        for (int i = 1; i <= Constant.PRODUCT_MAX_ATTRIBUTE_LENGTH; i++) {
            try {
                if (Integer.parseInt(BeanUtils.getProperty(ent, "attributeCode" + i)) == attributeCode) {
                    return i;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return -1;
    }

    @Override
    public List<EntCSVMode1> selectProductFitModelBySyouhinSysCodeList(List<String> syouhinSysCodeList) {
        List<EntCSVMode1> result = mstProductMapper.selectProductFitModelBySyouhinSysCodeList(syouhinSysCodeList);
        if (result == null) {
            result = new ArrayList<EntCSVMode1>();
        }
        return result;
    }

    @Override
    public List<EntCSVMode1> selectProductImageInfoBySyouhinSysCodeList(List<String> syouhinSysCodeList) {
        List<EntCSVMode1> result = mstProductMapper.selectProductImageInfoBySyouhinSysCodeList(syouhinSysCodeList);
        if (result == null) {
            result = new ArrayList<EntCSVMode1>();
        }
        return result;
    }

    @Override
    public List<EntCSVMode1> selectProductSelectInfoBySyouhinSysCodeList(List<String> syouhinSysCodeList) {
        List<EntCSVMode1> result = mstProductMapper.selectProductSelectInfoBySyouhinSysCodeList(syouhinSysCodeList);
        if (result == null) {
            result = new ArrayList<EntCSVMode1>();
        }
        return result;
    }

    @Override
    public List<EntCSVMode1> selectProductGuestInputInfoBySyouhinSysCodeList(List<String> syouhinSysCodeList) {
        List<EntCSVMode1> result = mstProductMapper.selectProductGuestInputInfoBySyouhinSysCodeList(syouhinSysCodeList);
        if (result == null) {
            result = new ArrayList<EntCSVMode1>();
        }
        return result;
    }

    @Override
    public List<EntCSVMode1> selectProductLinkInfoBySyouhinSysCodeList(List<String> syouhinSysCodeList) {
        List<EntCSVMode1> result = mstProductMapper.selectProductLinkInfoBySyouhinSysCodeList(syouhinSysCodeList);
        if (result == null) {
            result = new ArrayList<EntCSVMode1>();
        }
        return result;
    }

    @Override
    public List<EntCSVMode1> selectProductVideoInfoBySyouhinSysCodeList(List<String> syouhinSysCodeList) {
        List<EntCSVMode1> result = mstProductMapper.selectProductVideoInfoBySyouhinSysCodeList(syouhinSysCodeList);
        if (result == null) {
            result = new ArrayList<EntCSVMode1>();
        }
        return result;
    }

    @Override
    public void appendDataByFieldName(List<EntCSVMode1> mainList, List<EntCSVMode1> subList, String fieldName) {
        for (EntCSVMode1 mainEnt : mainList) {
            for (EntCSVMode1 subEnt : subList) {
                if (mainEnt.getProductSyouhinSysCode().equals(subEnt.getProductSyouhinSysCode())) {
                    // append value to current value
                    if (fieldName.equals("compatibleModel")) {
                        mainEnt.setCompatibleModel(mainEnt.getcompatibleModelNotNull() + subEnt.getcompatibleModelNotNull());
                    } else if (fieldName.equals("customerConfirmationItem")) {
                        mainEnt.setCustomersConfirmationItem(mainEnt.getCustomersConfirmationItemNotNull() + subEnt.getCustomersConfirmationItemNotNull());
                    } else if (fieldName.equals("link")) {
                        mainEnt.setLink(mainEnt.getLinkNotNull() + subEnt.getLinkNotNull());
                    } else if (fieldName.equals("video")) {
                        mainEnt.setAnimation(mainEnt.getAnimationNotNull() + subEnt.getAnimationNotNull());
                    }
                }
            }
        }
    }

    @Override
    public void appendImageSelectData(List<EntCSVMode1> mainList, List<EntCSVMode1> subList, String fieldName) {
        for (EntCSVMode1 ent : mainList) {
            int count = 1;
            for (int i = 0; i < subList.size(); i++) {
                if (!(ent.getProductSyouhinSysCode().equals(subList.get(i).getProductSyouhinSysCode()))) {
                    continue;
                }
                try {
                    if (fieldName.equals("image")) {
                        BeanUtils.setProperty(ent, "productImageThumbnailPath" + count,
                            BeanUtils.getProperty(subList.get(i), "productImageThumbnailPath1"));
                        BeanUtils.setProperty(ent, "productImageDetailPath" + count,
                            BeanUtils.getProperty(subList.get(i), "productImageDetailPath1"));
                        count++;
                    } else if (fieldName.equals("option")) {
                        BeanUtils.setProperty(ent, "selectCode" + count,
                            BeanUtils.getProperty(subList.get(i), "selectCode1"));
                        BeanUtils.setProperty(ent, "selectName" + count,
                            BeanUtils.getProperty(subList.get(i), "selectName1"));
                        count++;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public List<EntMstProduct> selectproductListByPrdIdList(List<Long> prdIdList) {
        List<EntMstProduct> productList = null;
        try {
            productList = mstProductMapper.selectproductListByPrdIdList(prdIdList);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return productList;
    }

    @Override
    public int selectCountProductSyouhinNotMaintenacedByPrdIdList(List<Long> prdIdList) {
        int count = 0;
        try {
            count = mstProductMapper.selectCountProductSyouhinNotMaintenacedByPrdIdList(prdIdList);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return count;
    }

    @Override
    public List<Long> selectproductSyouhinNotMaintenacedByPrdIdList(List<Long> prdIdList) {
        List<Long> listSyouhin = null;
        try {
            listSyouhin = mstProductMapper.selectproductSyouhinNotMaintenacedByPrdIdList(prdIdList);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return listSyouhin;
    }

    @Override
    public int selectCountProductSyouhinDisabledByPrdIdList(List<Long> prdIdList) {
        int countReturn = 0;
        try {
            countReturn = mstProductMapper.selectCountProductSyouhinDisabledByPrdIdList(prdIdList);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return countReturn;
    }

    @Override
    public List<String> selectAllProductsForExportCSV(EntMstProduct entMstProduct) {
        List<String> listProductId = new ArrayList<String>();
        try {
            //Get sortField sortDir of grid main
            /*if (StringUtils.isEmpty(entMstProduct.getSortField()) || StringUtils.isEmpty(entMstProduct.getSortDir())) {
                entMstProduct.setSortField(Constant.DEFAULT_SORT_FIELD_PRODUCT_MANAGE);
                entMstProduct.setSortDir(Constant.DEFAULT_SORT_DIR);
            }*/
            entMstProduct.setSortField("");
            entMstProduct.setSortDir("");
            if (entMstProduct.getDetailSearchFlag() == 2) {
                // Model details search
                MstProductSqlCondition sqlCondition = new MstProductSqlCondition(entMstProduct);
                listProductId = mstProductMapper.selectDetailSearchProductList(0, 0, entMstProduct, sqlCondition);
            } else {
                // Mode simple search.
                listProductId = mstProductMapper.selectProductIdList(0, 0, entMstProduct);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return listProductId;
    }

    @Override
    public List<Long> convertListStringToListLong(List<String> listString) {
        List<Long> listLong = new ArrayList<Long>();
        for (String string : listString) {
            if (StringUtils.isNumeric(string)) {
                listLong.add(Long.parseLong(string));
            }
        }
        return listLong;
    }

    @Override
    public int selectMaxMatterNo() {
        int matterNo = mstFactoryMatterMapper.selectMaxMatterNo();
        return matterNo;
    }

    @Override
    public List<Long> selectSyouhinSysCodeListByStatus(List<Long> syouhinSysCodeList, int statusType) {
        return mstProductMapper.selectSyouhinSysCodeListByStatus(syouhinSysCodeList, statusType);
    }

    @Override
    public List<EntMstSiire> selectListAllSiire() throws SQLException {
        List<EntMstSiire> list = mstSiireMapper.selectListAllSiire();
        return list;
    }

    @Override
    public List<EntTblFactoryProductNew> selectMatterDetailByProductSyouhinSysCode(Long syouhinSysCode) throws SQLException {
    	List<EntTblFactoryProductNew> listMatterInfo = tblFactoryProductMapper.selectMatterDetailByProductSyouhinSysCode(syouhinSysCode);
    	//Check null data
    	if (null == listMatterInfo) {
    		listMatterInfo = new ArrayList<EntTblFactoryProductNew>();
    	}
    	return listMatterInfo;
    }
}
