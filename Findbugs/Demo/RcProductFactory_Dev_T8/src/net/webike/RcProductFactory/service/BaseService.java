/************************************************************************
 * File Name ： BaseService.java
 * Author ： Long Vu
 * Version ： 1.0.0
 * Date Created ： 2014/01/23
 * Description ： Service to process transaction.
 ***********************************************************************/
package net.webike.RcProductFactory.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

/**
 * Service to process transaction.
 */
public class BaseService {
    @Autowired
    @Qualifier("transactionManager")
    private PlatformTransactionManager transactionManager;

    /************************************************************************
     * <b>Description:</b><br>
     * start transaction.
     *
     * @return      TransactionStatus
     * @author      Long Vu
     * @date        Jan 23, 2014
     ************************************************************************/
    protected TransactionStatus startTransaction() {
        TransactionStatus status = null;
        DefaultTransactionDefinition def = null;
        try {
            def = new DefaultTransactionDefinition();
            def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
            status = transactionManager.getTransaction(def);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return status;
    }

    /************************************************************************
     * <b>Description:</b><br>
     * commit transaction.
     *
     * @param       status transaction status.
     * @return      boolean
     * @author      Long Vu
     * @date        Jan 23, 2014
     ************************************************************************/
    protected boolean commit(TransactionStatus status) {
        boolean isSucess = false;
        try {
            transactionManager.commit(status);
        } catch (Exception e) {
            isSucess = false;
        }
        return isSucess;
    }

    /************************************************************************
     * <b>Description:</b><br>
     * roll back if any error occurs.
     *
     * @param       status transaction status.
     * @return      boolean
     * @author      Long Vu
     * @date        Jan 23, 2014
     ************************************************************************/
    protected boolean rollback(TransactionStatus status) {
        boolean isSucess = false;
        try {
            transactionManager.rollback(status);
        } catch (Exception e) {
            isSucess = false;
        }
        return isSucess;
    }
}
