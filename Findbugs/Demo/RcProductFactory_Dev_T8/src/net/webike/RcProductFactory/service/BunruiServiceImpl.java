/**
 * BunruikaisouImpl
 */
package net.webike.RcProductFactory.service;

import java.util.List;

import net.webike.RcProductFactory.entity.EntCategory;
import net.webike.RcProductFactory.entity.EntMstBunrui;
import net.webike.RcProductFactory.entity.EntMstBunruikaisou;
import net.webike.RcProductFactory.mapper.MstBunruiMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * @author Tran.Thanh
 * BunruiServiceImpl implements BunruiService
 */
@Service("BunruiServiceImpl")
@Transactional
public class BunruiServiceImpl implements BunruiService {
    @Autowired
    private MstBunruiMapper bunruiMapper;

	@Override
	public List<EntMstBunrui> selectListBunruiByOyaCode(EntMstBunruikaisou entBunruikaisou) {
		List<EntMstBunrui> listEntMstBunrui = null;
		listEntMstBunrui  = bunruiMapper.selectListBunruiByOyaCode(entBunruikaisou);
        return listEntMstBunrui;
	}

	@Override
	public List<EntCategory> selectCategory() {
		List<EntCategory> entCategory = null;
		entCategory = bunruiMapper.selectCategory();
		return entCategory;
	}

}
