/************************************************************************
 * File Name	： ModelService.java
 * Author		： Nguyen.Chuong
 * Version		： 1.0.0
 * Date Created	： 2014/01/23
 * Description	： Service to get model from database.
 *
 ***********************************************************************/
package net.webike.RcProductFactory.service;

import java.util.List;

import net.webike.RcProductFactory.action.form.FitModelCheckActionForm;
import net.webike.RcProductFactory.entity.EntMstModel;

/**
 * Service to get model from database.
 */
public interface ModelService {
    /************************************************************************
     * <b>Description:</b><br>
     *  select list model all and filter for checkFitModel page.
     *
     * @author      Nguyen.Chuong
     * @date        Jan 23, 2014
     * @param       entMstModel contain param
     * @param       pageSize pageSize
     * @param       pageNumber pageNumber
     * @return      List<EntMstModel>
     ************************************************************************/
    List<EntMstModel> selectListModel(int pageSize, int pageNumber, EntMstModel entMstModel);

    /************************************************************************
     * <b>Description:</b><br>
     *  select list model count.
     *
     * @author      Nguyen.Chuong
     * @date        Jan 23, 2014
     * @param       entMstModel contain param
     * @return      List<EntMstModel>
     ************************************************************************/
    int selectListModelCount(EntMstModel entMstModel);

    /************************************************************************
     * <b>Description:</b><br>
     *  select list model all and filter for checkFitModel page.
     *
     * @author      hoang.ho
     * @date        Feb 13, 2014
     * @param       fitModelCheckActionForm contain parameters
     *              1. autoCompleteField  field used for get data
     *              2. entMstModel information
     * @return      List<EntMstModel>
     ************************************************************************/
    List<String> selectModelFilterList(FitModelCheckActionForm fitModelCheckActionForm);

    /************************************************************************
     * <b>Description:</b><br>
     * get model all model list in DB.
     *
     * @author      Long Vu
     * @date        Feb 10, 2014
     * @return      List<EntMstModel>
     ************************************************************************/
    List<EntMstModel> selectAllListModel();
}
