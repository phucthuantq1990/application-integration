/**
 * AttributeServiceImpl
 */
package net.webike.RcProductFactory.service;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.webike.RcProductFactory.entity.EntMstAttribute;
import net.webike.RcProductFactory.entity.EntMstAttributeGroup;
import net.webike.RcProductFactory.mapper.MstAttributeGroupMapper;
import net.webike.RcProductFactory.mapper.MstAttributeMapper;
import net.webike.RcProductFactory.util.Constant;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * AttributeServiceImpl implements AttributeService.
 */
@Service("attributeServiceImpl")
public class AttributeServiceImpl implements AttributeService {
    @Autowired
    private MstAttributeMapper mstAttributeMapper;
    @Autowired
    private MstAttributeGroupMapper mstAttributeGroupMapper;

    private static final String ERROR_UPDATE = "errorUpdate";
    private static final String OTHER_ERROR = "other";
    private static final String ERROR_INSERT = "errorInsert";

    @Override
    public List<EntMstAttribute> selectListAttributeWithFilter(int limit
                                                         , int offset
                                                         , EntMstAttribute entAttribute) throws SQLException {
        //Get limit and offset to load data.
        offset = (offset - 1) * limit;
        List<EntMstAttribute> listAttribute = mstAttributeMapper.selectListAttributeWithFilter(limit
                                                                                             , offset
                                                                                             , entAttribute);
        return listAttribute;
    }

    @Override
    public EntMstAttribute selectAttributeByCode(Integer attributeCode) {
        EntMstAttribute attribute = mstAttributeMapper.selectAttributeByCode(attributeCode);
        return attribute;
    }

    @Override
    public int selectTotalRecordOfFilter(EntMstAttribute entAttribute) throws SQLException {
        int count = mstAttributeMapper.selectTotalRecordOfFilter(entAttribute);
        return count;
    }

    /* (non-Javadoc)
     * @see net.webike.RcProductFactory.service.AttributeService#checkAttrExistedName(net.webike.RcProductFactory.entity.EntMstAttribute, int)
     */
    @Override
    public boolean checkAttrExistedName(EntMstAttribute attribute, int insertMode) {
        boolean result = false;
        EntMstAttribute attributeObj = null;
        try {
            if (insertMode == 1) {
                attributeObj = mstAttributeMapper.selectAttributeByName(attribute.getAttributeName());
                if (attributeObj != null) {
                    result = true;
                } else {
                    result = false;
                }
            } else {
                attributeObj = mstAttributeMapper.selectAttributeWithouCode(attribute);
                if (attributeObj != null) {
                    result = true;
                } else {
                    result = false;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public List<EntMstAttributeGroup> selectAttrGroupByCode(Integer attributeCode) {
        List<EntMstAttributeGroup> lstAttrGroup = mstAttributeGroupMapper.selectListAttrGroupByCode(attributeCode);

        return lstAttrGroup;
    }

    @Override
    @Transactional
    public void processUpdateDataAttrGroup(EntMstAttribute attribute, String deletedCodeList, List<EntMstAttributeGroup> attrGroupInsertdList,
    List<EntMstAttributeGroup> attrGroupUpdatedList, String userId) {
        Map<String, String> deleteCode;
        int isContinue = 0;
        try {
            attribute.setLoginUserId(userId);
            isContinue = mstAttributeMapper.updateAttributeByCode(attribute);
            if (isContinue > 0) {
                // delete data
                if (!StringUtils.isEmpty(deletedCodeList)) {
                    deleteCode = new HashMap<String, String>();
                    deleteCode.put("attributeCode", attribute.getAttributeCode());
                    deleteCode.put("attributeGroupCode", deletedCodeList);
                    mstAttributeGroupMapper.deleteAttrGroupByCode(deleteCode);
                }
                // update data
                for (EntMstAttributeGroup obj : attrGroupUpdatedList) {
                    obj.setLoginUserId(userId);
                    isContinue = mstAttributeGroupMapper.updateAttrGroupByCode(obj);
                    if (isContinue <= 0) {
                        break;
                    }
                }
                if (isContinue > 0) {
                    // insert data
                    for (EntMstAttributeGroup obj : attrGroupInsertdList) {
                        obj.setLoginUserId(userId);
                        obj.setAttributeCode(Integer.parseInt(attribute.getAttributeCode()));
                        isContinue = mstAttributeGroupMapper.insertAttrGroupByCode(obj);
                        if (isContinue <= 0) {
                            break;
                        }
                    }
                }
            }
            if (isContinue <= 0) {
                throw new Exception(ERROR_UPDATE);
            }
        } catch (Exception e) {
            String msgError = e.getLocalizedMessage();
            if (StringUtils.equals(ERROR_UPDATE, msgError)) {
                throw new RuntimeException("update");
            } else {
                throw new RuntimeException(OTHER_ERROR);
            }
        }
    }

    @Override
    @Transactional
    public boolean insertNewAttribute(String userId, EntMstAttribute attribute, List<EntMstAttributeGroup> attributeGroupList) {
        Map<String, Integer> map = new HashMap<String, Integer>();
        Integer isContinue = 0;
        String attributeCode;
        boolean result = false;
        try {
            map.put("returnAttributeCode", 0);
            attribute.setLoginUserId(userId);
            //Set lengthLimit when Type = flag;
            if (attribute.getAttributeType().equals(Constant.TYPE_FLAG)) {
                attribute.setAttributeLengthLimit("0");
            }

            isContinue = mstAttributeMapper.insertNewAttribute(attribute, map);

            //Validate continue
            if (isContinue > 0) {
                // get key of new attribue
                attributeCode = String.valueOf(map.get("returnAttributeCode"));
                // insert attribute group
                if (attributeGroupList != null) {
                    for (EntMstAttributeGroup attrGroup : attributeGroupList) {
                        attrGroup.setLoginUserId(userId);
                        attrGroup.setAttributeCode(Integer.valueOf(attributeCode));
                        isContinue = mstAttributeGroupMapper.insertNewAttributeGroup(attrGroup);
                        if (isContinue <= 0) {
                            // insert fail then roll back
                            throw new Exception(ERROR_INSERT);
                        }
                    }
                }

                result = true;
            } else {
                // insert fail then roll back
                throw new Exception(ERROR_INSERT);
            }
        } catch (Exception e) {
            if (StringUtils.equals(ERROR_INSERT, e.getLocalizedMessage())) {
                throw new RuntimeException(ERROR_INSERT);
            } else {
                throw new RuntimeException(OTHER_ERROR);
            }
        }
        return result;
    }
}
