/************************************************************************
 * File Name    ： AttributeService.java
 * Author       ： Long Vu
 * Version      ： 1.0.0
 * Date Created ： 2014/01/23
 * Description  ： Service to process with fit model.
 ***********************************************************************/
package net.webike.RcProductFactory.service;

import java.sql.SQLException;
import java.util.List;

import net.webike.RcProductFactory.entity.EntMstFactoryMatter;
import net.webike.RcProductFactory.entity.EntMstMaker;
import net.webike.RcProductFactory.entity.EntTblFactoryProductFitModel;
import net.webike.RcProductFactory.entity.EntTblFactoryProductNew;

/**
 * Service to process with fit model.
 */
public interface FitModelService {

    /*BOE [#] Nguyen.Chuong 2014/01/24: add query to get list fit model in fitModelCheck page.*/
    /************************************************************************
     * <b>Description:</b><br>
     *  select list productFitModel by productCode.
     * @author      Nguyen.Chuong
     * @date        Jan 23, 2014
     * @param       entproduct content filter and productCode
     * @return      List<EntTblFactoryProductModel>
     ************************************************************************/
    List<EntTblFactoryProductFitModel> selectListFitModelByProductCode(EntTblFactoryProductFitModel entproduct);
    /*BOE [#] Nguyen.Chuong 2014/01/24 */

    /************************************************************************
     * <b>Description:</b><br>
     * get list fit model with max model sort.
     *
     * @author      Long Vu
     * @date        Jan 23, 2014
     * @param       prdIdList list product id.
     * @return      boolean
     ************************************************************************/
    List<EntTblFactoryProductFitModel> selectListFitModelWithMaxModelSort(List<Long> prdIdList);

    /************************************************************************
     * <b>Description:</b><br>
     * process udpate, insert and delete for single product.
     *
     * @author      Long Vu
     * @date        Jan 23, 2014
     * @param       listFitModel list fit model.
     * @param       product object product
     * @param       factoryMatter object matter
     * @param       user login user id
     * @return      boolean
     ************************************************************************/
    boolean processDataFitModelBySingleProduct(List<EntTblFactoryProductFitModel> listFitModel, List<EntTblFactoryProductNew> product,
    EntMstFactoryMatter factoryMatter, String user);

    /************************************************************************
     * <b>Description:</b><br>
     * process udpate, insert and delete for multi product.
     *
     * @author      Long Vu
     * @date        Jan 24, 2014
     * @param       listFitModel list fit model.
     * @param       listProduct list product
     * @param       factoryMatter object matter
     * @param       user login user id
     * @return      boolean
     ************************************************************************/
    boolean processDataFitModelByMultiProduct(List<EntTblFactoryProductFitModel> listFitModel, List<EntTblFactoryProductNew> listProduct,
    EntMstFactoryMatter factoryMatter, String user);

    /************************************************************************
     * <b>Description:</b><br>
     * process insert fit model for multi product.
     *
     * @author      Long Vu
     * @date        Jan 24, 2014
     * @param       listFitModel list fit model.
     * @param       listProduct list product
     * @param       factoryMatter object matter
     * @param       user login user id
     * @return      boolean
     ************************************************************************/
    boolean processInserFitModelByMultiProduct(List<EntTblFactoryProductFitModel> listFitModel, List<EntTblFactoryProductNew> listProduct,
    EntMstFactoryMatter factoryMatter, String user);

    /************************************************************************
     * <b>Description:</b><br>
     * get list maker.
     *
     * @author      Long Vu
     * @date        Feb 10, 2014
     * @return      List<EntMstMaker>
     ************************************************************************/
    List<EntMstMaker> selectMakerList();

    /************************************************************************
     * <b>Description:</b><br>
     *  select list productFitModel by productId.
     * @author      Long Vu
     * @date        Feb 12, 2014
     * @param       productId product Id
     * @return      List<EntTblFactoryProductModel>
     ************************************************************************/
    List<EntTblFactoryProductFitModel> selectListFitModelByProductId(Long productId);

    /************************************************************************
     * <b>Description:</b><br>
     * update product check flg if there is no error for selected product.
     *
     * @param       product productId
     * @author      Long Vu
     * @date        Mar 11, 2014
     ************************************************************************/
    void updateProductCheckFlgByProductId(EntTblFactoryProductNew product);

    /************************************************************************
     * <b>Description:</b><br>
     *  select list product has fit model same data with entModel.
     *  Duplicate value is same make, model and style
     *
     * @author		Luong.Dai
     * @date		Aug 22, 2014
     * @param 		lstProduct		List product to check
     * @param 		entModel		model to check
     * @return		List<Long>		List product has duplicate data
     * @throws 		SQLException	exception when excute query
     ************************************************************************/
    List<Long> selectListProductByFitMode(List<EntTblFactoryProductNew> lstProduct
    									, EntTblFactoryProductFitModel entModel) throws SQLException;
}
