package net.webike.RcProductFactory.service;

import java.sql.SQLException;
import java.util.Collections;
import java.util.List;

import net.webike.RcProductFactory.entity.EntMstSyouhinSelect;
import net.webike.RcProductFactory.mapper.MstSyouhinSelectMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author nguyen.hieu
 *
 */
@Service("syouhinSelectService")
public class SyouhinSelectServiceImpl implements SyouhinSelectService {

    @Autowired
    private MstSyouhinSelectMapper syouhinSelectMapper;

    /**
     * Get syouhin select list.
     * @author      nguyen.hieu
     * @since       2014/02/17
     * @return		Syouhin select list.
	 * @throws		SQLException SQLException.
     */
    public List<EntMstSyouhinSelect> selectSyouhinSelect() throws SQLException {
    	List<EntMstSyouhinSelect> syouhinSelectList = syouhinSelectMapper.selectSyouhinSelectList();
    	if (syouhinSelectList != null) {
    		return syouhinSelectList;
    	}
    	return Collections.emptyList();
    }
}
