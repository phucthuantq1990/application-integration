/**
 * MainMenuServiceImpl
 */
package net.webike.RcProductFactory.service;

import java.util.List;

import net.webike.RcProductFactory.entity.EntMstFactoryUserTmp;
import net.webike.RcProductFactory.mapper.MstFactoryUserTmpMapper;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * @author Tran.Thanh
 * UserServiceImpl implements UserTmpService
 */
@Service("UserTmpServiceImpl")
@Transactional
public class UserTmpServiceImpl implements UserTmpService {
    @Autowired
    private MstFactoryUserTmpMapper userTmpMapper;

    @Override
    public EntMstFactoryUserTmp selectUserTmpById(String id) {
        // Check params id not empty
        if (StringUtils.isNotEmpty(id)) {
        	EntMstFactoryUserTmp user = userTmpMapper.selectUserTmpById(id);
            return user;
        }
        return null;
    }

	@Override
	public Boolean insertNewUserTmp(EntMstFactoryUserTmp ent) throws Exception {
		// Check entity has insert value not null
        if (null != ent) {
            try {
                userTmpMapper.insertNewUserTmp(ent);
            } catch (Exception e) {
                // Insert failed
            	return false;
            }
        }
        return true;
	}

	@Override
	public Boolean selectUserIdExistedOrNot(String userId) {
		// Check params userId not empty
        if (StringUtils.isNotEmpty(userId)) {
            String result = userTmpMapper.selectUserIdExistedOrNot(userId);
            // User not existed
            if (null == result) {
                return false;
            }
        }
        return true;
	}

	@Override
	public void deleteUserByUserId(String userId, String updatedUserId) {
		userTmpMapper.deleteUserByUserId(userId, updatedUserId);
	}

	@Override
	public void activeUserByUSerId(String userId, String updatedUserId) {
		userTmpMapper.activeUserByUserId(userId, updatedUserId);
	}

	@Override
	public void updateUserInfo(EntMstFactoryUserTmp entUserTmp) {
		userTmpMapper.updateUserInfo(entUserTmp);
	}

	@Override
	public List<EntMstFactoryUserTmp> selectFilterUser(EntMstFactoryUserTmp entUserTmp) {
		List<EntMstFactoryUserTmp> listFislterUser = userTmpMapper.selectFilterUser(entUserTmp);
		return listFislterUser;
	}

	@Override
	public EntMstFactoryUserTmp selectUserById(String id) {
		// Check params id not empty
        if (StringUtils.isNotEmpty(id)) {
            EntMstFactoryUserTmp user = userTmpMapper.selectUserById(id);
            return user;
        }
        return null;
	}

	@Override
	public List<EntMstFactoryUserTmp> selectAllUser() {
        List<EntMstFactoryUserTmp> listUser = userTmpMapper.selectAllUser();
        return listUser;
	}

	@Override
	public List<EntMstFactoryUserTmp> selectListAllUser() {
		List<EntMstFactoryUserTmp> listUser = userTmpMapper.selectListAllUser();
        return listUser;
	}

}
