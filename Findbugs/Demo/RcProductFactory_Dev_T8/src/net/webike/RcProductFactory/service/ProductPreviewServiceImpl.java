/************************************************************************
 * File Name    ： ProductPreview.java
 * Author       ： Nguyen.Chuong
 * Version      ： 1.0.0
 * Date Created ： 2014/06/11
 * Description  ： Service implement of productPreviewService.
 *
 ***********************************************************************/
package net.webike.RcProductFactory.service;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.webike.RcProductFactory.entity.EntTblFactoryCalibrationRequestIndividual;
import net.webike.RcProductFactory.entity.EntTblFactoryProductDescription;
import net.webike.RcProductFactory.entity.EntTblFactoryProductFitModel;
import net.webike.RcProductFactory.entity.EntTblFactoryProductGeneral;
import net.webike.RcProductFactory.entity.EntTblFactoryProductGuestInput;
import net.webike.RcProductFactory.entity.EntTblFactoryProductLink;
import net.webike.RcProductFactory.entity.EntTblFactoryProductNew;
import net.webike.RcProductFactory.entity.EntTblFactoryProductSelect;
import net.webike.RcProductFactory.entity.EntTblFactoryProductSupplier;
import net.webike.RcProductFactory.mapper.TblFactoryCalibrationRequestIndividualMapper;
import net.webike.RcProductFactory.mapper.TblFactoryProductDescriptionMapper;
import net.webike.RcProductFactory.mapper.TblFactoryProductGeneralMapper;
import net.webike.RcProductFactory.mapper.TblFactoryProductGuestInputMapper;
import net.webike.RcProductFactory.mapper.TblFactoryProductLinkMapper;
import net.webike.RcProductFactory.mapper.TblFactoryProductMapper;
import net.webike.RcProductFactory.mapper.TblFactoryProductModelMapper;
import net.webike.RcProductFactory.mapper.TblFactoryProductSelectMapper;
import net.webike.RcProductFactory.mapper.TblFactoryProductSupplierMapper;
import net.webike.RcProductFactory.util.Constant;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * ProductPreviewServiceImpl implements ProducPreviewtService.
 */
@Service("productPreviewServiceImpl")
public class ProductPreviewServiceImpl implements ProductPreviewService {

	/* BOE autowire Tran.Thanh */
	@Autowired
    private TblFactoryProductMapper tblFactoryProductMapper;

	@Autowired
    private TblFactoryProductGeneralMapper tblFactoryProductGeneralMapper;

	@Autowired
    private TblFactoryProductSupplierMapper tblFactoryProductSupplierMapper;

	@Autowired
    private TblFactoryProductDescriptionMapper tblFactoryProductDescriptionMapper;

//	@Autowired
//    private MstFactoryMatterMapper matterMapper;

//	@Autowired
//	private TblBrandConditionRuleMapper tblBrandConditionRuleMapper;
	/* EOE autowire Tran.Thanh */

	/* BOE autowire Luong.Dai */
	@Autowired
	private TblFactoryProductGuestInputMapper tblFactoryProductGuestInputMapper;
	@Autowired
	private TblFactoryProductLinkMapper tblFactoryProductLinkMapper;
	@Autowired
	private TblFactoryProductModelMapper tblFactoryProductModelMapper;
	@Autowired
	private TblFactoryProductSelectMapper tblFactoryProductSelectMapper;
	@Autowired
    private TblFactoryCalibrationRequestIndividualMapper tblFactoryCalibrationRequestIndividualMapper;
	/* EOE autowire Luong.Dai */

	/* BOE autowire Nguyen.Chuong */
	/* EOE autowire Nguyen.Chuong */

	/* BOE Function Tran.Thanh */
	/* Start Block 2*/
	@Override
	public Long selectMatterNoByProductId(Long productId) {
		Long matterNo = tblFactoryProductMapper.selectLongMatterNoByProductId(productId);
		return matterNo;
	}

	@Override
	public int countTotalProductInMatter(Long matterNo) {
		int totalProductInMatter = tblFactoryProductMapper.countTotalProductInMatter(matterNo);
		return totalProductInMatter;
	}

	@Override
	public int indexProductInMatter(Long productId, Long matterNo) {
		int indexProductInMatter = tblFactoryProductMapper.indexProductInMatter(productId, matterNo);
		return indexProductInMatter;
	}

	@Override
	public List<EntTblFactoryProductGeneral> selectListGroupByMatterNo(Long matterNo) {
		List<EntTblFactoryProductGeneral> listGroupInMatter = tblFactoryProductGeneralMapper.selectListGeneralByMatterNo(matterNo);
		return listGroupInMatter;
	}

	@Override
	public int indexGroupInMatter(Long productId, List<EntTblFactoryProductGeneral> listGroupInMatter) throws SQLException {
		EntTblFactoryProductGeneral entProductGeneral = tblFactoryProductGeneralMapper.selectProductFactoryProductGeneralByProductid(productId);
		if (entProductGeneral != null) {
		    //If groupCode of product is 0 then index of group is 0
		    if (entProductGeneral.getProductGroupCode().equals(0L)) {
		        return 0;
		    }
		    //Loop list group in matter and check if groupCode and brandCode of product match then return index + 1 is index group in matter.
			for (int i = 0; i < listGroupInMatter.size(); i++) {
				if (listGroupInMatter.get(i).getProductGroupCode().compareTo(entProductGeneral.getProductGroupCode()) == 0
				        && listGroupInMatter.get(i).getProductBrandCode().compareTo(entProductGeneral.getProductBrandCode()) == 0) {
					return i + 1;
				}
			}
		}
		//Final if not match any case return 0
		return 0;
	}

	@Override
	public EntTblFactoryProductGeneral selectProductGeneralByProductId(Long productId) throws SQLException {
		EntTblFactoryProductGeneral entProductGeneral = tblFactoryProductGeneralMapper.selectProductFactoryProductGeneralByProductid(productId);
		return entProductGeneral;
	}
	/* End Block 2*/
	/* Start Block 5 */
	@Override
	public List<EntTblFactoryProductSupplier> selectListSupplierByProductId(Long productId) throws SQLException {
		List<EntTblFactoryProductSupplier> list = tblFactoryProductSupplierMapper.selectListSupplierByProductId(productId);
	    return list;
	}
	/* End Block 5 */
	/* Start Block 10 */
	@Override
	public EntTblFactoryProductDescription selectProductDescriptionByProductId(Long productId) throws SQLException {
		EntTblFactoryProductDescription list = tblFactoryProductDescriptionMapper.selectProductDescriptionByProductId(productId);
	    return list;
	}
	/* End Block 10 */
	/* EOE Function Tran.Thanh */

	/* BOE Function Dai.Luong */
	@Override
	public List<EntTblFactoryProductGuestInput> selectListGuestInputOfProduct(Long productId) {
		 //List result
	    List<EntTblFactoryProductGuestInput> listResult = tblFactoryProductGuestInputMapper.selectListProductGuestInputByProductId(productId);
	    if (listResult == null) {
	    	listResult = new ArrayList<EntTblFactoryProductGuestInput>();
	    }
	    //Return list result
	    return listResult;
	}

	@Override
	public List<EntTblFactoryProductLink> selectLinkOfProduct(Long productId) {
	    String linkSort = "link_reason_name";
		List<EntTblFactoryProductLink> lstLink = tblFactoryProductLinkMapper.selectLinkByProductId(productId, linkSort);
		if (lstLink == null) {
			lstLink = new ArrayList<EntTblFactoryProductLink>();
		}
		return lstLink;
	}

	@Override
	public List<EntTblFactoryProductFitModel> selectListFitModelOfProduct(Long productId) {
		EntTblFactoryProductFitModel entSearch = new EntTblFactoryProductFitModel();
		//Set product to param search
		entSearch.setProductId(productId);
		entSearch.setSortField(Constant.SORT_FIELD_FIT_MODEL_MAKER);
		entSearch.setSortDir(Constant.DEFAULT_SORT_DIR);

		List<EntTblFactoryProductFitModel> lstFitModels = tblFactoryProductModelMapper.selectListFitModelWithMakerLogo(entSearch);
		if (lstFitModels == null) {
			lstFitModels = new ArrayList<EntTblFactoryProductFitModel>();
		}

		return lstFitModels;
	}

	@Override
	public List<EntTblFactoryProductSelect> selectListSelectCodeByProductId(Long productId) {
		EntTblFactoryProductSelect selectParam = new EntTblFactoryProductSelect();
		//Set value
		selectParam.setProductId(productId);
		selectParam.setLimitListSelect(Constant.LIMIT_SELECT);
		//Query
		List<EntTblFactoryProductSelect> lstSelect = tblFactoryProductSelectMapper.selectListSelectCodeByProductId(selectParam);
		//Check null result
		if (lstSelect == null) {
			lstSelect = new ArrayList<EntTblFactoryProductSelect>();
		}
		return lstSelect;
	}

	@Override
	public List<Long> selectListProductIdSameGroup(Long groupCode, Integer brandCode, Long matterNo) {
		//Select group of product
		if (groupCode == null || groupCode <= 0) {
			return null;
		}
		//Create search param
		EntTblFactoryProductGeneral product = new EntTblFactoryProductGeneral();
		product.setProductBrandCode(brandCode);
		product.setProductMatterNo(matterNo);
		product.setProductGroupCode(groupCode);

		//Get list product id
		List<Long> lstProductId = tblFactoryProductMapper.selectProductIdGroup(product);
		return lstProductId;
	}

	@Override
	public Map<Integer, Object> selectListSelectDisplayValue(List<EntTblFactoryProductSelect> lstSelect, List<Long> lstProductId) {
		Map<Integer, Object> mapSelect = new HashMap<Integer, Object>();

		for (EntTblFactoryProductSelect select : lstSelect) {
			//Get list display value of select
			List<EntTblFactoryProductSelect> lstDisplay =
					tblFactoryProductSelectMapper.selectProductSelectDisplayByListProductIdAndSelectCode(select.getSelectCode()
					                                                                                   , lstProductId);
			if (lstDisplay == null) {
				lstDisplay = new ArrayList<EntTblFactoryProductSelect>();
			}
			//Set display value of current product to top of combobox
			findDisplayValueOfCurrentProduct(select, lstDisplay);
			Map<String, Object> mapDisplayValue = new HashMap<String, Object>();
			//Map: select_name : list display value
			mapDisplayValue.put(select.getName(), lstDisplay);
			mapSelect.put(select.getSelectCode(), mapDisplayValue);
		}

		return mapSelect;
	}

	/************************************************************************
	 * <b>Description:</b><br>
	 *  Find display value of current product (in entity select) at list display.
	 *  Set sort value of current display is 0.
	 *
	 * @author		Luong.Dai
	 * @date		Jun 17, 2014
	 * @param 		select			Current select of product
	 * @param 		lstDisplay		List display of group
	 ************************************************************************/
	private void findDisplayValueOfCurrentProduct(EntTblFactoryProductSelect select,
												List<EntTblFactoryProductSelect> lstDisplay) {
		for (EntTblFactoryProductSelect item : lstDisplay) {
			if (select.getProductSelectDisplay().equals(item.getProductSelectDisplay())) {
				//Set sort value is 0
				item.setProductSelectSort(0);
			} else {
				//set sort value is 1
				item.setProductSelectSort(1);
			}
		}
	}

	@Override
	public Long findProductByListSelectedOption(List<EntTblFactoryProductSelect> lstSelect, List<Long> lstProductId) {
		for (Long productId : lstProductId) {
			List<EntTblFactoryProductSelect> lstSelectOfProduct =
						tblFactoryProductSelectMapper.selectProductSelectDisplayOfProductBySelectCode(productId, lstSelect);
	    	//Check select display value match with option choise by user
			boolean isMatchOption = checkOptionMatchWithClient(lstSelectOfProduct, lstSelect);
			if (isMatchOption) {
				return productId;
			}
		}
		//Cannot find product match with list select of user
		return null;
	}

	@Override
	public EntTblFactoryProductGeneral selectProductPriceByProductId(String productId) {
		return tblFactoryProductGeneralMapper.selectProductPriceByProductId(productId);
	}
	/* EOE Function Dai.Luong */

	/************************************************************************
	 * <b>Description:</b><br>
	 *  Check list option of product match with list option choise by user.
	 *
	 * @author		Luong.Dai
	 * @date		Jun 16, 2014
	 * @param 		lstSelectOfProduct	List select of product
	 * @param 		lstSelectOfUser		List select choise by user
	 * @return		boolean				true if match
	 ************************************************************************/
	private boolean checkOptionMatchWithClient(List<EntTblFactoryProductSelect> lstSelectOfProduct,
											List<EntTblFactoryProductSelect> lstSelectOfUser) {
		if (lstSelectOfUser.size() != lstSelectOfProduct.size()) {
			//Check size of list, if different return false
			return false;
		}
		for (EntTblFactoryProductSelect selectOfUser : lstSelectOfUser) {
			for (EntTblFactoryProductSelect selectOfProduct : lstSelectOfProduct) {
				//Check if same select code but different select display => return false
				if (selectOfUser.getSelectCode().equals(selectOfProduct.getSelectCode())) {
					if (!selectOfUser.getProductSelectDisplay().equals(selectOfProduct.getProductSelectDisplay())) {
						return false;
					}
				}
			}
		}
		return true;
	}

	/* BOE Function Nguyen.Chuong */
	@Override
    public Long getNextOrPreviousProductId(Long productId, Long matterNo, String changeMode, String orderBy) {
	    //If changeMode is empty return -1 => error.
        if (StringUtils.isEmpty(changeMode)) {
            return -1L;
        }
        //Mode all: move in all products of matter.
        if (changeMode.equals("all")) {
            return tblFactoryProductMapper.selectOneNextOrPreProductInMatter(productId, matterNo, orderBy);
        }
        //Mode product not in group: move in all product having group code = 0
        if (changeMode.equals("productNotInGroup")) {
            return tblFactoryProductMapper.selectOneNextOrPreProductByGroup(productId, matterNo, false, orderBy);
        }
        //Mode product in group code: move in all products have group code > 0, each group code and brand code is one products.
        if (changeMode.equals("productInGroup")) {
            return tblFactoryProductMapper.selectOneNextOrPreProductByGroup(productId, matterNo, true, orderBy);
        }
        //Invalid mode.
        return -1L;
    }
	/* EOE Function Nguyen.Chuong */

	/* BOE Function @rcv!dau.phuong */
	/************************************************************************
     * <b>Description:</b><br>
     * Select list calibration request individual from rc_product_factory.tbl_factory_calibration_request_individual.
     *
     * @author      Dau.Phuong
     * @date        July 16, 2014
     * @param       ent            EntTblFactoryCalibrationRequestIndividual
     * @param       productId      product id
     * @return      List<EntTblFactoryCalibrationRequestIndividual>
     ************************************************************************/
    @Override
    public List<EntTblFactoryCalibrationRequestIndividual> selectCalibrationRequestIndividual(EntTblFactoryCalibrationRequestIndividual ent, Long productId) {
        Long matterNo = tblFactoryProductMapper.selectLongMatterNoByProductId(productId);
        List<EntTblFactoryCalibrationRequestIndividual> lstSelect = null;
        if (null != matterNo) {
          //Query
          lstSelect = tblFactoryCalibrationRequestIndividualMapper.selectCalibrationRequestIndividual(ent, matterNo);
        }
        //Check null result
        if (lstSelect == null) {
            lstSelect = new ArrayList<EntTblFactoryCalibrationRequestIndividual>();
        }
        return lstSelect;
    }

    /************************************************************************
     * <b>Description:</b><br>
     *  select calibration type from mst_factory_calibration_type.
     *
     * @author      Dau.Phuong
     * @date        July 16, 2014
     * @return      List<EntTblFactoryCalibrationRequestIndividual>
     ************************************************************************/
    @Override
    public List<EntTblFactoryCalibrationRequestIndividual> selectCalibrationType() {
        List<EntTblFactoryCalibrationRequestIndividual> lstSelect = tblFactoryCalibrationRequestIndividualMapper.selectCalibrationType();
        //Check null result
        if (lstSelect == null) {
            lstSelect = new ArrayList<EntTblFactoryCalibrationRequestIndividual>();
        }
        return lstSelect;
    }

    /************************************************************************
     * <b>Description:</b><br>
     *  select status check when preview product
     *
     * @author      Dau.Phuong
     * @date        July 17, 2014
     * @param       productId           Product id
     * @return      EntTblFactoryProductNew
     ************************************************************************/
    @Override
    public EntTblFactoryProductNew selectStatusCheckPreviewProduct(Long productId) {
        EntTblFactoryProductNew result = new EntTblFactoryProductNew();
        if (productId != null) {
            EntTblFactoryProductNew user = tblFactoryProductMapper.checkExistedRequestUser(productId);
            if (user != null) {
                result = tblFactoryProductMapper.selectStatusCheckPreviewProduct(user);
            }
        }
        return result;
    }
 
    /************************************************************************
     * <b>Description:</b><br>
     *  update status check when preview product
     *
     * @author      Dau.Phuong
     * @date        July 17, 2014
     * @param       param           EntTblFactoryProductNew
     ************************************************************************/
    @Override
    public void updateStatusPopUpCheck(EntTblFactoryProductNew param) {
        try {
            if (param != null) {
                tblFactoryProductMapper.updateStatusPopUpCheck(param);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /************************************************************************
     * <b>Description:</b><br>
     *  Change status of request
     *
     * @author      Dau.Phuong
     * @date        July 18, 2014
     * @param       param           EntTblFactoryCalibrationRequestIndividual
     ************************************************************************/
    @Override
    public void changeCorrespondRequest(EntTblFactoryCalibrationRequestIndividual param) {
        try {
            if (param != null) {
                tblFactoryCalibrationRequestIndividualMapper.changeCorrespondRequest(param);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /************************************************************************
     * <b>Description:</b><br>
     *  update content of request.
     *
     * @author      Dau.Phuong
     * @date        July 21, 2014
     * @param       param           EntTblFactoryCalibrationRequestIndividual
     ************************************************************************/
    @Override
    public void updateContentRequest(EntTblFactoryCalibrationRequestIndividual param) {
        try {
            if (param != null) {
                tblFactoryCalibrationRequestIndividualMapper.updateContentRequest(param);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    /* EOE Function @rcv!dau.phuong */
    /************************************************************************
    *  count total product in matter using for validate duplicate  .
     *
     * @author      Luong.Luong
     * @date        Jul 15, 2014
     * @param       matterNo matter number
     * @param       productCode  String
     * @param       productEanCode  String
     * @return      int total product in matter using for validate duplicate 
     ************************************************************************/
    @Override
    public int countCheckInvalidDataInMatter(Long matterNo, String productCode,
            String productEanCode) {
        return tblFactoryProductMapper.countCheckInvalidDataInMatter(matterNo, productCode, productEanCode);
    }
    /************************************************************************
     * <b>Description:</b><br>
     * delete  calibration request individual
     *
     * @author      Luong.Tuong
     * @date        July 21, 2014
     * @param       ent EntTblFactoryCalibrationRequestIndividual
     * @return      boolean
     ************************************************************************/
    @Override
    public boolean deleteCalibrationRequestIndividual(EntTblFactoryCalibrationRequestIndividual ent)
    {
        boolean result = true;
        try {
            if (ent != null) {
                tblFactoryCalibrationRequestIndividualMapper.deleteCalibrationRequestIndividual(ent);
            } else {
                result = false;
            }
        } catch (SQLException e) {
            result = false;
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public EntTblFactoryProductNew countTotalProductCheckInMatter(Long matterNo) throws SQLException {
    	//Count from DB
    	return tblFactoryProductMapper.countTotalProductCheckInMatter(matterNo);
    }
}
