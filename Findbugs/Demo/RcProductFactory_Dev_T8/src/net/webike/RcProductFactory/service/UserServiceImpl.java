///**
// * MainMenuServiceImpl
// */
//package net.webike.RcProductFactory.service;
//
//import java.util.List;
//
//import net.webike.RcProductFactory.entity.EntMstFactoryUser;
//import net.webike.RcProductFactory.mapper.MstFactoryUserMapper;
//
//import org.apache.commons.lang.StringUtils;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//import org.springframework.transaction.annotation.Transactional;
//
//
///**
// * @author Nguyen.Chuong
// * MainMenuServiceImpl implements MainMenuService
// */
//@Service("UserServiceImpl")
//@Transactional
//public class UserServiceImpl implements UserService {
////    @Autowired
////    private MstFactoryUserMapper userMapper;
////
////    @Override
////    public List<EntMstFactoryUser> selectAllUser() {
////        List<EntMstFactoryUser> listUser = userMapper.selectAllUser();
////        return listUser;
////    }
////
////    @Override
////    public EntMstFactoryUser selectUserById(String id) {
////        // Check params id not empty
////        if (StringUtils.isNotEmpty(id)) {
////            EntMstFactoryUser user = userMapper.selectUserById(id);
////            return user;
////        }
////        return null;
////    }
////
////	@Override
////	public List<EntMstFactoryUser> selectFilterUser(EntMstFactoryUser entUser) {
////		List<EntMstFactoryUser> listFilterUser = userMapper.selectFilterUser(entUser);
////		return listFilterUser;
////	}
////
////	@Override
////	public void deleteUserByUserId(String userId, String kousinId) {
////		userMapper.deleteUserByUserId(userId, kousinId);
////	}
////
////	@Override
////	public void activeUserByUSerId(String userId, String kousinId) {
////		userMapper.activeUserByUserId(userId, kousinId);
////	}
////
////	@Override
////	public EntMstFactoryUser selectAllInfoUserById(String userId) {
////	    EntMstFactoryUser user = null;
////	    // Check params userId is not empty
////		if (StringUtils.isNotEmpty(userId)) {
////            // Get user from userId params
////		    user = userMapper.selectAllInfoUserById(userId);
////        }
////		return user;
////	}
////
////	@Override
////    public Boolean insertNewUser(EntMstFactoryUser ent) throws Exception {
////	    // Check entity has insert value not null
////        if (null != ent) {
////            int result;
////            try {
////                result = userMapper.insertNewUser(ent);
////            } catch (Exception e) {
////                // Insert failed
////                result = 0;
////            }
////            // result > 0: insert success
////            if (result > 0) {
////                return true;
////            }
////        }
////        return false;
////    }
////
////    @Override
////    public Boolean selectUserIdExistedOrNot(String userId) {
////        // Check params userId not empty
////        if (StringUtils.isNotEmpty(userId)) {
////            String result = userMapper.selectUserIdExistedOrNot(userId);
////            // User not existed
////            if (null == result) {
////                return false;
////            }
////        }
////        return true;
////    }
////
////	@Override
////	public void updateUserInfo(EntMstFactoryUser entUser) {
////		userMapper.updateUserInfo(entUser);
////	}
////
////	@Override
////	public List<EntMstFactoryUser> selectListAllUser() {
////		List<EntMstFactoryUser> listUser = userMapper.selectListAllUser();
////        return listUser;
////	}
//
//}
