/************************************************************************
 * File Name    ： AttributeService.java
 * Author       ： Long Vu
 * Version      ： 1.0.0
 * Date Created ： 2014/01/23
 * Description  ： Service to process with fit model.
 ***********************************************************************/
package net.webike.RcProductFactory.service;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import net.webike.RcProductFactory.entity.EntMstFactoryMatter;
import net.webike.RcProductFactory.entity.EntMstMaker;
import net.webike.RcProductFactory.entity.EntTblFactoryProductFitModel;
import net.webike.RcProductFactory.entity.EntTblFactoryProductNew;
import net.webike.RcProductFactory.mapper.MstFactoryMatterMapper;
import net.webike.RcProductFactory.mapper.MstMakerMapper;
import net.webike.RcProductFactory.mapper.TblFactoryProductMapper;
import net.webike.RcProductFactory.mapper.TblFactoryProductModelMapper;
import net.webike.RcProductFactory.util.Constant;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionStatus;

/**
 * Service to process with fit model.
 */
@Service("fitModelService")
public class FitModelServiceImpl extends BaseService implements FitModelService {

    @Autowired
    private TblFactoryProductModelMapper tblFactoryProductModelMapper;
    @Autowired
    private TblFactoryProductMapper factoryProductMapper;
    @Autowired
    private MstFactoryMatterMapper mstFactoryMatterMapper;
    @Autowired
    private MstMakerMapper mstMakerMapper;

    @Override
    public List<EntTblFactoryProductFitModel> selectListFitModelByProductCode(EntTblFactoryProductFitModel entproduct) {
        List<EntTblFactoryProductFitModel> listFitModel = tblFactoryProductModelMapper.selectListFitModelOfProduct(entproduct);
        return listFitModel;
    }

    @Override
    public List<EntTblFactoryProductFitModel> selectListFitModelWithMaxModelSort(List<Long> prdIdList) {
        List<EntTblFactoryProductFitModel> result = null;
        try {
            result = tblFactoryProductModelMapper.selectListFitModelWithMaxModelSort(prdIdList);
        } catch (Exception e) {
            e.getLocalizedMessage();
        }
        return result;
    }

    @Override
    public boolean processDataFitModelBySingleProduct(List<EntTblFactoryProductFitModel> listFitModel, List<EntTblFactoryProductNew> productList,
    EntMstFactoryMatter factoryMatter, String user) {
        boolean result = false;
        int isContinue = 0;
        List<Long> pl = new ArrayList<Long>();
        EntTblFactoryProductNew product = null;
        if (productList != null && productList.size() != 0) {
            product = productList.get(0);
        }
        // create transaction.
        TransactionStatus status = null;
        try {
            // start transaction.
            status =  this.startTransaction();
            // check product code is null or non numberic
            if (product != null) {
                // add product code to list
                pl.add(product.getProductId());
                // delete all fit model by product code
                tblFactoryProductModelMapper.deleteFitModel(pl);
                // check list fit model null or size = 0
                if (listFitModel != null && listFitModel.size() != 0) {
                    int i = 0;
                    for (EntTblFactoryProductFitModel obj : listFitModel) {
                        // add model sort for object insert
                        obj.setFitModelSort(i);
                        obj.setUpdateUserId(user);
                        // insert each fit model to DB
                        isContinue = tblFactoryProductModelMapper.insertToFitModel(obj);
                        // check if one insert fall then break
                        i++;
                        if (isContinue <= 0) {
                            break;
                        }
                    }
                    if (isContinue > 0) {
                        // update product error flg
                        product.setUpdatedUserId(user);
                        isContinue = factoryProductMapper.updateProductModelErrorFlg(product);
                        if (isContinue > 0) {
                            // get the error count model by matter no
                            int errorCountModel = mstFactoryMatterMapper.selectErrorCountModelByMatterId(factoryMatter.getMatterNo());
                            int errorNum = errorCountModel + factoryMatter.getMatterFactoryErrorCountModel();
                            if (errorNum < 0) {
                                factoryMatter.setMatterFactoryErrorCountModel(0);
                            } else {
                                factoryMatter.setMatterFactoryErrorCountModel(errorNum);
                            }
                            // update error count model in factory matter
                            factoryMatter.setKousinUserId(user);
                            isContinue = mstFactoryMatterMapper.updateMatterErrorCountModel(factoryMatter);
                        }
                    }
                } else {
                    // update data in product and matter table when fit model is empty
                    isContinue = updateDataWhenFitModelIsEmpty(pl, user, factoryMatter);
                }
                if (isContinue > 0) {
                    // check if product has model error then no update
                    if (product.getProductModelErrorFlg() == 0) {
                        isContinue = factoryProductMapper.updateProductModelCheckFlg(product.getProductId());
                    }
                }
            }
        } catch (Exception e) {
            this.rollback(status);
        }
        if (isContinue > 0) {
            this.commit(status);
            result = true;
        } else {
            this.rollback(status);
        }
        return result;
    }

    @Override
    public boolean processDataFitModelByMultiProduct(List<EntTblFactoryProductFitModel> listFitModel, List<EntTblFactoryProductNew> listProduct,
    EntMstFactoryMatter factoryMatter, String user) {
        boolean result = false;
        int isContinue = 0;
        List<Long> pl = new ArrayList<Long>();
        // add product id to list
        if (listProduct != null) {
            for (EntTblFactoryProductNew prd : listProduct) {
                pl.add(prd.getProductId());
            }
        }
        // create transaction.
        TransactionStatus status = null;
        try {
        	//Count the change of product in list to change product error count of matter.
        	//int countProductErrorChange = 0;
            // start transaction
            status =  this.startTransaction();
            // check product id list id null or not
            if (pl != null && pl.size() != 0) {
                // delete all fit model by product code
                tblFactoryProductModelMapper.deleteFitModel(pl);
                // check product list is null or not
                if (listFitModel != null && listFitModel.size() != 0) {
                    if (listProduct != null && listProduct.size() != 0) {
                    	/*//Check list fitModel to get model_error_flg.
                    	int modelErrorFlg;
                    	//Fit model match with master.
                    	if (checkListFitModelMatchMaster(listFitModel)) {
                    		modelErrorFlg = 0;
                    	} else {
                    		//Fit model not match with master.
                    		modelErrorFlg = 1;
                    	}*/
                    	//Count the change of product in list to change product error count of matter.
                        for (EntTblFactoryProductNew prd : listProduct) {
                            // check list fit model null or size = 0
                            int i = 0;
                            //Count the change of product in list to change product error count of matter.
                            /*If modelErrorFlg > productModelErrorFlg => error flg change from 0 to 1 => increase the count error product
                              If modelErrorFlg < productModelErrorFlg => error flg change from 1 to 0 => decrease the count error product
                              If modelErrorFlg = productModelErrorFlg => error flg not change => the count error product not change*/
                            /*if (modelErrorFlg > prd.getProductModelErrorFlg()) {
                            	countProductErrorChange++;
                            } else if (modelErrorFlg < prd.getProductModelErrorFlg()) {
                            	countProductErrorChange--;
                            }*/
                            List<EntTblFactoryProductFitModel> listFitModelToInsert = new ArrayList<EntTblFactoryProductFitModel>(); 
                            for (EntTblFactoryProductFitModel obj : listFitModel) {
                                // add model sort for object insert
                                obj.setFitModelSort(i);
                                obj.setUpdateUserId(user);
                                obj.setProductId(prd.getProductId());
                                // BOE @rcv! Nguyen.Chuong Aug 15, 2014 : add insert fitModel to multi row.
                                //Add obj fitModel to insert DB
                                listFitModelToInsert.add(obj);
                                // insert each fit model to DB
//                                isContinue = tblFactoryProductModelMapper.insertToFitModel(obj);
                                // check if one insert fall then break
                                i++;
//                                if (isContinue <= 0) {
//                                    break;
//                                }
                            }
                            //Check size of list fitModel.
                            if (listFitModelToInsert.size() > 0) {
                                //For to loop sub list fitModel to insert DB: each time insert 1000 rows.
                            	for (int k = 0; k <= listFitModelToInsert.size() / Constant.INT_1000; k++) {
                                    int from = k * Constant.INT_1000;
                                    int to = (k + 1) * Constant.INT_1000;
                                    if (to > listFitModelToInsert.size()) {
                                        to = listFitModelToInsert.size();
                                    }
                                    //Insert to DB.
                                    isContinue = tblFactoryProductModelMapper.insertFitModelMultiRow(listFitModelToInsert.subList(from, to));
                                    //If insert fail then break
                                    if (isContinue <= 0) {
                                        break;
                                    }
                            	}
	                            if (isContinue > 0) {
	                                // update product error flg
	                                prd.setUpdatedUserId(user);
	                                /*prd.setProductModelErrorFlg(modelErrorFlg);*/
//	                                isContinue = factoryProductMapper.updateProductModelErrorFlg(prd);
	                            }
                            // EOE @rcv!Nguyen.Chuong Aug 15, 2014 : add insert fitModel to multi row.
                            }
                        }
                      //Update product error flg in list
                        //For to loop sub list fitModel to insert DB: each time insert 1000 rows.
                      	for (int k = 0; k <= listProduct.size() / Constant.INT_10000; k++) {
                              int from = k * Constant.INT_10000;
                              int to = (k + 1) * Constant.INT_10000;
                              if (to > listProduct.size()) {
                                  to = listProduct.size();
                              }
                              //Insert to DB.
                              isContinue = factoryProductMapper.updateMultiProductModelErrorFlg(listProduct.subList(from, to)
                                                                                                , listProduct.get(0).getProductModelErrorFlg()
                                                                                                , user);
                              //If insert fail then break
                              if (isContinue <= 0) {
                                  break;
                              }
                      	}
                    }
                    if (isContinue > 0) {
                        // get the error count model by matter no
                        int errorCountModel = mstFactoryMatterMapper.selectErrorCountModelByMatterId(factoryMatter.getMatterNo());
                        int errorNum = factoryMatter.getMatterFactoryErrorCountModel() + errorCountModel;
//                        int errorNum = errorCountModel + factoryMatter.getMatterFactoryErrorCountModel();
                        if (errorNum < 0) {
                            factoryMatter.setMatterFactoryErrorCountModel(0);
                        } else {
                            factoryMatter.setMatterFactoryErrorCountModel(errorNum);
                        }
                        // update error count model in factory matter
                        factoryMatter.setKousinUserId(user);
                        isContinue = mstFactoryMatterMapper.updateMatterErrorCountModel(factoryMatter);
                    }
                } else {
                    // update data in product and matter table when fit model is empty
                    isContinue = updateDataWhenFitModelIsEmpty(pl, user, factoryMatter);
                }
            }
            //BOE Nguyen.chuong 2014/03/10: remove process to update check flg for each product after process add model for multi product.
//            if (isContinue > 0) {
//                for (Integer i : pl) {
//                    isContinue = factoryProductMapper.updateProductModelCheckFlg(i);
//                    if (isContinue <= 0) {
//                        break;
//                    }
//                }
//            }
            //EOE Nguyen.chuong 2014/03/10
        } catch (RuntimeException e) {
            this.rollback(status);
            throw e;
        } catch (Exception e) {
            this.rollback(status);
        }
        if (isContinue > 0) {
            result = true;
            this.commit(status);
        } else {
            this.rollback(status);
        }
        return result;
    }

    @Override
    public boolean processInserFitModelByMultiProduct(List<EntTblFactoryProductFitModel> listFitModel
    													, List<EntTblFactoryProductNew> listProduct
    													, EntMstFactoryMatter factoryMatter
    													, String user) {
        boolean result = false;
        int isContinue = 0;
        // create transaction
        TransactionStatus status = null;
        try {
            // start transaction
            status =  this.startTransaction();
            if (listProduct != null && listProduct.size() != 0) {
                if (listFitModel != null && listFitModel.size() != 0) {
                    // get first fit model object
                    EntTblFactoryProductFitModel fitModel = listFitModel.get(0);
                    /* BOE @rcv! Luong.Dai 2014/08/22: update multi row to DB */
                    //Insert multi fit model to DB
                    isContinue = tblFactoryProductModelMapper.insertMultiFitModel(listProduct, fitModel, user);
                    if (isContinue >= 0) {
                    	//Update list product has model error flag = 1
                    	List<Long> lstProductErrorModel = new ArrayList<Long>();
                    	List<Long> lstProductNoErrorModel = new ArrayList<Long>();
                    	//Get list modelErrorFlag is 1 or 0
                    	for (EntTblFactoryProductNew product : listProduct) {
                    		if (product.getProductModelErrorFlg() == 0) {
                    			lstProductNoErrorModel.add(product.getProductId());
                    		} else {
                    			lstProductErrorModel.add(product.getProductId());
                    		}
                    	}
                    	//Update product model error flag to DB
                    	if (isContinue >= 0 && lstProductErrorModel.size() > 0) {
                    		isContinue = factoryProductMapper.updateMultiProductModelErrorFlgByListId(lstProductErrorModel, 1, user);
                    	}
                    	if (isContinue >= 0 && lstProductNoErrorModel.size() > 0) {
                    		isContinue = factoryProductMapper.updateMultiProductModelErrorFlgByListId(lstProductNoErrorModel, 0, user);
                    	}
                    }
                    if (isContinue > 0) {
                        if (factoryMatter.getMatterFactoryErrorCountModel() != 0) {
                            // get the error count model by matter no
                            int errorCountModel = mstFactoryMatterMapper.selectErrorCountModelByMatterId(factoryMatter.getMatterNo());
                            int errorNum = errorCountModel + factoryMatter.getMatterFactoryErrorCountModel();
                            if (errorNum < 0) {
                                factoryMatter.setMatterFactoryErrorCountModel(0);
                            } else {
                                factoryMatter.setMatterFactoryErrorCountModel(errorNum);
                            }
                            // update error count model in factory matter
                            factoryMatter.setKousinUserId(user);
                            isContinue = mstFactoryMatterMapper.updateMatterErrorCountModel(factoryMatter);
                        }
                    }
                }
            }
        } catch (Exception e) {
            this.rollback(status);
        }
        if (isContinue > 0) {
            result = true;
            this.commit(status);
        } else {
            this.rollback(status);
        }
        return result;
    }

    @Override
    public List<EntMstMaker> selectMakerList() {
        List<EntMstMaker> result = null;
        try {
            result = mstMakerMapper.selectMakerList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public List<EntTblFactoryProductFitModel> selectListFitModelByProductId(Long productId) {
        List<EntTblFactoryProductFitModel> result = null;
        try {
            result = tblFactoryProductModelMapper.selectListFitModelByProductId(productId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public void updateProductCheckFlgByProductId(EntTblFactoryProductNew prd) {
        EntTblFactoryProductNew product = null;
        try {
            // get product by product id to check data in DB
            product = factoryProductMapper.selectProductByProductId(prd.getProductId());
            if (product != null) {
                // check if product is checked or not
                // 1: checked, 0: not checked
                if (product.getProductModelCheckFlg() != 1) {
                    // check product has error or not
                    // 1: error, 0: no error
                    if (product.getProductModelErrorFlg() != 1) {
                        // set login user id to object
                        product.setUpdatedUserId(prd.getUpdatedUserId());
                        // set product model check flg to 1
                        product.setProductModelCheckFlg(1);
                        // update product model check flg
                        factoryProductMapper.updateProductCheckFlag(product);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /************************************************************************
     * <b>Description:</b><br>
     * update data of product and matter when fit model list is empty.
     *
     * @author      Long Vu
     * @date        Feb 13, 2014
     * @param       pl list list product id.
     * @param       user user id
     * @param       factoryMatter object matter
     * @return      int
     ************************************************************************/
    private int updateDataWhenFitModelIsEmpty(List<Long> pl, String user, EntMstFactoryMatter factoryMatter) {
        // get list product with model error flg by list product id
        List<EntTblFactoryProductNew> prdList = factoryProductMapper.selectProductListByProductIdList(pl);
        // set default param is 1 (mean process is ok)
        int isContinue = 1;
        int i = 0;
        for (EntTblFactoryProductNew obj: prdList) {
            // check if model error flg of product is 1
            // if 1 then set it to 0 then update data in product
            if (obj.getProductModelErrorFlg() == 1) {
                i++;
                obj.setProductModelErrorFlg(0);
                obj.setUpdatedUserId(user);
                isContinue = factoryProductMapper.updateProductModelErrorFlg(obj);
            }
        }
        // if the i > 0, mean the above process is happen
        // then update data in matter
        if (i > 0) {
            if (isContinue > 0) {
                // get the error count model by matter no
                int errorCountModel = mstFactoryMatterMapper.selectErrorCountModelByMatterId(factoryMatter.getMatterNo());
                int errorNum = errorCountModel - i;
                if (errorNum >= 0) {
                    // update data in matter
                    factoryMatter.setMatterFactoryErrorCountModel(errorNum);
                    factoryMatter.setKousinUserId(user);
                    isContinue = mstFactoryMatterMapper.updateMatterErrorCountModel(factoryMatter);
                }
            }
        }
        return isContinue;
    }
    @Override
    public List<Long> selectListProductByFitMode(List<EntTblFactoryProductNew> lstProduct,
    											EntTblFactoryProductFitModel entModel) throws SQLException {
    	return tblFactoryProductModelMapper.selectListProductIdByFitModelData(lstProduct, entModel);
    }
//    /************************************************************************
//     * <b>Description:</b><br>
//     * update data of product and matter when fit model list is empty.
//     *
//     * @author      Nguyen.Chuong
//     * @date        2014/08/19
//     * @param       listFitModel List<EntTblFactoryProductFitModel> to check.
//     * @return      boolean: true => all fitModel match with master, false => at least one fitModel not match with master.
//     ************************************************************************/
//    private boolean checkListFitModelMatchMaster(List<EntTblFactoryProductFitModel> listFitModel) {
//    	//Get list master maker and model from DB.
//        List<EntMstMaker> listMstMaker = mstMakerMapper.selectMakerList();
//        List<EntMstModel> listMstModel = mstModelmMapper.selectAllListModel();
//        //Loop list fitModel to check match with master.
//    	for (EntTblFactoryProductFitModel fitModelEnt : listFitModel) {
//    		EntMstMaker tmpMakerEnt = new EntMstMaker();
//    		tmpMakerEnt.setMakerName(fitModelEnt.getFitModelMaker());
//    		EntMstModel tmpModelEnt = new EntMstModel();
//    		tmpModelEnt.setModelName(fitModelEnt.getFitModelModel());
//    		//If maker or model not match with master then return list fitModel not match.
//    		if (!listMstMaker.contains(tmpMakerEnt) || !listMstModel.contains(tmpModelEnt)) {
//    			return false;
//    		}
//    	}
//    	//All maker, model in list match with master.
//    	return true;
//    }
}
