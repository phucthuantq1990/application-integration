/************************************************************************
 * file name	： ProductManageService.java
 * author		： Le.Dinh
 * version		： 1.0.0
 * date created	： 11 Feb 2014
 * date updated	： 11 Feb 2014
 * description	： services process all for Product Manage page
 *
 ***********************************************************************/
package net.webike.RcProductFactory.service;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import net.webike.RcProductFactory.action.form.ProductEditActionForm;
import net.webike.RcProductFactory.entity.EntMstAttribute;
import net.webike.RcProductFactory.entity.EntMstAttributeGroup;
import net.webike.RcProductFactory.entity.EntMstCategory;
import net.webike.RcProductFactory.entity.EntMstFactoryMatterNew;
import net.webike.RcProductFactory.entity.EntMstNouki;
import net.webike.RcProductFactory.entity.EntMstProductSupplierStatus;
import net.webike.RcProductFactory.entity.EntMstSiire;
import net.webike.RcProductFactory.entity.EntMstSyouhinSelect;
import net.webike.RcProductFactory.entity.EntReplaceElement;
import net.webike.RcProductFactory.entity.EntTblBrandConditionRule;
import net.webike.RcProductFactory.entity.EntTblFactoryProductDescription;
import net.webike.RcProductFactory.entity.EntTblFactoryProductGeneral;
import net.webike.RcProductFactory.entity.EntTblFactoryProductGuestInput;
import net.webike.RcProductFactory.entity.EntTblFactoryProductLink;
import net.webike.RcProductFactory.entity.EntTblFactoryProductNew;
import net.webike.RcProductFactory.entity.EntTblFactoryProductSelect;
import net.webike.RcProductFactory.entity.EntTblFactoryProductSupplier;
import net.webike.RcProductFactory.entity.EntTblFactoryProductVideo;

/* import new lib at here. */
/* BOE global process by  Dinh */
/* EOE global process by  Dinh */

/* BOE global process by  Thanh */
import net.webike.RcProductFactory.entity.EntMstLinkReason;
import net.webike.RcProductFactory.entity.EntMstSyouhinSearch;
/* EOE global process by  Thanh */

/* BOE global process by  Dai */
/* EOE global process by  Dai */

/* BOE global process by  Hoang */
/* EOE global process by  Hoang */

/**
 * services process all for Product Manage page.
 */
public interface ProductEditService {

	/* BOE Block1 */

	/* BOE global process by  Dinh */
	/************************************************************************
	 * <b>Description:</b><br>
	 *  deleteProducts.
	 *
	 * @author		Le.Dinh
	 * @date		21 Feb 2014
	 * @param 		listFactoryProducSession	List<EntTblFactoryProductNew>
	 * @param       loginUserId login user id
	 * @param       matterNo matterNo
	 * @return		boolean
	 * @throws 		SQLException		SQLException
	 ************************************************************************/
	boolean deleteProducts(List<EntTblFactoryProductNew> listFactoryProducSession, Long matterNo, String loginUserId) throws SQLException;
	/************************************************************************
	 * <b>Description:</b><br>
	 *  selectListProductSameGroupNoSelect.
	 * @author		Le.Dinh
	 * @date		24 Feb 2014
	 * @param		productGen EntTblFactoryProductGeneral
	 * @return		List<EntTblFactoryProductNew>
	 * @throws 		SQLException	SQLException
	 ************************************************************************/
	List<EntTblFactoryProductNew> selectListProductSameGroupNoSelect(EntTblFactoryProductGeneral productGen) throws SQLException;
	/************************************************************************
	 * <b>Description:</b><br>
	 *  selectMaxProductIdPlusOne.
	 * @author		Le.Dinh
	 * @date		3 Mar 2014
	 * @return		Long
	 * @throws 		SQLException	SQLException
	 ************************************************************************/
	Long selectMaxProductIdPlusOne() throws SQLException;
	/************************************************************************
	 * <b>Description:</b><br>
	 *  insertNewTblFactoryProductNumbering.
	 * @author		Le.Dinh
	 * @date		3 Mar 2014
	 * @param 		productIdMax	Integer
	 * @return		Integer
	 * @throws 		SQLException	SQLException
	 ************************************************************************/
	Integer insertNewTblFactoryProductNumbering(Long productIdMax) throws SQLException;

	/* EOE global process by  Dinh */

	/* BOE global process by  Thanh */

	/************************************************************************
	 * <b>Description:</b><br>
	 *  process to save product.
	 *
	 * @author		Tran.Thanh
	 * @date		24 Feb 2014
	 * @param 		mapProduct map product.
	 * @param 		entQuery ent query.
	 * @param 		errorFlg errorFlg.
	 * @param 		form ProductEditActionForm.
	 * @return		boolean
	 ************************************************************************/
	boolean processSaveDataProductEdit(Map<Integer, Object> mapProduct
			, EntTblFactoryProductNew entQuery
			, ArrayList<Integer> errorFlg, ProductEditActionForm form);
	/* EOE global process by  Thanh */

	/* BOE global process by  Dai */
	/* EOE global process by  Dai */

	/* BOE global process by  Hoang */
	/* EOE global process by  Hoang */

    /************************************************************************
     * <b>Description:</b><br>
     *  Check exists product in rc_product_factory.tbl_factory_product AND rc_product_factory.tbl_factory_product_general.
     *
     * @author		Luong.Dai
     * @date		Feb 17, 2014
     * @param       productId Integer
     * @return		EntTblFactoryProductNew
     ************************************************************************/
    EntTblFactoryProductNew checkExistProduct(Long productId);
	/* EOE Block1 */

	/* BOE Block2 */
    /************************************************************************
     * <b>Description:</b><br>
     *  selectUrlPathFromSettingTable.
     *
     * @author		Tran.Thanh
     * @date		15 Mar 2014
     * @return		String url Product Factory Path.
     ************************************************************************/
    String selectUrlPathFromSettingTable();
	/* EOE Block2 */

	/* BOE Block3 */
	/* EOE Block3 */

	/* BOE Block4 */
    /************************************************************************
     * <b>Description:</b><br>
     *  select list attribute of product.
     *      - Select list attribute by categoryCode.
     *      - Select list attribute by productId from attribute type table.
     *
     * @author		Luong.Dai
     * @date		Feb 12, 2014
     * @param       product EntTblFactoryProductNew
     * @return      List<EntMstAttribute>
     * @throws      SQLException    Integer
     ************************************************************************/
    List<EntMstAttribute> selectListAttributeOfProduct(EntTblFactoryProductNew product) throws SQLException;

    /************************************************************************
     * <b>Description:</b><br>
     *  Select all list of attribute with attribute_del_flg = 0.
     *
     * @author		Luong.Dai
     * @date		Feb 13, 2014
     * @return		List<EntMstAttribute>
     ************************************************************************/
    List<EntMstAttribute> selectAllListAttribute();

    /************************************************************************
     * <b>Description:</b><br>
     *  Select list attributeGroup by attribute Code.
     *
     * @author		Luong.Dai
     * @date		Feb 15, 2014
     * @param       attributeCode String
     * @return		List<EntMstAttributeGroup>
     ************************************************************************/
    List<EntMstAttributeGroup> selectListAttributeGroupByAttributeCode(String attributeCode);
    /* EOE Block4 */

	/* BOE Block4 */
	/* EOE Block4 */

	/* BOE Block5 */
    /************************************************************************
     * <b>Description:</b><br>
     *  select Link by product Id.
     *
     * @author		Tran.Thanh
     * @date		18 Feb 2014
     * @param 		productId productId
     * @return		List<EntTblFactoryProductLink>
     ************************************************************************/
    List<EntTblFactoryProductLink> selectLinkByProductId(Long productId);

    /************************************************************************
     * <b>Description:</b><br>
     *  select List All Link Reason.
     *
     * @author		Tran.Thanh
     * @date		18 Feb 2014
     * @return		List<EntMstLinkReason>
     ************************************************************************/
    List<EntMstLinkReason> selectListAllLinkReason();

    /************************************************************************
     * <b>Description:</b><br>
     *  filter data from webikesh.mst_syouhin_all.
     *
     * @author		Tran.Thanh
     * @date		20 Feb 2014
     * @param 		syouhin EntMstSyouhinAll
     * @return		List<EntMstSyouhinAll>
     ************************************************************************/
    List<EntMstSyouhinSearch> selectSyouhinSearch(EntMstSyouhinSearch syouhin);
	/* EOE Block5 */

	/* BOE Block6 */
    /************************************************************************
     * <b>Description:</b><br>
     *  select Video By ProductId.
     *
     * @author		Tran.Thanh
     * @date		13 Feb 2014
     * @param 		productId productId
     * @return		List EntTblFactoryProductVideo
     * @throws SQLException		List<EntTblFactoryProductVideo>
     ************************************************************************/
    List<EntTblFactoryProductVideo> selectVideoByProductId(Long productId) throws SQLException;
	/* EOE Block6 */

	/* BOE Block7 */
	/************************************************************************
	 * <b>Description:</b><br>
	 *  select Remarks By ProductId.
	 *
	 * @author		Tran.Thanh
	 * @date		13 Feb 2014
	 * @param 		productId Long
	 * @return		EntTblFactoryProductDescription EntTblFactoryProductDescription
	 * @throws 		SQLException		EntTblFactoryProductDescription
	 ************************************************************************/
	EntTblFactoryProductDescription selectRemarksByProductId(Long productId) throws SQLException;

	/************************************************************************
	 * <b>Description:</b><br>
	 *  insert New Product Description.
	 *
	 * @author		Tran.Thanh
	 * @date		13 Feb 2014
	 * @param 		entTblFactoryProductDescription entTblFactoryProductDescription
	 * @return		productId.
	 * @throws SQLException		int
	 ************************************************************************/
	long insertNewProductDescription(EntTblFactoryProductDescription entTblFactoryProductDescription) throws SQLException;

	/************************************************************************
	 * <b>Description:</b><br>
	 *  update Product Description.
	 *
	 * @author		Tran.Thanh
	 * @date		13 Feb 2014
	 * @param 		entTblFactoryProductDescription entTblFactoryProductDescription.
	 * @throws 		SQLException		void
	 ************************************************************************/
	void updateProductDescription(EntTblFactoryProductDescription entTblFactoryProductDescription) throws SQLException;
	/* EOE Block7 */

	/* BOE Block8 */
	/************************************************************************
	 * <b>Description:</b><br>
	 *  get list productGuestInput of product by productId.
	 *
	 * @author		Luong.Dai
	 * @date		Feb 18, 2014
	 * @param       productId Long
	 * @return      List<EntTblFactoryProductGuestInput>
	 * @throws      SQLException Integer
	 ************************************************************************/
	List<EntTblFactoryProductGuestInput> selectListProductGuestInputByProductId(Long productId) throws SQLException;
	/* EOE Block8 */

	/* BOE Block9 */

	   /************************************************************************
     * <b>Description:</b><br>
     *  Get siire marume code from product id.
     *  when product id has more tb_factory_product_supplier
     *      then choose by supplier_syouhin_siire_order = 1
     *
     * @author      hoang.ho
     * @date        Feb 26, 2014
     * @param       productId Long
     * @return      Integer siire_marume_code
     * @throws      SQLException Integer
     ************************************************************************/
	Integer selectSiireMarumeCodeByProductId(Long productId) throws SQLException;

	/************************************************************************
	 * <b>Description:</b><br>
	 *  select price of product.
	 *
	 * @author		Luong.Dai
	 * @date		Feb 18, 2014
	 * @param       productId Long
	 * @return      EntTblFactoryProductNew product
	 * @throws      SQLException Integer
	 ************************************************************************/
	EntTblFactoryProductNew selectProductProperAndSupplierPriceByProductId(Long productId) throws SQLException;

	/************************************************************************
	 * <b>Description:</b><br>
	 *  Select default flag.
	 *
	 * @author		Luong.Dai
	 * @date		Feb 18, 2014
	 * @param       product EntTblFactoryProductNew
	 * @return      EntTblBrandConditionRule defaultRule
	 * @throws      SQLException integer
	 ************************************************************************/
	EntTblBrandConditionRule selectDefaultFlagByProductId(EntTblFactoryProductNew product) throws SQLException;

	/************************************************************************
	 * <b>Description:</b><br>
	 *  Select new rule by brand code and proper price.
	 *
	 * @author		Luong.Dai
	 * @date		Feb 18, 2014
	 * @param       product EntTblFactoryProductNew
	 * @return      EntTblBrandConditionRule
	 * @throws      SQLException Integer
	 ************************************************************************/
	EntTblBrandConditionRule selectPriceFlagByBrandCodeAndProperPrice(EntTblFactoryProductNew product) throws SQLException;

	/************************************************************************
	 * <b>Description:</b><br>
	 *  Select productBrandCode, productGroupCode and productMatterNo.
	 *
	 * @author		Luong.Dai
	 * @date		Feb 19, 2014
	 * @param       productId String
	 * @return      EntTblFactoryProductGeneral
	 * @throws      SQLException Integer
	 ************************************************************************/
	EntTblFactoryProductGeneral selectProductGeneralValueByProductId(String productId) throws SQLException;

	/************************************************************************
	 * <b>Description:</b><br>
	 *  select list product of same group.
	 *
	 * @author		Luong.Dai
	 * @date		Feb 19, 2014
	 * @param       productGen EntTblFactoryProductGeneral
	 * @param       listSelect List<EntTblFactoryProductSelect>
	 * @return      List<EntTblFactoryProductNew>
	 * @throws      SQLException Exception
	 ************************************************************************/
	List<EntTblFactoryProductNew> selectListProductSameGroup(
	                                    EntTblFactoryProductGeneral productGen,
	                                    List<EntTblFactoryProductSelect> listSelect) throws SQLException;
	/************************************************************************
	 * <b>Description:</b><br>
	 *  Select list syouhinSelect.
	 *
	 * @author		Luong.Dai
	 * @date		Feb 19, 2014
	 * @return      List<EntMstSyouhinSelect>
	 * @throws      SQLException Exception
	 ************************************************************************/
	List<EntMstSyouhinSelect> selectListSyouhinSelect() throws SQLException;

	/************************************************************************
	 * <b>Description:</b><br>
	 *  select list select of product by productId.
	 *
	 * @author		Luong.Dai
	 * @date		Feb 20, 2014
	 * @param       select EntTblFactoryProductSelect
	 * @return      List<EntTblFactoryProductSelect>
	 * @throws      SQLException Exception
	 ************************************************************************/
	List<EntTblFactoryProductSelect> selectListSelectCodeByProductId(EntTblFactoryProductSelect select) throws SQLException;

    /************************************************************************
     * <b>Description:</b><br>
     *  select list select of product by select code array.
     *
     * @author      hoang.ho
     * @date        2014/02/25
     * @param       selectCodeList string select code list
     * @return      List<EntTblFactoryProductSelect>
     * @throws      SQLException Exception
     ************************************************************************/
    List<EntTblFactoryProductSelect> selectListSelectByCodeList(String selectCodeList) throws SQLException;

    /************************************************************************
     * <b>Description:</b><br>
     *  select list option of list product.
     *
     * @author		Luong.Dai
     * @date		Mar 21, 2014
     * @param 		listProductId		String
     * @param 		listItemSelect		List<EntTblFactoryProductSelect>
     * @return		List<EntTblFactoryProductNew>
     * @throws 		SQLException		Exception
     ************************************************************************/
    List<EntTblFactoryProductNew> selectListOptionValueOfProduct(
                                            String listProductId
    									  , List<EntTblFactoryProductSelect> listItemSelect) throws SQLException;
    /* EOE Block9 */

	/* BOE Block10 */
    /************************************************************************
     * <b>Description:</b><br>
     *  get category_code of product.
     *
     * @author		Luong.Dai
     * @date		Feb 12, 2014
     * @param       productId Long
     * @return		Integer
     * @throws      SQLException Integer
     ************************************************************************/
    Integer selectCategoryCodeOfProduct(Long productId) throws SQLException;

    /************************************************************************
     * <b>Description:</b><br>
     *  select list category.
     *
     * @author		Luong.Dai
     * @date		Feb 12, 2014
     * @return List<EntMstCategory>
     * @throws SQLException Integer
     ************************************************************************/
    List<EntMstCategory> selectListCategory() throws SQLException;
	/* EOE Block10 */

	/* BOE Block11 */
	/* EOE Block11 */

	/* BOE Block12 */
	/************************************************************************
	 * <b>Description:</b><br>
	 *  selectListSupplierByProductId.
	 *
	 * @author		Le.Dinh
	 * @date		12 Feb 2014
	 * @param 		productId	Long
	 * @return		List<EntTblFactoryProductSupplier>
	 * @throws 		SQLException	SQLException
	 ************************************************************************/
    List<EntTblFactoryProductSupplier> selectListSupplierByProductId(Long productId) throws SQLException;

    /************************************************************************
     * <b>Description:</b><br>
     *  selectListAllSiire.
     *
     * @author		Le.Dinh
     * @date		2014/02/12
     * @return		List<EntMstSiire>
     * @throws		SQLException	SQLException
     ************************************************************************/
    List<EntMstSiire> selectListAllSiire() throws SQLException;
    /************************************************************************
     * <b>Description:</b><br>
     *  selectListAllNouki.
     *
     * @author		Le.Dinh
     * @date		2014/02/12
     * @return		List<EntMstSiire>
     * @throws		SQLException	SQLException
     ************************************************************************/
    List<EntMstNouki> selectListAllNouki() throws SQLException;
	/* EOE Block12 */

	/* BOE Block13 */
	/************************************************************************
	 * <b>Description:</b><br>
	 *  selectProductFactoryProductGeneralByProductid.
	 * @author		Le.Dinh
	 * @date		11 Feb 2014
	 * @param		productId	Long
	 * @return		EntTblFactoryProductGeneral	EntTblFactoryProductGeneral
	 * @throws 		SQLException	SQLException
	 ************************************************************************/
	EntTblFactoryProductGeneral selectProductFactoryProductGeneralByProductid(Long productId) throws SQLException;
	/************************************************************************
	 * <b>Description:</b><br>
	 *  rselectListAllSupplierStatus.
	 *
	 * @author		Le.Dinh
	 * @date		13 Feb 2014
	 * @return		List<EntMstProductSupplierStatus>
	 * @throws 		SQLException		SQLException
	 ************************************************************************/
	List<EntMstProductSupplierStatus> selectListAllSupplierStatus() throws SQLException;
	/* EOE Block13 */

	/* BOE Block14 */
	/* EOE Block14 */

	/* BOE Block15 */
	/************************************************************************
	 * <b>Description:</b><br>
	 *  Get list all product.
	 *
	 * @author		Luong.Dai
	 * @date		Feb 14, 2014
	 * @param       page Integer
	 * @param       pageSize Integer
	 * @param       product EntTblFactoryProductNew
	 * @return      List<EntTblFactoryProductNew>
	 * @throws      SQLException Integer
	 ************************************************************************/
	List<EntTblFactoryProductNew> selectAllListProduct(Integer page, Integer pageSize, EntTblFactoryProductNew product) throws SQLException;

	/************************************************************************
	 * <b>Description:</b><br>
	 *  Count number of product in block 15.
	 *
	 * @author		Luong.Dai
	 * @date		Feb 15, 2014
	 * @param       product EntTblFactoryProductNew
	 * @return		Integer
	 ************************************************************************/
	Integer selectCountListProduct(EntTblFactoryProductNew product);

	/************************************************************************
	 * <b>Description:</b><br>
	 *  Select number product of matterNo.
	 *
	 * @author		Luong.Dai
	 * @date		Mar 10, 2014
	 * @param       matterNo   Long
	 * @return      Integer    Number product of matterNo.
	 * @throws     SQLException		Exception
	 ************************************************************************/
	Integer selectCountNumberProductOfMatter(Long matterNo) throws SQLException;

	/************************************************************************
	 * <b>Description:</b><br>
	 *  Save data of block 15 to DB.
	 *
	 * @author		Luong.Dai
	 * @date		Mar 12, 2014
	 * @param       listProduct        List<EntTblFactoryProductNew>
	 * @param       userId             String
	 * @param       matterNo           String
	 * @return      boolean            Update result
	 * @throws      SQLException	   Exception
	 ************************************************************************/
	boolean saveDataOfListProductAtProductEdit(List<EntTblFactoryProductNew> listProduct
	                                            , String userId
	                                            , String matterNo) throws SQLException;

	/************************************************************************
	 * <b>Description:</b><br>
	 *  save Data Popup Block 15 to DB.
	 *
	 * @author		Tran.Thanh
	 * @date		Apr 21, 2014
	 * @param 		listObjPopupBlock15	list EntReplaceElement To Save
	 * @param 		listProductId		list Product Id checked
	 * @param 		entProductSearch	EntTblFactoryProductNew contains Search Condition
	 * @param 		updateUserId		String updated user id
	 * @return		boolean 			result process save data
	 * @throws 		Exception		boolean
	 ************************************************************************/
	boolean saveDataPopupBlock15(List<EntReplaceElement> listObjPopupBlock15
								, List<Long> listProductId
								, EntTblFactoryProductNew entProductSearch
								, String updateUserId) throws Exception;

	/************************************************************************
	 * <b>Description:</b><br>
	 *  Count number release product with search filter.
	 *
	 * @author		Luong.Dai
	 * @date		May 12, 2014
	 * @param 		productSearch 	filter params
	 * @return		Integer			number product
	 ************************************************************************/
	Integer countNumberReleasseProduct(EntTblFactoryProductNew productSearch);

	/************************************************************************
	 * <b>Description:</b><br>
	 *  select all productId not release with filter.
	 *
	 * @author		Luong.Dai
	 * @date		May 12, 2014
	 * @param		productSearch	Product with search filter
	 * @return		List<Long>	List result
	 ************************************************************************/
	List<Long> selectAllProductIdNotReleaseWithFilter(EntTblFactoryProductNew productSearch);

	/************************************************************************
	 * <b>Description:</b><br>
	 * Delete all list product not released with filter.
	 *
	 * @author		Luong.Dai
	 * @date		May 12, 2014
	 * @param 		lstProductId	List productId
	 * @param		entMatter		matter of product
	 * @param       lstSyouhinSysCode   list syouhin_sys_code
     * @param       loginId             login ID
	 * @return		boolean			Delete result
	 ************************************************************************/
	boolean deleteAllProductNotReleasedWithFilter(List<Long> lstProductId
												, EntMstFactoryMatterNew entMatter
												, List<Long> lstSyouhinSysCode
                                                , String loginId);

	/************************************************************************
	 * <b>Description:</b><br>
	 *  Count error of list product.
	 *
	 * @author		Luong.Dai
	 * @date		Jun 3, 2014
	 * @param 		lstProductId			List product to count
	 * @param 		matterNo				matter of product
	 * @param 		loginId					Login id
	 * @return		EntMstFactoryMatterNew	result
	 ************************************************************************/
	EntMstFactoryMatterNew countErrorOfListProduct(List<Long> lstProductId
													, Long matterNo
													, String loginId);

	/************************************************************************
	 * <b>Description:</b><br>
	 *  Select list syouhin_sys_code by list product id.
	 *
	 * @author		Luong.Dai
	 * @date		Jun 5, 2014
	 * @param       lstProductId List<long>
	 * @throws      SQLException query exception
	 * @return		List<Long>
	 ************************************************************************/
	List<Long> selectListSyouhinSysCode(List<Long> lstProductId) throws SQLException;
	/* EOE Block15 */
}
