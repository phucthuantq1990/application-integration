/************************************************************************
 * File Name    ： CalibrationServiceImpt.java
 * Author       ： Nguyen.Chuong
 * Version      ： 1.0.0
 * Date Created ： 2014/07/17
 * Date Updated ： 2014/07/17
 * Description  ： Service to process with Calibration object.
 ***********************************************************************/
package net.webike.RcProductFactory.service;
import net.webike.RcProductFactory.entity.EntTblFactoryCalibrationRequestIndividual;
import net.webike.RcProductFactory.entity.EntTblFactoryCalibrationRequestWhole;
import net.webike.RcProductFactory.mapper.TblFactoryCalibrationRequestIndividualMapper;
import net.webike.RcProductFactory.mapper.TblFactoryCalibrationRequestWholeMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Nguyen.Chuong
 *
 */
@Service("CalibrationServiceImpl")
public class CalibrationServiceImpl implements CalibrationService {

    @Autowired
    private TblFactoryCalibrationRequestWholeMapper tblCalibrationRequestWhole;
    @Autowired
    private TblFactoryCalibrationRequestIndividualMapper tblCalibrationRequestIndividual;

    @Override
    public EntTblFactoryCalibrationRequestWhole selectCalibrationRequestWhole(Long matterNo) {
        return tblCalibrationRequestWhole.selectCalibrationRequestWhole(matterNo);
    }

    @Override
    public EntTblFactoryCalibrationRequestWhole saveTabAllPopup(EntTblFactoryCalibrationRequestWhole ent, int tabNo) {
        EntTblFactoryCalibrationRequestWhole entReturn;
        //if tabNo = 1 then save tab all.
        if (tabNo == 1) {
            tblCalibrationRequestWhole.insertTabAllPopup(ent);
            entReturn = tblCalibrationRequestWhole.selectCalibrationRequestWhole(ent.getCalibrationRequestWholeMatterNo());
            return entReturn;
        }
        //else save tab note.
        tblCalibrationRequestWhole.insertTabMemoPopup(ent);
        entReturn = tblCalibrationRequestWhole.selectCalibrationRequestWhole(ent.getCalibrationRequestWholeMatterNo());
        return entReturn;
    }

    @Override
    public Long insertNewCalibrationRequestIndividual(EntTblFactoryCalibrationRequestIndividual ent) {
        //Insert new request individual into DB
        Integer insertStatus = tblCalibrationRequestIndividual.insertNewCalibrationRequestIndividual(ent);
        //If insertSuccess = 1 => insert success => get new insert individual code = max individual code.
        if (insertStatus > 0) {
            return tblCalibrationRequestIndividual.selectMaxIndividualCode();
        }
        return 0L;
    }
}
