/************************************************************************
 * File Name	： UserService.java
 * Author		： Tran.Thanh
 * Version		： 1.0.0
 * Date Created	： 2014/01/09
 * Date Updated	： 2014/01/09
 * Description	： Service to get user info from database.
 *
 ***********************************************************************/
package net.webike.RcProductFactory.service;

import java.util.List;

import net.webike.RcProductFactory.entity.EntMstFactoryUserTmp;

/**
 * Service to get matter from database.
 */
public interface UserTmpService {

    /************************************************************************
     * <b>Description:</b><br>
     *  select user by user id.
     *
     * @author		Tran.Thanh
     * @date		9 Jan 2014
     * @param       id userId.
     * @return		EntMstFactoryUser
     ************************************************************************/
	EntMstFactoryUserTmp selectUserTmpById(String id);

    /************************************************************************
     * <b>Description:</b><br>
     *  insert new user.
     *
     * @author		Tran.Thanh
     * @date		9 Jan 2014
     * @param       ent entity user.
     * @return      true if insert success, flase if insert false.
     * @throws Exception		Boolean
     ************************************************************************/
    Boolean insertNewUserTmp(EntMstFactoryUserTmp ent) throws Exception;

    /************************************************************************
     * <b>Description:</b><br>
     *  check existed user id in database.
     *
     * @author		Tran.Thanh
     * @date		10 Jan 2014
     * @param userId user_id
     * @return		Boolean
     ************************************************************************/
    Boolean selectUserIdExistedOrNot(String userId);


    /************************************************************************
     * <b>Description:</b><br>
     *  delete user by user_id and updatedUserId.
     *
     * @author		Tran.Thanh
     * @date		10 Jan 2014
     * @param userId user_id
     * @param updatedUserId		void
     ************************************************************************/
    void deleteUserByUserId(String userId, String updatedUserId);


    /************************************************************************
     * <b>Description:</b><br>
     *  active user by user_id and updated_user_id.
     *
     * @author		Tran.Thanh
     * @date		10 Jan 2014
     * @param userId user_id
     * @param updatedUserId		void
     ************************************************************************/
    void activeUserByUSerId(String userId, String updatedUserId);

    /************************************************************************
     * <b>Description:</b><br>
     *  update user info.
     *
     * @author		Tran.Thanh
     * @date		4 Oct 2013
     * @param 		entUserTmp	entity user.
     ************************************************************************/
    void updateUserInfo(EntMstFactoryUserTmp entUserTmp);

    /************************************************************************
     * <b>Description:</b><br>
     *  list user to filter.
     *
     * @author		Tran.Thanh
     * @date		10 Jan 2014
     * @param       entUserTmp entity to query.
     * @return		List<EntMstFactoryUserTmp>
     ************************************************************************/
    List<EntMstFactoryUserTmp> selectFilterUser(EntMstFactoryUserTmp entUserTmp);

    /************************************************************************
     * <b>Description:</b><br>
     *  Select User By Id with New Table.
     *
     * @author		Tran.Thanh
     * @date		14 Jan 2014
     * @param 		id	String
     * @return		EntMstFactoryUserTmp
     ************************************************************************/
    EntMstFactoryUserTmp selectUserById(String id);

    /************************************************************************
     * <b>Description:</b><br>
     *  Select All User with new Table.
     *
     * @author		Tran.Thanh
     * @date		14 Jan 2014
     * @return		List<EntMstFactoryUserTmp>
     ************************************************************************/
    List<EntMstFactoryUserTmp> selectAllUser();

    /************************************************************************
     * <b>Description:</b><br>
     *  Select list all user in editbrand page.
     *
     * @author		Tran.Thanh
     * @date		14 Jan 2014
     * @return		List<EntMstFactoryUserTmp>
     ************************************************************************/
    List<EntMstFactoryUserTmp> selectListAllUser();

}
