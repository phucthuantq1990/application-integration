///************************************************************************
// * File Name	： UserService.java
// * Author		： Nguyen.Chuong
// * Version		： 1.0.0
// * Date Created	： 2013/09/24
// * Date Updated	： 2013/09/24
// * Description	： Service to get matter from database.
// *
// ***********************************************************************/
//package net.webike.RcProductFactory.service;
//
//import java.util.List;
//
//import net.webike.RcProductFactory.entity.EntMstFactoryUser;
//
//
///**
// * Service to get matter from database.
// */
//public interface UserService {
//
//	/************************************************************************
//     * <b>Description:</b><br>
//     *  Get list of all user from db.
//     *
//     * @author      Nguyen.Chuong
//     * @date        Sep 10, 2013
//     * @return      List<EntMstFactoryMatterStatus>
//     * @throws      DataAccessException
//     ************************************************************************/
////	List<EntMstFactoryUser> selectAllUser();
////
////	/************************************************************************
////	 * <b>Description:</b><br>
////	 *  select list all user (del_flg = 0 && del_flg = 1).
////	 *
////	 * @author		Tran.Thanh
////	 * @date		6 Jan 2014
////	 * @return		List<EntMstFactoryUser>
////	 ************************************************************************/
////	List<EntMstFactoryUser> selectListAllUser();
////
////    /************************************************************************
////     * <b>Description:</b><br>
////     *  select user from mst_factory_user table by Id.
////     *
////     * @author      Nguyen.Chuong
////     * @date        Sep 11, 2013
////     * @param       id string
////     * @return      List<EntMstFactoryUser>
////     ************************************************************************/
////    EntMstFactoryUser selectUserById(String id);
////
////
////    /************************************************************************
////     * <b>Description:</b><br>
////     *  Select User with conditions.
////     *
////     * @author		Tran.Thanh
////     * @date		4 Oct 2013
////     * @param       entUser entity user
////     * @return		List<EntMstFactoryUser>
////     ************************************************************************/
////    List<EntMstFactoryUser> selectFilterUser(EntMstFactoryUser entUser);
////
////    /************************************************************************
////     * <b>Description:</b><br>
////     *  delete user by user_id and kousin_id.
////     *
////     * @author		Tran.Thanh
////     * @date		3 Oct 2013
////     * @param 		userId user_id
////     * @param 		kousinId kousin_id
////     ************************************************************************/
////    void deleteUserByUserId(String userId, String kousinId);
////
////    /************************************************************************
////     * <b>Description:</b><br>
////     *  active user by user_id and kousin_id.
////     *
////     * @author		Tran.Thanh
////     * @date		3 Oct 2013
////     * @param 		userId	user_id
////     * @param 		kousinId	kousin_id
////     ************************************************************************/
////    void activeUserByUSerId(String userId, String kousinId);
////
////    /************************************************************************
////     * <b>Description:</b><br>
////     *  select all info user by user_id.
////     *
////     * @author		Tran.Thanh
////     * @date		3 Oct 2013
////     * @param 		userId user_id
////     * @return		EntMstFactoryUser
////     ************************************************************************/
////    EntMstFactoryUser selectAllInfoUserById(String userId);
////
////    /************************************************************************
////     * <b>Description:</b><br>
////     *  insert new user to database.
////     *
////     * @author		Nguyen.Chuong
////     * @date		Oct 2, 2013
////     * @param       ent EntMstFactoryUser
////     * @return		Boolean
////     * @throws      Exception Data
////     ************************************************************************/
////    Boolean insertNewUser(EntMstFactoryUser ent) throws Exception;
////
////    /************************************************************************
////     * <b>Description:</b><br>
////     *  check userId is existed or not in DB.
////     *
////     * @author      Nguyen.Chuong
////     * @date        Sep 11, 2013
////     * @param       userId String
////     * @return      boolean: true when existed, false when not existed.
////     ************************************************************************/
////    Boolean selectUserIdExistedOrNot(String userId);
////
////    /************************************************************************
////     * <b>Description:</b><br>
////     *  update user info.
////     *
////     * @author		Tran.Thanh
////     * @date		4 Oct 2013
////     * @param 		entUser	entity user.
////     ************************************************************************/
////    void updateUserInfo(EntMstFactoryUser entUser);
//}
