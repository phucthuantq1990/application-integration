/************************************************************************
 * file name	： MatterManageServiceImpl.java
 * author		： Le.Dinh
 * version		： 1.0.0
 * date created	： 17 Jan 2014
 * date updated	： 17 Jan 2014
 * description	： MatterManageServiceImpl
 *
 ***********************************************************************/
package net.webike.RcProductFactory.service;

import java.sql.SQLException;
import java.util.List;

import net.webike.RcProductFactory.entity.EntMstFactoryMatterNew;
import net.webike.RcProductFactory.mapper.MstFactoryMatterMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * MatterManageServiceImpl.
 */
@Service("matterManageService")
public class MatterManageServiceImpl implements MatterManageService {
    @Autowired
    private MstFactoryMatterMapper matterMapper;

	@Override
	public List<EntMstFactoryMatterNew> selectMatterManageList(int offset, int numOfRows) throws SQLException {
		return matterMapper.selectMatterManageList(offset, numOfRows);
	}

	@Override
	public List<EntMstFactoryMatterNew> selectFilteredMatterManageList(
			int page, int pageSize,
			EntMstFactoryMatterNew entMstFactoryMatterNew) throws SQLException {
		int offset = (page - 1) * pageSize;
		int limit = pageSize;
		return matterMapper.selectFilteredMatterManageList(offset, limit, entMstFactoryMatterNew);
	}

    @Override
    public int selectFilteredMatterManageCount(EntMstFactoryMatterNew entMstFactoryMatterNew) throws SQLException {
        int count = matterMapper.selectFilteredMatterManageCount(entMstFactoryMatterNew);
        return count;
    }

	/* (non-Javadoc)
	 * @see net.webike.RcProductFactory.service.MatterManageService#selectAllFilteredMatterManage(net.webike.RcProductFactory.entity.EntMstFactoryMatterNew)
	 */
	@Override
	public List<EntMstFactoryMatterNew> selectAllFilteredMatterManage(
			EntMstFactoryMatterNew entMstFactoryMatterNew) throws SQLException {
		return matterMapper.selectAllFilteredMatterManage(entMstFactoryMatterNew);
	}
}
