/**
 * ProductServiceImpl
 */
package net.webike.RcProductFactory.service;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import net.webike.RcProductFactory.entity.EntCSVMode1;
import net.webike.RcProductFactory.entity.EntCSVMode3;
import net.webike.RcProductFactory.entity.EntCSVProductFitModel;
import net.webike.RcProductFactory.entity.EntMstAttribute;
import net.webike.RcProductFactory.entity.EntMstBrand;
import net.webike.RcProductFactory.entity.EntMstCategory;
import net.webike.RcProductFactory.entity.EntMstFactoryMatter;
import net.webike.RcProductFactory.entity.EntMstFactoryProductStatus;
import net.webike.RcProductFactory.entity.EntProduct;
import net.webike.RcProductFactory.entity.EntTblFactoryImportCondition;
import net.webike.RcProductFactory.entity.EntTblFactoryProductImage;
import net.webike.RcProductFactory.entity.EntTblFactoryProductModel;
import net.webike.RcProductFactory.entity.EntTblFactoryProductNew;
import net.webike.RcProductFactory.mapper.MstAttributeMapper;
import net.webike.RcProductFactory.mapper.MstBrandMapper;
import net.webike.RcProductFactory.mapper.MstCategoryMapper;
import net.webike.RcProductFactory.mapper.TblFactoryProductMapper;
import net.webike.RcProductFactory.mapper.TblFactorySettingMapper;
import net.webike.RcProductFactory.util.Constant;
import net.webike.RcProductFactory.util.Utils;

import org.apache.commons.beanutils.BeanComparator;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * @author Nguyen.Chuong
 * MainMenuServiceImpl implements MainMenuService
 */
@Service("ProductServiceImpl")
//@Transactional
public class ProductServiceImpl implements ProductService {
    @Autowired
    private TblFactoryProductMapper factoryProductMapper;
    @Autowired
    private TblFactorySettingMapper tblFactorySettingMapper;
    @Autowired
    private MstAttributeMapper mstAttributeMapper;

    @Autowired
    private MstBrandMapper brandMapper;

    @Autowired
    private MstCategoryMapper categoryMapper;

    @Autowired
    private TblFactoryProductMapper tblFactoryProductMapper;

//    private static final String TMP_IMAGE = "tmp_image";
    private final Logger logger = Logger.getLogger(this.getClass());
    @Override
    public List<EntTblFactoryProductModel> selectProductModelByProductCode(EntTblFactoryProductNew entProduct) {
        // Check productCode is not empty
        if (StringUtils.isEmpty(entProduct.getProductCode())) {
            return null;
        }

        return factoryProductMapper.selectProductModelByProductCode(entProduct);
    }

    @Override
    public List<EntTblFactoryProductImage> selectListImageAndThumbByProductId(EntTblFactoryProductNew entProduct) {
    	// Check productCode is not empty
        if (entProduct == null) {
            return null;
        }
    	// Get MatterNo by ProductId
    	EntTblFactoryProductNew productNew = factoryProductMapper.selectMatterNoByProductId(entProduct.getProductId());
        if (productNew == null) {
            return null;
        }
        /* BOE Tran.Thanh - 14/02/2014: delete matterNo - not use
//    	Integer matterNo = productNew.getProductMatterNo();
//    	if (matterNo == null) {
//    		matterNo = Integer.parseInt("0");
//    	}
		 * EOE Tran.Thanh - 14/02/2014
		 */

        List<EntTblFactoryProductImage> listImage = factoryProductMapper.selectListImageAndThumbByProductId(entProduct);
        // Check has image in listImage
        if (listImage.size() > 0) {
            for (EntTblFactoryProductImage ent : listImage) {
                // Check productImage or productThumb is empty
            	// BOE @rcv!Tran.Thanh Jul 28, 2014 #9956 : change logic validate image
//                if (StringUtils.isEmpty(ent.getProductImage()) || StringUtils.isEmpty(ent.getProductThumb())) {
//                    ent.setProductImage("");
//                    ent.setProductThumb("");
//                    continue;
//                }
            	// EOE @rcv!Tran.Thanh Jul 28, 2014 #9956 : BOE @rcv!Tran.Thanh Jul 28, 2014 #9956 : change logic validate image
                //Format link image
                //refer to real
                if (Constant.REFER_MODE_REAL.equals(ent.getReferMode())) {
                    ent.setProductImage(Constant.IMAGE_SERVER_2 + ent.getProductImage());
                    ent.setProductThumb(Constant.IMAGE_SERVER_2 + ent.getProductThumb());
                } else if (Constant.REFER_MODE_TEMPORARY.equals(ent.getReferMode())) {
                    //Refer to temporary
                    String factoryURL = tblFactorySettingMapper.selectSettingValueBySettingCode(Constant.SETTING_PRODUCT_FACTORY_URL);
                    ent.setProductImage(factoryURL + ent.getProductImage());
                    ent.setProductThumb(factoryURL + ent.getProductThumb());
                }
            }
        }
        return listImage;
    }

    @Override
    public List<EntMstFactoryProductStatus> selectAllProductStatus() {
        List<EntMstFactoryProductStatus> listProductStatus = factoryProductMapper.selectAllProductStatus();
        EntMstFactoryProductStatus entProductStatus = new EntMstFactoryProductStatus();
        entProductStatus.setProductStatusCode(Constant.STATUS_ALL_ERROR_VALUE);
        entProductStatus.setProductStatusName(Constant.STATUS_ALL_ERROR_NAME);
        listProductStatus.add(0, entProductStatus);
        return listProductStatus;
    }

    @Override
    public EntMstFactoryMatter countProductOfMatter(EntProduct entProduct) {
        // Check matterNo is not empty
        if (StringUtils.isEmpty(entProduct.getMatterNo())) {
            return null;
        }
        String checkedStatusCode = Constant.PRODUCT_CHECKED_STATUS_CODE;
        return factoryProductMapper.countProductOfMatter(entProduct, checkedStatusCode);
    }

    @Override
    public void updateBunruiCode(EntProduct entProduct) {
        factoryProductMapper.updateBunruiCode(entProduct);
    }

    @Override
    public void updateProductStatusCode(EntProduct entProduct) {
        factoryProductMapper.updateProductStatusCode(entProduct);
    }

    @Override
    public List<EntProduct> selectProductListByMatterNo(EntProduct entProduct, int pageNumber, int pageSize) {
        if (StringUtils.isEmpty(entProduct.getMatterNo())) {
            return null;
        }
        //Get limit and offset to load data.
        //Number of rows each page will load
        int limit = pageSize;

        int offset = (pageNumber - 1) * limit;

        List<EntProduct> listProduct = factoryProductMapper.selectProductListByMatterNo(entProduct, limit, offset);

        return listProduct;
    }

    @Override
    public List<EntTblFactoryProductNew> selectListProductByMatterCode(Integer page, Integer pageSize, EntTblFactoryProductNew product) throws Exception {
        List<EntTblFactoryProductNew> lstProduct;
        Integer limit = pageSize;
        //Get limit and offset to load data.
        Integer offset = (page - 1) * limit;
        try {
            //Get list product
            product.setLimit(limit);
            product.setOffset(offset);
            lstProduct = factoryProductMapper.selectListProductByMatterCode(product);
        } catch (Exception ex) {
            logger.error("Services[selectListProductByMatterCode] An error SQLException: " + ex.toString());
            throw ex;
        }

        return lstProduct;
    }

    @Override
    public Integer selectTotalRecordOfFilterProduct(EntTblFactoryProductNew product) {
        Integer total = factoryProductMapper.selectTotalRecordOfFilterProduct(product);

        return total;
    }
    /* EOE add new function by Luong.Dai */

    @Override
    public List<EntTblFactoryProductNew> selectBottomGridByMatterNoAndFilterForFitModelCheck(int limit, int offset,
    EntTblFactoryProductNew entTblFactoryProductNew) throws SQLException {
      //Get limit and offset to load data.
        offset = (offset - 1) * limit;
        List<EntTblFactoryProductNew> listEntTblFactoryProductNew = factoryProductMapper.selectBottomGridByMatterNoAndFilterForFitModelCheck(
        limit, offset, entTblFactoryProductNew);
        return listEntTblFactoryProductNew;
    }

    @Override
    public List<EntTblFactoryProductNew> selectProductListByProductIdList(List<Long> pl) {
        List<EntTblFactoryProductNew> lstReturn = factoryProductMapper.selectProductListByProductIdList(pl);
        return lstReturn;
    }

    @Override
    public int selectTotalRecordOfFilterBottomGrid(EntTblFactoryProductNew entTblFactoryProductNew) throws SQLException {
        int count = factoryProductMapper.selectTotalRecordOfFilterBottomGrid(entTblFactoryProductNew);
        return count;
    }

    @Override
    public EntTblFactoryProductNew selectEntProductByProductId(Long productId) {
        EntTblFactoryProductNew entProductNew = factoryProductMapper.selectEntProductByProductId(productId);
        return entProductNew;
    }

    @Override
    public List<EntCSVProductFitModel> selectProductFitModelList(Integer page, Integer pageSize, EntCSVProductFitModel searchConditions) {
        int offset = (page - 1) * pageSize;
        List<EntCSVProductFitModel> entCSVProductFitModelList = factoryProductMapper.selectProductFitModelList(pageSize, offset, searchConditions);
        return entCSVProductFitModelList;
    }

    @Override
    public Integer selectProductFitModelCount(EntCSVProductFitModel searchConditions) {
        Integer count = factoryProductMapper.selectProductFitModelCount(searchConditions);
        return count;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<EntCSVMode3> selectProductAttributeByProductIds(List<String> productIdList, List<String> attributeCodeList) {
        List<EntCSVMode3> result = factoryProductMapper.selectProductAttributeByProductIds(productIdList, attributeCodeList);
        if (result == null) {
            result = new ArrayList<EntCSVMode3>();
        } else {
            BeanComparator beanComparator = new BeanComparator("productId");
            Collections.sort(result, beanComparator);
        }
        return result;
    }

    @Override
    public List<EntCSVMode3> selectProductFitModelByProductIds(List<String> productIdList) {
        List<EntCSVMode3> result = factoryProductMapper.selectProductFitModelByProductIds(productIdList);
        if (result == null) {
            result = new ArrayList<EntCSVMode3>();
        }
        return result;
    }

    @Override
    public List<EntCSVMode1> selectProductInfoExportCSVMode1SingleTable(List<String> productIdList) {
        List<EntCSVMode1> result = factoryProductMapper.selectProductInfoExportCSVMode1SingleTable(productIdList);
        if (result == null) {
            result = new ArrayList<EntCSVMode1>();
        }
        return result;
    }

    @Override
    public List<EntCSVMode1> selectProductCompatibleModelByProductIds(List<String> productIdList) {
        List<EntCSVMode1> result = factoryProductMapper.selectProductCompatibleModelByProductIds(productIdList);
        if (result == null) {
            result = new ArrayList<EntCSVMode1>();
        }
        return result;
    }

    @Override
    public List<EntCSVMode1> selectProductImageInfoByProductIds(List<String> productIdList) {
        List<EntCSVMode1> result = factoryProductMapper.selectProductImageInfoByProductIds(productIdList);
        if (result == null) {
            result = new ArrayList<EntCSVMode1>();
        }
        return result;
    }

    @Override
    public List<EntCSVMode1> selectProductSelectInfoByProductIds(List<String> productIdList) {
        List<EntCSVMode1> result = factoryProductMapper.selectProductSelectInfoByProductIds(productIdList);
        if (result == null) {
            result = new ArrayList<EntCSVMode1>();
        }
        return result;
    }

    @Override
    public List<EntCSVMode1> selectProductGuestInputInfoByProductIds(List<String> productIdList) {
        List<EntCSVMode1> result = factoryProductMapper.selectProductGuestInputInfoByProductIds(productIdList);
        if (result == null) {
            result = new ArrayList<EntCSVMode1>();
        }
        return result;
    }

    @Override
    public List<EntCSVMode1> selectProductLinkInfoByProductIds(List<String> productIdList) {
        List<EntCSVMode1> result = factoryProductMapper.selectProductLinkInfoByProductIds(productIdList);
        if (result == null) {
            result = new ArrayList<EntCSVMode1>();
        }
        return result;
    }

    @Override
    public List<EntCSVMode1> selectProductVideoInfoByProductIds(List<String> productIdList) {
        List<EntCSVMode1> result = factoryProductMapper.selectProductVideoInfoByProductIds(productIdList);
        if (result == null) {
            result = new ArrayList<EntCSVMode1>();
        }
        return result;
    }

    @Override
    public void appendDataByFieldName(List<EntCSVMode1> mainList, List<EntCSVMode1> subList, String fieldName) {
        for (EntCSVMode1 mainEnt : mainList) {
            for (EntCSVMode1 subEnt : subList) {
                if (mainEnt.getProductId().equals(subEnt.getProductId())) {
                    // append value to current value
                    if (fieldName.equals("compatibleModel")) {
                        mainEnt.setCompatibleModel(mainEnt.getcompatibleModelNotNull() + subEnt.getcompatibleModelNotNull());
                    } else if (fieldName.equals("customerConfirmationItem")) {
                        mainEnt.setCustomersConfirmationItem(mainEnt.getCustomersConfirmationItemNotNull() + subEnt.getCustomersConfirmationItemNotNull());
                    } else if (fieldName.equals("link")) {
                        mainEnt.setLink(mainEnt.getLinkNotNull() + subEnt.getLinkNotNull());
                    } else if (fieldName.equals("video")) {
                        mainEnt.setAnimation(mainEnt.getAnimationNotNull() + subEnt.getAnimationNotNull());
                    }
                }
            }
        }
    }

    @Override
    public void appendImageSelectData(List<EntCSVMode1> mainList, List<EntCSVMode1> subList, String fieldName) {
        for (EntCSVMode1 ent : mainList) {
            int count = 1;
            for (int i = 0; i < subList.size(); i++) {
                if (!(ent.getProductId().equals(subList.get(i).getProductId()))) {
                    continue;
                }
                try {
                    if (fieldName.equals("image")) {
                        BeanUtils.setProperty(ent, "productImageThumbnailPath" + count,
                            BeanUtils.getProperty(subList.get(i), "productImageThumbnailPath1"));
                        BeanUtils.setProperty(ent, "productImageDetailPath" + count,
                            BeanUtils.getProperty(subList.get(i), "productImageDetailPath1"));
                        count++;
                    } else if (fieldName.equals("option")) {
                        BeanUtils.setProperty(ent, "selectCode" + count,
                            BeanUtils.getProperty(subList.get(i), "selectCode1"));
                        BeanUtils.setProperty(ent, "selectName" + count,
                            BeanUtils.getProperty(subList.get(i), "selectName1"));
                        count++;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public List<EntMstAttribute> selectAttributeByCodes(List<String> attributeCodeList) {
        List<EntMstAttribute> result = mstAttributeMapper.selectAttributeByCodes(attributeCodeList);
        if (result == null) {
            result = new ArrayList<EntMstAttribute>();
        }
        return result;
    }

    @Override
    public void appendAttributeCode(List<EntCSVMode1> data, List<EntMstAttribute> attributeList) {
        for (EntCSVMode1 ent : data) {
            for (int i = 1; i <= attributeList.size(); i++) {
                try {
                    BeanUtils.setProperty(ent, "attributeCode" + i, Integer.parseInt(attributeList.get(i - 1).getAttributeCode()));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void appendAttributeData(List<EntCSVMode1> data, List<EntCSVMode3> attributeDataList) {
        for (EntCSVMode1 ent : data) {
            for (EntCSVMode3 attr : attributeDataList) {
                if (ent.getProductId() == attr.getProductId()) {
                    int index = getAttributeIndex(attr.getAttributeCode1(), ent);
                    if (index != -1) {
                        try {
                            BeanUtils.setProperty(ent, "attributevalues" + index, attr.getAttributeValues1());
                            BeanUtils.setProperty(ent, "attributeDisplay" + index, attr.getAttributeDisplay1());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
    }

    /**
     * Find index of attribute that contain search value.
     * @param attributeCode int
     * @param ent EntCSVMode1
     * @return int
     */
    private int getAttributeIndex(int attributeCode, EntCSVMode1 ent) {
        for (int i = 1; i <= Constant.PRODUCT_MAX_ATTRIBUTE_LENGTH; i++) {
            try {
                if (Integer.parseInt(BeanUtils.getProperty(ent, "attributeCode" + i)) == attributeCode) {
                    return i;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return -1;
    }

    @Override
    public List<EntMstAttribute> selectAttributeList() {
        List<EntMstAttribute> result = mstAttributeMapper.selectAttributeList();
        if (result == null) {
            result = new ArrayList<EntMstAttribute>();
        }
        return result;
    }

    @Override
    public String selectAttributeNameByAttributeCode(List<EntMstAttribute> attributeList, int attributeCode) {
        for (EntMstAttribute ent : attributeList) {
            if (ent.getAttributeCode().equals(attributeCode + "")) {
                return ent.getAttributeName();
            }
        }
        return attributeCode + "";
    }

    @Override
    public int selectAttributeCodeByAttributeName(List<EntMstAttribute> attributeList, String attributeName) {
        for (EntMstAttribute ent : attributeList) {
            if (ent.getAttributeName().equals(attributeName)) {
                if (StringUtils.isNumeric(ent.getAttributeCode())) {
                    return Integer.parseInt(ent.getAttributeCode());
                } else {
                    return -1;
                }
            }
        }
        return -1;
    }

    /**
     * Get data for CSV mode1 validation.
     * @author nguyen.hieu
     * @date 2014-03-10
     * @param matterNo Matter no.
     * @return List<EntCSVMode1>
     * @throws SQLException SQLException.
     */
    private List<EntCSVMode1> getDataForValidation(long matterNo) throws SQLException {
    	EntTblFactoryProductNew product = new EntTblFactoryProductNew();
    	product.setMatterNo(matterNo);
    	// Get product info list.
    	List<EntCSVMode1> productInfoList = tblFactoryProductMapper.selectProductInfoListForValidation(product);
    	// Get product image list.
    	List<EntCSVMode1> productImageList = tblFactoryProductMapper.selectProductImageListForValidation(product);
    	// Get product select list.
    	List<EntCSVMode1> productSelectList = tblFactoryProductMapper.selectProductSelectListForValidation(product);
    	// Merge all of them.
    	if (productInfoList != null && productImageList != null && productSelectList != null) {
    		appendImageSelectData(productInfoList, productImageList, "image");
        	appendImageSelectData(productInfoList, productSelectList, "option");
        	return productInfoList;
    	}
    	return Collections.emptyList();
    }

    @Override
    @Transactional
    public boolean validateCsvData(EntTblFactoryImportCondition entCondition) throws SQLException  {
    	// Get data for validation.
    	List<EntCSVMode1> dataList = getDataForValidation(entCondition.getImportConditionMatterNo());

        // Get brandList.
        List<EntMstBrand> brandList = brandMapper.selectAllBrandList();
        List<EntMstCategory> categoryList = categoryMapper.selectCategoryList();

        // Validate data.
        int productGeneralErrorCount = 0;
        int productCategoryErrorCount = 0;
        for (EntCSVMode1 elem : dataList) {
        	boolean productGeneralErrorFlg = false;
        	boolean productCategoryErrorFlg = false;

        	// Check brandCode.
        	if (!contains(brandList, elem.getProductBrandCode(), new BrandCodeChecker())) {
        		productGeneralErrorFlg = true;
        		++productGeneralErrorCount;
        	}

        	// Check categoryCode.
        	if (!contains(categoryList, elem.getProductCategoryCode(), new CategoryCodeChecker())) {
        		productCategoryErrorFlg = true;
        		++productCategoryErrorCount;
        	}

        	// Check productName.
        	if (elem.getProductName().isEmpty()) {
        		productGeneralErrorFlg = true;
        		++productGeneralErrorCount;
        	}

        	// Check productCode.
        	if (!elem.getProductCode().isEmpty() && elem.getProductWebikeCodeFlg() == 1) {
        		productGeneralErrorFlg = true;
        		++productGeneralErrorCount;
        	}

        	// Check productWebikeCodeFlg.
        	if (elem.getProductWebikeCodeFlg() == 0 && elem.getProductCode().isEmpty()) {
        		productGeneralErrorFlg = true;
        		++productGeneralErrorCount;
        	}

        	// Check groupCode.
        	if (null != elem.getProductGroupCode() && elem.getProductGroupCode() > 0 && !selectDataExists(elem)) {
        		productGeneralErrorFlg = true;
        		++productGeneralErrorCount;
        	}

        	// Check descriptionSummary.
        	if (!Utils.symbolCheck(elem.getDescriptionSummary())) {
        		productGeneralErrorFlg = true;
        		++productGeneralErrorCount;
        	}

        	// Check descriptionRemarks.
        	if (!Utils.symbolCheck(elem.getDescriptionRemarks())) {
        		productGeneralErrorFlg = true;
        		++productGeneralErrorCount;
        	}

        	// Check descriptionCaution.
        	if (!Utils.symbolCheck(elem.getDescriptionCaution())) {
        		productGeneralErrorFlg = true;
        		++productGeneralErrorCount;
        	}

        	// Check productImageThumbnailPath and getProductImageDetailPath.
        	if (!checkProductImage(elem)) {
        		productGeneralErrorFlg = true;
        		++productGeneralErrorCount;
        	}

        	// Check selectName.
        	if (!selectDataExists(elem)) {
        		productGeneralErrorFlg = true;
        		++productGeneralErrorCount;
        	}

        	// Update error flags.
        	handleUpdatingErrorFlags(entCondition, elem.getProductId(), productGeneralErrorFlg, productCategoryErrorFlg);
        }

        return productGeneralErrorCount + productCategoryErrorCount == 0;
    }

    /**
     * Update error flags.
     * @author nguyen.hieu
     * @date 2014-03-10
     * @param entCondition Condition entity.
     * @param productId Product ID.
     * @param productGeneralErrorFlg Product general error flag.
     * @param productCategoryErrorFlg Product category error flag.
     * @throws SQLException SQLException.
     */
    private void handleUpdatingErrorFlags(EntTblFactoryImportCondition entCondition, long productId,
    		boolean productGeneralErrorFlg, boolean productCategoryErrorFlg) throws SQLException {
    	// Update general error.
    	if (productGeneralErrorFlg) {
        	EntTblFactoryProductNew product = new EntTblFactoryProductNew();
        	product.setUpdatedUserId(entCondition.getUpdatedUserId());
        	product.setProductId(productId);
        	product.setProductGeneralErrorFlg(1);
        	tblFactoryProductMapper.updateProductGeneralErrorFlg(product);
    	}

    	// Update category error.
    	if (productCategoryErrorFlg) {
        	EntTblFactoryProductNew product = new EntTblFactoryProductNew();
        	product.setUpdatedUserId(entCondition.getUpdatedUserId());
        	product.setProductId(productId);
        	product.setProductCategoryErrorFlg(1);
        	tblFactoryProductMapper.updateProductCategoryErrorFlg(product);
    	}
    }

    /**
     * Check product image.
     * @author nguyen.hieu
     * @date 2014-03-10
     * @param elem CSV entity.
     * @return If product image is valid.
     */
    private static boolean checkProductImage(EntCSVMode1 elem) {
    	if (
    		(elem.getProductImageThumbnailPath1NotNull().isEmpty() ^ elem.getProductImageDetailPath1NotNull().isEmpty())
    		|| (elem.getProductImageThumbnailPath2NotNull().isEmpty() ^ elem.getProductImageDetailPath2NotNull().isEmpty())
    		|| (elem.getProductImageThumbnailPath3NotNull().isEmpty() ^ elem.getProductImageDetailPath3NotNull().isEmpty())
    		|| (elem.getProductImageThumbnailPath4NotNull().isEmpty() ^ elem.getProductImageDetailPath4NotNull().isEmpty())
    		|| (elem.getProductImageThumbnailPath5NotNull().isEmpty() ^ elem.getProductImageDetailPath5NotNull().isEmpty())
    		|| (elem.getProductImageThumbnailPath6NotNull().isEmpty() ^ elem.getProductImageDetailPath6NotNull().isEmpty())
    		|| (elem.getProductImageThumbnailPath7NotNull().isEmpty() ^ elem.getProductImageDetailPath7NotNull().isEmpty())
    		|| (elem.getProductImageThumbnailPath8NotNull().isEmpty() ^ elem.getProductImageDetailPath8NotNull().isEmpty())
    		|| (elem.getProductImageThumbnailPath9NotNull().isEmpty() ^ elem.getProductImageDetailPath9NotNull().isEmpty())
    		|| (elem.getProductImageThumbnailPath10NotNull().isEmpty() ^ elem.getProductImageDetailPath10NotNull().isEmpty())
    		|| (elem.getProductImageThumbnailPath1NotNull().isEmpty()
    			&& elem.getProductImageThumbnailPath2NotNull().isEmpty()
    			&& elem.getProductImageThumbnailPath3NotNull().isEmpty()
    			&& elem.getProductImageThumbnailPath4NotNull().isEmpty()
    			&& elem.getProductImageThumbnailPath5NotNull().isEmpty()
    			&& elem.getProductImageThumbnailPath6NotNull().isEmpty()
    			&& elem.getProductImageThumbnailPath7NotNull().isEmpty()
    			&& elem.getProductImageThumbnailPath8NotNull().isEmpty()
    			&& elem.getProductImageThumbnailPath9NotNull().isEmpty()
    			&& elem.getProductImageThumbnailPath10NotNull().isEmpty())
    			) {
    		return false;
    	}
    	return true;
    }

    /**
     * Check if select data exists.
     * @author nguyen.hieu
     * @date 2014-03-10
     * @param elem CSV entity.
     * @return If select data exists.
     */
    private static boolean selectDataExists(EntCSVMode1 elem) {
    	if ((elem.getSelectCode1() == 0 ^ elem.getSelectName1NotNull().isEmpty())
		   || (elem.getSelectCode2() == 0 ^ elem.getSelectName2NotNull().isEmpty())
		   || (elem.getSelectCode3() == 0 ^ elem.getSelectName3NotNull().isEmpty())
		   || (elem.getSelectCode4() == 0 ^ elem.getSelectName4NotNull().isEmpty())
		   || (elem.getSelectCode5() == 0 ^ elem.getSelectName5NotNull().isEmpty())
		   || (elem.getSelectCode6() == 0 ^ elem.getSelectName6NotNull().isEmpty())
		   || (elem.getSelectCode7() == 0 ^ elem.getSelectName7NotNull().isEmpty())
		   || (elem.getSelectCode8() == 0 ^ elem.getSelectName8NotNull().isEmpty())
		   || (elem.getSelectCode9() == 0 ^ elem.getSelectName9NotNull().isEmpty())
		   || (elem.getSelectCode10() == 0 ^ elem.getSelectName10NotNull().isEmpty())
		   || (elem.getSelectCode1() == 0
			   && elem.getSelectCode1() == 0
			   && elem.getSelectCode2() == 0
			   && elem.getSelectCode3() == 0
			   && elem.getSelectCode4() == 0
			   && elem.getSelectCode5() == 0
			   && elem.getSelectCode6() == 0
			   && elem.getSelectCode7() == 0
			   && elem.getSelectCode8() == 0
			   && elem.getSelectCode9() == 0
			   && elem.getSelectCode10() == 0)
    	) {
    		return false;
    	}
    	return true;
    }



    /**
     * Predicate interface for checking property in entity.
     * @author nguyen.hieu
     * @date 2014-03-10
     * @param <EntityType>
     * @param <PropertyType>
     */
    public interface Checker<EntityType, PropertyType> {
    	/**
    	 * Check entity contains property.
    	 * @param entity Entity.
    	 * @param property Property.
    	 * @return If entity contains property.
    	 */
    	boolean check(EntityType entity, PropertyType property);
    }

    /**
     * Check if entity contains property.
     * @author nguyen.hieu
     * @date 2014-03-10
     * @param collection Collection.
     * @param property Property.
     * @param checker Checker.
     * @param <EntityType> Entity type.
     * @param <PropertyType> property type.
     * @return If entity contains property.
     */
    public static<EntityType, PropertyType> boolean contains(Collection<EntityType> collection,
    		PropertyType property, Checker<EntityType, PropertyType> checker) {
    	for (EntityType elem : collection) {
    		if (checker.check(elem, property)) {
    			return true;
    		}
    	}
    	return false;
    }

    /**
     * Predicate class for checking brandCode in EntMstBrand.
     * @author nguyen.hieu
     * @date 2014-03-10
     *
     */
    public static class BrandCodeChecker implements Checker<EntMstBrand, Integer> {
    	@Override
    	public boolean check(EntMstBrand entity, Integer property) {
    		if (entity.getBrandCode() == property) {
    			return true;
    		}
    		return false;
    	}
    }

    /**
     * Predicate class for checking categoryCode in EntMstCategory.
     * @author nguyen.hieu
     * @date 2014-03-10
     *
     */
    public static class CategoryCodeChecker implements Checker<EntMstCategory, Integer> {
    	@Override
    	public boolean check(EntMstCategory entity, Integer property) {
    		if (entity.getCategoryCode().equals(String.valueOf(property))) {
    			return true;
    		}
    		return false;
    	}
    }

    /**
     * Predicate class for checking attributeName in EntMstAttribute.
     * @author nguyen.hieu
     * @date 2014-03-10
     *
     */
    public static class AttributeNameChecker implements Checker<EntMstAttribute, String> {
    	@Override
    	public boolean check(EntMstAttribute entity, String property) {
    		if (entity.getAttributeName().equals(property)) {
    			return true;
    		}
    		return false;
    	}
    }

    @Override
    public List<String> selectProductIdListByFilterForExportCSV(EntTblFactoryProductNew entProduct) throws SQLException {
    	List<String> productList = tblFactoryProductMapper.selectProductIdListByFilterForExportCSV(entProduct);
    	if (productList != null) {
    		return productList;
    	}
    	return Collections.emptyList();
    }
}
