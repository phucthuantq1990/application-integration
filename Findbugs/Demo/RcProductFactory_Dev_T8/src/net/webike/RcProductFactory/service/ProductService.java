/************************************************************************
 * File Name	： ProductService.java
 * Author		： Nguyen.Chuong
 * Version		： 1.0.0
 * Date Created	： 2013/09/24
 * Date Updated	： 2013/09/24
 * Description	： Service to get product from database.
 *
 ***********************************************************************/
package net.webike.RcProductFactory.service;

import java.sql.SQLException;
import java.util.List;

import net.webike.RcProductFactory.entity.EntCSVMode1;
import net.webike.RcProductFactory.entity.EntCSVMode3;
import net.webike.RcProductFactory.entity.EntCSVProductFitModel;
import net.webike.RcProductFactory.entity.EntMstAttribute;
import net.webike.RcProductFactory.entity.EntMstFactoryMatter;
import net.webike.RcProductFactory.entity.EntMstFactoryProductStatus;
import net.webike.RcProductFactory.entity.EntProduct;
import net.webike.RcProductFactory.entity.EntTblFactoryImportCondition;
import net.webike.RcProductFactory.entity.EntTblFactoryProductImage;
import net.webike.RcProductFactory.entity.EntTblFactoryProductModel;
import net.webike.RcProductFactory.entity.EntTblFactoryProductNew;


/**
 * Service to get matter from database.
 */
public interface ProductService {
    /************************************************************************
     * <b>Description:</b><br>
     *  select product model(maker, model, style) by product_code.
     *
     * @author      Nguyen.Chuong
     * @date        Oct 29, 2013
     * @return      List<EntTblFactoryProductModel>
     * @param       entProduct EntProduct
     ************************************************************************/
    List<EntTblFactoryProductModel> selectProductModelByProductCode(EntTblFactoryProductNew entProduct);

    /************************************************************************
     * <b>Description:</b><br>
     *  select all image and thumb by product_id.
     *
     * @author      Nguyen.Chuong
     * @date        Oct 29, 2013
     * @return      List<EntTblFactoryProductImage>
     * @param       entProduct EntProduct
     ************************************************************************/
    List<EntTblFactoryProductImage> selectListImageAndThumbByProductId(EntTblFactoryProductNew entProduct);

    /************************************************************************
     * <b>Description:</b><br>
     *  Select all value of mst_factory_product_status table.
     *
     * @author      Nguyen.Chuong
     * @date        Oct 31, 2013
     * @return      List<EntMstFactoryProductStatus>
     ************************************************************************/
    List<EntMstFactoryProductStatus> selectAllProductStatus();

     /************************************************************************
     * <b>Description:</b><br>
     * count number product of matter, count number of checked product, count error product.
     *
     * @author  Nguyen.Chuong
     * @date    Nov 4, 2013
     * @param   entProduct : entProduct contain matterNo
     *                                          , sysProductCode
     *                                          , brandCode
     *                                          , manufactoryCode
     *                                          , bunruiCode
     *                                          , productStatusCode
     * @return  EntMstFactoryMatter
     ************************************************************************/
    EntMstFactoryMatter countProductOfMatter(EntProduct entProduct);

    /************************************************************************
     * <b>Description:</b><br>
     * Update bunrui_code.
     *
     * @author  Luong.Dai
     * @date    Nov 6, 2013
     * @param   entProduct : EntProduct
     ************************************************************************/
     void updateBunruiCode(EntProduct entProduct);

     /************************************************************************
      * <b>Description:</b><br>
      * Update product_status_code.
      *
      * @author  Luong.Dai
      * @date    Nov 11, 2013
      * @param   entProduct : EntProduct
      ************************************************************************/
     void updateProductStatusCode(EntProduct entProduct);

     /************************************************************************
      * <b>Description:</b><br>
      *  Select all product by matter_no.
      *
      * @author      Luong.Dai
      * @date        Nov 12, 2013
      * @return      List<EntProduct>
      * @param       entProduct EntProduct
      * @param       pageNumber int number of page
      * @param       pageSize int number row in page
      ************************************************************************/
     List<EntProduct> selectProductListByMatterNo(EntProduct entProduct, int pageNumber, int pageSize);

     /* BOE add new function by Luong.Dai 2014/01/18 #B07 */
     /************************************************************************
      * <b>Description:</b><br>
      *  get list product by matterCode.
      *
      * @author      Luong.Dai
      * @date        Jan 18, 2014
      * @param page Integer
      * @param pageSize Integer
      * @param product EntTblFactoryProductNew
      * @throws Exception ex
      * @return      List<EntTblFactoryProductNew>
      ************************************************************************/
     List<EntTblFactoryProductNew> selectListProductByMatterCode(Integer page, Integer pageSize, EntTblFactoryProductNew product) throws Exception;
     /************************************************************************
      * <b>Description:</b><br>
      *  select data for bottom grid of page ProductFitModel.
      *
      * @author      Thai.Son
      * @date        Jan 23, 2014
      * @param  limit int
      * @param  offset int
      * @param  entTblFactoryProductNew EntTblFactoryProductNew
      * @return      List<EntTblFactoryProductNew>
      * @throws      SQLException error xml
      ************************************************************************/
     List<EntTblFactoryProductNew> selectBottomGridByMatterNoAndFilterForFitModelCheck(
     int limit, int offset, EntTblFactoryProductNew entTblFactoryProductNew) throws SQLException;
     /************************************************************************
      * <b>Description:</b><br>
      *  count record when get data from DB.      *
      * @author      Thai.Son
      * @date        Jan 16, 2014
      * @param       entTblFactoryProductNew entity contain filter
      * @return      int
      * @throws      SQLException sqlException
      ************************************************************************/
     int selectTotalRecordOfFilterBottomGrid(EntTblFactoryProductNew entTblFactoryProductNew) throws SQLException;
     /************************************************************************
     * <b>Description:</b><br>
     *  count total record of product for pagging in grid.
     *
     * @author		Luong.Dai
     * @date		Jan 20, 2014
     * @param product EntTblFactoryProductNew
     * @return		Integer
     ************************************************************************/
    Integer selectTotalRecordOfFilterProduct(EntTblFactoryProductNew product);
     /* EOE add new function by Luong.Dai */


    /************************************************************************
     * <b>Description:</b><br>
     * select list product by product list id to get model error flg.
     * @author      Long Vu
     * @date        Jan 24, 2014
     * @param       pl product id list
     * @return      List<EntTblFactoryProductModel>
     ************************************************************************/
    List<EntTblFactoryProductNew> selectProductListByProductIdList(List<Long> pl);

    /************************************************************************
     * <b>Description:</b><br>
     *  Select EntProductNew by productId at tbl_factory_product.
     *
     * @author      Nguyen.Chuong
     * @date        Jan 27, 2014
     * @param       productId productId
     * @return      EntTblFactoryProductNew
     ************************************************************************/
    EntTblFactoryProductNew selectEntProductByProductId(Long productId);

    /************************************************************************
     * <b>Description:</b><br>
     *  Select products and all of its attribute.
     *
     * @author      Doan.Chuong
     * @date        Feb 26, 2014
     * @param       productIdList List<String>
     * @param       attributeCodeList List<String>
     * @return      List<EntCSVMode3>
     ************************************************************************/
    List<EntCSVMode3> selectProductAttributeByProductIds(List<String> productIdList,
        List<String> attributeCodeList);

    /************************************************************************
     * <b>Description:</b><br>
     *  Select products and all of its fit model.
     *
     * @author      Doan.Chuong
     * @date        Feb 26, 2014
     * @param       productIdList List<String>
     * @return      List<EntCSVMode3>
     ************************************************************************/
    List<EntCSVMode3> selectProductFitModelByProductIds(List<String> productIdList);
    /************************************************************************
     * <b>Description:</b><br>
     *  Select product and fit model check information with product id list.
     *
     * @author      hoang.ho
     * @date        2014/02/26
     * @param       page current page
     * @param       pageSize page size
     * @param       searchConditions store all search conditions
     * @return      EntTblFactoryProductNew
     ************************************************************************/
    List<EntCSVProductFitModel> selectProductFitModelList(Integer page, Integer pageSize, EntCSVProductFitModel searchConditions);

    /************************************************************************
     * <b>Description:</b><br>
     *  Select count product and fit model check information with product id .
     *
     * @author      hoang.ho
     * @date        2014/02/26
     * @param       searchConditions store all search conditions
     * @return      Integer
     ************************************************************************/
    Integer selectProductFitModelCount(EntCSVProductFitModel searchConditions);

    /************************************************************************
     * <b>Description:</b><br>
     *  Select product's info for export CSV mode 1 (tables with 1-1 mapping).
     *
     * @author      Doan.Chuong
     * @date        Feb 28, 2014
     * @param       productIdList List<String>
     * @return      List<EntCSVMode1>
     ************************************************************************/
    List<EntCSVMode1> selectProductInfoExportCSVMode1SingleTable(List<String> productIdList);

    /************************************************************************
     * <b>Description:</b><br>
     *  Select product compatible model for export CSV mode 1.
     *
     * @author      Doan.Chuong
     * @date        Feb 28, 2014
     * @param       productIdList List<String>
     * @return      List<EntCSVMode1>
     ************************************************************************/
    List<EntCSVMode1> selectProductCompatibleModelByProductIds(List<String> productIdList);

    /************************************************************************
     * <b>Description:</b><br>
     *  Select product image info for export CSV mode 1.
     *
     * @author      Doan.Chuong
     * @date        Feb 28, 2014
     * @param       productIdList List<String>
     * @return      List<EntCSVMode1>
     ************************************************************************/
    List<EntCSVMode1> selectProductImageInfoByProductIds(List<String> productIdList);

    /************************************************************************
     * <b>Description:</b><br>
     *  Select product select info for export CSV mode 1.
     *
     * @author      Doan.Chuong
     * @date        Feb 28, 2014
     * @param       productIdList List<String>
     * @return      List<EntCSVMode1>
     ************************************************************************/
    List<EntCSVMode1> selectProductSelectInfoByProductIds(List<String> productIdList);

    /************************************************************************
     * <b>Description:</b><br>
     *  Select product guest input info for export CSV mode 1.
     *
     * @author      Doan.Chuong
     * @date        Feb 28, 2014
     * @param       productIdList List<String>
     * @return      List<EntCSVMode1>
     ************************************************************************/
    List<EntCSVMode1> selectProductGuestInputInfoByProductIds(List<String> productIdList);

    /************************************************************************
     * <b>Description:</b><br>
     *  Select product link info for export CSV mode 1.
     *
     * @author      Doan.Chuong
     * @date        Feb 28, 2014
     * @param       productIdList List<String>
     * @return      List<EntCSVMode1>
     ************************************************************************/
    List<EntCSVMode1> selectProductLinkInfoByProductIds(List<String> productIdList);

    /************************************************************************
     * <b>Description:</b><br>
     *  Select product video info for export CSV mode 1.
     *
     * @author      Doan.Chuong
     * @date        Feb 28, 2014
     * @param       productIdList List<String>
     * @return      List<EntCSVMode1>
     ************************************************************************/
    List<EntCSVMode1> selectProductVideoInfoByProductIds(List<String> productIdList);

    /************************************************************************
     * <b>Description:</b><br>
     *  Append data of one field from subList to mainList by product id.
     *
     * @author      Doan.Chuong
     * @date        Feb 28, 2014
     * @param       mainList List<EntCSVMode1>
     * @param       subList List<EntCSVMode1>
     * @param       fieldName String
     ************************************************************************/
    void appendDataByFieldName(List<EntCSVMode1> mainList, List<EntCSVMode1> subList, String fieldName);

    /************************************************************************
     * <b>Description:</b><br>
     *  Append data image or select info from subList to mainList by product id.
     *
     * @author      Doan.Chuong
     * @date        Feb 28, 2014
     * @param       mainList List<EntCSVMode1>
     * @param       subList List<EntCSVMode1>
     * @param       fieldName String
     ************************************************************************/
    void appendImageSelectData(List<EntCSVMode1> mainList, List<EntCSVMode1> subList, String fieldName);

    /************************************************************************
     * <b>Description:</b><br>
     *  Select attributes by list of code.
     *
     * @author      Doan.Chuong
     * @date        Feb 28, 2014
     * @param       attributeCodeList List<String>
     * @return      EntMstAttribute
     ************************************************************************/
    List<EntMstAttribute> selectAttributeByCodes(List<String> attributeCodeList);

    /************************************************************************
     * <b>Description:</b><br>
     *  Set attribute code to list data.
     *
     * @author      Doan.Chuong
     * @date        Feb 28, 2014
     * @param       data List<EntCSVMode1>
     * @param       attributeList List<EntMstAttribute>
     ************************************************************************/
    void appendAttributeCode(List<EntCSVMode1> data, List<EntMstAttribute> attributeList);

    /************************************************************************
     * <b>Description:</b><br>
     *  Set attribute data to list data.
     *
     * @author      Doan.Chuong
     * @date        Feb 28, 2014
     * @param       data List<EntCSVMode1>
     * @param       attributeDataList List<EntCSVMode1>
     ************************************************************************/
    void appendAttributeData(List<EntCSVMode1> data, List<EntCSVMode3> attributeDataList);

    /**
     * Select attribute list.
     * @return List<EntMstAttribute>
     */
    List<EntMstAttribute> selectAttributeList();

    /**
     * Select selectAttributeNameByAttributeCode.
     * @param attributeList List<EntMstAttribute>
     * @param attributeCode int
     * @return String
     */
    String selectAttributeNameByAttributeCode(List<EntMstAttribute> attributeList, int attributeCode);

    /**
     * Select selectAttributeCodeByAttributeName.
     * @param attributeList List<EntMstAttribute>
     * @param attributeName String
     * @return int
     */
    int selectAttributeCodeByAttributeName(List<EntMstAttribute> attributeList, String attributeName);

    /**
     * Valdiate CSV data.
     * @param entCondition Import condition.
     * @return If data is valid.
     * @throws SQLException SQLException.
     */
    boolean validateCsvData(EntTblFactoryImportCondition entCondition) throws SQLException;

    /**
     * Get product ID list by matterNo.
     * @author nguyen.hieu
     * @date 2014-03-10
     * @param entProduct {@link EntTblFactoryProductNew}
     * @return List<String>
     * @throws SQLException SQLException.
     */
    List<String> selectProductIdListByFilterForExportCSV(EntTblFactoryProductNew entProduct) throws SQLException;
}
