/************************************************************************
 * file name	： MatterDetailService.java
 * author		： Le.Dinh
 * version		： 1.0.0
 * date created	： 17 Jan 2014
 * date updated	： 17 Jan 2014
 * description	： MatterDetailService
 *
 ***********************************************************************/
package net.webike.RcProductFactory.service;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import net.webike.RcProductFactory.entity.EntCSVMode1;
import net.webike.RcProductFactory.entity.EntInfoCSVImport;
import net.webike.RcProductFactory.entity.EntMstAttribute;
import net.webike.RcProductFactory.entity.EntMstFactoryMatterNew;
import net.webike.RcProductFactory.entity.EntTblFactoryImportCondition;
import net.webike.RcProductFactory.entity.EntTblFactoryProductNew;
import net.webike.RcProductFactory.entity.EntTblTmpCSV;

/**
 * Class process all services for matter detail page.
 */
public interface MatterDetailService {

    /* BOE by Luong.Dai 2014/01/18 #B07 */
    /************************************************************************
     * <b>Description:</b><br>
     *  select matter by code.
     *
     * @author		Luong.Dai
     * @date		Jan 18, 2014
     * @param       matterNo Long
     * @return		EntMstFactoryMatterNew
     ************************************************************************/
    EntMstFactoryMatterNew selectFactoryMatterByMatterCode(Long matterNo);

    /************************************************************************
     * <b>Description:</b><br>
     *  update matter data.
     *
     * @author		Luong.Dai
     * @date		Jan 20, 2014
     * @param matter EntMstFactoryMatterNew
     * @return		Integer
     * @throws SQLException     Integer
     ************************************************************************/
    Integer updateMatterDataByMatterCode(EntMstFactoryMatterNew matter) throws SQLException;
    /* EOE by Luong.Dai */

    /* BOE Add loginId param @rcv!nguyen.hieu 2014/03/11. */
    //boolean deleteProducts(String[] arrayProducts, String[] arrayUpdatedOn);
    /**
     * Delete products.
     * @param arrayProducts Product array.
     * @param arrayUpdatedOn Updated on array.
     * @param loginId LoginId.
     * @param entProductFilter EntTblFactoryProductNew.
     * @param deleteReleaseProduct flag to deleted release product
     * @return boolean
     */
    boolean deleteProducts(String[] arrayProducts, String[] arrayUpdatedOn, String loginId,
    		EntTblFactoryProductNew entProductFilter, String deleteReleaseProduct);
    /* EOE Add loginId param. */

    /************************************************************************
     * <b>Description:</b><br>
     *  Select list attribute by productId from tbl_factory_product_attribute_[type].
     *     - tbl_factory_product_attribute_group
     *     - tbl_factory_product_attribute_flag
     *     - tbl_factory_product_attribute_integer
     *     - tbl_factory_product_attribute_string
     *     - tbl_factory_product_attribute_float
     *
     * @author      Luong.Dai
     * @date        Feb 14, 2014
     * @param       product EntTblFactoryProductNew
     * @return      List<EntMstAttribute>
     ************************************************************************/
    List<EntMstAttribute> selectListAttributeByProductId(EntTblFactoryProductNew product);

    /************************************************************************
     * <b>Description:</b><br>
     *  read CSV file to get list header for import CSV mode 1.
     *
     * @author		Nguyen.Chuong
     * @date		Mar 5, 2014
     * @param       csvFile String file path and name
     * @param       splitSymbol String symbol to split
     * @param       boundedFieldSymbol String bound the value
     * @return		List<String> list header
     ************************************************************************/
    List<String> readCSVHeaderMode1(String csvFile, String splitSymbol, String boundedFieldSymbol);

    /************************************************************************
     * <b>Description:</b><br>
     *  read CSV file to get list ent contain for import CSV mode 1.
     *
     * @author		Nguyen.Chuong
     * @date		Mar 5, 2014
     * @param       csvFile String file path and name
     * @param       listCSVHeaderMode1 List<Integer> number of header
     * @param       matterNo matterNo
     * @param       errorTableName errorTableName
     * @param       outputInfoCSVImport contains some params.
     *                  outTotalRecords total records.
     *                  outKnowError count know error records
     *                  outUnknowError unknown error records
     *                  outKnowErrorIds know error record ids
     *                  outUnknowErrorIds unknown error record ids
     * @param       splitSymbol String symbol to split
     * @param       boundedFieldSymbol String bound the value
     * @param		loginId loginId
     * @return		Map<Object, Object> Map result
     * 				map result 0 : Status read CSV file
     * 									Status 0 : WRONG_HEADER
     *  								Status 1 : Import over 25000 product
     *  								Status 2 : When import success, over 25000 product
     * 				map result 1 : List CSV Mode1 read from CSV file
     * @throws		Exception 	exception when import
     ************************************************************************/
//    List<EntCSVMode1> readCSVContainMode1(String csvFile, List<String> listCSVHeaderMode1
//                                        , long matterNo
//                                        , String errorTableName
//                                        , EntInfoCSVImport outputInfoCSVImport
//                                        , String splitSymbol
//                                        , String boundedFieldSymbol);

    Map<Object, Object> readCSVContainMode1(String csvFile, List<String> listCSVHeaderMode1
			            , long matterNo
			            , String errorTableName
			            , EntInfoCSVImport outputInfoCSVImport
			            , String splitSymbol
			            , String boundedFieldSymbol
			            , String loginId) throws Exception;


    /************************************************************************
     * <b>Description:</b><br>
     *  read contain of CSV file and return list.
     *
     * @author		Nguyen.Chuong
     * @date		Feb 26, 2014
     * @param       csvFile CSV file path and name
     * @param       mode current mode in UI
     * @param       matterNo matterNo
     * @param       errorTableName errorTableName
     * @param       outputInfoCSVImport contains some params.
     *                  outTotalRecords total records.
     *                  outKnowError count know error records
     *                  outUnknowError unknown error records
     *                  outKnowErrorIds know error record ids
     *                  outUnknowErrorIds unknown error record ids
     * @param       splitSymbol String symbol to split
     * @param       boundedFieldSymbol String bound the value
     * @return      List<EntTblTmpCSVMode3> contain data in csv with format insert to Db
     ************************************************************************/
    List<EntTblTmpCSV> readCSVFile(String csvFile, int mode, long matterNo
                                 , String errorTableName
                                 , EntInfoCSVImport outputInfoCSVImport
                                 , String splitSymbol
                                 , String boundedFieldSymbol);
    /************************************************************************
     * <b>Description:</b><br>
     * Validate required field for mode 1.
     *
     * @author		Luong.Dai
     * @date		May 14, 2014
     * @param 		listHeader 		List header
     * @return		boolean
     ************************************************************************/
    boolean validateRequiredHeaderFieldMode1(List<String> listHeader);
    /************************************************************************
     * <b>Description:</b><br>
     *  Check header of CSV file is valid.
     *
     * @author      Nguyen.Chuong
     * @date        Feb 26, 2014
     * @param       csvFile CSV file path
     * @param       splitSymbol String symbol to split
     * @param       boundedFieldSymbol String bound the value
     * @return      Boolean
     ************************************************************************/
    Boolean checkHeaderCSVFile(String csvFile, String splitSymbol, String boundedFieldSymbol);

    //BOE #7864 Tran.Thanh 2014/05/27 : add loginId param to get temporary table.
    /************************************************************************
     * <b>Description:</b><br>
     *  Table used for insert value CSV into database.
     *
     * @author      hoang.ho
     * @date        2014/02/27
     * @param       tableName table name will be create
     * @param       mode int imported mode
     * @param       loginId login Id
     * @return      Boolean insert status
     ************************************************************************/
    Boolean createTemporaryTable(String tableName, int mode, String loginId);
    //Boolean createTemporaryTable(String tableName, int mode);
    //BOE #7864 Tran.Thanh 2014/05/27 : add loginId to get temporary table.

    /************************************************************************
     * <b>Description:</b><br>
     *  insert list product in CSV file to DB mode 3.
     *
     * @author		Nguyen.Chuong
     * @date		Feb 26, 2014
     * @param       listTmpCSV list tempCSV to insert DB
     * @param       tableName table name will be create
     * @param       mode int imported mode
     * @return		Boolean insert status
     ************************************************************************/
    Boolean insertListProductCSVToDB(List<EntTblTmpCSV> listTmpCSV, String tableName, int mode);

    /************************************************************************
     * <b>Description:</b><br>
     *  insert list product in CSV file to DB mode 1.
     *
     * @author      Nguyen.Chuong
     * @date        Feb 26, 2014
     * @param       listTmpCSVMode1 list EntCSVMode1 to insert DB
     * @param       tableName table name will be create
     * @return      Boolean insert status
     ************************************************************************/
    Boolean insertListProductCSVToDB(List<EntCSVMode1> listTmpCSVMode1, String tableName);

    /************************************************************************
     * <b>Description:</b><br>
     *  Check import condition is not working.
     *
     * @author      hoang.ho
     * @date        2014/02/27
     * @param       matterNo matter number.
     * @return      Boolean insert status
     ************************************************************************/
    Boolean checkImportConditionNotWorking(long matterNo);
    /************************************************************************
     * <b>Description:</b><br>
     *.
     * @author      Thai.Son
     * @date        Jan 23, 2014
     * @param  importedCSVTableName String
     * @param  limit int
     * @param  offset int
     * @param  entCSVMode1 EntCSVMode1
     * @return      List<EntTblFactoryProductNew>
     * @throws      SQLException error xml
     ************************************************************************/
    List<EntCSVMode1> selectListTempDataForGridMode1(String importedCSVTableName, int limit, int offset,
    EntCSVMode1 entCSVMode1) throws SQLException;
    /************************************************************************
     * <b>Description:</b><br>
     *  count record when get data from DB.      *
     * @author      Thai.Son
     * @date        Jan 16, 2014
     * @param       importedCSVTableName String
     * @param       entCSVMode1 EntCSVMode1 contain filter
     * @return      int
     * @throws      SQLException sqlException
     ************************************************************************/
     int selectTotalRecordForGridMode1(String importedCSVTableName, EntCSVMode1 entCSVMode1) throws SQLException;
    /************************************************************************
     * <b>Description:</b><br>
     *  Select list entTmpCSV tmp data from db.
     *
     * @author		Nguyen.Chuong
     * @date		Feb 28, 2014
     * @param       importedCSVTableName String
     * @param       entCSVMode3Filter EntProduct
     * @param       limit int
     * @param       offset int
     * @param       mode int imported mode
     * @return		List<Integer> list rows
     ************************************************************************/
    List<EntTblTmpCSV> selectListTmpDataInDB(String importedCSVTableName, EntTblTmpCSV entCSVMode3Filter, int limit, int offset, int mode);

    /************************************************************************
     * <b>Description:</b><br>
     *  total select filter list product imported to DB mode 3.
     *
     * @author      Nguyen.Chuong
     * @date        Mar 4, 2014
     * @param       importedCSVTableName String
     * @param       entCSVMode3Filter EntTblTmpCSVMode3 filter object
     * @param       mode int imported mode
     * @return      int total record
     ************************************************************************/
    int selectTotalListImportedCSVFilter(String importedCSVTableName, EntTblTmpCSV entCSVMode3Filter, int mode);
    /************************************************************************
     * <b>Description:</b><br>
     * process insert, update, delete in database.
     * @author      ho.hoang
     * @date        2014/03/05
     * @param       entTblTmpCSVList list product to reflect in DB
     * @param       importMode all, fit model, attribute
     * @param       importConditionEnt import condition value to insert
     * @param       errorTableName errorTableName
     * @param       splitSymbol String symbol to split
     * @param       boundedFieldSymbol String bound the value
     * @param       tableName tableName
     * @exception   Exception exception
     ************************************************************************/
    void processReflectCSVData(List<EntTblTmpCSV> entTblTmpCSVList, int importMode
        , EntTblFactoryImportCondition importConditionEnt, String tableName, String errorTableName
        , String splitSymbol, String boundedFieldSymbol) throws Exception;

    /************************************************************************
     * <b>Description:</b><br>
     *  Select list entTmpCSV tmp data from db.
     *
     * @author      hoang.ho
     * @date        2014/03/05
     * @param       mode mode
     * @param       importedCSVTableName String
     * @return      List<Integer> list rows
     ************************************************************************/
    List<EntTblTmpCSV> selectAllListTmpDataInDB(String importedCSVTableName, int mode);
     /************************************************************************
     * <b>Description:</b><br>
     *  Drop table in database.
     *
     * @param       mode import Mode all, fit model, attribute
     * @param       importedCSVTableName importedCSVTableName
     * @param       errorCSVTableName errorTableName
     ************************************************************************/
    void dropCSVTempTable(int mode, String importedCSVTableName, String errorCSVTableName);

    /************************************************************************
     * <b>Description:</b><br>
     * process insert, update, delete in database.
     * @author      Doan.Chuong
     * @date        2014/03/07
     * @param       dataList list product to import
     * @param       importMode all, fit model, attribute
     * @param       importConditionEnt import condition value to insert
     * @param       errorTableName errorTableName
     * @param       splitSymbol String symbol to split
     * @param       boundedFieldSymbol String bound the value
     * @param       tableName tableName
     * @param		matterNo matterNo
     * @param 		loginId loginId
     * @exception   Exception exception
     ************************************************************************/
    void importCSVDataMode1(List<List<EntCSVMode1>> dataList, int importMode
        , EntTblFactoryImportCondition importConditionEnt, String tableName, String errorTableName
        , String splitSymbol, String boundedFieldSymbol, Long matterNo, String loginId) throws Exception;

    /************************************************************************
     * <b>Description:</b><br>
     *  update table numbering by total new product.
     *
     * @author		Tran.Thanh
     * @date		May 16, 2014
     * @param		dataList list data from view client
     * @param 		countNewProductInCSVFile number of new product in csv file.
     * @return		int
     * @exception   SQLException SQLException
     ************************************************************************/
    int updateNumbering(List<List<EntCSVMode1>> dataList, int countNewProductInCSVFile) throws SQLException;
}
