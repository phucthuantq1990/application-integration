/**
 * modelServiceImpl
 */
package net.webike.RcProductFactory.service;

import java.util.List;

import net.webike.RcProductFactory.action.form.FitModelCheckActionForm;
import net.webike.RcProductFactory.entity.EntMstModel;
import net.webike.RcProductFactory.mapper.MstModelMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 * @author Nguyen.Chuong
 * modelServiceImpl implements modelService
 */
@Service("modelService")
//@Transactional
public class ModelServiceImpl implements ModelService {
    @Autowired
    private MstModelMapper mstModelMapper;

    @Override
    public List<EntMstModel> selectListModel(int pageSize, int pageNumber, EntMstModel entMstModel) {
        //Number of rows each page will load
        int limit = pageSize;

        int offset = (pageNumber - 1) * limit;

        List<EntMstModel> listModel = mstModelMapper.selectListModel(limit, offset, entMstModel);
        return listModel;
    }

    @Override
    public int selectListModelCount(EntMstModel entMstModel) {
        int listModelCount = mstModelMapper.selectListModelCount(entMstModel);
        return listModelCount;
    }

    @Override
    public List<String> selectModelFilterList(FitModelCheckActionForm fitModelCheckActionForm) {
        String autoCompleteField = fitModelCheckActionForm.getAutoCompleteField();
        EntMstModel entMstModel = fitModelCheckActionForm.getEntMstModel();
        List<String> listModel = mstModelMapper.selectModelFilterList(autoCompleteField, entMstModel);
        return listModel;
    }

    @Override
    public List<EntMstModel> selectAllListModel() {
        List<EntMstModel> result = null;
        try {
            result = mstModelMapper.selectAllListModel();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

}
