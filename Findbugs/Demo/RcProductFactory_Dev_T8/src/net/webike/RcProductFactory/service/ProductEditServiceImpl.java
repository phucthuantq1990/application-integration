/************************************************************************
 * file name	： ProductManageServiceImpl.java
 * author		： Le.Dinh
 * version		： 1.0.0
 * date created	： 11 Feb 2014
 * date updated	： 11 Feb 2014
 * description	： services process all for Product Manage page
 *
 ***********************************************************************/
package net.webike.RcProductFactory.service;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
/* BOE global process by  Dinh */
/* EOE global process by  Dinh */
import java.util.Iterator;
import java.util.List;
/* BOE global process by  Thanh */
import java.util.Map;
import java.util.Set;

import net.webike.RcProductFactory.action.form.ProductEditActionForm;
import net.webike.RcProductFactory.entity.EntMstAttribute;
import net.webike.RcProductFactory.entity.EntMstAttributeGroup;
import net.webike.RcProductFactory.entity.EntMstCategory;
import net.webike.RcProductFactory.entity.EntMstFactoryMatter;
import net.webike.RcProductFactory.entity.EntMstFactoryMatterNew;
import net.webike.RcProductFactory.entity.EntMstLinkReason;
import net.webike.RcProductFactory.entity.EntMstNouki;
import net.webike.RcProductFactory.entity.EntMstProduct;
import net.webike.RcProductFactory.entity.EntMstProductSupplierStatus;
import net.webike.RcProductFactory.entity.EntMstSiire;
import net.webike.RcProductFactory.entity.EntMstSyouhinSearch;
import net.webike.RcProductFactory.entity.EntMstSyouhinSelect;
import net.webike.RcProductFactory.entity.EntReplaceElement;
import net.webike.RcProductFactory.entity.EntTblBrandConditionRule;
import net.webike.RcProductFactory.entity.EntTblFactoryProductDescription;
import net.webike.RcProductFactory.entity.EntTblFactoryProductFitModel;
import net.webike.RcProductFactory.entity.EntTblFactoryProductGeneral;
import net.webike.RcProductFactory.entity.EntTblFactoryProductGuestInput;
import net.webike.RcProductFactory.entity.EntTblFactoryProductImage;
import net.webike.RcProductFactory.entity.EntTblFactoryProductLink;
import net.webike.RcProductFactory.entity.EntTblFactoryProductNew;
import net.webike.RcProductFactory.entity.EntTblFactoryProductSelect;
import net.webike.RcProductFactory.entity.EntTblFactoryProductSupplier;
import net.webike.RcProductFactory.entity.EntTblFactoryProductSupplierPrice;
import net.webike.RcProductFactory.entity.EntTblFactoryProductVideo;
import net.webike.RcProductFactory.mapper.MstAttributeGroupMapper;
import net.webike.RcProductFactory.mapper.MstAttributeMapper;
import net.webike.RcProductFactory.mapper.MstCategoryMapper;
import net.webike.RcProductFactory.mapper.MstFactoryMatterMapper;
import net.webike.RcProductFactory.mapper.MstLinkReasonMapper;
import net.webike.RcProductFactory.mapper.MstNoukiMapper;
import net.webike.RcProductFactory.mapper.MstProductMapper;
import net.webike.RcProductFactory.mapper.MstProductSupplierStatusMapper;
import net.webike.RcProductFactory.mapper.MstSiireMapper;
import net.webike.RcProductFactory.mapper.MstSyouhinSearchMapper;
import net.webike.RcProductFactory.mapper.MstSyouhinSelectMapper;
import net.webike.RcProductFactory.mapper.TblBrandConditionRuleMapper;
import net.webike.RcProductFactory.mapper.TblFactoryProductConditionMapper;
import net.webike.RcProductFactory.mapper.TblFactoryProductDescriptionMapper;
import net.webike.RcProductFactory.mapper.TblFactoryProductGeneralMapper;
import net.webike.RcProductFactory.mapper.TblFactoryProductGuestInputMapper;
import net.webike.RcProductFactory.mapper.TblFactoryProductImageMapper;
/* EOE global process by  Thanh */
import net.webike.RcProductFactory.mapper.TblFactoryProductLinkMapper;
import net.webike.RcProductFactory.mapper.TblFactoryProductMapper;
import net.webike.RcProductFactory.mapper.TblFactoryProductModelMapper;
import net.webike.RcProductFactory.mapper.TblFactoryProductNumberingMapper;
import net.webike.RcProductFactory.mapper.TblFactoryProductSelectMapper;
import net.webike.RcProductFactory.mapper.TblFactoryProductSupplierMapper;
import net.webike.RcProductFactory.mapper.TblFactoryProductSupplierPriceMapper;
import net.webike.RcProductFactory.mapper.TblFactoryProductVideoMapper;
import net.webike.RcProductFactory.mapper.TblFactorySettingMapper;
import net.webike.RcProductFactory.mapper.TblProductAttributeMapper;
import net.webike.RcProductFactory.util.Constant;
import net.webike.RcProductFactory.util.CustomException;

import org.apache.commons.lang.StringUtils;
import org.mybatis.spring.MyBatisSystemException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Transactional;

/* BOE global process by  Dai */
/* EOE global process by  Dai */

/* BOE global process by  Hoang */
/* EOE global process by  Hoang */

/**
 * MatterManageServiceImpl.
 */
@Service("productManageService")
public class ProductEditServiceImpl extends BaseService implements ProductEditService {
	private static final int LIST_FIT_MODEL = 1;
	private static final int LIST_PRODUCT_FIT_MODEL = 2;
	private static final int FACTORY_MATTER = 3;

	@Autowired
	private TblFactoryProductGeneralMapper tblFactoryProductGeneralMapper;
	@Autowired
	private TblFactoryProductSupplierMapper tblFactoryProductSupplierMapper;
	@Autowired
	private MstSiireMapper mstSiireMapper;
	@Autowired
	private MstNoukiMapper mstNoukiMapper;
	@Autowired
	private MstProductSupplierStatusMapper mstProductSupplierStatusMapper;
	@Autowired
	private TblFactoryProductConditionMapper tblFactoryProductConditionMapper;
    @Autowired
    private TblFactoryProductModelMapper tblFactoryProductModelMapper;
    @Autowired
    private MstFactoryMatterMapper mstFactoryMatterMapper;
    @Autowired
    private TblFactoryProductNumberingMapper tblFactoryProductNumberingMapper;

	@Autowired
	private MstCategoryMapper mstCategoryMapper;

	@Autowired
	private MstAttributeMapper mstAttributeMapper;

	@Autowired
	private TblFactorySettingMapper tblFactorySettingMapper;

	@Autowired
	private TblFactoryProductDescriptionMapper tblFactoryProductDescriptionMapper;

//	@Autowired
//	private ProductService productService;

	@Autowired
	private TblFactoryProductVideoMapper tblFactoryProductVideoMapper;

	@Autowired
	private TblProductAttributeMapper tblProductAttributeMapper;

	@Autowired
	private TblFactoryProductMapper tblFactoryProductMapper;

	@Autowired
	private MstAttributeGroupMapper mstAttributeGroupMapper;

	@Autowired
	private TblFactoryProductLinkMapper tblFactoryProductLinkMapper;

	@Autowired
	private TblFactoryProductImageMapper tblFactoryProductImageMapper;

	@Autowired
	private MstLinkReasonMapper mstLinkReasonMapper;

	@Autowired
	private TblFactoryProductGuestInputMapper tblFactoryProductGuestInputMapper;

	@Autowired
	private TblBrandConditionRuleMapper tblBrandConditionRuleMapper;

	@Autowired
	private MstSyouhinSelectMapper mstSyouhinSelectMapper;

	@Autowired
	private TblFactoryProductSelectMapper tblFactoryProductSelectMapper;

	@Autowired
	private MstSyouhinSearchMapper mstSyouhinAllMapper;

	@Autowired
	private TblFactoryProductSupplierPriceMapper tblFactoryProductSupplierPriceMapper;

	@Autowired
	private MstProductMapper mstProductMapper;

	@Autowired
    private TblFactoryProductMapper factoryProductMapper;
	/* BOE Block1 */

	/* BOE global process by  Dinh */
    /* (non-Javadoc)
     * @see net.webike.RcProductFactory.service.ProductEditService#deleteProducts(java.util.List)
     */
    @Override
    @Transactional
    public boolean deleteProducts(List<EntTblFactoryProductNew> listFactoryProducSession, Long matterNo, String loginUserId) {
        boolean result = true;
        try {
            if (listFactoryProducSession == null || listFactoryProducSession.size() <= 0) {
                return false;
            }
//            EntTblFactoryMatter matter = tblFactoryMatterMapper.selectFactoryMatterByMatterNo(entTblFactoryProductFirst.getMatterNo());
            int countGeneralErrorFlgChange = 0;
            int countCategoryErrorFlgChange = 0;
            int countModelErrorFlgChange = 0;
            int countAttributeErrorFlgChange = 0;
            List<Long> deletingProductList = new ArrayList<Long>();

            for (EntTblFactoryProductNew entTblFactoryProductNew : listFactoryProducSession) {
                Long productId = entTblFactoryProductNew.getProductId();
                // Select current product info
                EntTblFactoryProductNew currentProduct = tblFactoryProductMapper.selectProductUpdatedOnByProductCode(productId);
                if (currentProduct != null) {
                    // Check change update_on value
                    Date date = entTblFactoryProductNew.getUpdatedOn();

                    if (currentProduct.getUpdatedOn().compareTo(date) == 0) {
                        // Check flag is error and down matter
                        if (currentProduct.getProductGeneralErrorFlg().intValue() > 0) {
                            countGeneralErrorFlgChange--;
                        }
                        if (currentProduct.getProductCategoryErrorFlg().intValue() > 0) {
                            countCategoryErrorFlgChange--;
                        }
                        if (currentProduct.getProductModelErrorFlg().intValue() > 0) {
                            countModelErrorFlgChange--;
                        }
                        if (currentProduct.getProductAttributeErrorFlg().intValue() > 0) {
                            countAttributeErrorFlgChange--;
                        }
//                    	// Delete ProductsGeneral
//                    	tblFactoryProductMapper.deleteProductsGeneral(productId);
//                        // Delete product
//                    	tblFactoryProductMapper.deleteProducts(productId);
                        deletingProductList.add(productId);
                    } else {
                        result = false;
                        throw new CustomException(currentProduct);
                    }
                }
            }
            int size = deletingProductList.size();
            int affectRow = tblFactoryProductMapper.deleteProductGeneralList(deletingProductList);
            if (affectRow != size) {
                result = false;
                throw new Exception();
            }
            affectRow = tblFactoryProductMapper.deleteProductList(deletingProductList);
            if (affectRow != size) {
                result = false;
                throw new Exception();
            }
            EntMstFactoryMatterNew matter = new EntMstFactoryMatterNew();
            matter.setMatterNo(matterNo);
            matter.setMatterFactoryErrorCountGeneral(countGeneralErrorFlgChange);
            matter.setMatterFactoryErrorCountCategory(countCategoryErrorFlgChange);
            matter.setMatterFactoryErrorCountModel(countModelErrorFlgChange);
            matter.setMatterFactoryErrorCountAttribute(countAttributeErrorFlgChange);
            matter.setUpdatedUserId(loginUserId);
            mstFactoryMatterMapper.updateMatterErrorFlgByMatterCode(matter);
        } catch (CustomException e) {
        	result = false;
            throw new CustomException(e.getObj());
        } catch (Exception e) {
        	result = false;
            throw new RuntimeException("other error");
        }
        return result;
    }
    /* (non-Javadoc)
     * @see net.webike.RcProductFactory.service.ProductEditService#selectListProductSameGroupNoSelect
     * (net.webike.RcProductFactory.entity.EntTblFactoryProductGeneral)
     */
    @Override
    public List<EntTblFactoryProductNew> selectListProductSameGroupNoSelect(EntTblFactoryProductGeneral productGen) {
	    List<EntTblFactoryProductNew> listProduct = new ArrayList<EntTblFactoryProductNew>();
	    if (productGen.getProductGroupCode() > 0) {
	    	listProduct = tblFactoryProductMapper.selectProductOfSameGroup(productGen);
	    	if (listProduct == null || listProduct.size() < 1) {
		    	EntTblFactoryProductNew currentProduct = tblFactoryProductMapper.selectProductUpdatedOnByProductCode(productGen.getProductId());
		    	if (currentProduct != null) {
		    		listProduct.add(currentProduct);
		    	}
	    	}
	    } else {
	    	EntTblFactoryProductNew currentProduct = tblFactoryProductMapper.selectProductUpdatedOnByProductCode(productGen.getProductId());
	    	listProduct.add(currentProduct);
		}
	    return listProduct;
    }
    /* (non-Javadoc)
     * @see net.webike.RcProductFactory.service.ProductEditService#selectMaxProductIdPlusOneInteger()
     */
    @Override
    public Long selectMaxProductIdPlusOne() throws SQLException {
		Long rs = tblFactoryProductNumberingMapper.selectMaxProductIdPlusOne();
    	return rs;
    }

    /* (non-Javadoc)
     * @see net.webike.RcProductFactory.service.ProductEditService#insertNewTblFactoryProductNumbering(java.lang.Long)
     */
    @Override
    public Integer insertNewTblFactoryProductNumbering(Long productIdMax) throws SQLException {
		Integer rs = tblFactoryProductNumberingMapper.updateTblFactoryProductNumbering(productIdMax);
    	return rs;
    }
	/* EOE global process by  Dinh */

	/* BOE global process by  Thanh */
	/* EOE global process by  Thanh */

	/* BOE global process by  Dai */
	/* EOE global process by  Dai */

	/* BOE global process by  Hoang */
	/* EOE global process by  Hoang */

	/* (non-Javadoc)
	 * @see net.webike.RcProductFactory.service.ProductEditService#checkExistProduct(java.lang.Long)
	 */
	@Override
	public EntTblFactoryProductNew checkExistProduct(Long productId) {
	    EntTblFactoryProductNew returnValue = tblFactoryProductMapper.selectProductFromProductAndProductGeneral(productId);
	    return returnValue;
	}
	@SuppressWarnings({ "unchecked", "rawtypes" })
    @Override
    @Transactional
	public boolean processSaveDataProductEdit(Map<Integer, Object> mapProduct, EntTblFactoryProductNew entQuery
			, ArrayList<Integer> errorFlg, ProductEditActionForm actionForm) {
		int isContinue = 1;
		boolean result = false;
		EntMstFactoryMatterNew matter = null;
		EntTblFactoryProductNew entProductNew = null;
		// create transaction.
        TransactionStatus status = null;
        try {
            // start transaction.
            status =  this.startTransaction();
            // check Mode is edit or new
            matter = this.initEntMstFactoryMatterNewToCountError(entQuery);
            if (Constant.EDIT_MODE.equals(actionForm.getMode())) {
	            // check date UpdatedOn of product.
	            entProductNew = tblFactoryProductMapper.selectProductUpdatedOnByProductCode(entQuery.getProductId());
	    		// Check if product was been deleted
	            if (entProductNew == null || entProductNew.getUpdatedOn().compareTo(entQuery.getUpdatedOn()) != 0) {
	    			this.rollback(status);
	    			return result;
	    		}
	            // Set New Error Flg to Ent has been get from DB
	            if (entProductNew.getProductGeneralErrorFlg().compareTo(errorFlg.get(0)) != 0) {
	            	matter.setMatterFactoryErrorCountGeneral(errorFlg.get(0) - entProductNew.getProductGeneralErrorFlg());
	            	entProductNew.setProductGeneralErrorFlg(errorFlg.get(0));
	            }
	            if (entProductNew.getProductCategoryErrorFlg().compareTo(errorFlg.get(1)) != 0) {
	            	matter.setMatterFactoryErrorCountCategory(errorFlg.get(0) - entProductNew.getProductGeneralErrorFlg());
	            	entProductNew.setProductCategoryErrorFlg(errorFlg.get(1));
	            }
            } else {
	            entProductNew = new EntTblFactoryProductNew();
	            entProductNew.setProductGeneralErrorFlg(errorFlg.get(0));
	            entProductNew.setProductCategoryErrorFlg(errorFlg.get(1));
	            if (errorFlg.get(0).compareTo(1) == 0) {
	            	matter.setMatterFactoryErrorCountGeneral(1);
	            }
	            if (errorFlg.get(1).compareTo(1) == 0) {
	            	matter.setMatterFactoryErrorCountCategory(1);
	            }
			}
            // Loop and excute
            Set entries = mapProduct.entrySet(); Iterator it = entries.iterator();
    	    while (it.hasNext()) {
    	    	Map.Entry entry = (Map.Entry) it.next(); Integer block = (Integer) entry.getKey();
    	    	switch (block) {
	    	    	case Constant.PRODUCT_EDIT_GENARAL_BLOCK :
	    	    		ProductEditActionForm form = (ProductEditActionForm) entry.getValue();
	    	    		form.getFactoryProductGeneral().setUpdatedUserId(entQuery.getUpdatedUserId());
	    	    		form.getFactoryProductGeneral().setProductId(entQuery.getProductId());

	    	    		if (Constant.EDIT_MODE.equals(form.getMode())) {
		    	    		// update table ProductFactoryProductGeneral
		    	    		tblFactoryProductGeneralMapper.updateProductFactoryProductGeneral(form.getFactoryProductGeneral());
		    	    		// update table FactoryProductCondition
		    	    		tblFactoryProductConditionMapper.updateFactoryProductCondition(form.getFactoryProductGeneral());
		    	    		// update table ProductDescription
		    	    		EntTblFactoryProductNew entTblFactoryProductNew = this.initProductNewToUpdateProductDescription(form, entQuery);
							tblFactoryProductDescriptionMapper.updateProductDescriptionByProduct(entTblFactoryProductNew);
							//Update supplierPrice of product
							if (form.getFlgToGetPriceBlock9() == 0) {
    							EntTblFactoryProductNew product = new EntTblFactoryProductNew();
    							product.setProductId(form.getFactoryProductGeneral().getProductId());
    							product.setSupplierPricePrice(form.getFactoryProductGeneral().getSupplierPricePrice());
    							product.setUpdatedUserId(entQuery.getUpdatedUserId());
    							updateSupplierPriceByProductId(product);
							}
	    	    		} else {
	    	    			// insert ProductId to tbl_factory_product_numbering
	    	    			tblFactoryProductNumberingMapper.updateTblFactoryProductNumbering(entQuery.getProductId());
	    	    			//insert data Product
	    	    			EntMstProduct prd = this.initEntMstProduct(entQuery);
	    	    			tblFactoryProductMapper.insertNewFactoryProduct(entQuery.getProductMatterNo(), prd);
	    	    			//insert data Product General
	    	    			tblFactoryProductGeneralMapper.insertNewFactoryProductGeneralByProductGeneral(form.getFactoryProductGeneral());
	    	    			//insert data Product Condition
	    	    			tblFactoryProductConditionMapper.insertNewFactoryProductConditionByProductGeneral(form.getFactoryProductGeneral());
	    	    			//insert data product Descriptions
		    	    		EntTblFactoryProductNew entTblFactoryProductNew = this.initProductNewToUpdateProductDescriptions(form, entQuery);
	    	    			tblFactoryProductDescriptionMapper.insertNewFactoryProductDescriptionByProduct(entTblFactoryProductNew);
	    	    			//insertNewSupplierPriceOfGroupProduct
	    	    			EntTblFactoryProductNew objToInsertSupplierPrice = this.initProductNewToInsertSupplierPrice(form, entQuery);
	    	    			tblFactoryProductSupplierPriceMapper.insertNewSupplierPriceOfGroupProduct(objToInsertSupplierPrice);
						}
						break;
	    	    	case Constant.PRODUCT_EDIT_IMAGE_BLOCK:
	    	    		List<EntTblFactoryProductImage> listImage = (List<EntTblFactoryProductImage>) entry.getValue();
	    	    		saveDataForProductOfSaveImage(listImage, entQuery, actionForm);
						break;
	    	    	case Constant.PRODUCT_EDIT_ATRIBUTE_BLOCK :
						List<EntMstAttribute> lstAttribute = (List<EntMstAttribute>) entry.getValue();
						saveDataForProductOfSaveAttribute(lstAttribute, entQuery, actionForm);
						break;
	    	    	case Constant.PRODUCT_EDIT_LINK_BLOCK:
	    	    		List<EntTblFactoryProductLink> listLink = (List<EntTblFactoryProductLink>) entry.getValue();
	    	    		saveDataForProductOfSaveLink(listLink, entQuery, actionForm);
						break;
					case Constant.PRODUCT_EDIT_VIDEO_BLOCK :
						List<EntTblFactoryProductVideo> listVideo = (List<EntTblFactoryProductVideo>) entry.getValue();
						saveDataForProductOfSaveVideo(listVideo, entQuery, actionForm);
						break;
					case Constant.PRODUCT_EDIT_GUEST_BLOCK :
						List<EntTblFactoryProductGuestInput> listGuest = (List<EntTblFactoryProductGuestInput>) entry.getValue();
						saveDataForProductOfSaveGuestInput(listGuest, entQuery, actionForm);
						break;
					case Constant.PRODUCT_EDIT_FIT_MODEL_BLOCK :
						Map<Object, Object> mapFitModel = (Map<Object, Object>) entry.getValue();
						List<EntTblFactoryProductFitModel> listFitModel
									= (List<EntTblFactoryProductFitModel>) mapFitModel.get(LIST_FIT_MODEL);
						List<EntTblFactoryProductNew> productList
									= (List<EntTblFactoryProductNew>) mapFitModel.get(LIST_PRODUCT_FIT_MODEL);
						EntMstFactoryMatter factoryMatter
									= (EntMstFactoryMatter) mapFitModel.get(FACTORY_MATTER);
						String user = entQuery.getUpdatedUserId();
						processDataFitModelBySingleProduct(listFitModel, productList, factoryMatter, user);
						break;
					case Constant.PRODUCT_EDIT_SUPPLIER_STATUS_BLOCK :
						List<EntTblFactoryProductSupplier> listSupplier = (List<EntTblFactoryProductSupplier>) entry.getValue();
						saveDataForProductOfSaveSupplierStatus(listSupplier, entQuery, actionForm);
						break;
						/* BOE by Luong.Dai move to bottom */
//					case Constant.PRODUCT_EDIT_SUPPLIER_BLOCK :
//					    saveDataForProductOfSaveGroup((Map<String, Object>) mapProduct.get(Constant.PRODUCT_EDIT_SUPPLIER_BLOCK)
//					    							  , entQuery, matter, actionForm);
//						break;
				default:
					break;
				}
    	    }
    	    // Save group product
    	    saveDataForProductOfSaveGroup((Map<String, Object>) mapProduct.get(Constant.PRODUCT_EDIT_SUPPLIER_BLOCK)
					  					, entQuery, matter, actionForm, mapProduct);
    	    if (!Constant.EDIT_MODE.equals(actionForm.getMode())) {
    	    	entProductNew.setUpdatedUserId(entQuery.getUpdatedUserId());
    	    	entProductNew.setProductId(entQuery.getProductId());
    	    }
    	    tblFactoryProductMapper.updateProductErrorFlg(entProductNew);
    	    mstFactoryMatterMapper.updateMatterErrorFlgByMatterCode(matter);
        } catch (Exception e) {
        	isContinue = 0;
            this.rollback(status);
        } finally {
            if (isContinue > 0) {
                this.commit(status);
                result = true;
            } else {
                this.rollback(status);
            }
        }

        return result;
	}

	/************************************************************************
	 * <b>Description:</b><br>
	 *  initEntMstFactoryMatterNewToCountError.
	 * @author		Le.Dinh
	 * @date		4 Mar 2014
	 * @param		entQuery	EntTblFactoryProductNew
	 * @return		EntMstFactoryMatterNew
	 ************************************************************************/
	private EntMstFactoryMatterNew initEntMstFactoryMatterNewToCountError(EntTblFactoryProductNew entQuery) {
		EntMstFactoryMatterNew    matter = new EntMstFactoryMatterNew();
		matter.setMatterFactoryErrorCountGeneral(0);
        matter.setMatterFactoryErrorCountCategory(0);
        matter.setMatterFactoryErrorCountModel(0);
        matter.setMatterFactoryErrorCountAttribute(0);
        matter.setUpdatedUserId(entQuery.getUpdatedUserId());
        matter.setMatterNo(entQuery.getProductMatterNo());
        return matter;
	}
	/************************************************************************
	 * <b>Description:</b><br>
	 *  initEntTblFactoryProductNewToUpdateProductDescription.
	 * @author		Le.Dinh
	 * @date		4 Mar 2014
	 * @param 		form	ProductEditActionForm
	 * @param 		entQuery	EntTblFactoryProductNew
	 * @return		EntTblFactoryProductNew
	 ************************************************************************/
	private EntTblFactoryProductNew initProductNewToUpdateProductDescription(ProductEditActionForm form, EntTblFactoryProductNew entQuery) {
		EntTblFactoryProductNew entTblFactoryProductNew = new EntTblFactoryProductNew();
		entTblFactoryProductNew.setDescriptionSummary(form.getProductDescription().getDescriptionSummary());
		entTblFactoryProductNew.setDescriptionCaution(form.getProductDescription().getDescriptionCaution());
		entTblFactoryProductNew.setDescriptionRemarks(form.getProductDescription().getDescriptionRemarks());
		entTblFactoryProductNew.setDescriptionSentence(form.getProductDescription().getDescriptionSentence());
		entTblFactoryProductNew.setUpdatedUserId(entQuery.getUpdatedUserId());
		entTblFactoryProductNew.setProductId(entQuery.getProductId());
		return entTblFactoryProductNew;
	}
	/************************************************************************
	 * <b>Description:</b><br>
	 *  initEntMstProduct.
	 * @author		Le.Dinh
	 * @date		4 Mar 2014
	 * @param 		entQuery	EntTblFactoryProductNew
	 * @return		EntMstProduct
	 ************************************************************************/
	private EntMstProduct initEntMstProduct(EntTblFactoryProductNew entQuery) {
		EntMstProduct prd = new EntMstProduct();
		prd.setProductId(entQuery.getProductId());
		prd.setSyouhinSysCode(entQuery.getProductSyouhinSysCode());
		prd.setCreatedUserId(entQuery.getUpdatedUserId());
		return prd;
	}
	/************************************************************************
	 * <b>Description:</b><br>
	 *  initProductNewToUpdateProductDescriptions.
	 * @author		Le.Dinh
	 * @date		4 Mar 2014
	 * @param 		form	ProductEditActionForm
	 * @param 		entQuery	EntTblFactoryProductNew
	 * @return		EntTblFactoryProductNew
	 ************************************************************************/
	private EntTblFactoryProductNew initProductNewToUpdateProductDescriptions(ProductEditActionForm form, EntTblFactoryProductNew entQuery) {
		EntTblFactoryProductNew entTblFactoryProductNew = new EntTblFactoryProductNew();
		entTblFactoryProductNew.setDescriptionSummary(form.getProductDescription().getDescriptionSummary());
		entTblFactoryProductNew.setDescriptionCaution(form.getProductDescription().getDescriptionCaution());
		entTblFactoryProductNew.setDescriptionRemarks(form.getProductDescription().getDescriptionRemarks());
		entTblFactoryProductNew.setDescriptionSentence(form.getProductDescription().getDescriptionSentence());
		entTblFactoryProductNew.setUpdatedUserId(entQuery.getUpdatedUserId());
		entTblFactoryProductNew.setProductId(entQuery.getProductId());
		return entTblFactoryProductNew;
	}
	/************************************************************************
	 * <b>Description:</b><br>
	 *  initProductNewToInsertSupplierPrice.
	 * @author		Le.Dinh
	 * @date		4 Mar 2014
	 * @param 		form	ProductEditActionForm
	 * @param 		entQuery	EntTblFactoryProductNew
	 * @return		EntTblFactoryProductNew
	 ************************************************************************/
	private EntTblFactoryProductNew initProductNewToInsertSupplierPrice(ProductEditActionForm form, EntTblFactoryProductNew entQuery) {
		EntTblFactoryProductNew entTblFactoryProductNew = new EntTblFactoryProductNew();
		entTblFactoryProductNew.setUpdatedUserId(entQuery.getUpdatedUserId());
		entTblFactoryProductNew.setProductId(entQuery.getProductId());
		entTblFactoryProductNew.setSupplierPricePrice(form.getFactoryProductGeneral().getSupplierPricePrice());
		return entTblFactoryProductNew;
	}
	/* EOE Block1 */

	/* BOE Block2 */
	/************************************************************************
	 * <b>Description:</b><br>
	 *  saveDataForProductOfSaveImage.
	 *
	 * @author		Tran.Thanh
	 * @date		3 Mar 2014
	 * @param 		listImage List<EntTblFactoryProductImage>
	 * @param 		entQuery EntTblFactoryProductNew
	 * @param 		actionForm ProductEditActionForm
	 * @return		true if no have exception.
	 * @throws SQLException		boolean
	 ************************************************************************/
	private boolean saveDataForProductOfSaveImage(List<EntTblFactoryProductImage> listImage
												 , EntTblFactoryProductNew entQuery
												 , ProductEditActionForm actionForm) throws SQLException {
		tblFactoryProductImageMapper.deleteAllProductImageByProductId(entQuery.getProductId());
		for (int i = 0; i < listImage.size(); i++) {
			listImage.get(i).setUpdatetedUserId(entQuery.getUpdatedUserId());
			listImage.get(i).setProductId(entQuery.getProductId());
			if (Constant.EDIT_MODE.equals(actionForm.getMode())) {
//				if (StringUtils.equals(listImage.get(i).getCrudFlg(), Constant.DELETE_MODE)) {
//					//tblFactoryProductImageMapper.deleteProductLinkByProductIdAndProductLinkSort(listImage.get(i));
//					tblFactoryProductImageMapper.deleteProductImageByProductIdAndProductImageSort(listImage.get(i));
//				} else if (StringUtils.equals(listImage.get(i).getCrudFlg(), Constant.INSERT_MODE)) {
//					tblFactoryProductImageMapper.insertFactoryProductImage(listImage.get(i));
//				} else if (StringUtils.equals(listImage.get(i).getCrudFlg(), Constant.UPDATE_MODE)) {
//					tblFactoryProductImageMapper.updateProductImage(listImage.get(i));
//				}
				tblFactoryProductImageMapper.insertFactoryProductImage(listImage.get(i));
			} else {
				if (!StringUtils.equals(listImage.get(i).getCrudFlg(), Constant.DELETE_MODE)) {
					tblFactoryProductImageMapper.insertFactoryProductImage(listImage.get(i));
				}
			}
		}
		return true;
	}

	@Override
	public String selectUrlPathFromSettingTable() {
		String urlPF = tblFactorySettingMapper.selectSettingValueBySettingCode(Constant.SETTING_PRODUCT_FACTORY_URL);
		if (StringUtils.isNotEmpty(urlPF)) {
			return urlPF;
		}
		return "";
	}
	/* EOE Block2 */

	/* BOE Block3 */
	/* EOE Block3 */

	/* BOE Block4 */
	/* (non-Javadoc)
	 * @see net.webike.RcProductFactory.service.ProductManageService#selectListAttributeOfProduct(EntTblFactoryProductNew)
	 */
	@Override
	public List<EntMstAttribute> selectListAttributeOfProduct(EntTblFactoryProductNew product) throws SQLException {
	    List<EntMstAttribute> lstAttr = new ArrayList<EntMstAttribute>();
	    if (!StringUtils.isEmpty(product.getCategoryCode())) {
	        //Select list attribute by categoryCode
	        lstAttr = mstAttributeMapper.selectAttributeByCategoryCode(product);
	    }
	    //Select list attribute by productId from tbl_factory_product_attribute_[type]
	    List<EntMstAttribute> lstAttrType = selectListAttributeByProductId(product);
	    //Merge list attribute of category and list attribute of attributeType
	    lstAttr = mergeListAttrOfCatAndType(lstAttr, lstAttrType);
	    //Get values and display value by attributeType
	    if (lstAttr != null) {
	        int size = lstAttr.size();

	        for (int i = 0; i < size; i++) {
	            //Get attributeTypeValue and attributeTypeDisplay
	            EntMstAttribute attribute = selectAttributeValueByAttributeType(lstAttr.get(i), product);
	            if (attribute != null) {
	                lstAttr.get(i).setAttributeValues(attribute.getAttributeValues());
	                lstAttr.get(i).setAttributeDisplay(attribute.getAttributeDisplay());
	                lstAttr.get(i).setAttributeGroupCode(attribute.getAttributeGroupCode());
	                /* BOE Tran-Thanh 26/02/2014 : add rowId for each element */
	                lstAttr.get(i).setIdRow("Attr" + i);
	                lstAttr.get(i).setAttributeSort(attribute.getAttributeSort());
	                /* EOE Tran-Thanh 26/02/2014 : add rowId for each element */
	            }
	        }
	    }

	    return lstAttr;
	}

	/* (non-Javadoc)
	 * @see net.webike.RcProductFactory.service.ProductEditService#selectAllListAttribute()
	 */
	@Override
	public List<EntMstAttribute> selectAllListAttribute() {
	    List<EntMstAttribute> listResutl = mstAttributeMapper.selectAllListAttribute();

	    //Add first option, blank data
	    EntMstAttribute attribute = new EntMstAttribute();
	    listResutl.add(0, attribute);

	    return listResutl;
	}
	/************************************************************************
	 * <b>Description:</b><br>
	 *  Select attribute value and attribute_display by attribute_type.
	 *
	 * @author		Luong.Dai
	 * @date		Feb 12, 2014
	 * @param       attribute		EntMstAttribute
	 * @param       product         EntTblFactoryProductNew
	 * @return      EntMstAttribute attribute value
	 ************************************************************************/
	private EntMstAttribute selectAttributeValueByAttributeType(EntMstAttribute attribute, EntTblFactoryProductNew product) {
	    //Set attributeCode to entity
        product.setAttributeCode(attribute.getAttributeCode());
        EntMstAttribute attrResult = null;
        if (attribute.getAttributeType().equals("Flag")) {
            //Get attribute_flag_values
            attrResult = mstAttributeMapper.selectAttributeFlagValues(product);
        } else if (attribute.getAttributeType().equals("Float")) {
            //Get attribute_float_values
            attrResult = mstAttributeMapper.selectAttributeFloatValues(product);
        } else if (attribute.getAttributeType().equals("Group")) {
            //Get attribute_group_name
            attrResult = mstAttributeMapper.selectAttributeGroupValues(product);
        } else if (attribute.getAttributeType().equals("Integer")) {
            //Get attribute_integer_values
            attrResult = mstAttributeMapper.selectAttributeIntegerValues(product);
        } else {
            //Get attribute_string_values
            attrResult = mstAttributeMapper.selectAttributeStringValues(product);
        }
        if (attrResult == null) {
            attrResult = new EntMstAttribute();
        }
        //If values and display is null, set value to blank
        if (StringUtils.isEmpty(attrResult.getAttributeDisplay())) {
            attrResult.setAttributeDisplay("");
        }
        if (StringUtils.isEmpty(attrResult.getAttributeValues())) {
            attrResult.setAttributeValues("");
        }
        if (attrResult.getAttributeSort() == null) {
            attrResult.setAttributeSort(0);
        }
        if (attrResult.getAttributeGroupCode() == null) {
            attrResult.setAttributeGroupCode("0");
        }
        return attrResult;
	}

	/************************************************************************
	 * <b>Description:</b><br>
	 *  Select list attribute by productId from tbl_factory_product_attribute_[type].
	 *     - tbl_factory_product_attribute_group
	 *     - tbl_factory_product_attribute_flag
	 *     - tbl_factory_product_attribute_integer
	 *     - tbl_factory_product_attribute_string
	 *     - tbl_factory_product_attribute_float
	 *
	 * @author		Luong.Dai
	 * @date		Feb 14, 2014
	 * @param       product EntTblFactoryProductNew
	 * @return		List<EntMstAttribute>
	 ************************************************************************/
	private List<EntMstAttribute> selectListAttributeByProductId(EntTblFactoryProductNew product) {
	    List<EntMstAttribute> lstResult = new ArrayList<EntMstAttribute>();

	    //tbl_factory_product_attribute_group
	    List<EntMstAttribute> lstAttrGroup = mstAttributeMapper.selectAllListAttributeByAttributeGroup(product.getProductId());
	    //tbl_factory_product_attribute_group
        List<EntMstAttribute> lstAttrFlag = mstAttributeMapper.selectAllListAttributeByAttributeFlag(product.getProductId());
        //tbl_factory_product_attribute_group
        List<EntMstAttribute> lstAttrInt = mstAttributeMapper.selectAllListAttributeByAttributeInteger(product.getProductId());
        //tbl_factory_product_attribute_group
        List<EntMstAttribute> lstAttrString = mstAttributeMapper.selectAllListAttributeByAttributeString(product.getProductId());
        //tbl_factory_product_attribute_group
        List<EntMstAttribute> lstAttrFloat = mstAttributeMapper.selectAllListAttributeByAttributeFloat(product.getProductId());

        //Add list to listResult
        if (lstAttrGroup != null) {
            lstResult.addAll(lstAttrGroup);
        }
        if (lstAttrFlag != null) {
            lstResult.addAll(lstAttrFlag);
        }
        if (lstAttrInt != null) {
            lstResult.addAll(lstAttrInt);
        }
        if (lstAttrString != null) {
            lstResult.addAll(lstAttrString);
        }
        if (lstAttrFloat != null) {
            lstResult.addAll(lstAttrFloat);
        }
	    return lstResult;
	}

	/************************************************************************
	 * <b>Description:</b><br>
	 *  merge list attribute from list attribute get by category and get by attribute_type.
	 *
	 * @author		Luong.Dai
	 * @date		Feb 14, 2014
	 * @param       lstAttr List<EntMstAttribute>
	 * @param       lstAttrType List<EntMstAttribute>
	 * @return		List<EntMstAttribute>
	 ************************************************************************/
	private List<EntMstAttribute> mergeListAttrOfCatAndType(List<EntMstAttribute> lstAttr, List<EntMstAttribute> lstAttrType) {
	    if (lstAttr == null) {
	        //New list result if current list is null
	        lstAttr = new ArrayList<EntMstAttribute>();
	    }
	    //Set attrOfCategory of list attribute = 1
	    int sizeLstCat = lstAttr.size();
	    for (int i = 0; i < sizeLstCat; i++) {
	        lstAttr.get(i).setAttrOfCategory(true);
	    }

	    //Merge list and set attrOfCategory of list attribute = 0
	    int sizeLstType = lstAttrType.size();
	    for (int i = 0; i < sizeLstType; i++) {
	        EntMstAttribute attr = lstAttrType.get(i);
	        //Search attr in listAttr
	        int j;
	        sizeLstCat = lstAttr.size();
	        for (j = 0; j < sizeLstCat; j++) {
	            //Check attribute has in list attribute get by category
	            if (attr.getAttributeCode().equals(lstAttr.get(j).getAttributeCode())) {
	                break;
	            }
	        }
	        //attr do not exists in list
	        if (j >= sizeLstCat) {
	            attr.setAttrOfCategory(false);
                lstAttr.add(attr);
	        }
	    }
	    return lstAttr;
	}

	/* (non-Javadoc)
	 * @see net.webike.RcProductFactory.service.ProductEditService#selectListAttributeGroupByAttributeCode(java.lang.String)
	 */
	@Override
	public List<EntMstAttributeGroup> selectListAttributeGroupByAttributeCode(String attributeCode) {
	    return mstAttributeGroupMapper.selectListAttrGroupByCode(Integer.parseInt(attributeCode));
	}

	/************************************************************************
	 * <b>Description:</b><br>
	 *  saveDataForProductOfSaveAttribute.
	 *
	 * @author		Tran.Thanh
	 * @date		3 Mar 2014
	 * @param 		lstAttribute List<EntMstAttribute>
	 * @param 		entQuery EntTblFactoryProductNew
	 * @param 		actionForm ProductEditActionForm
	 * @return		boolean true if no have exception.
	 * @throws 		Exception	Exception
	 ************************************************************************/
	private boolean saveDataForProductOfSaveAttribute(List<EntMstAttribute> lstAttribute
								, EntTblFactoryProductNew entQuery, ProductEditActionForm actionForm) throws Exception {
	    Integer categoryCode = actionForm.getFactoryProductGeneral().getProductCategoryCode();
	    String listDeleteAttribute = "";
		for (EntMstAttribute attribute: lstAttribute) {
			attribute.setUpdatedUserId(entQuery.getUpdatedUserId());
			attribute.setProductId(entQuery.getProductId());

			if (Constant.EDIT_MODE.equals(actionForm.getMode())) {
				//set data for attribute, if value = null, set default value
				prepareAttributeData(attribute);
				if (StringUtils.equals(attribute.getCrudFlg(), Constant.DELETE_MODE)) {
				    //Add attribute to list delete attribute
				    listDeleteAttribute = listDeleteAttribute.concat(attribute.getAttributeCode());
				    listDeleteAttribute = listDeleteAttribute.concat(",");
					//delete
					if (StringUtils.equals(attribute.getAttributeType(), "Integer")) {
						tblProductAttributeMapper.deleteFactoryAttributeIntegerByProductIdAndAttributeCode(attribute);
					} else if (StringUtils.equals(attribute.getAttributeType(), "Float")) {
						tblProductAttributeMapper.deleteFactoryAttributeFloatByProductIdAndAttributeCode(attribute);
					} else if (StringUtils.equals(attribute.getAttributeType(), "String")) {
						tblProductAttributeMapper.deleteFactoryAttributeStringByProductIdAndAttributeCode(attribute);
					} else if (StringUtils.equals(attribute.getAttributeType(), "Group")) {
						tblProductAttributeMapper.deleteFactoryAttributeGroupByProductIdAndAttributeCode(attribute);
					} else if (StringUtils.equals(attribute.getAttributeType(), "Flag")) {
						tblProductAttributeMapper.deleteFactoryAttributeFlagByProductIdAndAttributeCode(attribute);
					}
				} else if (StringUtils.equals(attribute.getCrudFlg(), Constant.INSERT_MODE)) {
					// insert
					if (StringUtils.equals(attribute.getAttributeType(), "Integer")) {
						tblProductAttributeMapper.insertFactoryAttributeInteger(attribute);
					} else if (StringUtils.equals(attribute.getAttributeType(), "Float")) {
						tblProductAttributeMapper.insertFactoryAttributeFloat(attribute);
					} else if (StringUtils.equals(attribute.getAttributeType(), "String")) {
						tblProductAttributeMapper.insertFactoryAttributeString(attribute);
					} else if (StringUtils.equals(attribute.getAttributeType(), "Group")) {
						tblProductAttributeMapper.insertFactoryAttributeGroup(attribute);
					} else if (StringUtils.equals(attribute.getAttributeType(), "Flag")) {
						tblProductAttributeMapper.insertFactoryAttributeFlag(attribute);
					}
				} else if (StringUtils.equals(attribute.getCrudFlg(), Constant.UPDATE_MODE)) {
					// update
					if (StringUtils.equals(attribute.getAttributeType(), "Integer")) {
						int result = tblProductAttributeMapper.updateFactoryAttributeInteger(attribute);
						if (result == 0) {
							tblProductAttributeMapper.insertFactoryAttributeInteger(attribute);
						}
					} else if (StringUtils.equals(attribute.getAttributeType(), "Float")) {
						int result = tblProductAttributeMapper.updateFactoryAttributeFloat(attribute);
						if (result == 0) {
							tblProductAttributeMapper.insertFactoryAttributeFloat(attribute);
						}
					} else if (StringUtils.equals(attribute.getAttributeType(), "String")) {
						int result = tblProductAttributeMapper.updateFactoryAttributeString(attribute);
						if (result == 0) {
							tblProductAttributeMapper.insertFactoryAttributeString(attribute);
						}
					} else if (StringUtils.equals(attribute.getAttributeType(), "Group")) {
						int result = tblProductAttributeMapper.updateFactoryAttributeGroup(attribute);
						if (result == 0) {
							tblProductAttributeMapper.insertFactoryAttributeGroup(attribute);
						}
					} else if (StringUtils.equals(attribute.getAttributeType(), "Flag")) {
						int result = tblProductAttributeMapper.updateFactoryAttributeFlag(attribute);
						if (result == 0) {
							tblProductAttributeMapper.insertFactoryAttributeFlag(attribute);
						}
					}
				}
			} else {
				if (!StringUtils.equals(attribute.getCrudFlg(), Constant.DELETE_MODE)) {
					//AttributeValues and attributeDisplay not emply, insert to DB
					if (StringUtils.isNotEmpty(attribute.getAttributeValues())) {
						if (StringUtils.isNotEmpty(attribute.getAttributeDisplay())
							|| StringUtils.equals(attribute.getAttributeType(), Constant.TYPE_STRING)
							|| StringUtils.equals(attribute.getAttributeType(), Constant.TYPE_FLAG)) {
							// insert
							if (StringUtils.equals(attribute.getAttributeType(), "Integer")) {
								tblProductAttributeMapper.insertFactoryAttributeInteger(attribute);
							} else if (StringUtils.equals(attribute.getAttributeType(), "Float")) {
								tblProductAttributeMapper.insertFactoryAttributeFloat(attribute);
							} else if (StringUtils.equals(attribute.getAttributeType(), "String")) {
								tblProductAttributeMapper.insertFactoryAttributeString(attribute);
							} else if (StringUtils.equals(attribute.getAttributeType(), "Group")) {
								tblProductAttributeMapper.insertFactoryAttributeGroup(attribute);
							} else if (StringUtils.equals(attribute.getAttributeType(), "Flag")) {
								tblProductAttributeMapper.insertFactoryAttributeFlag(attribute);
							}
						}
					}
				}
			}
		}
		//Delete category attribute
		if (StringUtils.isNotEmpty(listDeleteAttribute)) {
		    //Remove last ','
		    listDeleteAttribute = listDeleteAttribute.substring(0, listDeleteAttribute.length() - 1);
		    Map<String, String> deleteCode = new HashMap<String, String>();
            deleteCode.put("categoryCode", categoryCode.toString());
            deleteCode.put("attributeCode", listDeleteAttribute);
		    mstCategoryMapper.deleteCatAttributeByCode(deleteCode);
		}
		return true;
	}

	 /************************************************************************
     * <b>Description:</b><br>
     *  prepare data for attribute.
     *  If value = empty, set value = default in DB.
     *
     * @author      Luong.Dai
     * @date        Mar 12, 2014
     * @param       attribute     EntMstAttribute
     ************************************************************************/
    private void prepareAttributeData(EntMstAttribute attribute) {
        if (StringUtils.isEmpty(attribute.getAttributeValues())) {
			if (StringUtils.equals(attribute.getAttributeType(), Constant.TYPE_INTEGER)
				|| StringUtils.equals(attribute.getAttributeType(), Constant.TYPE_FLOAT)
				|| StringUtils.equals(attribute.getAttributeType(), Constant.TYPE_FLAG)) {
				//Set attribute = 0
				attribute.setAttributeValues("0");
			}
		}
    }
	/* EOE Block4 */

	/* BOE Block4 */
	/* EOE Block4 */

    /* BOE Block5 */
	@Override
	public List<EntTblFactoryProductLink> selectLinkByProductId(Long productId) {
	    //BOE #7203 Nguyen.Chuong add second param sort when get list link.
	    //Second param is '' => default sort = product_link_sort
		return tblFactoryProductLinkMapper.selectLinkByProductId(productId, "");
	    //EOE #7203 Nguyen.Chuong add second param sort when get list link.
	}

	@Override
	public List<EntMstLinkReason> selectListAllLinkReason() {
		return mstLinkReasonMapper.selectListAllLinkReason();
	}

	@Override
	public List<EntMstSyouhinSearch> selectSyouhinSearch(EntMstSyouhinSearch syouhin) {
		return mstSyouhinAllMapper.selectSyouhinSearch(syouhin);
	}

	/************************************************************************
	 * <b>Description:</b><br>
	 *  saveDataForProductOfSaveLink.
	 *
	 * @author		Tran.Thanh
	 * @date		3 Mar 2014
	 * @param 		listLink List<EntTblFactoryProductLink>
	 * @param 		entQuery EntTblFactoryProductNew
	 * @param 		actionForm ProductEditActionForm
	 * @return		true false
	 * @throws 		Exception		boolean
	 ************************************************************************/
	private boolean saveDataForProductOfSaveLink(List<EntTblFactoryProductLink> listLink
							, EntTblFactoryProductNew entQuery, ProductEditActionForm actionForm) throws Exception {
		for (int i = 0; i < listLink.size(); i++) {
			listLink.get(i).setUpdatedUserId(entQuery.getUpdatedUserId());
			listLink.get(i).setProductId(entQuery.getProductId());
			if (Constant.EDIT_MODE.equals(actionForm.getMode())) {
				if (StringUtils.equals(listLink.get(i).getCrudFlg(), Constant.DELETE_MODE)) {
					tblFactoryProductLinkMapper.deleteProductLinkByProductIdAndProductLinkSort(listLink.get(i));
				} else if (StringUtils.equals(listLink.get(i).getCrudFlg(), Constant.INSERT_MODE)) {
					tblFactoryProductLinkMapper.insertNewProductLink(listLink.get(i));
				} else {
					tblFactoryProductLinkMapper.updateProductlink(listLink.get(i));
				}
			} else {
				if (!StringUtils.equals(listLink.get(i).getCrudFlg(), Constant.DELETE_MODE)) {
					tblFactoryProductLinkMapper.insertNewProductLink(listLink.get(i));
				}
			}
		}
		return true;
	}
	/* EOE Block5 */

	/* BOE Block6 */
	@Override
	public List<EntTblFactoryProductVideo> selectVideoByProductId(Long productId) throws SQLException {
		List<EntTblFactoryProductVideo> lstProductVideo = tblFactoryProductVideoMapper.selectVideoByProductId(productId);
		if (lstProductVideo != null) {
			return lstProductVideo;
		}
		return null;
	}

	/************************************************************************
	 * <b>Description:</b><br>
	 *  saveDataForProductOfSaveVideo.
	 *
	 * @author		Tran.Thanh
	 * @date		3 Mar 2014
	 * @param 		listVideo List<EntTblFactoryProductVideo>
	 * @param 		entQuery EntTblFactoryProductNew.
	 * @param 		actionForm ProductEditActionForm
	 * @return		true if no have exception.
	 * @throws Exception		boolean
	 ************************************************************************/
	private boolean saveDataForProductOfSaveVideo(List<EntTblFactoryProductVideo> listVideo
								, EntTblFactoryProductNew entQuery, ProductEditActionForm actionForm) throws Exception {
		for (int i = 0; i < listVideo.size(); i++) {
			listVideo.get(i).setUpdatedUserId(entQuery.getUpdatedUserId());
			listVideo.get(i).setProductId(entQuery.getProductId());
			if (Constant.EDIT_MODE.equals(actionForm.getMode())) {
				if (StringUtils.equals(listVideo.get(i).getCrudFlg(), Constant.DELETE_MODE)) {
					tblFactoryProductVideoMapper.deleteProductVideoByProductIdAndProductVideoSort(listVideo.get(i));
				} else if (StringUtils.equals(listVideo.get(i).getCrudFlg(), Constant.INSERT_MODE)) {
					tblFactoryProductVideoMapper.insertNewProductVideo(listVideo.get(i));
				} else {
					tblFactoryProductVideoMapper.updateProductVideo(listVideo.get(i));
				}
			} else {
				if (!StringUtils.equals(listVideo.get(i).getCrudFlg(), Constant.DELETE_MODE)) {
					tblFactoryProductVideoMapper.insertNewProductVideo(listVideo.get(i));
				}
			}
		}
		return true;
	}
	/* EOE Block6 */

	/* BOE Block7 */
	@Override
	public EntTblFactoryProductDescription selectRemarksByProductId(Long productId) throws SQLException {
		return tblFactoryProductDescriptionMapper.selectRemarksByProductId(productId);
	}

	@Override
	public long insertNewProductDescription(EntTblFactoryProductDescription entTblFactoryProductDescription) throws SQLException {
		return tblFactoryProductDescriptionMapper.insertNewProductDescription(entTblFactoryProductDescription);
	}

	@Override
	public void updateProductDescription(EntTblFactoryProductDescription entTblFactoryProductDescription) throws SQLException {
		tblFactoryProductDescriptionMapper.updateProductDescription(entTblFactoryProductDescription);
	}
	/* EOE Block7 */

	/* BOE Block8 */
	/* (non-Javadoc)
	 * @see net.webike.RcProductFactory.service.ProductEditService#selectListProductGuestInputByProductId(java.lang.Long)
	 */
	@Override
	public List<EntTblFactoryProductGuestInput> selectListProductGuestInputByProductId(Long productId) throws SQLException {
	    //List result
	    List<EntTblFactoryProductGuestInput> listResult = tblFactoryProductGuestInputMapper.selectListProductGuestInputByProductId(productId);
	    //Return list result
	    return listResult;
	}

	/************************************************************************
	 * <b>Description:</b><br>
	 *  saveDataForProductOfSaveGuestInput.
	 *
	 * @author		Tran.Thanh
	 * @date		3 Mar 2014
	 * @param 		listGuest List<EntTblFactoryProductGuestInput> listGuest
	 * @param 		entQuery EntTblFactoryProductNew
	 * @param 		actionForm ProductEditActionForm
	 * @return		true if no have exception.
	 * @throws Exception		boolean
	 ************************************************************************/
	private boolean saveDataForProductOfSaveGuestInput(List<EntTblFactoryProductGuestInput> listGuest
								, EntTblFactoryProductNew entQuery, ProductEditActionForm actionForm) throws Exception {
		for (int i = 0; i < listGuest.size(); i++) {
			listGuest.get(i).setUpdatedUserId(entQuery.getUpdatedUserId());
			listGuest.get(i).setProductId(entQuery.getProductId());
			if (Constant.EDIT_MODE.equals(actionForm.getMode())) {
			    //Update data
			    EntTblFactoryProductGuestInput guestInput = listGuest.get(i);
			    //Check crudFlg value
			    if (StringUtils.isNotEmpty(guestInput.getCrudFlg())) {
			        if (StringUtils.equals(guestInput.getCrudFlg(), Constant.DELETE_MODE)) {
			            //Delete guestInput value
	                    tblFactoryProductGuestInputMapper.deleteProductGuestInputByProductIdAndSort(guestInput);
	                } else if (StringUtils.equals(guestInput.getCrudFlg(), Constant.INSERT_MODE)) {
	                    //Insert new guestInput
	                    tblFactoryProductGuestInputMapper.insertNewProductGuestInput(guestInput);
	                } else if (StringUtils.equals(guestInput.getCrudFlg(), Constant.UPDATE_MODE)) {
	                    //Update guestInput value
	                    tblFactoryProductGuestInputMapper.updateProductGuestInput(guestInput);
	                }
			    }
			} else {
				if (!StringUtils.equals(listGuest.get(i).getCrudFlg(), Constant.DELETE_MODE)) {
					tblFactoryProductGuestInputMapper.insertNewProductGuestInput(listGuest.get(i));
				}
			}
		}
		return true;
	}
	/* EOE Block8 */

	/* BOE Block9 */
	@Override
    public Integer selectSiireMarumeCodeByProductId(Long productId) throws SQLException {
	    Integer marumeCode = 0;
        List<EntMstSiire> listSiire = mstSiireMapper.selectSiireMarumeCodeByProductId(productId);
        //If listSirre is null, set default marumeCode
        //confirm default value
        if (listSiire == null) {
            marumeCode = 1;
        } else {
            int size = listSiire.size();
            //If listSiire has 1 element, get marumeCode of element
            if (size == 1) {
                marumeCode = listSiire.get(0).getSiireMarumeCode();
            } else {
                //ListSiire has more than 1 element, get element has supplierSyouhinSiireOrder = 1
                Boolean hasMarumeCode = false;
                for (int i = 0; i < size; i++) {
                    EntMstSiire siire = listSiire.get(i);
                    if (siire.getSupplierSyouhinSiireOrder() == 1) {
                        marumeCode = siire.getSiireMarumeCode();
                        hasMarumeCode = true;
                        break;
                    }
                }
                //Check has marumeCode
                if (!hasMarumeCode) {
                    //Set default marumeCode
                    marumeCode = 1;
                }
            }
        }
        return marumeCode;
	}
	/* (non-Javadoc)
	 * @see net.webike.RcProductFactory.service.ProductEditService#selectProductProperAndSupplierPriceByProductId(java.lang.Long)
	 */
	@Override
	public EntTblFactoryProductNew selectProductProperAndSupplierPriceByProductId(Long productId) throws SQLException {
	    EntTblFactoryProductNew product = tblFactoryProductMapper.selectProductProperAndSupplierPriceByProductId(productId);
	    //if product is null, return empty product
	    if (product == null) {
	        product = new EntTblFactoryProductNew();
	    }
	    return product;
	}

	/* (non-Javadoc)
	 * @see net.webike.RcProductFactory.service.ProductEditService#selectDefaultFlagByProductId(net.webike.RcProductFactory.entity.EntTblFactoryProductNew)
	 */
	@Override
	public EntTblBrandConditionRule selectDefaultFlagByProductId(EntTblFactoryProductNew product) throws SQLException {
	    EntTblBrandConditionRule rule = tblBrandConditionRuleMapper.selectDefaultFlagOfProduct(product);
	    return rule;
	}

	/* (non-Javadoc)
	 * @see net.webike.RcProductFactory.service.ProductEditService
	 * #selectPriceFlagByBrandCodeAndProperPrice(net.webike.RcProductFactory.entity.EntTblFactoryProductNew)
	 */
	@Override
	public EntTblBrandConditionRule selectPriceFlagByBrandCodeAndProperPrice(EntTblFactoryProductNew product) throws SQLException {
	    EntTblBrandConditionRule rule = tblBrandConditionRuleMapper.selectRuleFlagByBrandCodeAndProperPrice(product);
	    return rule;
	}

	/* (non-Javadoc)
	 * @see net.webike.RcProductFactory.service.ProductEditService#selectProductGeneralValueByProductId(java.lang.String)
	 */
	@Override
	public EntTblFactoryProductGeneral selectProductGeneralValueByProductId(String productId) throws SQLException {
	    EntTblFactoryProductGeneral productGen = tblFactoryProductGeneralMapper.selectProductGeneralValueByProductId(productId);
	    return productGen;
	}

	/* (non-Javadoc)
	 * @see net.webike.RcProductFactory.service.ProductEditService#selectListProductSameGroup(
	 *     net.webike.RcProductFactory.entity.EntTblFactoryProductGeneral)
	 */
	@Override
	public List<EntTblFactoryProductNew> selectListProductSameGroup(EntTblFactoryProductGeneral productGen,
	                                                                List<EntTblFactoryProductSelect> listSelect) throws SQLException {
		//Get list product
	    List<EntTblFactoryProductNew> listProduct = tblFactoryProductMapper.selectProductOfSameGroup(productGen);
	    if (listProduct == null) {
	        listProduct = new ArrayList<EntTblFactoryProductNew>();
	    }
	    //Calc and set supplierRate
	    listProduct = calcSupplierRate(listProduct);

	    //Select select display value and siire marume code for product
	    int size = listProduct.size();
	    for (int i = 0; i < size; i++) {
	        EntTblFactoryProductNew product = listProduct.get(i);
	        //Select select_display_value
	        selectDisplayValueForProduct(product, listSelect);
	        //Select siire marume code
	        Integer siireMarumeCode = selectSiireMarumeCodeByProductId(product.getProductId());
	        if (siireMarumeCode == null || siireMarumeCode <= 0) {
	            siireMarumeCode = 1;
	        }
	        product.setSiireMarumeCode(siireMarumeCode);
	    }
	    return listProduct;
	}
	/************************************************************************
     * <b>Description:</b><br>
     *  Select display value for product.
     *
     * @author		Luong.Dai
     * @date		Feb 21, 2014
     * @param       product EntTblFactoryProductNew
     * @param       listSelect List<EntTblFactoryProductSelect>
     ************************************************************************/
    private void selectDisplayValueForProduct(EntTblFactoryProductNew product, List<EntTblFactoryProductSelect> listSelect) {
        //Create query params
        EntTblFactoryProductSelect select = new EntTblFactoryProductSelect();
        List<EntTblFactoryProductSelect> listSelectOfProduct = new ArrayList<EntTblFactoryProductSelect>();
        select.setProductId(product.getProductId());
        //Loop in listSelect. Get display value by productId and selectCode
        int size = listSelect.size();
        for (int i = 0; i < size; i++) {
            select.setSelectCode(listSelect.get(i).getSelectCode());

            String selectDisplay = tblFactoryProductSelectMapper.selectProductSelectDisplayByProductIdAndSelectCode(select);
            if (selectDisplay == null) {
            	selectDisplay = "";
            }
            EntTblFactoryProductSelect selectResult = new EntTblFactoryProductSelect();
            selectResult.setSelectCode(select.getSelectCode());
            selectResult.setProductSelectDisplay(selectDisplay);
            selectResult.setName(listSelect.get(i).getName());
            //Add to list
            listSelectOfProduct.add(selectResult);
        }
        //Add select of product to product
        product.setLstSelectOfProduct(listSelectOfProduct);
    }

    /************************************************************************
	 * <b>Description:</b><br>
	 *  calc supplierRate: = supplier * proper / 100.
	 *
	 * @author		Luong.Dai
	 * @date		Feb 19, 2014
	 * @param       listProduct List<EntTblFactoryProductNew>
	 * @return		List<EntTblFactoryProductNew>
	 ************************************************************************/
	private List<EntTblFactoryProductNew> calcSupplierRate(List<EntTblFactoryProductNew> listProduct) {
	    if (listProduct == null) {
	        listProduct = new ArrayList<EntTblFactoryProductNew>();
	        return listProduct;
	    }

	    int size = listProduct.size();
	    //Loop in listProduct
	    for (int i = 0; i < size; i++) {
	        EntTblFactoryProductNew product = listProduct.get(i);
	        if (product.getProductProperPrice() == null || product.getSupplierPricePrice() == null) {
	            product.setSupplierRate(Float.valueOf(0));
	        } else {
	            Float rate = product.getSupplierPricePrice() * Constant.PERCENT / product.getProductProperPrice();
	            product.setSupplierRate(Math.round(rate * Constant.PERCENT) / Constant.PERCENT);
	        }
	    }
	    return listProduct;
	}

	/* (non-Javadoc)
	 * @see net.webike.RcProductFactory.service.ProductEditService#selectListSyouhinSelect()
	 */
	@Override
	public List<EntMstSyouhinSelect> selectListSyouhinSelect() throws SQLException {
	    List<EntMstSyouhinSelect> listSelect = mstSyouhinSelectMapper.selectSyouhinSelectList();
	    return listSelect;
	}

	/* (non-Javadoc)
	 * @see net.webike.RcProductFactory.service.ProductEditService#selectListSelectCodeByProductId(
	 * net.webike.RcProductFactory.entity.EntTblFactoryProductSelect)
	 */
	@Override
	public List<EntTblFactoryProductSelect> selectListSelectCodeByProductId(EntTblFactoryProductSelect select) throws SQLException {
	    List<EntTblFactoryProductSelect> listSelect = tblFactoryProductSelectMapper.selectListSelectCodeByProductId(select);
	    return listSelect;
	}

    @Override
    public List<EntTblFactoryProductSelect> selectListSelectByCodeList(String selectCodeList) throws SQLException {
        List<EntTblFactoryProductSelect> listSelect = tblFactoryProductSelectMapper.selectListSelectByCodeList(selectCodeList);
        return listSelect;
    }

    /************************************************************************
     * <b>Description:</b><br>
     *  save data for block 9.
     *
     * @author      Luong.Dai
     * @date        Mar 3, 2014
     * @param       listData        Map<String, Object>
     * @param       entQuery        EntTblFactoryProductNew
     * @param       matter          EntMstFactoryMatterNew
     * @param       actionForm      ProductEditActionForm
     * @param		mapProduct		Map has data of current product
     * @throws      Exception       Exception
     ************************************************************************/
    @SuppressWarnings("unchecked")
    private void saveDataForProductOfSaveGroup(Map<String, Object> listData
                                            , EntTblFactoryProductNew entQuery
                                            , EntMstFactoryMatterNew matter
                                            , ProductEditActionForm actionForm
                                            , Map<Integer, Object> mapProduct) throws Exception {
        //list product to save to DB
        List<EntTblFactoryProductNew> listProduct = (List<EntTblFactoryProductNew>) listData.get("ListProduct");
        //List select
        List<EntTblFactoryProductSelect> listSelect = (List<EntTblFactoryProductSelect>) listData.get("ListSelect");

        //Get productGeneral value
        EntTblFactoryProductGeneral productGeneral = (EntTblFactoryProductGeneral) listData.get("ProductGeneral");

        //Get flag edit single product or group product
        Integer flagEditGroupProduct = (Integer) listData.get("editGroupFlg");
        if (flagEditGroupProduct > 0) {
        	//Edit group product
        	saveDataForEditProductMode(entQuery, matter, actionForm, listProduct, listSelect, productGeneral, mapProduct);
        } else {
        	//Edit single product
        	if (actionForm.getMode().equals(Constant.EDIT_MODE)) {
        		//Mode edit, ungroup of all product in group
        		saveDataForEditSingleProduct(entQuery);
        	} else {
        		//Remove group code of current product
        		EntTblFactoryProductGeneral gen = new EntTblFactoryProductGeneral();
        		gen.setProductId(entQuery.getProductId());
        		gen.setUpdatedUserId(entQuery.getUpdatedUserId());
        		gen.setProductGroupCode(0L);
        		//Remove groupcode
        		tblFactoryProductGeneralMapper.updateProductGroupCodeByProductId(gen);
				//Remove all list option of product
				tblFactoryProductSelectMapper.deleteListProductSelectNotExistsInCurrentSelect(gen.getProductId(), "");
        	}
        }
    }

	/************************************************************************
	 * <b>Description:</b><br>
	 *  Get data for new product of group.
	 *
	 * @author		Luong.Dai
	 * @date		Mar 25, 2014
	 * @param 		listProductSameGroup	List<EntTblFactoryProductNew>
	 * @param		entQuery				EntTblFactoryProductNew
	 * @return		Map<String,Object>		Data for new product
	 * @throws		SQLException			Exception
	 ************************************************************************/
	private Map<Integer, Object> selectDataForNewProductInGroup(List<EntTblFactoryProductNew> listProductSameGroup
										, EntTblFactoryProductNew entQuery) throws SQLException {
		Map<Integer, Object> result = new HashMap<Integer, Object>();
		Long productId = null;
		// Get lasted product
		int numberProduct = listProductSameGroup.size();
		int i = numberProduct - 1;
		// Select product not a new product and delete product
		while (i >= 0
				&& (listProductSameGroup.get(i).getCrudFlg().equals(Constant.INSERT_MODE)
					|| listProductSameGroup.get(i).getCrudFlg().equals(Constant.DELETE_MODE))) {
			i--;
		}
		if (i >= 0) {
			//Have product
			productId = listProductSameGroup.get(i).getProductId();
			if (productId <= 0) {
				//New mdoe
				productId = entQuery.getProductId();
			}
		} else {
			return null;
		}
		//Get product value from table factory_product and product_general
		EntTblFactoryProductNew product = checkExistProduct(productId);
		if (product == null) {
			return null;
		}
		//Get list image and thumb of product
		List<EntTblFactoryProductImage> listImageAndThumb = selectListImageAndThumbByProductId(product);
		//Get list attribute
	    List<EntMstAttribute> lstAttribute = selectListAttributeOfProduct(product);
	    //Get list link of product
	    List<EntTblFactoryProductLink> listLink  = selectLinkByProductId(product.getProductId());
	    //Get list video of product
	    List<EntTblFactoryProductVideo> listFactoryProductVideo = selectVideoByProductId(product.getProductId());
	    //Load product description
	    EntTblFactoryProductDescription entProductDescription =
	    						tblFactoryProductDescriptionMapper.selectProductDescriptionByProductId(product.getProductId());
	    //Get productGuestInput of product
	    List<EntTblFactoryProductGuestInput> listProductGuestInput = selectListProductGuestInputByProductId(product.getProductId());
	    //Get list fitModel of product
	    List<EntTblFactoryProductFitModel> listFitModel = selectListFitModelOfProduct(product);
	    //Get list supplier of product
	    List<EntTblFactoryProductSupplier> listFactoryProductSupplier = selectListSupplierByProductId(product.getProductId());
	    //Get release date of product
	    EntTblFactoryProductGeneral entTblFactoryProductGeneral = selectProductFactoryProductGeneralByProductid(productId);
		//Put product to map
		result.put(Constant.PRODUCT_EDIT_GENARAL_BLOCK, 		product);
		result.put(Constant.PRODUCT_EDIT_IMAGE_BLOCK, 			listImageAndThumb);
		result.put(Constant.PRODUCT_EDIT_ATRIBUTE_BLOCK, 		lstAttribute);
		result.put(Constant.PRODUCT_EDIT_LINK_BLOCK, 			listLink);
		result.put(Constant.PRODUCT_EDIT_VIDEO_BLOCK, 			listFactoryProductVideo);
		result.put(Constant.PRODUCT_EDIT_DESCRIPTION_BLOCK, 	entProductDescription);
		result.put(Constant.PRODUCT_EDIT_GUEST_BLOCK, 			listProductGuestInput);
		result.put(Constant.PRODUCT_EDIT_FIT_MODEL_BLOCK, 		listFitModel);
		result.put(Constant.PRODUCT_EDIT_SUPPLIER_STATUS_BLOCK, listFactoryProductSupplier);
		result.put(Constant.PRODUCT_EDIT_RELEASE_DATE_BLOCK, 	entTblFactoryProductGeneral);
		return result;
	}

	/************************************************************************
	 * <b>Description:</b><br>
	 *  Select list image and thumbImage of product.
	 *
	 * @author		Luong.Dai
	 * @date		Mar 26, 2014
	 * @param 		product		EntTblFactoryProductNew
	 * @return		List<EntTblFactoryProductImage>
	 ************************************************************************/
	private List<EntTblFactoryProductImage> selectListImageAndThumbByProductId(EntTblFactoryProductNew product) {
		// Check productCode is not empty
        if (product == null) {
            return null;
        }
    	// Get MatterNo by ProductId
    	EntTblFactoryProductNew productNew = factoryProductMapper.selectMatterNoByProductId(product.getProductId());
        if (productNew == null) {
            return null;
        }

        List<EntTblFactoryProductImage> listImage = factoryProductMapper.selectListImageAndThumbByProductId(product);
        // Check has image in listImage
        if (listImage.size() > 0) {
            for (EntTblFactoryProductImage ent : listImage) {
                // Check productImage or productThumb is empty
                if (StringUtils.isEmpty(ent.getProductImage()) || StringUtils.isEmpty(ent.getProductThumb())) {
                    ent.setProductImage("");
                    ent.setProductThumb("");
                    continue;
                }
                //Format link image
                //refer to real
                if (Constant.REFER_MODE_REAL.equals(ent.getReferMode())) {
                    ent.setProductImage(Constant.IMAGE_SERVER_2 + ent.getProductImage());
                    ent.setProductThumb(Constant.IMAGE_SERVER_2 + ent.getProductThumb());
                } else if (Constant.REFER_MODE_TEMPORARY.equals(ent.getReferMode())) {
                    //Refer to temporary
                    String factoryURL = tblFactorySettingMapper.selectSettingValueBySettingCode(Constant.SETTING_PRODUCT_FACTORY_URL);
                    ent.setProductImage(factoryURL + ent.getProductImage());
                    ent.setProductThumb(factoryURL + ent.getProductThumb());
                }
            }
        }
        return listImage;
	}
	/************************************************************************
	 * <b>Description:</b><br>
	 *  Select list fit model of product.
	 *
	 * @author		Luong.Dai
	 * @date		Mar 25, 2014
	 * @param 		product		EntTblFactoryProductNew
	 * @return		List<EntTblFactoryProductFitModel>
	 ************************************************************************/
	private List<EntTblFactoryProductFitModel> selectListFitModelOfProduct(EntTblFactoryProductNew product) {
		 //Get list model of product.
        EntTblFactoryProductFitModel entProductFitModel = new EntTblFactoryProductFitModel();
        entProductFitModel.setProductId(product.getProductId());
        entProductFitModel.setSortField(Constant.FIT_MODEL_SORT_FIELD);
        entProductFitModel.setSortDir(Constant.DEFAULT_SORT_DIR);

        return tblFactoryProductModelMapper.selectListFitModelOfProduct(entProductFitModel);
	}

	/************************************************************************
	 * <b>Description:</b><br>
	 *  Remove group of product.
	 *  Remove all option of product.
	 *
	 * @author		Luong.Dai
	 * @date		Mar 20, 2014
	 * @param 		entQuery			EntTblFactoryProductNew				current product, has login userId
	 * @throws		SQLException		Exception	exception when query DB
	 ************************************************************************/
	private void saveDataForEditSingleProduct(EntTblFactoryProductNew entQuery) throws SQLException {
		EntTblFactoryProductNew product = tblFactoryProductMapper.selectProductFromProductAndProductGeneral(entQuery.getProductId());
		if (product != null) {
			//Create search param
			EntTblFactoryProductGeneral productGen = new EntTblFactoryProductGeneral();
    	    productGen.setProductId(product.getProductId());
    	    productGen.setProductBrandCode(Integer.valueOf(product.getProductBrandCode()));
    	    productGen.setProductGroupCode(product.getProductGroupCode());
    	    productGen.setProductMatterNo(product.getProductMatterNo());
			//Select list product of same group
		    List<EntTblFactoryProductNew> listGroupProduct = null;
		    if (productGen.getProductGroupCode() > 0) {
		    	listGroupProduct = tblFactoryProductMapper.selectProductOfSameGroup(productGen);
		    }
		    if (listGroupProduct != null) {
				int size = listGroupProduct.size();
				for (int i = 0; i < size; i++) {
					EntTblFactoryProductNew currentProduct = listGroupProduct.get(i);
					//Remove group of product
					EntTblFactoryProductGeneral productGeneral = new EntTblFactoryProductGeneral();
					productGeneral.setProductId(currentProduct.getProductId());
					productGeneral.setProductGroupCode(0L);
					productGeneral.setUpdatedUserId(entQuery.getUpdatedUserId());
					//Save to DB
					tblFactoryProductGeneralMapper.updateProductGroupCodeByProductId(productGeneral);
					//Remove all list option of product
					tblFactoryProductSelectMapper.deleteListProductSelectNotExistsInCurrentSelect(productGeneral.getProductId(), "");
				}
			}
		}
	}
	/************************************************************************
	 * <b>Description:</b><br>
	 *  Save product of group to DB.
	 *
	 * @author		Luong.Dai
	 * @date		Mar 20, 2014
	 * @param 		entQuery			EntTblFactoryProductNew	current product, has login userId
	 * @param 		matter				EntMstFactoryMatterNew	count and set total error
	 * @param 		actionForm			ProductEditActionForm	Get mode Insert or Update
	 * @param 		listProduct			List<EntTblFactoryProductNew>	List group product
	 * @param 		listSelect			List<EntTblFactoryProductSelect> List option of this group
	 * @param 		productGeneral		EntTblFactoryProductGeneral productGeneral has info of new product in table general
	 * @param		mapProduct			map has value of current product
	 * @throws 		Exception		Exception	Exception when query DB fails.
	 ************************************************************************/
	private void saveDataForEditProductMode(EntTblFactoryProductNew entQuery
											, EntMstFactoryMatterNew matter
											, ProductEditActionForm actionForm
											, List<EntTblFactoryProductNew> listProduct
											, List<EntTblFactoryProductSelect> listSelect
											, EntTblFactoryProductGeneral productGeneral
											, Map<Integer, Object> mapProduct) throws Exception {
		//Get data for new product
        Map<Integer, Object> mapNewProductData = selectDataForNewProductInGroup(listProduct, entQuery);
		//Get string of list select delete
        String listDeleteSelect = createListDeleteSelectForListSelect(listSelect);
		//Save data for group
		int size = listProduct.size();
		for (int i = 0; i < size; i++) {
		    EntTblFactoryProductNew product = listProduct.get(i);
		    //Delete list select has been delete by user in client
		    if (StringUtils.isNotEmpty(listDeleteSelect)) {
		        deleteSelectOfProduct(product, listDeleteSelect);
		    }
		    //Create list current select of product
		    String listCurrentSelect = createListCurrentSelectOfProduct(listSelect);
		    if (product.getCrudFlg().equals(Constant.UPDATE_MODE)) {
		    	/* BOE by Luong.Dai at 2014/04/07 change function check error flag */
//		        if (i > 0) {
//		            //Count error flag. Do not count current product
//		            countAndSetFlagErrorForProduct(product, matter);
//		        }
		    	/* EOE by Luong.Dai at 2014/04/07 change function check error flag */
		        //Update product value
		        updateProductFromGroupProduct(product, entQuery, actionForm.getMode());
		        //Delete other select not in current select
		        deleteSelectNotExistsInCurrent(product, listCurrentSelect);
		    } else if (product.getCrudFlg().equals(Constant.DELETE_MODE)) {
		        //Count error flag
		        countErrorOfProductWhenDeleteProduct(product, matter);
		        //Remove product from group of product
		        removeProductFromGroupProduct(product, entQuery);
		        //Remove list option of product
		        if (StringUtils.isNotEmpty(listCurrentSelect)) {
		            deleteSelectOfProduct(product, listCurrentSelect);
		        }
		    } else if (product.getCrudFlg().equals(Constant.INSERT_MODE)) {
		    	/* BOE by Luong.Dai at 2014/04/07 change function check error flag */
//		        if (i > 0) {
//		            //Count error flag
//		            countAndSetFlagErrorForProduct(product, matter);
//		        }
		    	/* EOE by Luong.Dai at 2014/04/07 change function check error flag */
		        //Insert new product to group
		        insertNewProductToGroup(product, entQuery, productGeneral, mapNewProductData);
		    }

		    /* BOE by Luong.Dai fix bug No.108 Save data of current product to all product in group at 2014/04/03 */
		    if (i > 0 && !product.getCrudFlg().equals(Constant.DELETE_MODE)) {
		    	//Set updated userId
		    	product.setUpdatedUserId(entQuery.getUpdatedUserId());
		    	saveDataOfCurrentProductForGroupProduct(product, entQuery, mapProduct, actionForm, matter);
		    	if (i > 0) {
		    		//Check error flag of product
		    		checkErrorFlagOfProduct(product, matter, mapProduct);
		    	}
		    	//save error count to DB
		    	tblFactoryProductMapper.updateAllErrorFlgOfProduct(product);
		    }
		    /* EOE by Luong.Dai fix bug No.108 Save data of current product to all product in group */
		}
	}

    /************************************************************************
     * <b>Description:</b><br>
     *  Save data of current product for product in group.
     *  Save descriptionSummary, caution, productCondition of block 3.
     *  Save Block 6, 7, 8, 12, 14.
     *
     * @author		Luong.Dai
     * @date		Apr 3, 2014
     * @param 		product		product to save data
     * @param 		entQuery	ent has user information
     * @param 		mapProduct	map has current information
     * @param		actionForm	Form has process information
     * @param		matter		matter for count error flag
     * @throws		Exception	Exception when execute function
     ************************************************************************/
	private void saveDataOfCurrentProductForGroupProduct(EntTblFactoryProductNew product
    													, EntTblFactoryProductNew entQuery
    													, Map<Integer, Object> mapProduct
    													, ProductEditActionForm actionForm
    													, EntMstFactoryMatterNew matter) throws Exception {
		copyProductDescriptionForGroup(product, entQuery, actionForm);
		// update table FactoryProductCondition
		copyProductConditionForGroup(product, entQuery, actionForm);
		//Save block video
		copyProductVideoForGroup(product, entQuery, mapProduct);
		//Save guest input data
		copyProductGuestInputForGroup(product, entQuery, mapProduct);
		//Save list supplier of product
		copyProductSupplierForGroup(product, entQuery, mapProduct);
    }

	/************************************************************************
	 * <b>Description:</b><br>
	 *  Save supplier data of current product to group product.
	 *
	 * @author		Luong.Dai
	 * @date		Apr 4, 2014
	 * @param 		product				product for save data
	 * @param 		entQuery			entQuery has user info
	 * @param 		mapProduct			map has current product value
	 * @throws 		SQLException		Exception
	 ************************************************************************/
	@SuppressWarnings("unchecked")
	private void copyProductSupplierForGroup(EntTblFactoryProductNew product
											, EntTblFactoryProductNew entQuery
											, Map<Integer, Object> mapProduct) throws SQLException {
		//Delete all supplier of product
		tblFactoryProductSupplierMapper.deleteAllSupplierByProductId(product.getProductId());
		//Get list supplier
		List<EntTblFactoryProductSupplier> listSupplier =
				(List<EntTblFactoryProductSupplier>) mapProduct.get(Constant.PRODUCT_EDIT_SUPPLIER_STATUS_BLOCK);
		//Save list to DB
		if (listSupplier != null) {
			for (EntTblFactoryProductSupplier supplier : listSupplier) {
				if (!supplier.getCrudFlg().equals(Constant.DELETE_MODE)) {
					supplier.setProductId(product.getProductId());
					supplier.setUpdatedUserId(entQuery.getUpdatedUserId());
					tblFactoryProductSupplierMapper.insertSupplierByProductIdAndsiireCode(supplier);
				}
			}
		}
	}

	/************************************************************************
	 * <b>Description:</b><br>
	 *  Save guest input data of current product to group product.
	 *
	 * @author		Luong.Dai
	 * @date		Apr 4, 2014
	 * @param 		product				product for save data
	 * @param 		entQuery			entQuery has user info
	 * @param 		mapProduct			map has current product value
	 ************************************************************************/
	@SuppressWarnings("unchecked")
	private void copyProductGuestInputForGroup(EntTblFactoryProductNew product
											, EntTblFactoryProductNew entQuery
											, Map<Integer, Object> mapProduct) {
		//Delete all guest input of product
		tblFactoryProductGuestInputMapper.deleteAllGuestInputOfProduct(product.getProductId());
		//Get list guest input
		List<EntTblFactoryProductGuestInput> listGuest = (List<EntTblFactoryProductGuestInput>) mapProduct.get(Constant.PRODUCT_EDIT_GUEST_BLOCK);
		//Save list guestInput
		if (listGuest != null) {
			for (EntTblFactoryProductGuestInput guest : listGuest) {
				if (!guest.getCrudFlg().equals(Constant.DELETE_MODE)) {
					guest.setProductId(product.getProductId());
					guest.setUpdatedUserId(entQuery.getUpdatedUserId());
					tblFactoryProductGuestInputMapper.insertNewProductGuestInput(guest);
				}
			}
		}
	}

	/************************************************************************
	 * <b>Description:</b><br>
	 *  Save video data of current product to group product.
	 *
	 * @author		Luong.Dai
	 * @date		Apr 4, 2014
	 * @param 		product				product for save data
	 * @param 		entQuery			entQuery has user info
	 * @param 		mapProduct			map has current product value
	 ************************************************************************/
	@SuppressWarnings("unchecked")
	private void copyProductVideoForGroup(EntTblFactoryProductNew product
										, EntTblFactoryProductNew entQuery
										, Map<Integer, Object> mapProduct) {
		//Delete all video of product
		tblFactoryProductVideoMapper.deleteAllVideoOfProduct(product.getProductId());
		//Get all video of current product
		List<EntTblFactoryProductVideo> listVideo = (List<EntTblFactoryProductVideo>) mapProduct.get(Constant.PRODUCT_EDIT_VIDEO_BLOCK);
		//Save insert list video to DB
		if (listVideo != null) {
			for (EntTblFactoryProductVideo video : listVideo) {
				if (!video.getCrudFlg().equals(Constant.DELETE_MODE)) {
					//Set productId and updatedUserId for video
					video.setProductId(product.getProductId());
					video.setUpdatedUserId(entQuery.getUpdatedUserId());
					tblFactoryProductVideoMapper.insertNewProductVideo(video);
				}
			}
		}
	}

	/************************************************************************
	 * <b>Description:</b><br>
	 *  Save product condition data of current product to group product.
	 *
	 * @author		Luong.Dai
	 * @date		Apr 4, 2014
	 * @param 		product				product for save data
	 * @param 		entQuery			entQuery has user info
	 * @param 		actionForm			has current product value
	 * @throws		SQLException		Exception
	 ************************************************************************/
	private void copyProductConditionForGroup(EntTblFactoryProductNew product
											, EntTblFactoryProductNew entQuery
											, ProductEditActionForm actionForm) throws SQLException {
		EntTblFactoryProductGeneral productGeneral = new EntTblFactoryProductGeneral();
		productGeneral.setProductId(product.getProductId());
		productGeneral.setUpdatedUserId(entQuery.getUpdatedUserId());
		productGeneral.setProductOpenPriceFlg(actionForm.getFactoryProductGeneral().getProductOpenPriceFlg());
		productGeneral.setProductProperSellingFlg(actionForm.getFactoryProductGeneral().getProductProperSellingFlg());
		productGeneral.setProductOrderProductFlg(actionForm.getFactoryProductGeneral().getProductOrderProductFlg());
		productGeneral.setProductNoReturnableFlg(actionForm.getFactoryProductGeneral().getProductNoReturnableFlg());
		productGeneral.setProductAmbiguousImageFlg(actionForm.getFactoryProductGeneral().getProductAmbiguousImageFlg());
		Integer result = tblFactoryProductConditionMapper.updateFactoryProductCondition(productGeneral);
		if (result <= 0) {
			//Insert
			tblFactoryProductConditionMapper.insertNewFactoryProductConditionByProductGeneral(productGeneral);
		}
	}

	/************************************************************************
	 * <b>Description:</b><br>
	 *  Save product description data of current product to group product.
	 *
	 * @author		Luong.Dai
	 * @date		Apr 4, 2014
	 * @param 		product				product for save data
	 * @param 		entQuery			entQuery has user info
	 * @param 		actionForm			has current product value
	 ************************************************************************/
	private void copyProductDescriptionForGroup(EntTblFactoryProductNew product
												, EntTblFactoryProductNew entQuery
												, ProductEditActionForm actionForm) {
		//Save description
    	//Create new EntQuery for product
    	EntTblFactoryProductNew currentEntQuery = new EntTblFactoryProductNew();
    	currentEntQuery.setProductId(product.getProductId());
    	currentEntQuery.setUpdatedUserId(entQuery.getUpdatedUserId());
    	// update table ProductDescription
		EntTblFactoryProductNew entTblFactoryProductNew = this.initProductNewToUpdateProductDescription(actionForm, currentEntQuery);
		tblFactoryProductDescriptionMapper.updateProductDescriptionByProduct(entTblFactoryProductNew);
		//Count error flag for matter
	}

	/************************************************************************
     * <b>Description:</b><br>
     *  Create list current select of product.
     *
     * @author		Luong.Dai
     * @date		Mar 19, 2014
     * @param 		listSelect		List<EntTblFactoryProductSelect>
     * @return		String
     ************************************************************************/
    private String createListCurrentSelectOfProduct(List<EntTblFactoryProductSelect> listSelect) {
    	String result = "";
        int size = listSelect.size();
        for (int i = 0; i < size; i++) {
            if (!listSelect.get(i).getCrudFlg().equals(Constant.DELETE_MODE)) {
            	//Option has in current list
                if (StringUtils.isNotEmpty(result)) {
                    //Add character ','
                    result = result.concat(",");
                }
                //Select has DELETE_MODE, add to string
                result = result.concat(listSelect.get(i).getSelectCode().toString());
            }
        }
        return result;
	}
    /* BOE by Luong.Dai at 2014/04/07 - Change count error function of product */
//	/************************************************************************
//     * <b>Description:</b><br>
//     *  Check warning of productCode and productEanCode.
//     *
//     * @author		Luong.Dai
//     * @date		Mar 14, 2014
//     * @param       product         EntTblFactoryProductNew
//     * @param       matter		    EntMstFactoryMatterNew
//     ************************************************************************/
//    private void countAndSetFlagErrorForProduct(EntTblFactoryProductNew product, EntMstFactoryMatterNew matter) {
//        Integer oldGeneralFlag = product.getProductGeneralErrorFlg();
//        //Check productCode
//        if (product.getProductCode() != null) {
//            if (product.getProductCode().length() > Constant.PRODUCT_MAX_PRODUCT_CODE_LENGTH) {
//                product.setProductGeneralErrorFlg(1);
//            }
//        }
//        //Check productEanCode
//        if (product.getProductEanCode() != null) {
//            if (product.getProductEanCode().length() > Constant.PRODUCT_MAX_PRODUCT_EAN_CODE_LENGTH) {
//                product.setProductGeneralErrorFlg(1);
//            }
//        }
//        //Set matter flag value
//        Integer matterGeneralCount = matter.getMatterFactoryErrorCountGeneral();
//        matterGeneralCount = matterGeneralCount + (product.getProductGeneralErrorFlg() - oldGeneralFlag);
//        matter.setMatterFactoryErrorCountGeneral(matterGeneralCount);
//    }
    /* EOE by Luong.Dai at 2014/04/07 - Change count error function of product */

    /************************************************************************
     * <b>Description:</b><br>
     *  Check warning of product.
     *
     * @author		Luong.Dai
     * @date		Mar 14, 2014
     * @param       product         EntTblFactoryProductNew
     * @param       matter		    EntMstFactoryMatterNew
     * @param		mapProduct		map has product data
     * @throws		SQLException	exception when query DB
     ************************************************************************/
    private void checkErrorFlagOfProduct(EntTblFactoryProductNew product
    									, EntMstFactoryMatterNew matter
    									, Map<Integer, Object> mapProduct) throws SQLException {
    	//Get product general value
    	EntTblFactoryProductGeneral productGen =
    						tblFactoryProductGeneralMapper.selectProductFactoryProductGeneralByProductid(product.getProductId());
    	Integer oldGeneralErrorFlg 		= product.getProductGeneralErrorFlg();
    	Integer oldCategoryErrorFlg 	= product.getProductCategoryErrorFlg();
    	Integer oldModelErrorFlg 		= product.getProductModelErrorFlg();
    	Integer oldAttributeErrorFlg 	= product.getProductAttributeErrorFlg();
    	validateMaxLengthOfProductGeneral(product, productGen);
    	//Check productVideo
    	validateMaxLengthOfVideo(product, mapProduct);
    	//Check product guest input
		validateMaxLengthOfGuestInput(product, mapProduct);
		//Check product supplier
		validateMaxLengthOfSupplier(product, mapProduct);
		//Check link of product
		validateMaxLengthOfLink(product);
		//Check fitModel
		validateMaxLengthOfFitModel(product);

		/* BOE # Luong.Dai 2014/04/24 fix bug count error for product */
		if (product.getFlgErrorGeneral() == 0) {
			product.setProductGeneralErrorFlg(0);
		}
		if (product.getFlgErrorCategory() == 0) {
			product.setProductCategoryErrorFlg(0);
		}
		if (product.getFlgErrorModel() == 0) {
			product.setProductModelErrorFlg(0);
		}
		/* EOE # Luong.Dai 2014/04/24 fix bug count error for product */

		//Set matter flag value
        Integer matterGeneralCount = matter.getMatterFactoryErrorCountGeneral();
        matterGeneralCount = matterGeneralCount + (product.getProductGeneralErrorFlg() - oldGeneralErrorFlg);
        matter.setMatterFactoryErrorCountGeneral(matterGeneralCount);

        Integer matterCategoryCount = matter.getMatterFactoryErrorCountCategory();
        matterCategoryCount = matterCategoryCount + (product.getProductCategoryErrorFlg() - oldCategoryErrorFlg);
        matter.setMatterFactoryErrorCountCategory(matterCategoryCount);

        Integer matterModelCount = matter.getMatterFactoryErrorCountModel();
        matterModelCount = matterModelCount + (product.getProductModelErrorFlg() - oldModelErrorFlg);
        matter.setMatterFactoryErrorCountModel(matterModelCount);

        Integer matterAttributeCount = matter.getMatterFactoryErrorCountAttribute();
        matterAttributeCount = matterAttributeCount + (product.getProductAttributeErrorFlg() - oldAttributeErrorFlg);
        matter.setMatterFactoryErrorCountAttribute(matterAttributeCount);
    }

    /************************************************************************
     * <b>Description:</b><br>
     *  check Error Flag Of Product Popup Block 15.
     *
     * @author		Tran.Thanh
     * @date		Apr 23, 2014
     * @param 		product EntTblFactoryProductNew
     * @param 		matter EntMstFactoryMatterNew
     * @param 		mapProduct Map<Integer, Object>
     * @throws 		SQLException		void
     ************************************************************************/
    private void checkErrorFlagOfProductPopupBlock15(EntTblFactoryProductNew product
			, EntMstFactoryMatterNew matter
			, Map<Integer, Object> mapProduct) throws SQLException {
		//Get product general value

		Integer oldGeneralErrorFlg 		= product.getProductGeneralErrorFlg();
		Integer oldCategoryErrorFlg 	= product.getProductCategoryErrorFlg();
		Integer oldModelErrorFlg 		= product.getProductModelErrorFlg();
		Integer oldAttributeErrorFlg 	= product.getProductAttributeErrorFlg();
		validateMaxLengthOfProductGeneralPopupBlock15(product);
		//Check product Description
		validateMaxLengthOfDescription(product, mapProduct);
		//Check productVideo
		validateMaxLengthOfVideo(product, mapProduct);
		//Check product guest input
		validateMaxLengthOfGuestInput(product, mapProduct);
		//Check product supplier
		validateMaxLengthOfSupplier(product, mapProduct);
		//Check link of product
		validateMaxLengthOfLink(product);
		//Check fitModel
		validateMaxLengthOfFitModel(product);

		if (product.getFlgErrorGeneral() == 0) {
			product.setProductGeneralErrorFlg(0);
		}
		if (product.getFlgErrorCategory() == 0) {
			product.setProductCategoryErrorFlg(0);
		}
		if (product.getFlgErrorModel() == 0) {
			product.setProductModelErrorFlg(0);
		}

		//Set matter flag value
		Integer matterGeneralCount = matter.getMatterFactoryErrorCountGeneral();
		matterGeneralCount = matterGeneralCount + (product.getProductGeneralErrorFlg() - oldGeneralErrorFlg);
		matter.setMatterFactoryErrorCountGeneral(matterGeneralCount);

		Integer matterCategoryCount = matter.getMatterFactoryErrorCountCategory();
		matterCategoryCount = matterCategoryCount + (product.getProductCategoryErrorFlg() - oldCategoryErrorFlg);
		matter.setMatterFactoryErrorCountCategory(matterCategoryCount);

		Integer matterModelCount = matter.getMatterFactoryErrorCountModel();
		matterModelCount = matterModelCount + (product.getProductModelErrorFlg() - oldModelErrorFlg);
		matter.setMatterFactoryErrorCountModel(matterModelCount);

		Integer matterAttributeCount = matter.getMatterFactoryErrorCountAttribute();
		matterAttributeCount = matterAttributeCount + (product.getProductAttributeErrorFlg() - oldAttributeErrorFlg);
		matter.setMatterFactoryErrorCountAttribute(matterAttributeCount);
	}

    /************************************************************************
     * <b>Description:</b><br>
     *  Validate max length and set error flag for product general of product.
     *  Validate max length and set error flag for product description of product.
     *
     * @author		Luong.Dai
     * @date		Apr 7, 2014
     * @param 		product		product need validate
     * @param 		productGen	has value need to validate
     ************************************************************************/
    private void validateMaxLengthOfProductGeneral(EntTblFactoryProductNew product,
													EntTblFactoryProductGeneral productGen) {
		if (!checkMaxLengthOfInteger(productGen.getProductBrandCode(), Constant.PRODUCT_BRAND_CODE_MAX_LENGTH_INPUT)
    		|| !checkMaxLengthOfString(productGen.getProductName(), Constant.PRODUCT_NAME_MAX_LENGTH_INPUT)
    		|| !checkMaxLengthOfString(productGen.getProductCode(), Constant.PRODUCT_CODE_LENGTH)
    		|| !checkMaxLengthOfString(productGen.getProductEanCode(), Constant.PRODUCT_EAN_CODE_LENGTH)
    		|| !checkMaxLengthOfString(productGen.getProductDescriptionSummary(), Constant.DESCRIPTION_SUMMARY_LENGTH)
    		|| !checkMaxLengthOfString(productGen.getProductDescriptionCaution(), Constant.DESCRIPTION_CAUTION_LENGTH)
    		|| !checkMaxLengthOfString(productGen.getDescriptionRemarks(), Constant.DESCRIPTION_REMARKS_LENGTH)
    		|| !checkMaxLengthOfString(productGen.getProductSupplierReleaseDate(), Constant.PRODUCT_SUPPLIER_RELEASE_DATE_LENGTH)) {
    		//Set error for generalErrorFlg
    		product.setProductGeneralErrorFlg(1);
    		/* BOE # Luong.Dai 2014/04/24 Fix bug set error flg for general flag */
    		product.setFlgErrorGeneral(1);
    		/* EOE # Luong.Dai 2014/04/24 Fix bug set error flg for general flag */
    	}
		//Check productCategoryCode
    	if (!checkMaxLengthOfInteger(productGen.getProductCategoryCode(), Constant.PRODUCT_CATEGORY_CODE_MAX_LENGTH_INPUT)) {
    		//Set error for categoryErrorFlg
    		product.setProductCategoryErrorFlg(1);
    		/* BOE # Luong.Dai 2014/04/24 Fix bug set error flg for general flag */
    		product.setFlgErrorCategory(1);
    		/* EOE # Luong.Dai 2014/04/24 Fix bug set error flg for general flag */
    	}
	}

    /************************************************************************
     * <b>Description:</b><br>
     *  validate MaxLength Of Product General Popup Block 15.
     *
     * @author		Tran.Thanh
     * @date		Apr 24, 2014
     * @param 		product EntTblFactoryProductNew
     * @throws 		SQLException		void
     ************************************************************************/
    private void validateMaxLengthOfProductGeneralPopupBlock15(EntTblFactoryProductNew product) throws SQLException {
    	EntTblFactoryProductGeneral productGen =
				tblFactoryProductGeneralMapper.selectProductFactoryProductGeneralByProductid(product.getProductId());
		if (product.getProductBrandCode() != null) {
			productGen.setProductBrandCode(Integer.parseInt(product.getProductBrandCode()));
		}
		if (product.getCategoryCode() != null) {
			productGen.setProductCategoryCode(Integer.parseInt(product.getCategoryCode()));
		}
		if (product.getProductName() != null) {
			productGen.setProductName(product.getProductName());
		}
		if (product.getProductCode() != null) {
			productGen.setProductCode(product.getProductCode());
		}
		if (product.getProductEanCode() != null) {
			productGen.setProductEanCode(product.getProductEanCode());
		}
		if (product.getSupplierReleaseDate() != null) {
			productGen.setProductSupplierReleaseDate(product.getSupplierReleaseDate());
		}

		if (!checkMaxLengthOfInteger(productGen.getProductBrandCode(), Constant.PRODUCT_BRAND_CODE_MAX_LENGTH_INPUT)
			|| !checkMaxLengthOfString(productGen.getProductName(), Constant.PRODUCT_NAME_MAX_LENGTH_INPUT)
			|| !checkMaxLengthOfString(productGen.getProductCode(), Constant.PRODUCT_CODE_LENGTH)
			|| !checkMaxLengthOfString(productGen.getProductEanCode(), Constant.PRODUCT_EAN_CODE_LENGTH)
			|| !checkMaxLengthOfString(productGen.getProductSupplierReleaseDate(), Constant.PRODUCT_SUPPLIER_RELEASE_DATE_LENGTH)) {
			//Set error for generalErrorFlg
			product.setProductGeneralErrorFlg(1);
			product.setFlgErrorGeneral(1);
		}
		//Check productCategoryCode
		if (!checkMaxLengthOfInteger(productGen.getProductCategoryCode(), Constant.PRODUCT_CATEGORY_CODE_MAX_LENGTH_INPUT)) {
			//Set error for categoryErrorFlg
			product.setProductCategoryErrorFlg(1);
			product.setFlgErrorCategory(1);
		}
	}

    /************************************************************************
     * <b>Description:</b><br>
     *  validate MaxLength Of Description.
     *
     * @author		Tran.Thanh
     * @date		Apr 24, 2014
     * @param 		product EntTblFactoryProductNew
     * @param 		mapProduct mapProduct
     * @throws 		SQLException		void
     ************************************************************************/
    private void validateMaxLengthOfDescription(EntTblFactoryProductNew product,
												Map<Integer, Object> mapProduct) throws SQLException {
    	EntTblFactoryProductDescription productDes = (EntTblFactoryProductDescription) mapProduct.get(2);

		if (!checkMaxLengthOfString(productDes.getDescriptionSummary(), Constant.DESCRIPTION_SUMMARY_LENGTH)
			|| !checkMaxLengthOfString(productDes.getDescriptionCaution(), Constant.DESCRIPTION_CAUTION_LENGTH)
			|| !checkMaxLengthOfString(productDes.getDescriptionRemarks(), Constant.DESCRIPTION_REMARKS_LENGTH)) {
			//Set error for generalErrorFlg
			product.setProductGeneralErrorFlg(1);
			product.setFlgErrorGeneral(1);
		}
	}

    /************************************************************************
     * <b>Description:</b><br>
     *  validate max length and set error flag for video of product.
     *
     * @author		Luong.Dai
     * @date		Apr 7, 2014
     * @param 		product		product need validate
     * @param 		mapProduct	mapProduct has supplier
     ************************************************************************/
    @SuppressWarnings("unchecked")
	private void validateMaxLengthOfVideo(EntTblFactoryProductNew product,
										Map<Integer, Object> mapProduct) {
		if (product.getProductGeneralErrorFlg().equals(0)) {
    		//Get all video of current product
        	List<EntTblFactoryProductVideo> listVideo = (List<EntTblFactoryProductVideo>) mapProduct.get(Constant.PRODUCT_EDIT_VIDEO_BLOCK);
        	if (null != listVideo) {
        		for (EntTblFactoryProductVideo video : listVideo) {
        			if (!video.getCrudFlg().equals(Constant.DELETE_MODE)) {
        				//Video is not delete video
        				if (!checkMaxLengthOfString(video.getProductVideoTitle(), Constant.PRODUCT_VIDEO_TITLE_LENGTH)
    	    				|| !checkMaxLengthOfString(video.getProductVideoUrl(), Constant.PRODUCT_VIDEO_URL_LENGTH)
    	    				|| !checkMaxLengthOfString(video.getProductVideoDescription(), Constant.PRODUCT_VIDEO_DESCRIPTION_LENGTH)) {
    	    				//Set error for generalErrorFlg
    	    	    		product.setProductGeneralErrorFlg(1);
    	    	    		product.setFlgErrorGeneral(1);
        				}
        			}
        		}
        	}
    	}
	}

    /************************************************************************
     * <b>Description:</b><br>
     *  Validate max length and set error for guest input of product.
     *
     * @author		Luong.Dai
     * @date		Apr 7, 2014
     * @param 		product		product need validate
     * @param 		mapProduct	mapProduct has supplier
     ************************************************************************/
    @SuppressWarnings("unchecked")
	private void validateMaxLengthOfGuestInput(EntTblFactoryProductNew product,
											Map<Integer, Object> mapProduct) {
		if (product.getProductGeneralErrorFlg().equals(0)) {
			//Get list guest input
            List<EntTblFactoryProductGuestInput> listGuest = (List<EntTblFactoryProductGuestInput>) mapProduct.get(Constant.PRODUCT_EDIT_GUEST_BLOCK);
			//Save list guestInput
			if (null != listGuest) {
				for (EntTblFactoryProductGuestInput guest : listGuest) {
					if (!guest.getCrudFlg().equals(Constant.DELETE_MODE)) {
						//guest input is not deleted item
						if (!checkMaxLengthOfString(guest.getProductGuestInputTitle(), Constant.PRODUCT_GUEST_INPUT_TITLE_LENGTH)
                         || !checkMaxLengthOfString(guest.getProductGuestInputDescription(), Constant.PRODUCT_GUEST_INPUT_DESCRIPTION_LENGTH)) {
							//Set error for generalErrorFlg
		    	    		product.setProductGeneralErrorFlg(1);
		    	    		product.setFlgErrorGeneral(1);
						}
					}
				}
			}
		}
	}

    /************************************************************************
     * <b>Description:</b><br>
     *  Validate max length and set error flag for supplier of product.
     *
     * @author		Luong.Dai
     * @date		Apr 7, 2014
     * @param 		product		product need validate
     * @param 		mapProduct	mapProduct has supplier
     ************************************************************************/
    @SuppressWarnings("unchecked")
	private void validateMaxLengthOfSupplier(EntTblFactoryProductNew product,
											Map<Integer, Object> mapProduct) {
		if (product.getProductGeneralErrorFlg().equals(0)) {
			//Get list supplier
            List<EntTblFactoryProductSupplier> listSupplier = (List<EntTblFactoryProductSupplier>) mapProduct.get(Constant.PRODUCT_EDIT_SUPPLIER_STATUS_BLOCK);
			//Check error flag of list supplier
			if (null != listSupplier) {
				for (EntTblFactoryProductSupplier supplier : listSupplier) {
					if (!supplier.getCrudFlg().equals(Constant.DELETE_MODE)) {
						//supplier is not delete item
						if (!checkMaxLengthOfString(supplier.getSiireCode(), Constant.SIIRE_CODE_LENGTH)
						            || !checkMaxLengthOfString(supplier.getNoukiCode(), Constant.NOUKI_CODE_LENGTH)
                            || !checkMaxLengthOfInteger(supplier.getSupplierSyouhinSiireOrder(), Constant.SUPPLIER_SYOUHIN_SIIRE_ORDER_LENGTH)) {
							//Set error for generalErrorFlg
		    	    		product.setProductGeneralErrorFlg(1);
		    	    		product.setFlgErrorGeneral(1);
						}
					}
				}
			}
		}
	}

    /************************************************************************
     * <b>Description:</b><br>
     *  Validate max length and set error flag for link of product.
     *
     * @author		Luong.Dai
     * @date		Apr 7, 2014
     * @param 		product		Product need validate
     ************************************************************************/
    private void validateMaxLengthOfLink(EntTblFactoryProductNew product) {
		if (product.getProductGeneralErrorFlg().equals(0)) {
			//Get link of product
			List<EntTblFactoryProductLink> listLink = tblFactoryProductLinkMapper.selectProductLinkValueByProductId(product.getProductId());
			if (null != listLink) {
				for (EntTblFactoryProductLink link : listLink) {
					//Check error flag
					if (!checkMaxLengthOfString(link.getProductLinkTitle(), Constant.PRODUCT_LINK_TITLE_LENGTH)
						|| !checkMaxLengthOfString(link.getProductLinkUrl(), Constant.PRODUCT_LINK_URL_LENGTH)) {
						//Set error for generalErrorFlg
	    	    		product.setProductGeneralErrorFlg(1);
	    	    		product.setFlgErrorGeneral(1);
					}
				}
			}
		}
	}

    /************************************************************************
     * <b>Description:</b><br>
     *  Validate and set error flag for fitModel of product.
     *
     * @author		Luong.Dai
     * @date		Apr 7, 2014
     * @param 		product	Product need validate
     ************************************************************************/
    private void validateMaxLengthOfFitModel(EntTblFactoryProductNew product) {
		if (product.getProductModelErrorFlg().equals(0)) {
			//Get list fitModel
			List<EntTblFactoryProductFitModel> listFitModels = tblFactoryProductModelMapper.selectListFitModelByProductId(product.getProductId());
			if (null != listFitModels) {
				for (EntTblFactoryProductFitModel fitModel : listFitModels) {
					//Check error flag
					if (!checkMaxLengthOfString(fitModel.getFitModelMaker(), Constant.FIT_MODEL_MAKER_LENGTH)
						|| !checkMaxLengthOfString(fitModel.getFitModelModel(), Constant.FIT_MODEL_MODEL_LENGTH)
						|| !checkMaxLengthOfString(fitModel.getFitModelStyle(), Constant.FIT_MODEL_STYLE_WARNING_LENGTH)) {
						//Set error for modelErrorFlg
	    	    		product.setProductModelErrorFlg(1);
	    	    		product.setFlgErrorModel(1);
					}
				}
			}
		}
	}

    /************************************************************************
     * <b>Description:</b><br>
     *  Check maxlenght of integer value.
     *
     * @author		Luong.Dai
     * @date		Apr 4, 2014
     * @param 		value			Value for check
     * @param 		maxLength		max length
     * @return		boolean			result
     ************************************************************************/
    private boolean checkMaxLengthOfInteger(Integer value, int maxLength) {
    	boolean result = false;
    	if (null == value
    		|| value.toString().length() <= maxLength) {
    		result = true;
    	}
    	return result;
    }

    /************************************************************************
     * <b>Description:</b><br>
     *  Check maxlenght of string value.
     *
     * @author		Luong.Dai
     * @date		Apr 4, 2014
     * @param 		value			Value for check
     * @param 		maxLength		max length
     * @return		boolean			result
     ************************************************************************/
    private boolean checkMaxLengthOfString(String value, int maxLength) {
    	boolean result = false;
    	if (null == value
    		|| value.length() <= maxLength) {
    		result = true;
    	}
    	return result;
    }
    /************************************************************************
     * <b>Description:</b><br>
     *  Insert new product to product group.
     *
     * @author		Luong.Dai
     * @date		Mar 4, 2014
     * @param       product         	EntTblFactoryProductNew
     * @param       entQuery        	EntTblFactoryProductNew
     * @param       productGeneral  	EntTblFactoryProductGeneral
     * @param		mapNewProductData	Map<Integer, Object>
     * @return		Boolean
     * @throws      SQLException 		Exception
     ************************************************************************/
    private Boolean insertNewProductToGroup(EntTblFactoryProductNew product
                                        , EntTblFactoryProductNew entQuery
                                        , EntTblFactoryProductGeneral productGeneral
                                        , Map<Integer, Object> mapNewProductData) throws SQLException {
        Boolean result = true;
        //Get new productId for new product
        Long productId = selectMaxProductIdPlusOne();
        //Insert productId to table numberring
        insertNewTblFactoryProductNumbering(productId);

        /* BOE by Luong.Dai 2014/03/25 Fix new product in group */
        //Insert new product to table tbl_factory_product
        result = insertNewProductForGroupProduct(product, entQuery, productId, mapNewProductData);

        //Insert to productGeneral
        if (result) {
            result = insertNewProductGeneralForGroupProduct(product, entQuery, productGeneral, productId, mapNewProductData);
        }
        //Insert into supplier_price
        if (result) {
            result = insertSupplierPriceForGroupProduct(product, entQuery, productId);
        }
        //Insert list select for new product
        if (result) {
            result = insertSelectOfProduct(product, entQuery, productId);
        }
        //Insert list image for product
        insertListImageOfProduct(entQuery, productId, mapNewProductData);
        //Insert list attribute of product
        insertListAttributeOfProduct(entQuery, productId, mapNewProductData);
        //Insert list link of product
        insertListLinkOfProduct(entQuery, productId, mapNewProductData);
        //Insert list video of product
        insertListVideoOfProduct(entQuery, productId, mapNewProductData);
        //Insert description for product
        insertProductDescriptionToDB(entQuery, productId, mapNewProductData);
        //Insert list product guest input
        insertListGuestInputOfProduct(entQuery, productId, mapNewProductData);
        //Insert list fit model
        insertListFitModelOfNewProduct(entQuery, productId, mapNewProductData);
        //Insert list supplier of product
        insertListSupplierOfProduct(entQuery, productId, mapNewProductData);
        /*//Insert new product to table tbl_factory_product
        result = insertNewProductForGroupProduct(product, entQuery, productId);

        //Insert to productGeneral
        if (result) {
            result = insertNewProductGeneralForGroupProduct(product, entQuery, productGeneral, productId);
        }

        //Insert into supplier_price
        if (result) {
            result = insertSupplierPriceForGroupProduct(product, entQuery, productId);
        }

        //Insert list select for new product
        if (result) {
            result = insertSelectOfProduct(product, entQuery, productId);
        }*/
        /* EOE by Luong.Dai 2014/03/25 Fix new product in group */
        return result;
    }

    /************************************************************************
     * <b>Description:</b><br>
     *  insert list supplier of product.
     *
     * @author		Luong.Dai
     * @date		Mar 25, 2014
     * @param 		entQuery			Ent has product data
	 * @param 		productId			New productId
	 * @param 		mapNewProductData	Map has old product data
	 * @throws 		SQLException		Exception
     ************************************************************************/
    private void insertListSupplierOfProduct(EntTblFactoryProductNew entQuery
    										, Long productId
    										, Map<Integer, Object> mapNewProductData) throws SQLException {
		@SuppressWarnings("unchecked")
		List<EntTblFactoryProductSupplier> listSupplier =
		                                      (List<EntTblFactoryProductSupplier>) mapNewProductData.get(Constant.PRODUCT_EDIT_SUPPLIER_STATUS_BLOCK);
		if (listSupplier != null) {
			int size = listSupplier.size();
			for (int i = 0; i < size; i++) {
				EntTblFactoryProductSupplier supplier = listSupplier.get(i);
				//Set productId and updatedUserid
				supplier.setProductId(productId);
				supplier.setUpdatedUserId(entQuery.getUpdatedUserId());
				//Save to DB
				tblFactoryProductSupplierMapper.insertSupplierByProductIdAndsiireCode(supplier);
			}
		}
	}
	/************************************************************************
     * <b>Description:</b><br>
     *  insert list fit model of product.
     *
     * @author		Luong.Dai
     * @date		Mar 25, 2014
     * @param 		entQuery			Ent has product data
	 * @param 		productId			New productId
	 * @param 		mapNewProductData	Map has old product data
     ************************************************************************/
    private void insertListFitModelOfNewProduct(EntTblFactoryProductNew entQuery
                                              , Long productId
    										  , Map<Integer, Object> mapNewProductData) {
		@SuppressWarnings("unchecked")
		List<EntTblFactoryProductFitModel> lstFitModel =
							(List<EntTblFactoryProductFitModel>) mapNewProductData.get(Constant.PRODUCT_EDIT_FIT_MODEL_BLOCK);
		if (lstFitModel != null) {
			int size = lstFitModel.size();
			for (int i = 0; i < size; i++) {
				EntTblFactoryProductFitModel fitModel = lstFitModel.get(i);
				//Set new productId and updatedUserId
				fitModel.setProductId(productId);
				fitModel.setUpdateUserId(entQuery.getUpdatedUserId());
				//Save to DB
				tblFactoryProductModelMapper.insertToFitModel(fitModel);
			}
		}
	}
	/************************************************************************
     * <b>Description:</b><br>
     *  insert list guest input of product.
     *
     * @author		Luong.Dai
     * @date		Mar 25, 2014
     * @param 		entQuery			Ent has product data
	 * @param 		productId			New productId
	 * @param 		mapNewProductData	Map has old product data
     ************************************************************************/
    private void insertListGuestInputOfProduct(EntTblFactoryProductNew entQuery
    										, Long productId
    										, Map<Integer, Object> mapNewProductData) {
		@SuppressWarnings("unchecked")
		List<EntTblFactoryProductGuestInput> listProductGuestInput =
		                        (List<EntTblFactoryProductGuestInput>) mapNewProductData.get(Constant.PRODUCT_EDIT_GUEST_BLOCK);
		if (listProductGuestInput != null) {
			int size = listProductGuestInput.size();
			for (int i = 0; i < size; i++) {
				EntTblFactoryProductGuestInput guestInput = listProductGuestInput.get(i);
				guestInput.setProductId(productId);
				guestInput.setUpdatedUserId(entQuery.getUpdatedUserId());
				//Insert to DB
				tblFactoryProductGuestInputMapper.insertNewProductGuestInput(guestInput);
			}
		}
	}
	/************************************************************************
     * <b>Description:</b><br>
     *  insert product description to DB.
     *
     * @author		Luong.Dai
     * @date		Mar 25, 2014
     * @param 		entQuery			Ent has product data
	 * @param 		productId			New productId
	 * @param 		mapNewProductData	Map has old product data
     ************************************************************************/
    private void insertProductDescriptionToDB(EntTblFactoryProductNew entQuery
    										, Long productId
    										, Map<Integer, Object> mapNewProductData) {
    	EntTblFactoryProductDescription description = (EntTblFactoryProductDescription) mapNewProductData.get(Constant.PRODUCT_EDIT_DESCRIPTION_BLOCK);
    	if (description != null) {
    		EntTblFactoryProductNew product = new EntTblFactoryProductNew();
    		product.setProductId(productId);
    		product.setDescriptionRemarks(description.getDescriptionRemarks());
    		product.setDescriptionSummary(description.getDescriptionSummary());
    		product.setDescriptionSentence(description.getDescriptionSentence());
    		product.setDescriptionCaution(description.getDescriptionCaution());
    		product.setUpdatedUserId(entQuery.getUpdatedUserId());
    		//Save to DB
    		tblFactoryProductDescriptionMapper.updateProductDescriptionByProduct(product);
    	}
	}
	/************************************************************************
	 * <b>Description:</b><br>
	 *  Insert list videos of product to DB.
	 *
	 * @author		Luong.Dai
	 * @date		Mar 25, 2014
	 * @param 		entQuery			Ent has product data
	 * @param 		productId			New productId
	 * @param 		mapNewProductData	Map has old product data
	 ************************************************************************/
	private void insertListVideoOfProduct(EntTblFactoryProductNew entQuery
    									, Long productId
    									, Map<Integer, Object> mapNewProductData) {
    	@SuppressWarnings("unchecked")
		List<EntTblFactoryProductVideo> lstVideos = (List<EntTblFactoryProductVideo>) mapNewProductData.get(Constant.PRODUCT_EDIT_VIDEO_BLOCK);
		if (lstVideos != null) {
			int size = lstVideos.size();
			for (int i = 0; i < size; i++) {
				EntTblFactoryProductVideo video = lstVideos.get(i);
				//set new productId
				video.setProductId(productId);
				//Insert to DB
				tblFactoryProductVideoMapper.insertNewProductVideo(video);
			}
		}
	}
	/************************************************************************
     * <b>Description:</b><br>
     *  insert list link of product.
     *
     * @author		Luong.Dai
     * @date		Mar 25, 2014
     * @param 		entQuery			Ent has product info
     * @param 		productId			New productId of product
     * @param 		mapNewProductData	Map has old data
     ************************************************************************/
    private void insertListLinkOfProduct(EntTblFactoryProductNew entQuery
    								   , Long productId
    								   , Map<Integer, Object> mapNewProductData) {
		@SuppressWarnings("unchecked")
		List<EntTblFactoryProductLink> lstLink = (List<EntTblFactoryProductLink>) mapNewProductData.get(Constant.PRODUCT_EDIT_LINK_BLOCK);
		if (lstLink != null) {
			int size = lstLink.size();
			for (int i = 0; i < size; i++) {
				EntTblFactoryProductLink link = lstLink.get(i);
				link.setProductId(productId);
				tblFactoryProductLinkMapper.insertNewProductLink(link);
			}
		}
	}
	/************************************************************************
     * <b>Description:</b><br>
     *  Insert list attribute duplicate from old product data.
     *
     * @author		Luong.Dai
     * @date		Mar 25, 2014
     * @param	 	entQuery			Ent has product data
     * @param 		productId			Product id of new product
     * @param 		mapNewProductData	map has old data
     ************************************************************************/
    private void insertListAttributeOfProduct(EntTblFactoryProductNew entQuery
    										, Long productId
    										, Map<Integer, Object> mapNewProductData) {
    	@SuppressWarnings("unchecked")
		List<EntMstAttribute> lstAttribute = (List<EntMstAttribute>) mapNewProductData.get(Constant.PRODUCT_EDIT_ATRIBUTE_BLOCK);
    	if (lstAttribute != null) {
    		int size = lstAttribute.size();
    		for (int i = 0; i < size; i++) {
    			// insert attribute to DB
        		EntMstAttribute attribute = lstAttribute.get(i);
        		attribute.setProductId(productId);
        		attribute.setUpdatedUserId(entQuery.getUpdatedUserId());

        		if (!StringUtils.equals(attribute.getCrudFlg(), Constant.DELETE_MODE)) {
					//AttributeValues and attributeDisplay not emply, insert to DB
					if (StringUtils.isNotEmpty(attribute.getAttributeValues())) {
						if (StringUtils.isNotEmpty(attribute.getAttributeDisplay())
							|| StringUtils.equals(attribute.getAttributeType(), Constant.TYPE_STRING)
							|| StringUtils.equals(attribute.getAttributeType(), Constant.TYPE_FLAG)) {
			    			if (StringUtils.equals(attribute.getAttributeType(), "Integer")) {
			    				tblProductAttributeMapper.insertFactoryAttributeInteger(attribute);
			    			} else if (StringUtils.equals(attribute.getAttributeType(), "Float")) {
								tblProductAttributeMapper.insertFactoryAttributeFloat(attribute);
			    			} else if (StringUtils.equals(attribute.getAttributeType(), "String")) {
			    				tblProductAttributeMapper.insertFactoryAttributeString(attribute);
			    			} else if (StringUtils.equals(attribute.getAttributeType(), "Group")) {
			    				tblProductAttributeMapper.insertFactoryAttributeGroup(attribute);
			    			} else if (StringUtils.equals(attribute.getAttributeType(), "Flag")) {
			    				tblProductAttributeMapper.insertFactoryAttributeFlag(attribute);
			    			}
						}
					}
        		}
    		}
    	}
	}
	/************************************************************************
     * <b>Description:</b><br>
     *  Insert list image duplicate from old product to new product.
     *
     * @author		Luong.Dai
     * @date		Mar 25, 2014
     * @param 		entQuery	Ent to get infor
     * @param 		productId	New product id
     * @param 		mapNewProductData Map has old product data
     ************************************************************************/
    private void insertListImageOfProduct(EntTblFactoryProductNew entQuery
    										, Long productId
    										, Map<Integer, Object> mapNewProductData) {
    	@SuppressWarnings("unchecked")
		List<EntTblFactoryProductImage> listImageAndThumb =
		                                    (List<EntTblFactoryProductImage>) mapNewProductData.get(Constant.PRODUCT_EDIT_IMAGE_BLOCK);
    	if (listImageAndThumb != null) {
    		int length = listImageAndThumb.size();
    		for (int i = 0; i < length; i++) {
    			EntTblFactoryProductImage entImage = listImageAndThumb.get(i);
    			//Set productId for image
    			entImage.setProductId(productId);
    			entImage.setUpdatetedUserId(entQuery.getUpdatedUserId());
    			tblFactoryProductImageMapper.insertFactoryProductImage(entImage);
    		}
    	}
	}
	/************************************************************************
     * <b>Description:</b><br>
     *  insert list select for product.
     *
     * @author		Luong.Dai
     * @date		Mar 4, 2014
     * @param       product     EntTblFactoryProductNew
     * @param       entQuery    EntTblFactoryProductNew
     * @param       productId	Long
     * @return      Boolean
     * @throws      SQLException Exception
     ************************************************************************/
    private Boolean insertSelectOfProduct(EntTblFactoryProductNew product
                                        , EntTblFactoryProductNew entQuery
                                        , Long productId) throws SQLException {
        Boolean result = true;
        List<EntTblFactoryProductSelect> listSelect = product.getLstSelectOfProduct();
        if (listSelect != null) {
            int size = listSelect.size();
            for (int i = 0; i < size; i++) {
                EntTblFactoryProductSelect select = listSelect.get(i);
                select.setProductId(productId);
                select.setUpdatedUserId(entQuery.getUpdatedUserId());
                Integer returnValue = tblFactoryProductSelectMapper.insertNewSelectOfProduct(select);
                if (returnValue <= 0) {
                    result = false;
                    break;
                }
            }
        }
        return result;
    }
    /************************************************************************
     * <b>Description:</b><br>
     *  Insert new supplier for group product.
     *
     * @author		Luong.Dai
     * @date		Mar 4, 2014
     * @param       product     EntTblFactoryProductNew
     * @param       entQuery    EntTblFactoryProductNew
     * @param       productId   Long
     * @return      Boolean
     * @throws      SQLException Exception
     ************************************************************************/
    private Boolean insertSupplierPriceForGroupProduct(EntTblFactoryProductNew product
                                                     , EntTblFactoryProductNew entQuery
                                                     , Long productId) throws SQLException {
        product.setProductId(productId);
        product.setUpdatedUserId(entQuery.getUpdatedUserId());
        Integer returnValue = tblFactoryProductSupplierPriceMapper.insertNewSupplierPriceOfGroupProduct(product);
        if (returnValue <= 0) {
            return false;
        }
        return true;
    }

    /************************************************************************
     * <b>Description:</b><br>
     *  Insert new product general for group product.
     *
     * @author		Luong.Dai
     * @date		Mar 4, 2014
     * @param       product         	EntTblFactoryProductNew
     * @param       entQuery        	EntTblFactoryProductNew
     * @param       productGeneral  	EntTblFactoryProductGeneral
     * @param       productId			Long
     * @param		mapDataNewProduct	Map<Integer, Object>
     * @return      Boolean
     * @throws      SQLException 		Exception
     ************************************************************************/
    private Boolean insertNewProductGeneralForGroupProduct(EntTblFactoryProductNew product
                                                         , EntTblFactoryProductNew entQuery
                                                         , EntTblFactoryProductGeneral productGeneral
                                                         , Long productId
                                                         , Map<Integer, Object> mapDataNewProduct) throws SQLException {
        EntTblFactoryProductGeneral general = new EntTblFactoryProductGeneral();
        general.setProductId(productId);
        general.setProductCode(product.getProductCode());
        general.setProductEanCode(product.getProductEanCode());
        general.setProductProperPrice(product.getProductProperPrice().intValue());
        general.setProductBrandCode(productGeneral.getProductBrandCode());
        general.setProductGroupCode(productGeneral.getProductGroupCode());
        general.setUpdatedUserId(entQuery.getUpdatedUserId());
        //Set value get from entProduct
        EntTblFactoryProductGeneral oldGeneral = (EntTblFactoryProductGeneral) mapDataNewProduct.get(Constant.PRODUCT_EDIT_RELEASE_DATE_BLOCK);
        general.setProductCategoryCode(oldGeneral.getProductCategoryCode());
        general.setProductName(oldGeneral.getProductName());
        general.setProductWebikeCodeFlg(oldGeneral.getProductWebikeCodeFlg());
        general.setProductGroupname(oldGeneral.getProductGroupname());
        general.setProductSupplierReleaseDate(oldGeneral.getProductSupplierReleaseDate());
        general.setProductSupplierStatusCode(oldGeneral.getProductSupplierStatusCode());
        general.setProductDelFlg(oldGeneral.getProductDelFlg());
        //Set productCondition flag
        general.setProductOpenPriceFlg(oldGeneral.getProductOpenPriceFlg());
        general.setProductProperSellingFlg(oldGeneral.getProductProperSellingFlg());
        general.setProductOrderProductFlg(oldGeneral.getProductOrderProductFlg());
        general.setProductNoReturnableFlg(oldGeneral.getProductNoReturnableFlg());
        general.setProductAmbiguousImageFlg(oldGeneral.getProductAmbiguousImageFlg());

        Integer returnValue = tblFactoryProductGeneralMapper.insertNewFactoryProductGeneralByProductGeneral(general);

        //Insert into productCondition
        tblFactoryProductConditionMapper.insertNewFactoryProductConditionByProductGeneral(general);
        if (returnValue <= 0) {
            return false;
        }
        return true;
    }

    /************************************************************************
     * <b>Description:</b><br>
     *  Insert new product for group product.
     *
     * @author		Luong.Dai
     * @date		Mar 4, 2014
     * @param       product    	 			EntTblFactoryProductNew
     * @param       entQuery    			EntTblFactoryProductNew
     * @param       productId				Long
     * @param		mapDataNewProduct 		Map<Integer, Object>
     * @return      Boolean
     * @throws      SQLException Exception
     ************************************************************************/
    private Boolean insertNewProductForGroupProduct(EntTblFactoryProductNew product
                                                , EntTblFactoryProductNew entQuery
                                                , Long productId
                                                , Map<Integer, Object> mapDataNewProduct) throws SQLException {
    	//product to copy data
        EntTblFactoryProductNew oldProduct = (EntTblFactoryProductNew) mapDataNewProduct.get(Constant.PRODUCT_EDIT_GENARAL_BLOCK);
        product.setProductId(productId);
        product.setProductMatterNo(entQuery.getProductMatterNo());
        product.setUpdatedUserId(entQuery.getUpdatedUserId());
        //Set flag
        /* BOE by Luong.Dai at 2014/04/17 change default of error flag */
    	product.setProductGeneralErrorFlg(0);
        product.setProductCategoryErrorFlg(0);
        product.setProductModelErrorFlg(0);
        product.setProductAttributeErrorFlg(0);
        /*product.setProductGeneralErrorFlg(oldProduct.getProductGeneralErrorFlg());
        product.setProductCategoryErrorFlg(oldProduct.getProductCategoryErrorFlg());
        product.setProductModelErrorFlg(oldProduct.getProductModelErrorFlg());
        product.setProductAttributeErrorFlg(oldProduct.getProductAttributeErrorFlg());*/
        /* EOE by Luong.Dai at 2014/04/17 change default of error flag */
        product.setProductRegistrationFlg(oldProduct.getProductRegistrationFlg());
        product.setProductGeneralCheckFlg(oldProduct.getProductGeneralCheckFlg());
        product.setProductCategoryCheckFlg(oldProduct.getProductCategoryCheckFlg());
        product.setProductModelCheckFlg(oldProduct.getProductModelCheckFlg());
        product.setProductAttributeCheckFlg(oldProduct.getProductAttributeCheckFlg());

        Integer returnValue = tblFactoryProductMapper.duplicateNewProduct(product);
        if (returnValue <= 0) {
            return false;
        }
        return true;
    }
    /************************************************************************
     * <b>Description:</b><br>
     *  Remove product from group.
     *  Set group_code = 0.
     *
     * @author      Luong.Dai
     * @date        Mar 3, 2014
     * @param       product     EntTblFactoryProductNew
     * @param       entQuery    EntTblFactoryProductNew
     * @return      Boolean
     ************************************************************************/
    private Boolean removeProductFromGroupProduct(EntTblFactoryProductNew product, EntTblFactoryProductNew entQuery) {
        EntTblFactoryProductGeneral productGeneral = new EntTblFactoryProductGeneral();
        //Set productId an prouductGroupCode to ent
        productGeneral.setProductId(product.getProductId());
        //GroupCode = 0: product doesn't group
        productGeneral.setProductGroupCode(0L);
        //Set update userId
        productGeneral.setUpdatedUserId(entQuery.getUpdatedUserId());
        Integer result = tblFactoryProductMapper.updateProductGroupCodeByProductId(productGeneral);
        if (result > 0) {
            return true;
        }
        return false;
    }

    /************************************************************************
     * <b>Description:</b><br>
     *  Update product value at grid group product.
     *
     * @author      Luong.Dai
     * @date        Mar 3, 2014
     * @param       product     EntTblFactoryProductNew
     * @param       entQuery    EntTblFactoryProductNew
     * @param       mode        String
     ************************************************************************/
    private void updateProductFromGroupProduct(EntTblFactoryProductNew product, EntTblFactoryProductNew entQuery, String mode) {
        if (mode.equals(Constant.NEW_MODE)
        		&& product.getProductId() <= 0) {
        	// Set new productId for new product
            product.setProductId(entQuery.getProductId());
        }
        //Set updatedUserId
        product.setUpdatedUserId(entQuery.getUpdatedUserId());
        //Update product_syouhin_sys_codee
        Boolean result = updateSyouhinCodeAndGeneralCountByProductId(product, mode);
        if (!result) {
            //Cannot update this product
            throw new RuntimeException();
        }
        //Update product_code, product_proper_price and product_ean_code
        tblFactoryProductGeneralMapper.updateProductCodeProperPriceAndEANCodeByProductId(product);

        //Update product_supplier_price
        result = updateSupplierPriceByProductId(product);
        if (!result) {
            //Cannot update this product
            throw new RuntimeException();
        }
        //Update product_select value
        updateSelectValueOfProduct(product);
    }

    /************************************************************************
     * <b>Description:</b><br>
     *  Get updatedOn and update supplierPrice by productId.
     *
     * @author		Luong.Dai
     * @date		Mar 4, 2014
     * @param       product EntTblFactoryProductNew
     * @return		Boolean
     ************************************************************************/
    private Boolean updateSupplierPriceByProductId(EntTblFactoryProductNew product) {
            tblFactoryProductSupplierMapper.updateSupplierPriceByProductId(product);
            return true;
    }

    /************************************************************************
     * <b>Description:</b><br>
     *  Check update time and update syouhin_sys_code and general_error count of product.
     *
     * @author		Luong.Dai
     * @date		Mar 4, 2014
     * @param       product	EntTblFactoryProductNew
     * @param       mode String
     * @return      Boolean
     ************************************************************************/
    private Boolean updateSyouhinCodeAndGeneralCountByProductId(EntTblFactoryProductNew product, String mode) {
        if (product.getProductSyouhinSysCode() == null) {
            product.setProductSyouhinSysCode((long) 0);
        }
        tblFactoryProductMapper.updateSyouhinSysCodeProductId(product);
        return true;
    }
    /************************************************************************
     * <b>Description:</b><br>
     *  Create string of list delete select.
     *  Format: select_code1,select_code2....
     *
     * @author      Luong.Dai
     * @date        Mar 4, 2014
     * @param       listSelect  List<EntTblFactoryProductSelect>
     * @return      String
     ************************************************************************/
    private String createListDeleteSelectForListSelect(List<EntTblFactoryProductSelect> listSelect) {
        String result = "";
        int size = listSelect.size();
        for (int i = 0; i < size; i++) {
            if (listSelect.get(i).getCrudFlg().equals(Constant.DELETE_MODE)) {
                if (StringUtils.isNotEmpty(result)) {
                    //Add character ','
                    result = result.concat(",");
                }
                //Select has DELETE_MODE, add to string
                result = result.concat(listSelect.get(i).getSelectCode().toString());
            }
        }
        return result;
    }
    /************************************************************************
     * <b>Description:</b><br>
     *  Delete list select of product.
     *
     * @author      Luong.Dai
     * @date        Mar 4, 2014
     * @param       product             EntTblFactoryProductNew
     * @param       listDeleteSelect    String
     ************************************************************************/
    private void deleteSelectOfProduct(EntTblFactoryProductNew product, String listDeleteSelect) {
        tblFactoryProductSelectMapper.deleteListProductSelectOfProduct(product.getProductId(), listDeleteSelect);
    }

    /************************************************************************
     * <b>Description:</b><br>
     *  Delete list select not exists in list current select.
     *
     * @author      Luong.Dai
     * @date        Mar 4, 2014
     * @param       product             EntTblFactoryProductNew
     * @param       listCurrentSelect   String
     ************************************************************************/
    private void deleteSelectNotExistsInCurrent(EntTblFactoryProductNew product, String listCurrentSelect) {
        tblFactoryProductSelectMapper.deleteListProductSelectNotExistsInCurrentSelect(product.getProductId(), listCurrentSelect);
    }
    /************************************************************************
     * <b>Description:</b><br>
     *  Update list select value of product.
     *
     * @author      Luong.Dai
     * @date        Mar 3, 2014
     * @param       product EntTblFactoryProductNew
     * @return      Integer
     ************************************************************************/
    private Integer updateSelectValueOfProduct(EntTblFactoryProductNew product) {
        Integer result = 0;
        List<EntTblFactoryProductSelect> lstSelectOfProduct = product.getLstSelectOfProduct();
        if (lstSelectOfProduct != null) {
            int size = lstSelectOfProduct.size();
            //Save select_display_value of product
            for (int i = 0; i < size; i++) {
                EntTblFactoryProductSelect select = lstSelectOfProduct.get(i);
                //Set productId for select
                select.setProductId(product.getProductId());
                //Set update user
                select.setUpdatedUserId(product.getUpdatedUserId());
                //Save select display value to DB
                result = tblFactoryProductSelectMapper.updateSelectDisplayByProductIdAndSelectCode(select);
                if (result <= 0) {
                    //Cannot update
                    break;
                }
            }
        }
        return result;
    }

    @Override
    public List<EntTblFactoryProductNew> selectListOptionValueOfProduct(String listProductId
                                                                      , List<EntTblFactoryProductSelect> listItemSelect) throws SQLException {
	    //list productId
	    String[] arrayProductId = listProductId.split(",");
	    Integer length = arrayProductId.length;
	    //List product
	    List<EntTblFactoryProductNew> listProduct = new ArrayList<EntTblFactoryProductNew>();
	    for (int i = 0; i < length; i++) {
	    	EntTblFactoryProductNew product = new EntTblFactoryProductNew();
	    	product.setProductId(Long.valueOf(arrayProductId[i]));
	    	selectDisplayValueForProduct(product, listItemSelect);
	    	//Add product to listProduct
	    	listProduct.add(product);
	    }
	    return listProduct;
    }
    /* EOE Block9 */

	/* BOE Block10 */
	/* (non-Javadoc)
	 * @see net.webike.RcProductFactory.service.ProductManageService#selecteCategoryCodeOfProduct(java.lang.String)
	 */
	@Override
	public Integer selectCategoryCodeOfProduct(Long productId) throws SQLException {
	    return tblFactoryProductGeneralMapper.selectCategoryCodeByProductId(productId);
	}

	/* (non-Javadoc)
	 * @see net.webike.RcProductFactory.service.ProductManageService#selectListCategory()
	 */
	@Override
	public List<EntMstCategory> selectListCategory() throws SQLException {
	    List<EntMstCategory> lstCategory = mstCategoryMapper.selectListCategoryCodeAndName();
	    //Unselect category at top of list
	    EntMstCategory category = new EntMstCategory();
	    category.setCategoryCode(" ");
	    category.setCategoryName("");
	    //Add to list category
	    lstCategory.add(0, category);
	    return lstCategory;
	}
	/* EOE Block10 */

	/* BOE Block11 */
    /************************************************************************
     * <b>Description:</b><br>
     *  processDataFitModelBySingleProduct.
     * @author		Le.Dinh
     * @date		27 Feb 2014
     * @param 		listFitModel	List<EntTblFactoryProductFitModel>
     * @param 		productList		List<EntTblFactoryProductNew>
     * @param 		factoryMatter	EntMstFactoryMatter
     * @param 		user			String
     * @throws		Exception	    Exception
     ************************************************************************/
    private void processDataFitModelBySingleProduct(List<EntTblFactoryProductFitModel> listFitModel, List<EntTblFactoryProductNew> productList,
    EntMstFactoryMatter factoryMatter, String user) throws Exception {
        int isContinue = 0;
        List<Long> pl = new ArrayList<Long>();
        EntTblFactoryProductNew product = null;
        if (productList != null && productList.size() != 0) {
            product = productList.get(0);
        }
        // start transaction.
        // check product code is null or non numberic
        if (product != null) {
            // add product code to list
            pl.add(product.getProductId());
            // delete all fit model by product code
            tblFactoryProductModelMapper.deleteFitModel(pl);
            // check list fit model null or size = 0
            if (listFitModel != null && listFitModel.size() != 0) {
                int i = 0;
                for (EntTblFactoryProductFitModel obj : listFitModel) {
                    // add model sort for object insert
                	obj.setProductId(product.getProductId());
                    obj.setFitModelSort(i);
                    obj.setUpdateUserId(user);
                    // insert each fit model to DB
                    //IF fitMode has been delete, don;t insert to DB
                    if (!StringUtils.equals(obj.getCrudFlg(), Constant.DELETE_MODE)) {
                        isContinue = tblFactoryProductModelMapper.insertToFitModel(obj);
                    }
                    // check if one insert fall then break
                    i++;
                    if (isContinue <= 0) {
                        break;
                    }
                }
                if (isContinue > 0) {
                    // update product error flg
                    product.setUpdatedUserId(user);
                    isContinue = tblFactoryProductMapper.updateProductModelErrorFlg(product);
                    if (isContinue > 0) {
                        // get the error count model by matter no
                        int errorCountModel = mstFactoryMatterMapper.selectErrorCountModelByMatterId(factoryMatter.getMatterNo());
                        int errorNum = errorCountModel + factoryMatter.getMatterFactoryErrorCountModel();
                        if (errorNum < 0) {
                            factoryMatter.setMatterFactoryErrorCountModel(0);
                        } else {
                            factoryMatter.setMatterFactoryErrorCountModel(errorNum);
                        }
                        // update error count model in factory matter
                        factoryMatter.setKousinUserId(user);
                        isContinue = mstFactoryMatterMapper.updateMatterErrorCountModel(factoryMatter);
                    }
                }
            } else {
                // update data in product and matter table when fit model is empty
                isContinue = updateDataWhenFitModelIsEmpty(pl, user, factoryMatter);
            }
            if (isContinue > 0) {
                // check if product has model error then no update
                if (product.getProductModelErrorFlg() == 0) {
                    isContinue = tblFactoryProductMapper.updateProductModelCheckFlg(product.getProductId());
                }
            }
        }
    }
    /************************************************************************
     * <b>Description:</b><br>
     * update data of product and matter when fit model list is empty.
     *
     * @author      Le.Dinh
     * @date        Feb 13, 2014
     * @param       pl list list product id.
     * @param       user user id
     * @param       factoryMatter object matter
     * @return      int
     ************************************************************************/
    private int updateDataWhenFitModelIsEmpty(List<Long> pl, String user, EntMstFactoryMatter factoryMatter) {
        // get list product with model error flg by list product id
        List<EntTblFactoryProductNew> prdList = tblFactoryProductMapper.selectProductListByProductIdList(pl);
        // set default param is 1 (mean process is ok)
        int isContinue = 1;
        int i = 0;
        for (EntTblFactoryProductNew obj: prdList) {
            // check if model error flg of product is 1
            // if 1 then set it to 0 then update data in product
            if (obj.getProductModelErrorFlg() == 1) {
                i++;
                obj.setProductModelErrorFlg(0);
                obj.setUpdatedUserId(user);
                isContinue = tblFactoryProductMapper.updateProductModelErrorFlg(obj);
            }
        }
        // if the i > 0, mean the above process is happen
        // then update data in matter
        if (i > 0) {
            if (isContinue > 0) {
                // get the error count model by matter no
                int errorCountModel = mstFactoryMatterMapper.selectErrorCountModelByMatterId(factoryMatter.getMatterNo());
                int errorNum = errorCountModel - i;
                if (errorNum >= 0) {
                    // update data in matter
                    factoryMatter.setMatterFactoryErrorCountModel(errorNum);
                    factoryMatter.setKousinUserId(user);
                    isContinue = mstFactoryMatterMapper.updateMatterErrorCountModel(factoryMatter);
                }
            }
        }
        return isContinue;
    }
	/* EOE Block11 */

	/* BOE Block12 */
	/* (non-Javadoc)
	 * @see net.webike.RcProductFactory.service.ProductManageService#selectlistSupplierByProductId(java.lang.Long)
	 */
	@Override
	public List<EntTblFactoryProductSupplier> selectListSupplierByProductId(Long productId) throws SQLException {
		List<EntTblFactoryProductSupplier> list = tblFactoryProductSupplierMapper.selectListSupplierByProductId(productId);
	    return list;
	}
	/* (non-Javadoc)
	 * @see net.webike.RcProductFactory.service.ProductEditService#selectListAllSiire()
	 */
	@Override
	public List<EntMstSiire> selectListAllSiire() throws SQLException {
		List<EntMstSiire> list = mstSiireMapper.selectListAllSiireNameS();
	    return list;
	}
	/* (non-Javadoc)
	 * @see net.webike.RcProductFactory.service.ProductEditService#selectListAllNouki()
	 */
	@Override
	public List<EntMstNouki> selectListAllNouki() throws SQLException {
		List<EntMstNouki> list = mstNoukiMapper.selectListAllNouki();
	    return list;
	}

	/************************************************************************
	 * <b>Description:</b><br>
	 *  saveDataForProductOfSaveSupplierStatus.
	 *
	 * @author		Tran.Thanh
	 * @date		3 Mar 2014
	 * @param 		listSupplier List<EntTblFactoryProductSupplier>
	 * @param 		entQuery EntTblFactoryProductNew
	 * @param 		actionForm ProductEditActionForm
	 * @return		true if no have exception.
	 * @throws Exception		boolean
	 ************************************************************************/
	private boolean saveDataForProductOfSaveSupplierStatus(List<EntTblFactoryProductSupplier> listSupplier
							, EntTblFactoryProductNew entQuery, ProductEditActionForm actionForm) throws Exception {
		for (EntTblFactoryProductSupplier supplier: listSupplier) {
			supplier.setUpdatedUserId(entQuery.getUpdatedUserId());
			supplier.setProductId(entQuery.getProductId());
			if (Constant.EDIT_MODE.equals(actionForm.getMode())) {
				if (StringUtils.equals(supplier.getCrudFlg(), Constant.DELETE_MODE)) {
					tblFactoryProductSupplierMapper.deleteSupplierByProductIdAndsiireCode(supplier.getProductId()
					                                                                    , supplier.getSiireCode());
				} else if (StringUtils.equals(supplier.getCrudFlg(), Constant.INSERT_MODE)) {
					tblFactoryProductSupplierMapper.insertSupplierByProductIdAndsiireCode(supplier);
				} else if (StringUtils.equals(supplier.getCrudFlg(), Constant.UPDATE_MODE)) {
					tblFactoryProductSupplierMapper.updateSupplierByProductIdAndsiireCode(supplier);
				}
			} else {
				if (!StringUtils.equals(supplier.getCrudFlg(), Constant.DELETE_MODE)) {
					tblFactoryProductSupplierMapper.insertSupplierByProductIdAndsiireCode(supplier);
				}
			}
		}
		return true;
	}
	/* EOE Block12 */

	/* BOE Block13 */
	/* (non-Javadoc)
	 * @see net.webike.RcProductFactory.service.ProductManageService#selectProductSupplierReleaseDateByProductid(java.lang.Long)
	 */
	@Override
	public EntTblFactoryProductGeneral selectProductFactoryProductGeneralByProductid(Long productId)  throws SQLException {
		EntTblFactoryProductGeneral  productSupplierReleaseDate = tblFactoryProductGeneralMapper.selectProductFactoryProductGeneralByProductid(
		                                                                                                                        productId);
		/* BOE #7206 Luong.Dai 2014/05/22 Set product condition if null data */
		if (null == productSupplierReleaseDate.getProductOpenPriceFlg()) {
			productSupplierReleaseDate.setProductOpenPriceFlg(0);
		}
		if (null == productSupplierReleaseDate.getProductProperSellingFlg()) {
			productSupplierReleaseDate.setProductProperSellingFlg(0);
		}
		if (null == productSupplierReleaseDate.getProductOrderProductFlg()) {
			productSupplierReleaseDate.setProductOrderProductFlg(0);
		}
		if (null == productSupplierReleaseDate.getProductNoReturnableFlg()) {
			productSupplierReleaseDate.setProductNoReturnableFlg(0);
		}
		if (null == productSupplierReleaseDate.getProductAmbiguousImageFlg()) {
			productSupplierReleaseDate.setProductAmbiguousImageFlg(0);
		}
		/* EOE #7206 Luong.Dai 2014/05/22 Set product condition if null data */

		return productSupplierReleaseDate;
	}
	/* (non-Javadoc)
	 * @see net.webike.RcProductFactory.service.ProductEditService#selectListAllSupplierStatus()
	 */
	@Override
	public List<EntMstProductSupplierStatus> selectListAllSupplierStatus() throws SQLException {
		List<EntMstProductSupplierStatus> list = mstProductSupplierStatusMapper.selectListAllSupplierStatus();
		return list;
	}
	/* EOE Block13 */

	/* BOE Block14 */
	/* EOE Block14 */

	/* BOE Block15 */
	/* (non-Javadoc)
	 * @see net.webike.RcProductFactory.service.ProductEditService#selectAllListProduct()
	 */
	@Override
	public List<EntTblFactoryProductNew> selectAllListProduct(Integer page, Integer pageSize, EntTblFactoryProductNew product) throws SQLException {
	    // Get limit and offset to load data.
	    Integer limit = pageSize;
        Integer offset = (page - 1) * limit;

        //Set limit and offset to Entity
        if (product == null) {
            product = new EntTblFactoryProductNew();
            //Set proper_price = null
            product.setProductProperPriceFrom(null);
            product.setProductProperPriceTo(null);
        }
        product.setLimit(limit);
        product.setOffset(offset);
	    List<EntTblFactoryProductNew> list = tblFactoryProductMapper.selectAllListProduct(product);
	    return list;
	}

	/* (non-Javadoc)
	 * @see net.webike.RcProductFactory.service.ProductEditService#selectCountListProduct()
	 */
	@Override
	public Integer selectCountListProduct(EntTblFactoryProductNew product) {
	    return tblFactoryProductMapper.selectCountListProduct(product);
	}

	/************************************************************************
	 * <b>Description:</b><br>
	 *  saveDataForProductOfSaveProduct.
	 *
	 * @author		Tran.Thanh
	 * @date		3 Mar 2014
	 * @param 		listProduct    List<EntTblFactoryProductNew>
	 * @param       userId         String
	 * @param       matter         EntMstFactoryMatterNew
	 * @return		true if no have exception
	 * @throws 		Exception		boolean
	 ************************************************************************/
	private boolean saveDataForProductOfSaveProduct(List<EntTblFactoryProductNew> listProduct
									                , String userId
									                , EntMstFactoryMatterNew matter) throws Exception {
		//List syouhin sys code of deleted product
		List<String> listDeleteSyouhinSysCode = new ArrayList<String>();
		for (int i = 0; i < listProduct.size(); i++) {
		    EntTblFactoryProductNew product = listProduct.get(i);
		    product.setUpdatedUserId(userId);
			Long productId = product.getProductId();
			//Check crudFlg, if crudFlg is empty, do nothing
			if (StringUtils.isNotEmpty(product.getCrudFlg())) {
			    // Select current product info
	            EntTblFactoryProductNew currentProduct = tblFactoryProductMapper.selectProductUpdatedOnByProductCode(productId);
	            if (currentProduct != null) {
	                // Check change update_on value
	                Date date = product.getUpdatedOn();
	                if (currentProduct.getUpdatedOn().compareTo(date) == 0) {
	                    if (StringUtils.equals(product.getCrudFlg(), Constant.DELETE_MODE)) {
	                    	//Add attribute to list delete attribute
	                    	listDeleteSyouhinSysCode.add(product.getProductSyouhinSysCode().toString());
	                        //Recount error
	                        countErrorOfProductWhenDeleteProduct(product, matter);
	                        // Delete ProductsGeneral
	                        tblFactoryProductMapper.deleteProductsGeneral(productId);
	                        // Delete product
	                        tblFactoryProductMapper.deleteProducts(productId);
	                    } else if (StringUtils.equals(product.getCrudFlg(), Constant.UPDATE_MODE)) {
	                        //Count error
                            countErrorOfProductDiscription(product, matter);
	                        // update table Description
	                        tblFactoryProductDescriptionMapper.updateProductDescriptionByProduct(product);
	                        // update table General
	                        tblFactoryProductGeneralMapper.updateProductGeneralByProductId(product);
	                        // update table product
	                        tblFactoryProductMapper.updateProductGeneralCountError(product);
	                    }
	                } else {
	                    return false;
	                }
	            } else { // Case product has been deleted by another user.
	                return false;
	            }
			}
		}
		//Save matter error count value
		mstFactoryMatterMapper.updateMatterErrorFlgByMatterCode(matter);
		//Update product maintenance flg
		if (listDeleteSyouhinSysCode != null && listDeleteSyouhinSysCode.size() > 0) {
			mstProductMapper.updateProductMaintenanceFlgToZeroBySyouhinSysCode(listDeleteSyouhinSysCode, userId);
		}
		return true;
	}

	/************************************************************************
	 * <b>Description:</b><br>
	 *  if product has error, recount error.
	 *
	 * @author		Luong.Dai
	 * @date		Mar 14, 2014
	 * @param       product        EntTblFactoryProductNew
	 * @param       matter         EntMstFactoryMatterNew
	 ************************************************************************/
	private void countErrorOfProductWhenDeleteProduct(EntTblFactoryProductNew product, EntMstFactoryMatterNew matter) {
	    Integer oldGeneralFlag = product.getProductGeneralErrorFlg();
	    Integer oldAttributeFlag = product.getProductAttributeErrorFlg();
	    Integer oldModelFlag = product.getProductModelErrorFlg();
	    Integer oldCategoryFlag = product.getProductCategoryErrorFlg();
	    if (oldGeneralFlag != null && oldGeneralFlag == 1) {
	        //Set matter flag value
            Integer matterGeneralCount = matter.getMatterFactoryErrorCountGeneral();
            matter.setMatterFactoryErrorCountGeneral(matterGeneralCount - 1);
	    }
	    if (oldAttributeFlag != null && oldAttributeFlag == 1) {
            //Set matter flag value
            Integer matterAttributeCount = matter.getMatterFactoryErrorCountAttribute();
            matter.setMatterFactoryErrorCountAttribute(matterAttributeCount - 1);
        }
	    if (oldModelFlag != null && oldModelFlag == 1) {
            //Set matter flag value
            Integer matterModelCount = matter.getMatterFactoryErrorCountModel();
            matter.setMatterFactoryErrorCountModel(matterModelCount - 1);
        }
	    if (oldCategoryFlag != null && oldCategoryFlag == 1) {
            //Set matter flag value
            Integer matterCategoryCount = matter.getMatterFactoryErrorCountCategory();
            matter.setMatterFactoryErrorCountCategory(matterCategoryCount - 1);
        }
	}

	/************************************************************************
	 * <b>Description:</b><br>
	 *  count error for block 15.
	 *
	 * @author		Luong.Dai
	 * @date		Mar 14, 2014
	 * @param       product        EntTblFactoryProductNew
	 * @param       matter		   EntMstFactoryMatterNew
	 ************************************************************************/
	private void countErrorOfProductDiscription(EntTblFactoryProductNew product, EntMstFactoryMatterNew matter) {
	    Integer oldGeneralFlag = product.getProductGeneralErrorFlg();
        //Check productName
        if (product.getProductName() != null) {
            if (product.getProductName().length() > Constant.PRODUCT_MAX_PRODUCT_NAME_LENGTH) {
                product.setProductGeneralErrorFlg(1);
            }
        }
        //Check descriptionSummary
        if (product.getDescriptionSummary() != null) {
            if (product.getDescriptionSummary().length() > Constant.PRODUCT_MAX_DESCRIPTION_SUMMARY_LENGTH) {
                product.setProductGeneralErrorFlg(1);
            }
        }
        //Check descriptionSummary
        if (product.getDescriptionCaution() != null) {
            if (product.getDescriptionCaution().length() > Constant.PRODUCT_MAX_DESCRIPTION_CAUTION_LENGTH) {
                product.setProductGeneralErrorFlg(1);
            }
        }
        //Check descriptionRemarks
        if (product.getDescriptionRemarks() != null) {
            if (product.getDescriptionRemarks().length() > Constant.PRODUCT_MAX_DESCRIPTION_REMARKS_LENGTH) {
                product.setProductGeneralErrorFlg(1);
            }
        }
        //Set matter flag value
        Integer matterGeneralCount = matter.getMatterFactoryErrorCountGeneral();
        matterGeneralCount = matterGeneralCount + (product.getProductGeneralErrorFlg() - oldGeneralFlag);
        matter.setMatterFactoryErrorCountGeneral(matterGeneralCount);
	}

	/* (non-Javadoc)
	 * @see net.webike.RcProductFactory.service.ProductEditService#selectCountNumberProductOfMatter(java.lang.Long)
	 */
	@Override
	public Integer selectCountNumberProductOfMatter(Long matterNo) throws SQLException {
	    Integer numberProduct = tblFactoryProductMapper.selectCountNumberProductOfMatter(matterNo);
	    if (numberProduct == null) {
	        numberProduct = 0;
	    }
	    return numberProduct;
	}

	/* (non-Javadoc)
	 * @see net.webike.RcProductFactory.service.ProductEditService#saveDataOfListProductAtProductEdit(java.util.List)
	 */
	@Override
	@Transactional
	public boolean saveDataOfListProductAtProductEdit(List<EntTblFactoryProductNew> listProduct
	                                                  , String userId
	                                                  , String matterNo) throws SQLException {
        boolean result = true;
	    // create transaction.
        TransactionStatus status = null;
        try {
            // start transaction.
            status =  this.startTransaction();
            // matter for count error
            EntMstFactoryMatterNew matter = new EntMstFactoryMatterNew();
            matter.setMatterFactoryErrorCountGeneral(0);
            matter.setMatterFactoryErrorCountCategory(0);
            matter.setMatterFactoryErrorCountModel(0);
            matter.setMatterFactoryErrorCountAttribute(0);
            matter.setUpdatedUserId(userId);
            matter.setMatterNo(Long.valueOf(matterNo));

            result = saveDataForProductOfSaveProduct(listProduct, userId, matter);
        } catch (Exception e) {
            this.rollback(status);
            result = false;
        }
        if (result) {
            this.commit(status);
        } else {
            this.rollback(status);
        }
        return result;
	}

	@Override
	@Transactional
	public boolean saveDataPopupBlock15(List<EntReplaceElement> listObjPopupBlock15
									, List<Long> listProductId
									, EntTblFactoryProductNew entProductSearch
									, String updateUserId) throws Exception {
		boolean result = true;
	    // create transaction.
        TransactionStatus status = null;
        try {
            // start transaction.
            status =  this.startTransaction();

            //Get List Product Id by Search Condition and set to listProductId
            if (entProductSearch != null) {
            	//Select ALL is checked then get list Product by Search Condition
            	List<EntTblFactoryProductNew> listProduct = tblFactoryProductMapper.selectListProductIdFilter(entProductSearch);
            	//Check if list not null ( case productId had been deleted )
            	if (listProduct.size() > 0) {
            		listProductId = new ArrayList<Long>();
            		// Parse List Product to List Long
                	for (EntTblFactoryProductNew entProduct : listProduct) {
                		if (entProduct.getProductId() != null) {
                			listProductId.add(entProduct.getProductId());
                		}
                	}
            	}
            }

        	//Select All is not checked
        	saveDataForEachProduct(listProductId, listObjPopupBlock15, updateUserId);

        } catch (MyBatisSystemException e) {
        	this.rollback(status);
            result = false;
        } catch (Exception e) {
        	this.rollback(status);
            result = false;
        }
        if (result) {
            this.commit(status);
        } else {
            this.rollback(status);
        }
        return result;
	}

	/************************************************************************
	 * <b>Description:</b><br>
	 *  save Data Popup block 15 For Each Product.
	 *
	 * @author		Tran.Thanh
	 * @date		Apr 21, 2014
	 * @param 		listProductId listProductId
	 * @param 		listObjPopupBlock15 list Object Popup Block15
	 * @param 		updateUserId updated user Id
	 * @return		boolean
	 * @throws 		Exception		boolean
	 ************************************************************************/
	private boolean saveDataForEachProduct(List<Long> listProductId
										, List<EntReplaceElement> listObjPopupBlock15
										, String updateUserId) throws Exception {
		boolean res = false;

		// Entity for insert/update table tbl_factory_product_general
		EntTblFactoryProductGeneral entTblFactoryProductGeneral = new EntTblFactoryProductGeneral();
		entTblFactoryProductGeneral.setUpdatedUserId(updateUserId);

		// Entity for insert/update table tbl_factory_product_description (using entity Product)
		EntTblFactoryProductDescription entTblFactoryProductDescription = new EntTblFactoryProductDescription();
		entTblFactoryProductDescription.setUpdatedUserId(updateUserId);

		// Entity for insert/update table tbl_factory_product_supplier_price
		EntTblFactoryProductSupplierPrice entTblFactoryProductSupplierPrice = new EntTblFactoryProductSupplierPrice();
		entTblFactoryProductSupplierPrice.setUpdatedUserId(updateUserId);

		for (EntReplaceElement obj : listObjPopupBlock15) {
			String typeToSave = obj.getField();
			// Set value to EntTblFactoryProductGeneral
			if (typeToSave.equals("category")) {
				entTblFactoryProductGeneral.setProductCategoryCode(Integer.parseInt(obj.getValue()));
			} else if (typeToSave.equals("brandName")) {
				entTblFactoryProductGeneral.setProductBrandCode(Integer.parseInt(obj.getValue()));
			} else if (typeToSave.equals("productName")) {
				entTblFactoryProductGeneral = initEnityGeneralForQueryWithProductName(entTblFactoryProductGeneral, obj);
			} else if (typeToSave.equals("janPopupBlock15")) {
				entTblFactoryProductGeneral = initEnityGeneralForQueryWithProductEanCode(entTblFactoryProductGeneral, obj);
			} else if (typeToSave.equals("listPrice")) {
				entTblFactoryProductGeneral = initEnityGeneralForQueryWithProductProperPrice(entTblFactoryProductGeneral, obj);
			} else if (typeToSave.equals("productCodeNoFlag")) {
				entTblFactoryProductGeneral = initEnityGeneralForQueryWithProductCodeNoFlag(entTblFactoryProductGeneral, obj);
			} else if (typeToSave.equals("dateOfIssue")) {
				entTblFactoryProductGeneral = initEnityGeneralForQueryWithProductSupplierReleaseDate(entTblFactoryProductGeneral, obj);
			} else if (typeToSave.equals("allTypeFlags")) {
				entTblFactoryProductGeneral = initEnityGeneralForQueryWithAllTypeFlags(entTblFactoryProductGeneral, obj);
			} else if (typeToSave.equals("descriptionSummary")) {
				entTblFactoryProductDescription = initEnityDescriptionSummary(entTblFactoryProductDescription, obj);
			} else if (typeToSave.equals("descriptionCaution")) {
				entTblFactoryProductDescription = initEnityDescriptionCaution(entTblFactoryProductDescription, obj);
			} else if (typeToSave.equals("descriptionSentence")) {
				entTblFactoryProductDescription = initEnityDescriptionSentence(entTblFactoryProductDescription, obj);
			} else if (typeToSave.equals("descriptionRemarks")) {
				entTblFactoryProductDescription = initEnityDescriptionRemarks(entTblFactoryProductDescription, obj);
			} else if (typeToSave.equals("division")) {
				entTblFactoryProductSupplierPrice = initEntitySupplierPricePrice(entTblFactoryProductSupplierPrice, obj);
			} else if (typeToSave.contains("image")) {
				res = processSavePopupBlock15Image(listProductId, obj, updateUserId);
				if (!res) {
					return false;
				}
			}
		}

		//process save/update table tbl_factory_product_general
		boolean saveGeneral = processSaveProductGeneral(entTblFactoryProductGeneral, listProductId);
		if (!saveGeneral) {
			return false;
		}

		//process save/update table tbl_factory_product_condition
		boolean saveCondition = processSaveProductCondition(entTblFactoryProductGeneral, listProductId);
		if (!saveCondition) {
			return false;
		}

		//process save/ table tbl_factory_product_description
		boolean saveDescription = processSaveProductDescription(entTblFactoryProductDescription, listProductId);
		if (!saveDescription) {
			return false;
		}

		//process save/ table tbl_factory_product_supplier_price
		boolean saveSupplierPrice = processSaveProductSupplierPrice(entTblFactoryProductSupplierPrice, listProductId);
		if (!saveSupplierPrice) {
			return false;
		}

		Map<Integer, Object> mapEntity = new HashMap<Integer , Object>();
		//Get Data from DB and set to mapProduct.

		mapEntity.put(1, entTblFactoryProductGeneral);
		mapEntity.put(2, entTblFactoryProductDescription);

		boolean saveErrorFlg = processSaveErrorFlgProduct(mapEntity, listProductId, updateUserId);

		if (!saveErrorFlg) {
			return false;
		}

		return res;
	}

	/************************************************************************
	 * <b>Description:</b><br>
	 *  init Enity product General For Query With Product Name.
	 *
	 * @author		Tran.Thanh
	 * @date		Apr 22, 2014
	 * @param 		entTblFactoryProductGeneral EntTblFactoryProductGeneral for query
	 * @param 		obj EntReplaceElement
	 * @return		EntTblFactoryProductGeneral
	 ************************************************************************/
	private EntTblFactoryProductGeneral initEnityGeneralForQueryWithProductName(
				EntTblFactoryProductGeneral entTblFactoryProductGeneral,
				EntReplaceElement obj) {
		entTblFactoryProductGeneral.setProductName(obj.getValue());
		String changeType = obj.getChangeType();
		//set mode replace for entity query
		if (changeType.equals(Constant.STR_REPLACE_ALL)) {
			entTblFactoryProductGeneral.setReplaceModeProductName(Constant.REPLACE_ALL);
		} else if (changeType.equals(Constant.STR_REPLACE_HEAD)) {
			entTblFactoryProductGeneral.setReplaceModeProductName(Constant.REPLACE_HEAD);
		} else if (changeType.equals(Constant.STR_REPLACE_TAIL)) {
			entTblFactoryProductGeneral.setReplaceModeProductName(Constant.REPLACE_TAIL);
		} else if (changeType.equals(Constant.STR_FIND_REPLACE)) {
			entTblFactoryProductGeneral.setReplaceModeProductName(Constant.FIND_REPLACE);
			entTblFactoryProductGeneral.setProductNameFind(obj.getFindValue());
		}
		return entTblFactoryProductGeneral;
	}

	/************************************************************************
	 * <b>Description:</b><br>
	 *  init Enity General For Query With Product Ean Code.
	 *
	 * @author		Tran.Thanh
	 * @date		Apr 22, 2014
	 * @param 		entTblFactoryProductGeneral EntTblFactoryProductGeneral for query
	 * @param 		obj EntReplaceElement
	 * @return		EntTblFactoryProductGeneral
	 ************************************************************************/
	private EntTblFactoryProductGeneral initEnityGeneralForQueryWithProductEanCode(
			EntTblFactoryProductGeneral entTblFactoryProductGeneral,
			EntReplaceElement obj) {
		entTblFactoryProductGeneral.setProductEanCode(obj.getValue());
		String changeType = obj.getChangeType();
		//set mode replace for entity query
		if (changeType.equals(Constant.STR_REPLACE_ALL)) {
			entTblFactoryProductGeneral.setReplaceModeProductEanCode(Constant.REPLACE_ALL);
		} else if (changeType.equals(Constant.STR_REPLACE_HEAD)) {
			entTblFactoryProductGeneral.setReplaceModeProductEanCode(Constant.REPLACE_HEAD);
		} else if (changeType.equals(Constant.STR_REPLACE_TAIL)) {
			entTblFactoryProductGeneral.setReplaceModeProductEanCode(Constant.REPLACE_TAIL);
		} else if (changeType.equals(Constant.STR_FIND_REPLACE)) {
			entTblFactoryProductGeneral.setReplaceModeProductEanCode(Constant.FIND_REPLACE);
			entTblFactoryProductGeneral.setProductEanCodeFind(obj.getFindValue());
		}
		return entTblFactoryProductGeneral;
	}

	/************************************************************************
	 * <b>Description:</b><br>
	 *  init Enity General For Query With Product Proper Price.
	 *
	 * @author		Tran.Thanh
	 * @date		Apr 22, 2014
	 * @param 		entTblFactoryProductGeneral EntTblFactoryProductGeneral
	 * @param 		obj EntReplaceElement
	 * @return		EntTblFactoryProductGeneral
	 ************************************************************************/
	private EntTblFactoryProductGeneral initEnityGeneralForQueryWithProductProperPrice(
			EntTblFactoryProductGeneral entTblFactoryProductGeneral,
			EntReplaceElement obj) {
		entTblFactoryProductGeneral.setProductProperPriceStr(obj.getValue());
		String changeType = obj.getChangeType();
		//set mode replace for entity query
		if (changeType.equals(Constant.STR_REPLACE_ALL)) {
			entTblFactoryProductGeneral.setReplaceModeProductProperPrice(Constant.REPLACE_ALL);
		} else if (changeType.equals(Constant.STR_REPLACE_HEAD)) {
			entTblFactoryProductGeneral.setReplaceModeProductProperPrice(Constant.REPLACE_HEAD);
		} else if (changeType.equals(Constant.STR_REPLACE_TAIL)) {
			entTblFactoryProductGeneral.setReplaceModeProductProperPrice(Constant.REPLACE_TAIL);
		} else if (changeType.equals(Constant.STR_FIND_REPLACE)) {
			entTblFactoryProductGeneral.setReplaceModeProductProperPrice(Constant.FIND_REPLACE);
			entTblFactoryProductGeneral.setProductProperPriceFind(obj.getFindValue());
		}
		return entTblFactoryProductGeneral;
	}

	/************************************************************************
	 * <b>Description:</b><br>
	 *  init Enity General For Query With Product Code No Flag.
	 *
	 * @author		Tran.Thanh
	 * @date		Apr 22, 2014
	 * @param 		entTblFactoryProductGeneral EntTblFactoryProductGeneral for query
	 * @param 		obj EntReplaceElement
	 * @return		EntTblFactoryProductGeneral
	 ************************************************************************/
	private EntTblFactoryProductGeneral initEnityGeneralForQueryWithProductCodeNoFlag(
			EntTblFactoryProductGeneral entTblFactoryProductGeneral,
			EntReplaceElement obj) {
		if (obj.getCheckFlg().equals("1")) {
			entTblFactoryProductGeneral.setProductWebikeCodeFlg(1);
		} else {
			entTblFactoryProductGeneral.setProductWebikeCodeFlg(0);
			entTblFactoryProductGeneral.setProductCode(obj.getValue());
			String changeType = obj.getChangeType();
			//set mode replace for entity query
			if (changeType.equals(Constant.STR_REPLACE_ALL)) {
				entTblFactoryProductGeneral.setReplaceModeProductCode(Constant.REPLACE_ALL);
			} else if (changeType.equals(Constant.STR_REPLACE_HEAD)) {
				entTblFactoryProductGeneral.setReplaceModeProductCode(Constant.REPLACE_HEAD);
			} else if (changeType.equals(Constant.STR_REPLACE_TAIL)) {
				entTblFactoryProductGeneral.setReplaceModeProductCode(Constant.REPLACE_TAIL);
			} else if (changeType.equals(Constant.STR_FIND_REPLACE)) {
				entTblFactoryProductGeneral.setReplaceModeProductCode(Constant.FIND_REPLACE);
				entTblFactoryProductGeneral.setProductCodeFind(obj.getFindValue());
			}
		}

		return entTblFactoryProductGeneral;
	}

	/************************************************************************
	 * <b>Description:</b><br>
	 *  init Enity General For Query With Product Supplier Release Date.
	 *
	 * @author		Tran.Thanh
	 * @date		Apr 22, 2014
	 * @param 		entTblFactoryProductGeneral EntTblFactoryProductGeneral for query
	 * @param 		obj EntReplaceElement
	 * @return		EntTblFactoryProductGeneral
	 ************************************************************************/
	private EntTblFactoryProductGeneral initEnityGeneralForQueryWithProductSupplierReleaseDate(
			EntTblFactoryProductGeneral entTblFactoryProductGeneral,
			EntReplaceElement obj) {
		entTblFactoryProductGeneral.setProductSupplierReleaseDate(obj.getValue());
		String changeType = obj.getChangeType();
		//set mode replace for entity query
		if (changeType.equals(Constant.STR_REPLACE_ALL)) {
			entTblFactoryProductGeneral.setReplaceModeProductReleaseDate(Constant.REPLACE_ALL);
		} else if (changeType.equals(Constant.STR_REPLACE_HEAD)) {
			entTblFactoryProductGeneral.setReplaceModeProductReleaseDate(Constant.REPLACE_HEAD);
		} else if (changeType.equals(Constant.STR_REPLACE_TAIL)) {
			entTblFactoryProductGeneral.setReplaceModeProductReleaseDate(Constant.REPLACE_TAIL);
		} else if (changeType.equals(Constant.STR_FIND_REPLACE)) {
			entTblFactoryProductGeneral.setReplaceModeProductReleaseDate(Constant.FIND_REPLACE);
			entTblFactoryProductGeneral.setProductSupplierReleaseDateFind(obj.getFindValue());
		}
		return entTblFactoryProductGeneral;
	}

	/************************************************************************
	 * <b>Description:</b><br>
	 *  init Enity General For Query With All Type Flags.
	 *
	 * @author		Tran.Thanh
	 * @date		Apr 22, 2014
	 * @param 		entTblFactoryProductGeneral EntTblFactoryProductGeneral for query
	 * @param 		obj EntReplaceElement
	 * @return		EntTblFactoryProductGeneral
	 ************************************************************************/
	private EntTblFactoryProductGeneral initEnityGeneralForQueryWithAllTypeFlags(
			EntTblFactoryProductGeneral entTblFactoryProductGeneral,
			EntReplaceElement obj) {
		/* BOE #7204 Luong.Dai 2014/04/25 Set default of Flag to 0 */
		entTblFactoryProductGeneral.setProductOpenPriceFlg(0);
		entTblFactoryProductGeneral.setProductProperSellingFlg(0);
		entTblFactoryProductGeneral.setProductOrderProductFlg(0);
		entTblFactoryProductGeneral.setProductNoReturnableFlg(0);
		entTblFactoryProductGeneral.setProductAmbiguousImageFlg(0);
		/* EOE #7204 Luong.Dai 2014/04/25 Set default of Flag to 0 */
		String listTypeSelect = obj.getValue();
		String[] parts = listTypeSelect.split(",");
		for (String flag : parts) {
			if (flag.contains("PRODUCT_OPEN_PRICE_FLG")) {
				entTblFactoryProductGeneral.setProductOpenPriceFlg(1);
			} else if (flag.contains("PRODUCT_PROPER_SELLING_FLG")) {
				entTblFactoryProductGeneral.setProductProperSellingFlg(1);
			} else if (flag.contains("PRODUCT_ORDER_PRODUCT_FLG")) {
				entTblFactoryProductGeneral.setProductOrderProductFlg(1);
			} else if (flag.contains("PRODUCT_NO_RETURNABLE_FLG")) {
				entTblFactoryProductGeneral.setProductNoReturnableFlg(1);
			} else if (flag.contains("PRODUCT_AMBIGUOUS_IMAGE_FLG")) {
				entTblFactoryProductGeneral.setProductAmbiguousImageFlg(1);
			}
		}

		return entTblFactoryProductGeneral;
	}

	/************************************************************************
	 * <b>Description:</b><br>
	 *  init Enity Description Summary.
	 *
	 * @author		Tran.Thanh
	 * @date		Apr 23, 2014
	 * @param 		entTblFactoryProductDescription EntTblFactoryProductDescription for query
	 * @param 		obj EntReplaceElement
	 * @return		EntTblFactoryProductDescription
	 ************************************************************************/
	private EntTblFactoryProductDescription initEnityDescriptionSummary(
					EntTblFactoryProductDescription entTblFactoryProductDescription,
					EntReplaceElement obj) {
		entTblFactoryProductDescription.setDescriptionSummary(obj.getValue());
		String changeType = obj.getChangeType();
		//set mode replace for entity query
		if (changeType.equals(Constant.STR_REPLACE_ALL)) {
			entTblFactoryProductDescription.setReplaceModeDescriptionSummary(Constant.REPLACE_ALL);
		} else if (changeType.equals(Constant.STR_REPLACE_HEAD)) {
			entTblFactoryProductDescription.setReplaceModeDescriptionSummary(Constant.REPLACE_HEAD);
		} else if (changeType.equals(Constant.STR_REPLACE_TAIL)) {
			entTblFactoryProductDescription.setReplaceModeDescriptionSummary(Constant.REPLACE_TAIL);
		} else if (changeType.equals(Constant.STR_FIND_REPLACE)) {
			entTblFactoryProductDescription.setReplaceModeDescriptionSummary(Constant.FIND_REPLACE);
			entTblFactoryProductDescription.setDescriptionSummaryFind(obj.getFindValue());
		}
		return entTblFactoryProductDescription;
	}

	/************************************************************************
	 * <b>Description:</b><br>
	 *  init Enity Description Caution.
	 *
	 * @author		Tran.Thanh
	 * @date		Apr 23, 2014
	 * @param 		entTblFactoryProductDescription EntTblFactoryProductDescription
	 * @param 		obj EntReplaceElement
	 * @return		EntTblFactoryProductDescription
	 ************************************************************************/
	private EntTblFactoryProductDescription initEnityDescriptionCaution(
			EntTblFactoryProductDescription entTblFactoryProductDescription,
			EntReplaceElement obj) {
		entTblFactoryProductDescription.setDescriptionCaution(obj.getValue());
		String changeType = obj.getChangeType();
		//set mode replace for entity query
		if (changeType.equals(Constant.STR_REPLACE_ALL)) {
			entTblFactoryProductDescription.setReplaceModeDescriptionCaution(Constant.REPLACE_ALL);
		} else if (changeType.equals(Constant.STR_REPLACE_HEAD)) {
			entTblFactoryProductDescription.setReplaceModeDescriptionCaution(Constant.REPLACE_HEAD);
		} else if (changeType.equals(Constant.STR_REPLACE_TAIL)) {
			entTblFactoryProductDescription.setReplaceModeDescriptionCaution(Constant.REPLACE_TAIL);
		} else if (changeType.equals(Constant.STR_FIND_REPLACE)) {
			entTblFactoryProductDescription.setReplaceModeDescriptionCaution(Constant.FIND_REPLACE);
			entTblFactoryProductDescription.setDescriptionCautionFind(obj.getFindValue());
		}
		return entTblFactoryProductDescription;
	}

	/************************************************************************
	 * <b>Description:</b><br>
	 *  init Enity Description Sentence.
	 *
	 * @author		Tran.Thanh
	 * @date		Apr 23, 2014
	 * @param 		entTblFactoryProductDescription EntTblFactoryProductDescription
	 * @param 		obj EntReplaceElement
	 * @return		EntTblFactoryProductDescription
	 ************************************************************************/
	private EntTblFactoryProductDescription initEnityDescriptionSentence(
			EntTblFactoryProductDescription entTblFactoryProductDescription,
			EntReplaceElement obj) {
		entTblFactoryProductDescription.setDescriptionSentence(obj.getValue());
		String changeType = obj.getChangeType();
		//set mode replace for entity query
		if (changeType.equals(Constant.STR_REPLACE_ALL)) {
			entTblFactoryProductDescription.setReplaceModeDescriptionSentence(Constant.REPLACE_ALL);
		} else if (changeType.equals(Constant.STR_REPLACE_HEAD)) {
			entTblFactoryProductDescription.setReplaceModeDescriptionSentence(Constant.REPLACE_HEAD);
		} else if (changeType.equals(Constant.STR_REPLACE_TAIL)) {
			entTblFactoryProductDescription.setReplaceModeDescriptionSentence(Constant.REPLACE_TAIL);
		} else if (changeType.equals(Constant.STR_FIND_REPLACE)) {
			entTblFactoryProductDescription.setReplaceModeDescriptionSentence(Constant.FIND_REPLACE);
			entTblFactoryProductDescription.setDescriptionSentenceFind(obj.getFindValue());
		}
		return entTblFactoryProductDescription;
	}

	/************************************************************************
	 * <b>Description:</b><br>
	 *  init Enity Description Remarks.
	 *
	 * @author		Tran.Thanh
	 * @date		Apr 23, 2014
	 * @param 		entTblFactoryProductDescription EntTblFactoryProductDescription
	 * @param 		obj EntReplaceElement
	 * @return		EntTblFactoryProductDescription
	 ************************************************************************/
	private EntTblFactoryProductDescription initEnityDescriptionRemarks(
			EntTblFactoryProductDescription entTblFactoryProductDescription,
			EntReplaceElement obj) {
		entTblFactoryProductDescription.setDescriptionRemarks(obj.getValue());
		String changeType = obj.getChangeType();
		//set mode replace for entity query
		if (changeType.equals(Constant.STR_REPLACE_ALL)) {
			entTblFactoryProductDescription.setReplaceModeDescriptionRemarks(Constant.REPLACE_ALL);
		} else if (changeType.equals(Constant.STR_REPLACE_HEAD)) {
			entTblFactoryProductDescription.setReplaceModeDescriptionRemarks(Constant.REPLACE_HEAD);
		} else if (changeType.equals(Constant.STR_REPLACE_TAIL)) {
			entTblFactoryProductDescription.setReplaceModeDescriptionRemarks(Constant.REPLACE_TAIL);
		} else if (changeType.equals(Constant.STR_FIND_REPLACE)) {
			entTblFactoryProductDescription.setReplaceModeDescriptionRemarks(Constant.FIND_REPLACE);
			entTblFactoryProductDescription.setDescriptionRemarksFind(obj.getFindValue());
		}
		return entTblFactoryProductDescription;
	}

	/************************************************************************
	 * <b>Description:</b><br>
	 *  init Entity Supplier Price Price.
	 *
	 * @author		Tran.Thanh
	 * @date		Apr 23, 2014
	 * @param 		entTblFactoryProductSupplierPrice EntTblFactoryProductSupplierPrice for query.
	 * @param 		obj EntReplaceElement
	 * @return		EntTblFactoryProductSupplierPrice
	 ************************************************************************/
	private EntTblFactoryProductSupplierPrice initEntitySupplierPricePrice(
			EntTblFactoryProductSupplierPrice entTblFactoryProductSupplierPrice,
			EntReplaceElement obj) {
		entTblFactoryProductSupplierPrice.setSupplierPricePriceStr(obj.getValue());
		String changeType = obj.getChangeType();
		//set mode replace for entity query
		if (changeType.equals(Constant.STR_REPLACE_ALL)) {
			entTblFactoryProductSupplierPrice.setReplaceModeSupplierPricePrice(Constant.REPLACE_ALL);
		} else if (changeType.equals(Constant.STR_REPLACE_HEAD)) {
			entTblFactoryProductSupplierPrice.setReplaceModeSupplierPricePrice(Constant.REPLACE_HEAD);
		} else if (changeType.equals(Constant.STR_REPLACE_TAIL)) {
			entTblFactoryProductSupplierPrice.setReplaceModeSupplierPricePrice(Constant.REPLACE_TAIL);
		} else if (changeType.equals(Constant.STR_FIND_REPLACE)) {
			entTblFactoryProductSupplierPrice.setReplaceModeSupplierPricePrice(Constant.FIND_REPLACE);
			entTblFactoryProductSupplierPrice.setSupplierPricePriceFind(obj.getFindValue());
		}
		return entTblFactoryProductSupplierPrice;
	}

	/************************************************************************
	 * <b>Description:</b><br>
	 *  process Save Popup Block15 - Table TblFactoryProductImage.
	 *
	 * @author		Tran.Thanh
	 * @date		Apr 21, 2014
	 * @param 		listProductId listProductId
	 * @param 		entReplaceElement EntReplaceElement
	 * @param 		updateUserId update user id
	 * @return		boolean
	 * @throws 		SQLException		boolean
	 ************************************************************************/
	@Transactional
	private boolean processSavePopupBlock15Image(List<Long> listProductId
												, EntReplaceElement entReplaceElement
												, String updateUserId) throws SQLException {
		String changeType = entReplaceElement.getChangeType();
		if (changeType == null) {
			return false;
		}

		//BOE #9601 Tran.Thanh 2014/07/01 : Add list image to delete and update
		//List image to delete muti product
		List<EntTblFactoryProductImage> listImageToDelete = new ArrayList<EntTblFactoryProductImage>();

		List<Long> listProductIdImageToInsertOrUpdate = new ArrayList<Long>();
		//EOE #9601 Tran.Thanh 2014/07/01 : Add list image to delete and update

		for (Long productId : listProductId) {
			EntTblFactoryProductImage entTblFactoryProductImage = new EntTblFactoryProductImage();
			//create entity to insert or update
			entTblFactoryProductImage.setProductId(productId);
			entTblFactoryProductImage.setProductImageDetailPath(entReplaceElement.getImageDetailPath());
			entTblFactoryProductImage.setProductImageThumbnailPath(entReplaceElement.getImageThumbPath());
			entTblFactoryProductImage.setImageSort(Integer.parseInt(entReplaceElement.getField()
										.substring(Constant.IMAGE_CHAR_HEAD, entReplaceElement.getField().length())));
			entTblFactoryProductImage.setReferMode(entReplaceElement.getImageMode());
			entTblFactoryProductImage.setUpdatetedUserId(updateUserId);

			//set mode replace for entity query
			if (changeType.equals(Constant.STR_REPLACE_ALL)) {
				entTblFactoryProductImage.setReplaceMode(Constant.REPLACE_ALL);
			} else if (changeType.equals(Constant.STR_REPLACE_HEAD)) {
				entTblFactoryProductImage.setReplaceMode(Constant.REPLACE_HEAD);
			} else if (changeType.equals(Constant.STR_REPLACE_TAIL)) {
				entTblFactoryProductImage.setReplaceMode(Constant.REPLACE_TAIL);
			} else if (changeType.equals(Constant.STR_FIND_REPLACE)) {
				entTblFactoryProductImage.setReplaceMode(Constant.FIND_REPLACE);
				entTblFactoryProductImage.setFindThumb(entReplaceElement.getFindThumb());
				entTblFactoryProductImage.setFindDetail(entReplaceElement.getFindThumb());
			}

			//excute query
			if (StringUtils.isEmpty(entTblFactoryProductImage.getProductImageDetailPath())) {
				//When select checkbock image but not have value productImageDetailPath, so delete this image
				if ((entTblFactoryProductImage.getProductId() != null)
						&& (entTblFactoryProductImage.getImageSort() != null)) {
					//BOE #9601 Tran.Thanh : add entityProductImage to list for delete
					//tblFactoryProductImageMapper.deleteProductImageByProductIdAndProductImageSort(entTblFactoryProductImage);
					listImageToDelete.add(entTblFactoryProductImage);
					//EOE #9601 Tran.Thanh : add entityProductImage to list for delete
				}
			} else {
				//BOE #9601 Tran.Thanh : add entityProductImage to list for update
//				Integer k = tblFactoryProductImageMapper.updateTypeReplaceAll(entTblFactoryProductImage);
//				if (k == 0) {
//					res = false;
//				}
				listProductIdImageToInsertOrUpdate.add(productId);
				//EOE #9601 Tran.Thanh : add entityProductImage to list for update
			}
		}

		//EOE #9612 Tran.Thanh : add process to delete and update muti image
		//Inset or update image
		boolean resInsUpd  = insertOrUpdateImageByListProductIdAndEntImage(listProductIdImageToInsertOrUpdate, entReplaceElement, updateUserId);

		//Delete List Image
		boolean resDel = deleteImageByListProductId(listImageToDelete);
		//EOE #9612 Tran.Thanh : add process to delete and update muti image

		return resInsUpd && resDel;
	}

	/************************************************************************
	 * <b>Description:</b><br>
	 *  delete Image By ListProductId.
	 *
	 * @author		Tran.Thanh
	 * @date		Jul 2, 2014
	 * @param 		listImageToDelete list image to delete
	 * @return 		true if delete success
	 * @throws 		SQLException		boolean
	 ************************************************************************/
	private boolean deleteImageByListProductId(List<EntTblFactoryProductImage> listImageToDelete) throws SQLException {
		// Delete list image when imageDetail is empty
		if (listImageToDelete.size() > 0) {
			//listImageToDelete have the same imageSort, so get one
			int sortImage = listImageToDelete.get(0).getImageSort();
			// k is number of row effect
			int k = tblFactoryProductImageMapper.deleteProductImageByProductIdAndProductImageSort(listImageToDelete, sortImage);
			//if size list > 0 and row effect = 0, return false
			if (k == 0) {
				return false;
			}
		}
		return true;
	}


	/************************************************************************
	 * <b>Description:</b><br>
	 *  insert Or Update Image By LIstProductIdAndEntImage.
	 *
	 * @author		Tran.Thanh
	 * @date		Jul 2, 2014
	 * @param 		listProductIdImageToInsertOrUpdate listProductId to insert or update.
	 * @param 		entReplaceElement ent to replace
	 * @param 		updateUserId		void
	 * @return		true if insert or update success
	 ************************************************************************/
	private boolean insertOrUpdateImageByListProductIdAndEntImage(List<Long> listProductIdImageToInsertOrUpdate
			, EntReplaceElement entReplaceElement, String updateUserId) {
		//BOE #9601 Tran.Thanh : add process to delete and update muti image
		boolean res = true;
		EntTblFactoryProductImage entTblFactoryProductImage = new EntTblFactoryProductImage();
		if (listProductIdImageToInsertOrUpdate.size() > 0) {
			//create entity to insert or update
			entTblFactoryProductImage.setProductImageDetailPath(entReplaceElement.getImageDetailPath());
			entTblFactoryProductImage.setProductImageThumbnailPath(entReplaceElement.getImageThumbPath());
			entTblFactoryProductImage.setImageSort(Integer.parseInt(entReplaceElement.getField()
										.substring(Constant.IMAGE_CHAR_HEAD, entReplaceElement.getField().length())));
			entTblFactoryProductImage.setReferMode(entReplaceElement.getImageMode());
			entTblFactoryProductImage.setUpdatetedUserId(updateUserId);

			//set mode replace for entity query
			String changeType = entReplaceElement.getChangeType();
			if (changeType.equals(Constant.STR_REPLACE_ALL)) {
				entTblFactoryProductImage.setReplaceMode(Constant.REPLACE_ALL);
			} else if (changeType.equals(Constant.STR_REPLACE_HEAD)) {
				entTblFactoryProductImage.setReplaceMode(Constant.REPLACE_HEAD);
			} else if (changeType.equals(Constant.STR_REPLACE_TAIL)) {
				entTblFactoryProductImage.setReplaceMode(Constant.REPLACE_TAIL);
			} else if (changeType.equals(Constant.STR_FIND_REPLACE)) {
				entTblFactoryProductImage.setReplaceMode(Constant.FIND_REPLACE);
				entTblFactoryProductImage.setFindThumb(entReplaceElement.getFindThumb());
				entTblFactoryProductImage.setFindDetail(entReplaceElement.getFindThumb());
			}
			//List Long productID to insert
			List<Long> listLongImageInsert = new ArrayList<Long>();
			//List Long productId to update
			List<Long> listLongImageUpdate = new ArrayList<Long>();

			//Get list image need update in db by listProductIdImageToInsertOrUpdate
			//List Ent Image need insert or update in db
            List<EntTblFactoryProductImage> listImageUpdate = tblFactoryProductImageMapper.selectProductImageByListProductIdAndImageSort(
					listProductIdImageToInsertOrUpdate, entTblFactoryProductImage);
			//
			boolean flg = false;
			//loop List Image need to insert or update
			for (int i = 0; i < listProductIdImageToInsertOrUpdate.size(); i++) {
				flg = false;
				//Loop List image get from BD
				for (int j = 0; j < listImageUpdate.size(); j++) {
					//If list image get from DB have value in list image need to insert of update
					if (listProductIdImageToInsertOrUpdate.get(i).equals(listImageUpdate.get(j).getProductId())) {
						flg = true;
						listLongImageUpdate.add(listProductIdImageToInsertOrUpdate.get(i));
						break;
					}
				}
				//If iamge not exits in db, then add to list update
				if (!flg) {
					listLongImageInsert.add(listProductIdImageToInsertOrUpdate.get(i));
				}
			}

			//insert image
			if (listLongImageInsert.size() > 0) {
				// k is number of row effect
				Integer k = tblFactoryProductImageMapper.insertProductImageReplaceAll(listLongImageInsert, entTblFactoryProductImage);
				//If list iamge >0 and row effect is 0, return false
				if (k == 0) {
					res = false;
				}
			}

			//update image
			if (listLongImageUpdate.size() > 0) {
				//l is number of row effect
				Integer l = tblFactoryProductImageMapper.updateProductImageReplaceAll(listLongImageUpdate, entTblFactoryProductImage);
				//If list iamge >0 and row effect is 0, return false
				if (l == 0) {
					res = false;
				}
			}

		}
		return res;
	}

	/************************************************************************
	 * <b>Description:</b><br>
	 *  process Save Product General.
	 *
	 * @author		Tran.Thanh
	 * @date		Apr 22, 2014
	 * @param 		entTblFactoryProductGeneral EntTblFactoryProductGeneral for query
	 * @param 		listProductId list productId
	 * @return		true if had row effect
	 * @throws 		SQLException		boolean
	 ************************************************************************/
	private boolean processSaveProductGeneral(EntTblFactoryProductGeneral entTblFactoryProductGeneral
											, List<Long> listProductId) throws SQLException {
		int res = tblFactoryProductGeneralMapper.updateProductGeneralPopupBlock15(listProductId, entTblFactoryProductGeneral);
		if (res > 0) {
			return true;
		}
		return false;
	}

	/************************************************************************
	 * <b>Description:</b><br>
	 *  process Save Product Condition.
	 *
	 * @author		Tran.Thanh
	 * @date		Apr 22, 2014
	 * @param 		entTblFactoryProductGeneral EntTblFactoryProductGeneral for query.
	 * @param 		listProductId list productId
	 * @return		true if had row effect
	 * @throws 		SQLException		boolean
	 ************************************************************************/
	private boolean processSaveProductCondition(EntTblFactoryProductGeneral entTblFactoryProductGeneral
			, List<Long> listProductId) throws SQLException {
		int res = tblFactoryProductConditionMapper.updateFactoryListProductCondition(listProductId, entTblFactoryProductGeneral);
		if (res > 0) {
			return true;
		}
		return false;
	}

	/************************************************************************
	 * <b>Description:</b><br>
	 *  process Save Product Description.
	 *
	 * @author		Tran.Thanh
	 * @date		Apr 23, 2014
	 * @param 		entTblFactoryProductDescription EntTblFactoryProductDescription for query
	 * @param 		listProductId list productId
	 * @return		true if had row effect
	 * @throws 		SQLException		boolean
	 ************************************************************************/
	private boolean processSaveProductDescription(EntTblFactoryProductDescription entTblFactoryProductDescription
			, List<Long> listProductId) throws SQLException {
		int res = tblFactoryProductDescriptionMapper.updateProductDescriptionByModeReplace(listProductId, entTblFactoryProductDescription);
		if (res > 0) {
			return true;
		}
		return false;
	}

	/************************************************************************
	 * <b>Description:</b><br>
	 *  process Save Product Supplier Price.
	 *
	 * @author		Tran.Thanh
	 * @date		Apr 23, 2014
	 * @param 		entTblFactoryProductSupplierPrice EntTblFactoryProductSupplierPrice for query.
	 * @param 		listProductId list productId
	 * @return		true if had row effect
	 * @throws 		SQLException		boolean
	 ************************************************************************/
	private boolean processSaveProductSupplierPrice(EntTblFactoryProductSupplierPrice entTblFactoryProductSupplierPrice
			, List<Long> listProductId) throws SQLException {
		int res = tblFactoryProductSupplierPriceMapper.updateProductSupplierPricePrice(listProductId, entTblFactoryProductSupplierPrice);
		if (res > 0) {
			return true;
		}
		return false;
	}

	/************************************************************************
	 * <b>Description:</b><br>
	 *  process Save Error Flg Product.
	 *
	 * @author		Tran.Thanh
	 * @date		Apr 24, 2014
	 * @param 		mapProduct mapProduct
	 * @param 		listProductId list productId
	 * @param 		updateUserId updateUserId
	 * @return		true if save success
	 * @throws 		SQLException		boolean
	 ************************************************************************/
	private boolean processSaveErrorFlgProduct(Map<Integer, Object> mapProduct
											, List<Long> listProductId
											, String updateUserId) throws SQLException {
		for (Long productId : listProductId) {
			//Get errorFlag on server of product by productId
			EntTblFactoryProductNew productNeedToValidate = tblFactoryProductMapper.selectProductUpdatedOnByProductCode(productId);
			productNeedToValidate.setUpdatedUserId(updateUserId);
			//Get productName on server of product by productId
			EntTblFactoryProductGeneral entGeneral = tblFactoryProductGeneralMapper.selectProductFactoryProductGeneralByProductid(productId);
			productNeedToValidate.setProductName(entGeneral.getProductName());
			productNeedToValidate.setProductCode(entGeneral.getProductCode());

			//Set productEanCode from DB to productNeedToValidate
			productNeedToValidate.setProductEanCode(entGeneral.getProductEanCode());

			//init error count matter by productId
			EntMstFactoryMatterNew entMatterNew = this.initEntMstFactoryMatterNewToCountError(productNeedToValidate);
			entMatterNew.setUpdatedUserId(updateUserId);

			//Set product General get from mapProduct to productNeedToValidate
			initProductGeneralToValidate(productNeedToValidate, mapProduct);

			//Set product Description to mapProduct for Validate
			initProductDescriptionToValide(productNeedToValidate, mapProduct);

			//Set productVideo to Map
			int level = Constant.PRODUCT_EDIT_VIDEO_BLOCK;
			List<EntTblFactoryProductVideo> entTblFactoryProductVideoList =
							tblFactoryProductVideoMapper.selectVideoByProductId(productId);
	        mapProduct.put(level, entTblFactoryProductVideoList);
			//Set product guest to Map
	        int levelGuest = Constant.PRODUCT_EDIT_GUEST_BLOCK;
	        List<EntTblFactoryProductGuestInput> entTblFactoryProductGuestInputList =
	        				tblFactoryProductGuestInputMapper.selectListProductGuestInputByProductId(productId);
	        mapProduct.put(levelGuest, entTblFactoryProductGuestInputList);
			//Set product supplier to Map
	        List<EntTblFactoryProductSupplier> listFactoryProductSupplier =
	        				tblFactoryProductSupplierMapper.selectListSupplierByProductId(productId);
	        mapProduct.put(Constant.PRODUCT_EDIT_SUPPLIER_STATUS_BLOCK, listFactoryProductSupplier);

			//Check error flag of product
			checkErrorFlagOfProductPopupBlock15(productNeedToValidate, entMatterNew, mapProduct);
			//save product error count to DB
	    	tblFactoryProductMapper.updateAllErrorFlgOfProduct(productNeedToValidate);
	    	//Save matter error count to DB
	    	mstFactoryMatterMapper.updateMatterErrorFlgByMatterCode(entMatterNew);
		}
		return true;
	}


	/************************************************************************
	 * <b>Description:</b><br>
	 *  init ProductGeneral To Validate.
	 *
	 * @author		Tran.Thanh
	 * @date		Apr 24, 2014
	 * @param 		productNeedToValidate EntTblFactoryProductNew
	 * @param 		mapProduct mapProduct
	 * @throws 		SQLException SQLException
	 ************************************************************************/
	private void initProductGeneralToValidate(EntTblFactoryProductNew productNeedToValidate
											, Map<Integer, Object> mapProduct) throws SQLException {
		EntTblFactoryProductGeneral entTblFactoryProductGeneral = (EntTblFactoryProductGeneral) mapProduct.get(1);
		//entTblFactoryProductGeneral.ProductBrandCode is BrandCode on Popup
		if (entTblFactoryProductGeneral.getProductBrandCode() != null) {
			productNeedToValidate.setProductBrandCode(entTblFactoryProductGeneral.getProductBrandCode().toString());
		}

		//entTblFactoryProductGeneral.ProductCategoryCode is Category on Popup
		if (entTblFactoryProductGeneral.getProductCategoryCode() != null) {
			productNeedToValidate.setCategoryCode(entTblFactoryProductGeneral.getProductCategoryCode().toString());
		}

		//entTblFactoryProductGeneral.ProductName is ProductName on Popup
//		if (StringUtils.isNotEmpty(entTblFactoryProductGeneral.getProductName())) {
//			if (entTblFactoryProductGeneral.getReplaceModeProductName().equals(Constant.REPLACE_ALL)) {
//				productNeedToValidate.setProductName(entTblFactoryProductGeneral.getProductName());
//			} else if (entTblFactoryProductGeneral.getReplaceModeProductName().equals(Constant.REPLACE_HEAD)) {
//				productNeedToValidate.setProductName(
//						entTblFactoryProductGeneral.getProductName().concat(productNeedToValidate.getProductName()));
//			} else if (entTblFactoryProductGeneral.getReplaceModeProductName().equals(Constant.REPLACE_TAIL)) {
//				productNeedToValidate.setProductName(
//						productNeedToValidate.getProductName().concat(entTblFactoryProductGeneral.getProductName()));
//			} else if (entTblFactoryProductGeneral.getReplaceModeProductName().equals(Constant.FIND_REPLACE)) {
//				productNeedToValidate.setProductName(
//					productNeedToValidate.getProductName().replace(
//						productNeedToValidate.getProductName(), entTblFactoryProductGeneral.getProductName()));
//			}
//		}

		//entTblFactoryProductGeneral.ProductCode is ProductCode on Popup
//		if (StringUtils.isNotEmpty(entTblFactoryProductGeneral.getProductCode())) {
//			//productNeedToValidate.setProductCode(entTblFactoryProductGeneral.getProductCode());
//			if (entTblFactoryProductGeneral.getProductWebikeCodeFlg().equals(0)) {
//				if (entTblFactoryProductGeneral.getReplaceModeProductCode().equals(Constant.REPLACE_ALL)) {
//					productNeedToValidate.setProductCode(entTblFactoryProductGeneral.getProductEanCode());
//				} else if (entTblFactoryProductGeneral.getReplaceModeProductCode().equals(Constant.REPLACE_HEAD)) {
//					productNeedToValidate.setProductCode(
//							entTblFactoryProductGeneral.getProductCode().concat(productNeedToValidate.getProductCode()));
//				} else if (entTblFactoryProductGeneral.getReplaceModeProductCode().equals(Constant.REPLACE_TAIL)) {
//					productNeedToValidate.setProductCode(
//							productNeedToValidate.getProductCode().concat(entTblFactoryProductGeneral.getProductCode()));
//				} else if (entTblFactoryProductGeneral.getReplaceModeProductCode().equals(Constant.FIND_REPLACE)) {
//					productNeedToValidate.setProductCode(
//						productNeedToValidate.getProductCode().replace(
//							entTblFactoryProductGeneral.getProductCodeFind(), entTblFactoryProductGeneral.getProductCode()));
//				}
//			}
//		}

		//entTblFactoryProductGeneral.ProductEanCode is ProductEanCode on Popup
		if (StringUtils.isNotEmpty(entTblFactoryProductGeneral.getProductEanCode())) {
			if (entTblFactoryProductGeneral.getReplaceModeProductEanCode().equals(Constant.REPLACE_ALL)) {
				productNeedToValidate.setProductEanCode(entTblFactoryProductGeneral.getProductEanCode());
			} else if (entTblFactoryProductGeneral.getReplaceModeProductEanCode().equals(Constant.REPLACE_HEAD)) {
				productNeedToValidate.setProductEanCode(
						entTblFactoryProductGeneral.getProductEanCode().concat(productNeedToValidate.getProductEanCode()));
			} else if (entTblFactoryProductGeneral.getReplaceModeProductEanCode().equals(Constant.REPLACE_TAIL)) {
				productNeedToValidate.setProductEanCode(
						productNeedToValidate.getProductEanCode().concat(entTblFactoryProductGeneral.getProductEanCode()));
			} else if (entTblFactoryProductGeneral.getReplaceModeProductEanCode().equals(Constant.FIND_REPLACE)) {
				productNeedToValidate.setProductEanCode(
					productNeedToValidate.getProductEanCode().replace(
							entTblFactoryProductGeneral.getProductEanCodeFind(), entTblFactoryProductGeneral.getProductEanCode()));
			}
		}

		EntTblFactoryProductGeneral entGenInDB = tblFactoryProductGeneralMapper.
				selectProductFactoryProductGeneralByProductid(productNeedToValidate.getProductId());
		//entTblFactoryProductGeneral.ProductSupplierReleaseDate is SupplierReleaseDate on Popup
		if (StringUtils.isNotEmpty(entTblFactoryProductGeneral.getProductSupplierReleaseDate())) {
			if (entTblFactoryProductGeneral.getReplaceModeProductReleaseDate().equals(Constant.REPLACE_ALL)) {
				productNeedToValidate.setSupplierReleaseDate(entTblFactoryProductGeneral.getProductSupplierReleaseDate());
			} else if (entTblFactoryProductGeneral.getReplaceModeProductReleaseDate().equals(Constant.REPLACE_HEAD)) {
				productNeedToValidate.setSupplierReleaseDate(
						entGenInDB.getProductSupplierReleaseDate().concat(entTblFactoryProductGeneral.getProductSupplierReleaseDate()));
			} else if (entTblFactoryProductGeneral.getReplaceModeProductReleaseDate().equals(Constant.REPLACE_TAIL)) {
				productNeedToValidate.setSupplierReleaseDate(
						entTblFactoryProductGeneral.getProductSupplierReleaseDate().concat(entGenInDB.getProductSupplierReleaseDate()));
			} else if (entTblFactoryProductGeneral.getReplaceModeProductReleaseDate().equals(Constant.FIND_REPLACE)) {
				productNeedToValidate.setSupplierReleaseDate(
						entGenInDB.getProductSupplierReleaseDate().replace(
								entTblFactoryProductGeneral.getProductSupplierReleaseDateFind(),
								entTblFactoryProductGeneral.getProductSupplierReleaseDate()));
			}
		}
	}

	/************************************************************************
	 * <b>Description:</b><br>
	 *  init Product Description To Valide.
	 *
	 * @author		Tran.Thanh
	 * @date		Apr 24, 2014
	 * @param 		productNeedToValidate EntTblFactoryProductNew
	 * @param 		mapProduct		void
	 ************************************************************************/
	private void initProductDescriptionToValide(EntTblFactoryProductNew productNeedToValidate
												, Map<Integer, Object> mapProduct) {
		//Entity Description in Popup
		EntTblFactoryProductDescription entTblFactoryProductDescription =
										(EntTblFactoryProductDescription) mapProduct.get(2);
		//Entity Description in DB
		EntTblFactoryProductDescription descriptionInDB =
				tblFactoryProductDescriptionMapper.selectProductDescriptionByProductId(productNeedToValidate.getProductId());

		if (descriptionInDB == null) {
			descriptionInDB = new EntTblFactoryProductDescription();
			descriptionInDB.setDescriptionSummary("");
		}
		//Merge Description in Popup and Description in DB into entTblFactoryProductDescription
		//DescriptionSummary
		if (StringUtils.isNotEmpty(entTblFactoryProductDescription.getDescriptionSummary())) {
			if (entTblFactoryProductDescription.getReplaceModeDescriptionSummary().equals(Constant.REPLACE_HEAD)) {
				entTblFactoryProductDescription.setDescriptionSummary(
						entTblFactoryProductDescription.getDescriptionSummary().concat(
								descriptionInDB.getDescriptionSummary()));
			}
			if (entTblFactoryProductDescription.getReplaceModeDescriptionSummary().equals(Constant.REPLACE_TAIL)) {
				entTblFactoryProductDescription.setDescriptionSummary(
						descriptionInDB.getDescriptionSummary().concat(
								entTblFactoryProductDescription.getDescriptionSummary()));
			}
			if (entTblFactoryProductDescription.getReplaceModeDescriptionSummary().equals(Constant.FIND_REPLACE)) {
				entTblFactoryProductDescription.setDescriptionSummary(
						descriptionInDB.getDescriptionSummary().replace(
								entTblFactoryProductDescription.getDescriptionSummaryFind()
								, entTblFactoryProductDescription.getDescriptionSummary()));
			}
		}

		//DescriptionCaution
		if (StringUtils.isNotEmpty(entTblFactoryProductDescription.getDescriptionCaution())) {
			if (entTblFactoryProductDescription.getReplaceModeDescriptionCaution().equals(Constant.REPLACE_HEAD)) {
				entTblFactoryProductDescription.setDescriptionCaution(
						entTblFactoryProductDescription.getDescriptionCaution().concat(
								descriptionInDB.getDescriptionCaution()));
			}
			if (entTblFactoryProductDescription.getReplaceModeDescriptionCaution().equals(Constant.REPLACE_TAIL)) {
				entTblFactoryProductDescription.setDescriptionCaution(
						descriptionInDB.getDescriptionCaution().concat(
								entTblFactoryProductDescription.getDescriptionCaution()));
			}
			if (entTblFactoryProductDescription.getReplaceModeDescriptionCaution().equals(Constant.FIND_REPLACE)) {
				entTblFactoryProductDescription.setDescriptionCaution(
						descriptionInDB.getDescriptionCaution().replace(
								entTblFactoryProductDescription.getDescriptionCautionFind()
								, entTblFactoryProductDescription.getDescriptionCaution()));
			}
		}

		//DescriptionRemask
		if (StringUtils.isNotEmpty(entTblFactoryProductDescription.getDescriptionRemarks())) {
			if (entTblFactoryProductDescription.getReplaceModeDescriptionRemarks().equals(Constant.REPLACE_HEAD)) {
				entTblFactoryProductDescription.setDescriptionRemarks(
						entTblFactoryProductDescription.getDescriptionRemarks().concat(
								descriptionInDB.getDescriptionRemarks()));
			}
			if (entTblFactoryProductDescription.getReplaceModeDescriptionRemarks().equals(Constant.REPLACE_TAIL)) {
				entTblFactoryProductDescription.setDescriptionRemarks(
						descriptionInDB.getDescriptionRemarks().concat(
								entTblFactoryProductDescription.getDescriptionRemarks()));
			}
			if (entTblFactoryProductDescription.getReplaceModeDescriptionRemarks().equals(Constant.FIND_REPLACE)) {
				entTblFactoryProductDescription.setDescriptionRemarks(
						descriptionInDB.getDescriptionRemarks().replace(
								entTblFactoryProductDescription.getDescriptionRemarksFind()
								, entTblFactoryProductDescription.getDescriptionRemarks()));
			}
		}
		mapProduct.put(2, entTblFactoryProductDescription);
	}

	@Override
	public Integer countNumberReleasseProduct(EntTblFactoryProductNew productSearch) {
		Integer numberReleaseProduct = tblFactoryProductMapper.countReleaseProductWithFilter(productSearch);
		if (numberReleaseProduct == null) {
			//Cannot count product
			numberReleaseProduct = -1;
		}
		return numberReleaseProduct;
	}

	@Override
	public List<Long> selectAllProductIdNotReleaseWithFilter(EntTblFactoryProductNew productSearch) {
		List<Long> lstProductId = tblFactoryProductMapper.selectAllProductNotReleasedWithFilter(productSearch);
		if (lstProductId == null) {
			//Cannot get list productID
			lstProductId = new ArrayList<Long>();
		}
		return lstProductId;
	}

	@Override
	@Transactional
	public boolean deleteAllProductNotReleasedWithFilter(List<Long> lstProductId
														, EntMstFactoryMatterNew entMatter
														, List<Long> lstSyouhinSysCode
														, String loginId) {
		TransactionStatus status = null;
		Boolean isContinue = true;
		Boolean result = false;
		try {
			// start transaction.
            status =  this.startTransaction();
            //Delete list product
            tblFactoryProductMapper.deleteProductList(lstProductId);
            //Update error in matter
            mstFactoryMatterMapper.updateMatterErrorFlgByMatterCode(entMatter);
            //Update flag maintenance in mst_product to 0
            //Remove product has syouhinSysCode = 0
            lstSyouhinSysCode = removeSyouhinSysCodeEqual0(lstSyouhinSysCode);
            if (lstSyouhinSysCode.size() > 0) {
            	mstProductMapper.updateProductMaintenanceFlgByListSyouhinSysCode(0, loginId, lstSyouhinSysCode);
            }
		} catch (Exception e) {
			//Error when update
        	isContinue = false;
            this.rollback(status);
        } finally {
            if (isContinue) {
                this.commit(status);
                //delete success
                result = true;
            } else {
                this.rollback(status);
            }
        }
		return result;
	}

	/************************************************************************
	 * <b>Description:</b><br>
	 *  Remove syouhin sys code = 0 in list syouhinSysCode.
	 *
	 * @author		Luong.Dai
	 * @date		Jun 24, 2014
	 * @param 		lstSyouhinSysCode		List to remove
	 * @return		List<Long>				List result, not exist 0
	 ************************************************************************/
	private List<Long> removeSyouhinSysCodeEqual0(List<Long> lstSyouhinSysCode) {
		for (int i = lstSyouhinSysCode.size() - 1; i >= 0; i--) {
			if (lstSyouhinSysCode.get(i).equals(0L)) {
				//Remove sysouhin sys code equals 0
				lstSyouhinSysCode.remove(i);
			}
		}
		return lstSyouhinSysCode;
	}
	@Override
	public EntMstFactoryMatterNew countErrorOfListProduct(List<Long> lstProductId
														, Long matterNo
														, String loginId) {
		//Count error of list product delete
		EntTblFactoryProductNew entError = tblFactoryProductMapper.selectTotalErrorFlgProductByLstProductId(lstProductId);

		//Set value to matter
        EntMstFactoryMatterNew mstMatter = new EntMstFactoryMatterNew();
        if (entError != null) {
        	//Set value
        	mstMatter.setMatterFactoryErrorCountGeneral(-entError.getProductGeneralErrorFlg());
            mstMatter.setMatterFactoryErrorCountCategory(-entError.getProductCategoryErrorFlg());
            mstMatter.setMatterFactoryErrorCountModel(-entError.getProductModelErrorFlg());
            mstMatter.setMatterFactoryErrorCountAttribute(-entError.getProductAttributeErrorFlg());
            mstMatter.setMatterNo(matterNo);
            mstMatter.setUpdatedUserId(loginId);
        }
        return mstMatter;
	}

	@Override
	public List<Long> selectListSyouhinSysCode(List<Long> lstProductId) throws SQLException {
		List<Long> lstSyouhinSysCode = tblFactoryProductMapper.selectProductSyouhinSysCodeFromProductIdList(lstProductId);

		//Check null data
		if (null == lstSyouhinSysCode) {
			lstSyouhinSysCode = new ArrayList<Long>();
		}

		return lstSyouhinSysCode;
	}
	/* EOE Block15 */

}
