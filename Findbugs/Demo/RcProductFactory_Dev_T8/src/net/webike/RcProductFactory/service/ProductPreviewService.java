/************************************************************************
 * File Name	： ProductPreview.java
 * Author		： Nguyen.Chuong
 * Version		： 1.0.0
 * Date Created	： 2014/06/11
 * Description	： Service to process logic flow of productPreview page.
 *
 ***********************************************************************/
package net.webike.RcProductFactory.service;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import net.webike.RcProductFactory.entity.EntTblFactoryCalibrationRequestIndividual;
import net.webike.RcProductFactory.entity.EntTblFactoryProductDescription;
import net.webike.RcProductFactory.entity.EntTblFactoryProductFitModel;
import net.webike.RcProductFactory.entity.EntTblFactoryProductGeneral;
import net.webike.RcProductFactory.entity.EntTblFactoryProductGuestInput;
import net.webike.RcProductFactory.entity.EntTblFactoryProductLink;
import net.webike.RcProductFactory.entity.EntTblFactoryProductNew;
import net.webike.RcProductFactory.entity.EntTblFactoryProductSelect;
import net.webike.RcProductFactory.entity.EntTblFactoryProductSupplier;

/**
 * Service to process logic flow of productPreview page.
 */
public interface ProductPreviewService {

	/* BOE Function Tran.Thanh */
	/* Start Block 2*/
	/************************************************************************
	 * <b>Description:</b><br>
	 *  select matterNo by productId.
	 *
	 * @author		Tran.Thanh
	 * @date		Jun 13, 2014
	 * @param 		productId productId
	 * @return		Long
	 ************************************************************************/
	Long selectMatterNoByProductId(Long productId);

	/************************************************************************
	 * <b>Description:</b><br>
	 *  Count total product in matter.
	 *
	 * @author		Tran.Thanh
	 * @date		Jun 13, 2014
	 * @param 		matterNo matter number
	 * @return		int total productId
	 ************************************************************************/
	int countTotalProductInMatter(Long matterNo);

	/************************************************************************
	 * <b>Description:</b><br>
	 *  get current index of product in matter.
	 *
	 * @author		Tran.Thanh
	 * @date		Jun 13, 2014
	 * @param 		productId product id
	 * @param       matterNo Long
	 * @return		int index of product in matter
	 ************************************************************************/
	int indexProductInMatter(Long productId, Long matterNo);

	/************************************************************************
	 * <b>Description:</b><br>
	 *  Select list groupCode, brandCode in matter.
	 *
	 * @author		Tran.Thanh
	 * @date		Jun 13, 2014
	 * @param 		matterNo Long
	 * @return		List<EntTblFactoryProductGeneral> list group in matter.
	 ************************************************************************/
	List<EntTblFactoryProductGeneral> selectListGroupByMatterNo(Long matterNo);

	/************************************************************************
	 * <b>Description:</b><br>
	 *  index of product in group code.
	 *
	 * @author		Tran.Thanh
	 * @date		Jun 13, 2014
	 * @param 		productId product id
	 * @param 		listGroupInMatter List<EntTblFactoryProductGeneral> list all group in matter
	 * @return		index of group in matter
	 * @throws      SQLException excetion when query db
	 ************************************************************************/
	int indexGroupInMatter(Long productId, List<EntTblFactoryProductGeneral> listGroupInMatter) throws SQLException;

	/************************************************************************
	 * <b>Description:</b><br>
	 *  Select product general by product id.
	 *
	 * @author		Tran.Thanh
	 * @date		Jun 13, 2014
	 * @param 		productId product id
	 * @return		entProductGeneral
	 * @throws 		SQLException		EntTblFactoryProductGeneral
	 ************************************************************************/
	EntTblFactoryProductGeneral selectProductGeneralByProductId(Long productId) throws SQLException;
	/* End Block 2*/

	/* Start Block 5 */
	/************************************************************************
	 * <b>Description:</b><br>
	 *  select list supplier by product id.
	 *
	 * @author		Tran.Thanh
	 * @date		Jun 16, 2014
	 * @param 		productId productId
	 * @return		List siire
	 * @throws 		SQLException		List<EntTblFactoryProductSupplier>
	 ************************************************************************/
	List<EntTblFactoryProductSupplier> selectListSupplierByProductId(Long productId) throws SQLException;
	/* End Block 5 */
	/* Start Block 10 */
	/************************************************************************
	 * <b>Description:</b><br>
	 *  select Product description by product id.
	 *
	 * @author		Tran.Thanh
	 * @date		Jun 16, 2014
	 * @param 		productId product Id
	 * @return		EntTblFactoryProductDescription
	 * @throws 		SQLException		EntTblFactoryProductDescription
	 ************************************************************************/
	EntTblFactoryProductDescription selectProductDescriptionByProductId(Long productId) throws SQLException;
	/* End Block 10 */
	/* EOE Function Tran.Thanh */

	/* BOE Function Dai.Luong */
	/************************************************************************
	 * <b>Description:</b><br>
	 *  Get list guest input of product by product id.
	 *
	 * @author		Luong.Dai
	 * @date		Jun 13, 2014
	 * @param 		productId								ProductId to get data
	 * @return		List<EntTblFactoryProductGuestInput>	result
	 ************************************************************************/
	List<EntTblFactoryProductGuestInput> selectListGuestInputOfProduct(Long productId);

	/************************************************************************
	 * <b>Description:</b><br>
	 *  select list link of product by productId.
	 *
	 * @author		Luong.Dai
	 * @date		Jun 13, 2014
	 * @param 		productId						Id of product
	 * @return		List<EntTblFactoryProductLink>	result
	 ************************************************************************/
	List<EntTblFactoryProductLink> selectLinkOfProduct(Long productId);

	/************************************************************************
	 * <b>Description:</b><br>
	 *  select list fitModel of product by productID.
	 *
	 * @author		Luong.Dai
	 * @date		Jun 13, 2014
	 * @param       productId Long
	 * @return		List<EntTblFactoryProductFitModel>
	 ************************************************************************/
	List<EntTblFactoryProductFitModel> selectListFitModelOfProduct(Long productId);

	/************************************************************************
	 * <b>Description:</b><br>
	 * Select list option of product by productId .
	 *
	 * @author		Luong.Dai
	 * @date		Jun 14, 2014
	 * @param 		productId							Id of product
	 * @return		List<EntTblFactoryProductSelect>	result
	 ************************************************************************/
	List<EntTblFactoryProductSelect> selectListSelectCodeByProductId(Long productId);

	/************************************************************************
	 * <b>Description:</b><br>
	 *  Select list productId of same group.
	 *
	 * @author		Luong.Dai
	 * @date		Jun 14, 2014
	 * @param 		groupCode		group code of product
	 * @param 		brandCode		brand code
	 * @param 		matterNo		matterNo
	 * @return		List<Long>
	 ************************************************************************/
	List<Long> selectListProductIdSameGroup(Long groupCode, Integer brandCode, Long matterNo);

	/************************************************************************
	 * <b>Description:</b><br>
	 *  Select list selet display of group producr.
	 *
	 * @author		Luong.Dai
	 * @date		Jun 14, 2014
	 * @param 		lstSelect		List select code
	 * @param 		lstProductId	List product id
	 * @return		Map<Integer, Object>
	 ************************************************************************/
	Map<Integer, Object> selectListSelectDisplayValue(List<EntTblFactoryProductSelect> lstSelect, List<Long> lstProductId);

	/************************************************************************
	 * <b>Description:</b><br>
	 *  Find product has list select display match with list select display choise by user.
	 *
	 * @author		Luong.Dai
	 * @date		Jun 16, 2014
	 * @param 		lstSelect		List display choise by user
	 * @param 		lstProductId	List product id
	 * @return		Long			Product id: null if not found
	 ************************************************************************/
	Long findProductByListSelectedOption(List<EntTblFactoryProductSelect> lstSelect, List<Long> lstProductId);

	/************************************************************************
	 * <b>Description:</b><br>
	 *  Select proper .
	 *
	 * @author		Luong.Dai
	 * @date		Jun 16, 2014
	 * @param 		productId			Product id
	 * @return		EntTblFactoryProductGeneral
	 ************************************************************************/
	EntTblFactoryProductGeneral selectProductPriceByProductId(String productId);
	/* EOE Function Dai.Luong */

	/* BOE Function Nguyen.Chuong */
	/************************************************************************
	 * <b>Description:</b><br>
	 *  #7203 select one next/previous productId base on matter, groupCode and changeMode selected by user.<br>
	 *  - If select productId have groupCode > 0 then param changeModeInGroup = true<br>
     *  - If select productId have groupCode = 0 then param changeModeInGroup = false<br>
     *  - If load next product => order by ASC<br>
     *  - If load previous product => order by DESC<br>
	 *
	 * @author		Nguyen.Chuong
	 * @date		Jun 13, 2014
	 * @param       productId Long
	 * @param       matterNo Long
	 * @param       changeMode String
	 * @param       orderBy String
	 * @return		Long
	 ************************************************************************/
	Long getNextOrPreviousProductId(Long productId, Long matterNo, String changeMode, String orderBy);
	/* EOE Function Nguyen.Chuong */
    /* BOE Function @rcv!dau.phuong */
    /************************************************************************
     * <b>Description:</b><br>
     *  select calibration type from mst_factory_calibration_type.
     *
     * @author      Dau.Phuong
     * @date        July 16, 2014
     * @return      List<EntTblFactoryCalibrationRequestIndividual>
     ************************************************************************/
    List<EntTblFactoryCalibrationRequestIndividual> selectCalibrationType();

	/************************************************************************
     * <b>Description:</b><br>
     * Select list calibration request individual from rc_product_factory.tbl_factory_calibration_request_individual.
     *
     * @author      Dau.Phuong
     * @date        July 16, 2014
     * @param       ent            EntTblFactoryCalibrationRequestIndividual
     * @param       productId      product id
     * @return      List<EntTblFactoryCalibrationRequestIndividual>
     ************************************************************************/
    List<EntTblFactoryCalibrationRequestIndividual> selectCalibrationRequestIndividual(EntTblFactoryCalibrationRequestIndividual ent, Long productId);

    /************************************************************************
     * <b>Description:</b><br>
     *  select status check when preview product.
     *
     * @author      Dau.Phuong
     * @date        July 17, 2014
     * @param       productId           Product id
     * @return      EntTblFactoryProductNew
     ************************************************************************/
    EntTblFactoryProductNew selectStatusCheckPreviewProduct(Long productId);

    /************************************************************************
     * <b>Description:</b><br>
     *  update status check when preview product.
     *
     * @author      Dau.Phuong
     * @date        July 17, 2014
     * @param       param           EntTblFactoryProductNew
     ************************************************************************/
    void updateStatusPopUpCheck(EntTblFactoryProductNew param);
    /************************************************************************
     * <b>Description:</b><br>
     *  update status check when preview product.
     *
     * @author      Dau.Phuong
     * @date        July 18, 2014
     * @param       param           EntTblFactoryCalibrationRequestIndividual
     ************************************************************************/
    void changeCorrespondRequest(EntTblFactoryCalibrationRequestIndividual param);
    /************************************************************************
     * <b>Description:</b><br>
     *  update content of request.
     *
     * @author      Dau.Phuong
     * @date        July 21, 2014
     * @param       param           EntTblFactoryCalibrationRequestIndividual
     ************************************************************************/
    void updateContentRequest(EntTblFactoryCalibrationRequestIndividual param);
    /* EOE Function @rcv!dau.phuong */
    
    // BOE @rcv!Luong.Tuong 2014/07/15 #9955 :  count total product in matter using for validate duplicate
    /************************************************************************
    *  count total product in matter using for validate duplicate  .
     *
     * @author      Luong.Luong
     * @date        Jul 15, 2014
     * @param       matterNo matter number
     * @param       productCode  String
     * @param       productEanCode  String
     * @return      int total product in matter using for validate duplicate 
     ************************************************************************/
    int countCheckInvalidDataInMatter(Long matterNo, String productCode, String productEanCode);
	/************************************************************************
     * <b>Description:</b><br>
     * delete  calibration request individual
     *
     * @author      Luong.Tuong
     * @date        July 21, 2014
     * @param       ent EntTblFactoryCalibrationRequestIndividual
     * @return      boolean
     * @throws      SQLException        SQLException
     ************************************************************************/
    boolean deleteCalibrationRequestIndividual(EntTblFactoryCalibrationRequestIndividual ent) throws SQLException;

 // EOE @rcv!Luong.Tuong 2014/07/15 #9955 :  count total product in matter using for validate duplicate
    /************************************************************************
     * <b>Description:</b><br>
     *  count number product check 1, check 2 and total product of matter.
     *
     * @author		Luong.Dai
     * @date		Jul 30, 2014
     * @param 		matterNo			MatterNo
     * @return		EntTblFactoryProductNew		Entity result
     * @throws      SQLException        SQLException
     ************************************************************************/
    EntTblFactoryProductNew countTotalProductCheckInMatter(Long matterNo) throws SQLException;
}
