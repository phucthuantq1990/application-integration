/************************************************************************
 * File Name	： Bunruikaisou.java
 * Author		： Tran.Thanh
 * Version		： 1.0.0
 * Date Created	： 2013/10/23
 * Date Updated	： 2013/10/23
 * Description	： Service to get Bunruikaisou from database.
 *
 ***********************************************************************/
package net.webike.RcProductFactory.service;

import java.util.List;

import net.webike.RcProductFactory.entity.EntCategory;
import net.webike.RcProductFactory.entity.EntMstBunrui;
import net.webike.RcProductFactory.entity.EntMstBunruikaisou;


/**
 * Service to get bunrui from database.
 */
public interface BunruiService {

    /************************************************************************
     * <b>Description:</b><br>
     *  select List Bunrui By OyaCode.
     *
     * @author		Tran.Thanh
     * @date		23 Oct 2013
     * @param 		entBunruikaisou entity Bunruikaisou.
     * @return		List<EntMstBunruikaisou>
     ************************************************************************/
    List<EntMstBunrui> selectListBunruiByOyaCode(EntMstBunruikaisou entBunruikaisou);

    /************************************************************************
     * <b>Description:</b><br>
     *  Select Category TreeView.
     *
     * @author		Tran.Thanh
     * @date		25 Oct 2013
     * @return		EntCategory
     ************************************************************************/
    List<EntCategory> selectCategory();
}
