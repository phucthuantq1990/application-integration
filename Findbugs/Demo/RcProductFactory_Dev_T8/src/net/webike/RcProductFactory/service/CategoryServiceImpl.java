/************************************************************************
 * File Name    ： CategoryServiceImpl.java
 * Author       ： hoang.ho
 * Version      ： 1.0.0
 * Date Created ： 2014/01/08
 * Date Updated ： 2014/01/08
 * Description  ： Service to process with category object.
 ***********************************************************************/
package net.webike.RcProductFactory.service;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.webike.RcProductFactory.action.form.CategoryEditActionForm;
import net.webike.RcProductFactory.entity.EntMstAttribute;
import net.webike.RcProductFactory.entity.EntMstBunrui;
import net.webike.RcProductFactory.entity.EntMstCategory;
import net.webike.RcProductFactory.entity.EntTblCategoryAttribute;
import net.webike.RcProductFactory.entity.EnumAttributeType;
import net.webike.RcProductFactory.mapper.MstAttributeMapper;
import net.webike.RcProductFactory.mapper.MstCategoryMapper;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author hoang.ho
 *
 */
@Service("categoryServiceImpl")
public class CategoryServiceImpl implements CategoryService {

    @Autowired
    private MstCategoryMapper mstCategoryMapper;
    @Override
    public List<EntMstCategory> selectCategoryManageList(EntMstCategory searchConditions) {
        return mstCategoryMapper.selectCategoryManageList(searchConditions);
    }

    @Override
    public int selectCategoryManageListCount(EntMstCategory searchConditions) {
        return mstCategoryMapper.selectCategoryManageListCount(searchConditions);
    }

    @Override
    public List<EntMstAttribute> selectAttributeList() {
        return mstCategoryMapper.selectAttributeList();
    }

    @Autowired
    private MstAttributeMapper attributeMapper;

    /************************************************************************
     * <b>Description:</b><br>
     * get bunrui by bunrui code.
     *
     * @author      Long Vu
     * @date        Jan 10, 2014
     * @param bunruiCode bunrui code
     * @return      List<EntMstBunrui>
     ************************************************************************/
    @Override
    public EntMstBunrui getBunrui(String bunruiCode) {
        EntMstBunrui resutl = null;
        try {
            resutl = mstCategoryMapper.getBunrui(bunruiCode);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return resutl;
    }

    /************************************************************************
     * <b>Description:</b><br>
     * get userName by userId.
     *
     * @author      thai son
     * @date        Jan 10, 2014
     * @param userId String
     * @return      String
     ************************************************************************/
    @Override
    public String getUserNameByUserIdInMstFactoryUser(String userId) {
            return mstCategoryMapper.getUserNameByUserIdInMstFactoryUser(userId);
    }
    /************************************************************************
     * <b>Description:</b><br>
     * get category by code.
     *
     * @author      Long Vu
     * @date        Jan 10, 2014
     * @param categoryCode category code
     * @return      List<EntMstBunrui>
     ************************************************************************/
    @Override
    public EntMstCategory selectCategoryByCode(int categoryCode) {
        EntMstCategory result = null;
        try {
            result = mstCategoryMapper.selectCategoryByCode(categoryCode);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /************************************************************************
     * <b>Description:</b><br>
     * get catgory-attribute list by category code.
     *
     * @author      Long Vu
     * @date        Jan 10, 2014
     * @param categoryCode category cdoe
     * @return      List<EntMstBunrui>
     ************************************************************************/
    @Override
    public List<EntTblCategoryAttribute> selectCatAttributeByCode(int categoryCode) {
        List<EntTblCategoryAttribute> result = null;
        try {
            result = mstCategoryMapper.selectCatAttributeByCode(categoryCode);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /************************************************************************
     * <b>Description:</b><br>
     *  search and get list Attribute by attrName.
     *
     * @author      Luong.Dai
     * @date        Jan 9, 2014
     * @param attrName attribute name
     * @return      List<EntMstAttribute>
     ************************************************************************/
    @Override
    public List<EntMstAttribute> selectListAttribute(String attrName) {
        List<EntMstAttribute> resutl = null;
        try {
            resutl = attributeMapper.selectListAttribute(attrName);
            //use enum attribute type:
            if (resutl != null && resutl.size() > 0) {
                for (EntMstAttribute entMstAttribute : resutl) {
                    if (entMstAttribute != null && entMstAttribute.getAttributeType() != null) {
                        entMstAttribute.setAttributeType(EnumAttributeType.getTypeValueByKey(entMstAttribute.getAttributeType()));
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return resutl;
    }

    /************************************************************************
     * <b>Description:</b><br>
     *  search and get list bunrui by bunruiName.
     *
     * @author      Luong.Dai
     * @date        Jan 8, 2014
     * @param bunruiName bunrui name
     * @return      List<EntMstBunrui>
     ************************************************************************/
    @Override
    public List<EntMstBunrui> getListBunrui(String bunruiName) {
        List<EntMstBunrui> resutl = null;
        try {
            resutl = mstCategoryMapper.getListBunrui(bunruiName);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return resutl;
    }

    /**
     * Get category codes list from tbl_category_attribute byattributeCodes .
     * @param attributeCodes attributeCodes
     * @return List<EntMstCategory>
     */
    @Override
    public String selectCategoryCodes(String attributeCodes) {
        return mstCategoryMapper.selectCategoryCodes(attributeCodes);
    }

    /************************************************************************
     * <b>Description:</b><br>
     *  to process change in category and catgory_attribute in DB (insert, update, delete).
     *
     * @author      Long Vu
     * @date        Jan 10, 2014
     * @param form  insert, update, delete conditon.
     * @return      boolean
     ************************************************************************/
    @Override
    @Transactional
    public boolean processUpdateDataCatAttribute(CategoryEditActionForm form) {
        boolean existedName = true;
        int isContinue = 0;
        Map<String, String> deleteCode;
        try {
            // check if the given category name is exist in DB
            existedName = checkExistCategoryName(form);
            if (!existedName) {
                isContinue = mstCategoryMapper.updateCategoryNameByBunruiCode(form.getCategory());
                if (isContinue > 0) {
                    // delete data
                    if (form.getDeletedCodeList() != null && form.getDeletedCodeList().length() != 0) {
                        deleteCode = new HashMap<String, String>();
                        deleteCode.put("categoryCode", form.getCategory().getCategoryCode());
                        deleteCode.put("attributeCode", form.getDeletedCodeList());
                        mstCategoryMapper.deleteCatAttributeByCode(deleteCode);
                    }
                    // update data
                    if (form.getCatAttributeUpdateList() != null || form.getCatAttributeUpdateList().size() != 0) {
                        for (EntTblCategoryAttribute obj : form.getCatAttributeUpdateList()) {
                            obj.setLoginUserId(form.getCategory().getLoginUserId());
                            isContinue = mstCategoryMapper.updateCatAttribute(obj);
                            if (isContinue <= 0) {
                                break;
                            }
                        }
                    }
                    if (isContinue > 0) {
                        // insert data
                        if (form.getCatAttributeInsertList() != null || form.getCatAttributeInsertList().size() != 0) {
                            for (EntTblCategoryAttribute obj : form.getCatAttributeInsertList()) {
                                obj.setLoginUserId(form.getCategory().getLoginUserId());
                                isContinue = mstCategoryMapper.insertCatAttribute(obj);
                            }
                        }
                    }
                }
                if (isContinue <= 0) {
                    throw new Exception("update");
                }
            } else {
                throw new Exception("update");
            }
        } catch (Exception e) {
           // set msg for showing to view
           if (StringUtils.equals("update", e.getLocalizedMessage())) {
               throw new RuntimeException("update");
           } else {
               throw new RuntimeException("other");
           }
        }
        return true;
    }

    /************************************************************************
     * <b>Description:</b><br>
     *  to process change in category and catgory_attribute in DB (insert, update, delete).
     *
     * @author      Long Vu
     * @date        Jan 10, 2014
     * @param form  insert, update, delete conditon.
     * @return      boolean
     ************************************************************************/
    @Override
    public boolean processInsertDataCatAttribute(CategoryEditActionForm form) {
        Map<String, Integer> map = new HashMap<String, Integer>();
        boolean existedName = true;
        int isContinue = 0;
        String categoryCode;
        try {
            // check if the given category name is exist in DB
            existedName = checkExistCategoryName(form);
            if (!existedName) {
                map.put("returnCategoryCode", 0);
                isContinue = mstCategoryMapper.insertCategoryByCode(form.getCategory(), map);
                if (isContinue > 0) {
                    // get key auto generate
                    categoryCode = String.valueOf(map.get("returnCategoryCode"));
                    // insert data
                    if (form.getCatAttributeInsertList() != null && form.getCatAttributeInsertList().size() != 0) {
                        for (EntTblCategoryAttribute obj : form.getCatAttributeInsertList()) {
                            obj.setLoginUserId(form.getCategory().getLoginUserId());
                            obj.setCategoryCode(categoryCode);
                            obj.setRequiredTranferFlg(Integer.parseInt(obj.getRequiredFlg()));
                            isContinue = mstCategoryMapper.insertCatAttribute(obj);
                            if (isContinue <= 0) {
                                break;
                            }
                        }
                    }
                } else {
                    // insert fail then roll back
                    throw new Exception("insert");
                }
            } else {
                throw new Exception("insert");
            }
        } catch (Exception e) {
            // set msg for showing to view
            if (StringUtils.equals("insert", e.getLocalizedMessage())) {
                throw new RuntimeException("insert");
            } else {
                throw new RuntimeException("other");
            }
        }
        return true;
    }

    /************************************************************************
     * <b>Description:</b><br>
     *  to check that the insert or update name exist in DB or not.
     *
     * @author      Long Vu
     * @date        Jan 10, 2014
     * @param form  condition to check mode and select name.
     * @return      boolean
     ************************************************************************/
    private boolean checkExistCategoryName(CategoryEditActionForm form) {
        boolean result = false;
        List<EntMstCategory> categoryNameList = null;
        // get category name list from DB
        if (form.getInsertMode() == 0) {
            categoryNameList = mstCategoryMapper.selectCategoryNameListWithoutCode(form.getCategory());
        } else {
            categoryNameList = mstCategoryMapper.selectCategoryNameList(form.getCategory().getCategoryName());
        }
        if (categoryNameList != null && categoryNameList.size() != 0) {
            result = true;
        } else {
            result = false;
        }
        return result;
    }

    @Override
    //BOE THAI.SON
    public boolean checkAttrHasNotModified(List<EntTblCategoryAttribute> catAttributeList, String categoryCode, CategoryEditActionForm form) {
    //EOE THAI.SON
        boolean hasFlg = false;

        // check list old condition
        if (catAttributeList == null || catAttributeList.isEmpty()) {
            return true;
        }
        // check category code
        if (!StringUtils.isNumeric(categoryCode)) {
            return false;
        }

        // get all attributes by category code.
        int categoryCodeInt = Integer.parseInt(categoryCode);
        List<EntTblCategoryAttribute> currentAttrList = mstCategoryMapper.selectCatAttributeByCode(categoryCodeInt);
        if (currentAttrList == null) {
            currentAttrList = new ArrayList<EntTblCategoryAttribute>();
        }
      //BOE THAI.SON
        if (currentAttrList.size() > 0 && currentAttrList.get(0) != null) {
            String updatedUserFlgInDBNow = currentAttrList.get(0).getUpdatedUserFlg();
            String updatedOnInDBNow = currentAttrList.get(0).getUpdatedOn();
            EntTblCategoryAttribute catAttributeToCreateMessageUpdated = new EntTblCategoryAttribute();
            catAttributeToCreateMessageUpdated.setUpdatedUserFlg(updatedUserFlgInDBNow);
            catAttributeToCreateMessageUpdated.setUpdatedOn(updatedOnInDBNow);
            form.setCatAttributeToCreateMessageUpdated(catAttributeToCreateMessageUpdated);
        }
        //EOE THAI.SON
        // Return false If size of old and current atttrList not equals.
        if (catAttributeList.size() != currentAttrList.size()) {
            return false;
        }

        // Loop for all attributes old and compare with current atttrList
        for (EntTblCategoryAttribute oldAttr : catAttributeList) {
            hasFlg = false;
            for (EntTblCategoryAttribute currentAttr : currentAttrList) {
                // Check if old code = current code && old update date = current update date
                if (oldAttr.getAttributeCode().equals(currentAttr.getAttributeCode())
                   && oldAttr.getUpdatedOn().equals(currentAttr.getUpdatedOn())) {
                    hasFlg = true;
                    break;
                }
             }
            if (!hasFlg) {
                return false;
            }
        }
        return true;
    }

    @Override
    public boolean checkExistsCategoryByCategoryName(EntMstCategory category) {
        EntMstCategory result = mstCategoryMapper.selectCategoryByName(category);

        return result != null;
    }

    @Override
    public List<EntMstCategory> selectCategoryList() throws SQLException {
        List<EntMstCategory> categoryList = mstCategoryMapper.selectCategoryList();
        if (categoryList != null) {
        	return categoryList;
        }
        return Collections.emptyList();
    }
}
