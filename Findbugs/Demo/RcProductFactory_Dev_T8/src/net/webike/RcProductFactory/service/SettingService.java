/************************************************************************
 * File Name	： SettingService.java
 * Author		： Nguyen.Chuong
 * Version		： 1.0.0
 * Date Created	： 2014/01/13
 * Description	： Service to process setting object with database.
 *
 ***********************************************************************/
package net.webike.RcProductFactory.service;

import java.util.List;

import net.webike.RcProductFactory.entity.EntTblFactorySetting;


/**
 * Service to process setting object with database.
 */
public interface SettingService {

	/************************************************************************
     * <b>Description:</b><br>
     *  Get one ent setting from DB by setting_code.
     *
     * @author      Nguyen.Chuong
     * @date        jan 14, 2014
     * @param       settingCode String
     * @return      String
     ************************************************************************/
	String selectSettingValueBySettingCode(String settingCode);

	/************************************************************************
	 * <b>Description:</b><br>
	 *  Select list factory setting.
	 *
	 * @author		Tran.Thanh
	 * @date		13 Jan 2014
	 * @return		List<EntTblFactorySetting>
	 ************************************************************************/
	List<EntTblFactorySetting> selectListFactorySetting();
}
