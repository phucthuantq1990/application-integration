/************************************************************************
 * file name	： MatterManageService.java
 * author		： Le.Dinh
 * version		： 1.0.0
 * date created	： 17 Jan 2014
 * date updated	： 17 Jan 2014
 * description	： MatterManageService
 *
 ***********************************************************************/
package net.webike.RcProductFactory.service;

import java.sql.SQLException;
import java.util.List;

import net.webike.RcProductFactory.entity.EntMstFactoryMatterNew;

/**
 * MatterManageService.
 */
public interface MatterManageService {
	/**
	 * Get matter manage list.
     * @author      nguyen.hieu
     * @date		2014-01-18
	 * @param		offset Offset.
	 * @param		numOfRows Number of rows.
	 * @return		Matter list.
	 * @throws		SQLException SQLException.
	 */
	List<EntMstFactoryMatterNew> selectMatterManageList(int offset, int numOfRows) throws SQLException;

	/**
	 * Get filtered matter manage list.
     * @author      nguyen.hieu
     * @date        2014-01-18
	 * @param		page Page.
	 * @param		pageSize Page size.
	 * @param		entMstFactoryMatterNew Matter new.
	 * @return		Filtered matter manage list.
	 * @throws SQLException SQLException.
	 */
	List<EntMstFactoryMatterNew> selectFilteredMatterManageList(int page, int pageSize, EntMstFactoryMatterNew entMstFactoryMatterNew) throws SQLException;

	/************************************************************************
	 * <b>Description:</b><br>
	 *  Filter for select all.
	 *
	 * @author		Tran.Thanh
	 * @date		May 6, 2014
	 * @param 		entMstFactoryMatterNew ent query for filter
	 * @return		list entity for filter.
	 * @throws 		SQLException		List<EntMstFactoryMatterNew>
	 ************************************************************************/
	List<EntMstFactoryMatterNew> selectAllFilteredMatterManage(EntMstFactoryMatterNew entMstFactoryMatterNew) throws SQLException;

	/**
	 * Get filtered matter manage count.
     * @author      nguyen.hieu
     * @date        2014-01-18
	 * @param		entMstFactoryMatterNew Matter new.
	 * @return		Filtered matter manage count.
	 * @throws		SQLException SQLException.
	 */
	int selectFilteredMatterManageCount(EntMstFactoryMatterNew entMstFactoryMatterNew) throws SQLException;
}
