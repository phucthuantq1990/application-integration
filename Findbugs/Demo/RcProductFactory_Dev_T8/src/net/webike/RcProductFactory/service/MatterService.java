/************************************************************************
 * File Name    ： MatterService.java
 * Author       ： Nguyen.Chuong
 * Version      ： 1.0.0
 * Date Created ： 2013/09/24
 * Date Updated ： 2013/09/24
 * Description  ： Service to get matter from database.
 *
 ***********************************************************************/
package net.webike.RcProductFactory.service;

import java.sql.SQLException;
import java.util.List;

import net.webike.RcProductFactory.entity.EntMstFactoryMatter;
import net.webike.RcProductFactory.entity.EntMstFactoryMatterNew;
import net.webike.RcProductFactory.entity.EntMstFactoryMatterStatus;

/**
 * @author Nguyen.Chuong
 *
 */
public interface MatterService {
    /************************************************************************
     * <b>Description:</b><br>
     *  Get list of all matter status from db.
     *
     * @author      Nguyen.Chuong
     * @date        Sep 10, 2013
     * @return      List<EntMstFactoryMatterStatus>
     ************************************************************************/
    List<EntMstFactoryMatterStatus> selectAllMatterStatus();

    /************************************************************************
     * <b>Description:</b><br>
     *  get list of matter from DB with 2 param: status and user.
     *
     * @author      Nguyen.Chuong
     * @date        Sep 10, 2013
     * @param       status string
     * @param       user string
     * @return      List<EntMstFactoryMatterStatus>
     ************************************************************************/
    List<EntMstFactoryMatter> selectListMatterByStatusAndUser(String status, String user);

    /************************************************************************
     * <b>Description:</b><br>
     *  get list of detail PJ from DB with param: matter_no.
     *
     * @author      Tran.Thanh
     * @date        24 Sep 2013
     * @param       matterNo matter no
     * @return      EntMstFactoryMatter
     ************************************************************************/
    EntMstFactoryMatter selectDetailMatterByMatterNo(String matterNo);

    /************************************************************************
     * <b>Description:</b><br>
     *  delete Matter by mater_no and set kousin_user_id is userId.
     *
     * @author      Tran.Thanh
     * @date        24 Sep 2013
     * @param       userId matter kousin user id
     * @param       matterNo matter no
     ************************************************************************/
    void deleteMatter(String userId, String matterNo);

    /************************************************************************
     * <b>Description:</b><br>
     *  update matter detail.
     *
     * @author      Tran.Thanh
     * @date        24 Sep 2013
     * @param       entMatter EntMstFactoryMatter
     * @return      boolean.
     ************************************************************************/
     boolean updateDetailMatter(EntMstFactoryMatter entMatter);

     /************************************************************************
      * <b>Description:</b><br>
      *  get maximum matter_no in database.
      *
      * @author      Luong.Dai
      * @date        Now 7, 2013
      * @return       Integer
      ************************************************************************/
     int selectMaxMatterNo();

     /************************************************************************
      * <b>Description:</b><br>
      *  insert new matter to DB.
      *
      * @author      Luong.Dai
      * @date        Now 7, 2013
      * @param       entFactoryMatter EntMstFactoryMatter
      ************************************************************************/
     void insertNewMatter(EntMstFactoryMatter entFactoryMatter);

     /************************************************************************
     * <b>Description:</b><br>
     *  release matter: change matter status to "10".
     *
     * @author		Nguyen.Chuong
     * @date		Nov 7, 2013
     * @param       userId
     * @param       matterNo String
     * @param       userId login user
     * @return      boolean: true: release success, false: release fail.
     ************************************************************************/
    boolean releaseMatter(String userId, String matterNo);

    /**
     * Insert new matter to DB.
     * @author      nguyen.hieu
     * @date        2014-01-20
     * @param		entFactoryMatterNew Matter new.
     */
    void insertNewMatterNew(EntMstFactoryMatterNew entFactoryMatterNew);

    /**
     * Delete matter new list.
     * @author      nguyen.hieu
     * @date        2014-01-20
     * @param		matterNoList Matter no list.
     * @param		updatedUserId Updated user Id.
     * @param		matterChargeUserId : Null if admin.
     * @throws		SQLException SQLException.
     */
    //void deleteNewMatterNewList(List<Long> matterNoList, String updatedUserId, List<String> keyList, String matterChargeUserId) throws SQLException;
    /*BOE dang.nam 16/6/2014 add popupConfirm param*/
    /************************************************************************
     * <b>Description:</b><br>
     *  Delete matter new list.
     *
     * @author		Tran.Thanh
     * @date		May 8, 2014
     * @param 		matterNoList Matter no list.
     * @param 		updatedUserId Updated user Id.
     * @param 		matterChargeUserId Null if admin.
     * @param       popupConfirm String
     * @return		true if delete success
     * @throws 		SQLException		boolean
     ************************************************************************/
    boolean deleteNewMatterNewList(List<Long> matterNoList, String updatedUserId, String matterChargeUserId, String popupConfirm) throws SQLException;
    /*EOE dang.nam 16/6/2014 add popupConfirm param*/

    /************************************************************************
     * <b>Description:</b><br>
     *  select Factory Matter By Matter Code.
     *
     * @author		Tran.Thanh
     * @date		May 8, 2014
     * @param 		matterNo matterNo
     * @return		EntMstFactoryMatterNew EntMstFactoryMatterNew
     ************************************************************************/
    EntMstFactoryMatterNew selectFactoryMatterByMatterCode(Long matterNo); 
}
