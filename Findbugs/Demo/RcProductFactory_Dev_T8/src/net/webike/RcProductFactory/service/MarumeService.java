/************************************************************************
 * File Name	： BrandService.java
 * Author		： Nguyen.Chuong
 * Version		： 1.0.0
 * Date Created	： 2013/12/27
 * Date Updated	： 2013/12/27
 * Description	： Service to process with brand object.
 *
 ***********************************************************************/
package net.webike.RcProductFactory.service;

import java.util.List;

import net.webike.RcProductFactory.entity.EntMstMarume;


/**
 * Service to process with marume object.
 */
public interface MarumeService {

    /************************************************************************
     * <b>Description:</b><br>
     *  Select list all marume.
     *
     * @author		Tran.Thanh
     * @date		31 Dec 2013
     * @return		List<EntMstMarume>
     ************************************************************************/
    List<EntMstMarume> selectListAllMarume();
}
