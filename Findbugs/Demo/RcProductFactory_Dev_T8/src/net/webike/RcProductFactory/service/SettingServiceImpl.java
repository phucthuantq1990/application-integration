/**
 * SettingServiceImpl
 */
package net.webike.RcProductFactory.service;

import java.util.List;

import net.webike.RcProductFactory.entity.EntTblFactorySetting;
import net.webike.RcProductFactory.mapper.TblFactorySettingMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * @author Nguyen.Chuong
 * SettingServiceImpl implements SettingService
 */
@Service("SettingServiceImpl")
@Transactional
public class SettingServiceImpl implements SettingService {
    @Autowired
    private TblFactorySettingMapper settingMapper;

    @Override
    public String selectSettingValueBySettingCode(String settingCode) {
        String settingValue = settingMapper.selectSettingValueBySettingCode(settingCode);
        return settingValue;
    }

	@Override
	public List<EntTblFactorySetting> selectListFactorySetting() {
		List<EntTblFactorySetting> listFactorySetting = settingMapper.selectListFactorySetting();
		return listFactorySetting;
	}
}
