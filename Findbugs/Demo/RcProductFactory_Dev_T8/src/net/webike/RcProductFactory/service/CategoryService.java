/************************************************************************
 * File Name    ： CategoryService.java
 * Author       ： hoang.ho
 * Version      ： 1.0.0
 * Date Created ： 2014/01/08
 * Date Updated ： 2014/01/08
 * Description  ： Service to process with category object.
 ***********************************************************************/
package net.webike.RcProductFactory.service;
import java.sql.SQLException;
import java.util.List;

import net.webike.RcProductFactory.action.form.CategoryEditActionForm;
import net.webike.RcProductFactory.entity.EntMstAttribute;
import net.webike.RcProductFactory.entity.EntMstBunrui;
import net.webike.RcProductFactory.entity.EntMstCategory;
import net.webike.RcProductFactory.entity.EntTblCategoryAttribute;

/**
 * Category service used for get data and process business relate with category.
 * @author hoang.ho
 */
public interface CategoryService {

    /**
     * Get category list.
     * @param searchConditions filter information of category management
     * @return List<EntMstCategory>
     */
    List<EntMstCategory> selectCategoryManageList(EntMstCategory searchConditions);

    /**
     * Get size of category list.
     * @param searchConditions filter information of category management
     * @return List<EntMstCategory>
     */
    int selectCategoryManageListCount(EntMstCategory searchConditions);

    /**
     * Select attribute list.
     * @return List<EntMstAttribute>
     */
    List<EntMstAttribute> selectAttributeList();

    /**
     * Get category codes list from tbl_category_attribute byattributeCodes .
     * @param attributeCodes attributeCodes
     * @return List<EntMstCategory>
     */
    String selectCategoryCodes(String attributeCodes);

    /************************************************************************
     * <b>Description:</b><br>
     * get bunrui by bunrui code.
     *
     * @author      Long Vu
     * @date        Jan 10, 2014
     * @param bunruiCode bunrui code
     * @return      List<EntMstBunrui>
     ************************************************************************/
    EntMstBunrui getBunrui(String bunruiCode);

    /************************************************************************
     * <b>Description:</b><br>
     * get userName by userId.
     *
     * @author      thai son
     * @date        Jan 10, 2014
     * @param userId String
     * @return      String
     ************************************************************************/
    String getUserNameByUserIdInMstFactoryUser(String userId);

    /************************************************************************
     * <b>Description:</b><br>
     *  search and get list bunrui by bunruiName.
     *
     * @author		Luong.Dai
     * @date		Jan 8, 2014
     * @param bunruiName bunrui name
     * @return		List<EntMstBunrui>
     ************************************************************************/
    List<EntMstBunrui> getListBunrui(String bunruiName);

    /************************************************************************
     * <b>Description:</b><br>
     * get category by code.
     *
     * @author      Long Vu
     * @date        Jan 10, 2014
     * @param categoryCode category code
     * @return      List<EntMstBunrui>
     ************************************************************************/
    EntMstCategory selectCategoryByCode(int categoryCode);

    /************************************************************************
     * <b>Description:</b><br>
     * get catgory-attribute list by category code.
     *
     * @author      Long Vu
     * @date        Jan 10, 2014
     * @param categoryCode category cdoe
     * @return      List<EntMstBunrui>
     ************************************************************************/
    List<EntTblCategoryAttribute> selectCatAttributeByCode(int categoryCode);

    /************************************************************************
     * <b>Description:</b><br>
     *  search and get list Attribute by attrName.
     *
     * @author		Luong.Dai
     * @date		Jan 9, 2014
     * @param attrName attribute name
     * @return		List<EntMstAttribute>
     ************************************************************************/
    List<EntMstAttribute> selectListAttribute(String attrName);

    /************************************************************************
     * <b>Description:</b><br>
     *  to process update data in category and catgory_attribute in DB (insert, update, delete).
     *
     * @author      Long Vu
     * @date        Jan 10, 2014
     * @param form  insert, update, delete condition
     * @return      boolean
     ************************************************************************/
    boolean processUpdateDataCatAttribute(CategoryEditActionForm form);

    /************************************************************************
     * <b>Description:</b><br>
     *  to process insert data in category and catgory_attribute in DB (insert, update, delete).
     *
     * @author      Long Vu
     * @date        Jan 10, 2014
     * @param form  insert, update, delete condition
     * @return      boolean
     ************************************************************************/
    boolean processInsertDataCatAttribute(CategoryEditActionForm form);

    /************************************************************************
     * <b>Description:</b><br>
     *  to process check attribute list has not modified with database.
     *
     * @author      hoang.ho
     * @date        2014/01/10
     * @param catAttributeList list of catribute
     * @param form CategoryEditActionForm
     * @param categoryCode category Code
     * @return      boolean
     ************************************************************************/
    boolean checkAttrHasNotModified(List<EntTblCategoryAttribute> catAttributeList, String categoryCode, CategoryEditActionForm form);

    /************************************************************************
     * <b>Description:</b><br>
     *  Check duplicate category name.
     *
     * @author		Luong.Dai
     * @date		Jan 13, 2014
     * @param category category
     * @return		boolean
     ************************************************************************/
    boolean checkExistsCategoryByCategoryName(EntMstCategory category);

    /**
     * Get category list.
     * @author		nguyen.hieu
     * @date		2014-02-17
     * @return		Category list.
     * @throws		SQLException SQLException.
     */
    List<EntMstCategory> selectCategoryList() throws SQLException;
}
