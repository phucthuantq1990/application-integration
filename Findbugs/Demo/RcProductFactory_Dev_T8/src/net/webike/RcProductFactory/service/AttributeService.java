/************************************************************************
 * File Name	： AttributeService.java
 * Author		： Nguyen.Chuong
 * Version		： 1.0.0
 * Date Created	： 2014/01/14
 * Description	： Service to process with attribute object.
 *
 ***********************************************************************/
package net.webike.RcProductFactory.service;

import java.sql.SQLException;
import java.util.List;

import net.webike.RcProductFactory.entity.EntMstAttribute;
import net.webike.RcProductFactory.entity.EntMstAttributeGroup;

/**
 * Service to process with brand object.
 */
public interface AttributeService {

    /************************************************************************
     * <b>Description:</b><br>
     *  Select list entMstAttribute with filter.
     *
     * @author      Nguyen.Chuong
     * @date        Oct 29, 2013
     * @return      List<EntProduct>
     * @param       limit int number record in 1 page
     * @param       offset int current number page
     * @param       entAttribute content filter value
     * @throws      SQLException error xml
     ************************************************************************/
    List<EntMstAttribute> selectListAttributeWithFilter(int limit, int offset, EntMstAttribute entAttribute) throws SQLException;

    /************************************************************************
     * <b>Description:</b><br>
     *  count record when get data from DB.
     *
     * @author      Nguyen.Chuong
     * @date        Jan 16, 2014
     * @param       entAttribute entity contain filter
     * @return      int
     * @throws      SQLException sqlException
     ************************************************************************/
    int selectTotalRecordOfFilter(EntMstAttribute entAttribute) throws SQLException;
    /************************************************************************
     * <b>Description:</b><br>
     *  Select attribute by code.
     *
     * @author		Luong.Dai
     * @date		Jan 15, 2014
     * @param attributeCode Integer
     * @return		EntMstAttribute
     ************************************************************************/
    EntMstAttribute selectAttributeByCode(Integer attributeCode);

    /************************************************************************
     * <b>Description:</b><br>
     * Check the input name is exist in DB or not.
     *
     * @author      Long Vu
     * @date        Jan 15, 2014
     * @return      boolean
     * @param       attribute content filter value
     * @param       insertMode insert mode
     ************************************************************************/
    boolean checkAttrExistedName(EntMstAttribute attribute, int insertMode);

    /************************************************************************
     * <b>Description:</b><br>
     *  selec list attributeGroup by attribute code.
     *
     * @author      Luong.Dai
     * @date        Jan 15, 2014
     * @param attributeCode Integer
     * @return      List<EntMstAttributeGroup>
     ************************************************************************/
    List<EntMstAttributeGroup> selectAttrGroupByCode(Integer attributeCode);

    /************************************************************************
     * <b>Description:</b><br>
     * process for insert, update, delete in attribute and attribute-group.
     *
     * @author      Long Vu
     * @date        Jan 15, 2014
     * @param attribute object insert to attribute.
     * @param deletedCodeList String delete in attribute group.
     * @param attrGroupInsertdList object insert to attribute group.
     * @param attrGroupUpdatedList object update to attribute group.
     * @param userId login user id.
     ************************************************************************/
    void processUpdateDataAttrGroup(EntMstAttribute attribute, String deletedCodeList
                                   , List<EntMstAttributeGroup> attrGroupInsertdList
                                   , List<EntMstAttributeGroup> attrGroupUpdatedList, String userId);

    /************************************************************************
     * <b>Description:</b><br>
     *  insert new attribute into DB.
     *
     * @author		Luong.Dai
     * @date		Jan 15, 2014
     * @param userId String
     * @param attribute EntMstAttribute
     * @param attributeGroupList List<EntMstAttributeGroup>
     * @return		boolean
     ************************************************************************/
    boolean insertNewAttribute(String userId, EntMstAttribute attribute,
                                        List<EntMstAttributeGroup> attributeGroupList);
}
