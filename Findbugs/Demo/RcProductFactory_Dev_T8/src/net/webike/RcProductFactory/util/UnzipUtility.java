/************************************************************************
 * file name	： UnzipUtility.java
 * author		： Tran.Thanh
 * version		： 1.0.0
 * date created	： Sep 10, 2014
 * date updated	： Sep 10, 2014
 * description	： Replace your description for this class here
 *
 ***********************************************************************/
package net.webike.RcProductFactory.util;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.apache.commons.io.FileUtils;

/**
 * Unzip utility.
 */
public final class UnzipUtility {
    /**
     * Size of the buffer to read/write data.
     */
    private static final int BUFFER_SIZE = 4096;

    private static final int LENGTH_4 = 4;

    private static final int LENGTH_16 = 16;

    /************************************************************************
     * <b>Description:</b><br>
     *  unzip zip file.
     *
     * @author		Tran.Thanh
     * @date		Sep 10, 2014
     * @param 		zipFilePath path file unzip
     * @param 		destDirectory path to unzip
     * @throws IOException		void
     ************************************************************************/
    public void unzip(String zipFilePath, String destDirectory) throws IOException {
        File destDir = new File(destDirectory);
        if (!destDir.exists()) {
            destDir.mkdir();
        }

        ZipInputStream zipIn = new ZipInputStream(new FileInputStream(zipFilePath));
        ZipEntry entry = zipIn.getNextEntry();
        // iterates over entries in the zip file
        while (entry != null) {
            String filePath = destDirectory + File.separator + entry.getName();
            if (!entry.isDirectory()) {
                // if the entry is a file, extracts it
                extractFile(zipIn, filePath);
                renameNonUTF8FileName(filePath, destDirectory);
            } else {
                // if the entry is a directory, make the directory
                File dir = new File(filePath);
                if (!dir.exists()) {
                	dir.mkdir();
                }
            }
            zipIn.closeEntry();
            entry = zipIn.getNextEntry();
        }
        zipIn.close();
    }

    /************************************************************************
     * <b>Description:</b><br>
     *  extract File.
     *
     * @author		Tran.Thanh
     * @date		Sep 10, 2014
     * @param 		zipIn file Zip In
     * @param 		filePath file to unzip
     * @throws 		IOException		void
     ************************************************************************/
    private void extractFile(ZipInputStream zipIn, String filePath) throws IOException {
    	//Delete image if exists
    	File fileToDelete = new File(filePath);
    	if (fileToDelete.exists()) {
			FileUtils.forceDelete(fileToDelete);
		}

    	//Unzip file
        BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(filePath));
        byte[] bytesIn = new byte[BUFFER_SIZE];
        int read = 0;
        while ((read = zipIn.read(bytesIn)) != -1) {
            bos.write(bytesIn, 0, read);
        }
        bos.close();
    }

    /************************************************************************
     * <b>Description:</b><br>
     *  rename Non UTF8 File Name.
     *
     * @author		Tran.Thanh
     * @date		Sep 11, 2014
     * @param 		directory directory
     * @param 		destDirectory destDirectory
     * @throws UnsupportedEncodingException		void
     ************************************************************************/
    private void renameNonUTF8FileName(String directory, String destDirectory) throws UnsupportedEncodingException {
    	File fileEntry = new File(directory);

    	StringBuffer buf = new StringBuffer();
    	if (fileEntry.getName().indexOf("#U") > -1) {
    		String[] abc = fileEntry.getName().split("#U");

        	for (int i = 0; i < abc.length; i++) {
        		if (abc[i].length() > 2) {
        			String temp = abc[i].substring(0, LENGTH_4);
        			int hexVal = Integer.parseInt(temp, LENGTH_16);
    				buf.append((char) hexVal);
    				if (abc[i].length() > LENGTH_4) {
    					buf.append(abc[i].substring(LENGTH_4 + 1, abc[i].length()));
    				}
        		} else {
        			buf.append(abc[i]);
        		}
        	}
        	String newName = buf.toString();
        	fileEntry.renameTo(new File(destDirectory, newName));
    	}
    }
}
