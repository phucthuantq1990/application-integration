/************************************************************************
 * File Name    ： Utils.java
 * Author       ： Nguyen.Chuong
 * Version      ： 1.0.0
 * Date Created ： 2013/10/24
 * Date Updated ： 2013/10/24
 * Description  ： Common function of productFactory.
 *
 ***********************************************************************/
package net.webike.RcProductFactory.util;

/**
 *  Constant value of productFactory system.
 */
public final class Constant {
    /**---------------------------------------------------------------------------
     * private constructor.
     */
    private Constant() { };

    /**********user login authority.*****************/
    public static final String ADMIN_AUTHORITY = "1";
    public static final String OPERATOR_AUTHORITY = "0";

    /***********Contants value.*********************/
    //Normal contant values for all page.
    public static final String IMAGE_SERVER = "http://img.webike.net/catalogue/";
    public static final String IMAGE_SERVER_2 = "http://www.webike.net/catalogue/";
    public static final String NO_IMAGE_URL = "./images/no-image.jpg";
    public static final String PAGING_DEFAULT_PAGE = "0";
    public static final String DEFAULT_REDIRECT_PAGE = "./matterManage.html";
    public static final String MATTER_DETAIL_URL = "./matterDetail.html";
    public static final String PRODUCT_EDIT_URL = "./productEdit.html";
    public static final String DEFAULT_ERROR_PAGE = "./error.html";

    public static final String FORMAT_IMAGE_SUCCESS = "^(jpe?g|png)$";
    //Setting key to query db
    public static final String SETTING_PRODUCT_FACTORY_URL = "PRODUCT_FACTORY_URL";

    //Default sort dir of all grid
    public static final String DEFAULT_SORT_DIR = "ASC";
    //ProductInfo constants value
    public static final int    LIMIT_OF_PRODUCT_TABLE = 50;
    public static final String PRODUCT_CHECKED_STATUS_CODE = "10";
    public static final String PRODUCT_INFO_ERROR_FLAG_MESSAGE = "使用負荷分類";

    //All error status
    public static final String STATUS_ALL_ERROR_NAME = "すべてエラー";
    public static final String STATUS_ALL_ERROR_VALUE = "999";

    public static final int LIMIT_OF_CATEGORY_CODE_4 = 4;
    public static final int LIMIT_OF_CATEGORY_CODE_8 = 8;

    public static final int LEVEL_1 = 1;
    public static final int LEVEL_2 = 2;
    public static final int LEVEL_3 = 3;
    public static final int LEVEL_4 = 4;
    public static final int LEVEL_5 = 5;

    /*Constant to format date to Japan week part type*/
    public static final int FIRST_WEEK_PART_END_DAY =  10;
    public static final int MIDDLE_WEEK_PART_END_DAY =  20;
    public static final int LAST_WEEK_PART_END_DAY =  31;

    /*Static value for MD5 function.*/
    public static final int OXFF = 0xff;
    public static final int OX100 = 0x100;
    public static final int HEX = 16;

    /***********Validate rule.******************/
    //Static rule for validate userEnt.
    public static final int USER_ID_MAX_LENGTH = 30;
    public static final int PASSWORD_MIN_LENGTH = 5;
    public static final int PASSWORD_MAX_LENGTH = 30;
    public static final int EMAIL_MAX_LENGTH = 80;
    public static final int USER_NAME_MAX_LENGTH = 10;
    //Regex for alphaber charater and number(1byte).
    public static final String LETTERONLY = "[a-zA-Z0-9.-_]+";
  //Regex for number(1byte).
    public static final String NUMBERONLY = "[0-9]+";
    //Regex for charater(alphabet character, hiragana, katakana, kanji) number(2byte, 1byte), space(2byte, 1byte).
    public static final String LETTERANDCPACEONLY = "[a-zA-Z0-9０-９ｂ-ｚＢ-Ｚｦ-ﾟァ-ンｧ-ﾝﾞﾟ\\u3000\\u3040-\\u309f\\u30a0-\\u30ff\\u4e00-\\u9faf\\s]+";
    //Regex for email.
    public static final String EMAIL = "^(([A-Za-z0-9]+\\_)|([A-Za-z0-9]+\\-)|([A-Za-z0-9]+\\.))*[A-Za-z0-9]"
                                        + "+@(([A-Za-z0-9]+\\-)|([A-Za-z0-9]+\\_)|([A-Za-z0-9]+\\.))*[A-Za-z0-9]{1,}\\.[a-zA-Z]{2,}$";
    //Static length for productEnt
    public static final int LIMIT_LENGTH_PRODUCT_MAKER = 8;
    public static final int LIMIT_LENGTH_PRODUCT_MODEL = 12;
    public static final int LIMIT_LENGTH_PRODUCT_STYLE = 10;
    //Max length of field in DB.
    public static final int PRODUCT_ID_MAX_LENGTH_INPUT = 18;
    public static final int PRODUCT_SYOUHIN_SYS_CODE_MAX_LENGTH_INPUT = 18;
    public static final int MATTER_NO_MAX_LENGTH_INPUT = 18;
    /**max length of tbl_factory_product_general.product_brand_code = 8.*/
    public static final int PRODUCT_BRAND_CODE_MAX_LENGTH_INPUT = 8;
    /**max length of rc_syouhin.mst_brand.name - length = 50.*/
    public static final int BRAND_NAME_MAX_LENGTH = 50;
    /**tbl_factory_product_general.product_category_code = 8.*/
    public static final int PRODUCT_CATEGORY_CODE_MAX_LENGTH_INPUT = 8;
    /**Max length input.*/
    public static final int PRODUCT_NAME_MAX_LENGTH_INPUT = 255;
    /**tbl_factory_product_general.product_code = 50.*/
    public static final int PRODUCT_CODE_LENGTH = 50;
    /**tbl_factory_product_general.product_ean_code = 30.*/
    public static final int PRODUCT_EAN_CODE_LENGTH = 30;
    //Max length input of DESCRIPTION_SUMMARY no error.
//    public static final int DESCRIPTION_SUMMARY_LENGTH = 35000;
    public static final int DESCRIPTION_SUMMARY_LENGTH = 30000;
    //Max length input of DESCRIPTION_CAUTION no error.
    public static final int DESCRIPTION_CAUTION_LENGTH = 6000;
    //Max length input of DESCRIPTION_SENTENCE_LENGTH no error.
    public static final int DESCRIPTION_SENTENCE_LENGTH = 60000;
    /**max length of link combo: [Link Reason::Show Name::URL][Link Reason::Show Name::URL]... = 30000*/
//    public static final int NUMBER_FIELD_PRODUCT_LINK = 3;
    public static final int PRODUCT_LINK_LENGTH = 30000;
    /**tbl_factory_product_link.product_link_title = 30.*/
    public static final int PRODUCT_LINK_TITLE_LENGTH = 30;
    /**tbl_factory_product_link.product_link_url = 10000.*/
    public static final int PRODUCT_LINK_URL_LENGTH = 10000;
    /**tbl_factory_product_video.product_video_title = 128.*/
    public static final int PRODUCT_VIDEO_TITLE_LENGTH = 128;
    /**tbl_factory_product_video.product_video_url = 1024.*/
    public static final int PRODUCT_VIDEO_URL_LENGTH = 1024;
    /**tbl_factory_product_video.product_video_description = 3000.*/
    public static final int PRODUCT_VIDEO_DESCRIPTION_LENGTH = 3000;
    //Max length input of DESCRIPTION_REMARKS no error.
    public static final int DESCRIPTION_REMARKS_LENGTH = 4000;
    /**max length of guest input combo: [Item name::item description][Item name::item description]... = 30000.*/
    public static final int PRODUCT_GUEST_INPUT_CONFIRM = 30000;
    /**tbl_factory_product_guest_input.product_guest_input_title = 20.*/
    public static final int PRODUCT_GUEST_INPUT_TITLE_LENGTH = 20;
    /**tbl_factory_product_guest_input.product_guest_input_description = 128.*/
    public static final int PRODUCT_GUEST_INPUT_DESCRIPTION_LENGTH = 128;
    /**tbl_factory_product_fit_model.fit_model_maker = 50.*/
    public static final int FIT_MODEL_MAKER_LENGTH = 50;
    /**tbl_factory_product_fit_model.fit_model_model = 1000.*/
    public static final int FIT_MODEL_MODEL_LENGTH = 1000;
    /**tbl_factory_product_fit_model.fit_model_style = 100.*/
    public static final int FIT_MODEL_STYLE_LENGTH = 512;
    /**tbl_factory_product_supplier.siire_code = 3.*/
    public static final int SIIRE_CODE_LENGTH = 3;
    /**tbl_factory_product_supplier.nouki_code = 3.*/
    public static final int NOUKI_CODE_LENGTH = 3;
    /**tbl_factory_product_supplier.supplier_syouhin_siire_order = 3.*/
    public static final int SUPPLIER_SYOUHIN_SIIRE_ORDER_LENGTH = 1;
    /**max length input PRODUCT_SUPPLIER_RELEASE_DATE no error.*/
    public static final int PRODUCT_SUPPLIER_RELEASE_DATE_LENGTH = 30;
    /**tbl_factory_product_general.product_proper_price = 8.*/
    public static final int PRODUCT_PROPER_PRICE_MAX_LENGTH = 8;
    /**tbl_factory_product_supplier_price.supplier_price_price = 8.*/
    public static final int SUPPLIER_PRICE_PRICE_MAX_LENGTH = 8;
    /**tbl_factory_product_general.product_group_code = 24. But max LONG is 18*/
    public static final int PRODUCT_GROUP_CODE_MAX_LENGTH = 18;
    /**tbl_factory_product_image.product_image_detail_path = tbl_factory_product_image.product_image_thumbnail_path = 255.*/
    public static final int PRODUCT_IMAGE_PATH_MAX_LENGTH = 255;
//    public static final int PRODUCT_IMAGE_THUMBNAIL_PATH_MAX_LENGTH = PRODUCT_IMAGE_PATH_MAX_LENGTH;
//    public static final int PRODUCT_IMAGE_DETAIL_PATH_MAX_LENGTH = PRODUCT_IMAGE_PATH_MAX_LENGTH;
    /**tbl_factory_product_select.select_code = 3.*/
    public static final int SELECT_CODE_MAX_LENGTH = 3;
    /**tbl_factory_product_select.product_select_display = 300.
     * && tbl_factory_product_select.product_select_value = 300*/
    public static final int SELECT_NAME_MAX_LENGTH = 150;
    /**tbl_factory_product_link.product_link_reason_code = 1.*/
    public static final int LINK_REASON_CODE_MAX_LENGTH = 1;

    /********************Parameter name.********************/
    //User param
    public static final String PARAM_USER_ID = "userId";
    public static final String PARAM_USER_NAME = "userName";
    public static final String PARAM_USER_AUTHORITY = "userAuthority";
    public static final String PARAM_STATUS = "status";
    public static final String PARAM_DEL_FLAG = "delFlg";
    public static final String PARAM_FLAG_PASS_CHANGE = "flgPassChange";
    //Matter param.
    public static final String PARAM_MATTER_ID = "matterNo";
    public static final String PARAM_MATTER_NO = "Matter_no";
    public static final String PARAM_MATTER_NAME = "mName";
    public static final String PARAM_MATTER_STATUS = "status";
    public static final String PARAM_MATTER_CHARGE_USER_ID = "matterChargeUserId";
    public static final String PARAM_MATTER_MESSAGE = "mMessage";
    //Product param.
    public static final String PARAM_PRODUCT_ID = "productId";
    public static final String PARAM_SYSTEM_PRODUCT_CODE = "systemProductCode";
    public static final String PARAM_PRODUCT_MANUFACTORY_CODE = "manufactoryCode";
    public static final String PARAM_PRODUCT_BUNRUI_CODE = "bunruiCode";
    public static final String PARAM_PRODUCT_STATUS_CODE = "statusProductCode";
    public static final String PARAM_PAGE_SIZE = "pageSize";
    public static final String PARAM_PAGE_NUMBER = "page";
    public static final String PARAM_PRODUCT_CODE = "productCode";
    public static final String PARAM_SYOUHIN_SYS_CODE = "syouhinSysCode";
    //Error param
    public static final String PARAM_ERROR = "error";

    //Param name of change product mode
    public static final String PARAM_CHANGE_PRODUCT_MODE = "changeProductMode";

    /*******************Parameter, default value.******************************/
    //Error param value for error page.
    public static final String ERROR_ERROR_PARAM = "param";
    public static final String ERROR_403 = "403";
    public static final String ERROR_404 = "404";
    public static final String ERROR_NO_PERMISSION = "noPermission";
    public static final String ERROR_NO_PERMISSION_EXCUTE = "noPermissionExcute";
    public static final String ERROR_NO_DATA = "noData";
    public static final String ERROR_SYSTEM_ERROR = "errorSystem";
    //Error param value for login.
    public static final String ERROR_LOGIN_VALUE = "login";
    //Default value of matter status.
    public static final String MATTER_STATUS_DEFAULT = "00";

    //Default value for new matter
    public static final String MATTER_KIND_CODE = "40";
    public static final String MATTER_STATUS_CODE = "00";
    public static final int DEL_FLG = 0;
    public static final String MATTER_MESSAGE = "";

    // Static value for upload image
    public static final int IMAGE_WIDTH = 140;
    public static final int IMAGE_HEIGHT = 140;
    public static final String FOLDER_TMP_UPLOAD = "tmp_upload";
    public static final String FOLDER_TMP_IMAGES_PRODUCT_SENTENCE = "tmp_image";
    public static final String FOLDER_BRAND_IMAGE = "brand_image";
    public static final String BRAND = "brand_";
    public static final String IMAGE_TYPE_GIF = "gif";
    public static final int LENGTH_TAIL_IMAGE = 3;
    public static final String MAX_IMAGE_FOLDER_MATTER = "MAX_IMAGE_FOLDER_MATTER";
    public static final String MAX_SIZE_UPLOAD_IMAGE = "MAX_SIZE_UPLOAD_IMAGE";
    public static final String IMAGE = "image";
    // symbol char
    public static final String UNDER_BAR = "_";
    public static final String DOT = ".";
    public static final String HYPHEN = "-";
    public static final String CHAR_BETWEEN_USER_IMAGE = "--";
	// Mapping attribute type
    public static final String INTEGER = "整数";
    public static final String FLOAT = "小数点";
    public static final String STRING = "文字列";
    public static final String GROUP = "グループ";
    public static final String FLAG = "フラグ";

    // Session key
    /**
     * SESSION_CATEGORY_EDIT_ATTR_LIST.
     */
    public static final String SESSION_CATEGORY_EDIT_ATTR_LIST = "SESSION_CATEGORY_EDIT_ATTR_LIST";
    public static final String SESSION_BRAND_EDIT = "SESSION_BRAND_EDIT";
    public static final String SESSION_CAT_ATTRIBUTE_TO_CREATE_MESSAGE_UPDATED = "catAttributeToCreateMessageUpdated";
    // redmine
    public static final int HTTPSCON_RESPONSECODE = 200;
    public static final String REDMINE_CURRENT_USER_URL = "users/current.json?include=memberships";
    public static final String REDMINE_USER_URL = "users/";
    public static final String REDMINE_ISSUES_URL = "issues/";
//    public static final String REDMINE_ISSUES_URL_NO_HTTP = "issues/";
//    public static final String REDMINE_HOST_NAME = "redmine.ig.webike.net";
//    public static final String REDMINE_HOST = "https://redmine.ig.webike.net/redmine_md_test";
    public static final String REDMINE_PRODUCTFACTORY_PJ_ID = "ROLE_CHECK_PROJET_ID";
    public static final String USER_MANAGER_ROLE_ID = "USER_MANAGER_ROLE_ID";
    public static final String MASTER_SHOW_ROLE_ID = "MASTER_SHOW_ROLE_ID";
    public static final String MASTER_EDIT_ROLE_ID = "MASTER_EDIT_ROLE_ID";

    /**
     * Session for edit attribute.
     */
    public static final String SESSION_ATTRIBUTE = "attribute";
    public static final String SESSION_ATTRIBUTE_GROUP_LIST = "attributeGroupList";
    public static final String SESSION_MSG_CODE = "msgCode";
    public static final String TYPE_FLAG = "Flag";

    /**
     * product_syouhin_sys_code.
     */
    public static final String SYOUHIN_CODE_MISTAKE = "新商品";
    public static final String SYOUHIN_CODE_0 = "新規製品";
    /**
     * refer_mode.
     */
    public static final String REFER_MODE_REAL = "real";
    public static final String REFER_MODE_TEMPORARY = "temporary";

    /**
     * The error length of fit model maker (if over this value it will show error).
     * value get from rc_product_factry.tbl_factory_product_fit_model DB
     * In the column fit_model_maker
     */
    public static final int MODEL_MAKER_LENGTH = 100;
    /**
    * The error length of fit model style (if over this value it will show error).
    * value get from rc_product_factry.tbl_factory_product_fit_model DB
    * In the column fit_model_style
    */
    public static final int MODEL_STYLE_LENGTH = 512;
    /**
    * The warning length of fit model maker (if over this value it will show warning).
    * value get from rc_syouhin.tbl_syouhin_fit_model DB
    * In the column maker
    */
    public static final int MAKER_LENGTH = 50;
    /**
    * The warning length of fit model style (if over this value it will show warning).
    * value get from rc_syouhin.tbl_syouhin_fit_model DB
    * In the column style
    */
    public static final int STYLE_LENGTH = 255;
    /**
     * Max length error of model.
     */
    public static final int MODEL_MODEL_LENGTH = 1020;
    /**
     * Max length warning of model.
     */
    public static final int MODEL_LENGTH = 1000;

    /**
    * field: rc_syouhin.mst_brand.hyouji_name1 - length = 50.
    */
    public static final int BRAND_HYOUJI_NAME1_LENGTH = 50;

    /**
    * field: rc_syouhin.mst_brand.hyouji_name2 - length = 50.
    */
    public static final int BRAND_HYOUJI_NAME2_LENGTH = 50;

    /**
    * field: rc_syouhin.mst_brand.hyouji_name3 - length = 50.
    */
    public static final int BRAND_HYOUJI_NAME3_LENGTH = 50;

    /**
    * field: rc_syouhin.mst_brand.sort_kana_name - length = 50.
    */
    public static final int BRAND_SORT_KANA_NAME_LENGTH = 50;

    /**
    * field: rc_syouhin.mst_brand.logo_LENGTH - length = 255.
    */
    public static final int BRAND_LOGO_LENGTH = 255;

    /**
    * field: rc_syouhin.mst_brand.url - length = 255.
    */
    public static final int BRAND_URL_LENGTH = 255;

    /**
    * field: rc_syouhin.mst_brand.rss - length = 255.
    */
    public static final int BRAND_RSS_LENGTH = 255;

    /**
    * field: rc_syouhin.mst_category.category_name - length = 255.
    */
    public static final int CATEGORY_NAME_LENGTH = 255;

    /**
    * field: rc_syouhin.mst_attribute.attribute_name - length = 256.
    */
    public static final int ATTRIBUTE_NAME_LENGTH = 256;

    /**
    * field: rc_syouhin.mst_attribute.attribute_regexp - length = 256.
    */
    public static final int ATTRIBUTE_REGEXP_LENGTH = 256;

    public static final Float PERCENT = 100f;

    /**
     * field: limit number to get list select of product.
     */
    public static final int LIMIT_SELECT = 10;

    /**
     * field: Edit mode for page.
     */
    public static final String EDIT_MODE = "E";

    /**
     * field: New mode for page.
     */
    public static final String NEW_MODE = "N";

    /** Product Edit. */
    /**tbl_factory_product_general.product_brand_code = 9.*/
    public static final int PRODUCT_BRAND_CODE_WARNING_LENGTH = 9;
    /**tbl_factory_product_general.product_category_code = 9.*/
    public static final int PRODUCT_CATEGORY_CODE_WARNING_LENGTH = 9;
    /**tbl_factory_product_general.product_name = 512.*/
    public static final int PRODUCT_NAME_WARNING_LENGTH = 512;
    /**tbl_factory_product_general.product_code = 100.*/
    public static final int PRODUCT_CODE_WARNING_LENGTH = 100;
    /**tbl_factory_product_general.product_ean_code = 40.*/
    public static final int PRODUCT_EAN_CODE_WARNING_LENGTH = 40;
    /**tbl_factory_product_description.description_summary = 35010.*/
    public static final int DESCRIPTION_SUMMARY_WARNING_LENGTH = 35010;
    /**tbl_factory_product_description.description_caution = 6010.*/
    public static final int DESCRIPTION_CAUTION_WARNING_LENGTH = 6010;
    /**tbl_factory_product_link.product_link_title = 50.*/
    public static final int PRODUCT_LINK_TITLE_WARNING_LENGTH = 50;
    /**tbl_factory_product_link.product_link_url = 1020.*/
    public static final int PRODUCT_LINK_URL_WARNING_LENGTH = 1020;
    /**tbl_factory_product_video.product_video_title = 255.*/
    public static final int PRODUCT_VIDEO_TITLE_WARNING_LENGTH = 255;
    /**tbl_factory_product_video.product_video_url = 2048.*/
    public static final int PRODUCT_VIDEO_URL_WARNING_LENGTH = 2048;
    /**tbl_factory_product_video.product_video_description = 3020.*/
    public static final int PRODUCT_VIDEO_DESCRIPTION_WARNING_LENGTH = 3020;
    /**tbl_factory_product_description.description_remarks = 4020.*/
    public static final int DESCRIPTION_REMARKS_WARNING_LENGTH = 4020;
    /**tbl_factory_product_guest_input.product_guest_input_title = 30.*/
    public static final int PRODUCT_GUEST_INPUT_TITLE_WARNING_LENGTH = 30;
    /**tbl_factory_product_guest_input.product_guest_input_description = 255.*/
    public static final int PRODUCT_GUEST_INPUT_DESCRIPTION_WARNING_LENGTH = 255;
    /**tbl_factory_product_fit_model.fit_model_maker = 100.*/
    public static final int FIT_MODEL_MAKER_WARNING_LENGTH = 100;
    /**tbl_factory_product_fit_model.fit_model_model = 1020.*/
    public static final int FIT_MODEL_MODEL_WARNING_LENGTH = 1020;
    /**tbl_factory_product_fit_model.fit_model_style = 120.*/
    public static final int FIT_MODEL_STYLE_WARNING_LENGTH = 255;
    /**tbl_factory_product_supplier.siire_code = 3.*/
    public static final int SIIRE_CODE_WARNING_LENGTH = 3;
    /**tbl_factory_product_supplier.nouki_code = 3.*/
    public static final int NOUKI_CODE_WARNING_LENGTH = 3;
    /**tbl_factory_product_supplier.supplier_syouhin_siire_order = 3.*/
    public static final int SUPPLIER_SYOUHIN_SIIRE_ORDER_WARNING_LENGTH = 3;

    public static final int FIT_MODEL_MAX_LENGTH_COLUMN_CSV = 20000;
    public static final int VIDEO_MAX_LENGTH_COLUMN_CSV = 30000;
//    public static final int LINK_MAX_LENGTH_COLUMN_CSV = 100;

    /**format date pattern is yyyyMMddHHmmss. */
    public static final String FORMAT_DATE_PATTERN_YYYYMMDD_HHMMSS = "yyyyMMddHHmmss";
    /**
     * Table File name format calc_import_csv_data_{0}_{1}.<br/>
     * {0} : login user id<br/>
     * {1} : string current date.
     * */
    public static final String TABLE_NAME_FORMAT = "calc_import_csv_data_%s_%s";
    public static final String REFIX_TMP_TABLE_FORMAT = "calc_import_csv_data_";
    public static final String ERROR_TABLE_NAME_FORMAT = "%s_%s";

    public static final String CSV_IMPORT_ERROR_CODE_0 = "0";
    public static final String CSV_IMPORT_ERROR_CODE_1 = "1";
    public static final String CSV_IMPORT_ERROR_CODE_2 = "2";
    public static final String CSV_IMPORT_ERROR_CODE_3 = "3";
    public static final String CSV_IMPORT_ERROR_CODE_4 = "4";
    public static final String CSV_IMPORT_ERROR_CODE_5 = "5";
    public static final String CSV_IMPORT_ERROR_CODE_6 = "6";
    public static final String CSV_IMPORT_ERROR_CODE_7 = "7";
    public static final String CSV_IMPORT_ERROR_CODE_8 = "8";
    public static final String CSV_IMPORT_ERROR_CODE_9 = "9";
    public static final String CSV_IMPORT_ERROR_CODE_10 = "10";
    public static final String CSV_IMPORT_ERROR_CODE_11 = "11";
    public static final String CSV_IMPORT_ERROR_CODE_99 = "99";

    public static final String CSV_IMPORT_ERROR_MSG_1 = "システム品番と識別番号が空欄です。いずれかのカラムに有効なコードを入力してください";
    public static final String CSV_IMPORT_ERROR_MSG_2 = "入力されている識別番号が一致しません";
    public static final String CSV_IMPORT_ERROR_MSG_3 = "入力されているシステム品番は案件内に存在しません。識別番号は一致しているのでシステム品番を空欄にしてください";
    public static final String CSV_IMPORT_ERROR_MSG_4 = "入力されている識別番号は案件内に存在しません。システム品番は一致しているので識別番号を空欄にしてください";
    public static final String CSV_IMPORT_ERROR_MSG_5 = "更新モードが指定されていません";
    public static final String CSV_IMPORT_ERROR_MSG_6 = "更新モードに正しくない文字が入力されています";
    public static final String CSV_IMPORT_ERROR_MSG_7 = "入力されているシステム品番が一致しません";
    public static final String CSV_IMPORT_ERROR_MSG_8 = "入力されている識別番号とシステム品番が一致しません";
    public static final String CSV_IMPORT_ERROR_MSG_9 = "更新モードがnの場合は0を入力してください";
    public static final String CSV_IMPORT_ERROR_MSG_10 = "対象案件に識別番号が存在しません";
    public static final String CSV_IMPORT_ERROR_MSG_11 = "対象案件にシステム品番が存在しません";

    public static final int CSV_ON_WORKING_ERROR_MSG_CODE = 1;
    public static final int CSV_SIZE_ERROR_MSG_CODE = 2;
    public static final int CSV_UPLOAD_ERROR_MSG_CODE = 3;
    public static final int CSV_HEADER_NOT_EXIST_ERROR_MSG_CODE = 4;
    public static final int CSV_CANCEL_IMPORT_ERROR_MSG_CODE = 5;
    public static final int CSV_WRONG_HEADER_ERROR_MSG_CODE = 6;
    public static final int CSV_CANNOT_CREATE_TEMP_TABLE_MSG_CODE = 7;
    public static final int CSV_MAXIMUM_IMPORT_LENGTH = 8;
    public static final int MAXIMUN_IMPORT_PRODUCT_CSV = 9;
    public static final int ERROR_IMPORT_CSV_FILE = 10;
    public static final int CSV_INVALID_FORMAT = 11;
    public static final long CSV_MAX_FILE_SIZE = 50 * 1024 * 1024;
  
    /**productEdit page. */
    public static final String MSG_STRING = "msgString";
    public static final int PRODUCT_EDIT_IMAGE_BLOCK = 2;
    public static final int PRODUCT_EDIT_GENARAL_BLOCK = 1;
    public static final int PRODUCT_EDIT_ATRIBUTE_BLOCK = 4;
    public static final int PRODUCT_EDIT_CONDITION_BLOCK = 3;
    public static final int PRODUCT_EDIT_LINK_BLOCK = 5;
    public static final int PRODUCT_EDIT_VIDEO_BLOCK = 6;
    public static final int PRODUCT_EDIT_DESCRIPTION_BLOCK = 7;
    public static final int PRODUCT_EDIT_GUEST_BLOCK = 8;
    public static final int PRODUCT_EDIT_SUPPLIER_BLOCK = 9;
    public static final int PRODUCT_EDIT_CATEGORY_BLOCK = 10;
    public static final int PRODUCT_EDIT_FIT_MODEL_BLOCK = 11;
    public static final int PRODUCT_EDIT_SUPPLIER_STATUS_BLOCK = 12;
    public static final int PRODUCT_EDIT_RELEASE_DATE_BLOCK = 13;
    public static final int PRODUCT_EDIT_UPLOAD_BLOCK = 14;
    public static final int PRODUCT_EDIT_PRODUCT_BLOCK = 15;
    public static final int PRODUCT_EDIT_PRODUCT_NULL = 0;
    public static final int PRODUCT_MAX_ATTRIBUTE_LENGTH = 30;
    public static final int PRODUCT_MAX_IMAGE_LENGTH = 10;
    public static final int PRODUCT_MAX_OPTION_LENGTH = 10;
    public static final String UPDATE_MODE = "U";
    public static final String INSERT_MODE = "I";
    public static final String DELETE_MODE = "D";
    public static final String SCREEN_MODE = "N";
    public static final int PRODUCT_MAX_PRODUCT_CODE_LENGTH = 50;
    public static final int PRODUCT_MAX_PRODUCT_EAN_CODE_LENGTH = 30;
    public static final int PRODUCT_MAX_SUPPLIER_RELEASE_DATE = 30;
    public static final int PRODUCT_MAX_DESCRIPTION_SUMMARY_LENGTH = 35000;
    public static final int PRODUCT_MAX_DESCRIPTION_CAUTION_LENGTH = 6000;
    public static final int PRODUCT_MAX_DESCRIPTION_REMARKS_LENGTH = 4000;
    public static final int PRODUCT_MAX_PRODUCT_NAME_LENGTH = 255;
    public static final String TYPE_INTEGER = "Integer";
    public static final String TYPE_FLOAT = "Float";
    public static final String TYPE_STRING = "String";
    public static final String TYPE_GROUP = "Group";
    public static final String FIT_MODEL_SORT_FIELD = "fitModelSort";
    //Format of productCode: W-brandCode-PproductCode
    public static final String PRODUCT_CODE_FORMAT_JAVASCRIPT = "W-{0}-P{1}";
    public static final String PRODUCT_CODE_FORMAT_JAVA = "W-%d-P%d";
    public static final String STRING_PRODUCT_CODE_FORMAT_JAVA = "W-%d-P%s";
    /**import page. */
    public static final String IMPORT_SPLIT_COMMA = ",";
    public static final String IMPORT_SPLIT_TAB = "/t";

    //BOE @rcv!Luong.Dai 2014/07/09 #9811: modify column number.
    //BOE #7206 Nguyen.Chuong 2014/05/13: modify column number.
    public static final int IMPORTED_MODE_COLUMN_NUMBER = 2;
    public static final int PRODUCT_ID_COLUMN_NUMBER = 4;
    public static final int PRODUCT_SYOUHIN_SYS_CODE_COLUMN_NUMBER = 6;
    public static final int PRODUCT_BRAND_CODE_COLUMN_NUMBER = 8;
    public static final int PRODUCT_BRAND_NAME_COLUMN_NUMBER = 10;
    public static final int PRODUCT_CATEGORY_CODE_COLUMN_NUMBER = 11;
    public static final int PRODUCT_CATEGORY_NAME_COLUMN_NUMBER = 13;
    public static final int PRODUCT_NAME_COLUMN_NUMBER = 14;
    public static final int PRODUCT_CODE_COLUMN_NUMBER = 16;
    public static final int PRODUCT_WEBIKE_CODE_FLG_COLUMN_NUMBER = 18;
    public static final int PRODUCT_EAN_CODE_COLUMN_NUMBER = 20;
    public static final int PRODUCT_PROPER_PRICE_COLUMN_NUMBER = 22;
    public static final int SUPPLIER_PRICE_PRICE_COLUMN_NUMBER = 24;
    public static final int PRODUCT_GROUP_CODE_COLUMN_NUMBER = 26;
    public static final int PRODUCT_SUPPLIER_RELEASE_DATE_COLUMN_NUMBER = 28;
    public static final int PRODUCT_OPEN_PRICE_FLG_COLUMN_NUMBER = 30;
    public static final int PRODUCT_PROPER_SELLING_FLG_COLUMN_NUMBER = 32;
    public static final int PRODUCT_ORDER_PRODUCT_FLG_COLUMN_NUMBER = 34;
    public static final int PRODUCT_NO_RETURNABLE_FLG_COLUMN_NUMBER = 36;
    public static final int PRODUCT_AMBIGUOUS_IMAGE_FLG_COLUMN_NUMBER = 38;
    public static final int DESCRIPTION_SUMMARY_COLUMN_NUMBER = 40;
    public static final int DESCRIPTION_REMARKS_COLUMN_NUMBER = 42;
    public static final int DESCRIPTION_CAUTION_COLUMN_NUMBER = 44;
    public static final int DESCRIPTION_SENTENCE_COLUMN_NUMBER = 46;
    public static final int SIIRE_CODE_1_COLUMN_NUMBER = 48;
    public static final int SIIRE_NAME_1_COLUMN_NUMBER = 50;
    public static final int NOUKI_CODE_1_COLUMN_NUMBER = 51;
    public static final int NOUKI_NAME_1_COLUMN_NUMBER = 53;
    public static final int SIIRE_CODE_2_COLUMN_NUMBER = 54;
    public static final int SIIRE_NAME_2_COLUMN_NUMBER = 56;
    public static final int NOUKI_CODE_2_COLUMN_NUMBER = 57;
    public static final int NOUKI_NAME_2_COLUMN_NUMBER = 59;
    public static final int COMPATIBLE_MODEL_COLUMN_NUMBER = 60;
    public static final int PRODUCT_IMAGE_THUMBNAIL_PATH1_COLUMN_NUMBER = 62;
    public static final int PRODUCT_IMAGE_DETAIL_PATH1_COLUMN_NUMBER = 64;
    public static final int PRODUCT_IMAGE_THUMBNAIL_PATH2_COLUMN_NUMBER = 66;
    public static final int PRODUCT_IMAGE_DETAIL_PATH2_COLUMN_NUMBER = 68;
    public static final int PRODUCT_IMAGE_THUMBNAIL_PATH3_COLUMN_NUMBER = 70;
    public static final int PRODUCT_IMAGE_DETAIL_PATH3_COLUMN_NUMBER = 72;
    public static final int PRODUCT_IMAGE_THUMBNAIL_PATH4_COLUMN_NUMBER = 74;
    public static final int PRODUCT_IMAGE_DETAIL_PATH4_COLUMN_NUMBER = 76;
    public static final int PRODUCT_IMAGE_THUMBNAIL_PATH5_COLUMN_NUMBER = 78;
    public static final int PRODUCT_IMAGE_DETAIL_PATH5_COLUMN_NUMBER = 80;
    public static final int PRODUCT_IMAGE_THUMBNAIL_PATH6_COLUMN_NUMBER = 82;
    public static final int PRODUCT_IMAGE_DETAIL_PATH6_COLUMN_NUMBER = 84;
    public static final int PRODUCT_IMAGE_THUMBNAIL_PATH7_COLUMN_NUMBER = 86;
    public static final int PRODUCT_IMAGE_DETAIL_PATH7_COLUMN_NUMBER = 88;
    public static final int PRODUCT_IMAGE_THUMBNAIL_PATH8_COLUMN_NUMBER = 90;
    public static final int PRODUCT_IMAGE_DETAIL_PATH8_COLUMN_NUMBER = 92;
    public static final int PRODUCT_IMAGE_THUMBNAIL_PATH9_COLUMN_NUMBER = 94;
    public static final int PRODUCT_IMAGE_DETAIL_PATH9_COLUMN_NUMBER = 96;
    public static final int PRODUCT_IMAGE_THUMBNAIL_PATH10_COLUMN_NUMBER = 98;
    public static final int PRODUCT_IMAGE_DETAIL_PATH10_COLUMN_NUMBER = 100;
    public static final int SELECT_CODE1_COLUMN_NUMBER = 102;
    public static final int SELECT_NAME1_COLUMN_NUMBER = 104;
    public static final int SELECT_CODE2_COLUMN_NUMBER = 106;
    public static final int SELECT_NAME2_COLUMN_NUMBER = 108;
    public static final int SELECT_CODE3_COLUMN_NUMBER = 110;
    public static final int SELECT_NAME3_COLUMN_NUMBER = 112;
    public static final int SELECT_CODE4_COLUMN_NUMBER = 114;
    public static final int SELECT_NAME4_COLUMN_NUMBER = 116;
    public static final int SELECT_CODE5_COLUMN_NUMBER = 118;
    public static final int SELECT_NAME5_COLUMN_NUMBER = 120;
    public static final int SELECT_CODE6_COLUMN_NUMBER = 122;
    public static final int SELECT_NAME6_COLUMN_NUMBER = 124;
    public static final int SELECT_CODE7_COLUMN_NUMBER = 126;
    public static final int SELECT_NAME7_COLUMN_NUMBER = 128;
    public static final int SELECT_CODE8_COLUMN_NUMBER = 130;
    public static final int SELECT_NAME8_COLUMN_NUMBER = 132;
    public static final int SELECT_CODE9_COLUMN_NUMBER = 134;
    public static final int SELECT_NAME9_COLUMN_NUMBER = 136;
    public static final int SELECT_CODE10_COLUMN_NUMBER = 138;
    public static final int SELECT_NAME10_COLUMN_NUMBER = 140;
    public static final int CUSTOMERS_CONFIRMATION_ITEM_COLUMN_NUMBER = 142;
    public static final int LINK_COLUMN_NUMBER = 144;
    public static final int ANIMATION_COLUMN_NUMBER = 146;
    public static final int ATTRIBUTE_NAME_COLUMN_NUMBER = 148;
    public static final int ATTRIBUTE_VALUE_COLUMN_NUMBER = 149;
    public static final int ATTRIBUTE_DISPLAY_COLUMN_NUMBER = 150;
    public static final int FIT_MODEL_MAKER_COLUMN_NUMBER = 151;
    public static final int FIT_MODEL_MODEL_COLUMN_NUMBER = 152;
    public static final int FIT_MODEL_STYLE_COLUMN_NUMBER = 153;
    //EOE #7206 Nguyen.Chuong 2014/05/13: modify column number.
    //EOE @rcv!Luong.Dai 2014/07/09 #9811: modify column number.


//    product manage page
    public static final int MAX_PRODUCT_MAINTENANCE_ACTION = 50000;
    public static final int NUMBER_OF_RECORD_INSERT_ONE_TIME = 2000;
    public static final String DEFAULT_SORT_FIELD_PRODUCT_MANAGE = "syouhinSysCode";
    public static final int INT_10000 = 10000;
    public static final int INT_1000 = 1000;
    public static final int INT_5000 = 5000;
    public static final int INT_2000 = 2000;
    public static final int MAX_PRODUCT_EXPORT_CSV = 25000;
    //Max product import one time
    public static final int MAX_PRODUCT_IMPORT_CSV_MODE_1 = 900;

    public static final int REPLACE_ALL =  0;
    public static final int REPLACE_HEAD =  1;
    public static final int REPLACE_TAIL =  2;
    public static final int FIND_REPLACE =  3;
    public static final int IMAGE_CHAR_HEAD =  5;
    public static final int IMAGE_CHAR_TAIL =  6;

    public static final String STR_REPLACE_ALL =  "0";
    public static final String STR_REPLACE_HEAD =  "1";
    public static final String STR_REPLACE_TAIL =  "2";
    public static final String STR_FIND_REPLACE =  "3";

    //Parameter name of delete status
    public static final String DELETE_STATUS = "delStatus";

    //CSV constant
    public static final int ZERO_WIDTH_NO_BREAK_SPACE = 65279;
    public static final String DEFAULT_MODE_EXPORT_CSV = "u";
    public static final String UPDATE_MODE_IMPORT_CSV = "u";
    public static final String NEW_MODE_IMPORT_CSV = "n";
    public static final String DELETE_MODE_IMPORT_CSV = "d";
    public static final String PREFIX_ERROR_COLUMN = "_エラー";
    public static final String ERROR_COLUMN_FIELD = "errorAllMess";

    // #9811 Nguyen.Chuong 2014/07/10: seperator of each error message when import CSV: [ERROR] message1 / message2
    public static final String SEPARATOR_ERROR_MESSAGE = " / ";
    //#9811 Nguyen.Chuong 2014/07/10: prefix of error message when import CSV: [ERROR] message1 / message2
    public static final String PREFIX_ERROR_MESSAGE = "[ERROR]";

    //Tmp_image
    public static final int IMAGE_TMP_HEAD = 0;
    public static final int IMAGE_TMP_TAIL = 9;

    public static final String NEW_PRODUCT = "新規";

    //File reader buffer
    public static final int FILE_READER_BUFFER = 4096;

    //Encoding
    public static final String UTF_8_ENCODE 		= "UTF-8";
    public static final String SHIFT_JIS_ENCODE 	= "SHIFT_JIS";
    public static final String MS932_ENCODE 	= "MS932";

    //Product edit block 15
    public static final int DELETE_SUCCESS = 1;
    public static final int DELETE_FAIL = 0;
    public static final int NO_PRODUCT_TO_DELETE = -1;

    //Tab character
    public static final char TAB_CHARACTER = '\t';
    public static final String STR_TAB_CHARACTER = "\\t";

    //Status read contains csv file
    public static final String STATUS_READ_CSV_CONTAINS = "0";
    public static final String STATUS_READ_CSV_WRONG_HEADER = "0";
    public static final String STATUS_READ_CSV_MAXIMUN_25000 = "1";
    public static final String STATUS_READ_CSV_MAXIMUN_25000_AFTER_IMPORT = "2";
    public static final String STATUS_READ_CSV_WRONG_DATA = "3";
    public static final String LIST_CSV_READ_FROM_CSV = "1";

    //Set default sort to fit model maker
    public static final String SORT_FIELD_FIT_MODEL_MAKER = "fit_model_maker";

    //remove string in select name
    public static final String PLEASE_CHOISE_OPTION = "を選択してください";
    public static final String SPLIT_SELECT_INFO = "|";
    public static final String SPLIT_SELECT_CODE_SELECT_NAME = "_";
    public static final String NO_LOGO_OF_MAKER = "汎用";

    // Error message import CSV.
    public static final String CSV_TEMP_FILE = "temp";
    public static final String CSV_FILE_EXTENSION = ".csv";
    public static final String QUOTE_SYMBOL = "\"";
    public static final String PREFIX_PRODUCT_ID = "P";

    public static final String ERROR = "_error";
    public static final String CSV_DECIMAL = "E";
    /*public static final String CSV_FILE = ".csv";*/
    
    //EOE @rcv!Tran.Toan 2014/07/17 #9961: option name column when import csv file.
    public static final int OPTION_NAME1 = 1;
    public static final int OPTION_NAME2 = 2;
    public static final int OPTION_NAME3 = 3;
    public static final int OPTION_NAME4 = 4;
    public static final int OPTION_NAME5 = 5;
    public static final int OPTION_NAME6 = 6;
    public static final int OPTION_NAME7 = 7;
    public static final int OPTION_NAME8 = 8;
    public static final int OPTION_NAME9 = 9;
    public static final int OPTION_NAME10 = 10;
    //EOE @rcv!Tran.Toan 2014/07/17 #9961: option name column when import csv file.
    public static final String PREFIX_SIIRE_OR_NOUKI_CODE = "0";
    public static final int MAX_COLUMN_FITMODEL = 3;
    public static final int MAX_COLUMN_CONFIRM = 2;
    public static final int MAX_COLUMN_LINK = 3;
    public static final int MAX_COLUMN_VIDEO = 3;
    //BOE @rcv!dau.phuong 2014/07/25 #9955: calibration request individual code.
    public static final String CALIBRATION_INDIVIDUAL_CODE = "individualCode";
    //EOE @rcv!dau.phuong 2014/07/25 #9955: calibration request individual code.
    public static final String REGEX_NUMBER_1BYTE_2BYTE = "^[^0-9０-９]$";
    public static final String REGEX_NUMBER_1BYTE_2BYTE_REPLACE = "/[^0-9０-９]/g";
    public static final String REGEX_DECIMAL_PERCENT_NUMBER_REPLACE = "/[^0-9.%]/g";
    public static final String REGEX_DECIMAL_PERCENT_NUMBER = "^\\d{1,2}(\\.\\d{1,2})?(\\%)?$";
    public static final String REGEX_NONE_SPECIAL_CHAR = "[-〜‖−¢£¬]";
    
    public static final String REGEX_NUMBER_2BYTE = "[^0-9０-９]";
    public static final String REGEX_NUMBER_2BYTE_SIIRE_ORDER = "[^1-2１-２]";
    public static final String REGEX_NUMBER_2BYTE_PERCENT = "[^0-9０-９.%。％]";
    public static final String REGEX_NUMBER_2BYTE_0 = "[０]";
    public static final String REGEX_NUMBER_2BYTE_1 = "[１]";
    public static final String REGEX_NUMBER_2BYTE_2 = "[２]";
    public static final String REGEX_NUMBER_2BYTE_3 = "[３]";
    public static final String REGEX_NUMBER_2BYTE_4 = "[４]";
    public static final String REGEX_NUMBER_2BYTE_5 = "[５]";
    public static final String REGEX_NUMBER_2BYTE_6 = "[６]";
    public static final String REGEX_NUMBER_2BYTE_7 = "[７]";
    public static final String REGEX_NUMBER_2BYTE_8 = "[８]";
    public static final String REGEX_NUMBER_2BYTE_9 = "[９]";
    public static final String REGEX_NUMBER_2BYTE_DOT_CHAR = "[。]";
    public static final String REGEX_NUMBER_2BYTE_PERCENT_CHAR = "[％]";
}
