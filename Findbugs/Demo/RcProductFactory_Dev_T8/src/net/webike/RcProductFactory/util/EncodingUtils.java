package net.webike.RcProductFactory.util;


/*
 * 2013 Rivercrane.All Rights Reserved.
 */

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.mozilla.universalchardet.UniversalDetector;

/**
 * Encoding utils.
 * @author hoang.pham
 *
 */
public final class EncodingUtils {
	public static final int NUMBER_16 = 16;

	/**
	 */
	private EncodingUtils() {
	}
	/**
     * 引数の文字列(UTF-8)を、Shift_JISにエンコードする。.
     * @param value 変換対象の文字列
     * @return エンコードされた文字列
     * @throws UnsupportedEncodingException UnsupportedEncodingException
     */
	public static String utf8ToSjis(String value)
			throws UnsupportedEncodingException {
		byte[] srcStream = value.getBytes("UTF-8");
		value = convert(new String(srcStream, "UTF-8"), "UTF-8", "SJIS");
		/* BOE @rcv! Luong.Dai 2014/09/04: don't replace any special character */
		/*value = utf8ToSjisCharacter(value);*/
		/* EOE @rcv! Luong.Dai 2014/09/04: don't replace any special character */
		byte[] destStream = value.getBytes("SJIS");
		value = new String(destStream, "SJIS");
		return value;
	}

    /**
     * 引数の文字列(Shift_JIS)を、UTF-8にエンコードする。.
     * @param value 変換対象の文字列
     * @return エンコードされた文字列
     * @throws UnsupportedEncodingException UnsupportedEncodingException
     */
	public static String sjisToUtf8(String value)
			throws UnsupportedEncodingException {
		/* BOE @rcv! Luong.Dai 2014/09/06: Change encode from sjis to ms932 */
		/*byte[] srcStream = value.getBytes("SJIS");
		byte[] destStream = (new String(srcStream, "SJIS")).getBytes("UTF-8");*/
		byte[] srcStream = value.getBytes(Constant.MS932_ENCODE);
		byte[] destStream = (new String(srcStream, Constant.MS932_ENCODE)).getBytes("UTF-8");
		/* EOE @rcv! Luong.Dai 2014/09/06: Change encode from sjis to ms932 */
		value = new String(destStream, "UTF-8");
		value = convert(value, "SJIS", "UTF-8");
		/* BOE @rcv! Luong.Dai 2014/09/04: don't replace any special character */
		/*value = sjisToUtf8Character(value);*/
		/* EOE @rcv! Luong.Dai 2014/09/04: don't replace any special character */
		return value;
	}

    /**
     * 引数の文字列を、エンコードする。.
     * @param value 変換対象の文字列
     * @param src 変換前の文字コード
     * @param dest 変換後の文字コード
     * @return エンコードされた文字列
     * @throws UnsupportedEncodingException UnsupportedEncodingException
     * @author Tri.Nguyen
     */
	private static String convert(String value, String src, String dest)
			throws UnsupportedEncodingException {
		Map<String, String> conversion = createConversionMap(src, dest);
		char oldChar;
		char newChar;
		String key;
		Set<Entry<String, String>> entries = conversion.entrySet();
        Iterator<Entry<String, String>> entryIter = entries.iterator();
        while (entryIter.hasNext()) {
        	Entry<String, String> entry = entryIter.next();
        	key = (String) entry.getKey();  // Get the key from the entry.
        	oldChar = toChar(key);
        	newChar = toChar(conversion.get(key));
        	value = value.replace(oldChar, newChar);
        }
		return value;
	}

    /**
     * 16進表記の文字を取得する。.
     * @param value 変換対象の文字列
     * @return 16進表記の文字
     */
	private static char toChar(String value) {
		return (char) Integer.parseInt(value.trim().substring("U+".length()),
				NUMBER_16);
	}

    /**
    * エンコード情報を作成する.
    * @param src 変換前の文字コード
    * @param dest 変換後の文字コード
    * @return エンコードされた文字列
    * @throws UnsupportedEncodingException UnsupportedEncodingException
    */
	private static Map<String, String> createConversionMap(String src,
			String dest) throws UnsupportedEncodingException {
		Map<String, String> conversion = new HashMap<String, String>();
		if ((src.equals("UTF-8")) && (dest.equals("SJIS"))) {
			// −（全角マイナス）
			conversion.put("U+FF0D", "U+2212");
			// 〜（全角チルダ）
			conversion.put("U+FF5E", "U+301C");
			// ¢（セント）
			conversion.put("U+FFE0", "U+00A2");
			// £（ポンド）
			conversion.put("U+FFE1", "U+00A3");
			// ¬（ノット）
			conversion.put("U+FFE2", "U+00AC");
			// ―（全角マイナスより少し幅のある文字）
			conversion.put("U+2015", "U+2014");
			// ‖（半角パイプが2つ並んだような文字）
			conversion.put("U+2225", "U+2016");

		} else if ((src.equals("SJIS")) && (dest.equals("UTF-8"))) {
			// −（全角マイナス）
			conversion.put("U+2212", "U+FF0D");
			// 〜（全角チルダ）
			conversion.put("U+301C", "U+FF5E");
			// ¢（セント）
			conversion.put("U+00A2", "U+FFE0");
			// £（ポンド）
			conversion.put("U+00A3", "U+FFE1");
			// ¬（ノット）
			conversion.put("U+00AC", "U+FFE2");
			// ―（全角マイナスより少し幅のある文字）
			conversion.put("U+2014", "U+2015");
			// ‖（半角パイプが2つ並んだような文字）
			conversion.put("U+2016", "U+2225");

		} else {
			throw new UnsupportedEncodingException("この文字コードはサポートしていません。\n・src="
					+ src + ",dest=" + dest);
		}
		return conversion;
	}

	/**
	 * 入力された文字列中の文字を、無難な文字列に置き換える。.
	 * @param s String
	 * @return 無難な文字列
	 */
	public static String utf8ToSjisCharacter(String s) {
		if (s == null) {
			return null;
		}
		s = s.replace("U+2460", "(1)");
		s = s.replace("U+2461", "(2)");
		s = s.replace("U+2462", "(3)");
		s = s.replace("U+2463", "(4)");
		s = s.replace("U+2464", "(5)");
		s = s.replace("U+2465", "(6)");
		s = s.replace("U+2466", "(7)");
		s = s.replace("U+2467", "(8)");
		s = s.replace("U+2468", "(9)");
		s = s.replace("U+2469", "(10)");
		s = s.replace("U+246a", "(11)");
		s = s.replace("U+246b", "(12)");
		s = s.replace("U+246c", "(13)");
		s = s.replace("U+246d", "(14)");
		s = s.replace("U+246e", "(15)");
		s = s.replace("U+246f", "(16)");
		s = s.replace("U+2470", "(17)");
		s = s.replace("U+2471", "(18)");
		s = s.replace("U+2472", "(19)");
		s = s.replace("U+2473", "(20)");
		return s;
	}

	/**
	 * 入力された文字列中の文字を、無難な文字列に置き換える。.
	 * @param s String
	 * @return 無難な文字列
	 */
	public static String sjisToUtf8Character(String s) {
		if (s == null) {
			return null;
		}
		s = s.replace("(1)", "U+2460");
		s = s.replace("(2)", "U+2461");
		s = s.replace("(3)", "U+2462");
		s = s.replace("(4)", "U+2463");
		s = s.replace("(5)", "U+2464");
		s = s.replace("(6)", "U+2465");
		s = s.replace("(7)", "U+2466");
		s = s.replace("(8)", "U+2467");
		s = s.replace("(9)", "U+2468");
		s = s.replace("(10)", "U+2469");
		s = s.replace("(11)", "U+246a");
		s = s.replace("(12)", "U+246b");
		s = s.replace("(13)", "U+246c");
		s = s.replace("(14)", "U+246d");
		s = s.replace("(15)", "U+246e");
		s = s.replace("(16)", "U+246f");
		s = s.replace("(17)", "U+2470");
		s = s.replace("(18)", "U+2471");
		s = s.replace("(19)", "U+2472");
		s = s.replace("(20)", "U+2473");
		return s;
	}

	/************************************************************************
	 * <b>Description:</b><br>
	 *  Convert file from shift-jis to utf-8.
	 *
	 * @author		Luong.Dai
	 * @date		Jun 2, 2014
	 * @param 		fileConvertName		File to convert
	 * @param		fileResultName		File result
	 * @return		boolean				Convert result
	 ************************************************************************/
	public static boolean convertFileFromShiftJisToUTF8(String fileConvertName, String fileResultName) {
		boolean result = false;
		BufferedReader bufferReader = null;
        InputStream fileStream = null;
        Reader readerDecoder = null;

        PrintWriter out = null;
        // type of file
		try {
			File fileResult = new File(fileResultName);
			//Set encode of file result if UTF-8
            out = new PrintWriter(fileResult, Constant.UTF_8_ENCODE);
            //Current readed line
            String line = "";
            String lineUtf8 = "";

            fileStream = new FileInputStream(fileConvertName);
            readerDecoder = new InputStreamReader(fileStream, Constant.SHIFT_JIS_ENCODE);
            bufferReader = new BufferedReader(readerDecoder);
            // read file line by line
            while ((line = bufferReader.readLine()) != null) {
            	//Convert to UTF-8
            	lineUtf8 = EncodingUtils.sjisToUtf8(line);
            	//Print line with encode UTF-8
             	out.print(lineUtf8);
             	out.print("\n");
            }
            //Close bufer
            if (bufferReader != null) {
				bufferReader.close();
			}
            if (readerDecoder != null) {
            	readerDecoder.close();
            }
            if (fileStream != null) {
            	fileStream.close();
            }
            if (out != null) {
                 out.flush();
                 out.close();
            }
            result = true;
		} catch (IOException ex) {
			result = false;
		}

		return result;
	}

    /************************************************************************
     * <b>Description:</b><br>
     *  read format file.
     *
     * @author		Tran.Thanh
     * @date		Jun 2, 2014
     * @param 		fileStream file to read
     * @return		String File format (UTF-8, SHIFT_JIS,...)
     ************************************************************************/
    public static String readEncodeFile(InputStream fileStream) {
    	try {
            byte[] buf = new byte[Constant.FILE_READER_BUFFER];
            UniversalDetector detector = new UniversalDetector(null);

            int nread  = fileStream.read(buf);
            while (nread > 0 && !detector.isDone()) {
            	detector.handleData(buf, 0, nread);
            	nread = fileStream.read(buf);
            }
            detector.dataEnd();

            String encoding = detector.getDetectedCharset();

            detector.reset();
            //EOE Tran.Thanh : validate format csv

            return encoding;
	    } catch (Exception e) { e.printStackTrace();
	        return "";
	    }
    }
}
