/************************************************************************
 * file name	： RedmineUtils.java
 * author		： Le.Dinh
 * version		： 1.0.0
 * date created	： 13 Jan 2014
 * date updated	： 13 Jan 2014
 * description	： Class process all for Redmine API
 *
 ***********************************************************************/
package net.webike.RcProductFactory.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONException;

import com.taskadapter.redmineapi.RedmineException;
import com.taskadapter.redmineapi.RedmineFormatException;
import com.taskadapter.redmineapi.bean.Issue;
import com.taskadapter.redmineapi.bean.Membership;
import com.taskadapter.redmineapi.bean.Role;
import com.taskadapter.redmineapi.bean.User;
import com.taskadapter.redmineapi.internal.RedmineJSONParser;
import com.taskadapter.redmineapi.internal.json.JsonObjectParser;

/**
 * Class process all for Redmine API.
 */
public final class RedmineUtils {
    /**---------------------------------------------------------------------------
     * private constructor.
     */
    private RedmineUtils() { };

    /**
     * Logger.
     */
    private static final Log  LOGGER = LogFactory.getLog(RedmineUtils.class);

	/************************************************************************
	 * <b>Description:</b><br>
	 *  getResultHttpsConWithUser.
	 *
	 * @author		Le.Dinh
	 * @date		2014/01/13
	 * @param 		userName	String
	 * @param 		passWord	String
	 * @param 		fullUrl		String
	 * @param 		rootUrl 	String
	 * @return		HttpsURLConnection
	 * @throws 		RedmineException	RedmineException
	 ************************************************************************/
	public static HttpsURLConnection getResultHttpsConWithUser(String userName,
															   String passWord,
															   String fullUrl,
															   final String rootUrl)
															   throws RedmineException {
		HttpsURLConnection httpsCon = null;
		try {
			String name = userName;
			String password = passWord;

			String authString = name + ":" + password;
			byte[] authEncBytes = Base64.encodeBase64(authString.getBytes("UTF-8"));
			String authStringEnc = new String(authEncBytes, "UTF-8");

			URL url = new URL(fullUrl);

			httpsCon = (HttpsURLConnection) url
					.openConnection();
			httpsCon.setRequestProperty("Authorization", "Basic "
					+ authStringEnc);
			httpsCon.setHostnameVerifier(new HostnameVerifier() {
				public boolean verify(String hostname, SSLSession session) {
					return rootUrl.equals(hostname);
				}
			});
		} catch (MalformedURLException e) {
			LOGGER.error("error when malformed URL has occurred. ");
		} catch (IOException e) {
			LOGGER.error("I/O exception of some sort has occurred. ");
		}
		return httpsCon;
	}

	/************************************************************************
	 * <b>Description:</b><br>
	 *  getResultHttpsConWithApiKey.
	 *
	 * @author		Le.Dinh
	 * @date		2014/01/13
	 * @param 		apiKey	String
	 * @param 		fullUrl	String
	 * @param 		rootUrl String
	 * @return		HttpsURLConnection	HttpsURLConnection
	 * @throws 		RedmineException	RedmineException
	 ************************************************************************/
	public static HttpsURLConnection getResultHttpsConWithApiKey(String apiKey, String fullUrl, final String rootUrl) throws RedmineException {
		HttpsURLConnection httpsCon = null;
		try {

			fullUrl += "&key=" + apiKey;
			URL url = new URL(fullUrl);

			httpsCon = (HttpsURLConnection) url
					.openConnection();
			httpsCon.setHostnameVerifier(new HostnameVerifier() {
				public boolean verify(String hostname, SSLSession session) {
					return rootUrl.equals(hostname);
				}
			});
		} catch (MalformedURLException e) {
			LOGGER.error("error when malformed URL has occurred. ");
		} catch (IOException e) {
			LOGGER.error("I/O exception of some sort has occurred. ");
		}
		return httpsCon;
	}

	/************************************************************************
	 * <b>Description:</b><br>
	 *  parseJsonUserRedmine.
	 *
	 * @author		Le.Dinh
	 * @date		2014/01/13
	 * @param 		httpsCon	HttpsURLConnection
	 * @return		com.taskadapter.redmineapi.bean.User
	 * @throws 		IOException	IOException
	 * @throws 		RedmineFormatException	RedmineFormatException
	 ************************************************************************/
	public static User getJsonUserRedmine(HttpsURLConnection httpsCon) throws IOException, RedmineFormatException {
		BufferedReader br = new BufferedReader(new InputStreamReader(httpsCon.getInputStream(), "UTF-8"));
        StringBuilder sb = new StringBuilder();
        String line;
        while ((line = br.readLine()) != null) {
            sb.append(line + "\n");
        }
        br.close();
       User userRedmine  = parseResponse(sb.toString(), "user", RedmineJSONParser.USER_PARSER);
       return userRedmine;
	}

	/************************************************************************
	 * <b>Description:</b><br>
	 *  getJsonIssuesRedmine.
	 *
	 * @author		Le.Dinh
	 * @date		18 Jan 2014
	 * @param 		httpsCon	HttpsURLConnection
	 * @return		Issue
	 * @throws 		IOException	Issue
	 * @throws 		RedmineFormatException	RedmineFormatException
	 ************************************************************************/
	public static Issue getJsonIssuesRedmine(HttpsURLConnection httpsCon) throws IOException, RedmineFormatException {
		BufferedReader br = new BufferedReader(new InputStreamReader(httpsCon.getInputStream(), "UTF-8"));
        StringBuilder sb = new StringBuilder();
        String line;
        while ((line = br.readLine()) != null) {
            sb.append(line + "\n");
        }
        br.close();
       Issue issue = parseResponse(sb.toString(), "issue", RedmineJSONParser.ISSUE_PARSER);
       return issue;
	}

	/************************************************************************
	 * <b>Description:</b><br>
	 *  parseResponse.
	 *
	 * @author		Le.Dinh
	 * @date		2014/01/13
	 * @param 		response	String
	 * @param 		tag	String
	 * @param 		parser	JsonObjectParser<T>
	 * @param 		<T>	T
	 * @return		<T>	T
	 * @throws 		RedmineFormatException RedmineFormatException
	 ************************************************************************/
	public static <T> T parseResponse(String response, String tag,
	                JsonObjectParser<T> parser) throws RedmineFormatException {
	try {
	return parser.parse(RedmineJSONParser.getResponseSingleObject(response, tag));
	} catch (JSONException e) {
		LOGGER.error("have error when parseResponse");
		throw new RedmineFormatException(e);
	}
	}

	/************************************************************************
	 * <b>Description:</b><br>
	 *  getListRolebyProjectId.
	 *
	 * @author		Le.Dinh
	 * @date		2014/01/13
	 * @param 		projectId	int
	 * @param 		userRedmine	User
	 * @return		List<Integer>
	 ************************************************************************/
	public static List<String> getListRolebyProjectId(String projectId, User userRedmine) {
		List<String> listRoleUser = new ArrayList<String>();
		for (Membership membership : userRedmine.getMemberships()) {
			if (String.valueOf(membership.getProject().getId()).equals(projectId)) {
				for (Role role : membership.getRoles()) {
					listRoleUser.add(String.valueOf(role.getId()));
				}
			}
		}
		return listRoleUser;
	}

	static {
		try {
			TrustManager[] trustAllCerts = {new X509TrustManager() {
				public X509Certificate[] getAcceptedIssuers() {
					return null;
				}

				public void checkClientTrusted(X509Certificate[] certs,
						String authType) {
					// Do nothing
				}

				public void checkServerTrusted(X509Certificate[] certs,
						String authType) {
					// Do nothing
				}
			} };
			SSLContext sc = SSLContext.getInstance("SSL");

			HostnameVerifier hv = new HostnameVerifier() {
				public boolean verify(String arg0, SSLSession arg1) {
					return true;
				}
			};
			sc.init(null, trustAllCerts, new SecureRandom());

			HttpsURLConnection
					.setDefaultSSLSocketFactory(sc.getSocketFactory());
			HttpsURLConnection.setDefaultHostnameVerifier(hv);
		} catch (RuntimeException e) {
		    throw e;
		} catch (Exception localException) {
			LOGGER.error("have error when checkClientTrusted");
		}
	}

}
