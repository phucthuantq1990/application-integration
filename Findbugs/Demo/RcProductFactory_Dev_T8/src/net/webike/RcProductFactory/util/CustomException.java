/************************************************************************
 * file name	： UpdateDataFalseException.java
 * author		： Luong.Dai
 * version		： 1.0.0
 * date created	： Jan 21, 2014
 * date updated	： Jan 21, 2014
 * description	： Custom exception
 *
 ***********************************************************************/
package net.webike.RcProductFactory.util;

/**
 * Replace your description for this class here.
 */
public class CustomException extends RuntimeException {

    /**
     * @description.
     */
    private static final long serialVersionUID = 1L;

    private Object obj;

    /**
     * Set object to exception.
     * @param obj Object
     */
    public CustomException(final Object obj) {
        this.obj = obj;
    }

    /**
     * @return the obj
     */
    public Object getObj() {
        return obj;
    }

    /**
     * @param obj the obj to set
     */
    public void setObj(Object obj) {
        this.obj = obj;
    }
}
