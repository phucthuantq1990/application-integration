package net.webike.RcProductFactory.util;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.charset.CharacterCodingException;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;

/**
 *
 * @author Georgios Migdos <cyberpython@gmail.com>
 * edit by rcv! Luong.Dai 2014/09/10
 * source url: http://www.java2s.com/Code/Java/I18N/Howtoautodetectafilesencoding.htm
 */
public final class CharsetDetectorUtils {

	/**
	 * Default constructor
	 */
	private CharsetDetectorUtils() {
	}
	
    /************************************************************************
     * <b>Description:</b><br>
     *  source url: http://www.java2s.com/Code/Java/I18N/Howtoautodetectafilesencoding.htm.
     *
     * @author		Luong.Dai
     * @date		Sep 10, 2014
     * @param 		f			File to check
     * @param 		charsets	List charsets to check
     * @return		Charset		Charset result
     ************************************************************************/
    public static Charset detectCharset(File f, String[] charsets) {
        Charset charset = null;

        for (String charsetName : charsets) {
            charset = detectCharset(f, Charset.forName(charsetName));
            if (charset != null) {
                break;
            }
        }

        return charset;
    }

    /************************************************************************
     * <b>Description:</b><br>
     * source url: http://www.java2s.com/Code/Java/I18N/Howtoautodetectafilesencoding.htm.
     * Detect charset of file
     *
     * @author		Luong.Dai
     * @date		Sep 10, 2014
     * @param 		f				File to check
     * @param 		charset			charset to check
     * @return		Charset			Charset result
     ************************************************************************/
    public static Charset detectCharset(File f, Charset charset) {
        try {
            BufferedInputStream input = new BufferedInputStream(new FileInputStream(f));

            CharsetDecoder decoder = charset.newDecoder();
            decoder.reset();

            byte[] buffer = new byte[Constant.FILE_READER_BUFFER];
            boolean identified = false;
            while ((input.read(buffer) != -1) && (!identified)) {
                identified = identify(buffer, decoder);
            }

            input.close();

            if (identified) {
                return charset;
            } else {
                return null;
            }
        } catch (IOException e) {
            return null;
        }
    }

    /************************************************************************
     * <b>Description:</b><br>
     * source url: http://www.java2s.com/Code/Java/I18N/Howtoautodetectafilesencoding.htm.
     * Detect charset of bytes array
     *
     * @author		Luong.Dai
     * @date		Sep 10, 2014
     * @param 		bytes		Array to check
     * @param 		decoder		decoder to check
     * @return		boolean		encode of file match with decoder
     ************************************************************************/
    public static boolean identify(byte[] bytes, CharsetDecoder decoder) {
        try {
            decoder.decode(ByteBuffer.wrap(bytes));
        } catch (CharacterCodingException e) {
            return false;
        }
        return true;
    }
    
    /************************************************************************
     * <b>Description:</b><br>
     * Get charset of file.
     * Support check UTF-8, MS932 and SHIFT_JIS charset.
     *
     * @author		Luong.Dai
     * @date		Sep 10, 2014
     * @param 		filePath		File to get charset
     * @return		String			Charset of file
     ************************************************************************/
    public static String detectFileCharset(String filePath) {
    	String charsetName = "";

    	//File to detect
    	File f = new File(filePath);
    	//List charset need to detect
    	String[] charsetsToBeTested = {Constant.UTF_8_ENCODE, Constant.MS932_ENCODE, Constant.SHIFT_JIS_ENCODE};
    	//Detect charset of file
        Charset charset = detectCharset(f, charsetsToBeTested);

        //Get charset and return
        if (charset != null) {
        	Charset sjisCharset = Charset.forName(Constant.SHIFT_JIS_ENCODE);
        	Charset ms932Charset = Charset.forName(Constant.MS932_ENCODE);
        	Charset utf8Charset = Charset.forName(Constant.UTF_8_ENCODE);
        	//Check charset name
        	if (charset.equals(sjisCharset)) {
        		charsetName = Constant.SHIFT_JIS_ENCODE;
        	} else if (charset.equals(ms932Charset)) {
        		charsetName = Constant.MS932_ENCODE;
        	} else if (charset.equals(utf8Charset)) {
        		charsetName = Constant.UTF_8_ENCODE;
        	}
        }

        return charsetName;
    }
}
