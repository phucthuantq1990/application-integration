/************************************************************************
 * File Name    ： ValidateUtils.java
 * Author       ： Nguyen.Chuong
 * Version      ： 1.0.0
 * Date Created ： 2013/10/24
 * Date Updated ： 2013/10/24
 * Description  ： Common validate function of productFactory.
 *
 ***********************************************************************/
package net.webike.RcProductFactory.util;

import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;

import net.webike.RcProductFactory.entity.EntMstFactoryUserTmp;
import net.webike.RcProductFactory.entity.UserStatus;

import org.apache.commons.lang.StringUtils;

/**
 * Common validate function of productFactory.
 */
public final class ValidateUtils {
    /**---------------------------------------------------------------------------
     * Private constructor.
     */
    private ValidateUtils() { };

    /************************************************************************
     * <b>Description:</b><br>
     *  validate string email:
     *  -Invalid(false): null, "" or invalid format.
     *  -Valid(true): valid format.
     *
     * @author      Nguyen.Chuong
     * @date        Oct 8, 2013
     * @param       input String
     * @return      boolean
     ************************************************************************/
    public static boolean checkEmail(String input) {
        if (input == null || input.equals("")) {
            return false;
        }
        input =  input.trim();
        //email regex
        String mailchecker = Constant.EMAIL; //"[\\w\\.\\-]+@([\\w\\-]+\\.)+[\\w\\-]+";
        if (!input.matches(mailchecker)) {
            return false;
        }
        return true;
    }

    /************************************************************************
     * <b>Description:</b><br>
     *  validate entity EntMstFactoryUser.
     *
     * @author      Nguyen.Chuong
     * @date        Oct 8, 2013
     * @param       entUser EntMstFactoryUser
     * @return      boolean
     ************************************************************************/
//    public static boolean validateUserEnt(EntMstFactoryUser entUser) {
//        if (null == entUser) {
//            return false;
//        }
//
//        String userId = entUser.getUserId().trim();
//        String password = entUser.getUserPasswd().trim();
//        String passwordConfirm = entUser.getUserPasswdConfirm().trim();
//        String firstName =  entUser.getUserLastName().trim();
//        String lastName = entUser.getUserFirstName().trim();
//        String authority = entUser.getUserAuthority().trim();
//        String delFlag = entUser.getDelFlg().trim();
//        String email1 = entUser.getUserEmail1().trim();
//        String email2 = entUser.getUserEmail2().trim();
//
//        //Validate userId: 12 characters, numbers(char, number 1byte)
//        if (StringUtils.isNotEmpty(userId) && (userId.length() > Constant.USER_ID_MAX_LENGTH
//                                                || !userId.matches(Constant.LETTERONLY))) {
//            return false;
//        }
//        //Validate firsName: 10 characters, numbers(char, numbers 1byte, 2bytes)
//        if (StringUtils.isNotEmpty(firstName) && (firstName.length() > Constant.USER_NAME_MAX_LENGTH
//                                                    || !firstName.matches(Constant.LETTERANDCPACEONLY))) {
//            return false;
//        }
//        //Validate lastName: 10 characters, numbers(char, numbers 1byte, 2bytes)
//        if (StringUtils.isNotEmpty(lastName) && (lastName.length() > Constant.USER_NAME_MAX_LENGTH
//                                                    || !lastName.matches(Constant.LETTERANDCPACEONLY))) {
//            return false;
//        }
//        //Validate email: 80 characters, numbers(char, number 1byte) mail format : xxx@xx.xx
//        if (StringUtils.isNotEmpty(email1) && (!checkEmail(email1)
//                                                    || email1.length() > Constant.EMAIL_MAX_LENGTH)) {
//            return false;
//        }
//        if (StringUtils.isNotEmpty(email2) && (!checkEmail(email2)
//                                                    || email2.length() > Constant.EMAIL_MAX_LENGTH)) {
//            return false;
//        }
//        //Validate authority: value in authority eNum
//        if (StringUtils.isNotEmpty(authority) && (UserAuthority.getName(authority).isEmpty())) {
//            return false;
//        }
//        //Validate password: >=5 and <=8 characters, numbers(char, number 1byte)
//        if (StringUtils.isNotEmpty(password) && (Constant.PASSWORD_MIN_LENGTH > password.length()
//                                                         || password.length() > Constant.PASSWORD_MAX_LENGTH
//                                                         || !password.matches(Constant.LETTERONLY))) {
//            return false;
//        }
//        //Validate passwordConfirm: >=5 and <=8 characters, numbers(char, number 1byte)
//        if (StringUtils.isNotEmpty(passwordConfirm) && (Constant.PASSWORD_MIN_LENGTH > passwordConfirm.length()
//                                                                || passwordConfirm.length() > Constant.PASSWORD_MAX_LENGTH
//                                                                || !passwordConfirm.matches(Constant.LETTERONLY))) {
//            return false;
//        }
//        //Validate passwordConfirm match with password if password notNull
//        if (StringUtils.isNotEmpty(password) && !passwordConfirm.equals(password)) {
//            return false;
//        }
//        //Validate delFlag not null
//        if (StringUtils.isNotEmpty(delFlag) && UserStatus.getName(delFlag).isEmpty()) {
//            return false;
//        }
//        return true;
//    }

    /************************************************************************
     * <b>Description:</b><br>
     *  validate entity EntMstFactoryUserTmp.
     *
     * @author      Tran.Thanh
     * @date        Oct 8, 2013
     * @param       entUser EntMstFactoryUserTmp
     * @return      boolean
     ************************************************************************/
    public static boolean validateUserEnt(EntMstFactoryUserTmp entUser) {
        if (null == entUser) {
            return false;
        }

        String userId = entUser.getUserId().trim();
        String password = entUser.getUserPasswd().trim();
        String passwordConfirm = entUser.getUserPasswdConfirm().trim();
        String firstName =  entUser.getUserLastName().trim();
        String lastName = entUser.getUserFirstName().trim();
        String delFlag = entUser.getUserDelFlg().trim();
        String email = entUser.getUserEmail().trim();

        //Validate userId: 12 characters, numbers(char, number 1byte)
        if (StringUtils.isNotEmpty(userId) && (userId.length() > Constant.USER_ID_MAX_LENGTH
                                                || !userId.matches(Constant.LETTERONLY))) {
            return false;
        }
        //Validate firsName: 10 characters, numbers(char, numbers 1byte, 2bytes)
        if (StringUtils.isNotEmpty(firstName) && (firstName.length() > Constant.USER_NAME_MAX_LENGTH
                                                    || !firstName.matches(Constant.LETTERANDCPACEONLY))) {
            return false;
        }
        //Validate lastName: 10 characters, numbers(char, numbers 1byte, 2bytes)
        if (StringUtils.isNotEmpty(lastName) && (lastName.length() > Constant.USER_NAME_MAX_LENGTH
                                                    || !lastName.matches(Constant.LETTERANDCPACEONLY))) {
            return false;
        }
        //Validate email: 80 characters, numbers(char, number 1byte) mail format : xxx@xx.xx
        if (StringUtils.isNotEmpty(email) && (!checkEmail(email)
                                                    || email.length() > Constant.EMAIL_MAX_LENGTH)) {
            return false;
        }
        //Validate password: >=5 and <=8 characters, numbers(char, number 1byte)
        if (StringUtils.isNotEmpty(password) && (Constant.PASSWORD_MIN_LENGTH > password.length()
                                                         || password.length() > Constant.PASSWORD_MAX_LENGTH
                                                         || !password.matches(Constant.LETTERONLY))) {
            return false;
        }
        //Validate passwordConfirm: >=5 and <=8 characters, numbers(char, number 1byte)
        if (StringUtils.isNotEmpty(passwordConfirm) && (Constant.PASSWORD_MIN_LENGTH > passwordConfirm.length()
                                                                || passwordConfirm.length() > Constant.PASSWORD_MAX_LENGTH
                                                                || !passwordConfirm.matches(Constant.LETTERONLY))) {
            return false;
        }
        //Validate passwordConfirm match with password if password notNull
        if (StringUtils.isNotEmpty(password) && !passwordConfirm.equals(password)) {
            return false;
        }
        //Validate delFlag not null
        if (StringUtils.isNotEmpty(delFlag) && UserStatus.getName(delFlag).isEmpty()) {
            return false;
        }
        return true;
    }

    /************************************************************************
     * <b>Description:</b><br>
     *  validate checkSizeOfImageFileS.
     *
     * @author      Le.Dinh
     * @date        2014/01/02
     * @param       file File
     * @param       width int
     * @param       height int
     * @return      boolean
     * @throws Exception		boolean
     ************************************************************************/
    public static boolean checkSizeOfImageFileS(File file, int width, int height) throws Exception {
    	if (file == null) {
			return false;
		}
    	BufferedImage image = ImageIO.read(file);
		int imgWidth = image.getWidth();
		int imgHeight = image.getHeight();
		if (width == imgWidth && height == imgHeight) {
			return true;
		}
		return false;
    }

    /************************************************************************
     * <b>Description:</b><br>
     *  validate checkTypeOfImageFiles.
     *
     * @author      Le.Dinh
     * @date        2014/01/02
     * @param       imgType String
     * @param 		imgName String
     * @return      boolean
     * @throws 		Exception	exception
     ************************************************************************/
    public static boolean checkTypeOfImageFiles(String imgType, String imgName) throws Exception {
    	if (StringUtils.isEmpty(imgType) || StringUtils.isEmpty(imgName)) {
    		return false;
    	}
    	int imgNameLength = imgName.length();
    	if (imgNameLength > Constant.LENGTH_TAIL_IMAGE) {
    		// check tail imgName is gif
    		String imageNameTail = imgName.substring(imgNameLength - Constant.LENGTH_TAIL_IMAGE);
        	if (imgType.contains("gif") && imageNameTail.contains("gif")) {
        		return true;
        	}
    	}

    	return false;
    }

}
