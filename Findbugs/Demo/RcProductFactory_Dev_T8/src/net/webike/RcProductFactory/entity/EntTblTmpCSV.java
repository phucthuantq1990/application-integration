/************************************************************************
 * File Name    ： EntTblTmpCSV.java
 * Author       ： Nguyen.Chuong
 * Version      ： 1.0.0
 * Date Created ： 2014/02/26
 * Date Updated ： 2014/02/26
 * Description  ： Entity of EntTblTmpCSVMode3 for contain data of import CSV temp table mode 3.
 ***********************************************************************/
package net.webike.RcProductFactory.entity;

import java.io.Serializable;


/**
 * Entity of EntTblTmpCSV.
 */
public class EntTblTmpCSV implements Serializable {
    /**
	 * @seriablizable id for entity
	 */
	private static final long serialVersionUID = 4294582194564816444L;
	private String  importMode;
    private Long    productId;
    //Product Id with format Pxxxxx
    private String	sProductId;
    private Long    productSyouhinSysCode;
    private Integer attributeCode;
    private String  attributeName;
    private String  attributeValue;
    private String  attributeDisplay;
    private Integer attributeSort;

    private Integer fitModelSort;
    private String fitModelMaker;
    private String fitModelModel;
    private String fitModelStyle;
    //BOE #7206 Nguyen.Chuong 2014/06/04 add error_flg of fitModel.
    private boolean fitModelMakerNoFitFlg;
    private boolean fitModelModelNoFitFlg;
    //EOE #7206 Nguyen.Chuong 2014/06/04 add error_flg of fitModel.
    private String  sortField;
    private String  sortDir;
    /**
     * @return the importMode
     */
    public String getImportMode() {
        return importMode;
    }
    /**
     * @param importMode the importMode to set
     */
    public void setImportMode(String importMode) {
        this.importMode = importMode;
    }
    /**
     * @return the productId
     */
    public Long getProductId() {
        return productId;
    }
    /**
     * @param productId the productId to set
     */
    public void setProductId(Long productId) {
        this.productId = productId;
    }
    /**
     * @return the productSyouhinSysCode
     */
    public Long getProductSyouhinSysCode() {
        return productSyouhinSysCode;
    }
    /**
     * @param productSyouhinSysCode the productSyouhinSysCode to set
     */
    public void setProductSyouhinSysCode(Long productSyouhinSysCode) {
        this.productSyouhinSysCode = productSyouhinSysCode;
    }
    /**
     * @return the attributeCode
     */
    public Integer getAttributeCode() {
        return attributeCode;
    }
    /**
     * @param attributeCode the attributeCode to set
     */
    public void setAttributeCode(Integer attributeCode) {
        this.attributeCode = attributeCode;
    }
    /**
     * @return the attributeValue
     */
    public String getAttributeValue() {
        return attributeValue;
    }
    /**
     * @param attributeValue the attributeValue to set
     */
    public void setAttributeValue(String attributeValue) {
        this.attributeValue = attributeValue;
    }
    /**
     * @return the attributeDisplay
     */
    public String getAttributeDisplay() {
        return attributeDisplay;
    }
    /**
     * @param attributeDisplay the attributeDisplay to set
     */
    public void setAttributeDisplay(String attributeDisplay) {
        this.attributeDisplay = attributeDisplay;
    }
    /**
     * @return the sortField
     */
    public String getSortField() {
        return sortField;
    }
    /**
     * @param sortField the sortField to set
     */
    public void setSortField(String sortField) {
        this.sortField = sortField;
    }
    /**
     * @return the sortDir
     */
    public String getSortDir() {
        return sortDir;
    }
    /**
     * @param sortDir the sortDir to set
     */
    public void setSortDir(String sortDir) {
        this.sortDir = sortDir;
    }
    /**
     * @return the fitModelSort
     */
    public Integer getFitModelSort() {
        return fitModelSort;
    }
    /**
     * @param fitModelSort the fitModelSort to set
     */
    public void setFitModelSort(Integer fitModelSort) {
        this.fitModelSort = fitModelSort;
    }
    /**
     * @return the fitModelMaker
     */
    public String getFitModelMaker() {
        return fitModelMaker;
    }
    /**
     * @param fitModelMaker the fitModelMaker to set
     */
    public void setFitModelMaker(String fitModelMaker) {
        this.fitModelMaker = fitModelMaker;
    }
    /**
     * @return the fitModelModel
     */
    public String getFitModelModel() {
        return fitModelModel;
    }
    /**
     * @param fitModelModel the fitModelModel to set
     */
    public void setFitModelModel(String fitModelModel) {
        this.fitModelModel = fitModelModel;
    }
    /**
     * @return the fitModelStyle
     */
    public String getFitModelStyle() {
        return fitModelStyle;
    }
    /**
     * @param fitModelStyle the fitModelStyle to set
     */
    public void setFitModelStyle(String fitModelStyle) {
        this.fitModelStyle = fitModelStyle;
    }
    /**
     * @return the attributeName
     */
    public String getAttributeName() {
        return attributeName;
    }
    /**
     * @param attributeName the attributeName to set
     */
    public void setAttributeName(String attributeName) {
        this.attributeName = attributeName;
    }
    /**
     * @return the attributeSort
     */
    public Integer getAttributeSort() {
        return attributeSort;
    }
    /**
     * @param attributeSort the attributeSort to set
     */
    public void setAttributeSort(Integer attributeSort) {
        this.attributeSort = attributeSort;
    }
	/**
	 * @return the sProductId
	 */
	public String getsProductId() {
		return sProductId;
	}
	/**
	 * @param sProductId the sProductId to set
	 */
	public void setsProductId(String sProductId) {
		this.sProductId = sProductId;
	}
    /**
     * @return the fitModelMakerNoFitFlg
     */
    public boolean isFitModelMakerNoFitFlg() {
        return fitModelMakerNoFitFlg;
    }
    /**
     * @param fitModelMakerNoFitFlg the fitModelMakerNoFitFlg to set
     */
    public void setFitModelMakerNoFitFlg(boolean fitModelMakerNoFitFlg) {
        this.fitModelMakerNoFitFlg = fitModelMakerNoFitFlg;
    }
    /**
     * @return the fitModelModelNoFitFlg
     */
    public boolean isFitModelModelNoFitFlg() {
        return fitModelModelNoFitFlg;
    }
    /**
     * @param fitModelModelNoFitFlg the fitModelModelNoFitFlg to set
     */
    public void setFitModelModelNoFitFlg(boolean fitModelModelNoFitFlg) {
        this.fitModelModelNoFitFlg = fitModelModelNoFitFlg;
    }
}

