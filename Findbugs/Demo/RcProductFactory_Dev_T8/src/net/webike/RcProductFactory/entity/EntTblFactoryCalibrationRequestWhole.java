/************************************************************************
 * file name    ： EntTblFactoryProductGeneral.java
 * author       ： Nguyen.Chuong
 * version      ： 1.0.0
 * date created ： 2014/07/17
 * date updated ： 2014/07/17
 * description  ： Entity EntTblFactoryCalibrationRequestWhole mapping with rc_product_factory.tbl_factory_calibration_request_whole.
 *
 ***********************************************************************/
package net.webike.RcProductFactory.entity;

import java.util.Date;

/**
 * Entity of EntTblFactoryCalibrationRequestWhole.
 * @author Nguyen.Chuong
 * Date Created ： 2014/07/17
 */
public class EntTblFactoryCalibrationRequestWhole {

    //calibration_request_whole_matter_no
    private Long calibrationRequestWholeMatterNo;
    //calibration_request_whole_value
    private String calibrationRequestWholeValue;
    //calibration_request_whole_value_date
    private Date calibrationRequestWholeValueDate;
    //calibration_request_whole_value_user_id
    private String calibrationRequestWholeValueUserId;
    private String calibrationRequestWholeValueUserName;
    //calibration_request_whole_memo
    private String calibrationRequestWholeMemo;
    //calibration_request_whole_memo_date
    private Date calibrationRequestWholeMemoDate;
    //calibration_request_whole_memo_user_id
    private String calibrationRequestWholeMemoUserId;
    private String calibrationRequestWholeMemoUserName;
    /**
     * @return the calibrationRequestWholeMatterNo
     */
    public Long getCalibrationRequestWholeMatterNo() {
        return calibrationRequestWholeMatterNo;
    }
    /**
     * @param calibrationRequestWholeMatterNo the calibrationRequestWholeMatterNo to set
     */
    public void setCalibrationRequestWholeMatterNo(
            Long calibrationRequestWholeMatterNo) {
        this.calibrationRequestWholeMatterNo = calibrationRequestWholeMatterNo;
    }
    /**
     * @return the calibrationRequestWholeValue
     */
    public String getCalibrationRequestWholeValue() {
        return calibrationRequestWholeValue;
    }
    /**
     * @param calibrationRequestWholeValue the calibrationRequestWholeValue to set
     */
    public void setCalibrationRequestWholeValue(String calibrationRequestWholeValue) {
        this.calibrationRequestWholeValue = calibrationRequestWholeValue;
    }
    /**
     * @return the calibrationRequestWholeValueDate
     */
    public Date getCalibrationRequestWholeValueDate() {
        if (this.calibrationRequestWholeValueDate != null) {
            return (Date) calibrationRequestWholeValueDate.clone();
        }
        return null;
    }
    /**
     * @param calibrationRequestWholeValueDate the calibrationRequestWholeValueDate to set
     */
    public void setCalibrationRequestWholeValueDate(
            Date calibrationRequestWholeValueDate) {
        if (calibrationRequestWholeValueDate != null) {
            this.calibrationRequestWholeValueDate = (Date) calibrationRequestWholeValueDate.clone();
        } else {
            this.calibrationRequestWholeValueDate = null;
        }
    }
    /**
     * @return the calibrationRequestWholeValueUserId
     */
    public String getCalibrationRequestWholeValueUserId() {
        return calibrationRequestWholeValueUserId;
    }
    /**
     * @param calibrationRequestWholeValueUserId the calibrationRequestWholeValueUserId to set
     */
    public void setCalibrationRequestWholeValueUserId(
            String calibrationRequestWholeValueUserId) {
        this.calibrationRequestWholeValueUserId = calibrationRequestWholeValueUserId;
    }
    /**
     * @return the calibrationRequestWholeMemo
     */
    public String getCalibrationRequestWholeMemo() {
        return calibrationRequestWholeMemo;
    }
    /**
     * @param calibrationRequestWholeMemo the calibrationRequestWholeMemo to set
     */
    public void setCalibrationRequestWholeMemo(String calibrationRequestWholeMemo) {
        this.calibrationRequestWholeMemo = calibrationRequestWholeMemo;
    }
    /**
     * @return the calibrationRequestWholeMemoDate
     */
    public Date getCalibrationRequestWholeMemoDate() {
        if (this.calibrationRequestWholeMemoDate != null) {
            return (Date) calibrationRequestWholeMemoDate.clone();
        }
        return null;
    }
    /**
     * @param calibrationRequestWholeMemoDate the calibrationRequestWholeMemoDate to set
     */
    public void setCalibrationRequestWholeMemoDate(
            Date calibrationRequestWholeMemoDate) {
        if (calibrationRequestWholeMemoDate != null) {
            this.calibrationRequestWholeMemoDate = (Date) calibrationRequestWholeMemoDate.clone();
        } else {
            this.calibrationRequestWholeMemoDate = null;
        }
    }
    /**
     * @return the calibrationRequestWholeMemoUserId
     */
    public String getCalibrationRequestWholeMemoUserId() {
        return calibrationRequestWholeMemoUserId;
    }
    /**
     * @param calibrationRequestWholeMemoUserId the calibrationRequestWholeMemoUserId to set
     */
    public void setCalibrationRequestWholeMemoUserId(
            String calibrationRequestWholeMemoUserId) {
        this.calibrationRequestWholeMemoUserId = calibrationRequestWholeMemoUserId;
    }
    /**
     * @return the calibrationRequestWholeValueUserName
     */
    public String getCalibrationRequestWholeValueUserName() {
        return calibrationRequestWholeValueUserName;
    }
    /**
     * @param calibrationRequestWholeValueUserName the calibrationRequestWholeValueUserName to set
     */
    public void setCalibrationRequestWholeValueUserName(
            String calibrationRequestWholeValueUserName) {
        this.calibrationRequestWholeValueUserName = calibrationRequestWholeValueUserName;
    }
    /**
     * @return the calibrationRequestWholeMemoUserName
     */
    public String getCalibrationRequestWholeMemoUserName() {
        return calibrationRequestWholeMemoUserName;
    }
    /**
     * @param calibrationRequestWholeMemoUserName the calibrationRequestWholeMemoUserName to set
     */
    public void setCalibrationRequestWholeMemoUserName(
            String calibrationRequestWholeMemoUserName) {
        this.calibrationRequestWholeMemoUserName = calibrationRequestWholeMemoUserName;
    }
}
