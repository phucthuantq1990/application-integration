package net.webike.RcProductFactory.entity;

import java.io.Serializable;

/**
 * Class imageUpload.
 */
public class ImageUpload implements Serializable {
	/** serialVersionUID. */
	private static final long serialVersionUID = 4626928982018321955L;
	private int imageWidth;
	private int imageHeight;
	private String imageName;
	private String imageType;
	private String imageError = "";
	/**
	 * @return the imageWidth
	 */
	public int getImageWidth() {
		return imageWidth;
	}
	/**
	 * @param imageWidth the imageWidth to set
	 */
	public void setImageWidth(int imageWidth) {
		this.imageWidth = imageWidth;
	}
	/**
	 * @return the imageHeight
	 */
	public int getImageHeight() {
		return imageHeight;
	}
	/**
	 * @param imageHeight the imageHeight to set
	 */
	public void setImageHeight(int imageHeight) {
		this.imageHeight = imageHeight;
	}
	/**
	 * @return the imageName
	 */
	public String getImageName() {
		return imageName;
	}
	/**
	 * @param imageName the imageName to set
	 */
	public void setImageName(String imageName) {
		this.imageName = imageName;
	}
	/**
	 * @return the imageType
	 */
	public String getImageType() {
		return imageType;
	}
	/**
	 * @param imageType the imageType to set
	 */
	public void setImageType(String imageType) {
		this.imageType = imageType;
	}
	/**
	 * @return the imageError
	 */
	public String getImageError() {
		return imageError;
	}
	/**
	 * @param imageError the imageError to set
	 */
	public void setImageError(String imageError) {
		this.imageError = imageError;
	}

}
