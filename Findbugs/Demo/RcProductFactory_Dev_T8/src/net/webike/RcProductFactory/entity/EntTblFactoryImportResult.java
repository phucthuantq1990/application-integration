/************************************************************************
 * File Name    ： EntTblFactoryImportResult.java
 * Author       ： hoang.ho
 * Version      ： 1.0.0
 * Date Created ： 2014/03/04
 * Date Updated ： 2014/03/04
 * Description  ： Contain properties of EntTblFactoryImportResult.
 *
 ***********************************************************************/
package net.webike.RcProductFactory.entity;


/**
 * Contain properties of EntTblFactoryImportResult.
 * @author Hoang.Ho
 * @since 2014/03/04
 */
public class EntTblFactoryImportResult {
    /** import_result_code.*/
    private long importResultCode;
    /** import_condition_code.*/
    private long importConditionCode;
    /** import_result_key_code.*/
    private long importResultKeyCode;
    /** import_result_key_type.*/
    private String importResultKeyType;
    /** import_result_values.*/
    private String importResultValues;
    /** import_result_values_type.*/
    private String importResultValuesType;
    /**
     * @return the importResultCode
     */
    public long getImportResultCode() {
        return importResultCode;
    }
    /**
     * @param importResultCode the importResultCode to set
     */
    public void setImportResultCode(long importResultCode) {
        this.importResultCode = importResultCode;
    }
    /**
     * @return the importConditionCode
     */
    public long getImportConditionCode() {
        return importConditionCode;
    }
    /**
     * @param importConditionCode the importConditionCode to set
     */
    public void setImportConditionCode(long importConditionCode) {
        this.importConditionCode = importConditionCode;
    }
    /**
     * @return the importResultKeyCode
     */
    public long getImportResultKeyCode() {
        return importResultKeyCode;
    }
    /**
     * @param importResultKeyCode the importResultKeyCode to set
     */
    public void setImportResultKeyCode(long importResultKeyCode) {
        this.importResultKeyCode = importResultKeyCode;
    }
    /**
     * @return the importResultKeyType
     */
    public String getImportResultKeyType() {
        return importResultKeyType;
    }
    /**
     * @param importResultKeyType the importResultKeyType to set
     */
    public void setImportResultKeyType(String importResultKeyType) {
        this.importResultKeyType = importResultKeyType;
    }
    /**
     * @return the importResultValues
     */
    public String getImportResultValues() {
        return importResultValues;
    }
    /**
     * @param importResultValues the importResultValues to set
     */
    public void setImportResultValues(String importResultValues) {
        this.importResultValues = importResultValues;
    }
    /**
     * @return the importResultValuesType
     */
    public String getImportResultValuesType() {
        return importResultValuesType;
    }
    /**
     * @param importResultValuesType the importResultValuesType to set
     */
    public void setImportResultValuesType(String importResultValuesType) {
        this.importResultValuesType = importResultValuesType;
    }
}
