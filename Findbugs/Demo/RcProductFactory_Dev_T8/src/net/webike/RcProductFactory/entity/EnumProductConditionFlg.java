/************************************************************************
 * File Name    �F EnumProductConditionFlg.java
 * Author       �F Le.Dinh
 * Version      �F 1.0.0
 * Date Created �F 2014/02/19
 * Description  �F Enum content all value of EnumProductConditionFlg.
 *
 ***********************************************************************/
package net.webike.RcProductFactory.entity;

import java.util.HashMap;
import java.util.LinkedHashMap;

/**
 * Enum of attribute Type.
 */
public enum EnumProductConditionFlg {

    T1("PRODUCT_OPEN_PRICE_FLG", "�I�[�v�����i"),
    T2("PRODUCT_PROPER_SELLING_FLG", "�艿�̔�"),
    T3("PRODUCT_ORDER_PRODUCT_FLG", "�󒍐��Y"),
    T4("PRODUCT_NO_RETURNABLE_FLG", "�ԕi�����s��"),
    T5("PRODUCT_AMBIGUOUS_IMAGE_FLG", "�Q�l�摜");

    private String key;

    private String value;

    /**---------------------------------------------------------------------------
     * Private constructor for EnumProductConditionFlg.
     * @param key for get value
     * @param value of EnumProductConditionFlg
     */
    private EnumProductConditionFlg(final String key, final String value) {
        this.setKey(key);
        this.setValue(value);
    }

    /************************************************************************
     * <b>Description:</b><br>
     *  get all value of enum type.
     *
     * @author		Le.Dinh
     * @date		2014/02/19
     * @return		HashMap<String,String>
     ************************************************************************/
    public static HashMap<String, String> getHashMapAllFlg() {
        HashMap<String, String> tm = new LinkedHashMap<String, String>();

        for (EnumProductConditionFlg value : values()) {
            tm.put(value.getKey(), value.getValue());
        }

        return tm;
    }

    /************************************************************************
     * <b>Description:</b><br>
     *  get value of enum type by key.
     *
     * @author		Le.Dinh
     * @date		2014/02/19
     * @param       key to get value. Can be: String, integet, group,...
     * @return		String
     ************************************************************************/
    public static String getFlgValueByKey(String key) {
        for (EnumProductConditionFlg valueEnum : values()) {
            if (key.equals(valueEnum.getKey())) {
                return valueEnum.getValue();
            }
        }
        return null;
    }

    /************************************************************************
     * <b>Description:</b><br>
     *  get key by value of enum.
     *
     * @author		Le.Dinh
     * @date		2014/02/19
     * @param       value to get key.
     * @return		String
     ************************************************************************/
    public static String getFlgKeyByValue(String value) {
        for (EnumProductConditionFlg valueEnum : values()) {
            if (value.equals(valueEnum.getValue())) {
                return valueEnum.getKey();
            }
        }
        return null;
    }

    /**
     * @return the key
     */
    public String getKey() {
        return key;
    }

    /**
     * @param key the key to set
     */
    public void setKey(String key) {
        this.key = key;
    }

    /**
     * @return the value
     */
    public String getValue() {
        return value;
    }

    /**
     * @param value the value to set
     */
    public void setValue(String value) {
        this.value = value;
    }

}
