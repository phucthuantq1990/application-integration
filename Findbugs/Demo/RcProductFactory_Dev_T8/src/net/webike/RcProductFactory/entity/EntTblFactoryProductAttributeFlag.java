
package net.webike.RcProductFactory.entity;


/**
 * Entity of EntTblFactoryProductAttributeFlag.
 * @author Doan Chuong
 * Date Created �F 2014/03/05
 */
public class EntTblFactoryProductAttributeFlag {
    private Long productId;
    private Integer attributeCode;
    private Integer attributeFlagValues;
    private Integer attributeFlagSort;
    private String updatedOn;
    private String updatedUserId;
    /**
     * @return the productId
     */
    public Long getProductId() {
        return productId;
    }
    /**
     * @param productId the productId to set
     */
    public void setProductId(Long productId) {
        this.productId = productId;
    }
    /**
     * @return the attributeCode
     */
    public Integer getAttributeCode() {
        return attributeCode;
    }
    /**
     * @param attributeCode the attributeCode to set
     */
    public void setAttributeCode(Integer attributeCode) {
        this.attributeCode = attributeCode;
    }
    /**
     * @return the attributeFlagValues
     */
    public Integer getAttributeFlagValues() {
        return attributeFlagValues;
    }
    /**
     * @param attributeFlagValues the attributeFlagValues to set
     */
    public void setAttributeFlagValues(Integer attributeFlagValues) {
        this.attributeFlagValues = attributeFlagValues;
    }
    /**
     * @return the attributeFlagSort
     */
    public Integer getAttributeFlagSort() {
        return attributeFlagSort;
    }
    /**
     * @param attributeFlagSort the attributeFlagSort to set
     */
    public void setAttributeFlagSort(Integer attributeFlagSort) {
        this.attributeFlagSort = attributeFlagSort;
    }
    /**
     * @return the updatedOn
     */
    public String getUpdatedOn() {
        return updatedOn;
    }
    /**
     * @param updatedOn the updatedOn to set
     */
    public void setUpdatedOn(String updatedOn) {
        this.updatedOn = updatedOn;
    }
    /**
     * @return the updatedUserId
     */
    public String getUpdatedUserId() {
        return updatedUserId;
    }
    /**
     * @param updatedUserId the updatedUserId to set
     */
    public void setUpdatedUserId(String updatedUserId) {
        this.updatedUserId = updatedUserId;
    }
}
