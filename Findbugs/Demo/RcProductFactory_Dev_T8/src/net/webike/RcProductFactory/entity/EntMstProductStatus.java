/************************************************************************
 * file name	： EntMstProductStatus.java
 * author		： Long Vu
 * version		： 1.0.0
 * date created	： 2014/02/18
 * date updated	： 2014/02/18
 * description	： EntMstProductStatus.
 *
 ***********************************************************************/
package net.webike.RcProductFactory.entity;

import java.util.Date;

/**
 * EntMstProductStatus.
 */
public class EntMstProductStatus {
    private int productStatusCode;
    private String productStatusName;
    private int productStatusCanDeleteFlg;
    private int productStatusCanEditFlg;
    private int productStatusCanUploadFlg;
    private Date createdOn;
    private String createdUserId;
    private Date updatedOn;
    private String updatedUserId;
    /**
     * @return the productStatusCode
     */
    public int getProductStatusCode() {
        return productStatusCode;
    }
    /**
     * @param productStatusCode the productStatusCode to set
     */
    public void setProductStatusCode(int productStatusCode) {
        this.productStatusCode = productStatusCode;
    }
    /**
     * @return the productStatusName
     */
    public String getProductStatusName() {
        return productStatusName;
    }
    /**
     * @param productStatusName the productStatusName to set
     */
    public void setProductStatusName(String productStatusName) {
        this.productStatusName = productStatusName;
    }
    /**
     * @return the productStatusCanDeleteFlg
     */
    public int getProductStatusCanDeleteFlg() {
        return productStatusCanDeleteFlg;
    }
    /**
     * @param productStatusCanDeleteFlg the productStatusCanDeleteFlg to set
     */
    public void setProductStatusCanDeleteFlg(int productStatusCanDeleteFlg) {
        this.productStatusCanDeleteFlg = productStatusCanDeleteFlg;
    }
    /**
     * @return the productStatusCanEditFlg
     */
    public int getProductStatusCanEditFlg() {
        return productStatusCanEditFlg;
    }
    /**
     * @param productStatusCanEditFlg the productStatusCanEditFlg to set
     */
    public void setProductStatusCanEditFlg(int productStatusCanEditFlg) {
        this.productStatusCanEditFlg = productStatusCanEditFlg;
    }
    /**
     * @return the productStatusCanUploadFlg
     */
    public int getProductStatusCanUploadFlg() {
        return productStatusCanUploadFlg;
    }
    /**
     * @param productStatusCanUploadFlg the productStatusCanUploadFlg to set
     */
    public void setProductStatusCanUploadFlg(int productStatusCanUploadFlg) {
        this.productStatusCanUploadFlg = productStatusCanUploadFlg;
    }
    /**
     * @return the createdOn
     */
    public Date getCreatedOn() {
        if (this.createdOn != null) {
            return (Date) createdOn.clone();
        }
        return null;
    }
    /**
     * @param createdOn the createdOn to set
     */
    public void setCreatedOn(Date createdOn) {
        if (createdOn != null) {
            this.createdOn = (Date) createdOn.clone();
        } else {
            this.createdOn = null;
        }
    }
    /**
     * @return the createdUserId
     */
    public String getCreatedUserId() {
        return createdUserId;
    }
    /**
     * @param createdUserId the createdUserId to set
     */
    public void setCreatedUserId(String createdUserId) {
        this.createdUserId = createdUserId;
    }
    /**
     * @return the updatedOn
     */
    public Date getUpdatedOn() {
        if (this.updatedOn != null) {
            return (Date) updatedOn.clone();
        }
        return null;
    }
    /**
     * @param updatedOn the updatedOn to set
     */
    public void setUpdatedOn(Date updatedOn) {
        if (updatedOn != null) {
            this.updatedOn = (Date) updatedOn.clone();
        } else {
            this.updatedOn = null;
        }
    }
    /**
     * @return the updatedUserId
     */
    public String getUpdatedUserId() {
        return updatedUserId;
    }
    /**
     * @param updatedUserId the updatedUserId to set
     */
    public void setUpdatedUserId(String updatedUserId) {
        this.updatedUserId = updatedUserId;
    }
}
