/************************************************************************
 * File Name    ： EntTblFactoryProductSupplier.java
 * Author       ：Le.Dinh
 * Version      ： 1.0.0
 * Date Created ： 2014/02/12
 * Date Updated ： 2014/02/12
 * Description  ： Entity of EntTblFactoryProductSupplier.
 ***********************************************************************/
package net.webike.RcProductFactory.entity;

import java.util.Date;

/**
 * Entity of EntTblFactoryProductFitModel.
 */
public class EntTblFactoryProductSupplierPrice {

	private Long productId;

	private Date updatedOn;

	private String updatedUserId = "";

	private Integer supplierPricePrice;

	private String supplierPricePriceStr;

	private Integer replaceModeSupplierPricePrice;

	private String supplierPricePriceFind;

	private Date supplierPriceEffectDate;

	private Boolean supplierPriceChgChk;

	/**
	 * @return the productId
	 */
	public Long getProductId() {
		return productId;
	}

	/**
	 * @param productId the productId to set
	 */
	public void setProductId(Long productId) {
		this.productId = productId;
	}

	/**
	 * @return the updatedOn
	 */
	public Date getUpdatedOn() {
		if (updatedOn != null) {
            return (Date) updatedOn.clone();
        }
        return null;
	}

	/**
	 * @param updatedOn the updatedOn to set
	 */
	public void setUpdatedOn(Date updatedOn) {
		if (updatedOn != null) {
			this.updatedOn = (Date) updatedOn.clone();
        } else {
        	this.updatedOn = null;
        }
	}

	/**
	 * @return the updatedUserId
	 */
	public String getUpdatedUserId() {
		return updatedUserId;
	}

	/**
	 * @param updatedUserId the updatedUserId to set
	 */
	public void setUpdatedUserId(String updatedUserId) {
		this.updatedUserId = updatedUserId;
	}

	/**
	 * @return the supplierPricePrice
	 */
	public Integer getSupplierPricePrice() {
		return supplierPricePrice;
	}

	/**
	 * @param supplierPricePrice the supplierPricePrice to set
	 */
	public void setSupplierPricePrice(Integer supplierPricePrice) {
		this.supplierPricePrice = supplierPricePrice;
	}

	/**
	 * @return the replaceModeSupplierPricePrice
	 */
	public Integer getReplaceModeSupplierPricePrice() {
		return replaceModeSupplierPricePrice;
	}

	/**
	 * @param replaceModeSupplierPricePrice the replaceModeSupplierPricePrice to set
	 */
	public void setReplaceModeSupplierPricePrice(
			Integer replaceModeSupplierPricePrice) {
		this.replaceModeSupplierPricePrice = replaceModeSupplierPricePrice;
	}

	/**
	 * @return the supplierPricePriceFind
	 */
	public String getSupplierPricePriceFind() {
		return supplierPricePriceFind;
	}

	/**
	 * @param supplierPricePriceFind the supplierPricePriceFind to set
	 */
	public void setSupplierPricePriceFind(String supplierPricePriceFind) {
		this.supplierPricePriceFind = supplierPricePriceFind;
	}

	/**
	 * @return the supplierPricePriceStr
	 */
	public String getSupplierPricePriceStr() {
		return supplierPricePriceStr;
	}

	/**
	 * @param supplierPricePriceStr the supplierPricePriceStr to set
	 */
	public void setSupplierPricePriceStr(String supplierPricePriceStr) {
		this.supplierPricePriceStr = supplierPricePriceStr;
	}

    /**
     * @return the supplierPriceEffectDate
     */
    public Date getSupplierPriceEffectDate() {
        if (this.supplierPriceEffectDate != null) {
            return (Date) this.supplierPriceEffectDate.clone();
        }
        return null;
    }

    /**
     * @param supplierPriceEffectDate the supplierPriceEffectDate to set
     */
    public void setSupplierPriceEffectDate(Date supplierPriceEffectDate) {
        if (supplierPriceEffectDate != null) {
            this.supplierPriceEffectDate = (Date) supplierPriceEffectDate.clone();
        } else {
            this.supplierPriceEffectDate = null;
        }
    }

    /**
     * @return the supplierPriceChgChk
     */
    public Boolean getSupplierPriceChgChk() {
        return supplierPriceChgChk;
    }

    /**
     * @param supplierPriceChgChk the supplierPriceChgChk to set
     */
    public void setSupplierPriceChgChk(Boolean supplierPriceChgChk) {
        this.supplierPriceChgChk = supplierPriceChgChk;
    }

}
