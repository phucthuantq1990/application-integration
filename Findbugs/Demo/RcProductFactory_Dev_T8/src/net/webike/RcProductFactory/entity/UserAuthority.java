/************************************************************************
 * File Name    ： UserAuthority.java
 * Author       ： Nguyen.Chuong
 * Version      ： 1.0.0
 * Date Created ： 2013/10/02
 * Date Updated ： 2013/10/02
 * Description  ： enum to contain all authority value: admin, oppotunity.
 *
 ***********************************************************************/
package net.webike.RcProductFactory.entity;

import java.util.HashMap;
import java.util.TreeMap;

import org.apache.commons.lang.StringUtils;

/**
 * enum UserAuthority to contain authority.
 */
public enum UserAuthority {

    A1("0", "オペレーター"),
    A2("1", "管理者");

    /**---------------------------------------------------------------------------
     * String value.
     * @description value of authoriry. Can be 0, 1.
     */
    private String val;
    /**---------------------------------------------------------------------------
     * String name.
     * @description name of authoriry. Can be admin, opperator.
     */
    private String name;

    /**---------------------------------------------------------------------------
     * Enum for authority attribute.
     * @param val String
     * @param name String
     */
    private UserAuthority(final String val, final String name) {
        this.val = val;
        this.name = name;
    }
    /**
     * Get all enum and return as the treeMap.
     * @return TreeMap<val, name>
     * @author Nguyen.Chuong
     * @version 1.0
     */
    public static TreeMap<String, String> getListTreeMap() {
        TreeMap<String, String> tm = new TreeMap<String, String>();
        for (UserAuthority o : values()) {
            tm.put(o.getVal(), o.getName());
        }
        return tm;
    }

    /**
     * Get all enum and return as the hashMap.
     * @return HashMap<val, name>
     * @author Nguyen.Chuong
     * @version 1.0
     */
    public static HashMap<String, String> getListHashMap() {
        HashMap<String, String> tm = new HashMap<String, String>();
        for (UserAuthority o : values()) {
            tm.put(o.getVal(), o.getName());
        }
        return tm;
    }
    /**
     * get name of authority by value.
     * @param val String
     * @return name of authority: 0 => opperator, 1 => admin
     * @author Nguyen.Chuong
     * @version 1.0
     */
    public static String getName(String val) {
        if (StringUtils.isEmpty(val)) {
            return "";
        }
        for (UserAuthority o : values()) {
            if (o.getVal().equals(val)) {
                return o.getName();
            }
        }
        return "";
    }

    /**
     * get value of authority.
     * @return val
     */
    public String getVal() {
        return val;
    }

    /**
     * set value of authority.
     * @param val val
     */
    public void setVal(String val) {
        this.val = val;
    }

    /**
     * get name of authority.
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * set name of authority.
     * @param name name
     */
    public void setName(String name) {
        this.name = name;
    }
}
