package net.webike.RcProductFactory.entity;

import java.io.Serializable;

/**
 * Class ImageUploadKenDo.
 */
public class ImageUploadKenDo implements Serializable {
	/** serialVersionUID. */
	private static final long serialVersionUID = 4626928982018321955L;
	private String name;
	private long size;
	private String type;
	private String filePatch;
	/**
	 * @return the size
	 */
	public long getSize() {
		return size;
	}
	/**
	 * @param size the size to set
	 */
	public void setSize(long size) {
		this.size = size;
	}
	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}
	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the filePatch
	 */
	public String getFilePatch() {
		return filePatch;
	}
	/**
	 * @param filePatch the filePatch to set
	 */
	public void setFilePatch(String filePatch) {
		this.filePatch = filePatch;
	}

}
