/************************************************************************
 * file name	： EntTblFactoryProductNew.java
 * author		： Le.Dinh
 * version		： 1.0.0
 * date created	： 24 Feb 2014
 * date updated	： 24 Feb 2014
 * description	： Entity for table rc_product_factory.tbl_factory_product
 ***********************************************************************/
package net.webike.RcProductFactory.entity;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * @author Le.Dinh
 *
 */
public class EntTblFactoryProductNew implements Serializable, Cloneable {

	private static final long serialVersionUID = 4103369161778592593L;
	//more properties for ProductFitModel page:
	private Integer productModelCheckFlg;
    private Integer productGeneralCheckFlg;
    private Integer productCategoryCheckFlg;
    private Integer productAttributeCheckFlg;
	private String descriptionSummary;
	private String descriptionRemarks;
	private String productSelectDisplay;
	private Long productProperPrice = 0L;
	private Long productProperPriceFrom = 0L; //init to use in query
	private Long productProperPriceTo = 0L; //init to use in query
	private String categoryName;
	//end
	private Long productId;
	private Long productSyouhinSysCode;
	private Long productMatterNo;
	private Integer productGeneralErrorFlg = 0;
	private Integer productCategoryErrorFlg = 0;
	private Integer productModelErrorFlg;
	private Integer productAttributeErrorFlg = 0;
	private Integer productRegistrationFlg = 0;
	private Date createdOn;
	private String createdUserId;
	private String createUserName;
	private Date updatedOn;
	private String updatedUserId;
	private String updatedUserName;
	//Add attribute for get list product at matterDetail page
	private Long matterNo;
	private String productBrandCode;
	private String brandName;
	private String productName;
	private String productCode;
	private String productEanCode;
	private String showProductRegistrationFlg;

	private String updatedOnFrom;
	private String updatedOnTo;
	private Integer limit;
	private Integer offset;
	private String showProductSyouhinSysCode;
	private String sortField;
	private String sortDir;
	// to keep max model sort to put it to fit model objec
	private Integer modelSort;
	//CategoryCode of product
	private String categoryCode;
	//AttributeCode for productManage page
	private String attributeCode;
	/* Add attribute for productEdit */
	private Integer matterRegistrationFlg;
	private String showMatterRegistrationFlg;
	private Integer matterFactoryErrorCountGeneral;
	private Integer matterFactoryErrorCountCategory;
	private Integer matterFactoryErrorCountModel;
	private Integer matterFactoryErrorCountAttribute;
	private String descriptionSentence;
	private String descriptionCaution;
	private Long supplierPricePrice = 0L;
	private Float supplierRate = 0F;
	private Long productGroupCode;
	private List<EntTblFactoryProductSelect> lstSelectOfProduct;
	private String idRow = "";
	private String crudFlg = "";
	private Integer siireMarumeCode;
	private String fitModelMaker;
	private String fitModelModel;
	private String masterMisMatch;
	private Integer masterMisMatchFlg;
	/* End of attribute for productEdit*/

	/*BOE #7363 Tran.Thanh : Add mode count error flg  */
	private Integer flgErrorGeneral = 0;
	private Integer flgErrorCategory = 0;
	private Integer flgErrorModel = 0;
	private String supplierReleaseDate;
	/*EOE #7363 Tran.Thanh : Add mode count error flg  */

	/*BOE @rcv!dau.phuong : Add more attribute for pop up request  */
    private Integer productCheck1Flg = 0;
    private Date productCheck1Date;
    private String productCheck1UserId;
    private String productCheck1UserLastName;
    private String productCheck1UserFirstName;
    private Integer productCheck2Flg = 0;
    private Date productCheck2Date;
    private String productCheck2UserId;
    private String productCheck2UserLastName;
    private String productCheck2UserFirstName;
    private Long firstCheckCount;
    private Long secondCheckCount;
    private Long allProductCount;
    private Long allMatterCount;
    /*EOE @rcv!dau.phuong : Add more attribute for pop up request  */
    private String matterName;
    private String userFirstName;
    private String userLastName;

	 /************************************************************************
     * <b>Description:</b><br>
     *  Clone ent.
     *
     * @author      Luong.Dai
     * @date        Feb 19, 2014
     * @return      EntTblFactoryProductNew
     ************************************************************************/
	public EntTblFactoryProductNew clone() {
	    EntTblFactoryProductNew product = null;
	    try {
	        product = (EntTblFactoryProductNew) super.clone();
        } catch (CloneNotSupportedException e) {
            product = null;
        }
        return product;
	}
	/**
     * @return the productId
     */
    public Long getProductId() {
        return productId;
    }
    /**
     * @param productId the productId to set
     */
    public void setProductId(Long productId) {
        this.productId = productId;
    }
    /**
     * @return the productSyouhinSysCode
     */
    public Long getProductSyouhinSysCode() {
        return productSyouhinSysCode;
    }
    /**
     * @param productSyouhinSysCode the productSyouhinSysCode to set
     */
    public void setProductSyouhinSysCode(Long productSyouhinSysCode) {
        this.productSyouhinSysCode = productSyouhinSysCode;
    }
    /**
     * @return the productMatterNo
     */
    public Long getProductMatterNo() {
        return productMatterNo;
    }
    /**
     * @param productMatterNo the productMatterNo to set
     */
    public void setProductMatterNo(Long productMatterNo) {
        this.productMatterNo = productMatterNo;
    }
    /**
     * @return the productGeneralErrorFlg
     */
    public Integer getProductGeneralErrorFlg() {
        return productGeneralErrorFlg;
    }
    /**
     * @param productGeneralErrorFlg the productGeneralErrorFlg to set
     */
    public void setProductGeneralErrorFlg(Integer productGeneralErrorFlg) {
        this.productGeneralErrorFlg = productGeneralErrorFlg;
    }
    /**
     * @return the productCategoryErrorFlg
     */
    public Integer getProductCategoryErrorFlg() {
        return productCategoryErrorFlg;
    }
    /**
     * @param productCategoryErrorFlg the productCategoryErrorFlg to set
     */
    public void setProductCategoryErrorFlg(Integer productCategoryErrorFlg) {
        this.productCategoryErrorFlg = productCategoryErrorFlg;
    }
    /**
     * @return the productModelErrorFlg
     */
    public Integer getProductModelErrorFlg() {
        return productModelErrorFlg;
    }
    /**
     * @param productModelErrorFlg the productModelErrorFlg to set
     */
    public void setProductModelErrorFlg(Integer productModelErrorFlg) {
        this.productModelErrorFlg = productModelErrorFlg;
    }
    /**
     * @return the productAttributeErrorFlg
     */
    public Integer getProductAttributeErrorFlg() {
        return productAttributeErrorFlg;
    }
    /**
     * @param productAttributeErrorFlg the productAttributeErrorFlg to set
     */
    public void setProductAttributeErrorFlg(Integer productAttributeErrorFlg) {
        this.productAttributeErrorFlg = productAttributeErrorFlg;
    }
    /**
     * @return the productRegistrationFlg
     */
    public Integer getProductRegistrationFlg() {
        return productRegistrationFlg;
    }
    /**
     * @param productRegistrationFlg the productRegistrationFlg to set
     */
    public void setProductRegistrationFlg(Integer productRegistrationFlg) {
        this.productRegistrationFlg = productRegistrationFlg;
    }
    /**
     * @return the createdOn
     */
    public Date getCreatedOn() {
        if (this.createdOn != null) {
            return (Date) createdOn.clone();
        }
        return null;
    }
    /**
     * @param createdOn the createdOn to set
     */
    public void setCreatedOn(Date createdOn) {
        if (createdOn != null) {
            this.createdOn = (Date) createdOn.clone();
        } else {
            this.createdOn = null;
        }
    }
    /**
     * @return the createdUserId
     */
    public String getCreatedUserId() {
        return createdUserId;
    }
    /**
     * @param createdUserId the createdUserId to set
     */
    public void setCreatedUserId(String createdUserId) {
        this.createdUserId = createdUserId;
    }
    /**
     * @return the updatedOn
     */
    public Date getUpdatedOn() {
        if (this.updatedOn != null) {
            return (Date) updatedOn.clone();
        }
        return null;
    }
    /**
     * @param updatedOn the updatedOn to set
     */
    public void setUpdatedOn(Date updatedOn) {
        if (updatedOn != null) {
            this.updatedOn = (Date) updatedOn.clone();
        } else {
            this.updatedOn = null;
        }
    }
    /**
     * @return the string updatedOn
     */
    public String getUpdatedOnToString() {
        if (this.updatedOn != null) {
            DateFormat df = new SimpleDateFormat("yyyy/MM/dd H:m:s");
            return df.format(this.updatedOn);
        }
        return null;
    }
    /**
     * @return the updatedUserId
     */
    public String getUpdatedUserId() {
        return updatedUserId;
    }
    /**
     * @param updatedUserId the updatedUserId to set
     */
    public void setUpdatedUserId(String updatedUserId) {
        this.updatedUserId = updatedUserId;
    }
    /**
     * @return the matterNo
     */
    public Long getMatterNo() {
        return matterNo;
    }
    /**
     * @param matterNo the matterNo to set
     */
    public void setMatterNo(Long matterNo) {
        this.matterNo = matterNo;
    }
    /**
     * @return the brandName
     */
    public String getBrandName() {
        return brandName;
    }
    /**
     * @param brandName the brandName to set
     */
    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }
    /**
     * @return the productName
     */
    public String getProductName() {
        return productName;
    }
    /**
     * @param productName the productName to set
     */
    public void setProductName(String productName) {
        this.productName = productName;
    }
    /**
     * @return the productCode
     */
    public String getProductCode() {
        return productCode;
    }
    /**
     * @param productCode the productCode to set
     */
    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }
    /**
     * @return the productEanCode
     */
    public String getProductEanCode() {
        return productEanCode;
    }
    /**
     * @param productEanCode the productEanCode to set
     */
    public void setProductEanCode(String productEanCode) {
        this.productEanCode = productEanCode;
    }
    /**
     * @return the productBrandCode
     */
    public String getProductBrandCode() {
        return productBrandCode;
    }
    /**
     * @param productBrandCode the productBrandCode to set
     */
    public void setProductBrandCode(String productBrandCode) {
        this.productBrandCode = productBrandCode;
    }
    /**
     * @return the showProductRegistrationFlg
     */
    public String getShowProductRegistrationFlg() {
        return showProductRegistrationFlg;
    }
    /**
     * @param showProductRegistrationFlg the showProductRegistrationFlg to set
     */
    public void setShowProductRegistrationFlg(String showProductRegistrationFlg) {
        this.showProductRegistrationFlg = showProductRegistrationFlg;
    }
    /**
     * @return the updatedOnFrom
     */
    public String getUpdatedOnFrom() {
        return updatedOnFrom;
    }
    /**
     * @param updatedOnFrom the updatedOnFrom to set
     */
    public void setUpdatedOnFrom(String updatedOnFrom) {
        this.updatedOnFrom = updatedOnFrom;
    }
    /**
     * @return the updatedOnTo
     */
    public String getUpdatedOnTo() {
        return updatedOnTo;
    }
    /**
     * @param updatedOnTo the updatedOnTo to set
     */
    public void setUpdatedOnTo(String updatedOnTo) {
        this.updatedOnTo = updatedOnTo;
    }
    /**
     * @return the limit
     */
    public Integer getLimit() {
        return limit;
    }
    /**
     * @param limit the limit to set
     */
    public void setLimit(Integer limit) {
        this.limit = limit;
    }
    /**
     * @return the offset
     */
    public Integer getOffset() {
        return offset;
    }
    /**
     * @param offset the offset to set
     */
    public void setOffset(Integer offset) {
        this.offset = offset;
    }
    /**
     * @return the createUserName
     */
    public String getCreateUserName() {
        return createUserName;
    }
    /**
     * @param createUserName the createUserName to set
     */
    public void setCreateUserName(String createUserName) {
        this.createUserName = createUserName;
    }
    /**
     * @return the showProductSyouhinSysCode
     */
    public String getShowProductSyouhinSysCode() {
        return showProductSyouhinSysCode;
    }
    /**
     * @param showProductSyouhinSysCode the showProductSyouhinSysCode to set
     */
    public void setShowProductSyouhinSysCode(String showProductSyouhinSysCode) {
        this.showProductSyouhinSysCode = showProductSyouhinSysCode;
    }
    /**
     * @return the sortField
     */
    public String getSortField() {
        return sortField;
    }
    /**
     * @param sortField the sortField to set
     */
    public void setSortField(String sortField) {
        this.sortField = sortField;
    }
    /**
     * @return the sortDir
     */
    public String getSortDir() {
        return sortDir;
    }
    /**
     * @param sortDir the sortDir to set
     */
    public void setSortDir(String sortDir) {
        this.sortDir = sortDir;
    }
    public Integer getProductModelCheckFlg() {
        return productModelCheckFlg;
    }
    public void setProductModelCheckFlg(Integer productModelCheckFlg) {
        this.productModelCheckFlg = productModelCheckFlg;
    }
    public String getDescriptionSummary() {
        return descriptionSummary;
    }
    public void setDescriptionSummary(String descriptionSummary) {
        this.descriptionSummary = descriptionSummary;
    }
    public String getProductSelectDisplay() {
        return productSelectDisplay;
    }
    public void setProductSelectDisplay(String productSelectDisplay) {
        this.productSelectDisplay = productSelectDisplay;
    }
    public Long getProductProperPrice() {
        return productProperPrice;
    }
    public void setProductProperPrice(Long productProperPrice) {
        this.productProperPrice = productProperPrice;
    }
    public String getCategoryName() {
        return categoryName;
    }
    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }
    public String getDescriptionRemarks() {
        return descriptionRemarks;
    }
    public void setDescriptionRemarks(String descriptionRemarks) {
        this.descriptionRemarks = descriptionRemarks;
    }
    public Long getProductProperPriceFrom() {
        return productProperPriceFrom;
    }
    public void setProductProperPriceFrom(Long productProperPriceFrom) {
        this.productProperPriceFrom = productProperPriceFrom;
    }
    public Long getProductProperPriceTo() {
        return productProperPriceTo;
    }
    public void setProductProperPriceTo(Long productProperPriceTo) {
        this.productProperPriceTo = productProperPriceTo;
    }
    /**
     * @return the modelSort
     */
    public Integer getModelSort() {
        return modelSort;
    }
    /**
     * @param modelSort the modelSort to set
     */
    public void setModelSort(Integer modelSort) {
        this.modelSort = modelSort;
    }
    /**
     * @return the categoryCode
     */
    public String getCategoryCode() {
        return categoryCode;
    }
    /**
     * @param categoryCode the categoryCode to set
     */
    public void setCategoryCode(String categoryCode) {
        this.categoryCode = categoryCode;
    }
    /**
     * @return the attributeCode
     */
    public String getAttributeCode() {
        return attributeCode;
    }
    /**
     * @param attributeCode the attributeCode to set
     */
    public void setAttributeCode(String attributeCode) {
        this.attributeCode = attributeCode;
    }
    /**
     * @return the matterRegistrationFlg
     */
    public Integer getMatterRegistrationFlg() {
        return matterRegistrationFlg;
    }
    /**
     * @param matterRegistrationFlg the matterRegistrationFlg to set
     */
    public void setMatterRegistrationFlg(Integer matterRegistrationFlg) {
        this.matterRegistrationFlg = matterRegistrationFlg;
    }
    /**
     * @return the matterFactoryErrorCountGeneral
     */
    public Integer getMatterFactoryErrorCountGeneral() {
        return matterFactoryErrorCountGeneral;
    }
    /**
     * @param matterFactoryErrorCountGeneral the matterFactoryErrorCountGeneral to set
     */
    public void setMatterFactoryErrorCountGeneral(
            Integer matterFactoryErrorCountGeneral) {
        this.matterFactoryErrorCountGeneral = matterFactoryErrorCountGeneral;
    }
    /**
     * @return the matterFactoryErrorCountCategory
     */
    public Integer getMatterFactoryErrorCountCategory() {
        return matterFactoryErrorCountCategory;
    }
    /**
     * @param matterFactoryErrorCountCategory the matterFactoryErrorCountCategory to set
     */
    public void setMatterFactoryErrorCountCategory(
            Integer matterFactoryErrorCountCategory) {
        this.matterFactoryErrorCountCategory = matterFactoryErrorCountCategory;
    }
    /**
     * @return the matterFactoryErrorCountModel
     */
    public Integer getMatterFactoryErrorCountModel() {
        return matterFactoryErrorCountModel;
    }
    /**
     * @param matterFactoryErrorCountModel the matterFactoryErrorCountModel to set
     */
    public void setMatterFactoryErrorCountModel(Integer matterFactoryErrorCountModel) {
        this.matterFactoryErrorCountModel = matterFactoryErrorCountModel;
    }
    /**
     * @return the matterFactoryErrorCountAttribute
     */
    public Integer getMatterFactoryErrorCountAttribute() {
        return matterFactoryErrorCountAttribute;
    }
    /**
     * @param matterFactoryErrorCountAttribute the matterFactoryErrorCountAttribute to set
     */
    public void setMatterFactoryErrorCountAttribute(
            Integer matterFactoryErrorCountAttribute) {
        this.matterFactoryErrorCountAttribute = matterFactoryErrorCountAttribute;
    }
    /**
     * @return the showMatterRegistrationFlg
     */
    public String getShowMatterRegistrationFlg() {
        return showMatterRegistrationFlg;
    }
    /**
     * @param showMatterRegistrationFlg the showMatterRegistrationFlg to set
     */
    public void setShowMatterRegistrationFlg(String showMatterRegistrationFlg) {
        this.showMatterRegistrationFlg = showMatterRegistrationFlg;
    }
    /**
     * @return the descriptionSentence
     */
    public String getDescriptionSentence() {
        return descriptionSentence;
    }
    /**
     * @param descriptionSentence the descriptionSentence to set
     */
    public void setDescriptionSentence(String descriptionSentence) {
        this.descriptionSentence = descriptionSentence;
    }
    /**
     * @return the descriptionCaution
     */
    public String getDescriptionCaution() {
        return descriptionCaution;
    }
    /**
     * @param descriptionCaution the descriptionCaution to set
     */
    public void setDescriptionCaution(String descriptionCaution) {
        this.descriptionCaution = descriptionCaution;
    }
    /**
     * @return the supplierPricePrice
     */
    public Long getSupplierPricePrice() {
        return supplierPricePrice;
    }
    /**
     * @param supplierPricePrice the supplierPricePrice to set
     */
    public void setSupplierPricePrice(Long supplierPricePrice) {
        this.supplierPricePrice = supplierPricePrice;
    }
    /**
     * @return the supplierRate
     */
    public Float getSupplierRate() {
        return supplierRate;
    }
    /**
     * @param supplierRate the supplierRate to set
     */
    public void setSupplierRate(Float supplierRate) {
        this.supplierRate = supplierRate;
    }
    /**
     * @return the productGroupCode
     */
    public Long getProductGroupCode() {
        return productGroupCode;
    }
    /**
     * @param productGroupCode the productGroupCode to set
     */
    public void setProductGroupCode(Long productGroupCode) {
        this.productGroupCode = productGroupCode;
    }
    /**
     * @return the lstSelectOfProduct
     */
    public List<EntTblFactoryProductSelect> getLstSelectOfProduct() {
        return lstSelectOfProduct;
    }
    /**
     * @param lstSelectOfProduct the lstSelectOfProduct to set
     */
    public void setLstSelectOfProduct(List<EntTblFactoryProductSelect> lstSelectOfProduct) {
        this.lstSelectOfProduct = lstSelectOfProduct;
    }
	/**
	 * @return the idRow
	 */
	public String getIdRow() {
		return idRow;
	}
	/**
	 * @param idRow the idRow to set
	 */
	public void setIdRow(String idRow) {
		this.idRow = idRow;
	}
	/**
	 * @return the crudFlg
	 */
	public String getCrudFlg() {
		return crudFlg;
	}
	/**
	 * @param crudFlg the crudFlg to set
	 */
	public void setCrudFlg(String crudFlg) {
		this.crudFlg = crudFlg;
	}
    /**
     * @return the productGeneralCheckFlg
     */
    public Integer getProductGeneralCheckFlg() {
        return productGeneralCheckFlg;
    }
    /**
     * @param productGeneralCheckFlg the productGeneralCheckFlg to set
     */
    public void setProductGeneralCheckFlg(Integer productGeneralCheckFlg) {
        this.productGeneralCheckFlg = productGeneralCheckFlg;
    }
    /**
     * @return the productCategoryCheckFlg
     */
    public Integer getProductCategoryCheckFlg() {
        return productCategoryCheckFlg;
    }
    /**
     * @param productCategoryCheckFlg the productCategoryCheckFlg to set
     */
    public void setProductCategoryCheckFlg(Integer productCategoryCheckFlg) {
        this.productCategoryCheckFlg = productCategoryCheckFlg;
    }
    /**
     * @return the productAttributeCheckFlg
     */
    public Integer getProductAttributeCheckFlg() {
        return productAttributeCheckFlg;
    }
    /**
     * @param productAttributeCheckFlg the productAttributeCheckFlg to set
     */
    public void setProductAttributeCheckFlg(Integer productAttributeCheckFlg) {
        this.productAttributeCheckFlg = productAttributeCheckFlg;
    }

    /**
     * @return the siireMarumeCode
     */
    public Integer getSiireMarumeCode() {
        return siireMarumeCode;
    }
    /**
     * @param siireMarumeCode the siireMarumeCode to set
     */
    public void setSiireMarumeCode(Integer siireMarumeCode) {
        this.siireMarumeCode = siireMarumeCode;
    }
    /**
     * @return the fitModelMaker
     */
    public String getFitModelMaker() {
        return fitModelMaker;
    }
    /**
     * @param fitModelMaker the fitModelMaker to set
     */
    public void setFitModelMaker(String fitModelMaker) {
        this.fitModelMaker = fitModelMaker;
    }
    /**
     * @return the fitModelModel
     */
    public String getFitModelModel() {
        return fitModelModel;
    }
    /**
     * @param fitModelModel the fitModelModel to set
     */
    public void setFitModelModel(String fitModelModel) {
        this.fitModelModel = fitModelModel;
    }
    /**
     * @return the masterMisMatch
     */
    public String getMasterMisMatch() {
        return masterMisMatch;
    }
    /**
     * @param masterMisMatch the masterMisMatch to set
     */
    public void setMasterMisMatch(String masterMisMatch) {
        this.masterMisMatch = masterMisMatch;
    }
    /**
     * @return the masterMisMatchFlg
     */
    public Integer getMasterMisMatchFlg() {
        return masterMisMatchFlg;
    }
    /**
     * @param masterMisMatchFlg the masterMisMatchFlg to set
     */
    public void setMasterMisMatchFlg(Integer masterMisMatchFlg) {
        this.masterMisMatchFlg = masterMisMatchFlg;
    }
    /**
     * @return the updatedUserName
     */
    public String getUpdatedUserName() {
        return updatedUserName;
    }
    /**
     * @param updatedUserName the updatedUserName to set
     */
    public void setUpdatedUserName(String updatedUserName) {
        this.updatedUserName = updatedUserName;
    }
	/**
	 * @return the flgErrorGeneral
	 */
	public Integer getFlgErrorGeneral() {
		return flgErrorGeneral;
	}
	/**
	 * @param flgErrorGeneral the flgErrorGeneral to set
	 */
	public void setFlgErrorGeneral(Integer flgErrorGeneral) {
		this.flgErrorGeneral = flgErrorGeneral;
	}
	/**
	 * @return the flgErrorCategory
	 */
	public Integer getFlgErrorCategory() {
		return flgErrorCategory;
	}
	/**
	 * @param flgErrorCategory the flgErrorCategory to set
	 */
	public void setFlgErrorCategory(Integer flgErrorCategory) {
		this.flgErrorCategory = flgErrorCategory;
	}
	/**
	 * @return the flgErrorModel
	 */
	public Integer getFlgErrorModel() {
		return flgErrorModel;
	}
	/**
	 * @param flgErrorModel the flgErrorModel to set
	 */
	public void setFlgErrorModel(Integer flgErrorModel) {
		this.flgErrorModel = flgErrorModel;
	}
	/**
	 * @return the supplierReleaseDate
	 */
	public String getSupplierReleaseDate() {
		return supplierReleaseDate;
	}
	/**
	 * @param supplierReleaseDate the supplierReleaseDate to set
	 */
	public void setSupplierReleaseDate(String supplierReleaseDate) {
		this.supplierReleaseDate = supplierReleaseDate;
	}
    /**
     * @return the productCheck1Flg
     */
    public Integer getProductCheck1Flg() {
        return productCheck1Flg;
    }
    /**
     * @param productCheck1Flg the productCheck1Flg to set
     */
    public void setProductCheck1Flg(Integer productCheck1Flg) {
        this.productCheck1Flg = productCheck1Flg;
    }
    /**
     * @return the productCheck1Date
     */
    public Date getProductCheck1Date() {
        if (this.productCheck1Date != null) {
            return (Date) productCheck1Date.clone();
        }
        return null;
    }
    /**
     * @param productCheck1Date the productCheck1Date to set
     */
    public void setProductCheck1Date(Date productCheck1Date) {
        if (productCheck1Date != null) {
            this.productCheck1Date = (Date) productCheck1Date.clone();
        } else {
            this.productCheck1Date = null;
        }
    }
    /**
     * @return the productCheck1UserId
     */
    public String getProductCheck1UserId() {
        return productCheck1UserId;
    }
    /**
     * @param productCheck1UserId the productCheck1UserId to set
     */
    public void setProductCheck1UserId(String productCheck1UserId) {
        this.productCheck1UserId = productCheck1UserId;
    }
    /**
     * @return the productCheck2Flg
     */
    public Integer getProductCheck2Flg() {
        return productCheck2Flg;
    }
    /**
     * @param productCheck2Flg the productCheck2Flg to set
     */
    public void setProductCheck2Flg(Integer productCheck2Flg) {
        this.productCheck2Flg = productCheck2Flg;
    }
    /**
     * @return the productCheck2Date
     */
    public Date getProductCheck2Date() {
        if (this.productCheck2Date != null) {
            return (Date) productCheck2Date.clone();
        }
        return null;
    }
    /**
     * @param productCheck2Date the productCheck2Date to set
     */
    public void setProductCheck2Date(Date productCheck2Date) {
        if (productCheck2Date != null) {
            this.productCheck2Date = (Date) productCheck2Date.clone();
        } else {
            this.productCheck2Date = null;
        }
    }
    
    /**
     * @return the productCheck2UserId
     */
    public String getProductCheck2UserId() {
        return productCheck2UserId;
    }
    /**
     * @param productCheck2UserId the productCheck2UserId to set
     */
    public void setProductCheck2UserId(String productCheck2UserId) {
        this.productCheck2UserId = productCheck2UserId;
    }
    /**
     * @return the productCheck1UserLastName
     */
    public String getProductCheck1UserLastName() {
        return productCheck1UserLastName;
    }
    /**
     * @param productCheck1UserLastName the productCheck1UserLastName to set
     */
    public void setProductCheck1UserLastName(String productCheck1UserLastName) {
        this.productCheck1UserLastName = productCheck1UserLastName;
    }
    /**
     * @return the productCheck1UserFirstName
     */
    public String getProductCheck1UserFirstName() {
        return productCheck1UserFirstName;
    }
    /**
     * @param productCheck1UserFirstName the productCheck1UserFirstName to set
     */
    public void setProductCheck1UserFirstName(String productCheck1UserFirstName) {
        this.productCheck1UserFirstName = productCheck1UserFirstName;
    }
    /**
     * @return the productCheck2UserLastName
     */
    public String getProductCheck2UserLastName() {
        return productCheck2UserLastName;
    }
    /**
     * @param productCheck2UserLastName the productCheck2UserLastName to set
     */
    public void setProductCheck2UserLastName(String productCheck2UserLastName) {
        this.productCheck2UserLastName = productCheck2UserLastName;
    }
    /**
     * @return the productCheck2UserFirstName
     */
    public String getProductCheck2UserFirstName() {
        return productCheck2UserFirstName;
    }
    /**
     * @param productCheck2UserFirstName the productCheck2UserFirstName to set
     */
    public void setProductCheck2UserFirstName(String productCheck2UserFirstName) {
        this.productCheck2UserFirstName = productCheck2UserFirstName;
    }
	public Long getFirstCheckCount() {
		return firstCheckCount;
	}
	public void setFirstCheckCount(Long firstCheckCount) {
		this.firstCheckCount = firstCheckCount;
	}
	public Long getSecondCheckCount() {
		return secondCheckCount;
	}
	public void setSecondCheckCount(Long secondCheckCount) {
		this.secondCheckCount = secondCheckCount;
	}
	public Long getAllProductCount() {
		return allProductCount;
	}
	public void setAllProductCount(Long allProductCount) {
		this.allProductCount = allProductCount;
	}
	public Long getAllMatterCount() {
		return allMatterCount;
	}
	public void setAllMatterCount(Long allMatterCount) {
		this.allMatterCount = allMatterCount;
	}
	public String getMatterName() {
		return matterName;
	}
	public void setMatterName(String matterName) {
		this.matterName = matterName;
	}
	public String getUserFirstName() {
		return userFirstName;
	}
	public void setUserFirstName(String userFirstName) {
		this.userFirstName = userFirstName;
	}
	public String getUserLastName() {
		return userLastName;
	}
	public void setUserLastName(String userLastName) {
		this.userLastName = userLastName;
	}
}
