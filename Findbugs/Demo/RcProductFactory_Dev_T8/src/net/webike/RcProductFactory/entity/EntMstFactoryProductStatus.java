/************************************************************************
 * file name	： EntMstFactoryProductStatus.java
 * author		： Nguyen.Chuong
 * version		： 1.0.0
 * date created	： 25 Oct 2013
 * date updated	： 25 Oct 2013
 * description	： Entity for mst_factory_product_status table.
 *
 ***********************************************************************/
package net.webike.RcProductFactory.entity;


/**
 * EntMstFactoryProductStatus.
 */
public class EntMstFactoryProductStatus {
    private String productStatusCode;
    private String productStatusName;
    /**
     * @return the productStatusCode
     */
    public String getProductStatusCode() {
        return productStatusCode;
    }
    /**
     * @param productStatusCode the productStatusCode to set
     */
    public void setProductStatusCode(String productStatusCode) {
        this.productStatusCode = productStatusCode;
    }
    /**
     * @return the productStatusName
     */
    public String getProductStatusName() {
        return productStatusName;
    }
    /**
     * @param productStatusName the productStatusName to set
     */
    public void setProductStatusName(String productStatusName) {
        this.productStatusName = productStatusName;
    }
}

