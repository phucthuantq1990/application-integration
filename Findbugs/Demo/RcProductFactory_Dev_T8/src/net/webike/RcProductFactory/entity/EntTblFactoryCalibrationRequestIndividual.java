/**
 * EntTblFactoryCalibrationRequestIndividual.
 */
package net.webike.RcProductFactory.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * @author @rcv!dau.phuong
 *
 */
public class EntTblFactoryCalibrationRequestIndividual implements Serializable {

    private static final long serialVersionUID = -3753450740775031228L;

    private Long calibrationRequestIndividualCode;
    private Long calibrationRequestIndividualProductId;
    private String calibrationRequestIndividualValue;
    private Integer calibrationRequestIndividualTypeCode;
    private String calibrationRequestIndividualTypeName;
    private Date calibrationRequestIndividualRequestDate;
    private String calibrationRequestIndividualRequestDateStr;
    private String calibrationRequestIndividualRequestUserId;
    private String calibrationRequestIndividualRequestUserName;
    private String userLastName;
    private String userFirstName;
    private Integer calibrationRequestIndividualFinishFlg;
    private Date calibrationRequestIndividualFinishDate;
    private String calibrationRequestIndividualFinishUserId;
    private String calibrationRequestIndividualFinishUserName;

    private String calibrationRequestIndividualRequestDateFrom;
    private String calibrationRequestIndividualRequestDateTo;
    private String calibrationRequestIndividualFinishDateFrom;
    private String calibrationRequestIndividualFinishDateTo;
    private String calibrationRequestIndividualEditURL;
    private String sortField;
    private String sortDir;
    /**
     * @return the calibrationRequestIndividualCode
     */
    public Long getCalibrationRequestIndividualCode() {
        return calibrationRequestIndividualCode;
    }
    /**
     * @param calibrationRequestIndividualCode the calibrationRequestIndividualCode to set
     */
    public void setCalibrationRequestIndividualCode(Long calibrationRequestIndividualCode) {
        this.calibrationRequestIndividualCode = calibrationRequestIndividualCode;
    }
    /**
     * @return the calibrationRequestIndividualProductId
     */
    public Long getCalibrationRequestIndividualProductId() {
        return calibrationRequestIndividualProductId;
    }
    /**
     * @param calibrationRequestIndividualProductId the calibrationRequestIndividualProductId to set
     */
    public void setCalibrationRequestIndividualProductId(Long calibrationRequestIndividualProductId) {
        this.calibrationRequestIndividualProductId = calibrationRequestIndividualProductId;
    }
    /**
     * @return the calibrationRequestIndividualValue
     */
    public String getCalibrationRequestIndividualValue() {
        return calibrationRequestIndividualValue;
    }
    /**
     * @param calibrationRequestIndividualValue the calibrationRequestIndividualValue to set
     */
    public void setCalibrationRequestIndividualValue(String calibrationRequestIndividualValue) {
        this.calibrationRequestIndividualValue = calibrationRequestIndividualValue;
    }
    /**
     * @return the calibrationRequestIndividualTypeCode
     */
    public Integer getCalibrationRequestIndividualTypeCode() {
        return calibrationRequestIndividualTypeCode;
    }
    /**
     * @param calibrationRequestIndividualTypeCode the calibrationRequestIndividualTypeCode to set
     */
    public void setCalibrationRequestIndividualTypeCode(Integer calibrationRequestIndividualTypeCode) {
        this.calibrationRequestIndividualTypeCode = calibrationRequestIndividualTypeCode;
    }
    /**
     * @return the calibrationRequestIndividualRequestDate
     */
    public Date getCalibrationRequestIndividualRequestDate() {
        if (this.calibrationRequestIndividualRequestDate != null) {
            return (Date) calibrationRequestIndividualRequestDate.clone();
        }
        return null;
    }
    /**
     * @param calibrationRequestIndividualRequestDate the calibrationRequestIndividualRequestDate to set
     */
    public void setCalibrationRequestIndividualRequestDate(Date calibrationRequestIndividualRequestDate) {
        if (calibrationRequestIndividualRequestDate != null) {
            this.calibrationRequestIndividualRequestDate = (Date) calibrationRequestIndividualRequestDate.clone();
        } else {
            this.calibrationRequestIndividualRequestDate = null;
        }
    }
    /**
     * @return the calibrationRequestIndividualRequestUserId
     */
    public String getCalibrationRequestIndividualRequestUserId() {
        return calibrationRequestIndividualRequestUserId;
    }
    /**
     * @param calibrationRequestIndividualRequestUserId the calibrationRequestIndividualRequestUserId to set
     */
    public void setCalibrationRequestIndividualRequestUserId(String calibrationRequestIndividualRequestUserId) {
        this.calibrationRequestIndividualRequestUserId = calibrationRequestIndividualRequestUserId;
    }
    /**
     * @return the calibrationRequestIndividualFinishFlg
     */
    public Integer getCalibrationRequestIndividualFinishFlg() {
        return calibrationRequestIndividualFinishFlg;
    }
    /**
     * @param calibrationRequestIndividualFinishFlg the calibrationRequestIndividualFinishFlg to set
     */
    public void setCalibrationRequestIndividualFinishFlg(Integer calibrationRequestIndividualFinishFlg) {
        this.calibrationRequestIndividualFinishFlg = calibrationRequestIndividualFinishFlg;
    }
    /**
     * @return the calibrationRequestIndividualFinishDate
     */
    public Date getCalibrationRequestIndividualFinishDate() {
        if (this.calibrationRequestIndividualFinishDate != null) {
            return (Date) calibrationRequestIndividualFinishDate.clone();
        }
        return null;
    }
    /**
     * @param calibrationRequestIndividualFinishDate the calibrationRequestIndividualFinishDate to set
     */
    public void setCalibrationRequestIndividualFinishDate(Date calibrationRequestIndividualFinishDate) {
        if (calibrationRequestIndividualFinishDate != null) {
            this.calibrationRequestIndividualFinishDate = (Date) calibrationRequestIndividualFinishDate.clone();
        } else {
            this.calibrationRequestIndividualFinishDate = null;
        }
    }
    /**
     * @return the calibrationRequestIndividualFinishUserId
     */
    public String getCalibrationRequestIndividualFinishUserId() {
        return calibrationRequestIndividualFinishUserId;
    }
    /**
     * @param calibrationRequestIndividualFinishUserId the calibrationRequestIndividualFinishUserId to set
     */
    public void setCalibrationRequestIndividualFinishUserId(String calibrationRequestIndividualFinishUserId) {
        this.calibrationRequestIndividualFinishUserId = calibrationRequestIndividualFinishUserId;
    }
    /**
     * @return the calibrationRequestIndividualTypeName
     */
    public String getCalibrationRequestIndividualTypeName() {
        return calibrationRequestIndividualTypeName;
    }
    /**
     * @param calibrationRequestIndividualTypeName the calibrationRequestIndividualTypeName to set
     */
    public void setCalibrationRequestIndividualTypeName(String calibrationRequestIndividualTypeName) {
        this.calibrationRequestIndividualTypeName = calibrationRequestIndividualTypeName;
    }
    /**
     * @return the calibrationRequestIndividualRequestUserName
     */
    public String getCalibrationRequestIndividualRequestUserName() {
        return calibrationRequestIndividualRequestUserName;
    }
    /**
     * @param calibrationRequestIndividualRequestUserName the calibrationRequestIndividualRequestUserName to set
     */
    public void setCalibrationRequestIndividualRequestUserName(String calibrationRequestIndividualRequestUserName) {
        this.calibrationRequestIndividualRequestUserName = calibrationRequestIndividualRequestUserName;
    }
    /**
     * @return the calibrationRequestIndividualFinishUserName
     */
    public String getCalibrationRequestIndividualFinishUserName() {
        return calibrationRequestIndividualFinishUserName;
    }
    /**
     * @param calibrationRequestIndividualFinishUserName the calibrationRequestIndividualFinishUserName to set
     */
    public void setCalibrationRequestIndividualFinishUserName(String calibrationRequestIndividualFinishUserName) {
        this.calibrationRequestIndividualFinishUserName = calibrationRequestIndividualFinishUserName;
    }
    /**
     * @return the userLastName
     */
    public String getUserLastName() {
        return userLastName;
    }
    /**
     * @param userLastName the userLastName to set
     */
    public void setUserLastName(String userLastName) {
        this.userLastName = userLastName;
    }
    /**
     * @return the userFirstName
     */
    public String getUserFirstName() {
        return userFirstName;
    }
    /**
     * @param userFirstName the userFirstName to set
     */
    public void setUserFirstName(String userFirstName) {
        this.userFirstName = userFirstName;
    }
    /**
     * @return the calibrationRequestIndividualRequestDateFrom
     */
    public String getCalibrationRequestIndividualRequestDateFrom() {
        return calibrationRequestIndividualRequestDateFrom;
    }
    /**
     * @param calibrationRequestIndividualRequestDateFrom the calibrationRequestIndividualRequestDateFrom to set
     */
    public void setCalibrationRequestIndividualRequestDateFrom(String calibrationRequestIndividualRequestDateFrom) {
        this.calibrationRequestIndividualRequestDateFrom = calibrationRequestIndividualRequestDateFrom;
    }
    /**
     * @return the calibrationRequestIndividualRequestDateTo
     */
    public String getCalibrationRequestIndividualRequestDateTo() {
        return calibrationRequestIndividualRequestDateTo;
    }
    /**
     * @param calibrationRequestIndividualRequestDateTo the calibrationRequestIndividualRequestDateTo to set
     */
    public void setCalibrationRequestIndividualRequestDateTo(String calibrationRequestIndividualRequestDateTo) {
        this.calibrationRequestIndividualRequestDateTo = calibrationRequestIndividualRequestDateTo;
    }
    /**
     * @return the calibrationRequestIndividualFinishDateFrom
     */
    public String getCalibrationRequestIndividualFinishDateFrom() {
        return calibrationRequestIndividualFinishDateFrom;
    }
    /**
     * @param calibrationRequestIndividualFinishDateFrom the calibrationRequestIndividualFinishDateFrom to set
     */
    public void setCalibrationRequestIndividualFinishDateFrom(String calibrationRequestIndividualFinishDateFrom) {
        this.calibrationRequestIndividualFinishDateFrom = calibrationRequestIndividualFinishDateFrom;
    }
    /**
     * @return the calibrationRequestIndividualFinishDateTo
     */
    public String getCalibrationRequestIndividualFinishDateTo() {
        return calibrationRequestIndividualFinishDateTo;
    }
    /**
     * @param calibrationRequestIndividualFinishDateTo the calibrationRequestIndividualFinishDateTo to set
     */
    public void setCalibrationRequestIndividualFinishDateTo(String calibrationRequestIndividualFinishDateTo) {
        this.calibrationRequestIndividualFinishDateTo = calibrationRequestIndividualFinishDateTo;
    }
    /**
     * @return the calibrationRequestIndividualEditURL
     */
    public String getCalibrationRequestIndividualEditURL() {
        return calibrationRequestIndividualEditURL;
    }
    /**
     * @param calibrationRequestIndividualEditURL the calibrationRequestIndividualEditURL to set
     */
    public void setCalibrationRequestIndividualEditURL(String calibrationRequestIndividualEditURL) {
        this.calibrationRequestIndividualEditURL = calibrationRequestIndividualEditURL;
    }
    /**
     * @return the calibrationRequestIndividualRequestDateStr
     */
    public String getCalibrationRequestIndividualRequestDateStr() {
        return calibrationRequestIndividualRequestDateStr;
    }
    /**
     * @param calibrationRequestIndividualRequestDateStr the calibrationRequestIndividualRequestDateStr to set
     */
    public void setCalibrationRequestIndividualRequestDateStr(
            String calibrationRequestIndividualRequestDateStr) {
        this.calibrationRequestIndividualRequestDateStr = calibrationRequestIndividualRequestDateStr;
    }
	public String getSortField() {
		return sortField;
	}
	public void setSortField(String sortField) {
		this.sortField = sortField;
	}
	public String getSortDir() {
		return sortDir;
	}
	public void setSortDir(String sortDir) {
		this.sortDir = sortDir;
	}

}
