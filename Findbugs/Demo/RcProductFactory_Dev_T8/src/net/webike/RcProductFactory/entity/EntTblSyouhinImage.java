/************************************************************************
 * file name	： EntSyouhinImage.java
 * author		： Nguyen.Chuong
 * version		： 1.0.0
 * date created	： 2014/02/15
 * date updated	： 2014/02/15
 * description	： Entity EntSyouhinImage to mapp with rc_syouhin.tbl_syouhin_image.
 *
 ***********************************************************************/
package net.webike.RcProductFactory.entity;

/**
 * EntSyouhinImage.
 */
public class EntTblSyouhinImage {
    private long syouhinSysCode;
	private String image;
	private String imagePath;
	private String thumbnail;
	private String thumbnailPath;
	private String sort;

    /**
     * @return the syouhinSysCode
     */
    public long getSyouhinSysCode() {
        return syouhinSysCode;
    }
    /**
     * @param syouhinSysCode the syouhinSysCode to set
     */
    public void setSyouhinSysCode(long syouhinSysCode) {
        this.syouhinSysCode = syouhinSysCode;
    }
    /**
     * @return the image
     */
    public String getImage() {
        return image;
    }
    /**
     * @param image the image to set
     */
    public void setImage(String image) {
        this.image = image;
    }
    /**
     * @return the thumbnail
     */
    public String getThumbnail() {
        return thumbnail;
    }
    /**
     * @param thumbnail the thumbnail to set
     */
    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }
    /**
     * @return the sort
     */
    public String getSort() {
        return sort;
    }
    /**
     * @param sort the sort to set
     */
    public void setSort(String sort) {
        this.sort = sort;
    }
    /**
     * @return the imagePath
     */
    public String getImagePath() {
        return imagePath;
    }
    /**
     * @param imagePath the imagePath to set
     */
    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }
    /**
     * @return the thumbnailPath
     */
    public String getThumbnailPath() {
        return thumbnailPath;
    }
    /**
     * @param thumbnailPath the thumbnailPath to set
     */
    public void setThumbnailPath(String thumbnailPath) {
        this.thumbnailPath = thumbnailPath;
    }

}
