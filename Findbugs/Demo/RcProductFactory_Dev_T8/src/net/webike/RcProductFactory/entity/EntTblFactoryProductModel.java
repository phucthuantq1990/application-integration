/************************************************************************
 * file name	： EntTblFactoryProductModel.java
 * author		： Nguyen.Chuong
 * version		： 1.0.0
 * date created	： 29 Oct 2013
 * date updated	： 29 Oct 2013
 * description	： Entity for product model.
 *
 ***********************************************************************/
package net.webike.RcProductFactory.entity;

import java.util.Date;



/**
 * EntTblFactoryProductModel.
 */
public class EntTblFactoryProductModel {
    private String productCode;
    private String productMaker;
    private String productModel;
    private String productStyle;
    private String updateUserId;
    private Date   updateDate;
    private String sortField;
    private String sortDir;

    /**
     * @return the productCode
     */
    public String getProductCode() {
        return productCode;
    }
    /**
     * @param productCode the productCode to set
     */
    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }
    /**
     * @return the productMaker
     */
    public String getProductMaker() {
        return productMaker;
    }

    /**
     * @param productMaker the productMaker to set
     */
    public void setProductMaker(String productMaker) {
        this.productMaker = productMaker;
    }
    /**
     * @return the productModel
     */
    public String getProductModel() {
        return productModel;
    }
    /**
     * @param productModel the productModel to set
     */
    public void setProductModel(String productModel) {
        this.productModel = productModel;
    }
    /**
     * @return the productStyle
     */
    public String getProductStyle() {
        return productStyle;
    }
    /**
     * @param productStyle the productStyle to set
     */
    public void setProductStyle(String productStyle) {
        this.productStyle = productStyle;
    }
    /**
     * @return the updateUserId
     */
    public String getUpdateUserId() {
        return updateUserId;
    }
    /**
     * @param updateUserId the updateUserId to set
     */
    public void setUpdateUserId(String updateUserId) {
        this.updateUserId = updateUserId;
    }
    /**
     * @return the updateDate
     */
    public Date getUpdateDate() {
        if (updateDate != null) {
            return (Date) updateDate.clone();
        }
        return null;
    }
    /**
     * @param updateDate the updateDate to set
     */
    public void setUpdateDate(Date updateDate) {
        if (updateDate != null) {
            this.updateDate = (Date) updateDate.clone();
        } else {
            this.updateDate = null;
        }
    }
    /**
     * @return the sortField
     */
    public String getSortField() {
        return sortField;
    }
    /**
     * @param sortField the sortField to set
     */
    public void setSortField(String sortField) {
        this.sortField = sortField;
    }
    /**
     * @return the sortDir
     */
    public String getSortDir() {
        return sortDir;
    }
    /**
     * @param sortDir the sortDir to set
     */
    public void setSortDir(String sortDir) {
        this.sortDir = sortDir;
    }
}
