/**
 * Add comment later.
 */
package net.webike.RcProductFactory.entity;


/**
 * @author hoang.ho
 *
 */
public class EntMstCategory {

    /**
     * Category code.
     */
    private String categoryCode;
    /**
     * Category name.
     */
    private String categoryName;
    /**
     * Bunrui code.
     */
    private String bunruiCode;
    /**
     * Bunrui name.
     */
    private String bunruiName;
    /**
     * category Attribute Count.
     */
    private String categoryAttributeCount;
    /**
     * category Attribute Count From used for search category manage page.
     */
    private String categoryAttributeCountFrom;
    /**
     * category Attribute Count To used for search category manage page.
     */
    private String categoryAttributeCountTo;
    /**
     * category Del Flg.
     */
    private String categoryDelFlg;
    /**
     * updated On.
     */
    private String updatedOn;
    /**
     * updatedFrom used for search category manage page.
     */
    private String updatedFrom;
    /**
     * updatedTo used for search category manage page.
     */
    private String updatedTo;
    /**
     *  attribute Codes used for search category manage page.
     */
    private String attributeCodes;

    private String loginUserId;
    /**
     * category Codesused for search category manage page.
     */
    private String categoryCodes;

    /**
     * number record has been get.
     */
    private int limit;

    /**
     * offset for get list.
     */
    private int offset;

    /**
     * sort field name.
     */
    private String sortField;

    /**
     * sort by.
     */
    private String sortDir;

    /**
     * if search condition had chose.
     */
    private boolean filterFlg;

    /**
     * @return the categoryCode
     */
    public String getCategoryCode() {
        return categoryCode;
    }

    /**
     * @param categoryCode the categoryCode to set
     */
    public void setCategoryCode(String categoryCode) {
        this.categoryCode = categoryCode;
    }

    /**
     * @return the categoryName
     */
    public String getCategoryName() {
        return categoryName;
    }

    /**
     * @param categoryName the categoryName to set
     */
    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    /**
     * @return the bunruiCode
     */
    public String getBunruiCode() {
        return bunruiCode;
    }

    /**
     * @param bunruiCode the bunruiCode to set
     */
    public void setBunruiCode(String bunruiCode) {
        this.bunruiCode = bunruiCode;
    }

    /**
     * @return the bunruiName
     */
    public String getBunruiName() {
        return bunruiName;
    }

    /**
     * @param bunruiName the bunruiName to set
     */
    public void setBunruiName(String bunruiName) {
        this.bunruiName = bunruiName;
    }

    /**
     * @return the categoryAttributeCount
     */
    public String getCategoryAttributeCount() {
        return categoryAttributeCount;
    }

    /**
     * @param categoryAttributeCount the categoryAttributeCount to set
     */
    public void setCategoryAttributeCount(String categoryAttributeCount) {
        this.categoryAttributeCount = categoryAttributeCount;
    }

    /**
     * @return the categoryDelFlg
     */
    public String getCategoryDelFlg() {
        return categoryDelFlg;
    }

    /**
     * @param categoryDelFlg the categoryDelFlg to set
     */
    public void setCategoryDelFlg(String categoryDelFlg) {
        this.categoryDelFlg = categoryDelFlg;
    }

    /**
     * @return the updatedOn
     */
    public String getUpdatedOn() {
        return updatedOn;
    }

    /**
     * @param updatedOn the updatedOn to set
     */
    public void setUpdatedOn(String updatedOn) {
        this.updatedOn = updatedOn;
    }

    /**
     * @return the updatedFrom
     */
    public String getUpdatedFrom() {
        return updatedFrom;
    }

    /**
     * @param updatedFrom the updatedFrom to set
     */
    public void setUpdatedFrom(String updatedFrom) {
        this.updatedFrom = updatedFrom;
    }

    /**
     * @return the updatedTo
     */
    public String getUpdatedTo() {
        return updatedTo;
    }

    /**
     * @param updatedTo the updatedTo to set
     */
    public void setUpdatedTo(String updatedTo) {
        this.updatedTo = updatedTo;
    }

    /**
     * @return the limit
     */
    public int getLimit() {
        return limit;
    }

    /**
     * @param limit the limit to set
     */
    public void setLimit(int limit) {
        this.limit = limit;
    }

    /**
     * @return the offset
     */
    public int getOffset() {
        return offset;
    }

    /**
     * @param offset the offset to set
     */
    public void setOffset(int offset) {
        this.offset = offset;
    }

    /**
     * @return the filterFlg
     */
    public boolean getFilterFlg() {
        return filterFlg;
    }

    /**
     * @param filterFlg the filterFlg to set
     */
    public void setFilterFlg(boolean filterFlg) {
        this.filterFlg = filterFlg;
    }

    /**
     * @return the loginUserId
     */
    public String getLoginUserId() {
        return loginUserId;
    }

    /**
     * @param loginUserId the loginUserId to set
     */
    public void setLoginUserId(String loginUserId) {
        this.loginUserId = loginUserId;
    }

    /**
     * @return the attributeCodes
     */
    public String getAttributeCodes() {
        return attributeCodes;
    }

    /**
     * @param attributeCodes the attributeCodes to set
     */
    public void setAttributeCodes(String attributeCodes) {
        this.attributeCodes = attributeCodes;
    }

    /**
     * @return the categoryCodes
     */
    public String getCategoryCodes() {
        return categoryCodes;
    }

    /**
     * @param categoryCodes the categoryCodes to set
     */
    public void setCategoryCodes(String categoryCodes) {
        this.categoryCodes = categoryCodes;
    }

    /**
     * @return the categoryAttributeCountFrom
     */
    public String getCategoryAttributeCountFrom() {
        return categoryAttributeCountFrom;
    }

    /**
     * @param categoryAttributeCountFrom the categoryAttributeCountFrom to set
     */
    public void setCategoryAttributeCountFrom(String categoryAttributeCountFrom) {
        this.categoryAttributeCountFrom = categoryAttributeCountFrom;
    }

    /**
     * @return the categoryAttributeCountTo
     */
    public String getCategoryAttributeCountTo() {
        return categoryAttributeCountTo;
    }

    /**
     * @param categoryAttributeCountTo the categoryAttributeCountTo to set
     */
    public void setCategoryAttributeCountTo(String categoryAttributeCountTo) {
        this.categoryAttributeCountTo = categoryAttributeCountTo;
    }

    /**
     * @return the sortField
     */
    public String getSortField() {
        return sortField;
    }

    /**
     * @param sortField the sortField to set
     */
    public void setSortField(String sortField) {
        this.sortField = sortField;
    }

    /**
     * @return the sortDir
     */
    public String getSortDir() {
        return sortDir;
    }

    /**
     * @param sortDir the sortDir to set
     */
    public void setSortDir(String sortDir) {
        this.sortDir = sortDir;
    }

}
