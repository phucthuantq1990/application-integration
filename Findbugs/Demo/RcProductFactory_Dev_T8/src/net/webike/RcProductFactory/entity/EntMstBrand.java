/**
 * EntMstBrand mapping with table mst_brand in rc_syouhin DB.
 */
package net.webike.RcProductFactory.entity;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang.StringUtils;



/**
 * @author Nguyen.Chuong
 *
 */
public class EntMstBrand {
    //brand_code column
    private int brandCode;

    //brand_name column
    private String name;

    //hyouji_name1 column
    private String hyoujiName1;
    //hyouji_name2 column
    private String hyoujiName2;
    //hyouji_name3 column
    private String hyoujiName3;
    //sort_kana_name column
    private String sortKanaName;
    //sentence column
    private String sentence;
    //logo column
    /* BOE Tran.Thanh Fix Bug 70 12014/03/17 */
    //private String logo;
    /* EOE Tran.Thanh Fix Bug 70 12014/03/17 */
    private String logo = "";
    //url column
    private String url;
    //rss column
    private String rss;
    //brand_marume_code colum
    private int brandMarumeCode;
    //tantou_user_id1 column
    private String tantouUserId1;
    //touroku_user_id column
    private String tourokuUserId;
    //kousin_user_id column
    private String kousinUserId;
    //kousin_user_name value: mst_factory_user.user_last_name + ' ' + mst_factory_user.user_first_name
    private String kousinUserName;

    //del_flg value: get del_flg column: IF del_flg = 1 THEN 'No' ELSE 'Yes'
    private int delFlg;

    //kousin_user_date column: format 'YYYY/MM/DD hh:mm:ss'
    private Date kousinDate;

    private String tbUser;
    private String kousinDateFrom;
    private String kousinDateTo;
    //Sort column name
    private String sortField;
    //Sort asc/ders
    private String sortDir;

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the hyoujiName1
     */
    public String getHyoujiName1() {
        return hyoujiName1;
    }

    /**
     * @param hyoujiName1 the hyoujiName1 to set
     */
    public void setHyoujiName1(String hyoujiName1) {
        this.hyoujiName1 = hyoujiName1;
    }

    /**
     * @return the hyoujiName2
     */
    public String getHyoujiName2() {
        return hyoujiName2;
    }

    /**
     * @param hyoujiName2 the hyoujiName2 to set
     */
    public void setHyoujiName2(String hyoujiName2) {
        this.hyoujiName2 = hyoujiName2;
    }

    /**
     * @return the hyoujiName3
     */
    public String getHyoujiName3() {
        return hyoujiName3;
    }

    /**
     * @param hyoujiName3 the hyoujiName3 to set
     */
    public void setHyoujiName3(String hyoujiName3) {
        this.hyoujiName3 = hyoujiName3;
    }

    /**
	 * @return the sortKanaName
	 */
	public String getSortKanaName() {
		return sortKanaName;
	}

	/**
	 * @param sortKanaName the sortKanaName to set
	 */
	public void setSortKanaName(String sortKanaName) {
		this.sortKanaName = sortKanaName;
	}

	/**
	 * @return the sentence
	 */
	public String getSentence() {
		return sentence;
	}

	/**
	 * @param sentence the sentence to set
	 */
	public void setSentence(String sentence) {
		this.sentence = sentence;
	}

	/**
	 * @return the logo
	 */
	public String getLogo() {
		return logo;
	}

	/**
	 * @param logo the logo to set
	 */
	public void setLogo(String logo) {
		this.logo = logo;
	}

	/**
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * @param url the url to set
	 */
	public void setUrl(String url) {
		this.url = url;
	}

	/**
	 * @return the rss
	 */
	public String getRss() {
		return rss;
	}

	/**
	 * @param rss the rss to set
	 */
	public void setRss(String rss) {
		this.rss = rss;
	}

	/**
	 * @return the tantouUserId1
	 */
	public String getTantouUserId1() {
		return tantouUserId1;
	}

	/**
	 * @param tantouUserId1 the tantouUserId1 to set
	 */
	public void setTantouUserId1(String tantouUserId1) {
		this.tantouUserId1 = tantouUserId1;
	}

    /**
	 * @return the tourokuUserId
	 */
	public String getTourokuUserId() {
		return tourokuUserId;
	}

	/**
	 * @param tourokuUserId the tourokuUserId to set
	 */
	public void setTourokuUserId(String tourokuUserId) {
		this.tourokuUserId = tourokuUserId;
	}

	/**
	 * @return the kousinUserId
	 */
	public String getKousinUserId() {
		return kousinUserId;
	}

	/**
	 * @param kousinUserId the kousinUserId to set
	 */
	public void setKousinUserId(String kousinUserId) {
		this.kousinUserId = kousinUserId;
	}

	/**
     * @return the kousinUserName
     */
    public String getKousinUserName() {
        return kousinUserName;
    }

    /**
     * @param kousinUserName the kousinUserName to set
     */
    public void setKousinUserName(String kousinUserName) {
        this.kousinUserName = kousinUserName;
    }

    /**
     * @return the tbUser
     */
    public String getTbUser() {
        return tbUser;
    }

    /**
     * @param tbUser the tbUser to set
     */
    public void setTbUser(String tbUser) {
        this.tbUser = tbUser;
    }

    /**
     * @return the kousinDateFrom
     */
    public String getKousinDateFrom() {
        return kousinDateFrom;
    }

    /**
     * @param kousinDateFrom the kousinDateFrom to set
     */
    public void setKousinDateFrom(String kousinDateFrom) {
        this.kousinDateFrom = kousinDateFrom;
    }

    /**
     * @return the kousinDateTo
     */
    public String getKousinDateTo() {
        return kousinDateTo;
    }

    /**
     * @param kousinDateTo the kousinDateTo to set
     */
    public void setKousinDateTo(String kousinDateTo) {
        this.kousinDateTo = kousinDateTo;
    }

    /**
     * @return the sortDir
     */
    public String getSortDir() {
        return sortDir;
    }

    /**
     * @param sortDir the sortDir to set
     */
    public void setSortDir(String sortDir) {
        this.sortDir = sortDir;
    }

    /**
     * @return the sortField
     */
    public String getSortField() {
        return sortField;
    }

    /**
     * @param sortField the sortField to set
     */
    public void setSortField(String sortField) {
        if (StringUtils.equals(sortField, "delFlgString")) {
            sortField = "delFlg";
        } else if (StringUtils.equals(sortField, "kousinDateString")) {
            sortField = "kousinDate";
        }
        this.sortField = sortField;
    }

    /**
     * @return the brandCode
     */
    public int getBrandCode() {
        return brandCode;
    }

    /**
     * @param brandCode the brandCode to set
     */
    public void setBrandCode(int brandCode) {
        this.brandCode = brandCode;
    }

    /**
     * @return the brandMarumeCode
     */
    public int getBrandMarumeCode() {
        return brandMarumeCode;
    }

    /**
     * @param brandMarumeCode the brandMarumeCode to set
     */
    public void setBrandMarumeCode(int brandMarumeCode) {
        this.brandMarumeCode = brandMarumeCode;
    }

    /**
     * @return the delFlg
     */
    public int getDelFlg() {
        return delFlg;
    }

    /**
     * @param delFlg the delFlg to set
     */
    public void setDelFlg(int delFlg) {
        this.delFlg = delFlg;
    }

    /**
     * @return the kousinDate
     */
    public Date getKousinDate() {
        if (kousinDate != null) {
            return (Date) kousinDate.clone();
        }
        return null;
    }

    /**
     * @param kousinDate the kousinDate to set
     */
    public void setKousinDate(Date kousinDate) {
        if (kousinDate != null) {
            this.kousinDate = (Date) kousinDate.clone();
        } else {
            this.kousinDate = null;
        }
    }

    /************************************************************************
     * <b>Description:</b><br>
     * Get delFlag String: Yes or No.
     *
     * @author      Long Vu
     * @date        Jan 21, 2011
     * @return      String
     ************************************************************************/
    public String getDelFlgString() {
        String delFlgString = "";
        if (this.delFlg == 0) {
            delFlgString = "Yes";
        } else {
            delFlgString = "No";
        }
        return delFlgString;
    }

    /************************************************************************
     * <b>Description:</b><br>
     * Format date to String in formation: yyyy/MM/dd HH:mm:ss.
     *
     * @author      Long Vu
     * @date        Jan 22, 2011
     * @return      String
     ************************************************************************/
    public String getKousinDateString() {
        String result = "";
        if (this.kousinDate != null) {
            result = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(this.kousinDate);
        }
        return result;
    }

}
