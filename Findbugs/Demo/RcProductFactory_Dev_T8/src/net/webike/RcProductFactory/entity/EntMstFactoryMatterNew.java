/**
 * EntMstFactoryMatterNew.
 */
package net.webike.RcProductFactory.entity;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author Le.Dinh
 *
 */
public class EntMstFactoryMatterNew implements Serializable {

	private static final long serialVersionUID = 4103369161778592593L;

	private Long matterNo;
	private Integer matterRedmineIssueId;
	private Date matterRedmineUpdatedOn;
	private String matterName;
	private String matterChargeUserId;
	private Integer matterFactoryCount;
	private Integer matterFactoryErrorCountGeneral;
	private Integer matterFactoryErrorCountCategory;
	private Integer matterFactoryErrorCountModel;
	private Integer matterFactoryErrorCountAttribute;
	private Integer matterRegistrationFlg;
	private Integer matterDelFlg;
	private Date createdOn;
	private String createdUserId;
	private Date updatedOn;
	private String updatedUserId;
	private String managerName;

	//BOE #7208 Tran.Thanh : add date filter
	private String errorMessage;
	//EOE #7208 Tran.Thanh : add date filter

	/* Used for filter */
	private String sortField;
	private String sortDir;
    private String updatedOnFrom;
    private String updatedOnTo;
    private Integer matterFactoryCountFrom;
    private Integer matterFactoryCountTo;
    private Integer matterFactoryErrorCountGeneralFrom;
    private Integer matterFactoryErrorCountGeneralTo;
    private Integer matterFactoryErrorCountCategoryFrom;
    private Integer matterFactoryErrorCountCategoryTo;
    private Integer matterFactoryErrorCountModelFrom;
    private Integer matterFactoryErrorCountModelTo;
    private Integer matterFactoryErrorCountAttributeFrom;
    private Integer matterFactoryErrorCountAttributeTo;
    private String oldUpdateOn;
    public void setMatterFactoryErrorCountModelFrom(
			Integer matterFactoryErrorCountModelFrom) {
		this.matterFactoryErrorCountModelFrom = matterFactoryErrorCountModelFrom;
	}

    public Integer getMatterFactoryErrorCountModelFrom() {
		return matterFactoryErrorCountModelFrom;
	}

    public void setMatterFactoryErrorCountModelTo(
			Integer matterFactoryErrorCountModelTo) {
		this.matterFactoryErrorCountModelTo = matterFactoryErrorCountModelTo;
	}

    public Integer getMatterFactoryErrorCountModelTo() {
		return matterFactoryErrorCountModelTo;
	}

    public void setMatterFactoryCountFrom(Integer matterFactoryCountFrom) {
		this.matterFactoryCountFrom = matterFactoryCountFrom;
	}

    public Integer getMatterFactoryCountFrom() {
		return matterFactoryCountFrom;
	}

    public void setMatterFactoryCountTo(Integer matterFactoryCountTo) {
		this.matterFactoryCountTo = matterFactoryCountTo;
	}

    public Integer getMatterFactoryCountTo() {
		return matterFactoryCountTo;
	}

    public void setMatterFactoryErrorCountAttributeFrom(
			Integer matterFactoryErrorCountAttributeFrom) {
		this.matterFactoryErrorCountAttributeFrom = matterFactoryErrorCountAttributeFrom;
	}

    public Integer getMatterFactoryErrorCountAttributeFrom() {
		return matterFactoryErrorCountAttributeFrom;
	}

    public void setMatterFactoryErrorCountAttributeTo(
			Integer matterFactoryErrorCountAttributeTo) {
		this.matterFactoryErrorCountAttributeTo = matterFactoryErrorCountAttributeTo;
	}

    public Integer getMatterFactoryErrorCountAttributeTo() {
		return matterFactoryErrorCountAttributeTo;
	}

    public void setMatterFactoryErrorCountCategoryFrom(
			Integer matterFactoryErrorCountCategoryFrom) {
		this.matterFactoryErrorCountCategoryFrom = matterFactoryErrorCountCategoryFrom;
	}

    public void setMatterFactoryErrorCountCategoryTo(
			Integer matterFactoryErrorCountCategoryTo) {
		this.matterFactoryErrorCountCategoryTo = matterFactoryErrorCountCategoryTo;
	}

    public void setMatterFactoryErrorCountGeneralFrom(
			Integer matterFactoryErrorCountGeneralFrom) {
		this.matterFactoryErrorCountGeneralFrom = matterFactoryErrorCountGeneralFrom;
	}

    public Integer getMatterFactoryErrorCountCategoryFrom() {
		return matterFactoryErrorCountCategoryFrom;
	}

    public void setMatterFactoryErrorCountGeneralTo(
			Integer matterFactoryErrorCountGeneralTo) {
		this.matterFactoryErrorCountGeneralTo = matterFactoryErrorCountGeneralTo;
	}

    public Integer getMatterFactoryErrorCountCategoryTo() {
		return matterFactoryErrorCountCategoryTo;
	}

    public Integer getMatterFactoryErrorCountGeneralFrom() {
		return matterFactoryErrorCountGeneralFrom;
	}

    public Integer getMatterFactoryErrorCountGeneralTo() {
		return matterFactoryErrorCountGeneralTo;
	}

    public static long getSerialversionuid() {
		return serialVersionUID;
	}

    public void setUpdatedOnFrom(String updatedOnFrom) {
		this.updatedOnFrom = updatedOnFrom;
	}

    public String getUpdatedOnFrom() {
		return updatedOnFrom;
	}

    public void setUpdatedOnTo(String updatedOnTo) {
		this.updatedOnTo = updatedOnTo;
	}

    public String getUpdatedOnTo() {
		return updatedOnTo;
	}

	public void setSortDir(String sortDir) {
		this.sortDir = sortDir;
	}

	public String getSortDir() {
		return sortDir;
	}

	public void setSortField(String sortField) {
		this.sortField = sortField;
	}

	public String getSortField() {
		return sortField;
	}

	public void setManagerName(String managerName) {
		this.managerName = managerName;
	}

	public String getManagerName() {
		return managerName;
	}

    /**
     * @return the matterNo
     */
    public Long getMatterNo() {
        return matterNo;
    }
    /**
     * @param matterNo the matterNo to set
     */
    public void setMatterNo(Long matterNo) {
        this.matterNo = matterNo;
    }
    /**
     * @return the matterRedmineIssueId
     */
    public Integer getMatterRedmineIssueId() {
        return matterRedmineIssueId;
    }
    /**
     * @param matterRedmineIssueId the matterRedmineIssueId to set
     */
    public void setMatterRedmineIssueId(Integer matterRedmineIssueId) {
        this.matterRedmineIssueId = matterRedmineIssueId;
    }
    /**
     * @return the matterRedmineUpdatedOn
     */
    public Date getMatterRedmineUpdatedOn() {
        if (this.matterRedmineUpdatedOn != null) {
            return (Date) this.matterRedmineUpdatedOn.clone();
        }
        return null;
    }
    /**
     * @param matterRedmineUpdatedOn the matterRedmineUpdatedOn to set
     */
    public void setMatterRedmineUpdatedOn(Date matterRedmineUpdatedOn) {
        if (matterRedmineUpdatedOn != null) {
            this.matterRedmineUpdatedOn = (Date) matterRedmineUpdatedOn.clone();
        } else {
            this.matterRedmineUpdatedOn = null;
        }
    }
    /**
     * @return the matterName
     */
    public String getMatterName() {
        return matterName;
    }
    /**
     * @param matterName the matterName to set
     */
    public void setMatterName(String matterName) {
        this.matterName = matterName;
    }
    /**
     * @return the matterChargeUserId
     */
    public String getMatterChargeUserId() {
        return matterChargeUserId;
    }
    /**
     * @param matterChargeUserId the matterChargeUserId to set
     */
    public void setMatterChargeUserId(String matterChargeUserId) {
        this.matterChargeUserId = matterChargeUserId;
    }
    /**
     * @return the matterFactoryCount
     */
    public Integer getMatterFactoryCount() {
        return matterFactoryCount;
    }
    /**
     * @param matterFactoryCount the matterFactoryCount to set
     */
    public void setMatterFactoryCount(Integer matterFactoryCount) {
        this.matterFactoryCount = matterFactoryCount;
    }
    /**
     * @return the matterFactoryErrorCountGeneral
     */
    public Integer getMatterFactoryErrorCountGeneral() {
        return matterFactoryErrorCountGeneral;
    }
    /**
     * @param matterFactoryErrorCountGeneral the matterFactoryErrorCountGeneral to set
     */
    public void setMatterFactoryErrorCountGeneral(
            Integer matterFactoryErrorCountGeneral) {
        this.matterFactoryErrorCountGeneral = matterFactoryErrorCountGeneral;
    }
    /**
     * @return the matterFactoryErrorCountCategory
     */
    public Integer getMatterFactoryErrorCountCategory() {
        return matterFactoryErrorCountCategory;
    }
    /**
     * @param matterFactoryErrorCountCategory the matterFactoryErrorCountCategory to set
     */
    public void setMatterFactoryErrorCountCategory(
            Integer matterFactoryErrorCountCategory) {
        this.matterFactoryErrorCountCategory = matterFactoryErrorCountCategory;
    }
    /**
     * @return the matterFactoryErrorCountModel
     */
    public Integer getMatterFactoryErrorCountModel() {
        return matterFactoryErrorCountModel;
    }
    /**
     * @param matterFactoryErrorCountModel the matterFactoryErrorCountModel to set
     */
    public void setMatterFactoryErrorCountModel(Integer matterFactoryErrorCountModel) {
        this.matterFactoryErrorCountModel = matterFactoryErrorCountModel;
    }
    /**
     * @return the matterFactoryErrorCountAttribute
     */
    public Integer getMatterFactoryErrorCountAttribute() {
        return matterFactoryErrorCountAttribute;
    }
    /**
     * @param matterFactoryErrorCountAttribute the matterFactoryErrorCountAttribute to set
     */
    public void setMatterFactoryErrorCountAttribute(
            Integer matterFactoryErrorCountAttribute) {
        this.matterFactoryErrorCountAttribute = matterFactoryErrorCountAttribute;
    }

    public Integer getMatterRegistrationFlg() {
		return matterRegistrationFlg;
	}

    public void setMatterRegistrationFlg(Integer matterRegistrationFlg) {
		this.matterRegistrationFlg = matterRegistrationFlg;
	}

	/**
     * @return the matterDelFlg
     */
    public Integer getMatterDelFlg() {
        return matterDelFlg;
    }
    /**
     * @param matterDelFlg the matterDelFlg to set
     */
    public void setMatterDelFlg(Integer matterDelFlg) {
        this.matterDelFlg = matterDelFlg;
    }
    /**
     * @return the createdOn
     */
    public Date getCreatedOn() {
        if (this.createdOn != null) {
            return (Date) this.createdOn.clone();
        }
        return null;
    }
    /**
     * @param createdOn the createdOn to set
     */
    public void setCreatedOn(Date createdOn) {
        if (createdOn != null) {
            this.createdOn = (Date) createdOn.clone();
        } else {
            this.createdOn = null;
        }
    }
    /**
     * @return the createdUserId
     */
    public String getCreatedUserId() {
        return createdUserId;
    }
    /**
     * @param createdUserId the createdUserId to set
     */
    public void setCreatedUserId(String createdUserId) {
        this.createdUserId = createdUserId;
    }
    /**
     * @return the updatedOn
     */
    public Date getUpdatedOn() {
    	if (this.updatedOn != null) {
            return (Date) this.updatedOn.clone();
        }
        return null;
    }
    /**
     * @param updatedOn the updatedOn to set
     */
    public void setUpdatedOn(Date updatedOn) {
        if (updatedOn != null) {
            this.updatedOn = (Date) updatedOn.clone();
        } else {
            this.updatedOn = null;
        }
    }
    /**
     * @return String of updateOn
     */
    public String getUpdatedOnString() {
        if (this.updatedOn != null) {
            return this.updatedOn.toString();
        }
        return null;
    }
    /**
     * @return the updatedUserId
     */
    public String getUpdatedUserId() {
        return updatedUserId;
    }
    /**
     * @param updatedUserId the updatedUserId to set
     */
    public void setUpdatedUserId(String updatedUserId) {
        this.updatedUserId = updatedUserId;
    }

    /**
     * @return the oldUpdateOn
     */
    public String getOldUpdateOn() {
        return oldUpdateOn;
    }

    /**
     * @param oldUpdateOn the oldUpdateOn to set
     */
    public void setOldUpdateOn(String oldUpdateOn) {
        this.oldUpdateOn = oldUpdateOn;
    }

    /**
     * Get updatedOn String.
     * @return String
     */
    public String getUpdatedOnToString() {
        if (this.updatedOn != null) {
            DateFormat df = new SimpleDateFormat("yyyy/MM/dd H:m:s");
            return df.format(this.updatedOn);
        }
        return null;
    }

	/**
	 * @return the errorMessage
	 */
	public String getErrorMessage() {
		return errorMessage;
	}

	/**
	 * @param errorMessage the errorMessage to set
	 */
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

}
