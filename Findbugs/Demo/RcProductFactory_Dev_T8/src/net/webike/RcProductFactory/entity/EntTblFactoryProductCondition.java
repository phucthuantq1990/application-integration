/************************************************************************
 * File Name    ： EntTblFactoryProductCondition.java
 * Author       ： Nguyen.Chuong
 * Version      ： 1.0.0
 * Date Created ： 2014/05/22
 * Date Updated ： 2014/05/22
 * Description  ： Entity to map data with table rc_product_factory.tbl_factory_product_condition.
 ***********************************************************************/
package net.webike.RcProductFactory.entity;

import java.util.Date;

/**
 * Entity of EntTblFactoryProductCondition.
 * @author Nguyen.Chuong
 * @since 2014/05/22
 */
public class EntTblFactoryProductCondition {
    private long productId;
    private Integer productOpenPriceFlg;
    private Integer productProperSellingFlg;
    private Integer productOrderProductFlg;
    private Integer productNoReturnableFlg;
    private Integer productAmbiguousImageFlg;

    private Date updatedOn;
    private String updatedUserId;
    /**
     * @return the productId
     */
    public long getProductId() {
        return productId;
    }
    /**
     * @param productId the productId to set
     */
    public void setProductId(long productId) {
        this.productId = productId;
    }
    /**
     * @return the productOpenPriceFlg
     */
    public Integer getProductOpenPriceFlg() {
        return productOpenPriceFlg;
    }
    /**
     * @param productOpenPriceFlg the productOpenPriceFlg to set
     */
    public void setProductOpenPriceFlg(Integer productOpenPriceFlg) {
        this.productOpenPriceFlg = productOpenPriceFlg;
    }
    /**
     * @return the productProperSellingFlg
     */
    public Integer getProductProperSellingFlg() {
        return productProperSellingFlg;
    }
    /**
     * @param productProperSellingFlg the productProperSellingFlg to set
     */
    public void setProductProperSellingFlg(Integer productProperSellingFlg) {
        this.productProperSellingFlg = productProperSellingFlg;
    }
    /**
     * @return the productOrderProductFlg
     */
    public Integer getProductOrderProductFlg() {
        return productOrderProductFlg;
    }
    /**
     * @param productOrderProductFlg the productOrderProductFlg to set
     */
    public void setProductOrderProductFlg(Integer productOrderProductFlg) {
        this.productOrderProductFlg = productOrderProductFlg;
    }
    /**
     * @return the productNoReturnableFlg
     */
    public Integer getProductNoReturnableFlg() {
        return productNoReturnableFlg;
    }
    /**
     * @param productNoReturnableFlg the productNoReturnableFlg to set
     */
    public void setProductNoReturnableFlg(Integer productNoReturnableFlg) {
        this.productNoReturnableFlg = productNoReturnableFlg;
    }
    /**
     * @return the productAmbiguousImageFlg
     */
    public Integer getProductAmbiguousImageFlg() {
        return productAmbiguousImageFlg;
    }
    /**
     * @param productAmbiguousImageFlg the productAmbiguousImageFlg to set
     */
    public void setProductAmbiguousImageFlg(Integer productAmbiguousImageFlg) {
        this.productAmbiguousImageFlg = productAmbiguousImageFlg;
    }
    /**
     * @return the updatedOn
     */
    public Date getUpdatedOn() {
        if (this.updatedOn != null) {
            return (Date) this.updatedOn.clone();
        }
        return null;
    }
    /**
     * @param updatedOn the updatedOn to set
     */
    public void setUpdatedOn(Date updatedOn) {
        if (updatedOn != null) {
            this.updatedOn = (Date) updatedOn.clone();
        } else {
            this.updatedOn = null;
        }
    }
    /**
     * @return the updatedUserId
     */
    public String getUpdatedUserId() {
        return updatedUserId;
    }
    /**
     * @param updatedUserId the updatedUserId to set
     */
    public void setUpdatedUserId(String updatedUserId) {
        this.updatedUserId = updatedUserId;
    }
    }

