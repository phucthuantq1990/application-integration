/************************************************************************
 * file name	： EntTblFactoryProductImage.java
 * author		： Nguyen.Chuong
 * version		： 1.0.0
 * date created	： 25 Oct 2013
 * date updated	： 25 Oct 2013
 * description	： Entity for product image table.
 *
 ***********************************************************************/
package net.webike.RcProductFactory.entity;

import java.util.Date;


/**
 * EntTblFactoryProductImage.
 */
public class EntTblFactoryProductImage {
    private String productImage;
    private String productThumb;
    //BOE [6433] Nguyen.Chuong: Add new field referMode from DB
    private String referMode;
    //EOE [6433] Nguyen.Chuong: Add new field referMode from DB

    //BOE [6582] Tran.Thanh: Add new field for block 2 from DB
    private Long productId;

    private String imageId;

    private String productImageSort;

    private String productImageDetailPath;

    private String productImageThumbnailPath;

    private String idRow = "";

	private String crudFlg = "";

	private String updatetedUserId = "";

	private Date updatedOn;

	private Integer imageSort;
    //EOE [6582] Tran.Thanh: Add new field for block 2 from DB
	private Long syouhinSysCode;

	//BOE #7363 Tran.Thanh : Add mode replace
	private Integer replaceMode; // (replace all is 0, replace first is 1,  replacelast is 2 and find/replace is 3

	private String findThumb;

	private String findDetail;
	//EOE #7363 Tran.Thanh : Add mode replace

	private boolean isValidThumbnailSize = true; //(true = valid,false = invalid)
	
	private boolean isExistsImage = false; //(true = available image, false = not available image)
    /**
     * @return the productImage
     */
    public String getProductImage() {
        return productImage;
    }
    /**
     * @param productImage the productImage to set
     */
    public void setProductImage(String productImage) {
        this.productImage = productImage;
    }
    /**
     * @return the productThumb
     */
    public String getProductThumb() {
        return productThumb;
    }
    /**
     * @param productThumb the productThumb to set
     */
    public void setProductThumb(String productThumb) {
        this.productThumb = productThumb;
    }
    /**
     * @return the referMode
     */
    public String getReferMode() {
        return referMode;
    }
    /**
     * @param referMode the referMode to set
     */
    public void setReferMode(String referMode) {
        this.referMode = referMode;
    }
	/**
	 * @return the productId
	 */
	public Long getProductId() {
		return productId;
	}
	/**
	 * @param productId the productId to set
	 */
	public void setProductId(Long productId) {
		this.productId = productId;
	}
	/**
	 * @return the imageId
	 */
	public String getImageId() {
		return imageId;
	}
	/**
	 * @param imageId the imageId to set
	 */
	public void setImageId(String imageId) {
		this.imageId = imageId;
	}
	/**
	 * @return the productImageSort
	 */
	public String getProductImageSort() {
		return productImageSort;
	}
	/**
	 * @param productImageSort the productImageSort to set
	 */
	public void setProductImageSort(String productImageSort) {
		this.productImageSort = productImageSort;
	}
	/**
	 * @return the productImageDetailPath
	 */
	public String getProductImageDetailPath() {
		return productImageDetailPath;
	}
	/**
	 * @param productImageDetailPath the productImageDetailPath to set
	 */
	public void setProductImageDetailPath(String productImageDetailPath) {
		this.productImageDetailPath = productImageDetailPath;
	}
	/**
	 * @return the productImageThumbnailPath
	 */
	public String getProductImageThumbnailPath() {
		return productImageThumbnailPath;
	}
	/**
	 * @param productImageThumbnailPath the productImageThumbnailPath to set
	 */
	public void setProductImageThumbnailPath(String productImageThumbnailPath) {
		this.productImageThumbnailPath = productImageThumbnailPath;
	}
	/**
	 * @return the idRow
	 */
	public String getIdRow() {
		return idRow;
	}
	/**
	 * @param idRow the idRow to set
	 */
	public void setIdRow(String idRow) {
		this.idRow = idRow;
	}
	/**
	 * @return the crudFlg
	 */
	public String getCrudFlg() {
		return crudFlg;
	}
	/**
	 * @param crudFlg the crudFlg to set
	 */
	public void setCrudFlg(String crudFlg) {
		this.crudFlg = crudFlg;
	}
	/**
	 * @return the updatetedUserId
	 */
	public String getUpdatetedUserId() {
		return updatetedUserId;
	}
	/**
	 * @param updatetedUserId the updatetedUserId to set
	 */
	public void setUpdatetedUserId(String updatetedUserId) {
		this.updatetedUserId = updatetedUserId;
	}
	/**
	 * @return the updatedOn
	 */
	public Date getUpdatedOn() {
		if (updatedOn != null) {
            return (Date) updatedOn.clone();
        }
        return null;
	}
	/**
	 * @param updatedOn the updatedOn to set
	 */
	public void setUpdatedOn(Date updatedOn) {
		if (updatedOn != null) {
			this.updatedOn = (Date) updatedOn.clone();
        } else {
        	this.updatedOn = null;
        }
	}
	/**
	 * @return the imageSort
	 */
	public Integer getImageSort() {
		return imageSort;
	}
	/**
	 * @param imageSort the imageSort to set
	 */
	public void setImageSort(Integer imageSort) {
		this.imageSort = imageSort;
	}
    /**
     * @return the syouhinSysCode
     */
    public Long getSyouhinSysCode() {
        return syouhinSysCode;
    }
    /**
     * @param syouhinSysCode the syouhinSysCode to set
     */
    public void setSyouhinSysCode(Long syouhinSysCode) {
        this.syouhinSysCode = syouhinSysCode;
    }
	/**
	 * @return the replaceMode
	 */
	public Integer getReplaceMode() {
		return replaceMode;
	}
	/**
	 * @param replaceMode the replaceMode to set
	 */
	public void setReplaceMode(Integer replaceMode) {
		this.replaceMode = replaceMode;
	}
	/**
	 * @return the findThumb
	 */
	public String getFindThumb() {
		return findThumb;
	}
	/**
	 * @param findThumb the findThumb to set
	 */
	public void setFindThumb(String findThumb) {
		this.findThumb = findThumb;
	}
	/**
	 * @return the findDetail
	 */
	public String getFindDetail() {
		return findDetail;
	}
	/**
	 * @param findDetail the findDetail to set
	 */
	public void setFindDetail(String findDetail) {
		this.findDetail = findDetail;
	}
	/**
	 * @return the isValidThumbnailSize
	 */
	public boolean isValidThumbnailSize() {
		return isValidThumbnailSize;
	}
	/**
	 * @param isValidThumbnailSize the isValidThumbnailSize to set
	 */
	public void setValidThumbnailSize(boolean isValidThumbnailSize) {
		this.isValidThumbnailSize = isValidThumbnailSize;
	}
	/**
	 * @return the isExitsImage
	 */
	public boolean isExistsImage() {
		return isExistsImage;
	}
	/**
	 * @param isExitsImage the isExitsImage to set
	 */
	public void setExistsImage(boolean isExitsImage) {
		this.isExistsImage = isExitsImage;
	}

}
