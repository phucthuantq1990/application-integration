/************************************************************************
 * file name	： EntMstSiire.java
 * author		： Le.Dinh
 * version		： 1.0.0
 * date created	： 2014/02/12
 * date updated	： 2014/02/12
 * description	： Entity EntMstSiire.
 *
 ***********************************************************************/
package net.webike.RcProductFactory.entity;

/**
 * EntMstSiire.
 */
public class EntMstSiire {

	private String siireCode;
	private String siireName;
	private String noukiName1;
	private Integer siireMarumeCode;
	private Integer supplierSyouhinSiireOrder;
	/**
	 * @return the siireCode
	 */
	public String getSiireCode() {
		return siireCode;
	}
	/**
	 * @param siireCode the siireCode to set
	 */
	public void setSiireCode(String siireCode) {
		this.siireCode = siireCode;
	}
	/**
	 * @return the siireName
	 */
	public String getSiireName() {
		return siireName;
	}
	/**
	 * @param siireName the siireName to set
	 */
	public void setSiireName(String siireName) {
		this.siireName = siireName;
	}
    /**
     * @return the noukiName1
     */
    public String getNoukiName1() {
        return noukiName1;
    }
    /**
     * @param noukiName1 the noukiName1 to set
     */
    public void setNoukiName1(String noukiName1) {
        this.noukiName1 = noukiName1;
    }
    /**
     * @return the siireMarumeCode
     */
    public Integer getSiireMarumeCode() {
        return siireMarumeCode;
    }
    /**
     * @param siireMarumeCode the siireMarumeCode to set
     */
    public void setSiireMarumeCode(Integer siireMarumeCode) {
        this.siireMarumeCode = siireMarumeCode;
    }
    /**
     * @return the supplierSyouhinSiireOrder
     */
    public Integer getSupplierSyouhinSiireOrder() {
        return supplierSyouhinSiireOrder;
    }
    /**
     * @param supplierSyouhinSiireOrder the supplierSyouhinSiireOrder to set
     */
    public void setSupplierSyouhinSiireOrder(Integer supplierSyouhinSiireOrder) {
        this.supplierSyouhinSiireOrder = supplierSyouhinSiireOrder;
    }

}
