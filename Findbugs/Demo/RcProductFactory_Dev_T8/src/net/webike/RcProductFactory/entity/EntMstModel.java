/************************************************************************
 * file name	： EntMstModel.java
 * author		： Nguyen.Chuong
 * version		： 1.0.0
 * date created	： 2014/01/23
 * description	： Entity for mst model.
 *
 ***********************************************************************/
package net.webike.RcProductFactory.entity;

import java.util.Date;

/**
 * EntMstModel.
 */
public class EntMstModel {
    private String modelCode;
    private String modelMaker;
    private String modelDisplacement;
    private String modelName;
    private String modelNameS;
    private Date modelKousinDate;
    private String sortField;
    private String sortDir;

    /**
     * @return the modelCode
     */
    public String getModelCode() {
        return modelCode;
    }
    /**
     * @param modelCode the modelCode to set
     */
    public void setModelCode(String modelCode) {
        this.modelCode = modelCode;
    }
    /**
     * @return the modelMaker
     */
    public String getModelMaker() {
        return modelMaker;
    }
    /**
     * @param modelMaker the modelMaker to set
     */
    public void setModelMaker(String modelMaker) {
        this.modelMaker = modelMaker;
    }
    /**
     * @return the modelDisplacement
     */
    public String getModelDisplacement() {
        return modelDisplacement;
    }
    /**
     * @param modelDisplacement the modelDisplacement to set
     */
    public void setModelDisplacement(String modelDisplacement) {
        this.modelDisplacement = modelDisplacement;
    }
    /**
     * @return the modelName
     */
    public String getModelName() {
        return modelName;
    }
    /**
     * @param modelName the modelName to set
     */
    public void setModelName(String modelName) {
        this.modelName = modelName;
    }
    /**
     * @return the modelNameS
     */
    public String getModelNameS() {
        return modelNameS;
    }
    /**
     * @param modelNameS the modelNameS to set
     */
    public void setModelNameS(String modelNameS) {
        this.modelNameS = modelNameS;
    }
    /**
     * @return the modelKousinDate
     */
    public Date getModelKousinDate() {
        if (modelKousinDate != null) {
            return (Date) modelKousinDate.clone();
        }
        return null;
    }
    /**
     * @param modelKousinDate the modelKousinDate to set
     */
    public void setModelKousinDate(Date modelKousinDate) {
        if (modelKousinDate != null) {
            this.modelKousinDate = (Date) modelKousinDate.clone();
        } else {
            this.modelKousinDate = null;
        }
    }
    /**
     * @return the sortField
     */
    public String getSortField() {
        return sortField;
    }
    /**
     * @param sortField the sortField to set
     */
    public void setSortField(String sortField) {
        this.sortField = sortField;
    }
    /**
     * @return the sortDir
     */
    public String getSortDir() {
        return sortDir;
    }
    /**
     * @param sortDir the sortDir to set
     */
    public void setSortDir(String sortDir) {
        this.sortDir = sortDir;
    }
}
