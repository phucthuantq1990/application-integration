
package net.webike.RcProductFactory.entity;


/**
 * Entity of EntTblFactoryProductAttributeString.
 * @author Doan Chuong
 * Date Created �F 2014/03/05
 */
public class EntTblFactoryProductAttributeString {
    private Long productId;
    private Integer attributeCode;
    private String attributeStringValues;
    private Integer attributeStringSort;
    private String updatedOn;
    private String updatedUserId;
    /**
     * @return the productId
     */
    public Long getProductId() {
        return productId;
    }
    /**
     * @param productId the productId to set
     */
    public void setProductId(Long productId) {
        this.productId = productId;
    }
    /**
     * @return the attributeCode
     */
    public Integer getAttributeCode() {
        return attributeCode;
    }
    /**
     * @param attributeCode the attributeCode to set
     */
    public void setAttributeCode(Integer attributeCode) {
        this.attributeCode = attributeCode;
    }
    /**
     * @return the attributeStringValues
     */
    public String getAttributeStringValues() {
        return attributeStringValues;
    }
    /**
     * @param attributeStringValues the attributeStringValues to set
     */
    public void setAttributeStringValues(String attributeStringValues) {
        this.attributeStringValues = attributeStringValues;
    }
    /**
     * @return the attributeStringSort
     */
    public Integer getAttributeStringSort() {
        return attributeStringSort;
    }
    /**
     * @param attributeStringSort the attributeStringSort to set
     */
    public void setAttributeStringSort(Integer attributeStringSort) {
        this.attributeStringSort = attributeStringSort;
    }
    /**
     * @return the updatedOn
     */
    public String getUpdatedOn() {
        return updatedOn;
    }
    /**
     * @param updatedOn the updatedOn to set
     */
    public void setUpdatedOn(String updatedOn) {
        this.updatedOn = updatedOn;
    }
    /**
     * @return the updatedUserId
     */
    public String getUpdatedUserId() {
        return updatedUserId;
    }
    /**
     * @param updatedUserId the updatedUserId to set
     */
    public void setUpdatedUserId(String updatedUserId) {
        this.updatedUserId = updatedUserId;
    }
}
