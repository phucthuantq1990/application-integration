/************************************************************************
 * file name    ： EntCSVMode1.java
 *
 * author       ： Thai.Son
 * version      ： 2.0.0
 * date created ： 2014/02/26
 * date updated ： 2014/02/26
 * description  ： Entity for product tbl_factory_product using by CSV.
 * Luong.Dai 2014/07/11: Add JsonIgnore to ignore file when read and write csv file
 ***********************************************************************/
package net.webike.RcProductFactory.entity;

import org.apache.commons.lang.StringUtils;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * EntCSVMode1.
 */
public class EntCSVMode1 {
	@JsonIgnore
    private String sortField;
	@JsonIgnore
    private String sortDir;
    private String errorAllMess;	//全体エラー
    //1.更新モード
    private String importMode;
    private String importModeError = "";	//更新モード_エラー
    @JsonIgnore
    private Long productId; //2.識別番号 product_id
    private String productIdError = ""; //識別番号_エラー
    //Product Id with format Pxxxxx
    private String	sProductId;
    @JsonIgnore
    private Long productSyouhinSysCode; //3.システム品番 product_syouhin_sys_code
    private String productSyouhinSysCodeError = ""; //システム品番_エラー
    private String sProductSyouhinSysCode;
    @JsonIgnore
    private Integer productBrandCode; //4.ブランドコード product_brand_code
    private String productBrandCodeError = "";	//ブランドコード_エラー
    private String sProductBrandCode;
    private String productBrandName; //5.ブランド名
    @JsonIgnore
    private Integer productCategoryCode; //6.品目コード product_category_code
    private String productCategoryCodeError = ""; //品目コード_エラー
    private String sProductCategoryCode;
    private String productCategoryName; //7.品目名
    private String productName; //8.商品名 product_name
    private String productNameError = "";	//製品名_エラー
    private String productCode; //9.メーカー品番 product_code
    private String productCodeError = "";	//メーカー品番_エラー
    @JsonIgnore
    private Integer productWebikeCodeFlg; //10. 品番無しフラグ  0/1 product_webike_code_flg;
    private String productWebikeCodeFlgError = "";	//品番なしフラグ_エラー
    private String sProductWebikeCodeFlg;
    private String productEanCode; //JAN 11. product_ean_code;
    private String productEanCodeError = "";	//JAN_エラー
    @JsonIgnore
    private  Integer productProperPrice; //12.定価 product_proper_price;
    private String productProperPriceError = "";	//定価_エラー
    private  String sProductProperPrice;
    @JsonIgnore
    private  Integer supplierPricePrice; //13.仕切り supplier_price_price;
    private String supplierPricePriceError = "";	//仕切り_エラー
    private  String sSupplierPricePrice;
    @JsonIgnore
    private  Long productGroupCode; //14.グループコード product_group_code;
    private String productGroupCodeError = "";	//グループコード_エラー
    private  String sProductGroupCode;
    private  String productSupplierReleaseDate; //15.発売日 product_supplier_release_date;
    private String productSupplierReleaseDateError = "";	//発売日_エラー
    @JsonIgnore
    private  Integer productOpenPriceFlg; //16.オープン価格 0 / 1 product_open_price_flg;
    private String productOpenPriceFlgError = ""; 	//オープン価格_エラー
    private  String sProductOpenPriceFlg;
    @JsonIgnore
    private  Integer productProperSellingFlg; //17.定価販売 0 / 1 product_proper_selling_flg;
    private String productProperSellingFlgError = ""; 	//定価販売_エラー
    private  String sProductProperSellingFlg;
    @JsonIgnore
    private  Integer productOrderProductFlg; //18.受注生産 0 / 1 product_order_product_flg;
    private String productOrderProductFlgError = ""; 	//受注生産_エラー
    private  String sProductOrderProductFlg;
    @JsonIgnore
    private  Integer productNoReturnableFlg; //18.5 返品不可 0 / 1 product_no_returnable_flg;
    private String productNoReturnableFlgError = "";		//返品不可_エラー
    private  String sProductNoReturnableFlg;
    @JsonIgnore
    private  Integer productAmbiguousImageFlg; //19.参考画像 0 / 1 product_ambiguous_image_flg;
    private String productAmbiguousImageFlgError = "";	//参考画像_エラー
    private  String sProductAmbiguousImageFlg;
    private  String descriptionSummary; //20.要点| symbol is not permissible description_summary;
    private String descriptionSummaryError = "";		//要点_エラー
    private  String descriptionRemarks; //21.備考| symbol is not permissible description_remarks;
    private String descriptionRemarksError = ""; 	//備考_エラー
    private  String descriptionCaution; //22.注意 | symbol is not permissible description_caution;
    private String descriptionCautionError = "";	//注意_エラー
    private  String descriptionSentence; //23.説明 description_sentence;
    private String descriptionSentenceError = "";	//説明_エラー
    private  String siireCode1; //24.仕入先コード1 siire_code1;//String [0-9] | Length is 3 only
    private String siireCode1Error = "";	//仕入先コード1_エラー
    private  String siireName1; //25.仕入先名1
    private  String noukiCode1; //納期コード1 nouki_code1;//String [0-9] | Length is 3only
    private String noukiCode1Error = "";	//納期コード1_エラー
    private  String noukiName1;
    private  String siireCode2; //仕入先コード2 siire_code2;//String [0-9]| Length is 3 only
    private String siireCode2Error = "";		//仕入先コード2_エラー
    private  String siireName2;
    private  String noukiCode2; //納期コード2 nouki_code2;//String [0-9] | Length is 3only
    private String noukiCode2Error = "";		//納期コード2_エラー
    private  String noukiName2;
    private  String compatibleModel; //適合車種 [Maker::Model::Style][Model::M.....
    private String compatibleModelError = "";	//適合車種_エラー
    //BOE #7206 change errorFlg to boolean.
    @JsonIgnore
    private boolean productGeneralErrorFlg = false;
    @JsonIgnore
    private boolean productCategoryErrorFlg = false;
    @JsonIgnore
    private boolean productModelErrorFlg = false;
    @JsonIgnore
    private boolean productAttributeErrorFlg = false;
    //EOE #7206 change errorFlg to boolean.

//    private  String productImageThumbnailPath; //サムネイルX product_image_thumbnail_path;//String | X = sort number
//  private  String productImageDetailPath; //詳細画像X  product_image_detail_path;//String | X = sort number
//    private  String optionCodeX; ////選択肢コードX private  105; String | X = sort number
//    private  String choicesNameX; //選択肢名X String | X = sort number
    private  String productImageThumbnailPath1;
    private String productImageThumbnailPath1Error = ""; 	//サムネイル1_エラー
    private  String productImageDetailPath1;
    private String productImageDetailPath1Error = "";		//詳細画像1_エラー
    private  String productImageThumbnailPath2;
    private String productImageThumbnailPath2Error = ""; 	//サムネイル2_エラー
    private  String productImageDetailPath2;
    private String productImageDetailPath2Error = "";		//詳細画像2_エラー
    private  String productImageThumbnailPath3;
    private String productImageThumbnailPath3Error = ""; 	//サムネイル3_エラー
    private  String productImageDetailPath3;
    private String productImageDetailPath3Error = "";		//詳細画像3_エラー
    private  String productImageThumbnailPath4;
    private String productImageThumbnailPath4Error = ""; 	//サムネイル4_エラー
    private  String productImageDetailPath4;
    private String productImageDetailPath4Error = "";		//詳細画像4_エラー
    private  String productImageThumbnailPath5;
    private String productImageThumbnailPath5Error = ""; 	//サムネイル5_エラー
    private  String productImageDetailPath5;
    private String productImageDetailPath5Error = "";		//詳細画像5_エラー
    private  String productImageThumbnailPath6;
    private String productImageThumbnailPath6Error = ""; 	//サムネイル6_エラー
    private  String productImageDetailPath6;
    private String productImageDetailPath6Error = "";		//詳細画像6_エラー
    private  String productImageThumbnailPath7;
    private String productImageThumbnailPath7Error = ""; 	//サムネイル7_エラー
    private  String productImageDetailPath7;
    private String productImageDetailPath7Error = "";		//詳細画像7_エラー
    private  String productImageThumbnailPath8;
    private String productImageThumbnailPath8Error = ""; 	//サムネイル8_エラー
    private  String productImageDetailPath8;
    private String productImageDetailPath8Error = "";		//詳細画像8_エラー
    private  String productImageThumbnailPath9;
    private String productImageThumbnailPath9Error = ""; 	//サムネイル9_エラー
    private  String productImageDetailPath9;
    private String productImageDetailPath9Error = "";		//詳細画像9_エラー
    private  String productImageThumbnailPath10;
    private String productImageThumbnailPath10Error = ""; 	//サムネイル10_エラー
    private  String productImageDetailPath10;
    private String productImageDetailPath10Error = "";		//詳細画像10_エラー

    @JsonIgnore
    private int selectCode1;
    private String selectCode1Error = "";	//選択肢コード1_エラー
    private String sSelectCode1;
    private String selectName1;
    private String selectName1Error = "";	//選択肢名1_エラー
    @JsonIgnore
    private int selectCode2;
    private String selectCode2Error = "";	//選択肢コード2_エラー
    private String sSelectCode2;
    private String selectName2;
    private String selectName2Error = "";	//選択肢名2_エラー
    @JsonIgnore
    private int selectCode3;
    private String selectCode3Error = "";	//選択肢コード3_エラー
    private String sSelectCode3;
    private String selectName3;
    private String selectName3Error = "";	//選択肢名3_エラー
    @JsonIgnore
    private int selectCode4;
    private String selectCode4Error = "";	//選択肢コード4_エラー
    private String sSelectCode4;
    private String selectName4;
    private String selectName4Error = "";	//選択肢名4_エラー
    @JsonIgnore
    private int selectCode5;
    private String selectCode5Error = "";	//選択肢コード5_エラー
    private String sSelectCode5;
    private String selectName5;
    private String selectName5Error = "";	//選択肢名5_エラー
    @JsonIgnore
    private int selectCode6;
    private String selectCode6Error = "";	//選択肢コード6_エラー
    private String sSelectCode6;
    private String selectName6;
    private String selectName6Error = "";	//選択肢名6_エラー
    @JsonIgnore
    private int selectCode7;
    private String selectCode7Error = "";	//選択肢コード7_エラー
    private String sSelectCode7;
    private String selectName7;
    private String selectName7Error = "";	//選択肢名7_エラー
    @JsonIgnore
    private int selectCode8;
    private String selectCode8Error = "";	//選択肢コード8_エラー
    private String sSelectCode8;
    private String selectName8;
    private String selectName8Error = "";	//選択肢名8_エラー
    @JsonIgnore
    private int selectCode9;
    private String selectCode9Error = "";	//選択肢コード9_エラー
    private String sSelectCode9;
    private String selectName9;
    private String selectName9Error = "";	//選択肢名9_エラー
    @JsonIgnore
    private int selectCode10;
    private String selectCode10Error = "";	//選択肢コード10_エラー
    private String sSelectCode10;
    private String selectName10;
    private String selectName10Error = "";	//選択肢名10_エラー
    //////////NOW, I TEST: X = 1-->3
    //END//////////////X = 1 --> 10   /////////////////////////////////////
    private  String customersConfirmationItem; //お客様確認項目 [Title::Description][Title....
    private String customersConfirmationItemError = "";	//お客様確認項目_エラー
    private  String link; //[Reason::Title::URL][Rea....
    private String linkError = "";	//リンク_エラー
    private  String animation; //[Title::URL::Description][Title...
    private String animationError = "";	//動画_エラー
//End Import mode 1 column
    @JsonIgnore
    private EntMstAttribute entMstAttribute1;
    @JsonIgnore
    private int attributeCode1;
    @JsonIgnore
    private String attributevalues1; //YYYYY_管理名  ex: entMstAttribute1.attributeName + "_管理名"
    @JsonIgnore
    private String attributeDisplay1; //YYYYY_表示名 ex: entMstAttribute1.attributeName + "_表示名"

    @JsonIgnore
    private EntMstAttribute entMstAttribute2;
    @JsonIgnore
    private int attributeCode2;
    @JsonIgnore
    private String attributevalues2; //YYYYY_管理名  ex: entMstAttribute2.attributeName + "_管理名"
    @JsonIgnore
    private String attributeDisplay2; //YYYYY_表示名 ex: entMstAttribute2.attributeName + "_表示名"

    @JsonIgnore
    private EntMstAttribute entMstAttribute3;
    @JsonIgnore
    private int attributeCode3;
    @JsonIgnore
    private String attributevalues3;
    @JsonIgnore
    private String attributeDisplay3;
    @JsonIgnore
    private EntMstAttribute entMstAttribute4;
    @JsonIgnore
    private int attributeCode4;
    @JsonIgnore
    private String attributevalues4;
    @JsonIgnore
    private String attributeDisplay4;

    @JsonIgnore
    private EntMstAttribute entMstAttribute5;
    @JsonIgnore
    private int attributeCode5;
    @JsonIgnore
    private String attributevalues5;
    @JsonIgnore
    private String attributeDisplay5;

    @JsonIgnore
    private EntMstAttribute entMstAttribute6;
    @JsonIgnore
    private int attributeCode6;
    @JsonIgnore
    private String attributevalues6;
    @JsonIgnore
    private String attributeDisplay6;
    @JsonIgnore
    private EntMstAttribute entMstAttribute7;
    @JsonIgnore
    private int attributeCode7;
    @JsonIgnore
    private String attributevalues7;
    @JsonIgnore
    private String attributeDisplay7;
    @JsonIgnore
    private EntMstAttribute entMstAttribute8;
    @JsonIgnore
    private int attributeCode8;
    @JsonIgnore
    private String attributevalues8;
    @JsonIgnore
    private String attributeDisplay8;
    @JsonIgnore
    private EntMstAttribute entMstAttribute9;
    @JsonIgnore
    private int attributeCode9;
    @JsonIgnore
    private String attributevalues9;
    @JsonIgnore
    private String attributeDisplay9;

    @JsonIgnore
    private EntMstAttribute entMstAttribute10;
    @JsonIgnore
    private int attributeCode10;
    @JsonIgnore
    private String attributevalues10;
    @JsonIgnore
    private String attributeDisplay10;

    @JsonIgnore
    private EntMstAttribute entMstAttribute11;
    @JsonIgnore
    private int attributeCode11;
    @JsonIgnore
    private String attributevalues11;
    @JsonIgnore
    private String attributeDisplay11;
    @JsonIgnore
    private EntMstAttribute entMstAttribute12;
    @JsonIgnore
    private int attributeCode12;
    @JsonIgnore
    private String attributevalues12;
    @JsonIgnore
    private String attributeDisplay12;
    @JsonIgnore
    private EntMstAttribute entMstAttribute13;
    @JsonIgnore
    private int attributeCode13;
    @JsonIgnore
    private String attributevalues13;
    @JsonIgnore
    private String attributeDisplay13;
    @JsonIgnore
    private EntMstAttribute entMstAttribute14;
    @JsonIgnore
    private int attributeCode14;
    @JsonIgnore
    private String attributevalues14;
    @JsonIgnore
    private String attributeDisplay14;

    @JsonIgnore
    private EntMstAttribute entMstAttribute15;
    @JsonIgnore
    private int attributeCode15;
    @JsonIgnore
    private String attributevalues15;
    @JsonIgnore
    private String attributeDisplay15;

    @JsonIgnore
    private EntMstAttribute entMstAttribute16;
    @JsonIgnore
    private int attributeCode16;
    @JsonIgnore
    private String attributevalues16;
    @JsonIgnore
    private String attributeDisplay16;
    @JsonIgnore
    private EntMstAttribute entMstAttribute17;
    @JsonIgnore
    private int attributeCode17;
    @JsonIgnore
    private String attributevalues17;
    @JsonIgnore
    private String attributeDisplay17;
    @JsonIgnore
    private EntMstAttribute entMstAttribute18;
    @JsonIgnore
    private int attributeCode18;
    @JsonIgnore
    private String attributevalues18;
    @JsonIgnore
    private String attributeDisplay18;
    @JsonIgnore
    private EntMstAttribute entMstAttribute19;
    @JsonIgnore
    private int attributeCode19;
    @JsonIgnore
    private String attributevalues19;
    @JsonIgnore
    private String attributeDisplay19;

    @JsonIgnore
    private EntMstAttribute entMstAttribute20;
    @JsonIgnore
    private int attributeCode20;
    @JsonIgnore
    private String attributevalues20;
    @JsonIgnore
    private String attributeDisplay20;

    @JsonIgnore
    private EntMstAttribute entMstAttribute21;
    @JsonIgnore
    private int attributeCode21;
    @JsonIgnore
    private String attributevalues21;
    @JsonIgnore
    private String attributeDisplay21;
    @JsonIgnore
    private EntMstAttribute entMstAttribute22;
    @JsonIgnore
    private int attributeCode22;
    @JsonIgnore
    private String attributevalues22;
    @JsonIgnore
    private String attributeDisplay22;
    @JsonIgnore
    private EntMstAttribute entMstAttribute23;
    @JsonIgnore
    private int attributeCode23;
    @JsonIgnore
    private String attributevalues23;
    @JsonIgnore
    private String attributeDisplay23;
    @JsonIgnore
    private EntMstAttribute entMstAttribute24;
    @JsonIgnore
    private int attributeCode24;
    @JsonIgnore
    private String attributevalues24;
    @JsonIgnore
    private String attributeDisplay24;

    @JsonIgnore
    private EntMstAttribute entMstAttribute25;
    @JsonIgnore
    private int attributeCode25;
    @JsonIgnore
    private String attributevalues25;
    @JsonIgnore
    private String attributeDisplay25;

    @JsonIgnore
    private EntMstAttribute entMstAttribute26;
    @JsonIgnore
    private int attributeCode26;
    @JsonIgnore
    private String attributevalues26;
    @JsonIgnore
    private String attributeDisplay26;
    @JsonIgnore
    private EntMstAttribute entMstAttribute27;
    @JsonIgnore
    private int attributeCode27;
    @JsonIgnore
    private String attributevalues27;
    @JsonIgnore
    private String attributeDisplay27;
    @JsonIgnore
    private EntMstAttribute entMstAttribute28;
    @JsonIgnore
    private int attributeCode28;
    @JsonIgnore
    private String attributevalues28;
    @JsonIgnore
    private String attributeDisplay28;
    @JsonIgnore
    private EntMstAttribute entMstAttribute29;
    @JsonIgnore
    private int attributeCode29;
    @JsonIgnore
    private String attributevalues29;
    @JsonIgnore
    private String attributeDisplay29;

    @JsonIgnore
    private EntMstAttribute entMstAttribute30;
    @JsonIgnore
    private int attributeCode30;
    @JsonIgnore
    private String attributevalues30;
    @JsonIgnore
    private String attributeDisplay30;

    @JsonIgnore
    private String errorMessage;
    ///////////////begin get-set/////////////////
    public Long getProductSyouhinSysCode() {
        return productSyouhinSysCode;
    }
    /************************************************************************
     * <b>Description:</b><br>
     *  getter not null if null return "".
     *
     * @author		Nguyen.Chuong
     * @date		Mar 14, 2014
     * @return		String
     ************************************************************************/
    @JsonIgnore
    public String getProductSyouhinSysCodeNotNull() {
        if (null == productSyouhinSysCode) {
            return "";
        }
        return productSyouhinSysCode.toString();
    }
    public void setProductSyouhinSysCode(Long productSyouhinSysCode) {
        this.productSyouhinSysCode = productSyouhinSysCode;
    }
    public Integer getProductBrandCode() {
        return productBrandCode;
    }
    /************************************************************************
     * <b>Description:</b><br>
     *  getter not null if null return "".
     *
     * @author      Nguyen.Chuong
     * @date        Mar 14, 2014
     * @return      String
     ************************************************************************/
    @JsonIgnore
    public String getProductBrandCodeNotNull() {
        if (null == productBrandCode) {
            return "";
        }
        return productBrandCode.toString();
    }
    public void setProductBrandCode(Integer productBrandCode) {
        this.productBrandCode = productBrandCode;
    }
    public Integer getProductCategoryCode() {
        return productCategoryCode;
    }
    /************************************************************************
     * <b>Description:</b><br>
     *  getter not null if null return "".
     *
     * @author      Nguyen.Chuong
     * @date        Mar 14, 2014
     * @return      String
     ************************************************************************/
    @JsonIgnore
    public String getProductCategoryCodeNotNull() {
        if (null == productCategoryCode) {
            return "";
        }
        return productCategoryCode.toString();
    }
    public void setProductCategoryCode(Integer productCategoryCode) {
        this.productCategoryCode = productCategoryCode;
    }
    public String getProductName() {
        return productName;
    }
    /**
     * @param productName the productName to set
     */
    public void setProductName(String productName) {
        this.productName = productName;
        if (StringUtils.isNotEmpty(this.productName)) {
        	this.productName = this.productName.trim();
        }
    }
    public String getProductCode() {
        return productCode;
    }
    /**
     * @param productCode the productCode to set
     */
    public void setProductCode(String productCode) {
        this.productCode = productCode;
        if (StringUtils.isNotEmpty(this.productCode)) {
        	this.productCode = this.productCode.trim();
        }
    }
    public Integer getProductWebikeCodeFlg() {
        return productWebikeCodeFlg;
    }
    /************************************************************************
     * <b>Description:</b><br>
     *  getter not null if null return "".
     *
     * @author      Nguyen.Chuong
     * @date        Mar 14, 2014
     * @return      String
     ************************************************************************/
    @JsonIgnore
    public String getProductWebikeCodeFlgNotNull() {
        if (null == productWebikeCodeFlg) {
            return "";
        }
        return productWebikeCodeFlg.toString();
    }
    public void setProductWebikeCodeFlg(Integer productWebikeCodeFlg) {
        this.productWebikeCodeFlg = productWebikeCodeFlg;
    }
    public String getProductEanCode() {
        return productEanCode;
    }
    /**
     * @param productEanCode the productEanCode to set
     */
    public void setProductEanCode(String productEanCode) {
        this.productEanCode = productEanCode;
        if (StringUtils.isNotEmpty(this.productEanCode)) {
        	this.productEanCode = this.productEanCode.trim();
        }
    }
    public Integer getProductProperPrice() {
        return productProperPrice;
    }
    /************************************************************************
     * <b>Description:</b><br>
     *  getter not null if null return "".
     *
     * @author      Nguyen.Chuong
     * @date        Mar 14, 2014
     * @return      String
     ************************************************************************/
    @JsonIgnore
    public String getProductProperPriceNotNull() {
        if (null == productProperPrice) {
            return "";
        }
        return productProperPrice.toString();
    }
    public void setProductProperPrice(Integer productProperPrice) {
        this.productProperPrice = productProperPrice;
    }
    public Integer getSupplierPricePrice() {
        return supplierPricePrice;
    }
    /************************************************************************
     * <b>Description:</b><br>
     *  getter not null if null return "".
     *
     * @author      Nguyen.Chuong
     * @date        Mar 14, 2014
     * @return      String
     ************************************************************************/
    @JsonIgnore
    public String getSupplierPricePriceNotNull() {
        if (null == supplierPricePrice) {
            return "";
        }
        return supplierPricePrice.toString();
    }
    public void setSupplierPricePrice(Integer supplierPricePrice) {
        this.supplierPricePrice = supplierPricePrice;
    }
    public String getProductSupplierReleaseDate() {
        return productSupplierReleaseDate;
    }
    /**
     * @param productSupplierReleaseDate the productSupplierReleaseDate to set
     */
    public void setProductSupplierReleaseDate(String productSupplierReleaseDate) {
        this.productSupplierReleaseDate = productSupplierReleaseDate;
        if (StringUtils.isNotEmpty(this.productSupplierReleaseDate)) {
        	this.productSupplierReleaseDate = this.productSupplierReleaseDate.trim();
        }
    }
    public Integer getProductOpenPriceFlg() {
        return productOpenPriceFlg;
    }
    /************************************************************************
     * <b>Description:</b><br>
     *  getter not null if null return "".
     *
     * @author      Nguyen.Chuong
     * @date        Mar 14, 2014
     * @return      String
     ************************************************************************/
    @JsonIgnore
    public String getProductOpenPriceFlgNotNull() {
        if (null == productOpenPriceFlg) {
            return "";
        }
        return productOpenPriceFlg.toString();
    }
    public void setProductOpenPriceFlg(Integer productOpenPriceFlg) {
        this.productOpenPriceFlg = productOpenPriceFlg;
    }
    public Integer getProductProperSellingFlg() {
        return productProperSellingFlg;
    }
    /************************************************************************
     * <b>Description:</b><br>
     *  getter not null if null return "".
     *
     * @author      Nguyen.Chuong
     * @date        Mar 14, 2014
     * @return      String
     ************************************************************************/
    @JsonIgnore
    public String getProductProperSellingFlgNotNull() {
        if (null == productProperSellingFlg) {
            return "";
        }
        return productProperSellingFlg.toString();
    }
    public void setProductProperSellingFlg(Integer productProperSellingFlg) {
        this.productProperSellingFlg = productProperSellingFlg;
    }
    public Integer getProductOrderProductFlg() {
        return productOrderProductFlg;
    }
    /************************************************************************
     * <b>Description:</b><br>
     *  getter not null if null return "".
     *
     * @author      Nguyen.Chuong
     * @date        Mar 14, 2014
     * @return      String
     ************************************************************************/
    @JsonIgnore
    public String getProductOrderProductFlgNotNull() {
        if (null == productOrderProductFlg) {
            return "";
        }
        return productOrderProductFlg.toString();
    }
    public void setProductOrderProductFlg(Integer productOrderProductFlg) {
        this.productOrderProductFlg = productOrderProductFlg;
    }
    public Integer getProductNoReturnableFlg() {
        return productNoReturnableFlg;
    }
    /************************************************************************
     * <b>Description:</b><br>
     *  getter not null if null return "".
     *
     * @author      Nguyen.Chuong
     * @date        Mar 14, 2014
     * @return      String
     ************************************************************************/
    @JsonIgnore
    public String getProductNoReturnableFlgNotNull() {
        if (null == productNoReturnableFlg) {
            return "";
        }
        return productNoReturnableFlg.toString();
    }
    public void setProductNoReturnableFlg(Integer productNoReturnableFlg) {
        this.productNoReturnableFlg = productNoReturnableFlg;
    }
    public Integer getProductAmbiguousImageFlg() {
        return productAmbiguousImageFlg;
    }
    /************************************************************************
     * <b>Description:</b><br>
     *  getter not null if null return "".
     *
     * @author      Nguyen.Chuong
     * @date        Mar 14, 2014
     * @return      String
     ************************************************************************/
    @JsonIgnore
    public String getProductAmbiguousImageFlgNotNull() {
        if (null == productAmbiguousImageFlg) {
            return "";
        }
        return productAmbiguousImageFlg.toString();
    }
    public void setProductAmbiguousImageFlg(Integer productAmbiguousImageFlg) {
        this.productAmbiguousImageFlg = productAmbiguousImageFlg;
    }
    public String getDescriptionSummary() {
        return descriptionSummary;
    }
    /**
     * @param descriptionSummary the descriptionSummary to set
     */
    public void setDescriptionSummary(String descriptionSummary) {
        this.descriptionSummary = descriptionSummary;
        if (StringUtils.isNotEmpty(this.descriptionSummary)) {
        	this.descriptionSummary = this.descriptionSummary.trim();
        }
    }
    public String getDescriptionRemarks() {
        return descriptionRemarks;
    }
    /**
     * @param descriptionRemarks the descriptionRemarks to set
     */
    public void setDescriptionRemarks(String descriptionRemarks) {
        this.descriptionRemarks = descriptionRemarks;
        if (StringUtils.isNotEmpty(this.descriptionRemarks)) {
        	this.descriptionRemarks = this.descriptionRemarks.trim();
        }
    }
    public String getDescriptionCaution() {
        return descriptionCaution;
    }
    /**
     * @param descriptionCaution the descriptionCaution to set
     */
    public void setDescriptionCaution(String descriptionCaution) {
        this.descriptionCaution = descriptionCaution;
        if (StringUtils.isNotEmpty(this.descriptionCaution)) {
        	this.descriptionCaution = this.descriptionCaution.trim();
        }
    }
    public String getDescriptionSentence() {
        return descriptionSentence;
    }
    /**
     * @param descriptionSentence the descriptionSentence to set
     */
    public void setDescriptionSentence(String descriptionSentence) {
        this.descriptionSentence = descriptionSentence;
        if (StringUtils.isNotEmpty(this.descriptionSentence)) {
        	this.descriptionSentence = this.descriptionSentence.trim();
        }
    }
    public String getSiireCode1() {
        return siireCode1;
    }
    /**
     * @param siireCode1 the siireCode1 to set
     */
    public void setSiireCode1(String siireCode1) {
        this.siireCode1 = siireCode1;
        if (StringUtils.isNotEmpty(this.siireCode1)) {
        	this.siireCode1 = this.siireCode1.trim();
        }
    }
    public String getNoukiCode1() {
        return noukiCode1;
    }
    /**
     * @param noukiCode1 the noukiCode1 to set
     */
    public void setNoukiCode1(String noukiCode1) {
        this.noukiCode1 = noukiCode1;
        if (StringUtils.isNotEmpty(this.noukiCode1)) {
        	this.noukiCode1 = this.noukiCode1.trim();
        }
    }
    public String getSiireCode2() {
        return siireCode2;
    }
    /**
     * @param siireCode2 the siireCode2 to set
     */
    public void setSiireCode2(String siireCode2) {
        this.siireCode2 = siireCode2;
        if (StringUtils.isNotEmpty(this.siireCode2)) {
        	this.siireCode2 = this.siireCode2.trim();
        }
    }
    public String getNoukiCode2() {
        return noukiCode2;
    }
    /**
     * @param noukiCode2 the noukiCode2 to set
     */
    public void setNoukiCode2(String noukiCode2) {
        this.noukiCode2 = noukiCode2;
        if (StringUtils.isNotEmpty(this.noukiCode2)) {
        	this.noukiCode2 = this.noukiCode2.trim();
        }
    }
    public String getCompatibleModel() {
        return compatibleModel;
    }
    /**
     * @return mpatibleModel without null value
     */
    @JsonIgnore
    public String getcompatibleModelNotNull() {
        if (compatibleModel == null) {
            return "";
        }
        return compatibleModel;
    }
    /**
     * @param compatibleModel the compatibleModel to set
     */
    public void setCompatibleModel(String compatibleModel) {
        this.compatibleModel = compatibleModel;
        if (StringUtils.isNotEmpty(this.compatibleModel)) {
        	this.compatibleModel = this.compatibleModel.trim();
        }
    }
    public String getCustomersConfirmationItem() {
        return customersConfirmationItem;
    }
    /**
     * @return customersConfirmationItem without null value
     */
    @JsonIgnore
    public String getCustomersConfirmationItemNotNull() {
        if (customersConfirmationItem == null) {
            return "";
        }
        return customersConfirmationItem;
    }
    /**
     * @param customersConfirmationItem the customersConfirmationItem to set
     */
    public void setCustomersConfirmationItem(String customersConfirmationItem) {
        this.customersConfirmationItem = customersConfirmationItem;
        if (StringUtils.isNotEmpty(this.customersConfirmationItem)) {
        	this.customersConfirmationItem = this.customersConfirmationItem.trim();
        }
    }
    public String getLink() {
        return link;
    }
    /**
     * @return link without null value
     */
    @JsonIgnore
    public String getLinkNotNull() {
        if (link == null) {
            return "";
        }
        return link;
    }
    /**
     * @param link the link to set
     */
    public void setLink(String link) {
        this.link = link;
        if (StringUtils.isNotEmpty(this.link)) {
        	this.link = this.link.trim();
        }
    }
    public String getAnimation() {
        return animation;
    }
    /**
     * @return animationNotNull without null value
     */
    @JsonIgnore
    public String getAnimationNotNull() {
        if (animation == null) {
            return "";
        }
        return animation;
    }
    /**
     * @param animation the animation to set
     */
    public void setAnimation(String animation) {
        this.animation = animation;
        if (StringUtils.isNotEmpty(this.animation)) {
        	this.animation = this.animation.trim();
        }
    }
    public EntMstAttribute getEntMstAttribute1() {
        return entMstAttribute1;
    }
    public void setEntMstAttribute1(EntMstAttribute entMstAttribute1) {
        this.entMstAttribute1 = entMstAttribute1;
    }
    public int getAttributeCode1() {
        return attributeCode1;
    }
    public void setAttributeCode1(int attributeCode1) {
        this.attributeCode1 = attributeCode1;
    }
    public String getAttributevalues1() {
        return attributevalues1;
    }
    /**
     * @return attributevalues1 without null value
     */
    @JsonIgnore
    public String getAttributevalues1NotNull() {
        if (attributevalues1 == null) {
            return "";
        }
        return attributevalues1;
    }
    /**
     * @return attributevalues2 without null value
     */
    @JsonIgnore
    public String getAttributevalues2NotNull() {
        if (attributevalues2 == null) {
            return "";
        }
        return attributevalues2;
    }
    /**
     * @return attributevalues3 without null value
     */
    @JsonIgnore
    public String getAttributevalues3NotNull() {
        if (attributevalues3 == null) {
            return "";
        }
        return attributevalues3;
    }
    /**
     * @return attributevalues4 without null value
     */
    @JsonIgnore
    public String getAttributevalues4NotNull() {
        if (attributevalues4 == null) {
            return "";
        }
        return attributevalues4;
    }
    /**
     * @return attributevalues5 without null value
     */
    @JsonIgnore
    public String getAttributevalues5NotNull() {
        if (attributevalues5 == null) {
            return "";
        }
        return attributevalues5;
    }
    /**
     * @return attributevalues6 without null value
     */
    @JsonIgnore
    public String getAttributevalues6NotNull() {
        if (attributevalues6 == null) {
            return "";
        }
        return attributevalues6;
    }
    /**
     * @return attributevalues7 without null value
     */
    @JsonIgnore
    public String getAttributevalues7NotNull() {
        if (attributevalues7 == null) {
            return "";
        }
        return attributevalues7;
    }
    /**
     * @return attributevalues8 without null value
     */
    @JsonIgnore
    public String getAttributevalues8NotNull() {
        if (attributevalues8 == null) {
            return "";
        }
        return attributevalues8;
    }
    /**
     * @return attributevalues9 without null value
     */
    @JsonIgnore
    public String getAttributevalues9NotNull() {
        if (attributevalues9 == null) {
            return "";
        }
        return attributevalues9;
    }
    /**
     * @return attributevalues10 without null value
     */
    @JsonIgnore
    public String getAttributevalues10NotNull() {
        if (attributevalues10 == null) {
            return "";
        }
        return attributevalues10;
    }
    /**
     * @return attributevalues11 without null value
     */
    @JsonIgnore
    public String getAttributevalues11NotNull() {
        if (attributevalues11 == null) {
            return "";
        }
        return attributevalues11;
    }
    /**
     * @return attributevalues12 without null value
     */
    @JsonIgnore
    public String getAttributevalues12NotNull() {
        if (attributevalues12 == null) {
            return "";
        }
        return attributevalues12;
    }
    /**
     * @return attributevalues13 without null value
     */
    @JsonIgnore
    public String getAttributevalues13NotNull() {
        if (attributevalues13 == null) {
            return "";
        }
        return attributevalues13;
    }
    /**
     * @return attributevalues14 without null value
     */
    @JsonIgnore
    public String getAttributevalues14NotNull() {
        if (attributevalues14 == null) {
            return "";
        }
        return attributevalues14;
    }
    /**
     * @return attributevalues15 without null value
     */
    @JsonIgnore
    public String getAttributevalues15NotNull() {
        if (attributevalues15 == null) {
            return "";
        }
        return attributevalues15;
    }
    /**
     * @return attributevalues16 without null value
     */
    @JsonIgnore
    public String getAttributevalues16NotNull() {
        if (attributevalues16 == null) {
            return "";
        }
        return attributevalues16;
    }
    /**
     * @return attributevalues17 without null value
     */
    @JsonIgnore
    public String getAttributevalues17NotNull() {
        if (attributevalues17 == null) {
            return "";
        }
        return attributevalues17;
    }
    /**
     * @return attributevalues18 without null value
     */
    @JsonIgnore
    public String getAttributevalues18NotNull() {
        if (attributevalues18 == null) {
            return "";
        }
        return attributevalues18;
    }
    /**
     * @return attributevalues19 without null value
     */
    @JsonIgnore
    public String getAttributevalues19NotNull() {
        if (attributevalues19 == null) {
            return "";
        }
        return attributevalues19;
    }
    /**
     * @return attributevalues20 without null value
     */
    @JsonIgnore
    public String getAttributevalues20NotNull() {
        if (attributevalues20 == null) {
            return "";
        }
        return attributevalues20;
    }
    /**
     * @return attributevalues21 without null value
     */
    @JsonIgnore
    public String getAttributevalues21NotNull() {
        if (attributevalues21 == null) {
            return "";
        }
        return attributevalues21;
    }
    /**
     * @return attributevalues22 without null value
     */
    @JsonIgnore
    public String getAttributevalues22NotNull() {
        if (attributevalues22 == null) {
            return "";
        }
        return attributevalues22;
    }
    /**
     * @return attributevalues23 without null value
     */
    @JsonIgnore
    public String getAttributevalues23NotNull() {
        if (attributevalues23 == null) {
            return "";
        }
        return attributevalues23;
    }
    /**
     * @return attributevalues24 without null value
     */
    @JsonIgnore
    public String getAttributevalues24NotNull() {
        if (attributevalues24 == null) {
            return "";
        }
        return attributevalues24;
    }
    /**
     * @return attributevalues25 without null value
     */
    @JsonIgnore
    public String getAttributevalues25NotNull() {
        if (attributevalues25 == null) {
            return "";
        }
        return attributevalues25;
    }
    /**
     * @return attributevalues26 without null value
     */
    @JsonIgnore
    public String getAttributevalues26NotNull() {
        if (attributevalues26 == null) {
            return "";
        }
        return attributevalues26;
    }
    /**
     * @return attributevalues27 without null value
     */
    @JsonIgnore
    public String getAttributevalues27NotNull() {
        if (attributevalues27 == null) {
            return "";
        }
        return attributevalues27;
    }
    /**
     * @return attributevalues28 without null value
     */
    @JsonIgnore
    public String getAttributevalues28NotNull() {
        if (attributevalues28 == null) {
            return "";
        }
        return attributevalues28;
    }
    /**
     * @return attributevalues29 without null value
     */
    @JsonIgnore
    public String getAttributevalues29NotNull() {
        if (attributevalues29 == null) {
            return "";
        }
        return attributevalues29;
    }
    /**
     * @return attributevalues30 without null value
     */
    @JsonIgnore
    public String getAttributevalues30NotNull() {
        if (attributevalues30 == null) {
            return "";
        }
        return attributevalues30;
    }
    /**
     * @param attributevalues1 the attributevalues1 to set
     */
    public void setAttributevalues1(String attributevalues1) {
        this.attributevalues1 = attributevalues1;
        if (StringUtils.isNotEmpty(this.attributevalues1)) {
        	this.attributevalues1 = this.attributevalues1.trim();
        }
    }
    public String getAttributeDisplay1() {
        return attributeDisplay1;
    }
    /**
     * @return attributeDisplay1 without null value
     */
    @JsonIgnore
    public String getAttributeDisplay1NotNull() {
        if (attributeDisplay1 == null) {
            return "";
        }
        return attributeDisplay1;
    }
    /**
     * @return attributeDisplay2 without null value
     */
    @JsonIgnore
    public String getAttributeDisplay2NotNull() {
        if (attributeDisplay2 == null) {
            return "";
        }
        return attributeDisplay2;
    }
    /**
     * @return attributeDisplay3 without null value
     */
    @JsonIgnore
    public String getAttributeDisplay3NotNull() {
        if (attributeDisplay3 == null) {
            return "";
        }
        return attributeDisplay3;
    }
    /**
     * @return attributeDisplay4 without null value
     */
    @JsonIgnore
    public String getAttributeDisplay4NotNull() {
        if (attributeDisplay4 == null) {
            return "";
        }
        return attributeDisplay4;
    }
    /**
     * @return attributeDisplay5 without null value
     */
    @JsonIgnore
    public String getAttributeDisplay5NotNull() {
        if (attributeDisplay5 == null) {
            return "";
        }
        return attributeDisplay5;
    }
    /**
     * @return attributeDisplay6 without null value
     */
    @JsonIgnore
    public String getAttributeDisplay6NotNull() {
        if (attributeDisplay6 == null) {
            return "";
        }
        return attributeDisplay6;
    }
    /**
     * @return attributeDisplay7 without null value
     */
    @JsonIgnore
    public String getAttributeDisplay7NotNull() {
        if (attributeDisplay7 == null) {
            return "";
        }
        return attributeDisplay7;
    }
    /**
     * @return attributeDisplay8 without null value
     */
    @JsonIgnore
    public String getAttributeDisplay8NotNull() {
        if (attributeDisplay8 == null) {
            return "";
        }
        return attributeDisplay8;
    }
    /**
     * @return attributeDisplay9 without null value
     */
    @JsonIgnore
    public String getAttributeDisplay9NotNull() {
        if (attributeDisplay9 == null) {
            return "";
        }
        return attributeDisplay9;
    }
    /**
     * @return attributeDisplay10 without null value
     */
    @JsonIgnore
    public String getAttributeDisplay10NotNull() {
        if (attributeDisplay10 == null) {
            return "";
        }
        return attributeDisplay10;
    }
    /**
     * @return attributeDisplay11 without null value
     */
    @JsonIgnore
    public String getAttributeDisplay11NotNull() {
        if (attributeDisplay11 == null) {
            return "";
        }
        return attributeDisplay11;
    }
    /**
     * @return attributeDisplay12 without null value
     */
    @JsonIgnore
    public String getAttributeDisplay12NotNull() {
        if (attributeDisplay12 == null) {
            return "";
        }
        return attributeDisplay12;
    }
    /**
     * @return attributeDisplay13 without null value
     */
    @JsonIgnore
    public String getAttributeDisplay13NotNull() {
        if (attributeDisplay13 == null) {
            return "";
        }
        return attributeDisplay13;
    }
    /**
     * @return attributeDisplay14 without null value
     */
    @JsonIgnore
    public String getAttributeDisplay14NotNull() {
        if (attributeDisplay14 == null) {
            return "";
        }
        return attributeDisplay14;
    }
    /**
     * @return attributeDisplay15 without null value
     */
    @JsonIgnore
    public String getAttributeDisplay15NotNull() {
        if (attributeDisplay15 == null) {
            return "";
        }
        return attributeDisplay15;
    }
    /**
     * @return attributeDisplay16 without null value
     */
    @JsonIgnore
    public String getAttributeDisplay16NotNull() {
        if (attributeDisplay16 == null) {
            return "";
        }
        return attributeDisplay16;
    }
    /**
     * @return attributeDisplay17 without null value
     */
    @JsonIgnore
    public String getAttributeDisplay17NotNull() {
        if (attributeDisplay17 == null) {
            return "";
        }
        return attributeDisplay17;
    }
    /**
     * @return attributeDisplay18 without null value
     */
    @JsonIgnore
    public String getAttributeDisplay18NotNull() {
        if (attributeDisplay18 == null) {
            return "";
        }
        return attributeDisplay18;
    }
    /**
     * @return attributeDisplay19 without null value
     */
    @JsonIgnore
    public String getAttributeDisplay19NotNull() {
        if (attributeDisplay19 == null) {
            return "";
        }
        return attributeDisplay19;
    }
    /**
     * @return attributeDisplay20 without null value
     */
    @JsonIgnore
    public String getAttributeDisplay20NotNull() {
        if (attributeDisplay20 == null) {
            return "";
        }
        return attributeDisplay20;
    }
    /**
     * @return attributeDisplay21 without null value
     */
    @JsonIgnore
    public String getAttributeDisplay21NotNull() {
        if (attributeDisplay21 == null) {
            return "";
        }
        return attributeDisplay21;
    }
    /**
     * @return attributeDisplay22 without null value
     */
    @JsonIgnore
    public String getAttributeDisplay22NotNull() {
        if (attributeDisplay22 == null) {
            return "";
        }
        return attributeDisplay22;
    }
    /**
     * @return attributeDisplay23 without null value
     */
    @JsonIgnore
    public String getAttributeDisplay23NotNull() {
        if (attributeDisplay23 == null) {
            return "";
        }
        return attributeDisplay23;
    }
    /**
     * @return attributeDisplay24 without null value
     */
    @JsonIgnore
    public String getAttributeDisplay24NotNull() {
        if (attributeDisplay24 == null) {
            return "";
        }
        return attributeDisplay24;
    }
    /**
     * @return attributeDisplay25 without null value
     */
    @JsonIgnore
    public String getAttributeDisplay25NotNull() {
        if (attributeDisplay25 == null) {
            return "";
        }
        return attributeDisplay25;
    }
    /**
     * @return attributeDisplay26 without null value
     */
    @JsonIgnore
    public String getAttributeDisplay26NotNull() {
        if (attributeDisplay26 == null) {
            return "";
        }
        return attributeDisplay26;
    }
    /**
     * @return attributeDisplay27 without null value
     */
    @JsonIgnore
    public String getAttributeDisplay27NotNull() {
        if (attributeDisplay27 == null) {
            return "";
        }
        return attributeDisplay27;
    }
    /**
     * @return attributeDisplay28 without null value
     */
    @JsonIgnore
    public String getAttributeDisplay28NotNull() {
        if (attributeDisplay28 == null) {
            return "";
        }
        return attributeDisplay28;
    }
    /**
     * @return attributeDisplay29 without null value
     */
    @JsonIgnore
    public String getAttributeDisplay29NotNull() {
        if (attributeDisplay29 == null) {
            return "";
        }
        return attributeDisplay29;
    }
    /**
     * @return attributeDisplay30 without null value
     */
    @JsonIgnore
    public String getAttributeDisplay30NotNull() {
        if (attributeDisplay30 == null) {
            return "";
        }
        return attributeDisplay30;
    }
    /**
     * @param attributeDisplay1 the attributeDisplay1 to set
     */
    public void setAttributeDisplay1(String attributeDisplay1) {
        this.attributeDisplay1 = attributeDisplay1;
        if (StringUtils.isNotEmpty(this.attributeDisplay1)) {
        	this.attributeDisplay1 = this.attributeDisplay1.trim();
        }
    }
    public EntMstAttribute getEntMstAttribute2() {
        return entMstAttribute2;
    }
    public void setEntMstAttribute2(EntMstAttribute entMstAttribute2) {
        this.entMstAttribute2 = entMstAttribute2;
    }
    public int getAttributeCode2() {
        return attributeCode2;
    }
    public void setAttributeCode2(int attributeCode2) {
        this.attributeCode2 = attributeCode2;
    }
    public String getAttributevalues2() {
        return attributevalues2;
    }
    /**
     * @param attributevalues2 the attributevalues2 to set
     */
    public void setAttributevalues2(String attributevalues2) {
        this.attributevalues2 = attributevalues2;
        if (StringUtils.isNotEmpty(this.attributevalues2)) {
        	this.attributevalues2 = this.attributevalues2.trim();
        }
    }
    public String getAttributeDisplay2() {
        return attributeDisplay2;
    }
    /**
     * @param attributeDisplay2 the attributeDisplay2 to set
     */
    public void setAttributeDisplay2(String attributeDisplay2) {
        this.attributeDisplay2 = attributeDisplay2;
        if (StringUtils.isNotEmpty(this.attributeDisplay2)) {
        	this.attributeDisplay2 = this.attributeDisplay2.trim();
        }
    }
    public EntMstAttribute getEntMstAttribute3() {
        return entMstAttribute3;
    }
    public void setEntMstAttribute3(EntMstAttribute entMstAttribute3) {
        this.entMstAttribute3 = entMstAttribute3;
    }
    public int getAttributeCode3() {
        return attributeCode3;
    }
    public void setAttributeCode3(int attributeCode3) {
        this.attributeCode3 = attributeCode3;
    }
    public String getAttributevalues3() {
        return attributevalues3;
    }
    /**
     * @param attributevalues3 the attributevalues3 to set
     */
    public void setAttributevalues3(String attributevalues3) {
        this.attributevalues3 = attributevalues3;
        if (StringUtils.isNotEmpty(this.attributevalues3)) {
        	this.attributevalues3 = this.attributevalues3.trim();
        }
    }
    public String getAttributeDisplay3() {
        return attributeDisplay3;
    }
    /**
     * @param attributeDisplay3 the attributeDisplay3 to set
     */
    public void setAttributeDisplay3(String attributeDisplay3) {
        this.attributeDisplay3 = attributeDisplay3;
        if (StringUtils.isNotEmpty(this.attributeDisplay3)) {
        	this.attributeDisplay3 = this.attributeDisplay3.trim();
        }
    }
    public EntMstAttribute getEntMstAttribute4() {
        return entMstAttribute4;
    }
    public void setEntMstAttribute4(EntMstAttribute entMstAttribute4) {
        this.entMstAttribute4 = entMstAttribute4;
    }
    public int getAttributeCode4() {
        return attributeCode4;
    }
    public void setAttributeCode4(int attributeCode4) {
        this.attributeCode4 = attributeCode4;
    }
    public String getAttributevalues4() {
        return attributevalues4;
    }
    /**
     * @param attributevalues4 the attributevalues4 to set
     */
    public void setAttributevalues4(String attributevalues4) {
        this.attributevalues4 = attributevalues4;
        if (StringUtils.isNotEmpty(this.attributevalues4)) {
        	this.attributevalues4 = this.attributevalues4.trim();
        }
    }
    public String getAttributeDisplay4() {
        return attributeDisplay4;
    }
    /**
     * @param attributeDisplay4 the attributeDisplay4 to set
     */
    public void setAttributeDisplay4(String attributeDisplay4) {
        this.attributeDisplay4 = attributeDisplay4;
        if (StringUtils.isNotEmpty(this.attributeDisplay4)) {
        	this.attributeDisplay4 = this.attributeDisplay4.trim();
        }
    }
    public EntMstAttribute getEntMstAttribute5() {
        return entMstAttribute5;
    }
    public void setEntMstAttribute5(EntMstAttribute entMstAttribute5) {
        this.entMstAttribute5 = entMstAttribute5;
    }
    public int getAttributeCode5() {
        return attributeCode5;
    }
    public void setAttributeCode5(int attributeCode5) {
        this.attributeCode5 = attributeCode5;
    }
    public String getAttributevalues5() {
        return attributevalues5;
    }
    /**
     * @param attributevalues5 the attributevalues5 to set
     */
    public void setAttributevalues5(String attributevalues5) {
        this.attributevalues5 = attributevalues5;
        if (StringUtils.isNotEmpty(this.attributevalues5)) {
        	this.attributevalues5 = this.attributevalues5.trim();
        }
    }
    public String getAttributeDisplay5() {
        return attributeDisplay5;
    }
    /**
     * @param attributeDisplay5 the attributeDisplay5 to set
     */
    public void setAttributeDisplay5(String attributeDisplay5) {
        this.attributeDisplay5 = attributeDisplay5;
        if (StringUtils.isNotEmpty(this.attributeDisplay5)) {
        	this.productName = this.productName.trim();
        }
    }
    public EntMstAttribute getEntMstAttribute6() {
        return entMstAttribute6;
    }
    public void setEntMstAttribute6(EntMstAttribute entMstAttribute6) {
        this.entMstAttribute6 = entMstAttribute6;
    }
    public int getAttributeCode6() {
        return attributeCode6;
    }
    public void setAttributeCode6(int attributeCode6) {
        this.attributeCode6 = attributeCode6;
    }
    public String getAttributevalues6() {
        return attributevalues6;
    }
    /**
     * @param attributevalues6 the attributevalues6 to set
     */
    public void setAttributevalues6(String attributevalues6) {
        this.attributevalues6 = attributevalues6;
        if (StringUtils.isNotEmpty(this.attributevalues6)) {
        	this.attributevalues6 = this.attributevalues6.trim();
        }
    }
    public String getAttributeDisplay6() {
        return attributeDisplay6;
    }
    /**
     * @param attributeDisplay6 the attributeDisplay6 to set
     */
    public void setAttributeDisplay6(String attributeDisplay6) {
        this.attributeDisplay6 = attributeDisplay6;
        if (StringUtils.isNotEmpty(this.attributeDisplay6)) {
        	this.attributeDisplay6 = this.attributeDisplay6.trim();
        }
    }
    public EntMstAttribute getEntMstAttribute7() {
        return entMstAttribute7;
    }
    public void setEntMstAttribute7(EntMstAttribute entMstAttribute7) {
        this.entMstAttribute7 = entMstAttribute7;
    }
    public int getAttributeCode7() {
        return attributeCode7;
    }
    public void setAttributeCode7(int attributeCode7) {
        this.attributeCode7 = attributeCode7;
    }
    public String getAttributevalues7() {
        return attributevalues7;
    }
    /**
     * @param attributevalues7 the attributevalues7 to set
     */
    public void setAttributevalues7(String attributevalues7) {
        this.attributevalues7 = attributevalues7;
        if (StringUtils.isNotEmpty(this.attributevalues7)) {
        	this.attributevalues7 = this.attributevalues7.trim();
        }
    }
    public String getAttributeDisplay7() {
        return attributeDisplay7;
    }
    /**
     * @param attributeDisplay7 the attributeDisplay7 to set
     */
    public void setAttributeDisplay7(String attributeDisplay7) {
        this.attributeDisplay7 = attributeDisplay7;
        if (StringUtils.isNotEmpty(this.attributeDisplay7)) {
        	this.attributeDisplay7 = this.attributeDisplay7.trim();
        }
    }
    public EntMstAttribute getEntMstAttribute8() {
        return entMstAttribute8;
    }
    public void setEntMstAttribute8(EntMstAttribute entMstAttribute8) {
        this.entMstAttribute8 = entMstAttribute8;
    }
    public int getAttributeCode8() {
        return attributeCode8;
    }
    public void setAttributeCode8(int attributeCode8) {
        this.attributeCode8 = attributeCode8;
    }
    public String getAttributevalues8() {
        return attributevalues8;
    }
    /**
     * @param attributevalues8 the attributevalues8 to set
     */
    public void setAttributevalues8(String attributevalues8) {
        this.attributevalues8 = attributevalues8;
        if (StringUtils.isNotEmpty(this.attributevalues8)) {
        	this.attributevalues8 = this.attributevalues8.trim();
        }
    }
    public String getAttributeDisplay8() {
        return attributeDisplay8;
    }
    /**
     * @param attributeDisplay8 the attributeDisplay8 to set
     */
    public void setAttributeDisplay8(String attributeDisplay8) {
        this.attributeDisplay8 = attributeDisplay8;
        if (StringUtils.isNotEmpty(this.attributeDisplay8)) {
        	this.attributeDisplay8 = this.attributeDisplay8.trim();
        }
    }
    public EntMstAttribute getEntMstAttribute9() {
        return entMstAttribute9;
    }
    public void setEntMstAttribute9(EntMstAttribute entMstAttribute9) {
        this.entMstAttribute9 = entMstAttribute9;
    }
    public int getAttributeCode9() {
        return attributeCode9;
    }
    public void setAttributeCode9(int attributeCode9) {
        this.attributeCode9 = attributeCode9;
    }
    public String getAttributevalues9() {
        return attributevalues9;
    }
    /**
     * @param attributevalues9 the attributevalues9 to set
     */
    public void setAttributevalues9(String attributevalues9) {
        this.attributevalues9 = attributevalues9;
        if (StringUtils.isNotEmpty(this.attributevalues9)) {
        	this.attributevalues9 = this.attributevalues9.trim();
        }
    }
    public String getAttributeDisplay9() {
        return attributeDisplay9;
    }
    /**
     * @param attributeDisplay9 the attributeDisplay9 to set
     */
    public void setAttributeDisplay9(String attributeDisplay9) {
        this.attributeDisplay9 = attributeDisplay9;
        if (StringUtils.isNotEmpty(this.attributeDisplay9)) {
        	this.attributeDisplay9 = this.attributeDisplay9.trim();
        }
    }
    public EntMstAttribute getEntMstAttribute10() {
        return entMstAttribute10;
    }
    public void setEntMstAttribute10(EntMstAttribute entMstAttribute10) {
        this.entMstAttribute10 = entMstAttribute10;
    }
    public int getAttributeCode10() {
        return attributeCode10;
    }
    public void setAttributeCode10(int attributeCode10) {
        this.attributeCode10 = attributeCode10;
    }
    public String getAttributevalues10() {
        return attributevalues10;
    }
    /**
     * @param attributevalues10 the attributevalues10 to set
     */
    public void setAttributevalues10(String attributevalues10) {
        this.attributevalues10 = attributevalues10;
        if (StringUtils.isNotEmpty(this.attributevalues10)) {
        	this.attributevalues10 = this.attributevalues10.trim();
        }
    }
    public String getAttributeDisplay10() {
        return attributeDisplay10;
    }
    /**
     * @param attributeDisplay10 the attributeDisplay10 to set
     */
    public void setAttributeDisplay10(String attributeDisplay10) {
        this.attributeDisplay10 = attributeDisplay10;
        if (StringUtils.isNotEmpty(this.attributeDisplay10)) {
        	this.attributeDisplay10 = this.attributeDisplay10.trim();
        }
    }
    public EntMstAttribute getEntMstAttribute11() {
        return entMstAttribute11;
    }
    public void setEntMstAttribute11(EntMstAttribute entMstAttribute11) {
        this.entMstAttribute11 = entMstAttribute11;
    }
    public int getAttributeCode11() {
        return attributeCode11;
    }
    public void setAttributeCode11(int attributeCode11) {
        this.attributeCode11 = attributeCode11;
    }
    public String getAttributevalues11() {
        return attributevalues11;
    }
    /**
     * @param attributevalues11 the attributevalues11 to set
     */
    public void setAttributevalues11(String attributevalues11) {
        this.attributevalues11 = attributevalues11;
        if (StringUtils.isNotEmpty(this.attributevalues11)) {
        	this.attributevalues11 = this.attributevalues11.trim();
        }
    }
    public String getAttributeDisplay11() {
        return attributeDisplay11;
    }
    /**
     * @param attributeDisplay11 the attributeDisplay11 to set
     */
    public void setAttributeDisplay11(String attributeDisplay11) {
        this.attributeDisplay11 = attributeDisplay11;
        if (StringUtils.isNotEmpty(this.attributeDisplay11)) {
        	this.attributeDisplay11 = this.attributeDisplay11.trim();
        }
    }
    public EntMstAttribute getEntMstAttribute12() {
        return entMstAttribute12;
    }
    public void setEntMstAttribute12(EntMstAttribute entMstAttribute12) {
        this.entMstAttribute12 = entMstAttribute12;
    }
    public int getAttributeCode12() {
        return attributeCode12;
    }
    public void setAttributeCode12(int attributeCode12) {
        this.attributeCode12 = attributeCode12;
    }
    public String getAttributevalues12() {
        return attributevalues12;
    }
    /**
     * @param attributevalues12 the attributevalues12 to set
     */
    public void setAttributevalues12(String attributevalues12) {
        this.attributevalues12 = attributevalues12;
        if (StringUtils.isNotEmpty(this.attributevalues12)) {
        	this.attributevalues12 = this.attributevalues12.trim();
        }
    }
    public String getAttributeDisplay12() {
        return attributeDisplay12;
    }
    /**
     * @param attributeDisplay12 the attributeDisplay12 to set
     */
    public void setAttributeDisplay12(String attributeDisplay12) {
        this.attributeDisplay12 = attributeDisplay12;
        if (StringUtils.isNotEmpty(this.attributeDisplay12)) {
        	this.attributeDisplay12 = this.attributeDisplay12.trim();
        }
    }
    public EntMstAttribute getEntMstAttribute13() {
        return entMstAttribute13;
    }
    public void setEntMstAttribute13(EntMstAttribute entMstAttribute13) {
        this.entMstAttribute13 = entMstAttribute13;
    }
    public int getAttributeCode13() {
        return attributeCode13;
    }
    public void setAttributeCode13(int attributeCode13) {
        this.attributeCode13 = attributeCode13;
    }
    public String getAttributevalues13() {
        return attributevalues13;
    }
    /**
     * @param attributevalues13 the attributevalues13 to set
     */
    public void setAttributevalues13(String attributevalues13) {
        this.attributevalues13 = attributevalues13;
        if (StringUtils.isNotEmpty(this.attributevalues13)) {
        	this.attributevalues13 = this.attributevalues13.trim();
        }
    }
    public String getAttributeDisplay13() {
        return attributeDisplay13;
    }
    /**
     * @param attributeDisplay13 the attributeDisplay13 to set
     */
    public void setAttributeDisplay13(String attributeDisplay13) {
        this.attributeDisplay13 = attributeDisplay13;
        if (StringUtils.isNotEmpty(this.attributeDisplay13)) {
        	this.attributeDisplay13 = this.attributeDisplay13.trim();
        }
    }
    public EntMstAttribute getEntMstAttribute14() {
        return entMstAttribute14;
    }
    public void setEntMstAttribute14(EntMstAttribute entMstAttribute14) {
        this.entMstAttribute14 = entMstAttribute14;
    }
    public int getAttributeCode14() {
        return attributeCode14;
    }
    public void setAttributeCode14(int attributeCode14) {
        this.attributeCode14 = attributeCode14;
    }
    public String getAttributevalues14() {
        return attributevalues14;
    }
    /**
     * @param attributevalues14 the attributevalues14 to set
     */
    public void setAttributevalues14(String attributevalues14) {
        this.attributevalues14 = attributevalues14;
        if (StringUtils.isNotEmpty(this.attributevalues14)) {
        	this.attributevalues14 = this.attributevalues14.trim();
        }
    }
    public String getAttributeDisplay14() {
        return attributeDisplay14;
    }
    /**
     * @param attributeDisplay14 the attributeDisplay14 to set
     */
    public void setAttributeDisplay14(String attributeDisplay14) {
        this.attributeDisplay14 = attributeDisplay14;
        if (StringUtils.isNotEmpty(this.attributeDisplay14)) {
        	this.attributeDisplay14 = this.attributeDisplay14.trim();
        }
    }
    public EntMstAttribute getEntMstAttribute15() {
        return entMstAttribute15;
    }
    public void setEntMstAttribute15(EntMstAttribute entMstAttribute15) {
        this.entMstAttribute15 = entMstAttribute15;
    }
    public int getAttributeCode15() {
        return attributeCode15;
    }
    public void setAttributeCode15(int attributeCode15) {
        this.attributeCode15 = attributeCode15;
    }
    public String getAttributevalues15() {
        return attributevalues15;
    }
    /**
     * @param attributevalues15 the attributevalues15 to set
     */
    public void setAttributevalues15(String attributevalues15) {
        this.attributevalues15 = attributevalues15;
        if (StringUtils.isNotEmpty(this.attributevalues15)) {
        	this.attributevalues15 = this.attributevalues15.trim();
        }
    }
    public String getAttributeDisplay15() {
        return attributeDisplay15;
    }
    /**
     * @param attributeDisplay15 the attributeDisplay15 to set
     */
    public void setAttributeDisplay15(String attributeDisplay15) {
        this.attributeDisplay15 = attributeDisplay15;
        if (StringUtils.isNotEmpty(this.attributeDisplay15)) {
        	this.attributeDisplay15 = this.attributeDisplay15.trim();
        }
    }
    public EntMstAttribute getEntMstAttribute16() {
        return entMstAttribute16;
    }
    public void setEntMstAttribute16(EntMstAttribute entMstAttribute16) {
        this.entMstAttribute16 = entMstAttribute16;
    }
    public int getAttributeCode16() {
        return attributeCode16;
    }
    public void setAttributeCode16(int attributeCode16) {
        this.attributeCode16 = attributeCode16;
    }
    public String getAttributevalues16() {
        return attributevalues16;
    }
    /**
     * @param attributevalues16 the attributevalues16 to set
     */
    public void setAttributevalues16(String attributevalues16) {
        this.attributevalues16 = attributevalues16;
        if (StringUtils.isNotEmpty(this.attributevalues16)) {
        	this.attributevalues16 = this.attributevalues16.trim();
        }
    }
    public String getAttributeDisplay16() {
        return attributeDisplay16;
    }
    /**
     * @param attributeDisplay16 the attributeDisplay16 to set
     */
    public void setAttributeDisplay16(String attributeDisplay16) {
        this.attributeDisplay16 = attributeDisplay16;
        if (StringUtils.isNotEmpty(this.attributeDisplay16)) {
        	this.attributeDisplay16 = this.attributeDisplay16.trim();
        }
    }
    public EntMstAttribute getEntMstAttribute17() {
        return entMstAttribute17;
    }
    public void setEntMstAttribute17(EntMstAttribute entMstAttribute17) {
        this.entMstAttribute17 = entMstAttribute17;
    }
    public int getAttributeCode17() {
        return attributeCode17;
    }
    public void setAttributeCode17(int attributeCode17) {
        this.attributeCode17 = attributeCode17;
    }
    public String getAttributevalues17() {
        return attributevalues17;
    }
    /**
     * @param attributevalues17 the attributevalues17 to set
     */
    public void setAttributevalues17(String attributevalues17) {
        this.attributevalues17 = attributevalues17;
        if (StringUtils.isNotEmpty(this.attributevalues17)) {
        	this.attributevalues17 = this.attributevalues17.trim();
        }
    }
    public String getAttributeDisplay17() {
        return attributeDisplay17;
    }
    /**
     * @param attributeDisplay17 the attributeDisplay17 to set
     */
    public void setAttributeDisplay17(String attributeDisplay17) {
        this.attributeDisplay17 = attributeDisplay17;
        if (StringUtils.isNotEmpty(this.attributeDisplay17)) {
        	this.attributeDisplay17 = this.attributeDisplay17.trim();
        }
    }
    public EntMstAttribute getEntMstAttribute18() {
        return entMstAttribute18;
    }
    public void setEntMstAttribute18(EntMstAttribute entMstAttribute18) {
        this.entMstAttribute18 = entMstAttribute18;
    }
    public int getAttributeCode18() {
        return attributeCode18;
    }
    public void setAttributeCode18(int attributeCode18) {
        this.attributeCode18 = attributeCode18;
    }
    public String getAttributevalues18() {
        return attributevalues18;
    }
    /**
     * @param attributevalues18 the attributevalues18 to set
     */
    public void setAttributevalues18(String attributevalues18) {
        this.attributevalues18 = attributevalues18;
        if (StringUtils.isNotEmpty(this.attributevalues18)) {
        	this.attributevalues18 = this.attributevalues18.trim();
        }
    }
    public String getAttributeDisplay18() {
        return attributeDisplay18;
    }
    /**
     * @param attributeDisplay18 the attributeDisplay18 to set
     */
    public void setAttributeDisplay18(String attributeDisplay18) {
        this.attributeDisplay18 = attributeDisplay18;
        if (StringUtils.isNotEmpty(this.attributeDisplay18)) {
        	this.attributeDisplay18 = this.attributeDisplay18.trim();
        }
    }
    public EntMstAttribute getEntMstAttribute19() {
        return entMstAttribute19;
    }
    public void setEntMstAttribute19(EntMstAttribute entMstAttribute19) {
        this.entMstAttribute19 = entMstAttribute19;
    }
    public int getAttributeCode19() {
        return attributeCode19;
    }
    public void setAttributeCode19(int attributeCode19) {
        this.attributeCode19 = attributeCode19;
    }
    public String getAttributevalues19() {
        return attributevalues19;
    }
    /**
     * @param attributevalues19 the attributevalues19 to set
     */
    public void setAttributevalues19(String attributevalues19) {
        this.attributevalues19 = attributevalues19;
        if (StringUtils.isNotEmpty(this.attributevalues19)) {
        	this.attributevalues19 = this.attributevalues19.trim();
        }
    }
    public String getAttributeDisplay19() {
        return attributeDisplay19;
    }
    /**
     * @param attributeDisplay19 the attributeDisplay19 to set
     */
    public void setAttributeDisplay19(String attributeDisplay19) {
        this.attributeDisplay19 = attributeDisplay19;
        if (StringUtils.isNotEmpty(this.attributeDisplay19)) {
        	this.attributeDisplay19 = this.attributeDisplay19.trim();
        }
    }
    public EntMstAttribute getEntMstAttribute20() {
        return entMstAttribute20;
    }
    public void setEntMstAttribute20(EntMstAttribute entMstAttribute20) {
        this.entMstAttribute20 = entMstAttribute20;
    }
    public int getAttributeCode20() {
        return attributeCode20;
    }
    public void setAttributeCode20(int attributeCode20) {
        this.attributeCode20 = attributeCode20;
    }
    public String getAttributevalues20() {
        return attributevalues20;
    }
    /**
     * @param attributevalues20 the attributevalues20 to set
     */
    public void setAttributevalues20(String attributevalues20) {
        this.attributevalues20 = attributevalues20;
        if (StringUtils.isNotEmpty(this.attributevalues20)) {
        	this.attributevalues20 = this.attributevalues20.trim();
        }
    }
    public String getAttributeDisplay20() {
        return attributeDisplay20;
    }
    /**
     * @param attributeDisplay20 the attributeDisplay20 to set
     */
    public void setAttributeDisplay20(String attributeDisplay20) {
        this.attributeDisplay20 = attributeDisplay20;
        if (StringUtils.isNotEmpty(this.attributeDisplay20)) {
        	this.attributeDisplay20 = this.attributeDisplay20.trim();
        }
    }
    public EntMstAttribute getEntMstAttribute21() {
        return entMstAttribute21;
    }
    public void setEntMstAttribute21(EntMstAttribute entMstAttribute21) {
        this.entMstAttribute21 = entMstAttribute21;
    }
    public int getAttributeCode21() {
        return attributeCode21;
    }
    public void setAttributeCode21(int attributeCode21) {
        this.attributeCode21 = attributeCode21;
    }
    public String getAttributevalues21() {
        return attributevalues21;
    }
    /**
     * @param attributevalues21 the attributevalues21 to set
     */
    public void setAttributevalues21(String attributevalues21) {
        this.attributevalues21 = attributevalues21;
        if (StringUtils.isNotEmpty(this.attributevalues21)) {
        	this.attributevalues21 = this.attributevalues21.trim();
        }
    }
    public String getAttributeDisplay21() {
        return attributeDisplay21;
    }
    /**
     * @param attributeDisplay21 the attributeDisplay21 to set
     */
    public void setAttributeDisplay21(String attributeDisplay21) {
        this.attributeDisplay21 = attributeDisplay21;
        if (StringUtils.isNotEmpty(this.attributeDisplay21)) {
        	this.attributeDisplay21 = this.attributeDisplay21.trim();
        }
    }
    public EntMstAttribute getEntMstAttribute22() {
        return entMstAttribute22;
    }
    public void setEntMstAttribute22(EntMstAttribute entMstAttribute22) {
        this.entMstAttribute22 = entMstAttribute22;
    }
    public int getAttributeCode22() {
        return attributeCode22;
    }
    public void setAttributeCode22(int attributeCode22) {
        this.attributeCode22 = attributeCode22;
    }
    public String getAttributevalues22() {
        return attributevalues22;
    }
    /**
     * @param attributevalues22 the attributevalues22 to set
     */
    public void setAttributevalues22(String attributevalues22) {
        this.attributevalues22 = attributevalues22;
        if (StringUtils.isNotEmpty(this.attributevalues22)) {
        	this.attributevalues22 = this.attributevalues22.trim();
        }
    }
    public String getAttributeDisplay22() {
        return attributeDisplay22;
    }
    /**
     * @param attributeDisplay22 the attributeDisplay22 to set
     */
    public void setAttributeDisplay22(String attributeDisplay22) {
        this.attributeDisplay22 = attributeDisplay22;
        if (StringUtils.isNotEmpty(this.attributeDisplay22)) {
        	this.attributeDisplay22 = this.attributeDisplay22.trim();
        }
    }
    public EntMstAttribute getEntMstAttribute23() {
        return entMstAttribute23;
    }
    public void setEntMstAttribute23(EntMstAttribute entMstAttribute23) {
        this.entMstAttribute23 = entMstAttribute23;
    }
    public int getAttributeCode23() {
        return attributeCode23;
    }
    public void setAttributeCode23(int attributeCode23) {
        this.attributeCode23 = attributeCode23;
    }
    public String getAttributevalues23() {
        return attributevalues23;
    }
    /**
     * @param attributevalues23 the attributevalues23 to set
     */
    public void setAttributevalues23(String attributevalues23) {
        this.attributevalues23 = attributevalues23;
        if (StringUtils.isNotEmpty(this.attributevalues23)) {
        	this.attributevalues23 = this.attributevalues23.trim();
        }
    }
    public String getAttributeDisplay23() {
        return attributeDisplay23;
    }
    /**
     * @param attributeDisplay23 the attributeDisplay23 to set
     */
    public void setAttributeDisplay23(String attributeDisplay23) {
        this.attributeDisplay23 = attributeDisplay23;
        if (StringUtils.isNotEmpty(this.attributeDisplay23)) {
        	this.attributeDisplay23 = this.attributeDisplay23.trim();
        }
    }
    public EntMstAttribute getEntMstAttribute24() {
        return entMstAttribute24;
    }
    public void setEntMstAttribute24(EntMstAttribute entMstAttribute24) {
        this.entMstAttribute24 = entMstAttribute24;
    }
    public int getAttributeCode24() {
        return attributeCode24;
    }
    public void setAttributeCode24(int attributeCode24) {
        this.attributeCode24 = attributeCode24;
    }
    public String getAttributevalues24() {
        return attributevalues24;
    }
    /**
     * @param attributevalues24 the attributevalues24 to set
     */
    public void setAttributevalues24(String attributevalues24) {
        this.attributevalues24 = attributevalues24;
        if (StringUtils.isNotEmpty(this.attributevalues24)) {
        	this.attributevalues24 = this.attributevalues24.trim();
        }
    }
    public String getAttributeDisplay24() {
        return attributeDisplay24;
    }
    /**
     * @param attributeDisplay24 the attributeDisplay24 to set
     */
    public void setAttributeDisplay24(String attributeDisplay24) {
        this.attributeDisplay24 = attributeDisplay24;
        if (StringUtils.isNotEmpty(this.attributeDisplay24)) {
        	this.attributeDisplay24 = this.attributeDisplay24.trim();
        }
    }
    public EntMstAttribute getEntMstAttribute25() {
        return entMstAttribute25;
    }
    public void setEntMstAttribute25(EntMstAttribute entMstAttribute25) {
        this.entMstAttribute25 = entMstAttribute25;
    }
    public int getAttributeCode25() {
        return attributeCode25;
    }
    public void setAttributeCode25(int attributeCode25) {
        this.attributeCode25 = attributeCode25;
    }
    public String getAttributevalues25() {
        return attributevalues25;
    }
    /**
     * @param attributevalues25 the attributevalues25 to set
     */
    public void setAttributevalues25(String attributevalues25) {
        this.attributevalues25 = attributevalues25;
        if (StringUtils.isNotEmpty(this.attributevalues25)) {
        	this.attributevalues25 = this.attributevalues25.trim();
        }
    }
    public String getAttributeDisplay25() {
        return attributeDisplay25;
    }
    /**
     * @param attributeDisplay25 the attributeDisplay25 to set
     */
    public void setAttributeDisplay25(String attributeDisplay25) {
        this.attributeDisplay25 = attributeDisplay25;
        if (StringUtils.isNotEmpty(this.attributeDisplay25)) {
        	this.attributeDisplay25 = this.attributeDisplay25.trim();
        }
    }
    public EntMstAttribute getEntMstAttribute26() {
        return entMstAttribute26;
    }
    public void setEntMstAttribute26(EntMstAttribute entMstAttribute26) {
        this.entMstAttribute26 = entMstAttribute26;
    }
    public int getAttributeCode26() {
        return attributeCode26;
    }
    public void setAttributeCode26(int attributeCode26) {
        this.attributeCode26 = attributeCode26;
    }
    public String getAttributevalues26() {
        return attributevalues26;
    }
    /**
     * @param attributevalues26 the attributevalues26 to set
     */
    public void setAttributevalues26(String attributevalues26) {
        this.attributevalues26 = attributevalues26;
        if (StringUtils.isNotEmpty(this.attributevalues26)) {
        	this.attributevalues26 = this.attributevalues26.trim();
        }
    }
    public String getAttributeDisplay26() {
        return attributeDisplay26;
    }
    /**
     * @param attributeDisplay26 the attributeDisplay26 to set
     */
    public void setAttributeDisplay26(String attributeDisplay26) {
        this.attributeDisplay26 = attributeDisplay26;
        if (StringUtils.isNotEmpty(this.attributeDisplay26)) {
        	this.attributeDisplay26 = this.attributeDisplay26.trim();
        }
    }
    public EntMstAttribute getEntMstAttribute27() {
        return entMstAttribute27;
    }
    public void setEntMstAttribute27(EntMstAttribute entMstAttribute27) {
        this.entMstAttribute27 = entMstAttribute27;
    }
    public int getAttributeCode27() {
        return attributeCode27;
    }
    public void setAttributeCode27(int attributeCode27) {
        this.attributeCode27 = attributeCode27;
    }
    public String getAttributevalues27() {
        return attributevalues27;
    }
    /**
     * @param attributevalues27 the attributevalues27 to set
     */
    public void setAttributevalues27(String attributevalues27) {
        this.attributevalues27 = attributevalues27;
        if (StringUtils.isNotEmpty(this.attributevalues27)) {
        	this.attributevalues27 = this.attributevalues27.trim();
        }
    }
    public String getAttributeDisplay27() {
        return attributeDisplay27;
    }
    /**
     * @param attributeDisplay27 the attributeDisplay27 to set
     */
    public void setAttributeDisplay27(String attributeDisplay27) {
        this.attributeDisplay27 = attributeDisplay27;
        if (StringUtils.isNotEmpty(this.attributeDisplay27)) {
        	this.attributeDisplay27 = this.attributeDisplay27.trim();
        }
    }
    public EntMstAttribute getEntMstAttribute28() {
        return entMstAttribute28;
    }
    public void setEntMstAttribute28(EntMstAttribute entMstAttribute28) {
        this.entMstAttribute28 = entMstAttribute28;
    }
    public int getAttributeCode28() {
        return attributeCode28;
    }
    public void setAttributeCode28(int attributeCode28) {
        this.attributeCode28 = attributeCode28;
    }
    public String getAttributevalues28() {
        return attributevalues28;
    }
    /**
     * @param attributevalues28 the attributevalues28 to set
     */
    public void setAttributevalues28(String attributevalues28) {
        this.attributevalues28 = attributevalues28;
        if (StringUtils.isNotEmpty(this.attributevalues28)) {
        	this.attributevalues28 = this.attributevalues28.trim();
        }
    }
    public String getAttributeDisplay28() {
        return attributeDisplay28;
    }
    /**
     * @param attributeDisplay28 the attributeDisplay28 to set
     */
    public void setAttributeDisplay28(String attributeDisplay28) {
        this.attributeDisplay28 = attributeDisplay28;
        if (StringUtils.isNotEmpty(this.attributeDisplay28)) {
        	this.attributeDisplay28 = this.attributeDisplay28.trim();
        }
    }
    public EntMstAttribute getEntMstAttribute29() {
        return entMstAttribute29;
    }
    public void setEntMstAttribute29(EntMstAttribute entMstAttribute29) {
        this.entMstAttribute29 = entMstAttribute29;
    }
    public int getAttributeCode29() {
        return attributeCode29;
    }
    public void setAttributeCode29(int attributeCode29) {
        this.attributeCode29 = attributeCode29;
    }
    public String getAttributevalues29() {
        return attributevalues29;
    }
    /**
     * @param attributevalues29 the attributevalues29 to set
     */
    public void setAttributevalues29(String attributevalues29) {
        this.attributevalues29 = attributevalues29;
        if (StringUtils.isNotEmpty(this.attributevalues29)) {
        	this.attributevalues29 = this.attributevalues29.trim();
        }
    }
    public String getAttributeDisplay29() {
        return attributeDisplay29;
    }
    /**
     * @param attributeDisplay29 the attributeDisplay29 to set
     */
    public void setAttributeDisplay29(String attributeDisplay29) {
        this.attributeDisplay29 = attributeDisplay29;
        if (StringUtils.isNotEmpty(this.attributeDisplay29)) {
        	this.attributeDisplay29 = this.attributeDisplay29.trim();
        }
    }
    public EntMstAttribute getEntMstAttribute30() {
        return entMstAttribute30;
    }
    public void setEntMstAttribute30(EntMstAttribute entMstAttribute30) {
        this.entMstAttribute30 = entMstAttribute30;
    }
    public int getAttributeCode30() {
        return attributeCode30;
    }
    public void setAttributeCode30(int attributeCode30) {
        this.attributeCode30 = attributeCode30;
    }
    public String getAttributevalues30() {
        return attributevalues30;
    }
    /**
     * @param attributevalues30 the attributevalues30 to set
     */
    public void setAttributevalues30(String attributevalues30) {
        this.attributevalues30 = attributevalues30;
        if (StringUtils.isNotEmpty(this.attributevalues30)) {
        	this.attributevalues30 = this.attributevalues30.trim();
        }
    }
    public String getAttributeDisplay30() {
        return attributeDisplay30;
    }
    /**
     * @param attributeDisplay30 the attributeDisplay30 to set
     */
    public void setAttributeDisplay30(String attributeDisplay30) {
        this.attributeDisplay30 = attributeDisplay30;
        if (StringUtils.isNotEmpty(this.attributeDisplay30)) {
        	this.attributeDisplay30 = this.attributeDisplay30.trim();
        }
    }
    public String getProductImageThumbnailPath1() {
        return productImageThumbnailPath1;
    }
    /**
     * @return productImageThumbnailPath1 without null value
     */
    @JsonIgnore
    public String getProductImageThumbnailPath1NotNull() {
        if (productImageThumbnailPath1 == null) {
            return "";
        }
        return productImageThumbnailPath1;
    }
    /**
     * @return productImageThumbnailPath2 without null value
     */
    @JsonIgnore
    public String getProductImageThumbnailPath2NotNull() {
        if (productImageThumbnailPath2 == null) {
            return "";
        }
        return productImageThumbnailPath2;
    }
    /**
     * @return productImageThumbnailPath3 without null value
     */
    @JsonIgnore
    public String getProductImageThumbnailPath3NotNull() {
        if (productImageThumbnailPath3 == null) {
            return "";
        }
        return productImageThumbnailPath3;
    }
    /**
     * @return productImageThumbnailPath4 without null value
     */
    @JsonIgnore
    public String getProductImageThumbnailPath4NotNull() {
        if (productImageThumbnailPath4 == null) {
            return "";
        }
        return productImageThumbnailPath4;
    }
    /**
     * @return productImageThumbnailPath5 without null value
     */
    @JsonIgnore
    public String getProductImageThumbnailPath5NotNull() {
        if (productImageThumbnailPath5 == null) {
            return "";
        }
        return productImageThumbnailPath5;
    }
    /**
     * @return productImageThumbnailPath6 without null value
     */
    @JsonIgnore
    public String getProductImageThumbnailPath6NotNull() {
        if (productImageThumbnailPath6 == null) {
            return "";
        }
        return productImageThumbnailPath6;
    }
    /**
     * @return productImageThumbnailPath7 without null value
     */
    @JsonIgnore
    public String getProductImageThumbnailPath7NotNull() {
        if (productImageThumbnailPath7 == null) {
            return "";
        }
        return productImageThumbnailPath7;
    }
    /**
     * @return productImageThumbnailPath8 without null value
     */
    @JsonIgnore
    public String getProductImageThumbnailPath8NotNull() {
        if (productImageThumbnailPath8 == null) {
            return "";
        }
        return productImageThumbnailPath8;
    }
    /**
     * @return productImageThumbnailPath9 without null value
     */
    @JsonIgnore
    public String getProductImageThumbnailPath9NotNull() {
        if (productImageThumbnailPath9 == null) {
            return "";
        }
        return productImageThumbnailPath9;
    }
    /**
     * @return productImageThumbnailPath10 without null value
     */
    @JsonIgnore
    public String getProductImageThumbnailPath10NotNull() {
        if (productImageThumbnailPath10 == null) {
            return "";
        }
        return productImageThumbnailPath10;
    }
    /**
     * @param productImageThumbnailPath1 the productImageThumbnailPath1 to set
     */
    public void setProductImageThumbnailPath1(String productImageThumbnailPath1) {
        this.productImageThumbnailPath1 = productImageThumbnailPath1;
        if (StringUtils.isNotEmpty(this.productImageThumbnailPath1)) {
        	this.productImageThumbnailPath1 = this.productImageThumbnailPath1.trim();
        }
    }
    public String getProductImageDetailPath1() {
        return productImageDetailPath1;
    }
    /**
     * @return productImageDetailPath1 without null value
     */
    @JsonIgnore
    public String getProductImageDetailPath1NotNull() {
        if (productImageDetailPath1 == null) {
            return "";
        }
        return productImageDetailPath1;
    }
    /**
     * @return productImageDetailPath2 without null value
     */
    @JsonIgnore
    public String getProductImageDetailPath2NotNull() {
        if (productImageDetailPath2 == null) {
            return "";
        }
        return productImageDetailPath2;
    }
    /**
     * @return productImageDetailPath3 without null value
     */
    @JsonIgnore
    public String getProductImageDetailPath3NotNull() {
        if (productImageDetailPath3 == null) {
            return "";
        }
        return productImageDetailPath3;
    }
    /**
     * @return productImageDetailPath4 without null value
     */
    @JsonIgnore
    public String getProductImageDetailPath4NotNull() {
        if (productImageDetailPath4 == null) {
            return "";
        }
        return productImageDetailPath4;
    }
    /**
     * @return productImageDetailPath5 without null value
     */
    @JsonIgnore
    public String getProductImageDetailPath5NotNull() {
        if (productImageDetailPath5 == null) {
            return "";
        }
        return productImageDetailPath5;
    }
    /**
     * @return productImageDetailPath6 without null value
     */
    @JsonIgnore
    public String getProductImageDetailPath6NotNull() {
        if (productImageDetailPath6 == null) {
            return "";
        }
        return productImageDetailPath6;
    }
    /**
     * @return productImageDetailPath7 without null value
     */
    @JsonIgnore
    public String getProductImageDetailPath7NotNull() {
        if (productImageDetailPath7 == null) {
            return "";
        }
        return productImageDetailPath7;
    }
    /**
     * @return productImageDetailPath8 without null value
     */
    @JsonIgnore
    public String getProductImageDetailPath8NotNull() {
        if (productImageDetailPath8 == null) {
            return "";
        }
        return productImageDetailPath8;
    }
    /**
     * @return productImageDetailPath9 without null value
     */
    @JsonIgnore
    public String getProductImageDetailPath9NotNull() {
        if (productImageDetailPath9 == null) {
            return "";
        }
        return productImageDetailPath9;
    }
    /**
     * @return productImageDetailPath10 without null value
     */
    @JsonIgnore
    public String getProductImageDetailPath10NotNull() {
        if (productImageDetailPath10 == null) {
            return "";
        }
        return productImageDetailPath10;
    }
    /**
     * @param productImageDetailPath1 the productImageDetailPath1 to set
     */
    public void setProductImageDetailPath1(String productImageDetailPath1) {
        this.productImageDetailPath1 = productImageDetailPath1;
        if (StringUtils.isNotEmpty(this.productImageDetailPath1)) {
        	this.productImageDetailPath1 = this.productImageDetailPath1.trim();
        }
    }
    public String getProductImageThumbnailPath2() {
        return productImageThumbnailPath2;
    }
    /**
     * @param productImageThumbnailPath2 the productImageThumbnailPath2 to set
     */
    public void setProductImageThumbnailPath2(String productImageThumbnailPath2) {
        this.productImageThumbnailPath2 = productImageThumbnailPath2;
        if (StringUtils.isNotEmpty(this.productImageThumbnailPath2)) {
        	this.productImageThumbnailPath2 = this.productImageThumbnailPath2.trim();
        }
    }
    public String getProductImageDetailPath2() {
        return productImageDetailPath2;
    }
    /**
     * @param productImageDetailPath2 the productImageDetailPath2 to set
     */
    public void setProductImageDetailPath2(String productImageDetailPath2) {
        this.productImageDetailPath2 = productImageDetailPath2;
        if (StringUtils.isNotEmpty(this.productImageDetailPath2)) {
        	this.productImageDetailPath2 = this.productImageDetailPath2.trim();
        }
    }
    public String getProductImageThumbnailPath3() {
        return productImageThumbnailPath3;
    }
    /**
     * @param productImageThumbnailPath3 the productImageThumbnailPath3 to set
     */
    public void setProductImageThumbnailPath3(String productImageThumbnailPath3) {
        this.productImageThumbnailPath3 = productImageThumbnailPath3;
        if (StringUtils.isNotEmpty(this.productImageThumbnailPath3)) {
        	this.productImageThumbnailPath3 = this.productImageThumbnailPath3.trim();
        }
    }
    public String getProductImageDetailPath3() {
        return productImageDetailPath3;
    }
    /**
     * @param productImageDetailPath3 the productImageDetailPath3 to set
     */
    public void setProductImageDetailPath3(String productImageDetailPath3) {
        this.productImageDetailPath3 = productImageDetailPath3;
        if (StringUtils.isNotEmpty(this.productImageDetailPath3)) {
        	this.productImageDetailPath3 = this.productImageDetailPath3.trim();
        }
    }
    /**
     * @return the productImageThumbnailPath4
     */
    public String getProductImageThumbnailPath4() {
        return productImageThumbnailPath4;
    }
    /**
     * @param productImageThumbnailPath4 the productImageThumbnailPath4 to set
     */
    public void setProductImageThumbnailPath4(String productImageThumbnailPath4) {
        this.productImageThumbnailPath4 = productImageThumbnailPath4;
        if (StringUtils.isNotEmpty(this.productImageThumbnailPath4)) {
        	this.productImageThumbnailPath4 = this.productImageThumbnailPath4.trim();
        }
    }
    /**
     * @return the productImageDetailPath4
     */
    public String getProductImageDetailPath4() {
        return productImageDetailPath4;
    }
    /**
     * @param productImageDetailPath4 the productImageDetailPath4 to set
     */
    public void setProductImageDetailPath4(String productImageDetailPath4) {
        this.productImageDetailPath4 = productImageDetailPath4;
        if (StringUtils.isNotEmpty(this.productImageDetailPath4)) {
        	this.productImageDetailPath4 = this.productImageDetailPath4.trim();
        }
    }
    /**
     * @return the productImageThumbnailPath5
     */
    public String getProductImageThumbnailPath5() {
        return productImageThumbnailPath5;
    }
    /**
     * @param productImageThumbnailPath5 the productImageThumbnailPath5 to set
     */
    public void setProductImageThumbnailPath5(String productImageThumbnailPath5) {
        this.productImageThumbnailPath5 = productImageThumbnailPath5;
        if (StringUtils.isNotEmpty(this.productImageThumbnailPath5)) {
        	this.productImageThumbnailPath5 = this.productImageThumbnailPath5.trim();
        }
    }
    /**
     * @return the productImageDetailPath5
     */
    public String getProductImageDetailPath5() {
        return productImageDetailPath5;
    }
    /**
     * @param productImageDetailPath5 the productImageDetailPath5 to set
     */
    public void setProductImageDetailPath5(String productImageDetailPath5) {
        this.productImageDetailPath5 = productImageDetailPath5;
        if (StringUtils.isNotEmpty(this.productImageDetailPath5)) {
        	this.productImageDetailPath5 = this.productImageDetailPath5.trim();
        }
    }
    /**
     * @return the productImageThumbnailPath6
     */
    public String getProductImageThumbnailPath6() {
        return productImageThumbnailPath6;
    }
    /**
     * @param productImageThumbnailPath6 the productImageThumbnailPath6 to set
     */
    public void setProductImageThumbnailPath6(String productImageThumbnailPath6) {
        this.productImageThumbnailPath6 = productImageThumbnailPath6;
        if (StringUtils.isNotEmpty(this.productImageThumbnailPath6)) {
        	this.productImageThumbnailPath6 = this.productImageThumbnailPath6.trim();
        }
    }
    /**
     * @return the productImageDetailPath6
     */
    public String getProductImageDetailPath6() {
        return productImageDetailPath6;
    }
    /**
     * @param productImageDetailPath6 the productImageDetailPath6 to set
     */
    public void setProductImageDetailPath6(String productImageDetailPath6) {
        this.productImageDetailPath6 = productImageDetailPath6;
        if (StringUtils.isNotEmpty(this.productImageDetailPath6)) {
        	this.productImageDetailPath6 = this.productImageDetailPath6.trim();
        }
    }
    /**
     * @return the productImageThumbnailPath7
     */
    public String getProductImageThumbnailPath7() {
        return productImageThumbnailPath7;
    }
    /**
     * @param productImageThumbnailPath7 the productImageThumbnailPath7 to set
     */
    public void setProductImageThumbnailPath7(String productImageThumbnailPath7) {
        this.productImageThumbnailPath7 = productImageThumbnailPath7;
        if (StringUtils.isNotEmpty(this.productImageThumbnailPath7)) {
        	this.productImageThumbnailPath7 = this.productImageThumbnailPath7.trim();
        }
    }
    /**
     * @return the productImageDetailPath7
     */
    public String getProductImageDetailPath7() {
        return productImageDetailPath7;
    }
    /**
     * @param productImageDetailPath7 the productImageDetailPath7 to set
     */
    public void setProductImageDetailPath7(String productImageDetailPath7) {
        this.productImageDetailPath7 = productImageDetailPath7;
        if (StringUtils.isNotEmpty(this.productImageDetailPath7)) {
        	this.productImageDetailPath7 = this.productImageDetailPath7.trim();
        }
    }
    /**
     * @return the productImageThumbnailPath8
     */
    public String getProductImageThumbnailPath8() {
        return productImageThumbnailPath8;
    }
    /**
     * @param productImageThumbnailPath8 the productImageThumbnailPath8 to set
     */
    public void setProductImageThumbnailPath8(String productImageThumbnailPath8) {
        this.productImageThumbnailPath8 = productImageThumbnailPath8;
        if (StringUtils.isNotEmpty(this.productImageThumbnailPath8)) {
        	this.productImageThumbnailPath8 = this.productImageThumbnailPath8.trim();
        }
    }
    /**
     * @return the productImageDetailPath8
     */
    public String getProductImageDetailPath8() {
        return productImageDetailPath8;
    }
    /**
     * @param productImageDetailPath8 the productImageDetailPath8 to set
     */
    public void setProductImageDetailPath8(String productImageDetailPath8) {
        this.productImageDetailPath8 = productImageDetailPath8;
        if (StringUtils.isNotEmpty(this.productImageDetailPath8)) {
        	this.productImageDetailPath8 = this.productImageDetailPath8.trim();
        }
    }
    /**
     * @return the productImageThumbnailPath9
     */
    public String getProductImageThumbnailPath9() {
        return productImageThumbnailPath9;
    }
    /**
     * @param productImageThumbnailPath9 the productImageThumbnailPath9 to set
     */
    public void setProductImageThumbnailPath9(String productImageThumbnailPath9) {
        this.productImageThumbnailPath9 = productImageThumbnailPath9;
        if (StringUtils.isNotEmpty(this.productImageThumbnailPath9)) {
        	this.productImageThumbnailPath9 = this.productImageThumbnailPath9.trim();
        }
    }
    /**
     * @return the productImageDetailPath9
     */
    public String getProductImageDetailPath9() {
        return productImageDetailPath9;
    }
    /**
     * @param productImageDetailPath9 the productImageDetailPath9 to set
     */
    public void setProductImageDetailPath9(String productImageDetailPath9) {
        this.productImageDetailPath9 = productImageDetailPath9;
        if (StringUtils.isNotEmpty(this.productImageDetailPath9)) {
        	this.productImageDetailPath9 = this.productImageDetailPath9.trim();
        }
    }
    /**
     * @return the productImageThumbnailPath10
     */
    public String getProductImageThumbnailPath10() {
        return productImageThumbnailPath10;
    }
    /**
     * @param productImageThumbnailPath10 the productImageThumbnailPath10 to set
     */
    public void setProductImageThumbnailPath10(String productImageThumbnailPath10) {
        this.productImageThumbnailPath10 = productImageThumbnailPath10;
        if (StringUtils.isNotEmpty(this.productImageThumbnailPath10)) {
        	this.productImageThumbnailPath10 = this.productImageThumbnailPath10.trim();
        }
    }
    /**
     * @return the productImageDetailPath10
     */
    public String getProductImageDetailPath10() {
        return productImageDetailPath10;
    }
    /**
     * @param productImageDetailPath10 the productImageDetailPath10 to set
     */
    public void setProductImageDetailPath10(String productImageDetailPath10) {
        this.productImageDetailPath10 = productImageDetailPath10;
        if (StringUtils.isNotEmpty(this.productImageDetailPath10)) {
        	this.productImageDetailPath10 = this.productImageDetailPath10.trim();
        }
    }
    /**
     * @return the selectCode1
     */
    public int getSelectCode1() {
        return selectCode1;
    }
    /**
     * @return the selectCode1 as String
     */
    @JsonIgnore
    public String getSelectCode1Str() {
        if (selectCode1 == 0) {
            return "";
        }
        return selectCode1 + "";
    }
    /**
     * @param selectCode1 the selectCode1 to set
     */
    public void setSelectCode1(int selectCode1) {
        this.selectCode1 = selectCode1;
    }
    /**
     * @return the selectName1
     */
    public String getSelectName1() {
        return selectName1;
    }
    /**
     * @return the selectName1 without null value
     */
    @JsonIgnore
    public String getSelectName1NotNull() {
        if (selectName1 == null) {
            return "";
        }
        return selectName1;
    }
    /**
     * @param selectName1 the selectName1 to set
     */
    public void setSelectName1(String selectName1) {
        this.selectName1 = selectName1;
        if (StringUtils.isNotEmpty(this.selectName1)) {
        	this.selectName1 = this.selectName1.trim();
        }
    }
    /**
     * @return the selectCode2
     */
    public int getSelectCode2() {
        return selectCode2;
    }
    /**
     * @return the selectCode2 as String
     */
    @JsonIgnore
    public String getSelectCode2Str() {
        if (selectCode2 == 0) {
            return "";
        }
        return selectCode2 + "";
    }
    /**
     * @param selectCode2 the selectCode2 to set
     */
    public void setSelectCode2(int selectCode2) {
        this.selectCode2 = selectCode2;
    }
    /**
     * @return the selectName2
     */
    public String getSelectName2() {
        return selectName2;
    }
    /**
     * @return the selectName2 without null value
     */
    @JsonIgnore
    public String getSelectName2NotNull() {
        if (selectName2 == null) {
            return "";
        }
        return selectName2;
    }
    /**
     * @param selectName2 the selectName2 to set
     */
    public void setSelectName2(String selectName2) {
        this.selectName2 = selectName2;
        if (StringUtils.isNotEmpty(this.selectName2)) {
        	this.selectName2 = this.selectName2.trim();
        }
    }
    /**
     * @return the selectCode3
     */
    public int getSelectCode3() {
        return selectCode3;
    }
    /**
     * @return the selectCode3 as String
     */
    @JsonIgnore
    public String getSelectCode3Str() {
        if (selectCode3 == 0) {
            return "";
        }
        return selectCode3 + "";
    }
    /**
     * @param selectCode3 the selectCode3 to set
     */
    public void setSelectCode3(int selectCode3) {
        this.selectCode3 = selectCode3;
    }
    /**
     * @return the selectName3
     */
    public String getSelectName3() {
        return selectName3;
    }
    /**
     * @return the selectName3 without null value
     */
    @JsonIgnore
    public String getSelectName3NotNull() {
        if (selectName3 == null) {
            return "";
        }
        return selectName3;
    }
    /**
     * @param selectName3 the selectName3 to set
     */
    public void setSelectName3(String selectName3) {
        this.selectName3 = selectName3;
        if (StringUtils.isNotEmpty(this.selectName3)) {
        	this.selectName3 = this.selectName3.trim();
        }
    }
    /**
     * @return the selectCode4
     */
    public int getSelectCode4() {
        return selectCode4;
    }
    /**
     * @return the selectCode4 as String
     */
    @JsonIgnore
    public String getSelectCode4Str() {
        if (selectCode4 == 0) {
            return "";
        }
        return selectCode4 + "";
    }
    /**
     * @param selectCode4 the selectCode4 to set
     */
    public void setSelectCode4(int selectCode4) {
        this.selectCode4 = selectCode4;
    }
    /**
     * @return the selectName4
     */
    public String getSelectName4() {
        return selectName4;
    }
    /**
     * @return the selectName4 without null value
     */
    @JsonIgnore
    public String getSelectName4NotNull() {
        if (selectName4 == null) {
            return "";
        }
        return selectName4;
    }
    /**
     * @param selectName4 the selectName4 to set
     */
    public void setSelectName4(String selectName4) {
        this.selectName4 = selectName4;
        if (StringUtils.isNotEmpty(this.selectName4)) {
        	this.selectName4 = this.selectName4.trim();
        }
    }
    /**
     * @return the selectCode5
     */
    public int getSelectCode5() {
        return selectCode5;
    }
    /**
     * @return the selectCode5 as String
     */
    @JsonIgnore
    public String getSelectCode5Str() {
        if (selectCode5 == 0) {
            return "";
        }
        return selectCode5 + "";
    }
    /**
     * @param selectCode5 the selectCode5 to set
     */
    public void setSelectCode5(int selectCode5) {
        this.selectCode5 = selectCode5;
    }
    /**
     * @return the selectName5
     */
    public String getSelectName5() {
        return selectName5;
    }
    /**
     * @return the selectName5 without null value
     */
    @JsonIgnore
    public String getSelectName5NotNull() {
        if (selectName5 == null) {
            return "";
        }
        return selectName5;
    }
    /**
     * @param selectName5 the selectName5 to set
     */
    public void setSelectName5(String selectName5) {
        this.selectName5 = selectName5;
        if (StringUtils.isNotEmpty(this.selectName5)) {
        	this.selectName5 = this.selectName5.trim();
        }
    }
    /**
     * @return the selectCode6
     */
    public int getSelectCode6() {
        return selectCode6;
    }
    /**
     * @return the selectCode6 as String
     */
    @JsonIgnore
    public String getSelectCode6Str() {
        if (selectCode6 == 0) {
            return "";
        }
        return selectCode6 + "";
    }
    /**
     * @param selectCode6 the selectCode6 to set
     */
    public void setSelectCode6(int selectCode6) {
        this.selectCode6 = selectCode6;
    }
    /**
     * @return the selectName6
     */
    public String getSelectName6() {
        return selectName6;
    }
    /**
     * @return the selectName6 without null value
     */
    @JsonIgnore
    public String getSelectName6NotNull() {
        if (selectName6 == null) {
            return "";
        }
        return selectName6;
    }
    /**
     * @param selectName6 the selectName6 to set
     */
    public void setSelectName6(String selectName6) {
        this.selectName6 = selectName6;
        if (StringUtils.isNotEmpty(this.selectName6)) {
        	this.selectName6 = this.selectName6.trim();
        }
    }
    /**
     * @return the selectCode7
     */
    public int getSelectCode7() {
        return selectCode7;
    }
    /**
     * @return the selectCode7 as String
     */
    @JsonIgnore
    public String getSelectCode7Str() {
        if (selectCode7 == 0) {
            return "";
        }
        return selectCode7 + "";
    }
    /**
     * @param selectCode7 the selectCode7 to set
     */
    public void setSelectCode7(int selectCode7) {
        this.selectCode7 = selectCode7;
    }
    /**
     * @return the selectName7
     */
    public String getSelectName7() {
        return selectName7;
    }
    /**
     * @return the selectName7 without null value
     */
    @JsonIgnore
    public String getSelectName7NotNull() {
        if (selectName7 == null) {
            return "";
        }
        return selectName7;
    }
    /**
     * @param selectName7 the selectName7 to set
     */
    public void setSelectName7(String selectName7) {
        this.selectName7 = selectName7;
        if (StringUtils.isNotEmpty(this.selectName7)) {
        	this.selectName7 = this.selectName7.trim();
        }
    }
    /**
     * @return the selectCode8
     */
    public int getSelectCode8() {
        return selectCode8;
    }
    /**
     * @return the selectCode8 as String
     */
    @JsonIgnore
    public String getSelectCode8Str() {
        if (selectCode8 == 0) {
            return "";
        }
        return selectCode8 + "";
    }
    /**
     * @param selectCode8 the selectCode8 to set
     */
    public void setSelectCode8(int selectCode8) {
        this.selectCode8 = selectCode8;
    }
    /**
     * @return the selectName8
     */
    public String getSelectName8() {
        return selectName8;
    }
    /**
     * @return the selectName8 without null value
     */
    @JsonIgnore
    public String getSelectName8NotNull() {
        if (selectName8 == null) {
            return "";
        }
        return selectName8;
    }
    /**
     * @param selectName8 the selectName8 to set
     */
    public void setSelectName8(String selectName8) {
        this.selectName8 = selectName8;
        if (StringUtils.isNotEmpty(this.selectName8)) {
        	this.selectName8 = this.selectName8.trim();
        }
    }
    /**
     * @return the selectCode9
     */
    public int getSelectCode9() {
        return selectCode9;
    }
    /**
     * @return the selectCode9 as String
     */
    @JsonIgnore
    public String getSelectCode9Str() {
        if (selectCode9 == 0) {
            return "";
        }
        return selectCode9 + "";
    }
    /**
     * @param selectCode9 the selectCode9 to set
     */
    public void setSelectCode9(int selectCode9) {
        this.selectCode9 = selectCode9;
    }
    /**
     * @return the selectName9
     */
    public String getSelectName9() {
        return selectName9;
    }
    /**
     * @return the selectName9 without null value
     */
    @JsonIgnore
    public String getSelectName9NotNull() {
        if (selectName9 == null) {
            return "";
        }
        return selectName9;
    }
    /**
     * @param selectName9 the selectName9 to set
     */
    public void setSelectName9(String selectName9) {
        this.selectName9 = selectName9;
        if (StringUtils.isNotEmpty(this.selectName9)) {
        	this.selectName9 = this.selectName9.trim();
        }
    }
    /**
     * @return the selectCode10
     */
    public int getSelectCode10() {
        return selectCode10;
    }
    /**
     * @return the selectCode10 as String
     */
    @JsonIgnore
    public String getSelectCode10Str() {
        if (selectCode10 == 0) {
            return "";
        }
        return selectCode10 + "";
    }
    /**
     * @param selectCode10 the selectCode10 to set
     */
    public void setSelectCode10(int selectCode10) {
        this.selectCode10 = selectCode10;
    }
    /**
     * @return the selectName10
     */
    public String getSelectName10() {
        return selectName10;
    }
    /**
     * @return the selectName10 without null value
     */
    @JsonIgnore
    public String getSelectName10NotNull() {
        if (selectName10 == null) {
            return "";
        }
        return selectName10;
    }
    /**
     * @param selectName10 the selectName10 to set
     */
    public void setSelectName10(String selectName10) {
        this.selectName10 = selectName10;
        if (StringUtils.isNotEmpty(this.selectName10)) {
        	this.selectName10 = this.selectName10.trim();
        }
    }
    /**
     * @return the productBrandName
     */
    public String getProductBrandName() {
        return productBrandName;
    }
    /**
     * @param productBrandName the productBrandName to set
     */
    public void setProductBrandName(String productBrandName) {
        this.productBrandName = productBrandName;
        if (StringUtils.isNotEmpty(this.productBrandName)) {
        	this.productBrandName = this.productBrandName.trim();
        }
    }
    /**
     * @return the productCategoryName
     */
    public String getProductCategoryName() {
        return productCategoryName;
    }
    /**
     * @param productCategoryName the productCategoryName to set
     */
    public void setProductCategoryName(String productCategoryName) {
        this.productCategoryName = productCategoryName;
        if (StringUtils.isNotEmpty(this.productCategoryName)) {
        	this.productCategoryName = this.productCategoryName.trim();
        }
    }
    /**
     * @return the siireName1
     */
    public String getSiireName1() {
        return siireName1;
    }
    /**
     * @param siireName1 the siireName1 to set
     */
    public void setSiireName1(String siireName1) {
        this.siireName1 = siireName1;
        if (StringUtils.isNotEmpty(this.siireName1)) {
        	this.siireName1 = this.siireName1.trim();
        }
    }
    /**
     * @return the noukiName1
     */
    public String getNoukiName1() {
        return noukiName1;
    }
    /**
     * @param noukiName1 the noukiName1 to set
     */
    public void setNoukiName1(String noukiName1) {
        this.noukiName1 = noukiName1;
        if (StringUtils.isNotEmpty(this.noukiName1)) {
        	this.noukiName1 = this.noukiName1.trim();
        }
    }
    /**
     * @return the siireName2
     */
    public String getSiireName2() {
        return siireName2;
    }
    /**
     * @param siireName2 the siireName2 to set
     */
    public void setSiireName2(String siireName2) {
        this.siireName2 = siireName2;
        if (StringUtils.isNotEmpty(this.siireName2)) {
        	this.siireName2 = this.siireName2.trim();
        }
    }
    /**
     * @return the noukiName2
     */
    public String getNoukiName2() {
        return noukiName2;
    }
    /**
     * @param noukiName2 the noukiName2 to set
     */
    public void setNoukiName2(String noukiName2) {
        this.noukiName2 = noukiName2;
        if (StringUtils.isNotEmpty(this.noukiName2)) {
        	this.noukiName2 = this.noukiName2.trim();
        }
    }
    /**
     * @return the errorMessage
     */
    public String getErrorMessage() {
        return errorMessage;
    }
    /**
     * @param errorMessage the errorMessage to set
     */
    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

//    /************************************************************************
//     * <b>Description:</b><br>
//     *  getter not null if null return "".
//     *
//     * @author      Nguyen.Chuong
//     * @date        Mar 14, 2014
//     * @return      String
//     ************************************************************************/
//    public String getProductGeneralErrorFlgNotNull() {
//        if (null == productGeneralErrorFlg) {
//            return "";
//        }
//        return productGeneralErrorFlg.toString();
//    }
//
//    /************************************************************************
//     * <b>Description:</b><br>
//     *  getter not null if null return "".
//     *
//     * @author      Nguyen.Chuong
//     * @date        Mar 14, 2014
//     * @return      String
//     ************************************************************************/
//    public String getProductCategoryErrorFlgNotNull() {
//        if (null == productCategoryErrorFlg) {
//            return "";
//        }
//        return productCategoryErrorFlg.toString();
//    }
//
//    /************************************************************************
//     * <b>Description:</b><br>
//     *  getter not null if null return "".
//     *
//     * @author      Nguyen.Chuong
//     * @date        Mar 14, 2014
//     * @return      String
//     ************************************************************************/
//    public String getProductModelErrorFlgNotNull() {
//        if (null == productModelErrorFlg) {
//            return "";
//        }
//        return productModelErrorFlg.toString();
//    }
//
//    /************************************************************************
//     * <b>Description:</b><br>
//     *  getter not null if null return "".
//     *
//     * @author      Nguyen.Chuong
//     * @date        Mar 14, 2014
//     * @return      String
//     ************************************************************************/
//    public String getProductAttributeErrorFlgNotNull() {
//        if (null == productAttributeErrorFlg) {
//            return "";
//        }
//        return productAttributeErrorFlg.toString();
//    }

    public String getSortField() {
        return sortField;
    }
    public void setSortField(String sortField) {
        this.sortField = sortField;
    }
    public String getSortDir() {
        return sortDir;
    }
    public void setSortDir(String sortDir) {
        this.sortDir = sortDir;
    }
    public String getImportMode() {
        return importMode;
    }
    /**
     * @param importMode the importMode to set
     */
    public void setImportMode(String importMode) {
        this.importMode = importMode;
        if (StringUtils.isNotEmpty(this.importMode)) {
        	this.importMode = this.importMode.trim();
        }
    }
    /**
     * @return the productGroupCode
     */
    public Long getProductGroupCode() {
        return productGroupCode;
    }
    /************************************************************************
     * <b>Description:</b><br>
     *  getter not null if null return "".
     *
     * @author      Nguyen.Chuong
     * @date        Mar 14, 2014
     * @return      String
     ************************************************************************/
    @JsonIgnore
    public String getProductGroupCodeNotNull() {
        if (null == productGroupCode) {
            return "";
        }
        return productGroupCode.toString();
    }
    /**
     * @param productGroupCode the productGroupCode to set
     */
    public void setProductGroupCode(Long productGroupCode) {
        this.productGroupCode = productGroupCode;
    }
    /**
     * @return the productId
     */
    public Long getProductId() {
        return productId;
    }
    /**
     * @param productId the productId to set
     */
    public void setProductId(Long productId) {
        this.productId = productId;
    }
	/**
	 * @return the sProductId
	 */
	public String getsProductId() {
		return sProductId;
	}
	/**
	 * @param sProductId the sProductId to set
	 */
	public void setsProductId(String sProductId) {
		this.sProductId = sProductId;
		if (StringUtils.isNotEmpty(this.sProductId)) {
        	this.sProductId = this.sProductId.trim();
        }
	}
	/**
	 * @return the sProductSyouhinSysCode
	 */
	public String getsProductSyouhinSysCode() {
		return sProductSyouhinSysCode;
	}
	/**
	 * @param sProductSyouhinSysCode the sProductSyouhinSysCode to set
	 */
	public void setsProductSyouhinSysCode(String sProductSyouhinSysCode) {
		this.sProductSyouhinSysCode = sProductSyouhinSysCode;
		if (StringUtils.isNotEmpty(this.sProductSyouhinSysCode)) {
        	this.sProductSyouhinSysCode = this.sProductSyouhinSysCode.trim();
        }
	}
	/**
	 * @return the sProductBrandCode
	 */
	public String getsProductBrandCode() {
		return sProductBrandCode;
	}
	/**
	 * @param sProductBrandCode the sProductBrandCode to set
	 */
	public void setsProductBrandCode(String sProductBrandCode) {
		this.sProductBrandCode = sProductBrandCode;
		if (StringUtils.isNotEmpty(this.sProductBrandCode)) {
        	this.sProductBrandCode = this.sProductBrandCode.trim();
        }
	}
	/**
	 * @return the sProductCategoryCode
	 */
	public String getsProductCategoryCode() {
		return sProductCategoryCode;
	}
	/**
	 * @param sProductCategoryCode the sProductCategoryCode to set
	 */
	public void setsProductCategoryCode(String sProductCategoryCode) {
		this.sProductCategoryCode = sProductCategoryCode;
		if (StringUtils.isNotEmpty(this.sProductCategoryCode)) {
        	this.sProductCategoryCode = this.sProductCategoryCode.trim();
        }
	}
	/**
	 * @return the sProductWebikeCodeFlg
	 */
	public String getsProductWebikeCodeFlg() {
		return sProductWebikeCodeFlg;
	}
	/**
	 * @param sProductWebikeCodeFlg the sProductWebikeCodeFlg to set
	 */
	public void setsProductWebikeCodeFlg(String sProductWebikeCodeFlg) {
		this.sProductWebikeCodeFlg = sProductWebikeCodeFlg;
		if (StringUtils.isNotEmpty(this.sProductWebikeCodeFlg)) {
        	this.sProductWebikeCodeFlg = this.sProductWebikeCodeFlg.trim();
        }
	}
	/**
	 * @return the sProductProperPrice
	 */
	public String getsProductProperPrice() {
		return sProductProperPrice;
	}
	/**
	 * @param sProductProperPrice the sProductProperPrice to set
	 */
	public void setsProductProperPrice(String sProductProperPrice) {
		this.sProductProperPrice = sProductProperPrice;
		if (StringUtils.isNotEmpty(this.sProductProperPrice)) {
        	this.sProductProperPrice = this.sProductProperPrice.trim();
        }
	}
	/**
	 * @return the sSupplierPricePrice
	 */
	public String getsSupplierPricePrice() {
		return sSupplierPricePrice;
	}
	/**
	 * @param sSupplierPricePrice the sSupplierPricePrice to set
	 */
	public void setsSupplierPricePrice(String sSupplierPricePrice) {
		this.sSupplierPricePrice = sSupplierPricePrice;
		if (StringUtils.isNotEmpty(this.sSupplierPricePrice)) {
        	this.sSupplierPricePrice = this.sSupplierPricePrice.trim();
        }
	}
	/**
	 * @return the sProductGroupCode
	 */
	public String getsProductGroupCode() {
		return sProductGroupCode;
	}
	/**
	 * @param sProductGroupCode the sProductGroupCode to set
	 */
	public void setsProductGroupCode(String sProductGroupCode) {
		this.sProductGroupCode = sProductGroupCode;
		if (StringUtils.isNotEmpty(this.sProductGroupCode)) {
        	this.sProductGroupCode = this.sProductGroupCode.trim();
        }
	}
	/**
	 * @return the sProductOpenPriceFlg
	 */
	public String getsProductOpenPriceFlg() {
		return sProductOpenPriceFlg;
	}
	/**
	 * @param sProductOpenPriceFlg the sProductOpenPriceFlg to set
	 */
	public void setsProductOpenPriceFlg(String sProductOpenPriceFlg) {
		this.sProductOpenPriceFlg = sProductOpenPriceFlg;
		if (StringUtils.isNotEmpty(this.sProductOpenPriceFlg)) {
        	this.sProductOpenPriceFlg = this.sProductOpenPriceFlg.trim();
        }
	}
	/**
	 * @return the sProductProperSellingFlg
	 */
	public String getsProductProperSellingFlg() {
		return sProductProperSellingFlg;
	}
	/**
	 * @param sProductProperSellingFlg the sProductProperSellingFlg to set
	 */
	public void setsProductProperSellingFlg(String sProductProperSellingFlg) {
		this.sProductProperSellingFlg = sProductProperSellingFlg;
		if (StringUtils.isNotEmpty(this.sProductProperSellingFlg)) {
        	this.sProductProperSellingFlg = this.sProductProperSellingFlg.trim();
        }
	}
	/**
	 * @return the sProductOrderProductFlg
	 */
	public String getsProductOrderProductFlg() {
		return sProductOrderProductFlg;
	}
	/**
	 * @param sProductOrderProductFlg the sProductOrderProductFlg to set
	 */
	public void setsProductOrderProductFlg(String sProductOrderProductFlg) {
		this.sProductOrderProductFlg = sProductOrderProductFlg;
		if (StringUtils.isNotEmpty(this.sProductOrderProductFlg)) {
        	this.sProductOrderProductFlg = this.sProductOrderProductFlg.trim();
        }
	}
	/**
	 * @return the sProductNoReturnableFlg
	 */
	public String getsProductNoReturnableFlg() {
		return sProductNoReturnableFlg;
	}
	/**
	 * @param sProductNoReturnableFlg the sProductNoReturnableFlg to set
	 */
	public void setsProductNoReturnableFlg(String sProductNoReturnableFlg) {
		this.sProductNoReturnableFlg = sProductNoReturnableFlg;
		if (StringUtils.isNotEmpty(this.sProductNoReturnableFlg)) {
        	this.sProductNoReturnableFlg = this.sProductNoReturnableFlg.trim();
        }
	}
	/**
	 * @return the sProductAmbiguousImageFlg
	 */
	public String getsProductAmbiguousImageFlg() {
		return sProductAmbiguousImageFlg;
	}
	/**
	 * @param sProductAmbiguousImageFlg the sProductAmbiguousImageFlg to set
	 */
	public void setsProductAmbiguousImageFlg(String sProductAmbiguousImageFlg) {
		this.sProductAmbiguousImageFlg = sProductAmbiguousImageFlg;
		if (StringUtils.isNotEmpty(this.sProductAmbiguousImageFlg)) {
        	this.sProductAmbiguousImageFlg = this.sProductAmbiguousImageFlg.trim();
        }
	}
	/**
	 * @return the sSelectCode1
	 */
	public String getsSelectCode1() {
		return sSelectCode1;
	}
	/**
	 * @param sSelectCode1 the sSelectCode1 to set
	 */
	public void setsSelectCode1(String sSelectCode1) {
		this.sSelectCode1 = sSelectCode1;
		if (StringUtils.isNotEmpty(this.sSelectCode1)) {
        	this.sSelectCode1 = this.sSelectCode1.trim();
        }
	}
	/**
	 * @return the sSelectCode2
	 */
	public String getsSelectCode2() {
		return sSelectCode2;
	}
	/**
	 * @param sSelectCode2 the sSelectCode2 to set
	 */
	public void setsSelectCode2(String sSelectCode2) {
		this.sSelectCode2 = sSelectCode2;
		if (StringUtils.isNotEmpty(this.sSelectCode2)) {
        	this.sSelectCode2 = this.sSelectCode2.trim();
        }
	}
	/**
	 * @return the sSelectCode3
	 */
	public String getsSelectCode3() {
		return sSelectCode3;
	}
	/**
	 * @param sSelectCode3 the sSelectCode3 to set
	 */
	public void setsSelectCode3(String sSelectCode3) {
		this.sSelectCode3 = sSelectCode3;
		if (StringUtils.isNotEmpty(this.sSelectCode3)) {
        	this.sSelectCode3 = this.sSelectCode3.trim();
        }
	}
	/**
	 * @return the sSelectCode4
	 */
	public String getsSelectCode4() {
		return sSelectCode4;
	}
	/**
	 * @param sSelectCode4 the sSelectCode4 to set
	 */
	public void setsSelectCode4(String sSelectCode4) {
		this.sSelectCode4 = sSelectCode4;
		if (StringUtils.isNotEmpty(this.sSelectCode4)) {
        	this.sSelectCode4 = this.sSelectCode4.trim();
        }
	}
	/**
	 * @return the sSelectCode5
	 */
	public String getsSelectCode5() {
		return sSelectCode5;
	}
	/**
	 * @param sSelectCode5 the sSelectCode5 to set
	 */
	public void setsSelectCode5(String sSelectCode5) {
		this.sSelectCode5 = sSelectCode5;
		if (StringUtils.isNotEmpty(this.sSelectCode5)) {
        	this.sSelectCode5 = this.sSelectCode5.trim();
        }
	}
	/**
	 * @return the sSelectCode6
	 */
	public String getsSelectCode6() {
		return sSelectCode6;
	}
	/**
	 * @param sSelectCode6 the sSelectCode6 to set
	 */
	public void setsSelectCode6(String sSelectCode6) {
		this.sSelectCode6 = sSelectCode6;
		if (StringUtils.isNotEmpty(this.sSelectCode6)) {
        	this.sSelectCode6 = this.sSelectCode6.trim();
        }
	}
	/**
	 * @return the sSelectCode7
	 */
	public String getsSelectCode7() {
		return sSelectCode7;
	}
	/**
	 * @param sSelectCode7 the sSelectCode7 to set
	 */
	public void setsSelectCode7(String sSelectCode7) {
		this.sSelectCode7 = sSelectCode7;
		if (StringUtils.isNotEmpty(this.sSelectCode7)) {
        	this.sSelectCode7 = this.sSelectCode7.trim();
        }
	}
	/**
	 * @return the sSelectCode8
	 */
	public String getsSelectCode8() {
		return sSelectCode8;
	}
	/**
	 * @param sSelectCode8 the sSelectCode8 to set
	 */
	public void setsSelectCode8(String sSelectCode8) {
		this.sSelectCode8 = sSelectCode8;
		if (StringUtils.isNotEmpty(this.sSelectCode8)) {
        	this.sSelectCode8 = this.sSelectCode8.trim();
        }
	}
	/**
	 * @return the sSelectCode9
	 */
	public String getsSelectCode9() {
		return sSelectCode9;
	}
	/**
	 * @param sSelectCode9 the sSelectCode9 to set
	 */
	public void setsSelectCode9(String sSelectCode9) {
		this.sSelectCode9 = sSelectCode9;
		if (StringUtils.isNotEmpty(this.sSelectCode9)) {
        	this.sSelectCode9 = this.sSelectCode9.trim();
        }
	}
	/**
	 * @return the sSelectCode10
	 */
	public String getsSelectCode10() {
		return sSelectCode10;
	}
	/**
	 * @param sSelectCode10 the sSelectCode10 to set
	 */
	public void setsSelectCode10(String sSelectCode10) {
		this.sSelectCode10 = sSelectCode10;
		if (StringUtils.isNotEmpty(this.sSelectCode10)) {
        	this.sSelectCode10 = this.sSelectCode10.trim();
        }
	}
    /**
     * @return the productGeneralErrorFlg
     */
    public boolean isProductGeneralErrorFlg() {
        return productGeneralErrorFlg;
    }
    /**
     * @param productGeneralErrorFlg the productGeneralErrorFlg to set
     */
    public void setProductGeneralErrorFlg(boolean productGeneralErrorFlg) {
        this.productGeneralErrorFlg = productGeneralErrorFlg;
    }
    /**
     * @return the productCategoryErrorFlg
     */
    public boolean isProductCategoryErrorFlg() {
        return productCategoryErrorFlg;
    }
    /**
     * @param productCategoryErrorFlg the productCategoryErrorFlg to set
     */
    public void setProductCategoryErrorFlg(boolean productCategoryErrorFlg) {
        this.productCategoryErrorFlg = productCategoryErrorFlg;
    }
    /**
     * @return the productModelErrorFlg
     */
    public boolean isProductModelErrorFlg() {
        return productModelErrorFlg;
    }
    /**
     * @param productModelErrorFlg the productModelErrorFlg to set
     */
    public void setProductModelErrorFlg(boolean productModelErrorFlg) {
        this.productModelErrorFlg = productModelErrorFlg;
    }
    /**
     * @return the productAttributeErrorFlg
     */
    public boolean isProductAttributeErrorFlg() {
        return productAttributeErrorFlg;
    }
    /**
     * @param productAttributeErrorFlg the productAttributeErrorFlg to set
     */
    public void setProductAttributeErrorFlg(boolean productAttributeErrorFlg) {
        this.productAttributeErrorFlg = productAttributeErrorFlg;
    }
	public String getImportModeError() {
		return importModeError;
	}
	public void setImportModeError(String importModeError) {
		this.importModeError = importModeError;
	}
	public String getProductIdError() {
		return productIdError;
	}
	public void setProductIdError(String productIdError) {
		this.productIdError = productIdError;
	}
	public String getProductSyouhinSysCodeError() {
		return productSyouhinSysCodeError;
	}
	public void setProductSyouhinSysCodeError(String productSyouhinSysCodeError) {
		this.productSyouhinSysCodeError = productSyouhinSysCodeError;
	}
	public String getProductBrandCodeError() {
		return productBrandCodeError;
	}
	public void setProductBrandCodeError(String productBrandCodeError) {
		this.productBrandCodeError = productBrandCodeError;
	}
	public String getProductCategoryCodeError() {
		return productCategoryCodeError;
	}
	public void setProductCategoryCodeError(String productCategoryCodeError) {
		this.productCategoryCodeError = productCategoryCodeError;
	}
	public String getProductNameError() {
		return productNameError;
	}
	public void setProductNameError(String productNameError) {
		this.productNameError = productNameError;
	}
	public String getProductCodeError() {
		return productCodeError;
	}
	public void setProductCodeError(String productCodeError) {
		this.productCodeError = productCodeError;
	}
	public String getProductWebikeCodeFlgError() {
		return productWebikeCodeFlgError;
	}
	public void setProductWebikeCodeFlgError(String productWebikeCodeFlgError) {
		this.productWebikeCodeFlgError = productWebikeCodeFlgError;
	}
	public String getProductEanCodeError() {
		return productEanCodeError;
	}
	public void setProductEanCodeError(String productEanCodeError) {
		this.productEanCodeError = productEanCodeError;
	}
	public String getProductProperPriceError() {
		return productProperPriceError;
	}
	public void setProductProperPriceError(String productProperPriceError) {
		this.productProperPriceError = productProperPriceError;
	}
	public String getSupplierPricePriceError() {
		return supplierPricePriceError;
	}
	public void setSupplierPricePriceError(String supplierPricePriceError) {
		this.supplierPricePriceError = supplierPricePriceError;
	}
	public String getProductGroupCodeError() {
		return productGroupCodeError;
	}
	public void setProductGroupCodeError(String productGroupCodeError) {
		this.productGroupCodeError = productGroupCodeError;
	}
	public String getProductSupplierReleaseDateError() {
		return productSupplierReleaseDateError;
	}
	public void setProductSupplierReleaseDateError(
			String productSupplierReleaseDateError) {
		this.productSupplierReleaseDateError = productSupplierReleaseDateError;
	}
	public String getProductOpenPriceFlgError() {
		return productOpenPriceFlgError;
	}
	public void setProductOpenPriceFlgError(String productOpenPriceFlgError) {
		this.productOpenPriceFlgError = productOpenPriceFlgError;
	}
	public String getProductProperSellingFlgError() {
		return productProperSellingFlgError;
	}
	public void setProductProperSellingFlgError(String productProperSellingFlgError) {
		this.productProperSellingFlgError = productProperSellingFlgError;
	}
	public String getProductOrderProductFlgError() {
		return productOrderProductFlgError;
	}
	public void setProductOrderProductFlgError(String productOrderProductFlgError) {
		this.productOrderProductFlgError = productOrderProductFlgError;
	}
	public String getProductNoReturnableFlgError() {
		return productNoReturnableFlgError;
	}
	public void setProductNoReturnableFlgError(String productNoReturnableFlgError) {
		this.productNoReturnableFlgError = productNoReturnableFlgError;
	}
	public String getProductAmbiguousImageFlgError() {
		return productAmbiguousImageFlgError;
	}
	public void setProductAmbiguousImageFlgError(
			String productAmbiguousImageFlgError) {
		this.productAmbiguousImageFlgError = productAmbiguousImageFlgError;
	}
	public String getDescriptionSummaryError() {
		return descriptionSummaryError;
	}
	public void setDescriptionSummaryError(String descriptionSummaryError) {
		this.descriptionSummaryError = descriptionSummaryError;
	}
	public String getDescriptionRemarksError() {
		return descriptionRemarksError;
	}
	public void setDescriptionRemarksError(String descriptionRemarksError) {
		this.descriptionRemarksError = descriptionRemarksError;
	}
	public String getDescriptionCautionError() {
		return descriptionCautionError;
	}
	public void setDescriptionCautionError(String descriptionCautionError) {
		this.descriptionCautionError = descriptionCautionError;
	}
	public String getDescriptionSentenceError() {
		return descriptionSentenceError;
	}
	public void setDescriptionSentenceError(String descriptionSentenceError) {
		this.descriptionSentenceError = descriptionSentenceError;
	}
	public String getSiireCode1Error() {
		return siireCode1Error;
	}
	public void setSiireCode1Error(String siireCode1Error) {
		this.siireCode1Error = siireCode1Error;
	}
	public String getNoukiCode1Error() {
		return noukiCode1Error;
	}
	public void setNoukiCode1Error(String noukiCode1Error) {
		this.noukiCode1Error = noukiCode1Error;
	}
	public String getSiireCode2Error() {
		return siireCode2Error;
	}
	public void setSiireCode2Error(String siireCode2Error) {
		this.siireCode2Error = siireCode2Error;
	}
	public String getNoukiCode2Error() {
		return noukiCode2Error;
	}
	public void setNoukiCode2Error(String noukiCode2Error) {
		this.noukiCode2Error = noukiCode2Error;
	}
	public String getCompatibleModelError() {
		return compatibleModelError;
	}
	public void setCompatibleModelError(String compatibleModelError) {
		this.compatibleModelError = compatibleModelError;
	}
	public String getProductImageThumbnailPath1Error() {
		return productImageThumbnailPath1Error;
	}
	public void setProductImageThumbnailPath1Error(
			String productImageThumbnailPath1Error) {
		this.productImageThumbnailPath1Error = productImageThumbnailPath1Error;
	}
	public String getProductImageDetailPath1Error() {
		return productImageDetailPath1Error;
	}
	public void setProductImageDetailPath1Error(String productImageDetailPath1Error) {
		this.productImageDetailPath1Error = productImageDetailPath1Error;
	}
	public String getProductImageThumbnailPath2Error() {
		return productImageThumbnailPath2Error;
	}
	public void setProductImageThumbnailPath2Error(
			String productImageThumbnailPath2Error) {
		this.productImageThumbnailPath2Error = productImageThumbnailPath2Error;
	}
	public String getProductImageDetailPath2Error() {
		return productImageDetailPath2Error;
	}
	public void setProductImageDetailPath2Error(String productImageDetailPath2Error) {
		this.productImageDetailPath2Error = productImageDetailPath2Error;
	}
	public String getProductImageThumbnailPath3Error() {
		return productImageThumbnailPath3Error;
	}
	public void setProductImageThumbnailPath3Error(
			String productImageThumbnailPath3Error) {
		this.productImageThumbnailPath3Error = productImageThumbnailPath3Error;
	}
	public String getProductImageDetailPath3Error() {
		return productImageDetailPath3Error;
	}
	public void setProductImageDetailPath3Error(String productImageDetailPath3Error) {
		this.productImageDetailPath3Error = productImageDetailPath3Error;
	}
	public String getProductImageThumbnailPath4Error() {
		return productImageThumbnailPath4Error;
	}
	public void setProductImageThumbnailPath4Error(
			String productImageThumbnailPath4Error) {
		this.productImageThumbnailPath4Error = productImageThumbnailPath4Error;
	}
	public String getProductImageDetailPath4Error() {
		return productImageDetailPath4Error;
	}
	public void setProductImageDetailPath4Error(String productImageDetailPath4Error) {
		this.productImageDetailPath4Error = productImageDetailPath4Error;
	}
	public String getProductImageThumbnailPath5Error() {
		return productImageThumbnailPath5Error;
	}
	public void setProductImageThumbnailPath5Error(
			String productImageThumbnailPath5Error) {
		this.productImageThumbnailPath5Error = productImageThumbnailPath5Error;
	}
	public String getProductImageDetailPath5Error() {
		return productImageDetailPath5Error;
	}
	public void setProductImageDetailPath5Error(String productImageDetailPath5Error) {
		this.productImageDetailPath5Error = productImageDetailPath5Error;
	}
	public String getProductImageThumbnailPath6Error() {
		return productImageThumbnailPath6Error;
	}
	public void setProductImageThumbnailPath6Error(
			String productImageThumbnailPath6Error) {
		this.productImageThumbnailPath6Error = productImageThumbnailPath6Error;
	}
	public String getProductImageDetailPath6Error() {
		return productImageDetailPath6Error;
	}
	public void setProductImageDetailPath6Error(String productImageDetailPath6Error) {
		this.productImageDetailPath6Error = productImageDetailPath6Error;
	}
	public String getProductImageThumbnailPath7Error() {
		return productImageThumbnailPath7Error;
	}
	public void setProductImageThumbnailPath7Error(
			String productImageThumbnailPath7Error) {
		this.productImageThumbnailPath7Error = productImageThumbnailPath7Error;
	}
	public String getProductImageDetailPath7Error() {
		return productImageDetailPath7Error;
	}
	public void setProductImageDetailPath7Error(String productImageDetailPath7Error) {
		this.productImageDetailPath7Error = productImageDetailPath7Error;
	}
	public String getProductImageThumbnailPath8Error() {
		return productImageThumbnailPath8Error;
	}
	public void setProductImageThumbnailPath8Error(
			String productImageThumbnailPath8Error) {
		this.productImageThumbnailPath8Error = productImageThumbnailPath8Error;
	}
	public String getProductImageDetailPath8Error() {
		return productImageDetailPath8Error;
	}
	public void setProductImageDetailPath8Error(String productImageDetailPath8Error) {
		this.productImageDetailPath8Error = productImageDetailPath8Error;
	}
	public String getProductImageThumbnailPath9Error() {
		return productImageThumbnailPath9Error;
	}
	public void setProductImageThumbnailPath9Error(
			String productImageThumbnailPath9Error) {
		this.productImageThumbnailPath9Error = productImageThumbnailPath9Error;
	}
	public String getProductImageDetailPath9Error() {
		return productImageDetailPath9Error;
	}
	public void setProductImageDetailPath9Error(String productImageDetailPath9Error) {
		this.productImageDetailPath9Error = productImageDetailPath9Error;
	}
	public String getProductImageThumbnailPath10Error() {
		return productImageThumbnailPath10Error;
	}
	public void setProductImageThumbnailPath10Error(
			String productImageThumbnailPath10Error) {
		this.productImageThumbnailPath10Error = productImageThumbnailPath10Error;
	}
	public String getProductImageDetailPath10Error() {
		return productImageDetailPath10Error;
	}
	public void setProductImageDetailPath10Error(
			String productImageDetailPath10Error) {
		this.productImageDetailPath10Error = productImageDetailPath10Error;
	}
	public String getSelectCode1Error() {
		return selectCode1Error;
	}
	public void setSelectCode1Error(String selectCode1Error) {
		this.selectCode1Error = selectCode1Error;
	}
	public String getSelectName1Error() {
		return selectName1Error;
	}
	public void setSelectName1Error(String selectName1Error) {
		this.selectName1Error = selectName1Error;
	}
	public String getSelectCode2Error() {
		return selectCode2Error;
	}
	public void setSelectCode2Error(String selectCode2Error) {
		this.selectCode2Error = selectCode2Error;
	}
	public String getSelectName2Error() {
		return selectName2Error;
	}
	public void setSelectName2Error(String selectName2Error) {
		this.selectName2Error = selectName2Error;
	}
	public String getSelectCode3Error() {
		return selectCode3Error;
	}
	public void setSelectCode3Error(String selectCode3Error) {
		this.selectCode3Error = selectCode3Error;
	}
	public String getSelectName3Error() {
		return selectName3Error;
	}
	public void setSelectName3Error(String selectName3Error) {
		this.selectName3Error = selectName3Error;
	}
	public String getSelectCode4Error() {
		return selectCode4Error;
	}
	public void setSelectCode4Error(String selectCode4Error) {
		this.selectCode4Error = selectCode4Error;
	}
	public String getSelectName4Error() {
		return selectName4Error;
	}
	public void setSelectName4Error(String selectName4Error) {
		this.selectName4Error = selectName4Error;
	}
	public String getSelectCode5Error() {
		return selectCode5Error;
	}
	public void setSelectCode5Error(String selectCode5Error) {
		this.selectCode5Error = selectCode5Error;
	}
	public String getSelectName5Error() {
		return selectName5Error;
	}
	public void setSelectName5Error(String selectName5Error) {
		this.selectName5Error = selectName5Error;
	}
	public String getSelectCode6Error() {
		return selectCode6Error;
	}
	public void setSelectCode6Error(String selectCode6Error) {
		this.selectCode6Error = selectCode6Error;
	}
	public String getSelectName6Error() {
		return selectName6Error;
	}
	public void setSelectName6Error(String selectName6Error) {
		this.selectName6Error = selectName6Error;
	}
	public String getSelectCode7Error() {
		return selectCode7Error;
	}
	public void setSelectCode7Error(String selectCode7Error) {
		this.selectCode7Error = selectCode7Error;
	}
	public String getSelectName7Error() {
		return selectName7Error;
	}
	public void setSelectName7Error(String selectName7Error) {
		this.selectName7Error = selectName7Error;
	}
	public String getSelectCode8Error() {
		return selectCode8Error;
	}
	public void setSelectCode8Error(String selectCode8Error) {
		this.selectCode8Error = selectCode8Error;
	}
	public String getSelectName8Error() {
		return selectName8Error;
	}
	public void setSelectName8Error(String selectName8Error) {
		this.selectName8Error = selectName8Error;
	}
	public String getSelectCode9Error() {
		return selectCode9Error;
	}
	public void setSelectCode9Error(String selectCode9Error) {
		this.selectCode9Error = selectCode9Error;
	}
	public String getSelectName9Error() {
		return selectName9Error;
	}
	public void setSelectName9Error(String selectName9Error) {
		this.selectName9Error = selectName9Error;
	}
	public String getSelectCode10Error() {
		return selectCode10Error;
	}
	public void setSelectCode10Error(String selectCode10Error) {
		this.selectCode10Error = selectCode10Error;
	}
	public String getSelectName10Error() {
		return selectName10Error;
	}
	public void setSelectName10Error(String selectName10Error) {
		this.selectName10Error = selectName10Error;
	}
	public String getCustomersConfirmationItemError() {
		return customersConfirmationItemError;
	}
	public void setCustomersConfirmationItemError(
			String customersConfirmationItemError) {
		this.customersConfirmationItemError = customersConfirmationItemError;
	}
	public String getLinkError() {
		return linkError;
	}
	public void setLinkError(String linkError) {
		this.linkError = linkError;
	}
	public String getAnimationError() {
		return animationError;
	}
	public void setAnimationError(String animationError) {
		this.animationError = animationError;
	}
    /**
     * @return the errorAllMess
     */
    public String getErrorAllMess() {
        return errorAllMess;
    }
    /**
     * @param errorAllMess the errorAllMess to set
     */
    public void setErrorAllMess(String errorAllMess) {
        this.errorAllMess = errorAllMess;
    }

}
