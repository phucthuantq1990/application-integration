/************************************************************************
 * File Name    ： EntCSVModel.java
 * Author       ： Phan.tinh
 * Version      ： 1.0.0
 * Date Created ： 2014/08/20
 * Date Updated ： 2014/08/20
 * Description  ： Entity for CSV import/Export
 ***********************************************************************/
package net.webike.RcProductFactory.entity;

import java.util.List;
import java.util.Map;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
/**
 * Entity for CSV import/Export.
 */
@Component
@Scope(value = "prototype")
public class EntCSVTranfer {
   private EntCSVMode1 entTmpMode1;
   private List<String> listCSVHeaderMode1;
   private List<EntMstSiire> siireList;
   private List<EntMstNouki> noukiList;
   private List<EntMstSyouhinSelect> selectList;
   private List<EntMstBrand> brandList;
   private List<EntMstCategory> categoryList;
   private List<EntMstLinkReason> linkMstList;
   private List<EntMstMaker> listMaker;
   private List<EntMstModel> listModel;
   private List<EntCSVMode1> listModel1;
   private int indexOfProduct;
   private Long maxNumbering;
   private int countNewProduct;
   private List<EntCSVMode1> listTmpCSVMode1;
   private List<EntTblFactoryProductNew> entProductNewList;
   private List<String> productIdReleased;
   private List<String> selectedCodeList;
   private Map<Integer, List<String>> hashMapOption;
   private boolean status = true;
	public EntCSVMode1 getEntTmpMode1() {
		return entTmpMode1;
	}
	public void setEntTmpMode1(EntCSVMode1 entTmpMode1) {
		this.entTmpMode1 = entTmpMode1;
	}
	public List<String> getListCSVHeaderMode1() {
		return listCSVHeaderMode1;
	}
	public void setListCSVHeaderMode1(List<String> listCSVHeaderMode1) {
		this.listCSVHeaderMode1 = listCSVHeaderMode1;
	}
	public List<EntMstSiire> getSiireList() {
		return siireList;
	}
	public void setSiireList(List<EntMstSiire> siireList) {
		this.siireList = siireList;
	}
	public List<EntMstNouki> getNoukiList() {
		return noukiList;
	}
	public void setNoukiList(List<EntMstNouki> noukiList) {
		this.noukiList = noukiList;
	}
	public List<EntMstSyouhinSelect> getSelectList() {
		return selectList;
	}
	public void setSelectList(List<EntMstSyouhinSelect> selectList) {
		this.selectList = selectList;
	}
	public List<EntMstBrand> getBrandList() {
		return brandList;
	}
	public void setBrandList(List<EntMstBrand> brandList) {
		this.brandList = brandList;
	}
	public List<EntMstCategory> getCategoryList() {
		return categoryList;
	}
	public void setCategoryList(List<EntMstCategory> categoryList) {
		this.categoryList = categoryList;
	}
	public List<EntMstLinkReason> getLinkMstList() {
		return linkMstList;
	}
	public void setLinkMstList(List<EntMstLinkReason> linkMstList) {
		this.linkMstList = linkMstList;
	}
	public List<EntMstMaker> getListMaker() {
		return listMaker;
	}
	public void setListMaker(List<EntMstMaker> listMaker) {
		this.listMaker = listMaker;
	}
	public List<EntMstModel> getListModel() {
		return listModel;
	}
	public void setListModel(List<EntMstModel> listModel) {
		this.listModel = listModel;
	}
	public int getIndexOfProduct() {
		return indexOfProduct;
	}
	public void setIndexOfProduct(int indexOfProduct) {
		this.indexOfProduct = indexOfProduct;
	}
	public Long getMaxNumbering() {
		return maxNumbering;
	}
	public void setMaxNumbering(Long maxNumbering) {
		this.maxNumbering = maxNumbering;
	}
	public int getCountNewProduct() {
		return countNewProduct;
	}
	public void setCountNewProduct(int countNewProduct) {
		this.countNewProduct = countNewProduct;
	}
	public List<EntCSVMode1> getListTmpCSVMode1() {
		return listTmpCSVMode1;
	}
	public void setListTmpCSVMode1(List<EntCSVMode1> listTmpCSVMode1) {
		this.listTmpCSVMode1 = listTmpCSVMode1;
	}
	public List<EntTblFactoryProductNew> getEntProductNewList() {
		return entProductNewList;
	}
	public void setEntProductNewList(List<EntTblFactoryProductNew> entProductNewList) {
		this.entProductNewList = entProductNewList;
	}
	public List<String> getProductIdReleased() {
		return productIdReleased;
	}
	public void setProductIdReleased(List<String> productIdReleased) {
		this.productIdReleased = productIdReleased;
	}
	public List<EntCSVMode1> getListModel1() {
		return listModel1;
	}
	public void setListModel1(List<EntCSVMode1> listModel1) {
		this.listModel1 = listModel1;
	}
	public List<String> getSelectedCodeList() {
		return selectedCodeList;
	}
	public void setSelectedCodeList(List<String> selectedCodeList) {
		this.selectedCodeList = selectedCodeList;
	}
	public Map<Integer, List<String>> getHashMapOption() {
		return hashMapOption;
	}
	public void setHashMapOption(Map<Integer, List<String>> hashMapOption) {
		this.hashMapOption = hashMapOption;
	}
	public boolean isStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	   
   
}
