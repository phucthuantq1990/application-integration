/************************************************************************
 * file name	： EntMstSyouhinAll.java
 * author		： Tran.Thanh
 * version		： 1.0.0
 * date created	： 20 Feb 2014
 * date updated	： 20 Feb 2014
 * description	： Entity for database webikesh.mst_syouhin_all
 *
 ***********************************************************************/
package net.webike.RcProductFactory.entity;

import java.util.List;

/**
 * Entity for database webikesh.mst_syouhin_all.
 */
public class EntMstSyouhinSearch {
	private String syouhinMaker = "";

	private String syouhinName = "";

	private String syouhinCode = "";

	private String syouhinThumbnailUrl = "";

	private String syouhinTeika = "";

	private String syouhinOp1 = "";

	private String syouhinOp2 = "";

	private String syouhinOp3 = "";

	private String syouhinBunruiCode = "";

	private List<Integer> syouhinBunruiList = null;

	private String rowId = "";

	private String crudFlg = "";

	/**
	 * @return the syouhinMaker
	 */
	public String getSyouhinMaker() {
		return syouhinMaker;
	}

	/**
	 * @param syouhinMaker the syouhinMaker to set
	 */
	public void setSyouhinMaker(String syouhinMaker) {
		this.syouhinMaker = syouhinMaker;
	}

	/**
	 * @return the syouhinName
	 */
	public String getSyouhinName() {
		return syouhinName;
	}

	/**
	 * @param syouhinName the syouhinName to set
	 */
	public void setSyouhinName(String syouhinName) {
		this.syouhinName = syouhinName;
	}

	/**
	 * @return the syouhinCode
	 */
	public String getSyouhinCode() {
		return syouhinCode;
	}

	/**
	 * @param syouhinCode the syouhinCode to set
	 */
	public void setSyouhinCode(String syouhinCode) {
		this.syouhinCode = syouhinCode;
	}

	/**
	 * @return the syouhinThumbnailUrl
	 */
	public String getSyouhinThumbnailUrl() {
		return syouhinThumbnailUrl;
	}

	/**
	 * @param syouhinThumbnailUrl the syouhinThumbnailUrl to set
	 */
	public void setSyouhinThumbnailUrl(String syouhinThumbnailUrl) {
		this.syouhinThumbnailUrl = syouhinThumbnailUrl;
	}

	/**
	 * @return the syouhinTeika
	 */
	public String getSyouhinTeika() {
		return syouhinTeika;
	}

	/**
	 * @param syouhinTeika the syouhinTeika to set
	 */
	public void setSyouhinTeika(String syouhinTeika) {
		this.syouhinTeika = syouhinTeika;
	}

	/**
	 * @return the syouhinOp1
	 */
	public String getSyouhinOp1() {
		return syouhinOp1;
	}

	/**
	 * @param syouhinOp1 the syouhinOp1 to set
	 */
	public void setSyouhinOp1(String syouhinOp1) {
		this.syouhinOp1 = syouhinOp1;
	}

	/**
	 * @return the syouhinOp2
	 */
	public String getSyouhinOp2() {
		return syouhinOp2;
	}

	/**
	 * @param syouhinOp2 the syouhinOp2 to set
	 */
	public void setSyouhinOp2(String syouhinOp2) {
		this.syouhinOp2 = syouhinOp2;
	}

	/**
	 * @return the syouhinOp3
	 */
	public String getSyouhinOp3() {
		return syouhinOp3;
	}

	/**
	 * @param syouhinOp3 the syouhinOp3 to set
	 */
	public void setSyouhinOp3(String syouhinOp3) {
		this.syouhinOp3 = syouhinOp3;
	}

	/**
	 * @return the syouhinBunruiCode
	 */
	public String getSyouhinBunruiCode() {
		return syouhinBunruiCode;
	}

	/**
	 * @param syouhinBunruiCode the syouhinBunruiCode to set
	 */
	public void setSyouhinBunruiCode(String syouhinBunruiCode) {
		this.syouhinBunruiCode = syouhinBunruiCode;
	}

	/**
	 * @return the syouhinBunruiList
	 */
	public List<Integer> getSyouhinBunruiList() {
		return syouhinBunruiList;
	}

	/**
	 * @param syouhinBunruiList the syouhinBunruiList to set
	 */
	public void setSyouhinBunruiList(List<Integer> syouhinBunruiList) {
		this.syouhinBunruiList = syouhinBunruiList;
	}

	/**
	 * @return the rowId
	 */
	public String getRowId() {
		return rowId;
	}

	/**
	 * @param rowId the rowId to set
	 */
	public void setRowId(String rowId) {
		this.rowId = rowId;
	}

	/**
	 * @return the crudFlg
	 */
	public String getCrudFlg() {
		return crudFlg;
	}

	/**
	 * @param crudFlg the crudFlg to set
	 */
	public void setCrudFlg(String crudFlg) {
		this.crudFlg = crudFlg;
	}

}
