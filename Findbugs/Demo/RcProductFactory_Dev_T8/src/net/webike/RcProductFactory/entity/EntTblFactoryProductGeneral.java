/************************************************************************
 * file name	： EntTblFactoryProductGeneral.java
 * author		： Le.Dinh
 * version		： 1.0.0
 * date created	： 2014/02/12
 * date updated	： 2014/02/12
 * description	： Entity EntTblFactoryProductGeneral.
 *
 ***********************************************************************/
package net.webike.RcProductFactory.entity;

import java.util.Date;


/**
 * EntMstProductSupplierStatus.
 */
public class EntTblFactoryProductGeneral {

	private Long productId;
	private String productSupplierReleaseDate = "";
	private Integer productSupplierStatusCode;
	private Long productGroupCode;
	private String productGroupname = "";
	private Boolean productDelFlg;
	private Long productMatterNo;
	private Date updatedOn;
	private Integer productCategoryCode;
	private Integer productProperPrice;
	private Long supplierPricePrice;


	private Integer productBrandCode;
	private Integer productWebikeCodeFlg;
	private String  productName = "";
	private String  productCode  = "";
	private String  productEanCode = "";
	private String  updatedUserId = "";

	private String productDescriptionSummary = "";
	private String productDescriptionCaution = "";

	private String productBrandName = "";

	private Integer productOpenPriceFlg;
	private Integer productProperSellingFlg;
	private Integer productOrderProductFlg;
	private Integer productNoReturnableFlg;
	private Integer productAmbiguousImageFlg;
	private String descriptionRemarks;

	private String rowId = "";
	private String crudFlg = "";

	//BOE #7363 Tran.Thanh : add replaceMode for productName, janPopupBlock15, listPrice
	private Integer replaceModeProductName;

	private String productNameFind;

	private Integer replaceModeProductEanCode;

	private String productEanCodeFind;

	private Integer replaceModeProductProperPrice;

	private String productProperPriceFind;

	private String productProperPriceStr;

	private Integer replaceModeProductCode;

	private String productCodeFind;

	private Integer replaceModeProductReleaseDate;

	private String productSupplierReleaseDateFind;

	private String productCategoryName = "";

	private String productSyouhinSysCode = "";
	
	//EOE #7363 Tran.Thanh : add replaceMode for productName, janPopupBlock15, listPrice
	private boolean isValidProductCode = false; //true= valid,false = invalid
	    
	private boolean isValidJanCode = false; //true= valid,false = invalid
    /**
     * @return the updatedOn
     */
    public Date getUpdatedOn() {
        if (this.updatedOn != null) {
            return (Date) updatedOn.clone();
        }
        return null;
    }
    /**
     * @param updatedOn the updatedOn to set
     */
    public void setUpdatedOn(Date updatedOn) {
        if (updatedOn != null) {
            this.updatedOn = (Date) updatedOn.clone();
        } else {
            this.updatedOn = null;
        }
    }
	/**
	 * @return the productProperPriceStr
	 */
	public String getProductProperPriceStr() {
		return productProperPriceStr;
	}
	/**
	 * @param productProperPriceStr the productProperPriceStr to set
	 */
	public void setProductProperPriceStr(String productProperPriceStr) {
		this.productProperPriceStr = productProperPriceStr;
	}
	/**
	 * @return the productBrandCode
	 */
	public Integer getProductBrandCode() {
		return productBrandCode;
	}
	/**
	 * @param productBrandCode the productBrandCode to set
	 */
	public void setProductBrandCode(Integer productBrandCode) {
		this.productBrandCode = productBrandCode;
	}
	/**
	 * @return the productWebikeCodeFlg
	 */
	public Integer getProductWebikeCodeFlg() {
		return productWebikeCodeFlg;
	}
	/**
	 * @param productWebikeCodeFlg the productWebikeCodeFlg to set
	 */
	public void setProductWebikeCodeFlg(Integer productWebikeCodeFlg) {
		this.productWebikeCodeFlg = productWebikeCodeFlg;
	}
	/**
	 * @return the productName
	 */
	public String getProductName() {
		return productName;
	}
	/**
	 * @param productName the productName to set
	 */
	public void setProductName(String productName) {
		this.productName = productName;
	}
	/**
	 * @return the productCode
	 */
	public String getProductCode() {
		return productCode;
	}
	/**
	 * @param productCode the productCode to set
	 */
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	/**
	 * @return the productDescriptionSummary
	 */
	public String getProductDescriptionSummary() {
		return productDescriptionSummary;
	}
	/**
	 * @param productDescriptionSummary the productDescriptionSummary to set
	 */
	public void setProductDescriptionSummary(String productDescriptionSummary) {
		this.productDescriptionSummary = productDescriptionSummary;
	}
	/**
	 * @return the productDescriptionCaution
	 */
	public String getProductDescriptionCaution() {
		return productDescriptionCaution;
	}
	/**
	 * @param productDescriptionCaution the productDescriptionCaution to set
	 */
	public void setProductDescriptionCaution(String productDescriptionCaution) {
		this.productDescriptionCaution = productDescriptionCaution;
	}
	/**
	 * @return the productBrandName
	 */
	public String getProductBrandName() {
		return productBrandName;
	}
	/**
	 * @param productBrandName the productBrandName to set
	 */
	public void setProductBrandName(String productBrandName) {
		this.productBrandName = productBrandName;
	}
	/**
	 * @return the productOpenPriceFlg
	 */
	public Integer getProductOpenPriceFlg() {
		return productOpenPriceFlg;
	}
	/**
	 * @param productOpenPriceFlg the productOpenPriceFlg to set
	 */
	public void setProductOpenPriceFlg(Integer productOpenPriceFlg) {
		this.productOpenPriceFlg = productOpenPriceFlg;
	}
	/**
	 * @return the productProperSellingFlg
	 */
	public Integer getProductProperSellingFlg() {
		return productProperSellingFlg;
	}
	/**
	 * @param productProperSellingFlg the productProperSellingFlg to set
	 */
	public void setProductProperSellingFlg(Integer productProperSellingFlg) {
		this.productProperSellingFlg = productProperSellingFlg;
	}
	/**
	 * @return the productOrderProductFlg
	 */
	public Integer getProductOrderProductFlg() {
		return productOrderProductFlg;
	}
	/**
	 * @param productOrderProductFlg the productOrderProductFlg to set
	 */
	public void setProductOrderProductFlg(Integer productOrderProductFlg) {
		this.productOrderProductFlg = productOrderProductFlg;
	}
	/**
	 * @return the productNoReturnableFlg
	 */
	public Integer getProductNoReturnableFlg() {
		return productNoReturnableFlg;
	}
	/**
	 * @param productNoReturnableFlg the productNoReturnableFlg to set
	 */
	public void setProductNoReturnableFlg(Integer productNoReturnableFlg) {
		this.productNoReturnableFlg = productNoReturnableFlg;
	}
	/**
	 * @return the productAmbiguousImageFlg
	 */
	public Integer getProductAmbiguousImageFlg() {
		return productAmbiguousImageFlg;
	}
	/**
	 * @param productAmbiguousImageFlg the productAmbiguousImageFlg to set
	 */
	public void setProductAmbiguousImageFlg(Integer productAmbiguousImageFlg) {
		this.productAmbiguousImageFlg = productAmbiguousImageFlg;
	}
	/**
	 * @return the productId
	 */
	public Long getProductId() {
		return productId;
	}
	/**
	 * @param productId the productId to set
	 */
	public void setProductId(Long productId) {
		this.productId = productId;
	}
	/**
	 * @return the productSupplierStatusCode
	 */
	public Integer getProductSupplierStatusCode() {
		return productSupplierStatusCode;
	}
	/**
	 * @param productSupplierStatusCode the productSupplierStatusCode to set
	 */
	public void setProductSupplierStatusCode(Integer productSupplierStatusCode) {
		this.productSupplierStatusCode = productSupplierStatusCode;
	}
	/**
	 * @return the productSupplierReleaseDate
	 */
	public String getProductSupplierReleaseDate() {
		return productSupplierReleaseDate;
	}
	/**
	 * @param productSupplierReleaseDate the productSupplierReleaseDate to set
	 */
	public void setProductSupplierReleaseDate(String productSupplierReleaseDate) {
		this.productSupplierReleaseDate = productSupplierReleaseDate;
	}

    /**
     * @return the productGroupCode
     */
    public Long getProductGroupCode() {
        return productGroupCode;
    }
    /**
     * @param productGroupCode the productGroupCode to set
     */
    public void setProductGroupCode(Long productGroupCode) {
        this.productGroupCode = productGroupCode;
    }
    /**
     * @return the productDelFlg
     */
    public Boolean getProductDelFlg() {
        return productDelFlg;
    }
    /**
     * @param productDelFlg the productDelFlg to set
     */
    public void setProductDelFlg(Boolean productDelFlg) {
        this.productDelFlg = productDelFlg;
    }
    /**
     * @return the productMatterNo
     */
    public Long getProductMatterNo() {
        return productMatterNo;
    }
    /**
     * @param productMatterNo the productMatterNo to set
     */
    public void setProductMatterNo(Long productMatterNo) {
        this.productMatterNo = productMatterNo;
    }
	/**
	 * @return the productEanCode
	 */
	public String getProductEanCode() {
		return productEanCode;
	}
	/**
	 * @param productEanCode the productEanCode to set
	 */
	public void setProductEanCode(String productEanCode) {
		this.productEanCode = productEanCode;
	}
	/**
	 * @return the rowId
	 */
	public String getRowId() {
		return rowId;
	}
	/**
	 * @param rowId the rowId to set
	 */
	public void setRowId(String rowId) {
		this.rowId = rowId;
	}
	/**
	 * @return the crudFlg
	 */
	public String getCrudFlg() {
		return crudFlg;
	}
	/**
	 * @param crudFlg the crudFlg to set
	 */
	public void setCrudFlg(String crudFlg) {
		this.crudFlg = crudFlg;
	}
	/**
	 * @return the updatedUserId
	 */
	public String getUpdatedUserId() {
		return updatedUserId;
	}
	/**
	 * @param updatedUserId the updatedUserId to set
	 */
	public void setUpdatedUserId(String updatedUserId) {
		this.updatedUserId = updatedUserId;
	}
	/**
	 * @return the productCategoryCode
	 */
	public Integer getProductCategoryCode() {
		return productCategoryCode;
	}
	/**
	 * @param productCategoryCode the productCategoryCode to set
	 */
	public void setProductCategoryCode(Integer productCategoryCode) {
		this.productCategoryCode = productCategoryCode;
	}
	/**
	 * @return the productProperPrice
	 */
	public Integer getProductProperPrice() {
		return productProperPrice;
	}
	/**
	 * @param productProperPrice the productProperPrice to set
	 */
	public void setProductProperPrice(Integer productProperPrice) {
		this.productProperPrice = productProperPrice;
	}
	/**
	 * @return the productGroupname
	 */
	public String getProductGroupname() {
		return productGroupname;
	}
	/**
	 * @param productGroupname the productGroupname to set
	 */
	public void setProductGroupname(String productGroupname) {
		this.productGroupname = productGroupname;
	}
    /**
     * @return the supplierPricePrice
     */
    public Long getSupplierPricePrice() {
        return supplierPricePrice;
    }
    /**
     * @param supplierPricePrice the supplierPricePrice to set
     */
    public void setSupplierPricePrice(Long supplierPricePrice) {
        this.supplierPricePrice = supplierPricePrice;
    }
	/**
	 * @return the descriptionRemarks
	 */
	public String getDescriptionRemarks() {
		return descriptionRemarks;
	}
	/**
	 * @param descriptionRemarks the descriptionRemarks to set
	 */
	public void setDescriptionRemarks(String descriptionRemarks) {
		this.descriptionRemarks = descriptionRemarks;
	}
	/**
	 * @return the replaceModeProductName
	 */
	public Integer getReplaceModeProductName() {
		return replaceModeProductName;
	}
	/**
	 * @param replaceModeProductName the replaceModeProductName to set
	 */
	public void setReplaceModeProductName(Integer replaceModeProductName) {
		this.replaceModeProductName = replaceModeProductName;
	}
	/**
	 * @return the replaceModeProductEanCode
	 */
	public Integer getReplaceModeProductEanCode() {
		return replaceModeProductEanCode;
	}
	/**
	 * @param replaceModeProductEanCode the replaceModeProductEanCode to set
	 */
	public void setReplaceModeProductEanCode(Integer replaceModeProductEanCode) {
		this.replaceModeProductEanCode = replaceModeProductEanCode;
	}
	/**
	 * @return the replaceModeProductProperPrice
	 */
	public Integer getReplaceModeProductProperPrice() {
		return replaceModeProductProperPrice;
	}
	/**
	 * @param replaceModeProductProperPrice the replaceModeProductProperPrice to set
	 */
	public void setReplaceModeProductProperPrice(
			Integer replaceModeProductProperPrice) {
		this.replaceModeProductProperPrice = replaceModeProductProperPrice;
	}
	/**
	 * @return the productNameFind
	 */
	public String getProductNameFind() {
		return productNameFind;
	}
	/**
	 * @param productNameFind the productNameFind to set
	 */
	public void setProductNameFind(String productNameFind) {
		this.productNameFind = productNameFind;
	}
	/**
	 * @return the productEanCodeFind
	 */
	public String getProductEanCodeFind() {
		return productEanCodeFind;
	}
	/**
	 * @param productEanCodeFind the productEanCodeFind to set
	 */
	public void setProductEanCodeFind(String productEanCodeFind) {
		this.productEanCodeFind = productEanCodeFind;
	}
	/**
	 * @return the productProperPriceFind
	 */
	public String getProductProperPriceFind() {
		return productProperPriceFind;
	}
	/**
	 * @param productProperPriceFind the productProperPriceFind to set
	 */
	public void setProductProperPriceFind(String productProperPriceFind) {
		this.productProperPriceFind = productProperPriceFind;
	}

	/**
	 * @return the replaceModeProductCode
	 */
	public Integer getReplaceModeProductCode() {
		return replaceModeProductCode;
	}
	/**
	 * @param replaceModeProductCode the replaceModeProductCode to set
	 */
	public void setReplaceModeProductCode(Integer replaceModeProductCode) {
		this.replaceModeProductCode = replaceModeProductCode;
	}
	/**
	 * @return the productCodeFind
	 */
	public String getProductCodeFind() {
		return productCodeFind;
	}
	/**
	 * @param productCodeFind the productCodeFind to set
	 */
	public void setProductCodeFind(String productCodeFind) {
		this.productCodeFind = productCodeFind;
	}
	/**
	 * @return the replaceModeProductReleaseDate
	 */
	public Integer getReplaceModeProductReleaseDate() {
		return replaceModeProductReleaseDate;
	}
	/**
	 * @param replaceModeProductReleaseDate the replaceModeProductReleaseDate to set
	 */
	public void setReplaceModeProductReleaseDate(
			Integer replaceModeProductReleaseDate) {
		this.replaceModeProductReleaseDate = replaceModeProductReleaseDate;
	}
	/**
	 * @return the productSupplierReleaseDateFind
	 */
	public String getProductSupplierReleaseDateFind() {
		return productSupplierReleaseDateFind;
	}
	/**
	 * @param productSupplierReleaseDateFind the productSupplierReleaseDateFind to set
	 */
	public void setProductSupplierReleaseDateFind(
			String productSupplierReleaseDateFind) {
		this.productSupplierReleaseDateFind = productSupplierReleaseDateFind;
	}
	/**
	 * @return the productCategoryName
	 */
	public String getProductCategoryName() {
		return productCategoryName;
	}
	/**
	 * @param productCategoryName the productCategoryName to set
	 */
	public void setProductCategoryName(String productCategoryName) {
		this.productCategoryName = productCategoryName;
	}
	/**
	 * @return the productSyouhinSysCode
	 */
	public String getProductSyouhinSysCode() {
		return productSyouhinSysCode;
	}
	/**
	 * @param productSyouhinSysCode the productSyouhinSysCode to set
	 */
	public void setProductSyouhinSysCode(String productSyouhinSysCode) {
		this.productSyouhinSysCode = productSyouhinSysCode;
	}
	/**
	 * @return the isValidProductCode
	 */
	public boolean isValidProductCode() {
		return isValidProductCode;
	}
	/**
	 * @param isValidProductCode the isValidProductCode to set
	 */
	public void setValidProductCode(boolean isValidProductCode) {
		this.isValidProductCode = isValidProductCode;
	}
	/**
	 * @return the isValidJanCode
	 */
	public boolean isValidJanCode() {
		return isValidJanCode;
	}
	/**
	 * @param isValidJanCode the isValidJanCode to set
	 */
	public void setValidJanCode(boolean isValidJanCode) {
		this.isValidJanCode = isValidJanCode;
	}

}
