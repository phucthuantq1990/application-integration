/************************************************************************
 * file name	： EntCategory.java
 * author		： Tran.Thanh
 * version		： 1.0.0
 * date created	： 25 Oct 2013
 * date updated	： 25 Oct 2013
 * description	： Date of TreeView Category.
 *
 ***********************************************************************/
package net.webike.RcProductFactory.entity;

/**
 * EntCategory.
 */
public class EntCategory {

	private String level1Code = "";

	private String level2Code = "";

	private String level3Code = "";

	private String level4Code = "";

	private String level5Code = "";

	private String level1Name = "";

	private String level2Name = "";

	private String level3Name = "";

	private String level4Name = "";

	private String level5Name = "";

	/**
	 * @return the level1Code
	 */
	public String getLevel1Code() {
		return level1Code;
	}

	/**
	 * @param level1Code the level1Code to set
	 */
	public void setLevel1Code(String level1Code) {
		this.level1Code = level1Code;
	}

	/**
	 * @return the level2Code
	 */
	public String getLevel2Code() {
		return level2Code;
	}

	/**
	 * @param level2Code the level2Code to set
	 */
	public void setLevel2Code(String level2Code) {
		this.level2Code = level2Code;
	}

	/**
	 * @return the level3Code
	 */
	public String getLevel3Code() {
		return level3Code;
	}

	/**
	 * @param level3Code the level3Code to set
	 */
	public void setLevel3Code(String level3Code) {
		this.level3Code = level3Code;
	}

	/**
	 * @return the level4Code
	 */
	public String getLevel4Code() {
		return level4Code;
	}

	/**
	 * @param level4Code the level4Code to set
	 */
	public void setLevel4Code(String level4Code) {
		this.level4Code = level4Code;
	}

	/**
	 * @return the level5Code
	 */
	public String getLevel5Code() {
		return level5Code;
	}

	/**
	 * @param level5Code the level5Code to set
	 */
	public void setLevel5Code(String level5Code) {
		this.level5Code = level5Code;
	}

	/**
	 * @return the level1Name
	 */
	public String getLevel1Name() {
		return level1Name;
	}

	/**
	 * @param level1Name the level1Name to set
	 */
	public void setLevel1Name(String level1Name) {
		this.level1Name = level1Name;
	}

	/**
	 * @return the level2Name
	 */
	public String getLevel2Name() {
		return level2Name;
	}

	/**
	 * @param level2Name the level2Name to set
	 */
	public void setLevel2Name(String level2Name) {
		this.level2Name = level2Name;
	}

	/**
	 * @return the level3Name
	 */
	public String getLevel3Name() {
		return level3Name;
	}

	/**
	 * @param level3Name the level3Name to set
	 */
	public void setLevel3Name(String level3Name) {
		this.level3Name = level3Name;
	}

	/**
	 * @return the level4Name
	 */
	public String getLevel4Name() {
		return level4Name;
	}

	/**
	 * @param level4Name the level4Name to set
	 */
	public void setLevel4Name(String level4Name) {
		this.level4Name = level4Name;
	}

	/**
	 * @return the level5Name
	 */
	public String getLevel5Name() {
		return level5Name;
	}

	/**
	 * @param level5Name the level5Name to set
	 */
	public void setLevel5Name(String level5Name) {
		this.level5Name = level5Name;
	}

}
