/************************************************************************
 * file name	： EntTblProductVideo.java
 * author		： Nguyen.Chuong
 * version		： 1.0.0
 * date created	： 2014/02/17
 * date updated	： 2014/02/17
 * description	： Entity EntTblProductVideo to map with rc_syouhin.tbl_product_video.
 *
 ***********************************************************************/
package net.webike.RcProductFactory.entity;

/**
 * EntTblProductVideo.
 */
public class EntTblProductVideo {
    private long syouhinSysCode;
    private String productVideoTitle;
    private String productVideoURL;
    private String productVideoDescription;

    /**
     * @return the syouhinSysCode
     */
    public long getSyouhinSysCode() {
        return syouhinSysCode;
    }
    /**
     * @param syouhinSysCode the syouhinSysCode to set
     */
    public void setSyouhinSysCode(long syouhinSysCode) {
        this.syouhinSysCode = syouhinSysCode;
    }
    /**
     * @return the productVideoTitle
     */
    public String getProductVideoTitle() {
        return productVideoTitle;
    }
    /**
     * @param productVideoTitle the productVideoTitle to set
     */
    public void setProductVideoTitle(String productVideoTitle) {
        this.productVideoTitle = productVideoTitle;
    }
    /**
     * @return the productVideoURL
     */
    public String getProductVideoURL() {
        return productVideoURL;
    }
    /**
     * @param productVideoURL the productVideoURL to set
     */
    public void setProductVideoURL(String productVideoURL) {
        this.productVideoURL = productVideoURL;
    }
    /**
     * @return the productVideoDescription
     */
    public String getProductVideoDescription() {
        return productVideoDescription;
    }
    /**
     * @param productVideoDescription the productVideoDescription to set
     */
    public void setProductVideoDescription(String productVideoDescription) {
        this.productVideoDescription = productVideoDescription;
    }

}
