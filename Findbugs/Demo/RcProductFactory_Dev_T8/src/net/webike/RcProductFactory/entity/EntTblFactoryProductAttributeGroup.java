
package net.webike.RcProductFactory.entity;


/**
 * Entity of EntTblFactoryProductAttributeGroup.
 * @author Doan Chuong
 * Date Created �F 2014/03/05
 */
public class EntTblFactoryProductAttributeGroup {
    private Long productId;
    private Integer attributeCode;
    private String attributeGroupDisplay;
    private Integer attributeGroupCode;
    private Integer attributeGroupSort;
    private String updatedOn;
    private String updatedUserId;
    /**
     * @return the productId
     */
    public Long getProductId() {
        return productId;
    }
    /**
     * @param productId the productId to set
     */
    public void setProductId(Long productId) {
        this.productId = productId;
    }
    /**
     * @return the attributeCode
     */
    public Integer getAttributeCode() {
        return attributeCode;
    }
    /**
     * @param attributeCode the attributeCode to set
     */
    public void setAttributeCode(Integer attributeCode) {
        this.attributeCode = attributeCode;
    }
    /**
     * @return the attributeGroupDisplay
     */
    public String getAttributeGroupDisplay() {
        return attributeGroupDisplay;
    }
    /**
     * @param attributeGroupDisplay the attributeGroupDisplay to set
     */
    public void setAttributeGroupDisplay(String attributeGroupDisplay) {
        this.attributeGroupDisplay = attributeGroupDisplay;
    }
    /**
     * @return the attributeGroupCode
     */
    public Integer getAttributeGroupCode() {
        return attributeGroupCode;
    }
    /**
     * @param attributeGroupCode the attributeGroupCode to set
     */
    public void setAttributeGroupCode(Integer attributeGroupCode) {
        this.attributeGroupCode = attributeGroupCode;
    }
    /**
     * @return the attributeGroupSort
     */
    public Integer getAttributeGroupSort() {
        return attributeGroupSort;
    }
    /**
     * @param attributeGroupSort the attributeGroupSort to set
     */
    public void setAttributeGroupSort(Integer attributeGroupSort) {
        this.attributeGroupSort = attributeGroupSort;
    }
    /**
     * @return the updatedOn
     */
    public String getUpdatedOn() {
        return updatedOn;
    }
    /**
     * @param updatedOn the updatedOn to set
     */
    public void setUpdatedOn(String updatedOn) {
        this.updatedOn = updatedOn;
    }
    /**
     * @return the updatedUserId
     */
    public String getUpdatedUserId() {
        return updatedUserId;
    }
    /**
     * @param updatedUserId the updatedUserId to set
     */
    public void setUpdatedUserId(String updatedUserId) {
        this.updatedUserId = updatedUserId;
    }
}
