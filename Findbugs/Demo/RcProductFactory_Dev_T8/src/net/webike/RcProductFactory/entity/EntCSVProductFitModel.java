/************************************************************************
 * file name    ： EntCSVProductFitModel.java
 * author       ： hoang.ho
 * version      ： 1.0.0
 * date created ： 2014/02/26
 * date updated ： 2014/02/26
 * description  ： Entity for product tbl_factory_product using by CSV.
 *
 ***********************************************************************/
package net.webike.RcProductFactory.entity;

import java.math.BigInteger;

/**
 * Entity used for export csv mode 2.
 * @author  hoang.ho
 * @since   2014/02/26
 */
public class EntCSVProductFitModel {
    /** importMode. */
    private String importMode;
    /** productId. */
    private BigInteger productId;
    /** productSyouhinSysCode. */
    private BigInteger productSyouhinSysCode;
    /** fitModelMaker. */
    private String fitModelMaker;
    /** fitModelModel. */
    private String fitModelModel;
    /** fitModelStyle. */
    private String fitModelStyle;
    /** productIdListStr.*/
    private String productIdListStr;
    /** sortField.*/
    private String sortField;
    /** sortDir.*/
    private String sortDir;
    /**
     * @return the productId
     */
    public BigInteger getProductId() {
        return productId;
    }
    /**
     * @param productId the productId to set
     */
    public void setProductId(BigInteger productId) {
        this.productId = productId;
    }
    /**
     * @return the productSyouhinSysCode
     */
    public BigInteger getProductSyouhinSysCode() {
        return productSyouhinSysCode;
    }
    /**
     * @param productSyouhinSysCode the productSyouhinSysCode to set
     */
    public void setProductSyouhinSysCode(BigInteger productSyouhinSysCode) {
        this.productSyouhinSysCode = productSyouhinSysCode;
    }
    /**
     * @return the fitModelMaker
     */
    public String getFitModelMaker() {
        return fitModelMaker;
    }
    /**
     * @param fitModelMaker the fitModelMaker to set
     */
    public void setFitModelMaker(String fitModelMaker) {
        this.fitModelMaker = fitModelMaker;
    }
    /**
     * @return the fitModelModel
     */
    public String getFitModelModel() {
        return fitModelModel;
    }
    /**
     * @param fitModelModel the fitModelModel to set
     */
    public void setFitModelModel(String fitModelModel) {
        this.fitModelModel = fitModelModel;
    }
    /**
     * @return the fitModelStyle
     */
    public String getFitModelStyle() {
        return fitModelStyle;
    }
    /**
     * @param fitModelStyle the fitModelStyle to set
     */
    public void setFitModelStyle(String fitModelStyle) {
        this.fitModelStyle = fitModelStyle;
    }
    /**
     * @return the productIdListStr
     */
    public String getProductIdListStr() {
        return productIdListStr;
    }
    /**
     * @param productIdListStr the productIdListStr to set
     */
    public void setProductIdListStr(String productIdListStr) {
        this.productIdListStr = productIdListStr;
    }
    /**
     * @return the sortField
     */
    public String getSortField() {
        return sortField;
    }
    /**
     * @param sortField the sortField to set
     */
    public void setSortField(String sortField) {
        this.sortField = sortField;
    }
    /**
     * @return the sortDir
     */
    public String getSortDir() {
        return sortDir;
    }
    /**
     * @param sortDir the sortDir to set
     */
    public void setSortDir(String sortDir) {
        this.sortDir = sortDir;
    }
    /**
     * @return the importMode
     */
    public String getImportMode() {
        return importMode;
    }
    /**
     * @param importMode the importMode to set
     */
    public void setImportMode(String importMode) {
        this.importMode = importMode;
    }
}
