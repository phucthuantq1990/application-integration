/************************************************************************
 * File Name    ： EntTblFactoryProductSupplier.java
 * Author       ：Le.Dinh
 * Version      ： 1.0.0
 * Date Created ： 2014/02/12
 * Date Updated ： 2014/02/12
 * Description  ： Entity of EntTblFactoryProductSupplier.
 ***********************************************************************/
package net.webike.RcProductFactory.entity;

import java.util.Date;

/**
 * Entity of EntTblFactoryProductFitModel.
 */
public class EntTblFactoryProductSupplier {

	private String siireCode;
	private Long productId;
	private Date updatedOn;
	private String noukiCode;
	private String updatedUserId;
	private Integer supplierDelFlg;
	private Integer supplierSyouhinSiireOrder;
	private Integer supplierTyokusouFlg;

	// reference properties
	private String siireName;
	private String noukiName1;

	private String idRow = "";
	private String crudFlg = "";
	private Long syouhinSysCode;

	/**
	 * @return the siireCode
	 */
	public String getSiireCode() {
		return siireCode;
	}
	/**
	 * @param siireCode the siireCode to set
	 */
	public void setSiireCode(String siireCode) {
		this.siireCode = siireCode;
	}
	/**
	 * @return the productId
	 */
	public Long getProductId() {
		return productId;
	}
	/**
	 * @param productId the productId to set
	 */
	public void setProductId(Long productId) {
		this.productId = productId;
	}
	/**
	 * @return the updatedOn
	 */
	public Date getUpdatedOn() {
    	if (this.updatedOn != null) {
            return (Date) this.updatedOn.clone();
        }
        return null;
	}
	/**
	 * @param updatedOn the updatedOn to set
	 */
	public void setUpdatedOn(Date updatedOn) {
        if (updatedOn != null) {
            this.updatedOn = (Date) updatedOn.clone();
        } else {
            this.updatedOn = null;
        }
	}
	/**
	 * @return the noukiCode
	 */
	public String getNoukiCode() {
		return noukiCode;
	}
	/**
	 * @param noukiCode the noukiCode to set
	 */
	public void setNoukiCode(String noukiCode) {
		this.noukiCode = noukiCode;
	}
	/**
	 * @return the updatedUserId
	 */
	public String getUpdatedUserId() {
		return updatedUserId;
	}
	/**
	 * @param updatedUserId the updatedUserId to set
	 */
	public void setUpdatedUserId(String updatedUserId) {
		this.updatedUserId = updatedUserId;
	}
	/**
	 * @return the supplierDelFlg
	 */
	public Integer getSupplierDelFlg() {
		return supplierDelFlg;
	}
	/**
	 * @param supplierDelFlg the supplierDelFlg to set
	 */
	public void setSupplierDelFlg(Integer supplierDelFlg) {
		this.supplierDelFlg = supplierDelFlg;
	}
	/**
	 * @return the supplierSyouhinSiireOrder
	 */
	public Integer getSupplierSyouhinSiireOrder() {
		return supplierSyouhinSiireOrder;
	}
	/**
	 * @param supplierSyouhinSiireOrder the supplierSyouhinSiireOrder to set
	 */
	public void setSupplierSyouhinSiireOrder(Integer supplierSyouhinSiireOrder) {
		this.supplierSyouhinSiireOrder = supplierSyouhinSiireOrder;
	}
	/**
	 * @return the supplierTyokusouFlg
	 */
	public Integer getSupplierTyokusouFlg() {
		return supplierTyokusouFlg;
	}
	/**
	 * @param supplierTyokusouFlg the supplierTyokusouFlg to set
	 */
	public void setSupplierTyokusouFlg(Integer supplierTyokusouFlg) {
		this.supplierTyokusouFlg = supplierTyokusouFlg;
	}
	/**
	 * @return the siireName
	 */
	public String getSiireName() {
		return siireName;
	}
	/**
	 * @param siireName the siireName to set
	 */
	public void setSiireName(String siireName) {
		this.siireName = siireName;
	}
	/**
	 * @return the noukiName1
	 */
	public String getNoukiName1() {
		return noukiName1;
	}
	/**
	 * @param noukiName1 the noukiName1 to set
	 */
	public void setNoukiName1(String noukiName1) {
		this.noukiName1 = noukiName1;
	}
	/**
	 * @return the crudFlg
	 */
	public String getCrudFlg() {
		return crudFlg;
	}
	/**
	 * @param crudFlg the crudFlg to set
	 */
	public void setCrudFlg(String crudFlg) {
		this.crudFlg = crudFlg;
	}
	/**
	 * @return the idRow
	 */
	public String getIdRow() {
		return idRow;
	}
	/**
	 * @param idRow the idRow to set
	 */
	public void setIdRow(String idRow) {
		this.idRow = idRow;
	}
    /**
     * @return the syouhinSysCode
     */
    public Long getSyouhinSysCode() {
        return syouhinSysCode;
    }
    /**
     * @param syouhinSysCode the syouhinSysCode to set
     */
    public void setSyouhinSysCode(Long syouhinSysCode) {
        this.syouhinSysCode = syouhinSysCode;
    }

}
