/**
 * EntMstFactoryMatterStatus.
 */
package net.webike.RcProductFactory.entity;

import java.util.Date;

/**
 * @author Nguyen.Chuong
 *
 */
public class EntMstFactoryMatterStatus {

	private String matterStatusCode = "";

	private String matterStatusName = "";

	private int sort;

	private int delFlg;

	private Date tourokuDate;

	private String tourokuUserId = "";

	private Date kousinDate;

	private String kousinUserId = "";

    /**
     * @return the matterStatusCode
     */
    public String getMatterStatusCode() {
        return matterStatusCode;
    }

    /**
     * @param matterStatusCode the matterStatusCode to set
     */
    public void setMatterStatusCode(String matterStatusCode) {
        this.matterStatusCode = matterStatusCode;
    }

    /**
     * @return the matterStatusName
     */
    public String getMatterStatusName() {
        return matterStatusName;
    }

    /**
     * @param matterStatusName the matterStatusName to set
     */
    public void setMatterStatusName(String matterStatusName) {
        this.matterStatusName = matterStatusName;
    }

    /**
     * @return the sort
     */
    public int getSort() {
        return sort;
    }

    /**
     * @param sort the sort to set
     */
    public void setSort(int sort) {
        this.sort = sort;
    }

    /**
     * @return the delFlg
     */
    public int getDelFlg() {
        return delFlg;
    }

    /**
     * @param delFlg the delFlg to set
     */
    public void setDelFlg(int delFlg) {
        this.delFlg = delFlg;
    }

    /**
     * @return the tourokuUserId
     */
    public String getTourokuUserId() {
        return tourokuUserId;
    }

    /**
     * @param tourokuUserId the tourokuUserId to set
     */
    public void setTourokuUserId(String tourokuUserId) {
        this.tourokuUserId = tourokuUserId;
    }

    /**
     * @return the kousinUserId
     */
    public String getKousinUserId() {
        return kousinUserId;
    }

    /**
     * @param kousinUserId the kousinUserId to set
     */
    public void setKousinUserId(String kousinUserId) {
        this.kousinUserId = kousinUserId;
    }

    /**
     * @return the tourokuDate
     */
    public Date getTourokuDate() {
        if (tourokuDate == null) {
            return null;
        }
        return new Date(tourokuDate.getTime());
    }

    /**
     * @param tourokuDate the tourokuDate to set
     */
    public void setTourokuDate(Date tourokuDate) {
        if (tourokuDate == null) {
            this.tourokuDate = null;
            return;
          }
        this.tourokuDate = new Date(tourokuDate.getTime());
    }

    /**
     * @return the kousinDate
     */
    public Date getKousinDate() {
        if (kousinDate == null) {
            return null;
        }
        return new Date(kousinDate.getTime());
    }

    /**
     * @param kousinDate the kousinDate to set
     */
    public void setKousinDate(Date kousinDate) {
        if (kousinDate == null) {
            this.kousinDate = null;
            return;
          }
        this.kousinDate = new Date(kousinDate.getTime());
    }


}
