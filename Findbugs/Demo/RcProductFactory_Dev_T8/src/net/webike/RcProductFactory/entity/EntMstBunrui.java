/************************************************************************
 * file name	： EntMstbunrui.java
 * author		： Tran.Thanh
 * version		： 1.0.0
 * date created	： 23 Oct 2013
 * date updated	： 23 Oct 2013
 * description	： Data from Database rc_webike table mst_bunrui
 *
 ***********************************************************************/
package net.webike.RcProductFactory.entity;

import java.util.Date;

/**
 * EntMstbunruikaisoukaisou.
 */
public class EntMstBunrui {

	private String bunruiCode = "";

	private String bunruiName = "";

	private String bunruiNameS = "";

	private String bunruiHyouji = "";

	private Date bunruiKousinDate;

	/**
	 * @return the bunruiCode
	 */
	public String getBunruiCode() {
		return bunruiCode;
	}

	/**
	 * @param bunruiCode the bunruiCode to set
	 */
	public void setBunruiCode(String bunruiCode) {
		this.bunruiCode = bunruiCode;
	}

	/**
	 * @return the bunruiName
	 */
	public String getBunruiName() {
		return bunruiName;
	}

	/**
	 * @param bunruiName the bunruiName to set
	 */
	public void setBunruiName(String bunruiName) {
		this.bunruiName = bunruiName;
	}

	/**
	 * @return the bunruiNameS
	 */
	public String getBunruiNameS() {
		return bunruiNameS;
	}

	/**
	 * @param bunruiNameS the bunruiNameS to set
	 */
	public void setBunruiNameS(String bunruiNameS) {
		this.bunruiNameS = bunruiNameS;
	}

	/**
	 * @return the bunruiHyouji
	 */
	public String getBunruiHyouji() {
		return bunruiHyouji;
	}

	/**
	 * @param bunruiHyouji the bunruiHyouji to set
	 */
	public void setBunruiHyouji(String bunruiHyouji) {
		this.bunruiHyouji = bunruiHyouji;
	}

	/**
	 * @return the bunruiKousinDate.
	 */
	public Date getBunruiKousinDate() {
		if (bunruiKousinDate == null) {
            return null;
        }
        return new Date(bunruiKousinDate.getTime());
	}

	/**
	 * @param bunruiKousinDate the bunruiKousinDate to set.
	 */
	public void setBunruiKousinDate(Date bunruiKousinDate) {
		if (bunruiKousinDate == null) {
            this.bunruiKousinDate = null;
            return;
          }
        this.bunruiKousinDate = new Date(bunruiKousinDate.getTime());
	}

}
