/************************************************************************
 * File Name    ： EntTblFactoryProductFitModel.java
 * Author       ： hoang.ho
 * Version      ： 1.0.0
 * Date Created ： 2014/01/23
 * Date Updated ： 2014/01/23
 * Description  ： Entity of EntTblFactoryProductFitModel.
 ***********************************************************************/
package net.webike.RcProductFactory.entity;

import java.util.Date;

/**
 * Entity of EntTblFactoryProductFitModel.
 * @author hoang.ho
 * @since 2014/01/23
 */
public class EntTblFactoryProductFitModel {

    private Long productId;
    private Integer fitModelSort;
    private String fitModelMaker;
    private Integer fitModelMakerNoFitFlg;
    private String fitModelModel;
    private Integer fitModelModelNoFitFlg;
    private String fitModelStyle;
    private Integer fitModelDelFlg = 0;
    private Date updateDate;
    private String updateUserId;

    private String sortField;
    private String sortDir;

    private String idRow = "";
    private String crudFlg = "";
    private String makerLogo;
    //BOE @rcv!Luong.Tuong 2014/07/18 #9955 :
    private boolean isValidModelName = false; //true is valid, false is invalid
    private boolean isValidMakerName = false; //true is valid, false is invalid
	//EOE @rcv!Luong.Tuong 2014/07/18 #9955 :
    /**
     * @return the fitModelSort
     */
    public Integer getFitModelSort() {
        return fitModelSort;
    }
    /**
     * @param fitModelSort the fitModelSort to set
     */
    public void setFitModelSort(Integer fitModelSort) {
        this.fitModelSort = fitModelSort;
    }
    /**
     * @return the fitModelMaker
     */
    public String getFitModelMaker() {
        return fitModelMaker;
    }
    /**
     * @param fitModelMaker the fitModelMaker to set
     */
    public void setFitModelMaker(String fitModelMaker) {
        this.fitModelMaker = fitModelMaker;
    }
    /**
     * @return the fitModelMakerNoFitFlg
     */
    public Integer getFitModelMakerNoFitFlg() {
        return fitModelMakerNoFitFlg;
    }
    /**
     * @param fitModelMakerNoFitFlg the fitModelMakerNoFitFlg to set
     */
    public void setFitModelMakerNoFitFlg(Integer fitModelMakerNoFitFlg) {
        this.fitModelMakerNoFitFlg = fitModelMakerNoFitFlg;
    }
    /**
     * @return the fitModelModel
     */
    public String getFitModelModel() {
        return fitModelModel;
    }
    /**
     * @param fitModelModel the fitModelModel to set
     */
    public void setFitModelModel(String fitModelModel) {
        this.fitModelModel = fitModelModel;
    }
    /**
     * @return the fitModelModelNoFitFlg
     */
    public Integer getFitModelModelNoFitFlg() {
        return fitModelModelNoFitFlg;
    }
    /**
     * @param fitModelModelNoFitFlg the fitModelModelNoFitFlg to set
     */
    public void setFitModelModelNoFitFlg(Integer fitModelModelNoFitFlg) {
        this.fitModelModelNoFitFlg = fitModelModelNoFitFlg;
    }
    /**
     * @return the fitModelStyle
     */
    public String getFitModelStyle() {
        return fitModelStyle;
    }
    /**
     * @param fitModelStyle the fitModelStyle to set
     */
    public void setFitModelStyle(String fitModelStyle) {
        this.fitModelStyle = fitModelStyle;
    }
    /**
     * @return the fitModelDelFlg
     */
    public Integer getFitModelDelFlg() {
        return fitModelDelFlg;
    }
    /**
     * @param fitModelDelFlg the fitModelDelFlg to set
     */
    public void setFitModelDelFlg(Integer fitModelDelFlg) {
        this.fitModelDelFlg = fitModelDelFlg;
    }
    /**
     * @return the productId
     */
    public Long getProductId() {
        return productId;
    }
    /**
     * @param productId the productId to set
     */
    public void setProductId(Long productId) {
        this.productId = productId;
    }
    /**
     * @return the sortField
     */
    public String getSortField() {
        return sortField;
    }
    /**
     * @param sortField the sortField to set
     */
    public void setSortField(String sortField) {
        this.sortField = sortField;
    }
    /**
     * @return the sortDir
     */
    public String getSortDir() {
        return sortDir;
    }
    /**
     * @param sortDir the sortDir to set
     */
    public void setSortDir(String sortDir) {
        this.sortDir = sortDir;
    }
    /**
     * @return the updateUserId
     */
    public String getUpdateUserId() {
        return updateUserId;
    }
    /**
     * @param updateUserId the updateUserId to set
     */
    public void setUpdateUserId(String updateUserId) {
        this.updateUserId = updateUserId;
    }
    /**
     * @return the updateDate
     */
    public Date getUpdateDate() {
        if (updateDate != null) {
            return (Date) updateDate.clone();
        }

        return null;
    }
    /**
     * @param updateDate the updateDate to set
     */
    public void setUpdateDate(Date updateDate) {
        if (updateDate != null) {
            this.updateDate = (Date) updateDate.clone();
        } else {
            this.updateDate = null;
        }
    }
	/**
	 * @return the rowId
	 */
	public String getRowId() {
		return idRow;
	}
	/**
	 * @param rowId the rowId to set
	 */
	public void setRowId(String rowId) {
		this.idRow = rowId;
	}
	/**
	 * @return the crudFlg
	 */
	public String getCrudFlg() {
		return crudFlg;
	}
	/**
	 * @param crudFlg the crudFlg to set
	 */
	public void setCrudFlg(String crudFlg) {
		this.crudFlg = crudFlg;
	}
	/**
	 * @return the makerLogo
	 */
	public String getMakerLogo() {
		return makerLogo;
	}
	/**
	 * @param makerLogo the makerLogo to set
	 */
	public void setMakerLogo(String makerLogo) {
		this.makerLogo = makerLogo;
	}
	   /**
     * @return the makerLogo
     */
    public boolean isValidModelName() {
        return isValidModelName;
    }
    /**
     * @param isValidModelName the isValidModelName to set
     */
    public void setValidModelName(boolean isValidModelName) {
        this.isValidModelName = isValidModelName;
    }
    /**
     * @return the isValidMakerName
     */
    public boolean isValidMakerName() {
        return isValidMakerName;
    }
    
    /**
     * @param isValidMakerName the isValidMakerName to set
     */
    public void setValidMakerName(boolean isValidMakerName) {
        this.isValidMakerName = isValidMakerName;
    }


}

