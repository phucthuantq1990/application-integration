/************************************************************************
 * file name	： EntTblProductLink.java
 * author		： Nguyen.Chuong
 * version		： 1.0.0
 * date created	： 2014/02/19
 * date updated	： 2014/02/19
 * description	： Entity EntTblProductLink to map with rc_syouhin.tbl_product_link.
 *
 ***********************************************************************/
package net.webike.RcProductFactory.entity;


/**
 * EntTblProductLink.
 */
public class EntTblProductLink {
	private Long syouhinSysCode;
	//rc_syouhin.tbl_product_link.link_reason_name
	private String linkReasonName = "";
	//rc_syouhin.tbl_product_link.product_link_title
	private String productLinkTitle = "";
	//c_syouhin.mst_link_reason.product_link_url
	private String productLinkURL;
    /**
     * @return the syouhinSysCode
     */
    public Long getSyouhinSysCode() {
        return syouhinSysCode;
    }
    /**
     * @param syouhinSysCode the syouhinSysCode to set
     */
    public void setSyouhinSysCode(Long syouhinSysCode) {
        this.syouhinSysCode = syouhinSysCode;
    }
    /**
     * @return the linkReasonName
     */
    public String getLinkReasonName() {
        return linkReasonName;
    }
    /**
     * @param linkReasonName the linkReasonName to set
     */
    public void setLinkReasonName(String linkReasonName) {
        this.linkReasonName = linkReasonName;
    }
    /**
     * @return the productLinkTitle
     */
    public String getProductLinkTitle() {
        return productLinkTitle;
    }
    /**
     * @param productLinkTitle the productLinkTitle to set
     */
    public void setProductLinkTitle(String productLinkTitle) {
        this.productLinkTitle = productLinkTitle;
    }
    /**
     * @return the productLinkURL
     */
    public String getProductLinkURL() {
        return productLinkURL;
    }
    /**
     * @param productLinkURL the productLinkURL to set
     */
    public void setProductLinkURL(String productLinkURL) {
        this.productLinkURL = productLinkURL;
    }
}
