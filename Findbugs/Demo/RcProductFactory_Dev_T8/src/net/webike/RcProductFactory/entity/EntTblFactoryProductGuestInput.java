/************************************************************************
 * file name	： EntTblFactoryProductGeneral.java
 * author		： Le.Dinh
 * version		： 1.0.0
 * date created	： 2014/02/12
 * date updated	： 2014/02/12
 * description	： Entity EntTblFactoryProductGeneral.
 *
 ***********************************************************************/
package net.webike.RcProductFactory.entity;

import java.util.Date;


/**
 * EntMstProductSupplierStatus.
 */
public class EntTblFactoryProductGuestInput {

	private Long productId;
	private Long syouhinSysCode;
	private Integer productGuestSort;
	private String productGuestInputTitle;
	private String productGuestInputDescription;
	private String updatedUserId;
	private Date updatedOn;
	private String idRow = "";
	private String crudFlg = "";

	/**
	 * @return the productId
	 */
	public Long getProductId() {
		return productId;
	}
	/**
	 * @param productId the productId to set
	 */
	public void setProductId(Long productId) {
		this.productId = productId;
	}
    /**
     * @return the productGuestInputTitle
     */
    public String getProductGuestInputTitle() {
        return productGuestInputTitle;
    }
    /**
     * @param productGuestInputTitle the productGuestInputTitle to set
     */
    public void setProductGuestInputTitle(String productGuestInputTitle) {
        this.productGuestInputTitle = productGuestInputTitle;
    }
    /**
     * @return the productGuestInputDescription
     */
    public String getProductGuestInputDescription() {
        return productGuestInputDescription;
    }
    /**
     * @param productGuestInputDescription the productGuestInputDescription to set
     */
    public void setProductGuestInputDescription(
            String productGuestInputDescription) {
        this.productGuestInputDescription = productGuestInputDescription;
    }
    /**
     * @return the productGuestSort
     */
    public Integer getProductGuestSort() {
        return productGuestSort;
    }
    /**
     * @param productGuestSort the productGuestSort to set
     */
    public void setProductGuestSort(Integer productGuestSort) {
        this.productGuestSort = productGuestSort;
    }
	/**
	 * @return the updatedUserId
	 */
	public String getUpdatedUserId() {
		return updatedUserId;
	}
	/**
	 * @param updatedUserId the updatedUserId to set
	 */
	public void setUpdatedUserId(String updatedUserId) {
		this.updatedUserId = updatedUserId;
	}
	/**
	 * @return the updatedOn
	 */
	public Date getUpdatedOn() {
		if (updatedOn != null) {
            return (Date) updatedOn.clone();
        }
        return null;
	}
	/**
	 * @param updatedOn the updatedOn to set
	 */
	public void setUpdatedOn(Date updatedOn) {
		if (updatedOn != null) {
			this.updatedOn = (Date) updatedOn.clone();
        } else {
        	this.updatedOn = null;
        }
	}
	/**
	 * @return the idRow
	 */
	public String getIdRow() {
		return idRow;
	}
	/**
	 * @param idRow the idRow to set
	 */
	public void setIdRow(String idRow) {
		this.idRow = idRow;
	}
	/**
	 * @return the crudFlg
	 */
	public String getCrudFlg() {
		return crudFlg;
	}
	/**
	 * @param crudFlg the crudFlg to set
	 */
	public void setCrudFlg(String crudFlg) {
		this.crudFlg = crudFlg;
	}
    /**
     * @return the syouhinSysCode
     */
    public Long getSyouhinSysCode() {
        return syouhinSysCode;
    }
    /**
     * @param syouhinSysCode the syouhinSysCode to set
     */
    public void setSyouhinSysCode(Long syouhinSysCode) {
        this.syouhinSysCode = syouhinSysCode;
    }
}
