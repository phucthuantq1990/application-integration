/**
 * EntMstFactoryMatter.
 */
package net.webike.RcProductFactory.entity;

import java.util.Date;

/**
 * @author Nguyen.Chuong
 *
 */
public class EntMstFactoryMatter {

	// PJ code
    private long matterNo;

	private String matterName = "";

	private String matterKindCode = "";

	private String matterStatusCode = "";

	private String matterChargeUserId = "";

	private String matterMessage = "";

	// 1 : deleted, 0 : not deleted
	private int delFlg;

	private Date completionDate;

	// start date (register date )
	private Date tourokuDate;

	private String tourokuUserId = "";

	private Date kousinDate;

	private String kousinUserId = "";

	private String userLastName = "";

	private String userFirstName = "";

	private String matterKindName = "";

	private String matterStatusName = "";

	//Count all product of matter.
	private int totalAllProduct;
	//Count all finished product.
	private int totalCheckedProduct;
	//Count all error product.
	private int totalErrorProduct;
	//matter factory error count model
	private int matterFactoryErrorCountModel;
    private String fullName;
    private Date createdOn;
    private String createdUserId;
    private Date updatedOn;
    private String updatedUserId;

    /**
     * @return the completionDate
     */
    public Date getCompletionDate() {
        if (completionDate == null) {
            return null;
        }
        return new Date(completionDate.getTime());
    }

    /**
     * @param completionDate the completionDate to set
     */
    public void setCompletionDate(Date completionDate) {
        if (completionDate == null) {
            this.completionDate = null;
            return;
          }
        this.completionDate = new Date(completionDate.getTime());
    }

    /**
     * @return the tourokuDate
     */
    public Date getTourokuDate() {
        if (tourokuDate == null) {
            return null;
        }
        return new Date(tourokuDate.getTime());
    }

    /**
     * @param tourokuDate the tourokuDate to set
     */
    public void setTourokuDate(Date tourokuDate) {
        if (tourokuDate == null) {
            this.tourokuDate = null;
            return;
          }
        this.tourokuDate = new Date(tourokuDate.getTime());
    }

    /**
     * @return the kousinDate
     */
    public Date getKousinDate() {
        if (kousinDate == null) {
            return null;
        }
        return new Date(kousinDate.getTime());
    }

    /**
     * @param kousinDate the kousinDate to set
     */
    public void setKousinDate(Date kousinDate) {
        if (kousinDate == null) {
            this.kousinDate = null;
            return;
          }
        this.kousinDate = new Date(kousinDate.getTime());
    }

    /**
     * @return the matterName
     */
    public String getMatterName() {
        return matterName;
    }

    /**
     * @param matterName the matterName to set
     */
    public void setMatterName(String matterName) {
        this.matterName = matterName;
    }

    /**
     * @return the matterKindCode
     */
    public String getMatterKindCode() {
        return matterKindCode;
    }

    /**
     * @param matterKindCode the matterKindCode to set
     */
    public void setMatterKindCode(String matterKindCode) {
        this.matterKindCode = matterKindCode;
    }

    /**
     * @return the matterStatusCode
     */
    public String getMatterStatusCode() {
        return matterStatusCode;
    }

    /**
     * @param matterStatusCode the matterStatusCode to set
     */
    public void setMatterStatusCode(String matterStatusCode) {
        this.matterStatusCode = matterStatusCode;
    }

    /**
     * @return the matterChargeUserId
     */
    public String getMatterChargeUserId() {
        return matterChargeUserId;
    }

    /**
     * @param matterChargeUserId the matterChargeUserId to set
     */
    public void setMatterChargeUserId(String matterChargeUserId) {
        this.matterChargeUserId = matterChargeUserId;
    }

    /**
     * @return the matterMessage
     */
    public String getMatterMessage() {
        return matterMessage;
    }

    /**
     * @param matterMessage the matterMessage to set
     */
    public void setMatterMessage(String matterMessage) {
        this.matterMessage = matterMessage;
    }

    /**
     * @return the delFlg
     */
    public int getDelFlg() {
        return delFlg;
    }

    /**
     * @param delFlg the delFlg to set
     */
    public void setDelFlg(int delFlg) {
        this.delFlg = delFlg;
    }

    /**
     * @return the tourokuUserId
     */
    public String getTourokuUserId() {
        return tourokuUserId;
    }

    /**
     * @param tourokuUserId the tourokuUserId to set
     */
    public void setTourokuUserId(String tourokuUserId) {
        this.tourokuUserId = tourokuUserId;
    }

    /**
     * @return the kousinUserId
     */
    public String getKousinUserId() {
        return kousinUserId;
    }

    /**
     * @param kousinUserId the kousinUserId to set
     */
    public void setKousinUserId(String kousinUserId) {
        this.kousinUserId = kousinUserId;
    }

    /**
     * @return the userLastName
     */
    public String getUserLastName() {
        return userLastName;
    }

    /**
     * @param userLastName the userLastName to set
     */
    public void setUserLastName(String userLastName) {
        this.userLastName = userLastName;
    }

    /**
     * @return the userFirstName
     */
    public String getUserFirstName() {
        return userFirstName;
    }

    /**
     * @param userFirstName the userFirstName to set
     */
    public void setUserFirstName(String userFirstName) {
        this.userFirstName = userFirstName;
    }

    /**
     * @return the matterKindName
     */
    public String getMatterKindName() {
        return matterKindName;
    }

    /**
     * @param matterKindName the matterKindName to set
     */
    public void setMatterKindName(String matterKindName) {
        this.matterKindName = matterKindName;
    }

    /**
     * @return the matterStatusName
     */
    public String getMatterStatusName() {
        return matterStatusName;
    }

    /**
     * @param matterStatusName the matterStatusName to set
     */
    public void setMatterStatusName(String matterStatusName) {
        this.matterStatusName = matterStatusName;
    }

    /**
     * @return the totalAllProduct
     */
    public int getTotalAllProduct() {
        return totalAllProduct;
    }

    /**
     * @param totalAllProduct the totalAllProduct to set
     */
    public void setTotalAllProduct(int totalAllProduct) {
        this.totalAllProduct = totalAllProduct;
    }

    /**
     * @return the totalErrorProduct
     */
    public int getTotalErrorProduct() {
        return totalErrorProduct;
    }

    /**
     * @param totalErrorProduct the totalErrorProduct to set
     */
    public void setTotalErrorProduct(int totalErrorProduct) {
        this.totalErrorProduct = totalErrorProduct;
    }

    /**
     * @return the totalCheckedProduct
     */
    public int getTotalCheckedProduct() {
        return totalCheckedProduct;
    }

    /**
     * @param totalCheckedProduct the totalCheckedProduct to set
     */
    public void setTotalCheckedProduct(int totalCheckedProduct) {
        this.totalCheckedProduct = totalCheckedProduct;
    }

    /**
     * @return the matterFactoryErrorCountModel
     */
    public int getMatterFactoryErrorCountModel() {
        return matterFactoryErrorCountModel;
    }

    /**
     * @param matterFactoryErrorCountModel the matterFactoryErrorCountModel to set
     */
    public void setMatterFactoryErrorCountModel(int matterFactoryErrorCountModel) {
        this.matterFactoryErrorCountModel = matterFactoryErrorCountModel;
    }

    /**
     * @return the fullName
     */
    public String getFullName() {
        return fullName;
    }

    /**
     * @param fullName the fullName to set
     */
    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    /**
     * @return the createdOn
     */
    public Date getCreatedOn() {
        if (this.createdOn != null) {
            return (Date) createdOn.clone();
        }
        return null;
    }

    /**
     * @param createdOn the createdOn to set
     */
    public void setCreatedOn(Date createdOn) {
        if (createdOn != null) {
            this.createdOn = (Date) createdOn.clone();
        } else {
            this.createdOn = null;
        }
    }

    /**
     * @return the createdUserId
     */
    public String getCreatedUserId() {
        return createdUserId;
    }

    /**
     * @param createdUserId the createdUserId to set
     */
    public void setCreatedUserId(String createdUserId) {
        this.createdUserId = createdUserId;
    }

    /**
     * @return the updatedOn
     */
    public Date getUpdatedOn() {
        if (this.updatedOn != null) {
            return (Date) updatedOn.clone();
        }
        return null;
    }

    /**
     * @param updatedOn the updatedOn to set
     */
    public void setUpdatedOn(Date updatedOn) {
        if (updatedOn != null) {
            this.updatedOn = (Date) updatedOn.clone();
        } else {
            this.updatedOn = null;
        }
    }

    /**
     * @return the updatedUserId
     */
    public String getUpdatedUserId() {
        return updatedUserId;
    }
    /**
     * @param updatedUserId the updatedUserId to set
     */
    public void setUpdatedUserId(String updatedUserId) {
        this.updatedUserId = updatedUserId;
    }

    /**
     * @return the matterNo
     */
    public long getMatterNo() {
        return matterNo;
    }

    /**
     * @param matterNo the matterNo to set
     */
    public void setMatterNo(long matterNo) {
        this.matterNo = matterNo;
    }
}
