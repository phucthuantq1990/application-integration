package net.webike.RcProductFactory.entity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.webike.RcProductFactory.util.Utils;

/**
 * Enum of various flag.
 */
public enum EnumVariousFlag {
    OPEN_PRICE(0, Utils.getProp("openPrice")),
    SALE_PRICE(1, Utils.getProp("salePrice")),
    BUILD_TO_ORDER_MANUFACTURING(2, Utils.getProp("buildToOrderManufacturing")),
    NOT_RETURNED_OR_EXCHANGED(3, Utils.getProp("notReturnedOrExchanged")),
    PREFERENCE_IMAGE(4, Utils.getProp("preferenceImage"));

    private final int key;
    private final String displayName;

    /**
     * Enum of various flag.
     * @param key Key
     * @param displayName Display name
     */
    private EnumVariousFlag(final int key, final String displayName) {
    	this.key = key;
    	this.displayName = displayName;
	}


    /**
     * Get enum list.
     * @author		nguyen.hieu
     * @date		2014-02-20
     * @return		Enum list.
     */
    public static List<Map<String, String>> getEnumList() {
        List<Map<String, String>> enumList = new ArrayList<Map<String, String>>();
        for (EnumVariousFlag value : values()) {
        	Map<String, String> map = new HashMap<String, String>(2);
        	map.put("key", String.valueOf(value.key));
        	map.put("displayName", value.displayName);
        	enumList.add(map);
        }

        return enumList;
    }

    public int getKey() {
		return key;
	}

    public String getDisplayName() {
		return displayName;
	}
}
