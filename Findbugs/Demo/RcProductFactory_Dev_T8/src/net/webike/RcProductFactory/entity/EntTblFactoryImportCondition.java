/************************************************************************
 * File Name    ： EntTblFactoryImportCondition.java
 * Author       ： Long Vu
 * Version      ： 1.0.0
 * Date Created ： 2014/02/21
 * Date Updated ： 2014/02/21
 * Description  ： Entity of EntTblFactoryImportCondition.
 ***********************************************************************/
package net.webike.RcProductFactory.entity;

import java.util.Date;

/**
 * Entity of EntTblFactoryImportCondition.
 * @author Long Vu
 * @since 2014/02/21
 */
public class EntTblFactoryImportCondition {
    private long importConditionCode;
    private String importConditionImportType;
    private long importConditionMatterNo;
    private String importConditionFileName;
    private String importConditionOriginalName;
    private Integer importConditionExeCount;
    private Integer importConditionExeCountAdd;
    private Integer importConditionMaxCount;
    private Integer importConditionStatus;
    private Date createdOn;
    private String createdUserId;
    private Date updatedOn;
    private String updatedUserId;
    /**
     * @return the importConditionCode
     */
    public long getImportConditionCode() {
        return importConditionCode;
    }
    /**
     * @param importConditionCode the importConditionCode to set
     */
    public void setImportConditionCode(long importConditionCode) {
        this.importConditionCode = importConditionCode;
    }
    /**
     * @return the importConditionImportType
     */
    public String getImportConditionImportType() {
        return importConditionImportType;
    }
    /**
     * @param importConditionImportType the importConditionImportType to set
     */
    public void setImportConditionImportType(String importConditionImportType) {
        this.importConditionImportType = importConditionImportType;
    }
    /**
     * @return the importConditionMatterNo
     */
    public long getImportConditionMatterNo() {
        return importConditionMatterNo;
    }
    /**
     * @param importConditionMatterNo the importConditionMatterNo to set
     */
    public void setImportConditionMatterNo(long importConditionMatterNo) {
        this.importConditionMatterNo = importConditionMatterNo;
    }
    /**
     * @return the importConditionFileName
     */
    public String getImportConditionFileName() {
        return importConditionFileName;
    }
    /**
     * @param importConditionFileName the importConditionFileName to set
     */
    public void setImportConditionFileName(String importConditionFileName) {
        this.importConditionFileName = importConditionFileName;
    }
    /**
     * @return the importConditionOriginalName
     */
    public String getImportConditionOriginalName() {
        return importConditionOriginalName;
    }
    /**
     * @param importConditionOriginalName the importConditionOriginalName to set
     */
    public void setImportConditionOriginalName(String importConditionOriginalName) {
        this.importConditionOriginalName = importConditionOriginalName;
    }
    /**
     * @return the importConditionExeCount
     */
    public Integer getImportConditionExeCount() {
        return importConditionExeCount;
    }
    /**
     * @param importConditionExeCount the importConditionExeCount to set
     */
    public void setImportConditionExeCount(Integer importConditionExeCount) {
        this.importConditionExeCount = importConditionExeCount;
    }
    /**
     * @return the importConditionMaxCount
     */
    public Integer getImportConditionMaxCount() {
        return importConditionMaxCount;
    }
    /**
     * @param importConditionMaxCount the importConditionMaxCount to set
     */
    public void setImportConditionMaxCount(Integer importConditionMaxCount) {
        this.importConditionMaxCount = importConditionMaxCount;
    }
    /**
     * @return the importConditionStatus
     */
    public Integer getImportConditionStatus() {
        return importConditionStatus;
    }
    /**
     * @param importConditionStatus the importConditionStatus to set
     */
    public void setImportConditionStatus(Integer importConditionStatus) {
        this.importConditionStatus = importConditionStatus;
    }
    /**
     * @return the createdOn
     */
    public Date getCreatedOn() {
        if (this.createdOn != null) {
            return (Date) createdOn.clone();
        }
        return null;
    }
    /**
     * @param createdOn the createdOn to set
     */
    public void setCreatedOn(Date createdOn) {
        if (createdOn != null) {
            this.createdOn = (Date) createdOn.clone();
        } else {
            this.createdOn = null;
        }
    }
    /**
     * @return the createdUserId
     */
    public String getCreatedUserId() {
        return createdUserId;
    }
    /**
     * @param createdUserId the createdUserId to set
     */
    public void setCreatedUserId(String createdUserId) {
        this.createdUserId = createdUserId;
    }
    /**
     * @return the updatedOn
     */
    public Date getUpdatedOn() {
        if (this.updatedOn != null) {
            return (Date) updatedOn.clone();
        }
        return null;
    }
    /**
     * @param updatedOn the updatedOn to set
     */
    public void setUpdatedOn(Date updatedOn) {
        if (updatedOn != null) {
            this.updatedOn = (Date) updatedOn.clone();
        } else {
            this.updatedOn = null;
        }
    }
    /**
     * @return the updatedUserId
     */
    public String getUpdatedUserId() {
        return updatedUserId;
    }
    /**
     * @param updatedUserId the updatedUserId to set
     */
    public void setUpdatedUserId(String updatedUserId) {
        this.updatedUserId = updatedUserId;
    }
    /**
     * @return the importConditionExeCountAdd
     */
    public Integer getImportConditionExeCountAdd() {
        return importConditionExeCountAdd;
    }
    /**
     * @param importConditionExeCountAdd the importConditionExeCountAdd to set
     */
    public void setImportConditionExeCountAdd(Integer importConditionExeCountAdd) {
        this.importConditionExeCountAdd = importConditionExeCountAdd;
    }
}

