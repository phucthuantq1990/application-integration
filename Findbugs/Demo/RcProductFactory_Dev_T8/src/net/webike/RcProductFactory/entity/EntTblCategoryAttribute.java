/************************************************************************
 * File Name EntTblCategoryAttribute.java
 * Author hoang.ho
 * Version 1.0.0
 * Date Created 2014/01/07
 * Date Updated 2014/01/07
 * Description �store value of EntTblCategoryAttribute.
 ***********************************************************************/
package net.webike.RcProductFactory.entity;

import java.util.Date;

import org.apache.commons.lang.StringUtils;

/**
 * @author hoang.ho
 *
 */
public class EntTblCategoryAttribute {
    private String categoryCode;
    private String attributeCode;
    private String requiredFlg;
    /** create an instance to get value from requiredFlg for fixing bug mapping. */
    private int requiredTranferFlg;
    private String sort;
    private Date createdOn;
    private String createdUserFlg;
    private String updatedOn;
    private String updatedUserFlg;
    private String attributeName;
    private String attributeType;
    private String chkRequiredFlg;
    private String loginUserId;
    /** Check delete in client 0: note delete, 1: delete.*/
    private String delFlg = "0";
    /** original attribute.*/
    private String originalAttribute;
    /**
     * @return the createdOn
     */
    public Date getCreatedOn() {
        if (createdOn != null) {
            return (Date) createdOn.clone();
        }
        return null;
    }
    /**
     * @param createdOn the createdOn to set
     */
    public void setCreatedOn(Date createdOn) {
        if (createdOn != null) {
            this.createdOn = (Date) createdOn.clone();
        } else {
            this.createdOn = null;
        }
    }
    /**
     * @return the createdUserFlg
     */
    public String getCreatedUserFlg() {
        return createdUserFlg;
    }
    /**
     * @param createdUserFlg the createdUserFlg to set
     */
    public void setCreatedUserFlg(String createdUserFlg) {
        this.createdUserFlg = createdUserFlg;
    }
    /**
     * @return the updatedOn
     */
    public String getUpdatedOn() {
        return updatedOn;
    }
    /**
     * @param updatedOn the updatedOn to set
     */
    public void setUpdatedOn(String updatedOn) {
        this.updatedOn = updatedOn;
    }
    /**
     * @return the updatedUserFlg
     */
    public String getUpdatedUserFlg() {
        return updatedUserFlg;
    }
    /**
     * @param updatedUserFlg the updatedUserFlg to set
     */
    public void setUpdatedUserFlg(String updatedUserFlg) {
        this.updatedUserFlg = updatedUserFlg;
    }
    /**
     * @return the attributeName
     */
    public String getAttributeName() {
        return attributeName;
    }
    /**
     * @param attributeName the attributeName to set
     */
    public void setAttributeName(String attributeName) {
        this.attributeName = attributeName;
    }
    /**
     * @return the attributeType
     */
    public String getAttributeType() {
        return attributeType;
    }
    /**
     * @param attributeType the attributeType to set
     */
    public void setAttributeType(String attributeType) {
        this.attributeType = attributeType;
    }
    /**
     * @return the chkRequiredFlg
     */
    public String getChkRequiredFlg() {
        if (StringUtils.equals(this.requiredFlg, "1")) {
            this.chkRequiredFlg = "true";
        } else {
            this.chkRequiredFlg = "false";
        }
        return this.chkRequiredFlg;
    }
    /**
     * @param chkRequiredFlg the chkRequiredFlg to set
     */
    public void setChkRequiredFlg(String chkRequiredFlg) {
        this.chkRequiredFlg = chkRequiredFlg;
    }
    /**
     * @return the loginUserId
     */
    public String getLoginUserId() {
        return loginUserId;
    }
    /**
     * @param loginUserId the loginUserId to set
     */
    public void setLoginUserId(String loginUserId) {
        this.loginUserId = loginUserId;
    }
    /**
     * @return the delFlg
     */
    public String getDelFlg() {
        return delFlg;
    }
    /**
     * @param delFlg the delFlg to set
     */
    public void setDelFlg(String delFlg) {
        this.delFlg = delFlg;
    }
    /**
     * @return the categoryCode
     */
    public String getCategoryCode() {
        return categoryCode;
    }
    /**
     * @param categoryCode the categoryCode to set
     */
    public void setCategoryCode(String categoryCode) {
        this.categoryCode = categoryCode;
    }
    /**
     * @return the attributeCode
     */
    public String getAttributeCode() {
        return attributeCode;
    }
    /**
     * @param attributeCode the attributeCode to set
     */
    public void setAttributeCode(String attributeCode) {
        this.attributeCode = attributeCode;
    }
    /**
     * @return the requiredFlg
     */
    public String getRequiredFlg() {
        return requiredFlg;
    }
    /**
     * @param requiredFlg the requiredFlg to set
     */
    public void setRequiredFlg(String requiredFlg) {
        this.requiredFlg = requiredFlg;
    }
    /**
     * @return the sort
     */
    public String getSort() {
        return sort;
    }
    /**
     * @param sort the sort to set
     */
    public void setSort(String sort) {
        this.sort = sort;
    }
    /**
     * @return the originalAttribute
     */
    public String getOriginalAttribute() {
        return originalAttribute;
    }
    /**
     * @param originalAttribute the originalAttribute to set
     */
    public void setOriginalAttribute(String originalAttribute) {
        this.originalAttribute = originalAttribute;
    }
    /**
     * @return the requiredTranferFlg
     */
    public int getRequiredTranferFlg() {
        return requiredTranferFlg;
    }
    /**
     * @param requiredTranferFlg the requiredTranferFlg to set
     */
    public void setRequiredTranferFlg(int requiredTranferFlg) {
        this.requiredTranferFlg = requiredTranferFlg;
    }
}
