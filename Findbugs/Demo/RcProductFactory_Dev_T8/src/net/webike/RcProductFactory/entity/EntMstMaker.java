/************************************************************************
 * file name	： EntMstMaker.java
 * author		：Long Vu
 * version		： 1.0.0
 * date created	： 2014/02/10
 * description	： Entity for mst maker.
 *
 ***********************************************************************/
package net.webike.RcProductFactory.entity;

/**
 * EntMstMaker.
 */
public class EntMstMaker {
    private String makerName;
    private String makerNameEnglish;
    private String makerSort;
    /**
     * @return the makerName
     */
    public String getMakerName() {
        return makerName;
    }
    /**
     * @param makerName the makerName to set
     */
    public void setMakerName(String makerName) {
        this.makerName = makerName;
    }
    /**
     * @return the makerNameEnglish
     */
    public String getMakerNameEnglish() {
        return makerNameEnglish;
    }
    /**
     * @param makerNameEnglish the makerNameEnglish to set
     */
    public void setMakerNameEnglish(String makerNameEnglish) {
        this.makerNameEnglish = makerNameEnglish;
    }
    /**
     * @return the makerSort
     */
    public String getMakerSort() {
        return makerSort;
    }
    /**
     * @param makerSort the makerSort to set
     */
    public void setMakerSort(String makerSort) {
        this.makerSort = makerSort;
    }
}
