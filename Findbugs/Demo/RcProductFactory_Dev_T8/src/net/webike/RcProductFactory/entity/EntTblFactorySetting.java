/**
 * EntTblFactorySetting mapping with rc_product_factory.tbl_factory_setting;.
 */
package net.webike.RcProductFactory.entity;


/**
 * @author Nguyen.Chuong
 *
 */
public class EntTblFactorySetting {
    private String settingValue;
    private String settingCode;
    /**
     * @return the settingValue
     */
    public String getSettingValue() {
        return settingValue;
    }

    /**
     * @param settingValue the settingValue to set
     */
    public void setSettingValue(String settingValue) {
        this.settingValue = settingValue;
    }

    /**
     * @return the settingCode
     */
    public String getSettingCode() {
        return settingCode;
    }

    /**
     * @param settingCode the settingCode to set
     */
    public void setSettingCode(String settingCode) {
        this.settingCode = settingCode;
    }

}
