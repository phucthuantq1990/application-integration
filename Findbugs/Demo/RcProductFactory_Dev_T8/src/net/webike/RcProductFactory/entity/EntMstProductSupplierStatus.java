/************************************************************************
 * file name	： EntMstProductSupplierStatus.java
 * author		： Le.Dinh
 * version		： 1.0.0
 * date created	： 2014/02/12
 * date updated	： 2014/02/12
 * description	： Entity EntMstProductSupplierStatus.
 *
 ***********************************************************************/
package net.webike.RcProductFactory.entity;

/**
 * EntMstProductSupplierStatus.
 */
public class EntMstProductSupplierStatus {
	private Integer supplierStatusCode;
	private String  supplierStatusName;

	private String rowId = "";

	private String crudFlg = "";

	/**
	 * @return the supplierStatusCode
	 */
	public Integer getSupplierStatusCode() {
		return supplierStatusCode;
	}
	/**
	 * @param supplierStatusCode the supplierStatusCode to set
	 */
	public void setSupplierStatusCode(Integer supplierStatusCode) {
		this.supplierStatusCode = supplierStatusCode;
	}
	/**
	 * @return the supplierStatusName
	 */
	public String getSupplierStatusName() {
		return supplierStatusName;
	}
	/**
	 * @param supplierStatusName the supplierStatusName to set
	 */
	public void setSupplierStatusName(String supplierStatusName) {
		this.supplierStatusName = supplierStatusName;
	}
	/**
	 * @return the rowId
	 */
	public String getRowId() {
		return rowId;
	}
	/**
	 * @param rowId the rowId to set
	 */
	public void setRowId(String rowId) {
		this.rowId = rowId;
	}
	/**
	 * @return the crudFlg
	 */
	public String getCrudFlg() {
		return crudFlg;
	}
	/**
	 * @param crudFlg the crudFlg to set
	 */
	public void setCrudFlg(String crudFlg) {
		this.crudFlg = crudFlg;
	}
}
