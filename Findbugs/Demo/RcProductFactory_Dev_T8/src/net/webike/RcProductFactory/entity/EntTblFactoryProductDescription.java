/************************************************************************
 * file name	： EntTblFactoryProductDescription.java.
 * author		： Tran.Thanh
 * version		： 1.0.0
 * date created	： 12 Feb 2014
 * date updated	： 12 Feb 2014
 * description	： Table Tbl_Factory_Product_Description
 *
 ***********************************************************************/
package net.webike.RcProductFactory.entity;

import java.util.Date;

/**
 * Table Tbl_Factory_Product_Description in Database.
 */
public class EntTblFactoryProductDescription {

	private Long productId;

	private String descriptionType = "";

	private String descriptionRemarks = "";
	private boolean descriptionRemarksErrorFlg = true; //Default true: valid
	private String descriptionSummary = "";
	private boolean descriptionSummaryErrorFlg = true; //Default true: valid
	private String descriptionSentence = "";
	private boolean descriptionSentenceErrorFlg = true; //Default true: valid
	private String descriptionCaution = "";
	private boolean descriptionCautionErrorFlg = true; //Default true: valid
	private Date createdDate;

	private String createdUserId = "";

	private Date updatedDate;

	private String updatedUserId = "";

	private String rowId = "";

	private String crudFlg = "";

	//BOE #7363 Tran.Thanh : add mode replace for description
	private Integer replaceModeDescriptionSummary;

	private String descriptionSummaryFind;

	private Integer replaceModeDescriptionSentence;

	private String descriptionSentenceFind;

	private Integer replaceModeDescriptionCaution;

	private String descriptionCautionFind;

	private Integer replaceModeDescriptionRemarks;

	private String descriptionRemarksFind;
	//EOE #7363 Tran.Thanh : add mode replace for description

	/**
	 * @return the productId
	 */
	public Long getProductId() {
		return productId;
	}

	/**
	 * @param productId the productId to set
	 */
	public void setProductId(Long productId) {
		this.productId = productId;
	}

	/**
	 * @return the descriptionType
	 */
	public String getDescriptionType() {
		return descriptionType;
	}

	/**
	 * @param descriptionType the descriptionType to set
	 */
	public void setDescriptionType(String descriptionType) {
		this.descriptionType = descriptionType;
	}

	/**
	 * @return the descriptionRemarks
	 */
	public String getDescriptionRemarks() {
		return descriptionRemarks;
	}

	/**
	 * @param descriptionRemarks the descriptionRemarks to set
	 */
	public void setDescriptionRemarks(String descriptionRemarks) {
		this.descriptionRemarks = descriptionRemarks;
	}

	/**
	 * @return the descriptionSummary
	 */
	public String getDescriptionSummary() {
		return descriptionSummary;
	}

	/**
	 * @param descriptionSummary the descriptionSummary to set
	 */
	public void setDescriptionSummary(String descriptionSummary) {
		this.descriptionSummary = descriptionSummary;
	}

	/**
	 * @return the descriptionSentence
	 */
	public String getDescriptionSentence() {
		return descriptionSentence;
	}

	/**
	 * @param descriptionSentence the descriptionSentence to set
	 */
	public void setDescriptionSentence(String descriptionSentence) {
		this.descriptionSentence = descriptionSentence;
	}

	/**
	 * @return the descriptionCaution
	 */
	public String getDescriptionCaution() {
		return descriptionCaution;
	}

	/**
	 * @param descriptionCaution the descriptionCaution to set
	 */
	public void setDescriptionCaution(String descriptionCaution) {
		this.descriptionCaution = descriptionCaution;
	}

	/**
	 * @return the createdDate
	 */
	public Date getCreatedDate() {
		if (createdDate != null) {
            return (Date) createdDate.clone();
        }
        return null;
	}

	/**
	 * @param createdDate the createdDate to set
	 */
	public void setCreatedDate(Date createdDate) {
		if (createdDate != null) {
			this.createdDate = (Date) createdDate.clone();
        } else {
        	this.createdDate = null;
        }
	}

	/**
	 * @return the createdUserId
	 */
	public String getCreatedUserId() {
		return createdUserId;
	}

	/**
	 * @param createdUserId the createdUserId to set
	 */
	public void setCreatedUserId(String createdUserId) {
		this.createdUserId = createdUserId;
	}

	/**
	 * @return the updatedDate
	 */
	public Date getUpdatedDate() {
		if (updatedDate != null) {
            return (Date) updatedDate.clone();
        }
        return null;
	}

	/**
	 * @param updatedDate the updatedDate to set
	 */
	public void setUpdatedDate(Date updatedDate) {
		if (updatedDate != null) {
			this.updatedDate = (Date) updatedDate.clone();
        } else {
        	this.updatedDate = null;
        }
	}

	/**
	 * @return the updatedUserId
	 */
	public String getUpdatedUserId() {
		return updatedUserId;
	}

	/**
	 * @param updatedUserId the updatedUserId to set
	 */
	public void setUpdatedUserId(String updatedUserId) {
		this.updatedUserId = updatedUserId;
	}

	/**
	 * @return the rowId
	 */
	public String getRowId() {
		return rowId;
	}

	/**
	 * @param rowId the rowId to set
	 */
	public void setRowId(String rowId) {
		this.rowId = rowId;
	}

	/**
	 * @return the crudFlg
	 */
	public String getCrudFlg() {
		return crudFlg;
	}

	/**
	 * @param crudFlg the crudFlg to set
	 */
	public void setCrudFlg(String crudFlg) {
		this.crudFlg = crudFlg;
	}

	/**
	 * @return the replaceModeDescriptionSummary
	 */
	public Integer getReplaceModeDescriptionSummary() {
		return replaceModeDescriptionSummary;
	}

	/**
	 * @param replaceModeDescriptionSummary the replaceModeDescriptionSummary to set
	 */
	public void setReplaceModeDescriptionSummary(
			Integer replaceModeDescriptionSummary) {
		this.replaceModeDescriptionSummary = replaceModeDescriptionSummary;
	}

	/**
	 * @return the descriptionSummaryFind
	 */
	public String getDescriptionSummaryFind() {
		return descriptionSummaryFind;
	}

	/**
	 * @param descriptionSummaryFind the descriptionSummaryFind to set
	 */
	public void setDescriptionSummaryFind(String descriptionSummaryFind) {
		this.descriptionSummaryFind = descriptionSummaryFind;
	}

	/**
	 * @return the replaceModeDescriptionSentence
	 */
	public Integer getReplaceModeDescriptionSentence() {
		return replaceModeDescriptionSentence;
	}

	/**
	 * @param replaceModeDescriptionSentence the replaceModeDescriptionSentence to set
	 */
	public void setReplaceModeDescriptionSentence(
			Integer replaceModeDescriptionSentence) {
		this.replaceModeDescriptionSentence = replaceModeDescriptionSentence;
	}

	/**
	 * @return the descriptionSentenceFind
	 */
	public String getDescriptionSentenceFind() {
		return descriptionSentenceFind;
	}

	/**
	 * @param descriptionSentenceFind the descriptionSentenceFind to set
	 */
	public void setDescriptionSentenceFind(String descriptionSentenceFind) {
		this.descriptionSentenceFind = descriptionSentenceFind;
	}

	/**
	 * @return the replaceModeDescriptionCaution
	 */
	public Integer getReplaceModeDescriptionCaution() {
		return replaceModeDescriptionCaution;
	}

	/**
	 * @param replaceModeDescriptionCaution the replaceModeDescriptionCaution to set
	 */
	public void setReplaceModeDescriptionCaution(
			Integer replaceModeDescriptionCaution) {
		this.replaceModeDescriptionCaution = replaceModeDescriptionCaution;
	}

	/**
	 * @return the descriptionCautionFind
	 */
	public String getDescriptionCautionFind() {
		return descriptionCautionFind;
	}

	/**
	 * @param descriptionCautionFind the descriptionCautionFind to set
	 */
	public void setDescriptionCautionFind(String descriptionCautionFind) {
		this.descriptionCautionFind = descriptionCautionFind;
	}

	/**
	 * @return the replaceModeDescriptionRemarks
	 */
	public Integer getReplaceModeDescriptionRemarks() {
		return replaceModeDescriptionRemarks;
	}

	/**
	 * @param replaceModeDescriptionRemarks the replaceModeDescriptionRemarks to set
	 */
	public void setReplaceModeDescriptionRemarks(
			Integer replaceModeDescriptionRemarks) {
		this.replaceModeDescriptionRemarks = replaceModeDescriptionRemarks;
	}

	/**
	 * @return the descriptionRemarksFind
	 */
	public String getDescriptionRemarksFind() {
		return descriptionRemarksFind;
	}

	/**
	 * @param descriptionRemarksFind the descriptionRemarksFind to set
	 */
	public void setDescriptionRemarksFind(String descriptionRemarksFind) {
		this.descriptionRemarksFind = descriptionRemarksFind;
	}

    /**
     * @return the descriptionRemarksErrorFlg
     */
    public boolean isDescriptionRemarksErrorFlg() {
        return descriptionRemarksErrorFlg;
    }

    /**
     * @param descriptionRemarksErrorFlg the descriptionRemarksErrorFlg to set
     */
    public void setDescriptionRemarksErrorFlg(boolean descriptionRemarksErrorFlg) {
        this.descriptionRemarksErrorFlg = descriptionRemarksErrorFlg;
    }

    /**
     * @return the descriptionSummaryErrorFlg
     */
    public boolean isDescriptionSummaryErrorFlg() {
        return descriptionSummaryErrorFlg;
    }

    /**
     * @param descriptionSummaryErrorFlg the descriptionSummaryErrorFlg to set
     */
    public void setDescriptionSummaryErrorFlg(boolean descriptionSummaryErrorFlg) {
        this.descriptionSummaryErrorFlg = descriptionSummaryErrorFlg;
    }

    /**
     * @return the descriptionSentenceErrorFlg
     */
    public boolean isDescriptionSentenceErrorFlg() {
        return descriptionSentenceErrorFlg;
    }

    /**
     * @param descriptionSentenceErrorFlg the descriptionSentenceErrorFlg to set
     */
    public void setDescriptionSentenceErrorFlg(boolean descriptionSentenceErrorFlg) {
        this.descriptionSentenceErrorFlg = descriptionSentenceErrorFlg;
    }

    /**
     * @return the descriptionCautionErrorFlg
     */
    public boolean isDescriptionCautionErrorFlg() {
        return descriptionCautionErrorFlg;
    }

    /**
     * @param descriptionCautionErrorFlg the descriptionCautionErrorFlg to set
     */
    public void setDescriptionCautionErrorFlg(boolean descriptionCautionErrorFlg) {
        this.descriptionCautionErrorFlg = descriptionCautionErrorFlg;
    }

}
