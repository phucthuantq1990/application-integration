/************************************************************************
 * File Name    ： EnumAttributeType.java
 * Author       ： Nguyen.Chuong
 * Version      ： 1.0.0
 * Date Created ： 2014/01/15
 * Description  ： Enum content all value of attribute type.
 *
 ***********************************************************************/
package net.webike.RcProductFactory.entity;

import java.util.HashMap;

/**
 * Enum of attribute Type.
 */
public enum EnumAttributeType {

    T1("Integer", "整数"),
    T2("Float", "小数点"),
    T3("String", "文字列"),
    T4("Group", "グループ"),
    T5("Flag", "フラグ");

    private String key;

    private String value;

    /**---------------------------------------------------------------------------
     * Private constructor for EnumAttributeType.
     * @param key for get value
     * @param value of EnumAttributeType
     */
    private EnumAttributeType(final String key, final String value) {
        this.setKey(key);
        this.setValue(value);
    }

    /************************************************************************
     * <b>Description:</b><br>
     *  get all value of enum type.
     *
     * @author		Nguyen.Chuong
     * @date		Jan 15, 2014
     * @return		HashMap<String,String>
     ************************************************************************/
    public static HashMap<String, String> getHashMapAllType() {
        HashMap<String, String> tm = new HashMap<String, String>();

        for (EnumAttributeType value : values()) {
            tm.put(value.getKey(), value.getValue());
        }

        return tm;
    }

    /************************************************************************
     * <b>Description:</b><br>
     *  get value of enum type by key.
     *
     * @author		Nguyen.Chuong
     * @date		Jan 15, 2014
     * @param       key to get value. Can be: String, integet, group,...
     * @return		String
     ************************************************************************/
    public static String getTypeValueByKey(String key) {
        for (EnumAttributeType valueEnum : values()) {
            if (key.equals(valueEnum.getKey())) {
                return valueEnum.getValue();
            }
        }
        return null;
    }

    /************************************************************************
     * <b>Description:</b><br>
     *  get key by value of enum.
     *
     * @author		Nguyen.Chuong
     * @date		Jan 15, 2014
     * @param       value to get key. Can be: 整数, 小数点,...
     * @return		String
     ************************************************************************/
    public static String getTypeKeyByValue(String value) {
        for (EnumAttributeType valueEnum : values()) {
            if (value.equals(valueEnum.getValue())) {
                return valueEnum.getKey();
            }
        }
        return null;
    }

    /**
     * @return the key
     */
    public String getKey() {
        return key;
    }

    /**
     * @param key the key to set
     */
    public void setKey(String key) {
        this.key = key;
    }

    /**
     * @return the value
     */
    public String getValue() {
        return value;
    }

    /**
     * @param value the value to set
     */
    public void setValue(String value) {
        this.value = value;
    }

}
