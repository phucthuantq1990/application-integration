/************************************************************************
 * file name	： EntTreeView.java
 * author		： Tran.Thanh
 * version		： 1.0.0
 * date created	： 30 Oct 2013
 * date updated	： 30 Oct 2013
 * description	： Date of TreeView Category.
 *
 ***********************************************************************/
package net.webike.RcProductFactory.entity;

import java.util.List;

/**
 * EntTreeView.
 */
public class EntTreeView {

	private String id = "";

	private String data = "";

	private EntTreeView attr = null;

	private List<EntTreeView> children = null;

	/**
	 * @return the data
	 */
	public String getData() {
		return data;
	}

	/**
	 * @param data the data to set
	 */
	public void setData(String data) {
		this.data = data;
	}

	/**
	 * @return the attr
	 */
	public EntTreeView getAttr() {
		return attr;
	}

	/**
	 * @param attr the attr to set
	 */
	public void setAttr(EntTreeView attr) {
		this.attr = attr;
	}

	/**
	 * @return the children
	 */
	public List<EntTreeView> getChildren() {
		return children;
	}

	/**
	 * @param children the children to set
	 */
	public void setChildren(List<EntTreeView> children) {
		this.children = children;
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

}
