/************************************************************************
 * file name	： EntReplaceElement.java
 * author		： Tran.Thanh
 * version		： 1.0.0
 * date created	： 2014/04/21
 * date updated	： 2014/04/21
 * description	： Ent contains data for popup block 15
 *
 ***********************************************************************/
package net.webike.RcProductFactory.entity;

/**
 * EntReplaceElement.
 */
public class EntReplaceElement {
    private String field;
    private String changeType;
    private String value;
    private String imageMode;
    private String findThumb;
    private String findDetail;
    private String imageThumbPath;
    private String imageDetailPath;
    private String checkFlg;
    private String findValue;
	/**
	 * @return the findValue
	 */
	public String getFindValue() {
		return findValue;
	}
	/**
	 * @param findValue the findValue to set
	 */
	public void setFindValue(String findValue) {
		this.findValue = findValue;
	}
	/**
	 * @return the field
	 */
	public String getField() {
		return field;
	}
	/**
	 * @param field the field to set
	 */
	public void setField(String field) {
		this.field = field;
	}
	/**
	 * @return the changeType
	 */
	public String getChangeType() {
		return changeType;
	}
	/**
	 * @param changeType the changeType to set
	 */
	public void setChangeType(String changeType) {
		this.changeType = changeType;
	}
	/**
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	/**
	 * @param value the value to set
	 */
	public void setValue(String value) {
		this.value = value;
	}
	/**
	 * @return the imageMode
	 */
	public String getImageMode() {
		return imageMode;
	}
	/**
	 * @param imageMode the imageMode to set
	 */
	public void setImageMode(String imageMode) {
		this.imageMode = imageMode;
	}
	/**
	 * @return the findThumb
	 */
	public String getFindThumb() {
		return findThumb;
	}
	/**
	 * @param findThumb the findThumb to set
	 */
	public void setFindThumb(String findThumb) {
		this.findThumb = findThumb;
	}
	/**
	 * @return the findDetail
	 */
	public String getFindDetail() {
		return findDetail;
	}
	/**
	 * @param findDetail the findDetail to set
	 */
	public void setFindDetail(String findDetail) {
		this.findDetail = findDetail;
	}
	/**
	 * @return the checkFlg
	 */
	public String getCheckFlg() {
		return checkFlg;
	}
	/**
	 * @param checkFlg the checkFlg to set
	 */
	public void setCheckFlg(String checkFlg) {
		this.checkFlg = checkFlg;
	}
	/**
	 * @return the imageThumbPath
	 */
	public String getImageThumbPath() {
		return imageThumbPath;
	}
	/**
	 * @param imageThumbPath the imageThumbPath to set
	 */
	public void setImageThumbPath(String imageThumbPath) {
		this.imageThumbPath = imageThumbPath;
	}
	/**
	 * @return the imageDetailPath
	 */
	public String getImageDetailPath() {
		return imageDetailPath;
	}
	/**
	 * @param imageDetailPath the imageDetailPath to set
	 */
	public void setImageDetailPath(String imageDetailPath) {
		this.imageDetailPath = imageDetailPath;
	}

}
