/************************************************************************
 * File Name    ： TblFactoryMatterMapper.java
 * Author       ： hoang.ho
 * Version      ： 1.0.0
 * Date Created ： 2014/03/03
 * Date Updated ： 2014/03/03
 * Description  ： Contain properties of TblFactoryMatterMapper.
 *
 ***********************************************************************/
package net.webike.RcProductFactory.entity;

import java.util.Date;

/**
 * Contain properties of TblFactoryMatterMapper.
 * @author Hoang.Ho
 * @since 2014/03/03
 */
public class EntTblFactoryMatter {
    /** matter_no. */
    private long matterNo;
    /** matter_factory_error_count_general. */
    private Integer matterFactoryErrorCountGeneral;
    /** matter_factory_error_count_category. */
    private Integer matterFactoryErrorCountCategory;
    /** matter_factory_error_count_model. */
    private Integer matterFactoryErrorCountModel;
    /** matter_factory_error_count_attribute. */
    private Integer matterFactoryErrorCountAttribute;
    /** matter_del_flg. */
    private Integer matterDelFlg;
    /** matter_factory_error_count_general for update. */
    private Integer matterFactoryErrorCountGeneralAdd;
    /** matter_factory_error_count_category for update. */
    private Integer matterFactoryErrorCountCategoryAdd;
    /** matter_factory_error_count_model for update. */
    private Integer matterFactoryErrorCountModelAdd;
    /** matter_factory_error_count_attribute for update. */
    private Integer matterFactoryErrorCountAttributeAdd;
    /** created_on. */
    private Date createdOn;
    /** created_user_id. */
    private String createdUserId;
    /** updated_on. */
    private Date updatedOn;
    /** updated_user_id. */
    private String updatedUserId;
    /**
     * @return the matterNo
     */
    public long getMatterNo() {
        return matterNo;
    }
    /**
     * @param matterNo the matterNo to set
     */
    public void setMatterNo(long matterNo) {
        this.matterNo = matterNo;
    }
    /**
     * @return the matterFactoryErrorCountGeneral
     */
    public Integer getMatterFactoryErrorCountGeneral() {
        return matterFactoryErrorCountGeneral;
    }
    /**
     * @param matterFactoryErrorCountGeneral the matterFactoryErrorCountGeneral to set
     */
    public void setMatterFactoryErrorCountGeneral(Integer matterFactoryErrorCountGeneral) {
        this.matterFactoryErrorCountGeneral = matterFactoryErrorCountGeneral;
    }
    /**
     * @return the matterFactoryErrorCountCategory
     */
    public Integer getMatterFactoryErrorCountCategory() {
        return matterFactoryErrorCountCategory;
    }
    /**
     * @param matterFactoryErrorCountCategory the matterFactoryErrorCountCategory to set
     */
    public void setMatterFactoryErrorCountCategory(Integer matterFactoryErrorCountCategory) {
        this.matterFactoryErrorCountCategory = matterFactoryErrorCountCategory;
    }
    /**
     * @return the matterFactoryErrorCountModel
     */
    public Integer getMatterFactoryErrorCountModel() {
        return matterFactoryErrorCountModel;
    }
    /**
     * @param matterFactoryErrorCountModel the matterFactoryErrorCountModel to set
     */
    public void setMatterFactoryErrorCountModel(Integer matterFactoryErrorCountModel) {
        this.matterFactoryErrorCountModel = matterFactoryErrorCountModel;
    }
    /**
     * @return the matterFactoryErrorCountAttribute
     */
    public Integer getMatterFactoryErrorCountAttribute() {
        return matterFactoryErrorCountAttribute;
    }
    /**
     * @param matterFactoryErrorCountAttribute the matterFactoryErrorCountAttribute to set
     */
    public void setMatterFactoryErrorCountAttribute(Integer matterFactoryErrorCountAttribute) {
        this.matterFactoryErrorCountAttribute = matterFactoryErrorCountAttribute;
    }
    /**
     * @return the matterDelFlg
     */
    public Integer getMatterDelFlg() {
        return matterDelFlg;
    }
    /**
     * @param matterDelFlg the matterDelFlg to set
     */
    public void setMatterDelFlg(Integer matterDelFlg) {
        this.matterDelFlg = matterDelFlg;
    }
    /**
     * @return the matterFactoryErrorCountModelAdd
     */
    public Integer getMatterFactoryErrorCountModelAdd() {
        return matterFactoryErrorCountModelAdd;
    }
    /**
     * @param matterFactoryErrorCountModelAdd the matterFactoryErrorCountModelAdd to set
     */
    public void setMatterFactoryErrorCountModelAdd(Integer matterFactoryErrorCountModelAdd) {
        this.matterFactoryErrorCountModelAdd = matterFactoryErrorCountModelAdd;
    }
    /**
     * @return the createdOn
     */
    public Date getCreatedOn() {
        if (this.createdOn != null) {
            return (Date) createdOn.clone();
        }
        return null;
    }
    /**
     * @param createdOn the createdOn to set
     */
    public void setCreatedOn(Date createdOn) {
        if (createdOn != null) {
            this.createdOn = (Date) createdOn.clone();
        } else {
            this.createdOn = null;
        }
    }
    /**
     * @return the createdUserId
     */
    public String getCreatedUserId() {
        return createdUserId;
    }
    /**
     * @param createdUserId the createdUserId to set
     */
    public void setCreatedUserId(String createdUserId) {
        this.createdUserId = createdUserId;
    }
    /**
     * @return the updatedOn
     */
    public Date getUpdatedOn() {
        if (this.updatedOn != null) {
            return (Date) updatedOn.clone();
        }
        return null;
    }
    /**
     * @param updatedOn the updatedOn to set
     */
    public void setUpdatedOn(Date updatedOn) {
        if (updatedOn != null) {
            this.updatedOn = (Date) updatedOn.clone();
        } else {
            this.updatedOn = null;
        }
    }
    /**
     * @return the updatedUserId
     */
    public String getUpdatedUserId() {
        return updatedUserId;
    }
    /**
     * @param updatedUserId the updatedUserId to set
     */
    public void setUpdatedUserId(String updatedUserId) {
        this.updatedUserId = updatedUserId;
    }
    /**
     * @return the matterFactoryErrorCountGeneralAdd
     */
    public Integer getMatterFactoryErrorCountGeneralAdd() {
        return matterFactoryErrorCountGeneralAdd;
    }
    /**
     * @param matterFactoryErrorCountGeneralAdd the matterFactoryErrorCountGeneralAdd to set
     */
    public void setMatterFactoryErrorCountGeneralAdd(Integer matterFactoryErrorCountGeneralAdd) {
        this.matterFactoryErrorCountGeneralAdd = matterFactoryErrorCountGeneralAdd;
    }
    /**
     * @return the matterFactoryErrorCountCategoryAdd
     */
    public Integer getMatterFactoryErrorCountCategoryAdd() {
        return matterFactoryErrorCountCategoryAdd;
    }
    /**
     * @param matterFactoryErrorCountCategoryAdd the matterFactoryErrorCountCategoryAdd to set
     */
    public void setMatterFactoryErrorCountCategoryAdd(Integer matterFactoryErrorCountCategoryAdd) {
        this.matterFactoryErrorCountCategoryAdd = matterFactoryErrorCountCategoryAdd;
    }
    /**
     * @return the matterFactoryErrorCountAttributeAdd
     */
    public Integer getMatterFactoryErrorCountAttributeAdd() {
        return matterFactoryErrorCountAttributeAdd;
    }
    /**
     * @param matterFactoryErrorCountAttributeAdd the matterFactoryErrorCountAttributeAdd to set
     */
    public void setMatterFactoryErrorCountAttributeAdd(Integer matterFactoryErrorCountAttributeAdd) {
        this.matterFactoryErrorCountAttributeAdd = matterFactoryErrorCountAttributeAdd;
    }
}
