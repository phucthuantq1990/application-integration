/************************************************************************
 * file name	： EntTblProductGuestInput.java
 * author		： Nguyen.Chuong
 * version		： 1.0.0
 * date created	： 2014/02/19
 * date updated	： 2014/02/19
 * description	： Entity EntTblProductGuestInput to contain data of rc_syouhin.tbl_product_guest_input.
 *
 ***********************************************************************/
package net.webike.RcProductFactory.entity;


/**
 * EntTblProductGuestInput.
 */
public class EntTblProductGuestInput {

	private Long syouhinSysCode;
	private String productGuestInputTitle;
	private String productGuestInputDescription;
    /**
     * @return the syouhinSysCode
     */
    public Long getSyouhinSysCode() {
        return syouhinSysCode;
    }
    /**
     * @param syouhinSysCode the syouhinSysCode to set
     */
    public void setSyouhinSysCode(Long syouhinSysCode) {
        this.syouhinSysCode = syouhinSysCode;
    }
    /**
     * @return the productGuestInputTitle
     */
    public String getProductGuestInputTitle() {
        return productGuestInputTitle;
    }
    /**
     * @param productGuestInputTitle the productGuestInputTitle to set
     */
    public void setProductGuestInputTitle(String productGuestInputTitle) {
        this.productGuestInputTitle = productGuestInputTitle;
    }
    /**
     * @return the productGuestInputDescription
     */
    public String getProductGuestInputDescription() {
        return productGuestInputDescription;
    }
    /**
     * @param productGuestInputDescription the productGuestInputDescription to set
     */
    public void setProductGuestInputDescription(String productGuestInputDescription) {
        this.productGuestInputDescription = productGuestInputDescription;
    }

}
