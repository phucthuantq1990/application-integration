/************************************************************************
 * file name	： EntMstbunruikaisoukaisou.java
 * author		： Tran.Thanh
 * version		： 1.0.0
 * date created	： 23 Oct 2013
 * date updated	： 23 Oct 2013
 * description	： Data from Database rc_webike table mst_bunruikaisoukaisou
 *
 ***********************************************************************/
package net.webike.RcProductFactory.entity;

import java.util.Date;

/**
 * EntMstbunruikaisoukaisou.
 */
public class EntMstBunruikaisou {

	private String bunruikaisouOyaCode = "";

	private String bunruikaisouKoCode = "";

	private String bunruikaisouFirstFlg = "";

	private String bunruikaisouSort = "";

	private String bunruikaisouCtgSort = "";

	private String bunruikaisouTitle = "";

	private String bunruikaisouKeyword = "";

	private String bunruikaisouDescription = "";

	private String bunruikaisouCssCode = "";

	private String bunruikaisouHeader = "";

	private String bunruikaisouHeaderCode = "";

	private String bunruikaisouBodyCode = "";

	private String bunruikaisouPageType = "";

	private String bunruikaisouColor = "";

	private String bunruikaisouListHyoji = "";

	private Date bunruikaisouKousinDate;

	/**
	 * @return the bunruikaisouOyaCode
	 */
	public String getBunruikaisouOyaCode() {
		return bunruikaisouOyaCode;
	}

	/**
	 * @param bunruikaisouOyaCode the bunruikaisouOyaCode to set
	 */
	public void setBunruikaisouOyaCode(String bunruikaisouOyaCode) {
		this.bunruikaisouOyaCode = bunruikaisouOyaCode;
	}

	/**
	 * @return the bunruikaisouKoCode
	 */
	public String getBunruikaisouKoCode() {
		return bunruikaisouKoCode;
	}

	/**
	 * @param bunruikaisouKoCode the bunruikaisouKoCode to set
	 */
	public void setBunruikaisouKoCode(String bunruikaisouKoCode) {
		this.bunruikaisouKoCode = bunruikaisouKoCode;
	}

	/**
	 * @return the bunruikaisouFirstFlg
	 */
	public String getBunruikaisouFirstFlg() {
		return bunruikaisouFirstFlg;
	}

	/**
	 * @param bunruikaisouFirstFlg the bunruikaisouFirstFlg to set
	 */
	public void setBunruikaisouFirstFlg(String bunruikaisouFirstFlg) {
		this.bunruikaisouFirstFlg = bunruikaisouFirstFlg;
	}

	/**
	 * @return the bunruikaisouSort
	 */
	public String getBunruikaisouSort() {
		return bunruikaisouSort;
	}

	/**
	 * @param bunruikaisouSort the bunruikaisouSort to set
	 */
	public void setBunruikaisouSort(String bunruikaisouSort) {
		this.bunruikaisouSort = bunruikaisouSort;
	}

	/**
	 * @return the bunruikaisouCtgSort
	 */
	public String getBunruikaisouCtgSort() {
		return bunruikaisouCtgSort;
	}

	/**
	 * @param bunruikaisouCtgSort the bunruikaisouCtgSort to set
	 */
	public void setBunruikaisouCtgSort(String bunruikaisouCtgSort) {
		this.bunruikaisouCtgSort = bunruikaisouCtgSort;
	}

	/**
	 * @return the bunruikaisouTitle
	 */
	public String getBunruikaisouTitle() {
		return bunruikaisouTitle;
	}

	/**
	 * @param bunruikaisouTitle the bunruikaisouTitle to set
	 */
	public void setBunruikaisouTitle(String bunruikaisouTitle) {
		this.bunruikaisouTitle = bunruikaisouTitle;
	}

	/**
	 * @return the bunruikaisouKeyword
	 */
	public String getBunruikaisouKeyword() {
		return bunruikaisouKeyword;
	}

	/**
	 * @param bunruikaisouKeyword the bunruikaisouKeyword to set
	 */
	public void setBunruikaisouKeyword(String bunruikaisouKeyword) {
		this.bunruikaisouKeyword = bunruikaisouKeyword;
	}

	/**
	 * @return the bunruikaisouDescription
	 */
	public String getBunruikaisouDescription() {
		return bunruikaisouDescription;
	}

	/**
	 * @param bunruikaisouDescription the bunruikaisouDescription to set
	 */
	public void setBunruikaisouDescription(String bunruikaisouDescription) {
		this.bunruikaisouDescription = bunruikaisouDescription;
	}

	/**
	 * @return the bunruikaisouCssCode
	 */
	public String getBunruikaisouCssCode() {
		return bunruikaisouCssCode;
	}

	/**
	 * @param bunruikaisouCssCode the bunruikaisouCssCode to set
	 */
	public void setBunruikaisouCssCode(String bunruikaisouCssCode) {
		this.bunruikaisouCssCode = bunruikaisouCssCode;
	}

	/**
	 * @return the bunruikaisouHeader
	 */
	public String getBunruikaisouHeader() {
		return bunruikaisouHeader;
	}

	/**
	 * @param bunruikaisouHeader the bunruikaisouHeader to set
	 */
	public void setBunruikaisouHeader(String bunruikaisouHeader) {
		this.bunruikaisouHeader = bunruikaisouHeader;
	}

	/**
	 * @return the bunruikaisouHeaderCode
	 */
	public String getBunruikaisouHeaderCode() {
		return bunruikaisouHeaderCode;
	}

	/**
	 * @param bunruikaisouHeaderCode the bunruikaisouHeaderCode to set
	 */
	public void setBunruikaisouHeaderCode(String bunruikaisouHeaderCode) {
		this.bunruikaisouHeaderCode = bunruikaisouHeaderCode;
	}

	/**
	 * @return the bunruikaisouBodyCode
	 */
	public String getBunruikaisouBodyCode() {
		return bunruikaisouBodyCode;
	}

	/**
	 * @param bunruikaisouBodyCode the bunruikaisouBodyCode to set
	 */
	public void setBunruikaisouBodyCode(String bunruikaisouBodyCode) {
		this.bunruikaisouBodyCode = bunruikaisouBodyCode;
	}

	/**
	 * @return the bunruikaisouPageType
	 */
	public String getBunruikaisouPageType() {
		return bunruikaisouPageType;
	}

	/**
	 * @param bunruikaisouPageType the bunruikaisouPageType to set
	 */
	public void setBunruikaisouPageType(String bunruikaisouPageType) {
		this.bunruikaisouPageType = bunruikaisouPageType;
	}

	/**
	 * @return the bunruikaisouColor
	 */
	public String getBunruikaisouColor() {
		return bunruikaisouColor;
	}

	/**
	 * @param bunruikaisouColor the bunruikaisouColor to set
	 */
	public void setBunruikaisouColor(String bunruikaisouColor) {
		this.bunruikaisouColor = bunruikaisouColor;
	}

	/**
	 * @return the bunruikaisouListHyoji
	 */
	public String getBunruikaisouListHyoji() {
		return bunruikaisouListHyoji;
	}

	/**
	 * @param bunruikaisouListHyoji the bunruikaisouListHyoji to set
	 */
	public void setBunruikaisouListHyoji(String bunruikaisouListHyoji) {
		this.bunruikaisouListHyoji = bunruikaisouListHyoji;
	}

	/**
	 * @return the bunruikaisouKousinDate
	 */
	public Date getBunruikaisouKousinDate() {
		if (bunruikaisouKousinDate == null) {
            return null;
        }
        return new Date(bunruikaisouKousinDate.getTime());
	}

	/**
	 * @param bunruikaisouKousinDate the bunruikaisouKousinDate to set
	 */
	public void setBunruikaisouKousinDate(Date bunruikaisouKousinDate) {
		if (bunruikaisouKousinDate == null) {
            this.bunruikaisouKousinDate = null;
            return;
          }
        this.bunruikaisouKousinDate = new Date(bunruikaisouKousinDate.getTime());
	}


}
