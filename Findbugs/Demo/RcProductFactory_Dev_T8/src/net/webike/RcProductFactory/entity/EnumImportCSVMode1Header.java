/************************************************************************
 * File Name    ： EnumImportCSVMode1Header.java
 * Author       ： Nguyen.Chuong
 * Version      ： 1.0.0
 * Date Created ： 2014/03/05
 * Description  ： Enum content all value of Header CSV import mode 1.
 *
 ***********************************************************************/
package net.webike.RcProductFactory.entity;

/**
 * Enum of attribute Type.
 */
public enum EnumImportCSVMode1Header {

	//BOE @rcv!Luong.Dai 2014/07/09 #9811: Fix enum to csv_v2
//    H1("importMode",                    	1,		"更新モード", 		"importMode"),
//    H2("productId",                    		2,      "識別番号",		"sProductId"),
//    H3("productSyouhinSysCode",         	3,      "システム品番",		"sProductSyouhinSysCode"),
//    H4("productBrandCode",              	4,      "ブランドコード",		"sProductBrandCode"),
//    H5("productBrandName",              	5,      "ブランド名",		"productBrandName"),
//    H6("productCategoryCode",               6,      "品目コード",		"sProductCategoryCode"),
//    H7("productCategoryName",               7,      "品目名",		"productCategoryName"),
//    H8("productName",                   	8,      "製品名",		"productName"),
//    H9("productCode",              			9,      "メーカー品番",		"productCode"),
//    H10("productWebikeCodeFlg",             10,     "品番無しフラグ",	"sProductWebikeCodeFlg"),
//    H11("productEanCode",          			11,     "JAN",			"productEanCode"),
//    H12("productProperPrice",               12,     "定価",			"sProductProperPrice"),
//    H13("supplierPricePrice",               13,     "仕切り",			"sSupplierPricePrice"),
//    H14("productGroupCode",                 14,     "グループコード",	"sProductGroupCode"),
//    H15("productSupplierReleaseDate",       15,     "発売日",		"productSupplierReleaseDate"),
//    H16("productOpenPriceFlg",              16,     "オープン価格",		"sProductOpenPriceFlg"),
//    H17("productProperSellingFlg",          17,     "定価販売",		"sProductProperSellingFlg"),
//    H18("productOrderProductFlg",    		18,     "受注生産",		"sProductOrderProductFlg"),
//    H19("productNoReturnableFlg",           19,     "返品不可",		"sProductNoReturnableFlg"),
//    H20("productAmbiguousImageFlg",         20,     "参考画像",		"sProductAmbiguousImageFlg"),
//    H21("descriptionSummary",               21,     "要点",			"descriptionSummary"),
//    H22("descriptionRemarks",               22,     "備考",			"descriptionRemarks"),
//    H23("descriptionCaution",               23,     "注意",			"descriptionCaution"),
//    H24("descriptionSentence",              24,     "説明",			"descriptionSentence"),
//    H25("siireCode1",                		25,     "仕入先コード1",	"siireCode1"),
//    H26("siireName1",                		26,     "仕入先名1",		"siireName1"),
//    H27("noukiCode1",            			27,     "納期コード1",		"noukiCode1"),
//    H28("noukiName1",            			28,     "納期名1",		"noukiName1"),
//    H29("siireCode2",                		29,     "仕入先コード2",	"siireCode2"),
//    H30("siireName2",                		30,     "仕入先名2",		"siireName2"),
//    H31("noukiCode2",            			31,     "納期コード2",		"noukiCode2"),
//    H32("noukiName2",            			32,     "納期名2",		"noukiName2"),
//    H33("compatibleModel",              	33,     "適合車種",		"compatibleModel"),
//    H34("productImageThumbnailPath1",    	34,     "サムネイル1",		"productImageThumbnailPath1"),
//    H35("productImageDetailPath1",       	35,     "詳細画像1",		"productImageDetailPath1"),
//    H36("productImageThumbnailPath2",    	36,     "サムネイル2",		"productImageThumbnailPath2"),
//    H37("productImageDetailPath2",       	37,     "詳細画像2",		"productImageDetailPath2"),
//    H38("productImageThumbnailPath3",    	38,     "サムネイル3",		"productImageThumbnailPath3"),
//    H39("productImageDetailPath3",       	39,     "詳細画像3",		"productImageDetailPath3"),
//    H40("productImageThumbnailPath4",    	40,     "サムネイル4",		"productImageThumbnailPath4"),
//    H41("productImageDetailPath4",       	41,     "詳細画像4",		"productImageDetailPath4"),
//    H42("productImageThumbnailPath5",    	42,     "サムネイル5",		"productImageThumbnailPath5"),
//    H43("productImageDetailPath5",       	43,     "詳細画像5",		"productImageDetailPath5"),
//    H44("productImageThumbnailPath6",    	44,     "サムネイル6",		"productImageThumbnailPath6"),
//    H45("productImageDetailPath6",       	45,     "詳細画像6",		"productImageDetailPath6"),
//    H46("productImageThumbnailPath7",    	46,     "サムネイル7",		"productImageThumbnailPath7"),
//    H47("productImageDetailPath7",       	47,     "詳細画像7",		"productImageDetailPath7"),
//    H48("productImageThumbnailPath8",    	48,     "サムネイル8",		"productImageThumbnailPath8"),
//    H49("productImageDetailPath8",       	49,     "詳細画像8",		"productImageDetailPath8"),
//    H50("productImageThumbnailPath9",    	50,     "サムネイル9",		"productImageThumbnailPath9"),
//    H51("productImageDetailPath9",       	51,     "詳細画像9",		"productImageDetailPath9"),
//    H52("productImageThumbnailPath10",   	52,     "サムネイル10",	"productImageThumbnailPath10"),
//    H53("productImageDetailPath10",      	53,     "詳細画像10",		"productImageDetailPath10"),
//    H54("selectCode1", 						54, 	"選択肢コード1",	"sSelectCode1"),
//    H55("selectName1", 						55, 	"選択肢名1",		"selectName1"),
//    H56("selectCode2", 						56, 	"選択肢コード2",	"sSelectCode2"),
//    H57("selectName2", 						57, 	"選択肢名2",		"selectName2"),
//    H58("selectCode3", 						58, 	"選択肢コード3",	"sSelectCode3"),
//    H59("selectName3", 						59, 	"選択肢名3",		"selectName3"),
//    H60("selectCode4", 						60, 	"選択肢コード4",	"sSelectCode4"),
//    H61("selectName4", 						61, 	"選択肢名4",		"selectName4"),
//    H62("selectCode5", 						62, 	"選択肢コード5",	"sSelectCode5"),
//    H63("selectName5", 						63, 	"選択肢名5",		"selectName5"),
//    H64("selectCode6", 						64, 	"選択肢コード6",	"sSelectCode6"),
//    H65("selectName6", 						65, 	"選択肢名6",		"selectName6"),
//    H66("selectCode7", 						66, 	"選択肢コード7",	"sSelectCode7"),
//    H67("selectName7", 						67, 	"選択肢名7",		"selectName7"),
//    H68("selectCode8", 						68, 	"選択肢コード8",	"sSelectCode8"),
//    H69("selectName8", 						69, 	"選択肢名8",		"selectName8"),
//    H70("selectCode9", 						70, 	"選択肢コード9",	"sSelectCode9"),
//    H71("selectName9", 						71, 	"選択肢名9",		"selectName9"),
//    H72("selectCode10", 					72, 	"選択肢コード10",	"sSelectCode10"),
//    H73("selectName10", 					73, 	"選択肢名10",		"selectName10"),
//    H74("customersConfirmationItem",     	74,     "お客様確認項目",	"customersConfirmationItem"),
//    H75("link",                         	75,     "リンク",			"link"),
//    H76("animation",                    	76,     "動画",			"animation"),		//Video
//    H77("attributeName",                	77,     "属性名",		"attributeName"),
//    H78("attributeValue",               	78,     "管理名",		"attributeValue"),
//    H79("attributeDisplay",             	79,     "表示名",		"attributeDisplay"),
//    H80("fitModelMaker",             		80,     "メーカー",		"fitModelMaker"),
//    H81("fitModelModel",             		81,     "車種",			"fitModelModel"),
//    /* BOE #9410 Tran.Thanh 2014/06/30 : Change 型式 to 年式・型式 */
//    //H82("fitModelStyle",             		82,     "型式",			"fitModelStyle");
//    H82("fitModelStyle",             		82,     "年式・型式",		"fitModelStyle");
//    /* EOE #9410 Tran.Thanh 2014/06/30 : Change 型式 to 年式・型式 */
	H1("errorAllMess", 						1, 		"全体エラー", 			"errorAllMess"),
	H2("importMode", 						2, 		"更新モード", 			"importMode"),
	H3("importModeError", 					3, 		"更新モード_エラー",		"importModeError"),
	H4("productId", 						4, 		"識別番号", 			"sProductId"),
	H5("productIdError", 					5, 		"識別番号_エラー", 		"productIdError"),
	H6("productSyouhinSysCode", 			6, 		"システム品番", 		"sProductSyouhinSysCode"),
	H7("productSyouhinSysCodeError", 		7, 		"システム品番_エラー",	"productSyouhinSysCodeError"),
	H8("productBrandCode", 					8, 		"ブランドコード", 		"sProductBrandCode"),
	H9("productBrandCodeError", 			9, 		"ブランドコード_エラー", 	"productBrandCodeError"),
	H10("productBrandName", 				10, 	"ブランド名", 			"productBrandName"),
	H11("productCategoryCode", 				11, 	"品目コード", 			"sProductCategoryCode"),
	H12("productCategoryCodeError", 		12, 	"品目コード_エラー", 	"productCategoryCodeError"),
	H13("productCategoryName", 				13, 	"品目名", 			"productCategoryName"),
	H14("productName", 						14, 	"製品名", 			"productName"),
	H15("productNameError", 				15, 	"製品名_エラー", 		"productNameError"),
	H16("productCode", 						16, 	"メーカー品番", 		"productCode"),
	H17("productCodeError", 				17, 	"メーカー品番_エラー", 	"productCodeError"),
	H18("productWebikeCodeFlg", 			18, 	"品番なしフラグ", 		"sProductWebikeCodeFlg"),
	H19("productWebikeCodeFlgError", 		19, 	"品番なしフラグ_エラー", 	"productWebikeCodeFlgError"),
	H20("productEanCode", 					20,	 	"JAN", 				"productEanCode"),
	H21("productEanCodeError", 				21, 	"JAN_エラー", 			"productEanCodeError"),
	H22("productProperPrice", 				22, 	"定価", 				"sProductProperPrice"),
	H23("productProperPriceError", 			23, 	"定価_エラー", 		"productProperPriceError"),
	H24("supplierPricePrice", 				24, 	"仕切り", 			"sSupplierPricePrice"),
	H25("supplierPricePriceError", 			25, 	"仕切り_エラー", 		"supplierPricePriceError"),
	H26("productGroupCode", 				26, 	"グループコード", 		"sProductGroupCode"),
	H27("productGroupCodeError", 			27, 	"グループコード_エラー", 	"productGroupCodeError"),
	H28("productSupplierReleaseDate", 		28, 	"発売日", 			"productSupplierReleaseDate"),
	H29("productSupplierReleaseDateError", 	29, 	"発売日_エラー", 		"productSupplierReleaseDateError"),
	H30("productOpenPriceFlg", 				30, 	"オープン価格", 		"sProductOpenPriceFlg"),
	H31("productOpenPriceFlgError", 		31, 	"オープン価格_エラー", 	"productOpenPriceFlgError"),
	H32("productProperSellingFlg", 			32, 	"定価販売", 			"sProductProperSellingFlg"),
	H33("productProperSellingFlgError", 	33, 	"定価販売_エラー", 		"productProperSellingFlgError"),
	H34("productOrderProductFlg", 			34, 	"受注生産", 			"sProductOrderProductFlg"),
	H35("productOrderProductFlgError", 		35, 	"受注生産_エラー", 		"productOrderProductFlgError"),
	H36("productNoReturnableFlg", 			36, 	"返品不可", 			"sProductNoReturnableFlg"),
	H37("productNoReturnableFlgError", 		37, 	"返品不可_エラー", 		"productNoReturnableFlgError"),
	H38("productAmbiguousImageFlg", 		38, 	"参考画像", 			"sProductAmbiguousImageFlg"),
	H39("productAmbiguousImageFlgError", 	39, 	"参考画像_エラー", 		"productAmbiguousImageFlgError"),
	H40("descriptionSummary", 				40, 	"要点", 				"descriptionSummary"),
	H41("descriptionSummaryError", 			41, 	"要点_エラー", 		"descriptionSummaryError"),
	H42("descriptionRemarks", 				42, 	"備考", 				"descriptionRemarks"),
	H43("descriptionRemarksError", 			43, 	"備考_エラー", 		"descriptionRemarksError"),
	H44("descriptionCaution", 				44, 	"注意", 				"descriptionCaution"),
	H45("descriptionCautionError", 			45, 	"注意_エラー", 		"descriptionCautionError"),
	H46("descriptionSentence", 				46, 	"説明", 				"descriptionSentence"),
	H47("descriptionSentenceError", 		47, 	"説明_エラー", 		"descriptionSentenceError"),
	H48("siireCode1", 						48, 	"仕入先コード1", 		"siireCode1"),
	H49("siireCode1Error", 					49, 	"仕入先コード1_エラー", 	"siireCode1Error"),
	H50("siireName1", 						50, 	"仕入先名1", 			"siireName1"),
	H51("noukiCode1", 						51, 	"納期コード1", 		"noukiCode1"),
	H52("noukiCode1Error", 					52, 	"納期コード1_エラー", 	"noukiCode1Error"),
	H53("noukiName1", 						53, 	"納期名1", 			"noukiName1"),
	H54("siireCode2", 						54, 	"仕入先コード2", 		"siireCode2"),
	H55("siireCode2Error", 					55, 	"仕入先コード2_エラー", 	"siireCode2Error"),
	H56("siireName2", 						56, 	"仕入先名2", 			"siireName2"),
	H57("noukiCode2", 						57, 	"納期コード2", 		"noukiCode2"),
	H58("noukiCode2Error", 					58, 	"納期コード2_エラー", 	"noukiCode2Error"),
	H59("noukiName2", 						59, 	"納期名2", 			"noukiName2"),
	H60("compatibleModel",					60, 	"適合車種", 			"compatibleModel"),
	H61("compatibleModelError", 			61, 	"適合車種_エラー", 		"compatibleModelError"),
	H62("productImageThumbnailPath1", 		62, 	"サムネイル1", 		"productImageThumbnailPath1"),
	H63("productImageThumbnailPath1Error", 	63, 	"サムネイル1_エラー", 	"productImageThumbnailPath1Error"),
	H64("productImageDetailPath1", 			64, 	"詳細画像1", 			"productImageDetailPath1"),
	H65("productImageDetailPath1Error", 	65, 	"詳細画像1_エラー", 	"productImageDetailPath1Error"),
	H66("productImageThumbnailPath2",		66, 	"サムネイル2", 		"productImageThumbnailPath2"),
	H67("productImageThumbnailPath2Error", 	67, 	"サムネイル2_エラー", 	"productImageThumbnailPath2Error"),
	H68("productImageDetailPath2", 			68, 	"詳細画像2", 			"productImageDetailPath2"),
	H69("productImageDetailPath2Error", 	69, 	"詳細画像2_エラー", 	"productImageDetailPath2Error"),
	H70("productImageThumbnailPath3", 		70, 	"サムネイル3", 		"productImageThumbnailPath3"),
	H71("productImageThumbnailPath3Error", 	71, 	"サムネイル3_エラー", 	"productImageThumbnailPath3Error"),
	H72("productImageDetailPath3", 			72, 	"詳細画像3", 			"productImageDetailPath3"),
	H73("productImageDetailPath3Error", 	73, 	"詳細画像3_エラー", 	"productImageDetailPath3Error"),
	H74("productImageThumbnailPath4", 		74, 	"サムネイル4", 		"productImageThumbnailPath4"),
	H75("productImageThumbnailPath4Error", 	75, 	"サムネイル4_エラー", 	"productImageThumbnailPath4Error"),
	H76("productImageDetailPath4", 			76, 	"詳細画像4", 			"productImageDetailPath4"),
	H77("productImageDetailPath4Error", 	77, 	"詳細画像4_エラー", 	"productImageDetailPath4Error"),
	H78("productImageThumbnailPath5", 		78, 	"サムネイル5", 		"productImageThumbnailPath5"),
	H79("productImageThumbnailPath5Error", 	79, 	"サムネイル5_エラー", 	"productImageThumbnailPath5Error"),
	H80("productImageDetailPath5", 			80, 	"詳細画像5", 			"productImageDetailPath5"),
	H81("productImageDetailPath5Error", 	81, 	"詳細画像5_エラー", 	"productImageDetailPath5Error"),
	H82("productImageThumbnailPath6", 		82, 	"サムネイル6", 		"productImageThumbnailPath6"),
	H83("productImageThumbnailPath6Error", 	83, 	"サムネイル6_エラー", 	"productImageThumbnailPath6Error"),
	H84("productImageDetailPath6", 			84, 	"詳細画像6", 			"productImageDetailPath6"),
	H85("productImageDetailPath6Error", 	85, 	"詳細画像6_エラー", 	"productImageDetailPath6Error"),
	H86("productImageThumbnailPath7", 		86, 	"サムネイル7", 		"productImageThumbnailPath7"),
	H87("productImageThumbnailPath7Error", 	87, 	"サムネイル7_エラー", 	"productImageThumbnailPath7Error"),
	H88("productImageDetailPath7", 			88, 	"詳細画像7", 			"productImageDetailPath7"),
	H89("productImageDetailPath7Error", 	89, 	"詳細画像7_エラー", 	"productImageDetailPath7Error"),
	H90("productImageThumbnailPath8", 		90, 	"サムネイル8", 		"productImageThumbnailPath8"),
	H91("productImageThumbnailPath8Error", 	91, 	"サムネイル8_エラー", 	"productImageThumbnailPath8Error"),
	H92("productImageDetailPath8", 			92, 	"詳細画像8", 			"productImageDetailPath8"),
	H93("productImageDetailPath8Error", 	93, 	"詳細画像8_エラー", 	"productImageDetailPath8Error"),
	H94("productImageThumbnailPath9", 		94, 	"サムネイル9", 		"productImageThumbnailPath9"),
	H95("productImageThumbnailPath9Error", 	95, 	"サムネイル9_エラー", 	"productImageThumbnailPath9Error"),
	H96("productImageDetailPath9", 			96, 	"詳細画像9", 			"productImageDetailPath9"),
	H97("productImageDetailPath9Error", 	97, 	"詳細画像9_エラー", 	"productImageDetailPath9Error"),
	H98("productImageThumbnailPath10", 		98, 	"サムネイル10", 		"productImageThumbnailPath10"),
	H99("productImageThumbnailPath10Error",	99, 	"サムネイル10_エラー", 	"productImageThumbnailPath10Error"),
	H100("productImageDetailPath10", 		100, 	"詳細画像10", 		"productImageDetailPath10"),
	H101("productImageDetailPath10Error", 	101, 	"詳細画像10_エラー", 	"productImageDetailPath10Error"),
	H102("selectCode1", 					102, 	"選択肢コード1", 		"sSelectCode1"),
	H103("selectCode1Error", 				103, 	"選択肢コード1_エラー", 	"selectCode1Error"),
	H104("selectName1", 					104, 	"選択肢名1", 			"selectName1"),
	H105("selectName1Error", 				105, 	"選択肢名1_エラー", 	"selectName1Error"),
	H106("selectCode2", 					106, 	"選択肢コード2", 		"sSelectCode2"),
	H107("selectCode2Error", 				107, 	"選択肢コード2_エラー", 	"selectCode2Error"),
	H108("selectName2",						108, 	"選択肢名2", 			"selectName2"),
	H109("selectName2Error", 				109, 	"選択肢名2_エラー", 	"selectName2Error"),
	H110("selectCode3", 					110, 	"選択肢コード3", 		"sSelectCode3"),
	H111("selectCode3Error", 				111, 	"選択肢コード3_エラー", 	"selectCode3Error"),
	H112("selectName3", 					112, 	"選択肢名3", 			"selectName3"),
	H113("selectName3Error", 				113, 	"選択肢名3_エラー", 	"selectName3Error"),
	H114("selectCode4", 					114, 	"選択肢コード4", 		"sSelectCode4"),
	H115("selectCode4Error", 				115, 	"選択肢コード4_エラー", 	"selectCode4Error"),
	H116("selectName4", 					116, 	"選択肢名4", 			"selectName4"),
	H117("selectName4Error", 				117, 	"選択肢名4_エラー", 	"selectName4Error"),
	H118("selectCode5", 					118, 	"選択肢コード5", 		"sSelectCode5"),
	H119("selectCode5Error", 				119, 	"選択肢コード5_エラー", 	"selectCode5Error"),
	H120("selectName5", 					120, 	"選択肢名5", 			"selectName5"),
	H121("selectName5Error", 				121, 	"選択肢名5_エラー", 	"selectName5Error"),
	H122("selectCode6", 					122, 	"選択肢コード6", 		"sSelectCode6"),
	H123("selectCode6Error", 				123, 	"選択肢コード6_エラー", 	"selectCode6Error"),
	H124("selectName6", 					124, 	"選択肢名6", 			"selectName6"),
	H125("selectName6Error", 				125, 	"選択肢名6_エラー", 	"selectName6Error"),
	H126("selectCode7", 					126, 	"選択肢コード7", 		"sSelectCode7"),
	H127("selectCode7Error", 				127, 	"選択肢コード7_エラー", 	"selectCode7Error"),
	H128("selectName7", 					128, 	"選択肢名7", 			"selectName7"),
	H129("selectName7Error", 				129, 	"選択肢名7_エラー", 	"selectName7Error"),
	H130("selectCode8", 					130, 	"選択肢コード8", 		"sSelectCode8"),
	H131("selectCode8Error", 				131, 	"選択肢コード8_エラー", 	"selectCode8Error"),
	H132("selectName8", 					132, 	"選択肢名8", 			"selectName8"),
	H133("selectName8Error", 				133, 	"選択肢名8_エラー", 	"selectName8Error"),
	H134("selectCode9", 					134, 	"選択肢コード9", 		"sSelectCode9"),
	H135("selectCode9Error", 				135, 	"選択肢コード9_エラー", 	"selectCode9Error"),
	H136("selectName9",	 					136, 	"選択肢名9", 			"selectName9"),
	H137("selectName9Error", 				137, 	"選択肢名9_エラー", 	"selectName9Error"),
	H138("selectCode10", 					138, 	"選択肢コード10", 		"sSelectCode10"),
	H139("selectCode10Error", 				139, 	"選択肢コード10_エラー", "selectCode10Error"),
	H140("selectName10", 					140, 	"選択肢名10", 		"selectName10"),
	H141("selectName10Error", 				141, 	"選択肢名10_エラー", 	"selectName10Error"),
	H142("customersConfirmationItem",		142, 	"お客様確認項目", 		"customersConfirmationItem"),
	H143("customersConfirmationItemError", 	143, 	"お客様確認項目_エラー",	"customersConfirmationItemError"),
	H144("link", 							144, 	"リンク", 				"link"),
	H145("linkError", 						145, 	"リンク_エラー", 		"linkError"),
	H146("animation", 						146, 	"動画", 				"animation"),
	H147("animationError", 					147, 	"動画_エラー", 		"animationError"),
	H148("attributeName", 					148, 	"属性名", 			"attributeName"),
	H149("attributeValue", 					149, 	"管理名", 			"attributeValue"),
	H150("attributeDisplay", 				150, 	"表示名",			"attributeDisplay"),
	H151("fitModelMaker", 					151, 	"メーカー", 			"fitModelMaker"),
	H152("fitModelModel", 					152, 	"車種", 				"fitModelModel"),
	H153("fitModelStyle", 					153, 	"年式・型式", 			"fitModelStyle");
	//EOE @rcv!Luong.Dai 2014/07/09 #9811: Fix enum to csv_v2


    private String headerEN;
    private Integer headerNumber;
    private String headerJP;
    //Header with type = string
    private String sHeader;

    /**---------------------------------------------------------------------------
     * Private constructor for EnumProductConditionFlg.
     * @param headerJP for get value
     * @param headerEN of EnumProductConditionFlg
     * @param headerNumber columnNumber
     * @param sHeader		header with type = string
     */
    private EnumImportCSVMode1Header(final String headerEN
    								, final Integer headerNumber
    								, final String headerJP
    								, final String sHeader) {
        this.setHeaderEN(headerEN);
        this.setHeaderNumber(headerNumber);
        this.setHeaderJP(headerJP);
        this.setsHeader(sHeader);
    }

    /************************************************************************
     * <b>Description:</b><br>
     *  get header number by headerJP.
     *
     * @author      Nguyen.Chuong
     * @date        2014/03/05
     * @param       headerJP String header text Japanese.
     * @return      Integer column number.
     ************************************************************************/
    public static Integer getHeaderNumberByHeaderJP(String headerJP) {
        for (EnumImportCSVMode1Header valueEnum : values()) {
            if (headerJP.equals(valueEnum.getHeaderJP())) {
                return valueEnum.getHeaderNumber();
            }
        }
        /* BOE #7206 Luong.Dai Fix return default value: 0 */
        /*return null;*/
        return 0;
        /* EOE #7206 Luong.Dai Fix return default value: 0 */
    }

    /************************************************************************
     * <b>Description:</b><br>
     *  Get sHeader by HeaderJP.
     *
     * @author		Luong.Dai
     * @date		May 13, 2014
     * @param 		headerJP		Header in JP
     * @return		String			sHeader
     ************************************************************************/
    public static String getSHeaderByHeaderJP(String headerJP) {
        for (EnumImportCSVMode1Header valueEnum : values()) {
            if (headerJP.equals(valueEnum.getHeaderJP())) {
                return valueEnum.getsHeader();
            }
        }
        return "";
    }

    /************************************************************************
     * <b>Description:</b><br>
     *  get header number by headerEn.
     *
     * @author      Nguyen.Chuong
     * @date        2014/03/05
     * @param       headerEN String header text Japanese.
     * @return      Integer column number.
     ************************************************************************/
    public static Integer getHeaderNumberByHeaderEN(String headerEN) {
        for (EnumImportCSVMode1Header valueEnum : values()) {
            if (headerEN.equals(valueEnum.getHeaderEN())) {
                return valueEnum.getHeaderNumber();
            }
        }
        return null;
    }

    /************************************************************************
     * <b>Description:</b><br>
     *  get header English by number.
     *
     * @author      Nguyen.Chuong
     * @date        2014/03/05
     * @param       headerNumber int the number of header in CSV
     * @return      String header name in EN
     ************************************************************************/
    public static String getHeaderENByHeaderNumber(int headerNumber) {
        for (EnumImportCSVMode1Header valueEnum : values()) {
            if (headerNumber ==  valueEnum.getHeaderNumber()) {
                return valueEnum.getHeaderEN();
            }
        }
        return "";
    }

    /************************************************************************
     * <b>Description:</b><br>
     *  get header Japan by header number.
     *
     * @author      Nguyen.Chuong
     * @date        2014/07/09
     * @param       headerNumber int the number of header in CSV
     * @return      String header name in JP
     ************************************************************************/
    public static String getHeaderJPByHeaderNumber(int headerNumber) {
        for (EnumImportCSVMode1Header valueEnum : values()) {
            if (headerNumber ==  valueEnum.getHeaderNumber()) {
                return valueEnum.getHeaderJP();
            }
        }
        return "";
    }

    /************************************************************************
     * <b>Description:</b><br>
     *  get header EN by header JP.
     *
     * @author      Nguyen.Chuong
     * @date        2014/03/05
     * @param       headerJP String the String of headerJP in CSV
     * @return      String header name in EN
     ************************************************************************/
    public static String getHeaderENByHeaderJP(String headerJP) {
        for (EnumImportCSVMode1Header valueEnum : values()) {
            if (headerJP.equals(valueEnum.getHeaderJP())) {
                return valueEnum.getHeaderEN();
            }
        }
        return "";
    }

    /************************************************************************
     * <b>Description:</b><br>
     *  get header EN by header JP.
     *
     * @author      Nguyen.Chuong
     * @date        2014/05/13
     * @param       headerEN String the String of headerEN in CSV
     * @return      String header name in EN
     ************************************************************************/
    public static String getHeaderJPByHeaderEN(String headerEN) {
        for (EnumImportCSVMode1Header valueEnum : values()) {
            if (headerEN.equals(valueEnum.getHeaderEN())) {
                return valueEnum.getHeaderJP();
            }
        }
        return "";
    }

    /**
     * @return the headerJP
     */
    public String getHeaderJP() {
        return headerJP;
    }

    /**
     * @param headerJP the headerJP to set
     */
    public void setHeaderJP(String headerJP) {
        this.headerJP = headerJP;
    }

    /**
     * @return the headerEN
     */
    public String getHeaderEN() {
        return headerEN;
    }

    /**
     * @param headerEN the headerEN to set
     */
    public void setHeaderEN(String headerEN) {
        this.headerEN = headerEN;
    }

    /**
     * @return the headerNumber
     */
    public Integer getHeaderNumber() {
        return headerNumber;
    }

    /**
     * @param headerNumber the headerNumber to set
     */
    public void setHeaderNumber(Integer headerNumber) {
        this.headerNumber = headerNumber;
    }

	/**
	 * @return the sHeader
	 */
	public String getsHeader() {
		return sHeader;
	}

	/**
	 * @param sHeader the sHeader to set
	 */
	public void setsHeader(String sHeader) {
		this.sHeader = sHeader;
	}
}
