package net.webike.RcProductFactory.entity;

/**
 * EntMstSyouhinSelect.
 */
public class EntMstSyouhinSelect {

	private int selectCode;
	private String name;
	/**
	 * @return the selectCode
	 */
	public int getSelectCode() {
		return selectCode;
	}
	/**
	 * @param selectCode the selectCode to set
	 */
	public void setSelectCode(int selectCode) {
		this.selectCode = selectCode;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
}
