/************************************************************************
 * file name	： EntProduct.java
 * author		： Nguyen.Chuong
 * version		： 1.0.0
 * date created	： 25 Oct 2013
 * date updated	： 25 Oct 2013
 * description	： Entity for product.
 *
 ***********************************************************************/
package net.webike.RcProductFactory.entity;

/**
 * EntProduct.
 */
public class EntProduct {
    private String matterNo;

    private String productCode;
    private String sysProductCode;
    private String brandCode;
    private String brandName;
    private String productName;
    private String manufactoryCode;
    private String bunruiCode;
    private String bunruiName;
    private String productStatusCode;
    private String productStatusName;
    private String productErrorFlag;
    private String productSummary;
    //sort attribute of kendoUI grid
    private String sortField;
    private String sortDir;
    // Add properties by luong.dai 2013/11/6
    private String userName;

    //BOE Nguyen.Chuong 2014/02/26
    private String productId;
    private Long productSyouhinSysCode;
    private Integer attributeCode;
    private String attributeValue;
    private String attributeDisplay;
    private String importMode;
    //EOE Nguyen.Chuong 2014/02/26

    /**
     * @return the productCode
     */
    public String getProductCode() {
        return productCode;
    }
    /**
     * @param productCode the productCode to set
     */
    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }
    /**
     * @return the brandName
     */
    public String getBrandName() {
        return brandName;
    }
    /**
     * @param brandName the brandName to set
     */
    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }
    /**
     * @return the productName
     */
    public String getProductName() {
        return productName;
    }
    /**
     * @param productName the productName to set
     */
    public void setProductName(String productName) {
        this.productName = productName;
    }
    /**
     * @return the bunruiCode
     */
    public String getBunruiCode() {
        return bunruiCode;
    }
    /**
     * @param bunruiCode the bunruiCode to set
     */
    public void setBunruiCode(String bunruiCode) {
        this.bunruiCode = bunruiCode;
    }
    /**
     * @return the bunruiName
     */
    public String getBunruiName() {
        return bunruiName;
    }
    /**
     * @param bunruiName the bunruiName to set
     */
    public void setBunruiName(String bunruiName) {
        this.bunruiName = bunruiName;
    }
    /**
     * @return the productStatusCode
     */
    public String getProductStatusCode() {
        return productStatusCode;
    }
    /**
     * @param productStatusCode the productStatusCode to set
     */
    public void setProductStatusCode(String productStatusCode) {
        this.productStatusCode = productStatusCode;
    }
    /**
     * @return the brandCode
     */
    public String getBrandCode() {
        return brandCode;
    }
    /**
     * @param brandCode the brandCode to set
     */
    public void setBrandCode(String brandCode) {
        this.brandCode = brandCode;
    }
    /**
     * @return the productSummary
     */
    public String getProductSummary() {
        return productSummary;
    }
    /**
     * @param productSummary the productSummary to set
     */
    public void setProductSummary(String productSummary) {
        this.productSummary = productSummary;
    }
    /**
     * @return the manufactoryCode
     */
    public String getManufactoryCode() {
        return manufactoryCode;
    }
    /**
     * @param manufactoryCode the manufactoryCode to set
     */
    public void setManufactoryCode(String manufactoryCode) {
        this.manufactoryCode = manufactoryCode;
    }
    /**
     * @return the sysProductCode
     */
    public String getSysProductCode() {
        return sysProductCode;
    }
    /**
     * @param sysProductCode the sysProductCode to set
     */
    public void setSysProductCode(String sysProductCode) {
        this.sysProductCode = sysProductCode;
    }
    /**
     * @return the productStatusName
     */
    public String getProductStatusName() {
        return productStatusName;
    }
    /**
     * @param productStatusName the productStatusName to set
     */
    public void setProductStatusName(String productStatusName) {
        this.productStatusName = productStatusName;
    }
    /**
     * @return the productErrorFlag
     */
    public String getProductErrorFlag() {
        return productErrorFlag;
    }
    /**
     * @param productErrorFlag the productErrorFlag to set
     */
    public void setProductErrorFlag(String productErrorFlag) {
        this.productErrorFlag = productErrorFlag;
    }
    /**
     * @return the matterNo
     */
    public String getMatterNo() {
        return matterNo;
    }
    /**
     * @param matterNo the matterNo to set
     */
    public void setMatterNo(String matterNo) {
        this.matterNo = matterNo;
    }

    /**
     * @return the userName
     */
    public String getUserName() {
        return userName;
    }

    /**
     * @param userName the userName to set
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * @return new EntProduct with same matterNo
     */
    public EntProduct cloneMatterNo() {
        EntProduct newEnt = new EntProduct();

        newEnt.setMatterNo(this.matterNo);

        return newEnt;
    }
    /**
     * @return the sortField
     */
    public String getSortField() {
        return sortField;
    }
    /**
     * @param sortField the sortField to set
     */
    public void setSortField(String sortField) {
        this.sortField = sortField;
    }
    /**
     * @return the sortDir
     */
    public String getSortDir() {
        return sortDir;
    }
    /**
     * @param sortDir the sortDir to set
     */
    public void setSortDir(String sortDir) {
        this.sortDir = sortDir;
    }
    /**
     * @return the attributeDisplay
     */
    public String getAttributeDisplay() {
        return attributeDisplay;
    }
    /**
     * @param attributeDisplay the attributeDisplay to set
     */
    public void setAttributeDisplay(String attributeDisplay) {
        this.attributeDisplay = attributeDisplay;
    }
    /**
     * @return the importMode
     */
    public String getImportMode() {
        return importMode;
    }
    /**
     * @param importMode the importMode to set
     */
    public void setImportMode(String importMode) {
        this.importMode = importMode;
    }
    /**
     * @return the productSyouhinSysCode
     */
    public Long getProductSyouhinSysCode() {
        return productSyouhinSysCode;
    }
    /**
     * @param productSyouhinSysCode the productSyouhinSysCode to set
     */
    public void setProductSyouhinSysCode(Long productSyouhinSysCode) {
        this.productSyouhinSysCode = productSyouhinSysCode;
    }
    /**
     * @return the attributeCode
     */
    public Integer getAttributeCode() {
        return attributeCode;
    }
    /**
     * @param attributeCode the attributeCode to set
     */
    public void setAttributeCode(Integer attributeCode) {
        this.attributeCode = attributeCode;
    }
    /**
     * @return the productId
     */
    public String getProductId() {
        return productId;
    }
    /**
     * @param productId the productId to set
     */
    public void setProductId(String productId) {
        this.productId = productId;
    }
    /**
     * @return the attributeValue
     */
    public String getAttributeValue() {
        return attributeValue;
    }
    /**
     * @param attributeValue the attributeValue to set
     */
    public void setAttributeValue(String attributeValue) {
        this.attributeValue = attributeValue;
    }
}
