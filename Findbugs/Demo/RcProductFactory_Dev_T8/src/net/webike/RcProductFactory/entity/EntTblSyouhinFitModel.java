/************************************************************************
 * File Name    ： EntTblSyouhinFitModel.java
 * Author       ： Nguyen.Chuong
 * Version      ： 1.0.0
 * Date Created ： 2014/02/17
 * Date Updated ： 2014/02/17
 * Description  ： Entity to contain data of rc_syouhin.tbl_syouhin_fit_model table.
 ***********************************************************************/
package net.webike.RcProductFactory.entity;


/**
 * Entity to contain data of rc_syouhin.tbl_syouhin_fit_model table.
 */
public class EntTblSyouhinFitModel {

    private long syouhinSysCode;
    private String maker;
    private String model;
    private String style;
    /**
     * @return the syouhinSysCode
     */
    public long getSyouhinSysCode() {
        return syouhinSysCode;
    }
    /**
     * @param syouhinSysCode the syouhinSysCode to set
     */
    public void setSyouhinSysCode(long syouhinSysCode) {
        this.syouhinSysCode = syouhinSysCode;
    }
    /**
     * @return the maker
     */
    public String getMaker() {
        return maker;
    }
    /**
     * @param maker the maker to set
     */
    public void setMaker(String maker) {
        this.maker = maker;
    }
    /**
     * @return the model
     */
    public String getModel() {
        return model;
    }
    /**
     * @param model the model to set
     */
    public void setModel(String model) {
        this.model = model;
    }
    /**
     * @return the style
     */
    public String getStyle() {
        return style;
    }
    /**
     * @param style the style to set
     */
    public void setStyle(String style) {
        this.style = style;
    }

}

