/************************************************************************
 * File Name    ： UserAuthority.java
 * Author       ： Nguyen.Chuong
 * Version      ： 1.0.0
 * Date Created ： 2013/10/02
 * Date Updated ： 2013/10/02
 * Description  ： enum to contain all status value: active, deleted.
 *
 ***********************************************************************/
package net.webike.RcProductFactory.entity;

import java.util.HashMap;
import java.util.TreeMap;

import org.apache.commons.lang.StringUtils;

/**
 * enum UserAuthority to contain status.
 */
public enum UserStatus {

    S1("0", "有効"),
    S2("1", "ロック");

    /**---------------------------------------------------------------------------
     * String value.
     * @description value of status. Can be 0, 1.
     */
    private String val;
    /**---------------------------------------------------------------------------
     * String name.
     * @description name of status. Can be active, deleted.
     */
    private String name;

    /**---------------------------------------------------------------------------
     * Enum for status attribute.
     * @param val String
     * @param name String
     */
    private UserStatus(final String val, final String name) {
        this.val = val;
        this.name = name;
    }
    /**
     * Get all enum and return as the treeMap.
     * @return TreeMap<val, name>
     * @author Nguyen.Chuong
     * @version 1.0
     */
    public static TreeMap<String, String> getListTreeMap() {
        TreeMap<String, String> tm = new TreeMap<String, String>();
        for (UserStatus o : values()) {
            tm.put(o.getVal(), o.getName());
        }
        return tm;
    }

    /**
     * Get all enum and return as the hashMap.
     * @return HashMap<val, name>
     * @author Nguyen.Chuong
     * @version 1.0
     */
    public static HashMap<String, String> getListHashMap() {
        HashMap<String, String> tm = new HashMap<String, String>();
        for (UserStatus o : values()) {
            tm.put(o.getVal(), o.getName());
        }
        return tm;
    }
    /**
     * get name of status by value.
     * @param val String
     * @return name of stauts: 0 => active, 1 => deleted
     * @author Nguyen.Chuong
     * @version 1.0
     */
    public static String getName(String val) {
        if (StringUtils.isEmpty(val)) {
            return "";
        }
        for (UserStatus o : values()) {
            if (o.getVal().equals(val)) {
                return o.getName();
            }
        }
        return "";
    }

    /**
     * get value of authority.
     * @return val
     */
    public String getVal() {
        return val;
    }

    /**
     * set value of authority.
     * @param val val
     */
    public void setVal(String val) {
        this.val = val;
    }

    /**
     * get name of authority.
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * set name of authority.
     * @param name name
     */
    public void setName(String name) {
        this.name = name;
    }
}
