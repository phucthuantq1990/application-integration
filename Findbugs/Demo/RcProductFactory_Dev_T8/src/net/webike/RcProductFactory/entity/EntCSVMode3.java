/************************************************************************
 * file name	： EntCSVMode3.java
 * author		： Nguyen.Chuong
 * version		： 1.0.0
 * date created	： 2014/02/26
 * date updated	： 2014/02/26
 * description	： Entity for product tbl_factory_product using by CSV.
 *
 ***********************************************************************/
package net.webike.RcProductFactory.entity;

/**
 * EntCSVMode3.
 */
public class EntCSVMode3 {
    private long productId;
    private long productSyouhinSysCode;
    private String attributeName;

    private int attributeCode1;
    private String attributeValues1;
    private String attributeDisplay1;

    private int attributeCode2;
    private String attributeValues2;
    private String attributeDisplay2;

    private int attributeCode3;
    private String attributeValues3;
    private String attributeDisplay3;

    private int attributeCode4;
    private String attributeValues4;
    private String attributeDisplay4;


    private int attributeCode5;
    private String attributeValues5;
    private String attributeDisplay5;


    private int attributeCode6;
    private String attributeValues6;
    private String attributeDisplay6;

    private int attributeCode7;
    private String attributeValues7;
    private String attributeDisplay7;

    private int attributeCode8;
    private String attributeValues8;
    private String attributeDisplay8;

    private int attributeCode9;
    private String attributeValues9;
    private String attributeDisplay9;


    private int attributeCode10;
    private String attributeValues10;
    private String attributeDisplay10;


    private int attributeCode11;
    private String attributeValues11;
    private String attributeDisplay11;

    private int attributeCode12;
    private String attributeValues12;
    private String attributeDisplay12;

    private int attributeCode13;
    private String attributeValues13;
    private String attributeDisplay13;

    private int attributeCode14;
    private String attributeValues14;
    private String attributeDisplay14;


    private int attributeCode15;
    private String attributeValues15;
    private String attributeDisplay15;


    private int attributeCode16;
    private String attributeValues16;
    private String attributeDisplay16;

    private int attributeCode17;
    private String attributeValues17;
    private String attributeDisplay17;

    private int attributeCode18;
    private String attributeValues18;
    private String attributeDisplay18;

    private int attributeCode19;
    private String attributeValues19;
    private String attributeDisplay19;


    private int attributeCode20;
    private String attributeValues20;
    private String attributeDisplay20;


    private int attributeCode21;
    private String attributeValues21;
    private String attributeDisplay21;

    private int attributeCode22;
    private String attributeValues22;
    private String attributeDisplay22;

    private int attributeCode23;
    private String attributeValues23;
    private String attributeDisplay23;

    private int attributeCode24;
    private String attributeValues24;
    private String attributeDisplay24;


    private int attributeCode25;
    private String attributeValues25;
    private String attributeDisplay25;


    private int attributeCode26;
    private String attributeValues26;
    private String attributeDisplay26;

    private int attributeCode27;
    private String attributeValues27;
    private String attributeDisplay27;

    private int attributeCode28;
    private String attributeValues28;
    private String attributeDisplay28;

    private int attributeCode29;
    private String attributeValues29;
    private String attributeDisplay29;


    private int attributeCode30;
    private String attributeValues30;
    private String attributeDisplay30;
    /**
     * @return the productId
     */
    public long getProductId() {
        return productId;
    }
    /**
     * @param productId the productId to set
     */
    public void setProductId(long productId) {
        this.productId = productId;
    }
    /**
     * @return the productSyouhinSysCode
     */
    public long getProductSyouhinSysCode() {
        return productSyouhinSysCode;
    }
    /**
     * @param productSyouhinSysCode the productSyouhinSysCode to set
     */
    public void setProductSyouhinSysCode(long productSyouhinSysCode) {
        this.productSyouhinSysCode = productSyouhinSysCode;
    }
    /**
     * @return the attributeCode1
     */
    public int getAttributeCode1() {
        return attributeCode1;
    }
    /**
     * @param attributeCode1 the attributeCode1 to set
     */
    public void setAttributeCode1(int attributeCode1) {
        this.attributeCode1 = attributeCode1;
    }
    /**
     * @return the attributeValues1
     */
    public String getAttributeValues1() {
        return attributeValues1;
    }
    /**
     * @param attributeValues1 the attributeValues1 to set
     */
    public void setAttributeValues1(String attributeValues1) {
        this.attributeValues1 = attributeValues1;
    }
    /**
     * @return the attributeDisplay1
     */
    public String getAttributeDisplay1() {
        return attributeDisplay1;
    }
    /**
     * @param attributeDisplay1 the attributeDisplay1 to set
     */
    public void setAttributeDisplay1(String attributeDisplay1) {
        this.attributeDisplay1 = attributeDisplay1;
    }
    /**
     * @return the attributeCode2
     */
    public int getAttributeCode2() {
        return attributeCode2;
    }
    /**
     * @param attributeCode2 the attributeCode2 to set
     */
    public void setAttributeCode2(int attributeCode2) {
        this.attributeCode2 = attributeCode2;
    }
    /**
     * @return the attributeValues2
     */
    public String getAttributeValues2() {
        return attributeValues2;
    }
    /**
     * @param attributeValues2 the attributeValues2 to set
     */
    public void setAttributeValues2(String attributeValues2) {
        this.attributeValues2 = attributeValues2;
    }
    /**
     * @return the attributeDisplay2
     */
    public String getAttributeDisplay2() {
        return attributeDisplay2;
    }
    /**
     * @param attributeDisplay2 the attributeDisplay2 to set
     */
    public void setAttributeDisplay2(String attributeDisplay2) {
        this.attributeDisplay2 = attributeDisplay2;
    }
    /**
     * @return the attributeCode3
     */
    public int getAttributeCode3() {
        return attributeCode3;
    }
    /**
     * @param attributeCode3 the attributeCode3 to set
     */
    public void setAttributeCode3(int attributeCode3) {
        this.attributeCode3 = attributeCode3;
    }
    /**
     * @return the attributeValues3
     */
    public String getAttributeValues3() {
        return attributeValues3;
    }
    /**
     * @param attributeValues3 the attributeValues3 to set
     */
    public void setAttributeValues3(String attributeValues3) {
        this.attributeValues3 = attributeValues3;
    }
    /**
     * @return the attributeDisplay3
     */
    public String getAttributeDisplay3() {
        return attributeDisplay3;
    }
    /**
     * @param attributeDisplay3 the attributeDisplay3 to set
     */
    public void setAttributeDisplay3(String attributeDisplay3) {
        this.attributeDisplay3 = attributeDisplay3;
    }
    /**
     * @return the attributeCode4
     */
    public int getAttributeCode4() {
        return attributeCode4;
    }
    /**
     * @param attributeCode4 the attributeCode4 to set
     */
    public void setAttributeCode4(int attributeCode4) {
        this.attributeCode4 = attributeCode4;
    }
    /**
     * @return the attributeValues4
     */
    public String getAttributeValues4() {
        return attributeValues4;
    }
    /**
     * @param attributeValues4 the attributeValues4 to set
     */
    public void setAttributeValues4(String attributeValues4) {
        this.attributeValues4 = attributeValues4;
    }
    /**
     * @return the attributeDisplay4
     */
    public String getAttributeDisplay4() {
        return attributeDisplay4;
    }
    /**
     * @param attributeDisplay4 the attributeDisplay4 to set
     */
    public void setAttributeDisplay4(String attributeDisplay4) {
        this.attributeDisplay4 = attributeDisplay4;
    }
    /**
     * @return the attributeCode5
     */
    public int getAttributeCode5() {
        return attributeCode5;
    }
    /**
     * @param attributeCode5 the attributeCode5 to set
     */
    public void setAttributeCode5(int attributeCode5) {
        this.attributeCode5 = attributeCode5;
    }
    /**
     * @return the attributeValues5
     */
    public String getAttributeValues5() {
        return attributeValues5;
    }
    /**
     * @param attributeValues5 the attributeValues5 to set
     */
    public void setAttributeValues5(String attributeValues5) {
        this.attributeValues5 = attributeValues5;
    }
    /**
     * @return the attributeDisplay5
     */
    public String getAttributeDisplay5() {
        return attributeDisplay5;
    }
    /**
     * @param attributeDisplay5 the attributeDisplay5 to set
     */
    public void setAttributeDisplay5(String attributeDisplay5) {
        this.attributeDisplay5 = attributeDisplay5;
    }
    /**
     * @return the attributeCode6
     */
    public int getAttributeCode6() {
        return attributeCode6;
    }
    /**
     * @param attributeCode6 the attributeCode6 to set
     */
    public void setAttributeCode6(int attributeCode6) {
        this.attributeCode6 = attributeCode6;
    }
    /**
     * @return the attributeValues6
     */
    public String getAttributeValues6() {
        return attributeValues6;
    }
    /**
     * @param attributeValues6 the attributeValues6 to set
     */
    public void setAttributeValues6(String attributeValues6) {
        this.attributeValues6 = attributeValues6;
    }
    /**
     * @return the attributeDisplay6
     */
    public String getAttributeDisplay6() {
        return attributeDisplay6;
    }
    /**
     * @param attributeDisplay6 the attributeDisplay6 to set
     */
    public void setAttributeDisplay6(String attributeDisplay6) {
        this.attributeDisplay6 = attributeDisplay6;
    }
    /**
     * @return the attributeCode7
     */
    public int getAttributeCode7() {
        return attributeCode7;
    }
    /**
     * @param attributeCode7 the attributeCode7 to set
     */
    public void setAttributeCode7(int attributeCode7) {
        this.attributeCode7 = attributeCode7;
    }
    /**
     * @return the attributeValues7
     */
    public String getAttributeValues7() {
        return attributeValues7;
    }
    /**
     * @param attributeValues7 the attributeValues7 to set
     */
    public void setAttributeValues7(String attributeValues7) {
        this.attributeValues7 = attributeValues7;
    }
    /**
     * @return the attributeDisplay7
     */
    public String getAttributeDisplay7() {
        return attributeDisplay7;
    }
    /**
     * @param attributeDisplay7 the attributeDisplay7 to set
     */
    public void setAttributeDisplay7(String attributeDisplay7) {
        this.attributeDisplay7 = attributeDisplay7;
    }
    /**
     * @return the attributeCode8
     */
    public int getAttributeCode8() {
        return attributeCode8;
    }
    /**
     * @param attributeCode8 the attributeCode8 to set
     */
    public void setAttributeCode8(int attributeCode8) {
        this.attributeCode8 = attributeCode8;
    }
    /**
     * @return the attributeValues8
     */
    public String getAttributeValues8() {
        return attributeValues8;
    }
    /**
     * @param attributeValues8 the attributeValues8 to set
     */
    public void setAttributeValues8(String attributeValues8) {
        this.attributeValues8 = attributeValues8;
    }
    /**
     * @return the attributeDisplay8
     */
    public String getAttributeDisplay8() {
        return attributeDisplay8;
    }
    /**
     * @param attributeDisplay8 the attributeDisplay8 to set
     */
    public void setAttributeDisplay8(String attributeDisplay8) {
        this.attributeDisplay8 = attributeDisplay8;
    }
    /**
     * @return the attributeCode9
     */
    public int getAttributeCode9() {
        return attributeCode9;
    }
    /**
     * @param attributeCode9 the attributeCode9 to set
     */
    public void setAttributeCode9(int attributeCode9) {
        this.attributeCode9 = attributeCode9;
    }
    /**
     * @return the attributeValues9
     */
    public String getAttributeValues9() {
        return attributeValues9;
    }
    /**
     * @param attributeValues9 the attributeValues9 to set
     */
    public void setAttributeValues9(String attributeValues9) {
        this.attributeValues9 = attributeValues9;
    }
    /**
     * @return the attributeDisplay9
     */
    public String getAttributeDisplay9() {
        return attributeDisplay9;
    }
    /**
     * @param attributeDisplay9 the attributeDisplay9 to set
     */
    public void setAttributeDisplay9(String attributeDisplay9) {
        this.attributeDisplay9 = attributeDisplay9;
    }
    /**
     * @return the attributeCode10
     */
    public int getAttributeCode10() {
        return attributeCode10;
    }
    /**
     * @param attributeCode10 the attributeCode10 to set
     */
    public void setAttributeCode10(int attributeCode10) {
        this.attributeCode10 = attributeCode10;
    }
    /**
     * @return the attributeValues10
     */
    public String getAttributeValues10() {
        return attributeValues10;
    }
    /**
     * @param attributeValues10 the attributeValues10 to set
     */
    public void setAttributeValues10(String attributeValues10) {
        this.attributeValues10 = attributeValues10;
    }
    /**
     * @return the attributeDisplay10
     */
    public String getAttributeDisplay10() {
        return attributeDisplay10;
    }
    /**
     * @param attributeDisplay10 the attributeDisplay10 to set
     */
    public void setAttributeDisplay10(String attributeDisplay10) {
        this.attributeDisplay10 = attributeDisplay10;
    }
    /**
     * @return the attributeCode11
     */
    public int getAttributeCode11() {
        return attributeCode11;
    }
    /**
     * @param attributeCode11 the attributeCode11 to set
     */
    public void setAttributeCode11(int attributeCode11) {
        this.attributeCode11 = attributeCode11;
    }
    /**
     * @return the attributeValues11
     */
    public String getAttributeValues11() {
        return attributeValues11;
    }
    /**
     * @param attributeValues11 the attributeValues11 to set
     */
    public void setAttributeValues11(String attributeValues11) {
        this.attributeValues11 = attributeValues11;
    }
    /**
     * @return the attributeDisplay11
     */
    public String getAttributeDisplay11() {
        return attributeDisplay11;
    }
    /**
     * @param attributeDisplay11 the attributeDisplay11 to set
     */
    public void setAttributeDisplay11(String attributeDisplay11) {
        this.attributeDisplay11 = attributeDisplay11;
    }
    /**
     * @return the attributeCode12
     */
    public int getAttributeCode12() {
        return attributeCode12;
    }
    /**
     * @param attributeCode12 the attributeCode12 to set
     */
    public void setAttributeCode12(int attributeCode12) {
        this.attributeCode12 = attributeCode12;
    }
    /**
     * @return the attributeValues12
     */
    public String getAttributeValues12() {
        return attributeValues12;
    }
    /**
     * @param attributeValues12 the attributeValues12 to set
     */
    public void setAttributeValues12(String attributeValues12) {
        this.attributeValues12 = attributeValues12;
    }
    /**
     * @return the attributeDisplay12
     */
    public String getAttributeDisplay12() {
        return attributeDisplay12;
    }
    /**
     * @param attributeDisplay12 the attributeDisplay12 to set
     */
    public void setAttributeDisplay12(String attributeDisplay12) {
        this.attributeDisplay12 = attributeDisplay12;
    }
    /**
     * @return the attributeCode13
     */
    public int getAttributeCode13() {
        return attributeCode13;
    }
    /**
     * @param attributeCode13 the attributeCode13 to set
     */
    public void setAttributeCode13(int attributeCode13) {
        this.attributeCode13 = attributeCode13;
    }
    /**
     * @return the attributeValues13
     */
    public String getAttributeValues13() {
        return attributeValues13;
    }
    /**
     * @param attributeValues13 the attributeValues13 to set
     */
    public void setAttributeValues13(String attributeValues13) {
        this.attributeValues13 = attributeValues13;
    }
    /**
     * @return the attributeDisplay13
     */
    public String getAttributeDisplay13() {
        return attributeDisplay13;
    }
    /**
     * @param attributeDisplay13 the attributeDisplay13 to set
     */
    public void setAttributeDisplay13(String attributeDisplay13) {
        this.attributeDisplay13 = attributeDisplay13;
    }
    /**
     * @return the attributeCode14
     */
    public int getAttributeCode14() {
        return attributeCode14;
    }
    /**
     * @param attributeCode14 the attributeCode14 to set
     */
    public void setAttributeCode14(int attributeCode14) {
        this.attributeCode14 = attributeCode14;
    }
    /**
     * @return the attributeValues14
     */
    public String getAttributeValues14() {
        return attributeValues14;
    }
    /**
     * @param attributeValues14 the attributeValues14 to set
     */
    public void setAttributeValues14(String attributeValues14) {
        this.attributeValues14 = attributeValues14;
    }
    /**
     * @return the attributeDisplay14
     */
    public String getAttributeDisplay14() {
        return attributeDisplay14;
    }
    /**
     * @param attributeDisplay14 the attributeDisplay14 to set
     */
    public void setAttributeDisplay14(String attributeDisplay14) {
        this.attributeDisplay14 = attributeDisplay14;
    }
    /**
     * @return the attributeCode15
     */
    public int getAttributeCode15() {
        return attributeCode15;
    }
    /**
     * @param attributeCode15 the attributeCode15 to set
     */
    public void setAttributeCode15(int attributeCode15) {
        this.attributeCode15 = attributeCode15;
    }
    /**
     * @return the attributeValues15
     */
    public String getAttributeValues15() {
        return attributeValues15;
    }
    /**
     * @param attributeValues15 the attributeValues15 to set
     */
    public void setAttributeValues15(String attributeValues15) {
        this.attributeValues15 = attributeValues15;
    }
    /**
     * @return the attributeDisplay15
     */
    public String getAttributeDisplay15() {
        return attributeDisplay15;
    }
    /**
     * @param attributeDisplay15 the attributeDisplay15 to set
     */
    public void setAttributeDisplay15(String attributeDisplay15) {
        this.attributeDisplay15 = attributeDisplay15;
    }
    /**
     * @return the attributeCode16
     */
    public int getAttributeCode16() {
        return attributeCode16;
    }
    /**
     * @param attributeCode16 the attributeCode16 to set
     */
    public void setAttributeCode16(int attributeCode16) {
        this.attributeCode16 = attributeCode16;
    }
    /**
     * @return the attributeValues16
     */
    public String getAttributeValues16() {
        return attributeValues16;
    }
    /**
     * @param attributeValues16 the attributeValues16 to set
     */
    public void setAttributeValues16(String attributeValues16) {
        this.attributeValues16 = attributeValues16;
    }
    /**
     * @return the attributeDisplay16
     */
    public String getAttributeDisplay16() {
        return attributeDisplay16;
    }
    /**
     * @param attributeDisplay16 the attributeDisplay16 to set
     */
    public void setAttributeDisplay16(String attributeDisplay16) {
        this.attributeDisplay16 = attributeDisplay16;
    }
    /**
     * @return the attributeCode17
     */
    public int getAttributeCode17() {
        return attributeCode17;
    }
    /**
     * @param attributeCode17 the attributeCode17 to set
     */
    public void setAttributeCode17(int attributeCode17) {
        this.attributeCode17 = attributeCode17;
    }
    /**
     * @return the attributeValues17
     */
    public String getAttributeValues17() {
        return attributeValues17;
    }
    /**
     * @param attributeValues17 the attributeValues17 to set
     */
    public void setAttributeValues17(String attributeValues17) {
        this.attributeValues17 = attributeValues17;
    }
    /**
     * @return the attributeDisplay17
     */
    public String getAttributeDisplay17() {
        return attributeDisplay17;
    }
    /**
     * @param attributeDisplay17 the attributeDisplay17 to set
     */
    public void setAttributeDisplay17(String attributeDisplay17) {
        this.attributeDisplay17 = attributeDisplay17;
    }
    /**
     * @return the attributeCode18
     */
    public int getAttributeCode18() {
        return attributeCode18;
    }
    /**
     * @param attributeCode18 the attributeCode18 to set
     */
    public void setAttributeCode18(int attributeCode18) {
        this.attributeCode18 = attributeCode18;
    }
    /**
     * @return the attributeValues18
     */
    public String getAttributeValues18() {
        return attributeValues18;
    }
    /**
     * @param attributeValues18 the attributeValues18 to set
     */
    public void setAttributeValues18(String attributeValues18) {
        this.attributeValues18 = attributeValues18;
    }
    /**
     * @return the attributeDisplay18
     */
    public String getAttributeDisplay18() {
        return attributeDisplay18;
    }
    /**
     * @param attributeDisplay18 the attributeDisplay18 to set
     */
    public void setAttributeDisplay18(String attributeDisplay18) {
        this.attributeDisplay18 = attributeDisplay18;
    }
    /**
     * @return the attributeCode19
     */
    public int getAttributeCode19() {
        return attributeCode19;
    }
    /**
     * @param attributeCode19 the attributeCode19 to set
     */
    public void setAttributeCode19(int attributeCode19) {
        this.attributeCode19 = attributeCode19;
    }
    /**
     * @return the attributeValues19
     */
    public String getAttributeValues19() {
        return attributeValues19;
    }
    /**
     * @param attributeValues19 the attributeValues19 to set
     */
    public void setAttributeValues19(String attributeValues19) {
        this.attributeValues19 = attributeValues19;
    }
    /**
     * @return the attributeDisplay19
     */
    public String getAttributeDisplay19() {
        return attributeDisplay19;
    }
    /**
     * @param attributeDisplay19 the attributeDisplay19 to set
     */
    public void setAttributeDisplay19(String attributeDisplay19) {
        this.attributeDisplay19 = attributeDisplay19;
    }
    /**
     * @return the attributeCode20
     */
    public int getAttributeCode20() {
        return attributeCode20;
    }
    /**
     * @param attributeCode20 the attributeCode20 to set
     */
    public void setAttributeCode20(int attributeCode20) {
        this.attributeCode20 = attributeCode20;
    }
    /**
     * @return the attributeValues20
     */
    public String getAttributeValues20() {
        return attributeValues20;
    }
    /**
     * @param attributeValues20 the attributeValues20 to set
     */
    public void setAttributeValues20(String attributeValues20) {
        this.attributeValues20 = attributeValues20;
    }
    /**
     * @return the attributeDisplay20
     */
    public String getAttributeDisplay20() {
        return attributeDisplay20;
    }
    /**
     * @param attributeDisplay20 the attributeDisplay20 to set
     */
    public void setAttributeDisplay20(String attributeDisplay20) {
        this.attributeDisplay20 = attributeDisplay20;
    }
    /**
     * @return the attributeCode21
     */
    public int getAttributeCode21() {
        return attributeCode21;
    }
    /**
     * @param attributeCode21 the attributeCode21 to set
     */
    public void setAttributeCode21(int attributeCode21) {
        this.attributeCode21 = attributeCode21;
    }
    /**
     * @return the attributeValues21
     */
    public String getAttributeValues21() {
        return attributeValues21;
    }
    /**
     * @param attributeValues21 the attributeValues21 to set
     */
    public void setAttributeValues21(String attributeValues21) {
        this.attributeValues21 = attributeValues21;
    }
    /**
     * @return the attributeDisplay21
     */
    public String getAttributeDisplay21() {
        return attributeDisplay21;
    }
    /**
     * @param attributeDisplay21 the attributeDisplay21 to set
     */
    public void setAttributeDisplay21(String attributeDisplay21) {
        this.attributeDisplay21 = attributeDisplay21;
    }
    /**
     * @return the attributeCode22
     */
    public int getAttributeCode22() {
        return attributeCode22;
    }
    /**
     * @param attributeCode22 the attributeCode22 to set
     */
    public void setAttributeCode22(int attributeCode22) {
        this.attributeCode22 = attributeCode22;
    }
    /**
     * @return the attributeValues22
     */
    public String getAttributeValues22() {
        return attributeValues22;
    }
    /**
     * @param attributeValues22 the attributeValues22 to set
     */
    public void setAttributeValues22(String attributeValues22) {
        this.attributeValues22 = attributeValues22;
    }
    /**
     * @return the attributeDisplay22
     */
    public String getAttributeDisplay22() {
        return attributeDisplay22;
    }
    /**
     * @param attributeDisplay22 the attributeDisplay22 to set
     */
    public void setAttributeDisplay22(String attributeDisplay22) {
        this.attributeDisplay22 = attributeDisplay22;
    }
    /**
     * @return the attributeCode23
     */
    public int getAttributeCode23() {
        return attributeCode23;
    }
    /**
     * @param attributeCode23 the attributeCode23 to set
     */
    public void setAttributeCode23(int attributeCode23) {
        this.attributeCode23 = attributeCode23;
    }
    /**
     * @return the attributeValues23
     */
    public String getAttributeValues23() {
        return attributeValues23;
    }
    /**
     * @param attributeValues23 the attributeValues23 to set
     */
    public void setAttributeValues23(String attributeValues23) {
        this.attributeValues23 = attributeValues23;
    }
    /**
     * @return the attributeDisplay23
     */
    public String getAttributeDisplay23() {
        return attributeDisplay23;
    }
    /**
     * @param attributeDisplay23 the attributeDisplay23 to set
     */
    public void setAttributeDisplay23(String attributeDisplay23) {
        this.attributeDisplay23 = attributeDisplay23;
    }
    /**
     * @return the attributeCode24
     */
    public int getAttributeCode24() {
        return attributeCode24;
    }
    /**
     * @param attributeCode24 the attributeCode24 to set
     */
    public void setAttributeCode24(int attributeCode24) {
        this.attributeCode24 = attributeCode24;
    }
    /**
     * @return the attributeValues24
     */
    public String getAttributeValues24() {
        return attributeValues24;
    }
    /**
     * @param attributeValues24 the attributeValues24 to set
     */
    public void setAttributeValues24(String attributeValues24) {
        this.attributeValues24 = attributeValues24;
    }
    /**
     * @return the attributeDisplay24
     */
    public String getAttributeDisplay24() {
        return attributeDisplay24;
    }
    /**
     * @param attributeDisplay24 the attributeDisplay24 to set
     */
    public void setAttributeDisplay24(String attributeDisplay24) {
        this.attributeDisplay24 = attributeDisplay24;
    }
    /**
     * @return the attributeCode25
     */
    public int getAttributeCode25() {
        return attributeCode25;
    }
    /**
     * @param attributeCode25 the attributeCode25 to set
     */
    public void setAttributeCode25(int attributeCode25) {
        this.attributeCode25 = attributeCode25;
    }
    /**
     * @return the attributeValues25
     */
    public String getAttributeValues25() {
        return attributeValues25;
    }
    /**
     * @param attributeValues25 the attributeValues25 to set
     */
    public void setAttributeValues25(String attributeValues25) {
        this.attributeValues25 = attributeValues25;
    }
    /**
     * @return the attributeDisplay25
     */
    public String getAttributeDisplay25() {
        return attributeDisplay25;
    }
    /**
     * @param attributeDisplay25 the attributeDisplay25 to set
     */
    public void setAttributeDisplay25(String attributeDisplay25) {
        this.attributeDisplay25 = attributeDisplay25;
    }
    /**
     * @return the attributeCode26
     */
    public int getAttributeCode26() {
        return attributeCode26;
    }
    /**
     * @param attributeCode26 the attributeCode26 to set
     */
    public void setAttributeCode26(int attributeCode26) {
        this.attributeCode26 = attributeCode26;
    }
    /**
     * @return the attributeValues26
     */
    public String getAttributeValues26() {
        return attributeValues26;
    }
    /**
     * @param attributeValues26 the attributeValues26 to set
     */
    public void setAttributeValues26(String attributeValues26) {
        this.attributeValues26 = attributeValues26;
    }
    /**
     * @return the attributeDisplay26
     */
    public String getAttributeDisplay26() {
        return attributeDisplay26;
    }
    /**
     * @param attributeDisplay26 the attributeDisplay26 to set
     */
    public void setAttributeDisplay26(String attributeDisplay26) {
        this.attributeDisplay26 = attributeDisplay26;
    }
    /**
     * @return the attributeCode27
     */
    public int getAttributeCode27() {
        return attributeCode27;
    }
    /**
     * @param attributeCode27 the attributeCode27 to set
     */
    public void setAttributeCode27(int attributeCode27) {
        this.attributeCode27 = attributeCode27;
    }
    /**
     * @return the attributeValues27
     */
    public String getAttributeValues27() {
        return attributeValues27;
    }
    /**
     * @param attributeValues27 the attributeValues27 to set
     */
    public void setAttributeValues27(String attributeValues27) {
        this.attributeValues27 = attributeValues27;
    }
    /**
     * @return the attributeDisplay27
     */
    public String getAttributeDisplay27() {
        return attributeDisplay27;
    }
    /**
     * @param attributeDisplay27 the attributeDisplay27 to set
     */
    public void setAttributeDisplay27(String attributeDisplay27) {
        this.attributeDisplay27 = attributeDisplay27;
    }
    /**
     * @return the attributeCode28
     */
    public int getAttributeCode28() {
        return attributeCode28;
    }
    /**
     * @param attributeCode28 the attributeCode28 to set
     */
    public void setAttributeCode28(int attributeCode28) {
        this.attributeCode28 = attributeCode28;
    }
    /**
     * @return the attributeValues28
     */
    public String getAttributeValues28() {
        return attributeValues28;
    }
    /**
     * @param attributeValues28 the attributeValues28 to set
     */
    public void setAttributeValues28(String attributeValues28) {
        this.attributeValues28 = attributeValues28;
    }
    /**
     * @return the attributeDisplay28
     */
    public String getAttributeDisplay28() {
        return attributeDisplay28;
    }
    /**
     * @param attributeDisplay28 the attributeDisplay28 to set
     */
    public void setAttributeDisplay28(String attributeDisplay28) {
        this.attributeDisplay28 = attributeDisplay28;
    }
    /**
     * @return the attributeCode29
     */
    public int getAttributeCode29() {
        return attributeCode29;
    }
    /**
     * @param attributeCode29 the attributeCode29 to set
     */
    public void setAttributeCode29(int attributeCode29) {
        this.attributeCode29 = attributeCode29;
    }
    /**
     * @return the attributeValues29
     */
    public String getAttributeValues29() {
        return attributeValues29;
    }
    /**
     * @param attributeValues29 the attributeValues29 to set
     */
    public void setAttributeValues29(String attributeValues29) {
        this.attributeValues29 = attributeValues29;
    }
    /**
     * @return the attributeDisplay29
     */
    public String getAttributeDisplay29() {
        return attributeDisplay29;
    }
    /**
     * @param attributeDisplay29 the attributeDisplay29 to set
     */
    public void setAttributeDisplay29(String attributeDisplay29) {
        this.attributeDisplay29 = attributeDisplay29;
    }
    /**
     * @return the attributeCode30
     */
    public int getAttributeCode30() {
        return attributeCode30;
    }
    /**
     * @param attributeCode30 the attributeCode30 to set
     */
    public void setAttributeCode30(int attributeCode30) {
        this.attributeCode30 = attributeCode30;
    }
    /**
     * @return the attributeValues30
     */
    public String getAttributeValues30() {
        return attributeValues30;
    }
    /**
     * @param attributeValues30 the attributeValues30 to set
     */
    public void setAttributeValues30(String attributeValues30) {
        this.attributeValues30 = attributeValues30;
    }
    /**
     * @return the attributeDisplay30
     */
    public String getAttributeDisplay30() {
        return attributeDisplay30;
    }
    /**
     * @param attributeDisplay30 the attributeDisplay30 to set
     */
    public void setAttributeDisplay30(String attributeDisplay30) {
        this.attributeDisplay30 = attributeDisplay30;
    }
    /**
     * @return the attributeName
     */
    public String getAttributeName() {
        return attributeName;
    }
    /**
     * @param attributeName the attributeName to set
     */
    public void setAttributeName(String attributeName) {
        this.attributeName = attributeName;
    }
}
