/************************************************************************
 * File Name    ： EntCSV.java
 * Author       ： Nguyen.Chuong
 * Version      ： 1.0.0
 * Date Created ： 2014/02/25
 * Date Updated ： 2014/02/25
 * Description  ： Entity for CSV import/Export
 ***********************************************************************/
package net.webike.RcProductFactory.entity;
/**
 * Entity for CSV import/Export.
 */
public class EntCSV {
    private long productId;
    private long productSyouhinSysCode;
    private int productBrandCode;
    private int productCategoryCode;
    private String productName;
    private int productCode;
    private boolean productWebikeCodeFlg;
    private int productEanCode;
    private int productProperPrice;
    private long productGroupCode;
    private int productSupplierReleaseDate;
    private boolean productOpenPriceFlg;
    private boolean productProperSellingFlg;
    private boolean productNoReturnableFlg;
    private boolean productOrderProductFlg;
    private boolean productAmbigousImageFlg;
    private String descriptionSummary;
    private String  descriptionRemarks;
    private String descriptionCaution;
    private String descriptionSentence;
    private int siireCode1;
    private int noukiCode1;
    private int siireCode2;
    private int noukiCode2;
    private String fitModelStr;

    private String productImageDetailPath1;
    private String productImageThumbnailPath1;

    private String productImageDetailPath2;
    private String productImageThumbnailPath2;

    private String productImageDetailPath3;
    private String productImageThumbnailPath3;

    private String productImageDetailPath4;
    private String productImageThumbnailPath4;

    private String productImageDetailPath5;
    private String productImageThumbnailPath5;

    private String productImageDetailPath6;
    private String productImageThumbnailPath6;

    private String productImageDetailPath7;
    private String productImageThumbnailPath7;

    private String productImageDetailPath8;
    private String productImageThumbnailPath8;

    private String productImageDetailPath9;
    private String productImageThumbnailPath9;

    private String productImageDetailPath10;
    private String productImageThumbnailPath10;

    private String productSelectDisplay;
    private String productSelectValue;

    private String productLinkReasonCode;
    private String productLinkTitle;
    private String productLinkURL;

    private String productVideoTitle;
    private String productVideoURL;
    private String productVideoDescription;

    private String attributeValue;
    private String attributeDisplay;
    /**
     * @return the productId
     */
    public long getProductId() {
        return productId;
    }
    /**
     * @param productId the productId to set
     */
    public void setProductId(long productId) {
        this.productId = productId;
    }
    /**
     * @return the productSyouhinSysCode
     */
    public long getProductSyouhinSysCode() {
        return productSyouhinSysCode;
    }
    /**
     * @param productSyouhinSysCode the productSyouhinSysCode to set
     */
    public void setProductSyouhinSysCode(long productSyouhinSysCode) {
        this.productSyouhinSysCode = productSyouhinSysCode;
    }
    /**
     * @return the productBrandCode
     */
    public int getProductBrandCode() {
        return productBrandCode;
    }
    /**
     * @param productBrandCode the productBrandCode to set
     */
    public void setProductBrandCode(int productBrandCode) {
        this.productBrandCode = productBrandCode;
    }
    /**
     * @return the productCategoryCode
     */
    public int getProductCategoryCode() {
        return productCategoryCode;
    }
    /**
     * @param productCategoryCode the productCategoryCode to set
     */
    public void setProductCategoryCode(int productCategoryCode) {
        this.productCategoryCode = productCategoryCode;
    }
    /**
     * @return the productName
     */
    public String getProductName() {
        return productName;
    }
    /**
     * @param productName the productName to set
     */
    public void setProductName(String productName) {
        this.productName = productName;
    }
    /**
     * @return the productCode
     */
    public int getProductCode() {
        return productCode;
    }
    /**
     * @param productCode the productCode to set
     */
    public void setProductCode(int productCode) {
        this.productCode = productCode;
    }
    /**
     * @return the productWebikeCodeFlg
     */
    public boolean isProductWebikeCodeFlg() {
        return productWebikeCodeFlg;
    }
    /**
     * @param productWebikeCodeFlg the productWebikeCodeFlg to set
     */
    public void setProductWebikeCodeFlg(boolean productWebikeCodeFlg) {
        this.productWebikeCodeFlg = productWebikeCodeFlg;
    }
    /**
     * @return the productEanCode
     */
    public int getProductEanCode() {
        return productEanCode;
    }
    /**
     * @param productEanCode the productEanCode to set
     */
    public void setProductEanCode(int productEanCode) {
        this.productEanCode = productEanCode;
    }
    /**
     * @return the productProperPrice
     */
    public int getProductProperPrice() {
        return productProperPrice;
    }
    /**
     * @param productProperPrice the productProperPrice to set
     */
    public void setProductProperPrice(int productProperPrice) {
        this.productProperPrice = productProperPrice;
    }
    /**
     * @return the productGroupCode
     */
    public long getProductGroupCode() {
        return productGroupCode;
    }
    /**
     * @param productGroupCode the productGroupCode to set
     */
    public void setProductGroupCode(long productGroupCode) {
        this.productGroupCode = productGroupCode;
    }
    /**
     * @return the productSupplierReleaseDate
     */
    public int getProductSupplierReleaseDate() {
        return productSupplierReleaseDate;
    }
    /**
     * @param productSupplierReleaseDate the productSupplierReleaseDate to set
     */
    public void setProductSupplierReleaseDate(int productSupplierReleaseDate) {
        this.productSupplierReleaseDate = productSupplierReleaseDate;
    }
    /**
     * @return the productOpenPriceFlg
     */
    public boolean isProductOpenPriceFlg() {
        return productOpenPriceFlg;
    }
    /**
     * @param productOpenPriceFlg the productOpenPriceFlg to set
     */
    public void setProductOpenPriceFlg(boolean productOpenPriceFlg) {
        this.productOpenPriceFlg = productOpenPriceFlg;
    }
    /**
     * @return the productProperSellingFlg
     */
    public boolean isProductProperSellingFlg() {
        return productProperSellingFlg;
    }
    /**
     * @param productProperSellingFlg the productProperSellingFlg to set
     */
    public void setProductProperSellingFlg(boolean productProperSellingFlg) {
        this.productProperSellingFlg = productProperSellingFlg;
    }
    /**
     * @return the productNoReturnableFlg
     */
    public boolean isProductNoReturnableFlg() {
        return productNoReturnableFlg;
    }
    /**
     * @param productNoReturnableFlg the productNoReturnableFlg to set
     */
    public void setProductNoReturnableFlg(boolean productNoReturnableFlg) {
        this.productNoReturnableFlg = productNoReturnableFlg;
    }
    /**
     * @return the productOrderProductFlg
     */
    public boolean isProductOrderProductFlg() {
        return productOrderProductFlg;
    }
    /**
     * @param productOrderProductFlg the productOrderProductFlg to set
     */
    public void setProductOrderProductFlg(boolean productOrderProductFlg) {
        this.productOrderProductFlg = productOrderProductFlg;
    }
    /**
     * @return the productAmbigousImageFlg
     */
    public boolean isProductAmbigousImageFlg() {
        return productAmbigousImageFlg;
    }
    /**
     * @param productAmbigousImageFlg the productAmbigousImageFlg to set
     */
    public void setProductAmbigousImageFlg(boolean productAmbigousImageFlg) {
        this.productAmbigousImageFlg = productAmbigousImageFlg;
    }
    /**
     * @return the descriptionSummary
     */
    public String getDescriptionSummary() {
        return descriptionSummary;
    }
    /**
     * @param descriptionSummary the descriptionSummary to set
     */
    public void setDescriptionSummary(String descriptionSummary) {
        this.descriptionSummary = descriptionSummary;
    }
    /**
     * @return the descriptionRemarks
     */
    public String getDescriptionRemarks() {
        return descriptionRemarks;
    }
    /**
     * @param descriptionRemarks the descriptionRemarks to set
     */
    public void setDescriptionRemarks(String descriptionRemarks) {
        this.descriptionRemarks = descriptionRemarks;
    }
    /**
     * @return the descriptionCaution
     */
    public String getDescriptionCaution() {
        return descriptionCaution;
    }
    /**
     * @param descriptionCaution the descriptionCaution to set
     */
    public void setDescriptionCaution(String descriptionCaution) {
        this.descriptionCaution = descriptionCaution;
    }
    /**
     * @return the descriptionSentence
     */
    public String getDescriptionSentence() {
        return descriptionSentence;
    }
    /**
     * @param descriptionSentence the descriptionSentence to set
     */
    public void setDescriptionSentence(String descriptionSentence) {
        this.descriptionSentence = descriptionSentence;
    }
    /**
     * @return the siireCode1
     */
    public int getSiireCode1() {
        return siireCode1;
    }
    /**
     * @param siireCode1 the siireCode1 to set
     */
    public void setSiireCode1(int siireCode1) {
        this.siireCode1 = siireCode1;
    }
    /**
     * @return the noukiCode1
     */
    public int getNoukiCode1() {
        return noukiCode1;
    }
    /**
     * @param noukiCode1 the noukiCode1 to set
     */
    public void setNoukiCode1(int noukiCode1) {
        this.noukiCode1 = noukiCode1;
    }
    /**
     * @return the siireCode2
     */
    public int getSiireCode2() {
        return siireCode2;
    }
    /**
     * @param siireCode2 the siireCode2 to set
     */
    public void setSiireCode2(int siireCode2) {
        this.siireCode2 = siireCode2;
    }
    /**
     * @return the noukiCode2
     */
    public int getNoukiCode2() {
        return noukiCode2;
    }
    /**
     * @param noukiCode2 the noukiCode2 to set
     */
    public void setNoukiCode2(int noukiCode2) {
        this.noukiCode2 = noukiCode2;
    }
    /**
     * @return the fitModelStr
     */
    public String getFitModelStr() {
        return fitModelStr;
    }
    /**
     * @param fitModelStr the fitModelStr to set
     */
    public void setFitModelStr(String fitModelStr) {
        this.fitModelStr = fitModelStr;
    }
    /**
     * @return the productImageDetailPath1
     */
    public String getProductImageDetailPath1() {
        return productImageDetailPath1;
    }
    /**
     * @param productImageDetailPath1 the productImageDetailPath1 to set
     */
    public void setProductImageDetailPath1(String productImageDetailPath1) {
        this.productImageDetailPath1 = productImageDetailPath1;
    }
    /**
     * @return the productImageThumbnailPath1
     */
    public String getProductImageThumbnailPath1() {
        return productImageThumbnailPath1;
    }
    /**
     * @param productImageThumbnailPath1 the productImageThumbnailPath1 to set
     */
    public void setProductImageThumbnailPath1(String productImageThumbnailPath1) {
        this.productImageThumbnailPath1 = productImageThumbnailPath1;
    }
    /**
     * @return the productImageDetailPath2
     */
    public String getProductImageDetailPath2() {
        return productImageDetailPath2;
    }
    /**
     * @param productImageDetailPath2 the productImageDetailPath2 to set
     */
    public void setProductImageDetailPath2(String productImageDetailPath2) {
        this.productImageDetailPath2 = productImageDetailPath2;
    }
    /**
     * @return the productImageThumbnailPath2
     */
    public String getProductImageThumbnailPath2() {
        return productImageThumbnailPath2;
    }
    /**
     * @param productImageThumbnailPath2 the productImageThumbnailPath2 to set
     */
    public void setProductImageThumbnailPath2(String productImageThumbnailPath2) {
        this.productImageThumbnailPath2 = productImageThumbnailPath2;
    }
    /**
     * @return the productImageDetailPath3
     */
    public String getProductImageDetailPath3() {
        return productImageDetailPath3;
    }
    /**
     * @param productImageDetailPath3 the productImageDetailPath3 to set
     */
    public void setProductImageDetailPath3(String productImageDetailPath3) {
        this.productImageDetailPath3 = productImageDetailPath3;
    }
    /**
     * @return the productImageThumbnailPath3
     */
    public String getProductImageThumbnailPath3() {
        return productImageThumbnailPath3;
    }
    /**
     * @param productImageThumbnailPath3 the productImageThumbnailPath3 to set
     */
    public void setProductImageThumbnailPath3(String productImageThumbnailPath3) {
        this.productImageThumbnailPath3 = productImageThumbnailPath3;
    }
    /**
     * @return the productImageDetailPath4
     */
    public String getProductImageDetailPath4() {
        return productImageDetailPath4;
    }
    /**
     * @param productImageDetailPath4 the productImageDetailPath4 to set
     */
    public void setProductImageDetailPath4(String productImageDetailPath4) {
        this.productImageDetailPath4 = productImageDetailPath4;
    }
    /**
     * @return the productImageThumbnailPath4
     */
    public String getProductImageThumbnailPath4() {
        return productImageThumbnailPath4;
    }
    /**
     * @param productImageThumbnailPath4 the productImageThumbnailPath4 to set
     */
    public void setProductImageThumbnailPath4(String productImageThumbnailPath4) {
        this.productImageThumbnailPath4 = productImageThumbnailPath4;
    }
    /**
     * @return the productImageDetailPath5
     */
    public String getProductImageDetailPath5() {
        return productImageDetailPath5;
    }
    /**
     * @param productImageDetailPath5 the productImageDetailPath5 to set
     */
    public void setProductImageDetailPath5(String productImageDetailPath5) {
        this.productImageDetailPath5 = productImageDetailPath5;
    }
    /**
     * @return the productImageThumbnailPath5
     */
    public String getProductImageThumbnailPath5() {
        return productImageThumbnailPath5;
    }
    /**
     * @param productImageThumbnailPath5 the productImageThumbnailPath5 to set
     */
    public void setProductImageThumbnailPath5(String productImageThumbnailPath5) {
        this.productImageThumbnailPath5 = productImageThumbnailPath5;
    }
    /**
     * @return the productImageDetailPath6
     */
    public String getProductImageDetailPath6() {
        return productImageDetailPath6;
    }
    /**
     * @param productImageDetailPath6 the productImageDetailPath6 to set
     */
    public void setProductImageDetailPath6(String productImageDetailPath6) {
        this.productImageDetailPath6 = productImageDetailPath6;
    }
    /**
     * @return the productImageThumbnailPath6
     */
    public String getProductImageThumbnailPath6() {
        return productImageThumbnailPath6;
    }
    /**
     * @param productImageThumbnailPath6 the productImageThumbnailPath6 to set
     */
    public void setProductImageThumbnailPath6(String productImageThumbnailPath6) {
        this.productImageThumbnailPath6 = productImageThumbnailPath6;
    }
    /**
     * @return the productImageDetailPath7
     */
    public String getProductImageDetailPath7() {
        return productImageDetailPath7;
    }
    /**
     * @param productImageDetailPath7 the productImageDetailPath7 to set
     */
    public void setProductImageDetailPath7(String productImageDetailPath7) {
        this.productImageDetailPath7 = productImageDetailPath7;
    }
    /**
     * @return the productImageThumbnailPath7
     */
    public String getProductImageThumbnailPath7() {
        return productImageThumbnailPath7;
    }
    /**
     * @param productImageThumbnailPath7 the productImageThumbnailPath7 to set
     */
    public void setProductImageThumbnailPath7(String productImageThumbnailPath7) {
        this.productImageThumbnailPath7 = productImageThumbnailPath7;
    }
    /**
     * @return the productImageDetailPath8
     */
    public String getProductImageDetailPath8() {
        return productImageDetailPath8;
    }
    /**
     * @param productImageDetailPath8 the productImageDetailPath8 to set
     */
    public void setProductImageDetailPath8(String productImageDetailPath8) {
        this.productImageDetailPath8 = productImageDetailPath8;
    }
    /**
     * @return the productImageThumbnailPath8
     */
    public String getProductImageThumbnailPath8() {
        return productImageThumbnailPath8;
    }
    /**
     * @param productImageThumbnailPath8 the productImageThumbnailPath8 to set
     */
    public void setProductImageThumbnailPath8(String productImageThumbnailPath8) {
        this.productImageThumbnailPath8 = productImageThumbnailPath8;
    }
    /**
     * @return the productImageDetailPath9
     */
    public String getProductImageDetailPath9() {
        return productImageDetailPath9;
    }
    /**
     * @param productImageDetailPath9 the productImageDetailPath9 to set
     */
    public void setProductImageDetailPath9(String productImageDetailPath9) {
        this.productImageDetailPath9 = productImageDetailPath9;
    }
    /**
     * @return the productImageThumbnailPath9
     */
    public String getProductImageThumbnailPath9() {
        return productImageThumbnailPath9;
    }
    /**
     * @param productImageThumbnailPath9 the productImageThumbnailPath9 to set
     */
    public void setProductImageThumbnailPath9(String productImageThumbnailPath9) {
        this.productImageThumbnailPath9 = productImageThumbnailPath9;
    }
    /**
     * @return the productImageDetailPath10
     */
    public String getProductImageDetailPath10() {
        return productImageDetailPath10;
    }
    /**
     * @param productImageDetailPath10 the productImageDetailPath10 to set
     */
    public void setProductImageDetailPath10(String productImageDetailPath10) {
        this.productImageDetailPath10 = productImageDetailPath10;
    }
    /**
     * @return the productImageThumbnailPath10
     */
    public String getProductImageThumbnailPath10() {
        return productImageThumbnailPath10;
    }
    /**
     * @param productImageThumbnailPath10 the productImageThumbnailPath10 to set
     */
    public void setProductImageThumbnailPath10(String productImageThumbnailPath10) {
        this.productImageThumbnailPath10 = productImageThumbnailPath10;
    }
    /**
     * @return the productSelectDisplay
     */
    public String getProductSelectDisplay() {
        return productSelectDisplay;
    }
    /**
     * @param productSelectDisplay the productSelectDisplay to set
     */
    public void setProductSelectDisplay(String productSelectDisplay) {
        this.productSelectDisplay = productSelectDisplay;
    }
    /**
     * @return the productSelectValue
     */
    public String getProductSelectValue() {
        return productSelectValue;
    }
    /**
     * @param productSelectValue the productSelectValue to set
     */
    public void setProductSelectValue(String productSelectValue) {
        this.productSelectValue = productSelectValue;
    }
    /**
     * @return the productLinkReasonCode
     */
    public String getProductLinkReasonCode() {
        return productLinkReasonCode;
    }
    /**
     * @param productLinkReasonCode the productLinkReasonCode to set
     */
    public void setProductLinkReasonCode(String productLinkReasonCode) {
        this.productLinkReasonCode = productLinkReasonCode;
    }
    /**
     * @return the productLinkTitle
     */
    public String getProductLinkTitle() {
        return productLinkTitle;
    }
    /**
     * @param productLinkTitle the productLinkTitle to set
     */
    public void setProductLinkTitle(String productLinkTitle) {
        this.productLinkTitle = productLinkTitle;
    }
    /**
     * @return the productLinkURL
     */
    public String getProductLinkURL() {
        return productLinkURL;
    }
    /**
     * @param productLinkURL the productLinkURL to set
     */
    public void setProductLinkURL(String productLinkURL) {
        this.productLinkURL = productLinkURL;
    }
    /**
     * @return the productVideoTitle
     */
    public String getProductVideoTitle() {
        return productVideoTitle;
    }
    /**
     * @param productVideoTitle the productVideoTitle to set
     */
    public void setProductVideoTitle(String productVideoTitle) {
        this.productVideoTitle = productVideoTitle;
    }
    /**
     * @return the productVideoURL
     */
    public String getProductVideoURL() {
        return productVideoURL;
    }
    /**
     * @param productVideoURL the productVideoURL to set
     */
    public void setProductVideoURL(String productVideoURL) {
        this.productVideoURL = productVideoURL;
    }
    /**
     * @return the productVideoDescription
     */
    public String getProductVideoDescription() {
        return productVideoDescription;
    }
    /**
     * @param productVideoDescription the productVideoDescription to set
     */
    public void setProductVideoDescription(String productVideoDescription) {
        this.productVideoDescription = productVideoDescription;
    }
    /**
     * @return the attributeValue
     */
    public String getAttributeValue() {
        return attributeValue;
    }
    /**
     * @param attributeValue the attributeValue to set
     */
    public void setAttributeValue(String attributeValue) {
        this.attributeValue = attributeValue;
    }
    /**
     * @return the attributeDisplay
     */
    public String getAttributeDisplay() {
        return attributeDisplay;
    }
    /**
     * @param attributeDisplay the attributeDisplay to set
     */
    public void setAttributeDisplay(String attributeDisplay) {
        this.attributeDisplay = attributeDisplay;
    }
}
