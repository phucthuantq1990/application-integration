/************************************************************************
 * file name	： EntMstProduct.java
 * author		： Nguyen.Chuong
 * version		： 1.0.0
 * date created	： 2014/02/15
 * date updated	： 2014/02/15
 * description	： Entity for table MstProduct in rc_syouhinh DB.
 *
 ***********************************************************************/
package net.webike.RcProductFactory.entity;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * EntMstProduct.
 */
public class EntMstProduct {
    //mst_product.product_code
    private String productCode;
    //product_brand.name
    private String brandName;
    //mst_product.product_brand_code
    private Integer brandCode;
    //mstProduct.syouhin_sys_code
    private Long syouhinSysCode;
    //mst_category.category_name
    private String categoryName;
    //mst_product.product_ean_code
    private String productEanCode;
    //mst_product.product_group_code
    private Long productGroupCode;
    //mst_product.product_proper_price
    private Integer productProperPrice;
    //tbl_sikiri_now.price
    private int sikiriPrice;
    // tbl_sikiri_now.price/ product_proper_price percentage
    private String sikiriProperPricePercentage;
    //mst_product.product_supplier_release_date
    private String productSupplierReleaseDate;
//    //mst_product.product_supplier_release_date
//    private Date productSupplierReleaseDate;
//    //mst_product.product_supplier_release_date format to Japan week part type
//    private String productSupplierReleaseDateStr;
    //tbl_product_condition.product_open_price_flg
    private boolean productOpenPriceFlg;
    //tbl_product_condition.product_proper_selling_flg
    private boolean productProperSellingFlg;
    //tbl_product_condition.product_no_returnable_flg
    private boolean productNoReturnableFlg;
    //tbl_product_condition.product_order_product_flg
    private boolean productOrderProductFlg;
    //tbl_product_condition.product_ambiguous_image_flg
    private boolean productAmbigousImageFlg;
    //tbl_syouhin_select.hyouji_name
    private String hyoujiName;
    //tbl_product_description.description_summary
    private String descriptionSummary;
    //tbl_product_description.description_remarks
    private String descriptionRemarks;
    //tbl_product_description.description_caution
    private String descriptionCaution;
    //tbl_product_description.description_sentence
    private String descriptionSentence;
    // mst_product.product_name
    private String productName;
    // tbl_sikiri_now.price / mst_product.product_proper_price
    private Double rate;
    // mst_siire.siire_name when tbl_syouhin_siire.syouhin_siire_order = 1
    private String siireName1;
    // mst_siire.siire_name when tbl_syouhin_siire.syouhin_siire_order = 2
    private String siireName2;
    // mst_product_supplier_status.supplier_status_name
    private String supplierStatusName;
    // mst_product_supplier_status.supplier_status_code
    private String supplierStatusCode;
    // mst_product_status.product_status_name
    private String productStatusName;
    // mst_product.product_del_flg
    private int productDelFlg;
    // update user full name
    private String fullName;
    // mst_product.updated_on
    private Date updatedOn;
    // mst_product.updated_user_id
    private String updatedUserId;
    // mst_product.created_on
    private Date createdOn;
    // mst_product.created_user_id
    private String createdUserId;
    // mst_product_status.product_status_can_edit_flg
    private int productStatusCanEditFlg;
    // mst_product_status.product_status_can_delete_flg
    private int productStatusCanDeleteFlg;
    // mst_product.product_maintenance_flg
    private int productMaintenanceFlg;
    // mst_product.product_brand_code
    private int productBrandCode;
    // rc_factory_product.tbl_factory_product.product_id
    private long productId;
    // sort field
    private String sortField;
    // sort description
    private String sortDir;

    /* BOE hoang.ho 2014/03/03 */
    // productDelFlgStatus
    private int productDelFlgStatus;
    /**
     * updateMode are:  'n' is new.
     *                  'u' is update
     *                  'd' is delete
     */
    private String updateMode;
    private Integer fitModelSort;
    private String fitModelMaker;
    private String fitModelModel;
    private String fitModelStyle;
    private String productIdList;
    /* EOE hoang.ho 2014/03/03 */

    private Integer productCategoryCode;
    //BOE @rcv! Luong.Dai 2014/08/08: chagne siireCode to string
    /*private Integer siireCode;*/
    private String siireCode;
    //EOE @rcv! Luong.Dai 2014/08/08: chagne siireCode to string
    //BOE #12097 2014/09/10 Nguyen.Chuong: add maker filter in detail search.
    private String maker;
    //EOE #12097 2014/09/10 Nguyen.Chuong: add maker filter in detail search.
    private String model;
    private String style;
    private Integer selectCode;
    private String thumbnail;
    private String image;
    private String noukiCode;
    private Integer attributeCode;
    private String attributeDisplayName;
    private String attributeManagementValue;
    private boolean attributeManagementBooleanValue;

    /*
     * 1: String
     * 2: Boolean
     */
    private Integer attributeManagementValueType;

    private String linkTitle;
    private String videoTitle;
    private List<String> variousFlag;
    private Integer fromPartition;
    private Integer toPartition;
    private String createdOnFrom;
    private String createdOnTo;

    /* Used to determine which criteria is searched. */
    private int multiSearchWhich;

    /* Used to store multi search criteria list. */
    private List<String> multiSearchCriteriaList;

    /* Used to check if detailSearch's done before.
     * 1: Initial filtering.
     * 2: Detail search only.
     * 3: Detail search then filtering.
     * */
    private int detailSearchFlag;

    /*
     * These flags used to set "==", "LIKE", "NOT LIKE". Used in detail search dialog's search option.
     * 1: EQUAL
     * 2: LIKE
     * 3: NOT LIKE
     */
    private int productNameSearchOption;
    private int makerStockNumberSearchOption;
    private int janSearchOption;
    private int groupCodeSearchOption;
    private int noteSearchOption;
    private int summarySearchOption;
    private int explanationSearchOption;
    private int cautionSearchOption;
    //BOE #12097 2014/09/10 Nguyen.Chuong: add maker filter in detail search.
    private int makerSearchOption;
    //EOE #12097 2014/09/10 Nguyen.Chuong: add maker filter in detail search.
    private int supportedBikeSearchOption;
    private int bikeModelSearchOption;
    private int optionSearchOption;
    private int thumbnailImagePathSearchOption;
    private int detailedImagePathSearchOption;
    private int attributeDisplayNameSearchOption;
    private int attributeManagementValueSearchOption;
    private int linkTitleSearchOption;
    private int videoTitleSearchOption;

    /*
     * Whether search condition.
     */
    private boolean janWhether;
    private boolean noteWhether;
    private boolean summaryWhether;
    private boolean explanationWhether;
    private boolean cautionWhether;
    //BOE #12097 2014/09/10 Nguyen.Chuong: add maker filter in detail search.
    private boolean makerWhether;
    //EOE #12097 2014/09/10 Nguyen.Chuong: add maker filter in detail search.
    private boolean supportedBikeWhether;
    private boolean bikeModelWhether;
    private boolean optionWhether;
    private boolean thumbnailImagePathWhether;
    private boolean detailedImagePathWhether;
    private boolean attributeDisplayNameWhether;
    private boolean attributeManagementValueWhether;
    private boolean linkTitleWhether;
    private boolean videoTitleWhether;
    private boolean productRegistrationDateIncluded;
    private boolean productRenovationDateIncluded;
    private boolean lastUpdatedPersonIncluded;

    /** BOE Data for search condition.*/
    private String updatedOnFrom;
    private String updatedOnTo;
    private Integer productProperPriceFrom;
    private Integer productProperPriceTo;
    private Double rateFrom;
    private Double rateTo;
    /** EOE Data for search condition*/
    private EntTblFactoryProductNew matterInfo;
    //if product has maintenance product at rc_factory_product: true ? false
    private boolean hasMaintenanceProduct;

    /**
     * @return the productCode
     */
    public String getProductCode() {
        return productCode;
    }
    /**
     * @param productCode the productCode to set
     */
    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }
    /**
     * @return the syouhinSysCode
     */
    public Long getSyouhinSysCode() {
        return syouhinSysCode;
    }
    /**
     * @param syouhinSysCode the syouhinSysCode to set
     */
    public void setSyouhinSysCode(Long syouhinSysCode) {
        this.syouhinSysCode = syouhinSysCode;
    }
    /**
     * @return the categoryName
     */
    public String getCategoryName() {
        return categoryName;
    }
    /**
     * @param categoryName the categoryName to set
     */
    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }
    /**
     * @return the productEanCode
     */
    public String getProductEanCode() {
        return productEanCode;
    }
    /**
     * @param productEanCode the productEanCode to set
     */
    public void setProductEanCode(String productEanCode) {
        this.productEanCode = productEanCode;
    }
    /**
     * @return the productGroupCode
     */
    public Long getProductGroupCode() {
        return productGroupCode;
    }
    /**
     * @param productGroupCode the productGroupCode to set
     */
    public void setProductGroupCode(Long productGroupCode) {
        this.productGroupCode = productGroupCode;
    }
    /**
     * @return the productProperPrice
     */
    public Integer getProductProperPrice() {
        return productProperPrice;
    }
    /**
     * @param productProperPrice the productProperPrice to set
     */
    public void setProductProperPrice(Integer productProperPrice) {
        this.productProperPrice = productProperPrice;
    }
    /**
     * @return the rate
     */
    public Double getRate() {
        return rate;
    }
    /**
     * @param rate the rate to set
     */
    public void setRate(Double rate) {
        this.rate = rate;
    }
    /**
     * @return the sikiriPrice
     */
    public int getSikiriPrice() {
        return sikiriPrice;
    }
    /**
     * @param sikiriPrice the sikiriPrice to set
     */
    public void setSikiriPrice(int sikiriPrice) {
        this.sikiriPrice = sikiriPrice;
    }
    /**
     * @return the productOpenPriceFlg
     */
    public boolean isProductOpenPriceFlg() {
        return productOpenPriceFlg;
    }
    /**
     * @param productOpenPriceFlg the productOpenPriceFlg to set
     */
    public void setProductOpenPriceFlg(boolean productOpenPriceFlg) {
        this.productOpenPriceFlg = productOpenPriceFlg;
    }
    /**
     * @return the productProperSellingFlg
     */
    public boolean isProductProperSellingFlg() {
        return productProperSellingFlg;
    }
    /**
     * @param productProperSellingFlg the productProperSellingFlg to set
     */
    public void setProductProperSellingFlg(boolean productProperSellingFlg) {
        this.productProperSellingFlg = productProperSellingFlg;
    }
    /**
     * @return the productNonReturnableFlg
     */
    public boolean isProductNoReturnableFlg() {
        return productNoReturnableFlg;
    }
    /**
     * @param productNoReturnableFlg the productNoReturnableFlg to set
     */
    public void setProductNoReturnableFlg(boolean productNoReturnableFlg) {
        this.productNoReturnableFlg = productNoReturnableFlg;
    }
    /**
     * @return the productAmbigousImageFlg
     */
    public boolean isProductAmbigousImageFlg() {
        return productAmbigousImageFlg;
    }
    /**
     * @param productAmbigousImageFlg the productAmbigousImageFlg to set
     */
    public void setProductAmbigousImageFlg(boolean productAmbigousImageFlg) {
        this.productAmbigousImageFlg = productAmbigousImageFlg;
    }
    /**
     * @return the hyoujiName
     */
    public String getHyoujiName() {
        return hyoujiName;
    }
    /**
     * @param hyoujiName the hyoujiName to set
     */
    public void setHyoujiName(String hyoujiName) {
        this.hyoujiName = hyoujiName;
    }
    /**
     * @return the descriptionSummary
     */
    public String getDescriptionSummary() {
        return descriptionSummary;
    }
    /**
     * @param descriptionSummary the descriptionSummary to set
     */
    public void setDescriptionSummary(String descriptionSummary) {
        this.descriptionSummary = descriptionSummary;
    }
    /**
     * @return the brandName
     */
    public String getBrandName() {
        return brandName;
    }
    /**
     * @param brandName the brandName to set
     */
    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }
    /**
     * @return the productName
     */
    public String getProductName() {
        return productName;
    }
    /**
     * @param productName the productName to set
     */
    public void setProductName(String productName) {
        this.productName = productName;
    }
    /**
     * @return the siireName1
     */
    public String getSiireName1() {
        return siireName1;
    }
    /**
     * @param siireName1 the siireName1 to set
     */
    public void setSiireName1(String siireName1) {
        this.siireName1 = siireName1;
    }
    /**
     * @return the siireName2
     */
    public String getSiireName2() {
        return siireName2;
    }
    /**
     * @param siireName2 the siireName2 to set
     */
    public void setSiireName2(String siireName2) {
        this.siireName2 = siireName2;
    }
    /**
     * @return the supplierStatusName
     */
    public String getSupplierStatusName() {
        return supplierStatusName;
    }
    /**
     * @param supplierStatusName the supplierStatusName to set
     */
    public void setSupplierStatusName(String supplierStatusName) {
        this.supplierStatusName = supplierStatusName;
    }
    /**
     * @return the productStatusName
     */
    public String getProductStatusName() {
        return productStatusName;
    }
    /**
     * @param productStatusName the productStatusName to set
     */
    public void setProductStatusName(String productStatusName) {
        this.productStatusName = productStatusName;
    }
    /**
     * @return the productDelFlg
     */
    public int getProductDelFlg() {
        return productDelFlg;
    }
    /**
     * @param productDelFlg the productDelFlg to set
     */
    public void setProductDelFlg(int productDelFlg) {
        this.productDelFlg = productDelFlg;
    }
    /**
     * @return the fullName
     */
    public String getFullName() {
        return fullName;
    }
    /**
     * @param fullName the fullName to set
     */
    public void setFullName(String fullName) {
        this.fullName = fullName;
    }
    /**
     * @return the updatedOn
     */
    public Date getUpdatedOn() {
        if (this.updatedOn != null) {
            return (Date) updatedOn.clone();
        }
        return null;
    }
    /**
     * @param updatedOn the updatedOn to set
     */
    public void setUpdatedOn(Date updatedOn) {
        if (updatedOn != null) {
            this.updatedOn = (Date) updatedOn.clone();
        } else {
            this.updatedOn = null;
        }
    }
    /**
     * @return the sortField
     */
    public String getSortField() {
        return sortField;
    }
    /**
     * @param sortField the sortField to set
     */
    public void setSortField(String sortField) {
        this.sortField = sortField;
    }
    /**
     * @return the sortDir
     */
    public String getSortDir() {
        return sortDir;
    }
    /**
     * @param sortDir the sortDir to set
     */
    public void setSortDir(String sortDir) {
        this.sortDir = sortDir;
    }
    /**
     * @return the descriptionRemarks
     */
    public String getDescriptionRemarks() {
        return descriptionRemarks;
    }
    /**
     * @param descriptionRemarks the descriptionRemarks to set
     */
    public void setDescriptionRemarks(String descriptionRemarks) {
        this.descriptionRemarks = descriptionRemarks;
    }
    /**
     * @return the descriptionCaution
     */
    public String getDescriptionCaution() {
        return descriptionCaution;
    }
    /**
     * @param descriptionCaution the descriptionCaution to set
     */
    public void setDescriptionCaution(String descriptionCaution) {
        this.descriptionCaution = descriptionCaution;
    }
    /**
     * @return the descriptionSentence
     */
    public String getDescriptionSentence() {
        return descriptionSentence;
    }
    /**
     * @param descriptionSentence the descriptionSentence to set
     */
    public void setDescriptionSentence(String descriptionSentence) {
        this.descriptionSentence = descriptionSentence;
    }
    /**
     * @return the sikiriProperPricePercentage
     */
    public String getSikiriProperPricePercentage() {
        return sikiriProperPricePercentage;
    }
    /**
     * @param sikiriProperPricePercentage the sikiriProperPricePercentage to set
     */
    public void setSikiriProperPricePercentage(
            String sikiriProperPricePercentage) {
        this.sikiriProperPricePercentage = sikiriProperPricePercentage;
    }
    /**
     * @return the productOrderProductFlg
     */
    public boolean isProductOrderProductFlg() {
        return productOrderProductFlg;
    }
    /**
     * @param productOrderProductFlg the productOrderProductFlg to set
     */
    public void setProductOrderProductFlg(boolean productOrderProductFlg) {
        this.productOrderProductFlg = productOrderProductFlg;
    }
    /**
     * @return the productStatusCanEditFlg
     */
    public int getProductStatusCanEditFlg() {
        return productStatusCanEditFlg;
    }
    /**
     * @param productStatusCanEditFlg the productStatusCanEditFlg to set
     */
    public void setProductStatusCanEditFlg(int productStatusCanEditFlg) {
        this.productStatusCanEditFlg = productStatusCanEditFlg;
    }
    /**
     * @return the productStatusCanDeleteFlg
     */
    public int getProductStatusCanDeleteFlg() {
        return productStatusCanDeleteFlg;
    }
    /**
     * @param productStatusCanDeleteFlg the productStatusCanDeleteFlg to set
     */
    public void setProductStatusCanDeleteFlg(int productStatusCanDeleteFlg) {
        this.productStatusCanDeleteFlg = productStatusCanDeleteFlg;
    }
    /************************************************************************
     * <b>Description:</b><br>
     * Get delFlag String: Yes or No.
     *
     * @author      Long Vu
     * @date        Feb 18, 2014
     * @return      String
     ************************************************************************/
    public String getDelFlgString() {
        String delFlgString = "";
        if (this.productDelFlg == 0) {
            delFlgString = "Yes";
        } else {
            delFlgString = "No";
        }
        return delFlgString;
    }

    /************************************************************************
     * <b>Description:</b><br>
     * Format date to String in formation: yyyy/MM/dd HH:mm:ss.
     *
     * @author      Long Vu
     * @date        Feb 18, 2014
     * @return      String
     ************************************************************************/
    public String getUpdatedOnString() {
        String result = "";
        if (this.updatedOn != null) {
            result = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(this.updatedOn);
        }
        return result;
    }
    /**
     * @return the updatedUserId
     */
    public String getUpdatedUserId() {
        return updatedUserId;
    }
    /**
     * @param updatedUserId the updatedUserId to set
     */
    public void setUpdatedUserId(String updatedUserId) {
        this.updatedUserId = updatedUserId;
    }
    /**
     * @return the supplierStatusCode
     */
    public String getSupplierStatusCode() {
        return supplierStatusCode;
    }
    /**
     * @param supplierStatusCode the supplierStatusCode to set
     */
    public void setSupplierStatusCode(String supplierStatusCode) {
        this.supplierStatusCode = supplierStatusCode;
    }

    public void setBrandCode(Integer brandCode) {
		this.brandCode = brandCode;
	}

    public Integer getBrandCode() {
		return brandCode;
	}

//    /**
//     * @return the productSupplierReleaseDate
//     */
//    public Date getProductSupplierReleaseDate() {
//        if (this.productSupplierReleaseDate != null) {
//            return (Date) productSupplierReleaseDate.clone();
//        }
//        return null;
//    }
//    /**
//     * @param productSupplierReleaseDate the productSupplierReleaseDate to set
//     */
//    public void setProductSupplierReleaseDate(Date productSupplierReleaseDate) {
//        if (productSupplierReleaseDate != null) {
//            this.productSupplierReleaseDate = (Date) productSupplierReleaseDate.clone();
//        } else {
//            this.productSupplierReleaseDate = null;
//        }
//    }
//    /**
//     * @return the productSupplierReleaseDateStr
//     */
//    public String getProductSupplierReleaseDateStr() {
//        return productSupplierReleaseDateStr;
//    }
//    /**
//     * @param productSupplierReleaseDateStr the productSupplierReleaseDateStr to set
//     */
//    public void setProductSupplierReleaseDateStr(
//            String productSupplierReleaseDateStr) {
//        this.productSupplierReleaseDateStr = productSupplierReleaseDateStr;
//    }
    /**
     * @return the productSupplierReleaseDate
     */
    public String getProductSupplierReleaseDate() {
        return productSupplierReleaseDate;
    }
    /**
     * @param productSupplierReleaseDate the productSupplierReleaseDate to set
     */
    public void setProductSupplierReleaseDate(String productSupplierReleaseDate) {
        this.productSupplierReleaseDate = productSupplierReleaseDate;
    }
    /**
     * @return the productMaintenanceFlg
     */
    public int getProductMaintenanceFlg() {
        return productMaintenanceFlg;
    }
    /**
     * @param productMaintenanceFlg the productMaintenanceFlg to set
     */
    public void setProductMaintenanceFlg(int productMaintenanceFlg) {
        this.productMaintenanceFlg = productMaintenanceFlg;
    }
    /**
     * @return the productBrandCode
     */
    public int getProductBrandCode() {
        return productBrandCode;
    }

    public void setProductBrandCode(int productBrandCode) {
		this.productBrandCode = productBrandCode;
	}

    public Integer getProductCategoryCode() {
		return productCategoryCode;
	}

	public void setProductCategoryCode(Integer productCategoryCode) {
		this.productCategoryCode = productCategoryCode;
	}

	public void setSiireCode(String siireCode) {
		this.siireCode = siireCode;
	}

	public String getSiireCode() {
		return siireCode;
	}

    public String getModel() {
		return model;
	}

    public void setModel(String model) {
		this.model = model;
	}

    public String getStyle() {
		return style;
	}

    public void setStyle(String style) {
		this.style = style;
	}

    public void setSelectCode(Integer selectCode) {
		this.selectCode = selectCode;
	}

    public Integer getSelectCode() {
		return selectCode;
	}

	/**
	 * @return the thumbnail
	 */
	public String getThumbnail() {
		return thumbnail;
	}
	/**
	 * @param thumbnail the thumbnail to set
	 */
	public void setThumbnail(String thumbnail) {
		this.thumbnail = thumbnail;
	}
	/**
	 * @return the image
	 */
	public String getImage() {
		return image;
	}
	/**
	 * @param image the image to set
	 */
	public void setImage(String image) {
		this.image = image;
	}
	/**
	 * @return the noukiCode
	 */
	public String getNoukiCode() {
		return noukiCode;
	}
	/**
	 * @param noukiCode the noukiCode to set
	 */
	public void setNoukiCode(String noukiCode) {
		this.noukiCode = noukiCode;
	}

	public void setAttributeCode(Integer attributeCode) {
		this.attributeCode = attributeCode;
	}

	public Integer getAttributeCode() {
		return attributeCode;
	}

	public String getAttributeDisplayName() {
		return attributeDisplayName;
	}
	public void setAttributeDisplayName(String attributeDisplayName) {
		this.attributeDisplayName = attributeDisplayName;
	}

	public void setAttributeManagementValue(String attributeManagementValue) {
		this.attributeManagementValue = attributeManagementValue;
	}

	public String getAttributeManagementValue() {
		return attributeManagementValue;
	}

	public String getLinkTitle() {
		return linkTitle;
	}

	public String getVideoTitle() {
		return videoTitle;
	}

	public void setLinkTitle(String linkTitle) {
		this.linkTitle = linkTitle;
	}

	public void setVideoTitle(String videoTitle) {
		this.videoTitle = videoTitle;
	}
    /**
     * @return the productId
     */
    public long getProductId() {
        return productId;
    }
    /**
     * @param productId the productId to set
     */
    public void setProductId(long productId) {
        this.productId = productId;
    }
    /**
     * @return the createdOn
     */
    public Date getCreatedOn() {
        if (this.createdOn != null) {
            return (Date) createdOn.clone();
        }
        return null;
    }
    /**
     * @param createdOn the createdOn to set
     */
    public void setCreatedOn(Date createdOn) {
        if (createdOn != null) {
            this.createdOn = (Date) createdOn.clone();
        } else {
            this.createdOn = null;
        }
    }
    /**
     * @return the createdUserId
     */
    public String getCreatedUserId() {
        return createdUserId;
    }
    /**
     * @param createdUserId the createdUserId to set
     */
    public void setCreatedUserId(String createdUserId) {
        this.createdUserId = createdUserId;
    }

	public void setAttributeManagementValueType(Integer attributeManagementValueType) {
		this.attributeManagementValueType = attributeManagementValueType;
	}

	public Integer getAttributeManagementValueType() {
		return attributeManagementValueType;
	}

    /**
     * @return the variousFlag
     */
    public List<String> getVariousFlag() {
        return variousFlag;
    }
    /**
     * @param variousFlag the variousFlag to set
     */
    public void setVariousFlag(List<String> variousFlag) {
        this.variousFlag = variousFlag;
    }

    /**
     * @return the multiSearchWhich
     */
    public int getMultiSearchWhich() {
        return multiSearchWhich;
    }
    /**
     * @param multiSearchWhich the multiSearchWhich to set
     */
    public void setMultiSearchWhich(int multiSearchWhich) {
        this.multiSearchWhich = multiSearchWhich;
    }
    /**
     * @return the multiSearchCriteriaList
     */
    public List<String> getMultiSearchCriteriaList() {
        return multiSearchCriteriaList;
    }
    /**
     * @param multiSearchCriteriaList the multiSearchCriteriaList to set
     */
    public void setMultiSearchCriteriaList(List<String> multiSearchCriteriaList) {
        this.multiSearchCriteriaList = multiSearchCriteriaList;
    }
    /**
     * @return the productNameSearchOption
     */
    public int getProductNameSearchOption() {
        return productNameSearchOption;
    }
    /**
     * @param productNameSearchOption the productNameSearchOption to set
     */
    public void setProductNameSearchOption(int productNameSearchOption) {
        this.productNameSearchOption = productNameSearchOption;
    }
    /**
     * @return the makerStockNumberSearchOption
     */
    public int getMakerStockNumberSearchOption() {
        return makerStockNumberSearchOption;
    }
    /**
     * @param makerStockNumberSearchOption the makerStockNumberSearchOption to set
     */
    public void setMakerStockNumberSearchOption(int makerStockNumberSearchOption) {
        this.makerStockNumberSearchOption = makerStockNumberSearchOption;
    }
    /**
     * @return the janSearchOption
     */
    public int getJanSearchOption() {
        return janSearchOption;
    }
    /**
     * @param janSearchOption the janSearchOption to set
     */
    public void setJanSearchOption(int janSearchOption) {
        this.janSearchOption = janSearchOption;
    }
    /**
     * @return the groupCodeSearchOption
     */
    public int getGroupCodeSearchOption() {
        return groupCodeSearchOption;
    }
    /**
     * @param groupCodeSearchOption the groupCodeSearchOption to set
     */
    public void setGroupCodeSearchOption(int groupCodeSearchOption) {
        this.groupCodeSearchOption = groupCodeSearchOption;
    }
    /**
     * @return the noteSearchOption
     */
    public int getNoteSearchOption() {
        return noteSearchOption;
    }
    /**
     * @param noteSearchOption the noteSearchOption to set
     */
    public void setNoteSearchOption(int noteSearchOption) {
        this.noteSearchOption = noteSearchOption;
    }
    /**
     * @return the summarySearchOption
     */
    public int getSummarySearchOption() {
        return summarySearchOption;
    }
    /**
     * @param summarySearchOption the summarySearchOption to set
     */
    public void setSummarySearchOption(int summarySearchOption) {
        this.summarySearchOption = summarySearchOption;
    }
    /**
     * @return the explanationSearchOption
     */
    public int getExplanationSearchOption() {
        return explanationSearchOption;
    }
    /**
     * @param explanationSearchOption the explanationSearchOption to set
     */
    public void setExplanationSearchOption(int explanationSearchOption) {
        this.explanationSearchOption = explanationSearchOption;
    }
    /**
     * @return the cautionSearchOption
     */
    public int getCautionSearchOption() {
        return cautionSearchOption;
    }
    /**
     * @param cautionSearchOption the cautionSearchOption to set
     */
    public void setCautionSearchOption(int cautionSearchOption) {
        this.cautionSearchOption = cautionSearchOption;
    }
    /**
     * @return the supportedBikeSearchOption
     */
    public int getSupportedBikeSearchOption() {
        return supportedBikeSearchOption;
    }
    /**
     * @param supportedBikeSearchOption the supportedBikeSearchOption to set
     */
    public void setSupportedBikeSearchOption(int supportedBikeSearchOption) {
        this.supportedBikeSearchOption = supportedBikeSearchOption;
    }
    /**
     * @return the bikeModelSearchOption
     */
    public int getBikeModelSearchOption() {
        return bikeModelSearchOption;
    }
    /**
     * @param bikeModelSearchOption the bikeModelSearchOption to set
     */
    public void setBikeModelSearchOption(int bikeModelSearchOption) {
        this.bikeModelSearchOption = bikeModelSearchOption;
    }
    /**
     * @return the optionSearchOption
     */
    public int getOptionSearchOption() {
        return optionSearchOption;
    }
    /**
     * @param optionSearchOption the optionSearchOption to set
     */
    public void setOptionSearchOption(int optionSearchOption) {
        this.optionSearchOption = optionSearchOption;
    }
    /**
     * @return the thumbnailImagePathSearchOption
     */
    public int getThumbnailImagePathSearchOption() {
        return thumbnailImagePathSearchOption;
    }
    /**
     * @param thumbnailImagePathSearchOption the thumbnailImagePathSearchOption to set
     */
    public void setThumbnailImagePathSearchOption(int thumbnailImagePathSearchOption) {
        this.thumbnailImagePathSearchOption = thumbnailImagePathSearchOption;
    }
    /**
     * @return the detailedImagePathSearchOption
     */
    public int getDetailedImagePathSearchOption() {
        return detailedImagePathSearchOption;
    }
    /**
     * @param detailedImagePathSearchOption the detailedImagePathSearchOption to set
     */
    public void setDetailedImagePathSearchOption(int detailedImagePathSearchOption) {
        this.detailedImagePathSearchOption = detailedImagePathSearchOption;
    }
    /**
     * @return the attributeDisplayNameSearchOption
     */
    public int getAttributeDisplayNameSearchOption() {
        return attributeDisplayNameSearchOption;
    }
    /**
     * @param attributeDisplayNameSearchOption the attributeDisplayNameSearchOption to set
     */
    public void setAttributeDisplayNameSearchOption(int attributeDisplayNameSearchOption) {
        this.attributeDisplayNameSearchOption = attributeDisplayNameSearchOption;
    }
    /**
     * @return the attributeManagementValueSearchOption
     */
    public int getAttributeManagementValueSearchOption() {
        return attributeManagementValueSearchOption;
    }
    /**
     * @param attributeManagementValueSearchOption the attributeManagementValueSearchOption to set
     */
    public void setAttributeManagementValueSearchOption(int attributeManagementValueSearchOption) {
        this.attributeManagementValueSearchOption = attributeManagementValueSearchOption;
    }
    /**
     * @return the linkTitleSearchOption
     */
    public int getLinkTitleSearchOption() {
        return linkTitleSearchOption;
    }
    /**
     * @param linkTitleSearchOption the linkTitleSearchOption to set
     */
    public void setLinkTitleSearchOption(int linkTitleSearchOption) {
        this.linkTitleSearchOption = linkTitleSearchOption;
    }
    /**
     * @return the videoTitleSearchOption
     */
    public int getVideoTitleSearchOption() {
        return videoTitleSearchOption;
    }
    /**
     * @param videoTitleSearchOption the videoTitleSearchOption to set
     */
    public void setVideoTitleSearchOption(int videoTitleSearchOption) {
        this.videoTitleSearchOption = videoTitleSearchOption;
    }
    /**
     * @return the janWhether
     */
    public boolean isJanWhether() {
        return janWhether;
    }
    /**
     * @param janWhether the janWhether to set
     */
    public void setJanWhether(boolean janWhether) {
        this.janWhether = janWhether;
    }
    /**
     * @return the noteWhether
     */
    public boolean isNoteWhether() {
        return noteWhether;
    }
    /**
     * @param noteWhether the noteWhether to set
     */
    public void setNoteWhether(boolean noteWhether) {
        this.noteWhether = noteWhether;
    }
    /**
     * @return the summaryWhether
     */
    public boolean isSummaryWhether() {
        return summaryWhether;
    }
    /**
     * @param summaryWhether the summaryWhether to set
     */
    public void setSummaryWhether(boolean summaryWhether) {
        this.summaryWhether = summaryWhether;
    }
    /**
     * @return the explanationWhether
     */
    public boolean isExplanationWhether() {
        return explanationWhether;
    }
    /**
     * @param explanationWhether the explanationWhether to set
     */
    public void setExplanationWhether(boolean explanationWhether) {
        this.explanationWhether = explanationWhether;
    }
    /**
     * @return the cautionWhether
     */
    public boolean isCautionWhether() {
        return cautionWhether;
    }
    /**
     * @param cautionWhether the cautionWhether to set
     */
    public void setCautionWhether(boolean cautionWhether) {
        this.cautionWhether = cautionWhether;
    }
    /**
     * @return the supportedBikeWhether
     */
    public boolean isSupportedBikeWhether() {
        return supportedBikeWhether;
    }
    /**
     * @param supportedBikeWhether the supportedBikeWhether to set
     */
    public void setSupportedBikeWhether(boolean supportedBikeWhether) {
        this.supportedBikeWhether = supportedBikeWhether;
    }
    /**
     * @return the bikeModelWhether
     */
    public boolean isBikeModelWhether() {
        return bikeModelWhether;
    }
    /**
     * @param bikeModelWhether the bikeModelWhether to set
     */
    public void setBikeModelWhether(boolean bikeModelWhether) {
        this.bikeModelWhether = bikeModelWhether;
    }
    /**
     * @return the optionWhether
     */
    public boolean isOptionWhether() {
        return optionWhether;
    }
    /**
     * @param optionWhether the optionWhether to set
     */
    public void setOptionWhether(boolean optionWhether) {
        this.optionWhether = optionWhether;
    }
    /**
     * @return the thumbnailImagePathWhether
     */
    public boolean isThumbnailImagePathWhether() {
        return thumbnailImagePathWhether;
    }
    /**
     * @param thumbnailImagePathWhether the thumbnailImagePathWhether to set
     */
    public void setThumbnailImagePathWhether(boolean thumbnailImagePathWhether) {
        this.thumbnailImagePathWhether = thumbnailImagePathWhether;
    }
    /**
     * @return the detailedImagePathWhether
     */
    public boolean isDetailedImagePathWhether() {
        return detailedImagePathWhether;
    }
    /**
     * @param detailedImagePathWhether the detailedImagePathWhether to set
     */
    public void setDetailedImagePathWhether(boolean detailedImagePathWhether) {
        this.detailedImagePathWhether = detailedImagePathWhether;
    }
    /**
     * @return the attributeDisplayNameWhether
     */
    public boolean isAttributeDisplayNameWhether() {
        return attributeDisplayNameWhether;
    }
    /**
     * @param attributeDisplayNameWhether the attributeDisplayNameWhether to set
     */
    public void setAttributeDisplayNameWhether(boolean attributeDisplayNameWhether) {
        this.attributeDisplayNameWhether = attributeDisplayNameWhether;
    }
    /**
     * @return the attributeManagementValueWhether
     */
    public boolean isAttributeManagementValueWhether() {
        return attributeManagementValueWhether;
    }
    /**
     * @param attributeManagementValueWhether the attributeManagementValueWhether to set
     */
    public void setAttributeManagementValueWhether(boolean attributeManagementValueWhether) {
        this.attributeManagementValueWhether = attributeManagementValueWhether;
    }
    /**
     * @return the linkTitleWhether
     */
    public boolean isLinkTitleWhether() {
        return linkTitleWhether;
    }
    /**
     * @param linkTitleWhether the linkTitleWhether to set
     */
    public void setLinkTitleWhether(boolean linkTitleWhether) {
        this.linkTitleWhether = linkTitleWhether;
    }
    /**
     * @return the videoTitleWhether
     */
    public boolean isVideoTitleWhether() {
        return videoTitleWhether;
    }
    /**
     * @param videoTitleWhether the videoTitleWhether to set
     */
    public void setVideoTitleWhether(boolean videoTitleWhether) {
        this.videoTitleWhether = videoTitleWhether;
    }

    /**
     * @return the createdOnFrom
     */
    public String getCreatedOnFrom() {
        return createdOnFrom;
    }

    /**
     * @param createdOnFrom the createdOnFrom to set
     */
    public void setCreatedOnFrom(String createdOnFrom) {
        this.createdOnFrom = createdOnFrom;
    }

    /**
     * @return the createdOnTo
     */
    public String getCreatedOnTo() {
        return createdOnTo;
    }

    /**
     * @param createdOnTo the createdOnTo to set
     */
    public void setCreatedOnTo(String createdOnTo) {
        this.createdOnTo = createdOnTo;
    }

    /**
     * @return the updatedOnFrom
     */
    public String getUpdatedOnFrom() {
        return updatedOnFrom;
    }

    /**
     * @param updatedOnFrom the updatedOnFrom to set
     */
    public void setUpdatedOnFrom(String updatedOnFrom) {
        this.updatedOnFrom = updatedOnFrom;
    }

    /**
     * @return the updatedOnTo
     */
    public String getUpdatedOnTo() {
        return updatedOnTo;
    }

    /**
     * @param updatedOnTo the updatedOnTo to set
     */
    public void setUpdatedOnTo(String updatedOnTo) {
        this.updatedOnTo = updatedOnTo;
    }

	public void setDetailSearchFlag(int detailSearchFlag) {
		this.detailSearchFlag = detailSearchFlag;
	}

	public int getDetailSearchFlag() {
		return detailSearchFlag;
	}
    /**
     * @return the productProperPriceFrom
     */
    public Integer getProductProperPriceFrom() {
        return productProperPriceFrom;
    }
    /**
     * @param productProperPriceFrom the productProperPriceFrom to set
     */
    public void setProductProperPriceFrom(Integer productProperPriceFrom) {
        this.productProperPriceFrom = productProperPriceFrom;
    }
    /**
     * @return the productProperPriceTo
     */
    public Integer getProductProperPriceTo() {
        return productProperPriceTo;
    }
    /**
     * @param productProperPriceTo the productProperPriceTo to set
     */
    public void setProductProperPriceTo(Integer productProperPriceTo) {
        this.productProperPriceTo = productProperPriceTo;
    }
    /**
     * @return the rateFrom
     */
    public Double getRateFrom() {
        return rateFrom;
    }
    /**
     * @param rateFrom the rateFrom to set
     */
    public void setRateFrom(Double rateFrom) {
        this.rateFrom = rateFrom;
    }
    /**
     * @return the rateTo
     */
    public Double getRateTo() {
        return rateTo;
    }
    /**
     * @param rateTo the rateTo to set
     */
    public void setRateTo(Double rateTo) {
        this.rateTo = rateTo;
    }

    public void setLastUpdatedPersonIncluded(boolean lastUpdatedPersonIncluded) {
		this.lastUpdatedPersonIncluded = lastUpdatedPersonIncluded;
	}

    public boolean isLastUpdatedPersonIncluded() {
		return lastUpdatedPersonIncluded;
	}

    public void setProductRegistrationDateIncluded(
			boolean productRegistrationDateIncluded) {
		this.productRegistrationDateIncluded = productRegistrationDateIncluded;
	}

    public boolean isProductRegistrationDateIncluded() {
		return productRegistrationDateIncluded;
	}

    public void setProductRenovationDateIncluded(
			boolean productRenovationDateIncluded) {
		this.productRenovationDateIncluded = productRenovationDateIncluded;
	}

    public boolean isProductRenovationDateIncluded() {
		return productRenovationDateIncluded;
	}
    /**
     * @return the updateMode
     */
    public String getUpdateMode() {
        return updateMode;
    }
    /**
     * @param updateMode the updateMode to set
     */
    public void setUpdateMode(String updateMode) {
        this.updateMode = updateMode;
    }
    /**
     * @return the fitModelMaker
     */
    public String getFitModelMaker() {
        return fitModelMaker;
    }
    /**
     * @param fitModelMaker the fitModelMaker to set
     */
    public void setFitModelMaker(String fitModelMaker) {
        this.fitModelMaker = fitModelMaker;
    }
    /**
     * @return the fitModelModel
     */
    public String getFitModelModel() {
        return fitModelModel;
    }
    /**
     * @param fitModelModel the fitModelModel to set
     */
    public void setFitModelModel(String fitModelModel) {
        this.fitModelModel = fitModelModel;
    }
    /**
     * @return the productIdList
     */
    public String getProductIdList() {
        return productIdList;
    }
    /**
     * @param productIdList the productIdList to set
     */
    public void setProductIdList(String productIdList) {
        this.productIdList = productIdList;
    }
    /**
     * @return the fitModelStyle
     */
    public String getFitModelStyle() {
        return fitModelStyle;
    }
    /**
     * @param fitModelStyle the fitModelStyle to set
     */
    public void setFitModelStyle(String fitModelStyle) {
        this.fitModelStyle = fitModelStyle;
    }

    /**
     * @return the fitModelSort
     */
    public Integer getFitModelSort() {
        return fitModelSort;
    }
    /**
     * @param fitModelSort the fitModelSort to set
     */
    public void setFitModelSort(Integer fitModelSort) {
        this.fitModelSort = fitModelSort;
    }
    public void setFromPartition(Integer fromPartition) {
		this.fromPartition = fromPartition;
	}

    public Integer getFromPartition() {
		return fromPartition;
	}

    public void setToPartition(Integer toPartition) {
		this.toPartition = toPartition;
	}

    public Integer getToPartition() {
		return toPartition;
	}

    public void setAttributeManagementBooleanValue(
			boolean attributeManagementBooleanValue) {
		this.attributeManagementBooleanValue = attributeManagementBooleanValue;
	}

    public boolean isAttributeManagementBooleanValue() {
		return attributeManagementBooleanValue;
	}
    /**
     * @return the productDelFlgStatus
     */
    public int getProductDelFlgStatus() {
        return productDelFlgStatus;
    }
    /**
     * @param productDelFlgStatus the productDelFlgStatus to set
     */
    public void setProductDelFlgStatus(int productDelFlgStatus) {
        this.productDelFlgStatus = productDelFlgStatus;
    }
	public EntTblFactoryProductNew getMatterInfo() {
		return matterInfo;
	}
	public void setMatterInfo(EntTblFactoryProductNew matterInfo) {
		this.matterInfo = matterInfo;
	}
	public boolean isHasMaintenanceProduct() {
		return hasMaintenanceProduct;
	}
	public void setHasMaintenanceProduct(boolean hasMaintenanceProduct) {
		this.hasMaintenanceProduct = hasMaintenanceProduct;
	}
	public String getMaker() {
		return maker;
	}
	public void setMaker(String maker) {
		this.maker = maker;
	}
	public boolean isMakerWhether() {
		return makerWhether;
	}
	public void setMakerWhether(boolean makerWhether) {
		this.makerWhether = makerWhether;
	}
	public int getMakerSearchOption() {
		return makerSearchOption;
	}
	public void setMakerSearchOption(int makerSearchOption) {
		this.makerSearchOption = makerSearchOption;
	}
}
