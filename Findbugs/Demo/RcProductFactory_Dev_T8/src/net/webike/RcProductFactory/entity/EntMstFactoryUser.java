///**
// * EntMstFactoryUser.
// */
//package net.webike.RcProductFactory.entity;
//
//import java.util.Date;
//
//import org.apache.commons.lang.StringUtils;
//
///**
// * @author Nguyen.Chuong
// *
// */
//public class EntMstFactoryUser {
//
//	private String userId = "";
//
//	private String userPasswd = "";
//
//	private String userPasswdConfirm = "";
//
//	private String userLastName = "";
//
//	private String userFirstName = "";
//
//	private String userEmail1 = "";
//
//	private String userEmail2 = "";
//
//	private String userAuthority = "";
//
//	private String delFlg = "";
//
//	private Date tourokuDate;
//
//	private String tourokuUserId = "";
//
//	private Date kousinDate;
//
//	private String kousinUserId = "";
//
//	// User last name and user first name
//	private String userName = "";
//
//    /**
//	 * @return the userName
//	 */
//	public String getUserName() {
//		return userName;
//	}
//
//	/**
//	 * @param userName the userName to set
//	 */
//	public void setUserName(String userName) {
//	    if (StringUtils.isNotEmpty(userName)) {
//	        userName = userName.trim();
//	    }
//		this.userName = userName;
//	}
//
//	/**
//     * @return the userId
//     */
//    public String getUserId() {
//        return userId;
//    }
//
//    /**
//     * @param userId the userId to set
//     */
//    public void setUserId(String userId) {
//        if (StringUtils.isNotEmpty(userId)) {
//            userId = userId.trim();
//        }
//        this.userId = userId;
//    }
//
//    /**
//     * @return the userPasswd
//     */
//    public String getUserPasswd() {
//        return userPasswd;
//    }
//
//    /**
//     * @param userPasswd the userPasswd to set
//     */
//    public void setUserPasswd(String userPasswd) {
//        if (StringUtils.isNotEmpty(userPasswd)) {
//            userPasswd = userPasswd.trim();
//        }
//        this.userPasswd = userPasswd;
//    }
//
//    /**
//     * @return the userLastName
//     */
//    public String getUserLastName() {
//        return userLastName;
//    }
//
//    /**
//     * @param userLastName the userLastName to set
//     */
//    public void setUserLastName(String userLastName) {
//        if (StringUtils.isNotEmpty(userLastName)) {
//            userLastName = userLastName.trim();
//        }
//        this.userLastName = userLastName;
//    }
//
//    /**
//     * @return the userFirstName
//     */
//    public String getUserFirstName() {
//        return userFirstName;
//    }
//
//    /**
//     * @param userFirstName the userFirstName to set
//     */
//    public void setUserFirstName(String userFirstName) {
//        if (StringUtils.isNotEmpty(userFirstName)) {
//            userFirstName = userFirstName.trim();
//        }
//        this.userFirstName = userFirstName;
//    }
//
//    /**
//     * @return the userEmail1
//     */
//    public String getUserEmail1() {
//        return userEmail1;
//    }
//
//    /**
//     * @param userEmail1 the userEmail1 to set
//     */
//    public void setUserEmail1(String userEmail1) {
//        if (StringUtils.isNotEmpty(userEmail1)) {
//            userEmail1 = userEmail1.trim();
//        }
//        this.userEmail1 = userEmail1;
//    }
//
//    /**
//     * @return the userEmail2
//     */
//    public String getUserEmail2() {
//        return userEmail2;
//    }
//
//    /**
//     * @param userEmail2 the userEmail2 to set
//     */
//    public void setUserEmail2(String userEmail2) {
//        if (StringUtils.isNotEmpty(userEmail2)) {
//            userEmail2 = userEmail2.trim();
//        }
//        this.userEmail2 = userEmail2;
//    }
//
//    /**
//     * @return the userAuthority
//     */
//    public String getUserAuthority() {
//        return userAuthority;
//    }
//
//    /**
//     * @param userAuthority the userAuthority to set
//     */
//    public void setUserAuthority(String userAuthority) {
//        if (StringUtils.isNotEmpty(userAuthority)) {
//            userAuthority = userAuthority.trim();
//        }
//        this.userAuthority = userAuthority;
//    }
//
//    /**
//	 * @return the delFlg
//	 */
//	public String getDelFlg() {
//		return delFlg;
//	}
//
//	/**
//	 * @param delFlg the delFlg to set
//	 */
//	public void setDelFlg(String delFlg) {
//        if (StringUtils.isNotEmpty(delFlg)) {
//            delFlg = delFlg.trim();
//        }
//		this.delFlg = delFlg;
//	}
//
//	/**
//     * @return the tourokuUserId
//     */
//    public String getTourokuUserId() {
//        return tourokuUserId;
//    }
//
//    /**
//     * @param tourokuUserId the tourokuUserId to set
//     */
//    public void setTourokuUserId(String tourokuUserId) {
//        if (StringUtils.isNotEmpty(tourokuUserId)) {
//            tourokuUserId = tourokuUserId.trim();
//        }
//        this.tourokuUserId = tourokuUserId;
//    }
//
//    /**
//     * @return the kousinUserId
//     */
//    public String getKousinUserId() {
//        return kousinUserId;
//    }
//
//    /**
//     * @param kousinUserId the kousinUserId to set
//     */
//    public void setKousinUserId(String kousinUserId) {
//        if (StringUtils.isNotEmpty(kousinUserId)) {
//            kousinUserId = kousinUserId.trim();
//        }
//        this.kousinUserId = kousinUserId;
//    }
//
//    /**
//     * @return the tourokuDate
//     */
//    public Date getTourokuDate() {
//        if (tourokuDate == null) {
//            return null;
//        }
//        return new Date(tourokuDate.getTime());
//    }
//
//    /**
//     * @param tourokuDate the tourokuDate to set
//     */
//    public void setTourokuDate(Date tourokuDate) {
//        if (tourokuDate == null) {
//            this.tourokuDate = null;
//            return;
//          }
//        this.tourokuDate = new Date(tourokuDate.getTime());
//    }
//
//    /**
//     * @return the kousinDate
//     */
//    public Date getKousinDate() {
//        if (kousinDate == null) {
//            return null;
//        }
//        return new Date(kousinDate.getTime());
//    }
//
//    /**
//     * @param kousinDate the kousinDate to set
//     */
//    public void setKousinDate(Date kousinDate) {
//        if (kousinDate == null) {
//            this.kousinDate = null;
//            return;
//          }
//        this.kousinDate = new Date(kousinDate.getTime());
//    }
//
//    /**
//     * @return the userPasswdConfirm
//     */
//    public String getUserPasswdConfirm() {
//        return userPasswdConfirm;
//    }
//
//    /**
//     * @param userPasswdConfirm the userPasswdConfirm to set
//     */
//    public void setUserPasswdConfirm(String userPasswdConfirm) {
//        this.userPasswdConfirm = userPasswdConfirm;
//    }
//
//}
