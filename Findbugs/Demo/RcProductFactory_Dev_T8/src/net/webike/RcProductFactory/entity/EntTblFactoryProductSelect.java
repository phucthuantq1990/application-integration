/************************************************************************
 * file name	： EntTblFactoryProductSelect.java
 * author		： Luong.Dai
 * version		： 1.0.0
  date created	： 2014/02/20
 * date updated	： 2014/02/20
 * description	： Entity EntTblFactoryProductSelect.
 *
 ***********************************************************************/
package net.webike.RcProductFactory.entity;

import java.io.Serializable;

/**
 * EntMstProductSupplierStatus.
 */
public class EntTblFactoryProductSelect implements Serializable {
	private static final long serialVersionUID = 7005304415581968835L;
	private Long productId;
	private Integer selectCode;
	private Integer productSelectSort;
	private String productSelectDisplay = "";
	private String productSelectValue;
	private Integer productSelectSoldoutFlg;
	private Integer limitListSelect;
	private String name;
	private String crudFlg = "";
	private String updatedUserId;
	private Long syouhinSysCode;
    /**
     * @return the productId
     */
    public Long getProductId() {
        return productId;
    }
    /**
     * @param productId the productId to set
     */
    public void setProductId(Long productId) {
        this.productId = productId;
    }
    /**
     * @return the selectCode
     */
    public Integer getSelectCode() {
        return selectCode;
    }
    /**
     * @param selectCode the selectCode to set
     */
    public void setSelectCode(Integer selectCode) {
        this.selectCode = selectCode;
    }
    /**
     * @return the productSelectSort
     */
    public Integer getProductSelectSort() {
        return productSelectSort;
    }
    /**
     * @param productSelectSort the productSelectSort to set
     */
    public void setProductSelectSort(Integer productSelectSort) {
        this.productSelectSort = productSelectSort;
    }
    /**
     * @return the productSelectDisplay
     */
    public String getProductSelectDisplay() {
        return productSelectDisplay;
    }
    /**
     * @param productSelectDisplay the productSelectDisplay to set
     */
    public void setProductSelectDisplay(String productSelectDisplay) {
        this.productSelectDisplay = productSelectDisplay;
    }
    /**
     * @return the productSelectValue
     */
    public String getProductSelectValue() {
        return productSelectValue;
    }
    /**
     * @param productSelectValue the productSelectValue to set
     */
    public void setProductSelectValue(String productSelectValue) {
        this.productSelectValue = productSelectValue;
    }
    /**
     * @return the limitListSelect
     */
    public Integer getLimitListSelect() {
        return limitListSelect;
    }
    /**
     * @param limitListSelect the limitListSelect to set
     */
    public void setLimitListSelect(Integer limitListSelect) {
        this.limitListSelect = limitListSelect;
    }
    /**
     * @return the name
     */
    public String getName() {
        return name;
    }
    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }
    /**
     * @return the crudFlg
     */
    public String getCrudFlg() {
        return crudFlg;
    }
    /**
     * @param crudFlg the crudFlg to set
     */
    public void setCrudFlg(String crudFlg) {
        this.crudFlg = crudFlg;
    }
    /**
     * @return the updatedUserId
     */
    public String getUpdatedUserId() {
        return updatedUserId;
    }
    /**
     * @param updatedUserId the updatedUserId to set
     */
    public void setUpdatedUserId(String updatedUserId) {
        this.updatedUserId = updatedUserId;
    }
    /**
     * @return the productSelectSoldoutFlg
     */
    public Integer getProductSelectSoldoutFlg() {
        return productSelectSoldoutFlg;
    }
    /**
     * @param productSelectSoldoutFlg the productSelectSoldoutFlg to set
     */
    public void setProductSelectSoldoutFlg(Integer productSelectSoldoutFlg) {
        this.productSelectSoldoutFlg = productSelectSoldoutFlg;
    }
    /**
     * @return the syouhinSysCode
     */
    public Long getSyouhinSysCode() {
        return syouhinSysCode;
    }
    /**
     * @param syouhinSysCode the syouhinSysCode to set
     */
    public void setSyouhinSysCode(Long syouhinSysCode) {
        this.syouhinSysCode = syouhinSysCode;
    }
}
