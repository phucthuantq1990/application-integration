/************************************************************************
 * file name	： EntCategory.java
 * author		： Tran.Thanh
 * version		： 1.0.0
 * date created	： 30 Dec 2013
 * date updated	： 30 Dec 2013
 * description	： Entity marume.
 *
 ***********************************************************************/
package net.webike.RcProductFactory.entity;

/**
 * EntCategory.
 */
public class EntMstMarume {

	private int marumeCode;

	private String marumeName = "";

	/**
	 * @return the marumeCode
	 */
	public int getMarumeCode() {
		return marumeCode;
	}

	/**
	 * @param marumeCode the marumeCode to set
	 */
	public void setMarumeCode(int marumeCode) {
		this.marumeCode = marumeCode;
	}

	/**
	 * @return the marumeName
	 */
	public String getMarumeName() {
		return marumeName;
	}

	/**
	 * @param marumeName the marumeName to set
	 */
	public void setMarumeName(String marumeName) {
		this.marumeName = marumeName;
	}
}
