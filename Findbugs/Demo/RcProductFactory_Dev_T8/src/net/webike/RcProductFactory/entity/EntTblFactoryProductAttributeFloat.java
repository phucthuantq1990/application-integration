
package net.webike.RcProductFactory.entity;


/**
 * Entity of EntTblFactoryProductAttributeFloat.
 * @author Doan Chuong
 * Date Created �F 2014/03/05
 */
public class EntTblFactoryProductAttributeFloat {
    private Long productId;
    private Integer attributeCode;
    private String attributeFloatDisplay;
    private Integer attributeFloatValues;
    private Integer attributeFloatSort;
    private String updatedOn;
    private String updatedUserId;
    /**
     * @return the productId
     */
    public Long getProductId() {
        return productId;
    }
    /**
     * @param productId the productId to set
     */
    public void setProductId(Long productId) {
        this.productId = productId;
    }
    /**
     * @return the attributeCode
     */
    public Integer getAttributeCode() {
        return attributeCode;
    }
    /**
     * @param attributeCode the attributeCode to set
     */
    public void setAttributeCode(Integer attributeCode) {
        this.attributeCode = attributeCode;
    }
    /**
     * @return the attributeFloatDisplay
     */
    public String getAttributeFloatDisplay() {
        return attributeFloatDisplay;
    }
    /**
     * @param attributeFloatDisplay the attributeFloatDisplay to set
     */
    public void setAttributeFloatDisplay(String attributeFloatDisplay) {
        this.attributeFloatDisplay = attributeFloatDisplay;
    }
    /**
     * @return the attributeFloatValues
     */
    public Integer getAttributeFloatValues() {
        return attributeFloatValues;
    }
    /**
     * @param attributeFloatValues the attributeFloatValues to set
     */
    public void setAttributeFloatValues(Integer attributeFloatValues) {
        this.attributeFloatValues = attributeFloatValues;
    }
    /**
     * @return the attributeFloatSort
     */
    public Integer getAttributeFloatSort() {
        return attributeFloatSort;
    }
    /**
     * @param attributeFloatSort the attributeFloatSort to set
     */
    public void setAttributeFloatSort(Integer attributeFloatSort) {
        this.attributeFloatSort = attributeFloatSort;
    }
    /**
     * @return the updatedOn
     */
    public String getUpdatedOn() {
        return updatedOn;
    }
    /**
     * @param updatedOn the updatedOn to set
     */
    public void setUpdatedOn(String updatedOn) {
        this.updatedOn = updatedOn;
    }
    /**
     * @return the updatedUserId
     */
    public String getUpdatedUserId() {
        return updatedUserId;
    }
    /**
     * @param updatedUserId the updatedUserId to set
     */
    public void setUpdatedUserId(String updatedUserId) {
        this.updatedUserId = updatedUserId;
    }
}
