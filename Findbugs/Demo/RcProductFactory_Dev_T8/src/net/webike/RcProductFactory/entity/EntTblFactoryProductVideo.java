/************************************************************************
 * file name	： EntTblFactoryProductDescription.java.
 * author		： Tran.Thanh
 * version		： 1.0.0
 * date created	： 12 Feb 2014
 * date updated	： 12 Feb 2014
 * description	： Table Tbl_Factory_Product_Description
 *
 ***********************************************************************/
package net.webike.RcProductFactory.entity;

import java.util.Date;

/**
 * Table Tbl_Factory_Product_Description in Database.
 */
public class EntTblFactoryProductVideo {

	private Long productId;

	private Integer productVideoSort;

	private String productVideoTitle = "";

	private String productVideoUrl = "";

	private String productVideoDescription = "";

	private Date updatedOn;

	private String updatedUserId = "";

	private boolean checked = false;

	private String idRow = "";

	private String crudFlg = "";

	private Long syouhinSysCode;

	/**
	 * @return the productId
	 */
	public Long getProductId() {
		return productId;
	}

	/**
	 * @param productId the productId to set
	 */
	public void setProductId(Long productId) {
		this.productId = productId;
	}

	/**
	 * @return the productVideoSort
	 */
	public Integer getProductVideoSort() {
		return productVideoSort;
	}

	/**
	 * @param productVideoSort the productVideoSort to set
	 */
	public void setProductVideoSort(Integer productVideoSort) {
		this.productVideoSort = productVideoSort;
	}

	/**
	 * @return the productVideoTitle
	 */
	public String getProductVideoTitle() {
		return productVideoTitle;
	}

	/**
	 * @param productVideoTitle the productVideoTitle to set
	 */
	public void setProductVideoTitle(String productVideoTitle) {
		this.productVideoTitle = productVideoTitle;
	}

	/**
	 * @return the productVideoUrl
	 */
	public String getProductVideoUrl() {
		return productVideoUrl;
	}

	/**
	 * @param productVideoUrl the productVideoUrl to set
	 */
	public void setProductVideoUrl(String productVideoUrl) {
		this.productVideoUrl = productVideoUrl;
	}

	/**
	 * @return the productVideoDescription
	 */
	public String getProductVideoDescription() {
		return productVideoDescription;
	}

	/**
	 * @param productVideoDescription the productVideoDescription to set
	 */
	public void setProductVideoDescription(String productVideoDescription) {
		this.productVideoDescription = productVideoDescription;
	}

	/**
	 * @return the updatedOn
	 */
	public Date getUpdatedOn() {
		if (updatedOn != null) {
            return (Date) updatedOn.clone();
        }
        return null;
	}

	/**
	 * @param updatedOn the updatedOn to set
	 */
	public void setUpdatedOn(Date updatedOn) {
		if (updatedOn != null) {
			this.updatedOn = (Date) updatedOn.clone();
        } else {
        	this.updatedOn = null;
        }
	}

	/**
	 * @return the updatedUserId
	 */
	public String getUpdatedUserId() {
		return updatedUserId;
	}

	/**
	 * @param updatedUserId the updatedUserId to set
	 */
	public void setUpdatedUserId(String updatedUserId) {
		this.updatedUserId = updatedUserId;
	}

	/**
	 * @return the checked
	 */
	public boolean isChecked() {
		return checked;
	}

	/**
	 * @param checked the checked to set
	 */
	public void setChecked(boolean checked) {
		this.checked = checked;
	}

	/**
	 * @return the idRow
	 */
	public String getIdRow() {
		return idRow;
	}

	/**
	 * @param idRow the idRow to set
	 */
	public void setIdRow(String idRow) {
		this.idRow = idRow;
	}

	/**
	 * @return the crudFlg
	 */
	public String getCrudFlg() {
		return crudFlg;
	}

	/**
	 * @param crudFlg the crudFlg to set
	 */
	public void setCrudFlg(String crudFlg) {
		this.crudFlg = crudFlg;
	}

    /**
     * @return the syouhinSysCode
     */
    public Long getSyouhinSysCode() {
        return syouhinSysCode;
    }

    /**
     * @param syouhinSysCode the syouhinSysCode to set
     */
    public void setSyouhinSysCode(Long syouhinSysCode) {
        this.syouhinSysCode = syouhinSysCode;
    }

}
