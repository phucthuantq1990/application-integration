/**
 * EntMstFactoryUser.
 */
package net.webike.RcProductFactory.entity;

import java.util.Date;

import org.apache.commons.lang.StringUtils;

/**
 * @author Tran.Thanh
 *
 */
public class EntMstFactoryUserTmp {

	private String userId = "";

	private String userPasswdConfirm = "";

	private String userPasswd = "";

	private String userLastName = "";

	private String userFirstName = "";

	private String userEmail = "";

	private String userDelFlg = "";

	private String userRedmineApiKey = "";

	private Date createdOn;

	private String createdUserId = "";

	private Date updatedOn;

	private String updatedUserId = "";

	private String fullName;

	// User last name and user first name
	private String userName = "";

    /**
	 * @return the userId
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * @return the userPasswdConfirm
	 */
	public String getUserPasswdConfirm() {
		return userPasswdConfirm;
	}

	/**
	 * @param userPasswdConfirm the userPasswdConfirm to set
	 */
	public void setUserPasswdConfirm(String userPasswdConfirm) {
		this.userPasswdConfirm = userPasswdConfirm;
	}

	/**
	 * @return the userPasswd
	 */
	public String getUserPasswd() {
		return userPasswd;
	}

	/**
	 * @param userPasswd the userPasswd to set
	 */
	public void setUserPasswd(String userPasswd) {
		this.userPasswd = userPasswd;
	}

	/**
	 * @return the userLastName
	 */
	public String getUserLastName() {
		return userLastName;
	}

	/**
	 * @param userLastName the userLastName to set
	 */
	public void setUserLastName(String userLastName) {
		this.userLastName = userLastName;
	}

	/**
	 * @return the userFirstName
	 */
	public String getUserFirstName() {
		return userFirstName;
	}

	/**
	 * @param userFirstName the userFirstName to set
	 */
	public void setUserFirstName(String userFirstName) {
		this.userFirstName = userFirstName;
	}

	/**
	 * @return the userEmail
	 */
	public String getUserEmail() {
		return userEmail;
	}

	/**
	 * @param userEmail the userEmail to set
	 */
	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	/**
	 * @return the userDelFlg
	 */
	public String getUserDelFlg() {
		return userDelFlg;
	}

	/**
	 * @param userDelFlg the userDelFlg to set
	 */
	public void setUserDelFlg(String userDelFlg) {
		this.userDelFlg = userDelFlg;
	}

	/**
	 * @return the userRedmineApiKey
	 */
	public String getUserRedmineApiKey() {
		return userRedmineApiKey;
	}

	/**
	 * @param userRedmineApiKey the userRedmineApiKey to set
	 */
	public void setUserRedmineApiKey(String userRedmineApiKey) {
		this.userRedmineApiKey = userRedmineApiKey;
	}

	/**
	 * @return the createdOn
	 */
	public Date getCreatedOn() {
		if (createdOn == null) {
            return null;
        }
        return new Date(createdOn.getTime());
	}

	/**
	 * @param createdOn the createdOn to set
	 */
	public void setCreatedOn(Date createdOn) {
		if (createdOn == null) {
            this.createdOn = null;
            return;
          }
        this.createdOn = new Date(createdOn.getTime());
	}

	/**
	 * @return the createdUserId
	 */
	public String getCreatedUserId() {
		return createdUserId;
	}

	/**
	 * @param createdUserId the createdUserId to set
	 */
	public void setCreatedUserId(String createdUserId) {
		this.createdUserId = createdUserId;
	}

	/**
	 * @return the updatedOn
	 */
	public Date getUpdatedOn() {
		if (updatedOn == null) {
            return null;
        }
        return new Date(updatedOn.getTime());
	}

	/**
	 * @param updatedOn the updatedOn to set
	 */
	public void setUpdatedOn(Date updatedOn) {
		if (updatedOn == null) {
            this.updatedOn = null;
            return;
        }
		this.updatedOn = new Date(updatedOn.getTime());
	}

	/**
	 * @return the updateUserId
	 */
	public String getUpdateUserId() {
		return updatedUserId;
	}

	/**
	 * @param updatedUserId the updatedUserId to set
	 */
	public void setUpdateUserId(String updatedUserId) {
		this.updatedUserId = updatedUserId;
	}

	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		if (StringUtils.isNotEmpty(userName)) {
	        userName = userName.trim();
	    }
		this.userName = userName;
	}

	/**
	 * @return the updatedUserId
	 */
	public String getUpdatedUserId() {
		return updatedUserId;
	}

	/**
	 * @param updatedUserId the updatedUserId to set
	 */
	public void setUpdatedUserId(String updatedUserId) {
		this.updatedUserId = updatedUserId;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getFullName() {
		return fullName;
	}
}
