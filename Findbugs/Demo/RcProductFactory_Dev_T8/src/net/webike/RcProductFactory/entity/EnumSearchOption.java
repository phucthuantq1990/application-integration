package net.webike.RcProductFactory.entity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Enum of whetherSearchCondition.
 */
public enum EnumSearchOption {
    EQUAL(1, "EQUAL"),
    LIKE(2, "LIKE"),
    NOT_LIKE(3, "NOT LIKE");

    private final int key;
    private final String displayName;

    /**
     * Enum of whether search condition.
     * @param key Key
     * @param displayName Display name
     */
    private EnumSearchOption(final int key, final String displayName) {
    	this.key = key;
    	this.displayName = displayName;
	}


    /**
     * Get enum list.
     * @author		nguyen.hieu
     * @date		2014-02-20
     * @return		Enum list.
     */
    public static List<Map<String, String>> getEnumList() {
        List<Map<String, String>> enumList = new ArrayList<Map<String, String>>();
        for (EnumSearchOption value : values()) {
        	Map<String, String> map = new HashMap<String, String>(2);
        	map.put("key", String.valueOf(value.key));
        	map.put("displayName", value.displayName);
        	enumList.add(map);
        }

        return enumList;
    }

    public int getKey() {
		return key;
	}

    public String getDisplayName() {
		return displayName;
	}
}
