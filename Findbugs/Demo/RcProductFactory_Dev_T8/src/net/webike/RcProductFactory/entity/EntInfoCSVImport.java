/************************************************************************
 * File Name    ： EntInfoCSVImport.java
 * Author       ： hoang.ho
 * Version      ： 1.0.0
 * Date Created ： 2014/03/06
 * Date Updated ： 2014/03/06
 * Description  ： Entity of EntInfoCSVImport.
 ***********************************************************************/
package net.webike.RcProductFactory.entity;

/**
 * Store output information when import CSV.
 * @author HoangHo
 *
 */
public class EntInfoCSVImport {
    /* BOE [6716] hoang.ho 2014/03/04 */
    private String errorKnownRows;
    private Integer errorKnownRowId;
    private String errorUnknownRows;
    private Integer errorUnknownRowId;
    private Integer totalRecords;
    /* BOE [6716] hoang.ho 2014/03/04 */

    /**
     * @return the totalRecords
     */
    public Integer getTotalRecords() {
        return totalRecords;
    }
    /**
     * @return the errorKnownRowId
     */
    public Integer getErrorKnownRowId() {
        return errorKnownRowId;
    }
    /**
     * @param errorKnownRowId the errorKnownRowId to set
     */
    public void setErrorKnownRowId(Integer errorKnownRowId) {
        this.errorKnownRowId = errorKnownRowId;
    }
    /**
     * @return the errorUnknownRowId
     */
    public Integer getErrorUnknownRowId() {
        return errorUnknownRowId;
    }
    /**
     * @param errorUnknownRowId the errorUnknownRowId to set
     */
    public void setErrorUnknownRowId(Integer errorUnknownRowId) {
        this.errorUnknownRowId = errorUnknownRowId;
    }
    /**
     * @param totalRecords the totalRecords to set
     */
    public void setTotalRecords(Integer totalRecords) {
        this.totalRecords = totalRecords;
    }
    /**
     * @return the errorKnownRows
     */
    public String getErrorKnownRows() {
        return errorKnownRows;
    }
    /**
     * @param errorKnownRows the errorKnownRows to set
     */
    public void setErrorKnownRows(String errorKnownRows) {
        this.errorKnownRows = errorKnownRows;
    }
    /**
     * @return the errorUnknownRows
     */
    public String getErrorUnknownRows() {
        return errorUnknownRows;
    }
    /**
     * @param errorUnknownRows the errorUnknownRows to set
     */
    public void setErrorUnknownRows(String errorUnknownRows) {
        this.errorUnknownRows = errorUnknownRows;
    }

}
