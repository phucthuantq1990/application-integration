/**
 * Add comment later.
 */
package net.webike.RcProductFactory.entity;

import net.webike.RcProductFactory.util.Utils;


/**
 * @author hoang.ho
 *
 */
public class EntMstAttribute {
    private String attributeCode;
    private String attributeName;
    private String attributeType;
    private String attributeLengthLimit;
    private String attributeRegexp;
    private String attributeSystemLockFlg;
    private String attributeShowFlg;
    private String attributeDelFlg;
    private String createOn;
    private String createUserId;
    private String updatedOn;
    private String updatedUserId;
    //[6305] Nguyen.Chuong: updateOn from and to for filter.
    private String updatedOnFrom;
    private String updatedOnTo;
    //Sort column name
    private String sortField;
    //Sort asc/ders
    private String sortDir;
    //add by Le.Dinh to process productEdit page
	private String idRow = "";
	private String crudFlg = "";
	private Long productId;

    private String loginUserId;
    private String attributeValues;
    private String attributeDisplay;
    private String categoryAttributeRequiredFlg;
    private Boolean attrOfCategory;
    private String attributeTypeJapan;
    private String attributeGroupCode;

    // add field for table tbl_factory_product_attribute_flag
    private Integer attributeFlagValues;
    private Integer attributeSort;
    // add field for table tbl_factory_product_attribute_float
    private String attributeFloatDisplay = "";
    private float attributeFloatValues;
    // add field for table tbl_factory_product_attribute_group
    private String attributeGroupDisplay = "";
    // add field for table tbl_factory_product_attribute_integer
    private String attributeIntegerDisplay = "";
    private Integer attributeIntegerValues;
    // add field for table tbl_factory_product_attribute_string
    private String attributeStringValues = "";
    private Long syouhinSysCode;

    /**
     * @return the attributeCode
     */
    public String getAttributeCode() {
        return attributeCode;
    }
    /**
     * @param attributeCode the attributeCode to set
     */
    public void setAttributeCode(String attributeCode) {
        this.attributeCode = attributeCode;
    }
    /**
     * @return the attributeName
     */
    public String getAttributeName() {
        return attributeName;
    }
    /**
     * @param attributeName the attributeName to set
     */
    public void setAttributeName(String attributeName) {
        this.attributeName = attributeName;
    }
    /**
     * @return the attributeType as English
     */
    public String getAttributeType() {
        return attributeType;
    }
    /**
     * @return the attributeType as Japanese
     */
    public String getAttributeTypeJapan() {
//        String typeJapan = EnumAttributeType.getTypeValueByKey(attributeType);
//        return typeJapan;
        // Luong.Dai add for productEdit 02/14/2014
        return this.attributeTypeJapan;
    }
    /**
     * @param attributeTypeJapan the attributeTypeJapan to set
     */
    public void setAttributeTypeJapan(String attributeTypeJapan) {
        this.attributeTypeJapan = attributeTypeJapan;
    }
    /**
     * @param attributeType the attributeType to set
     */
    public void setAttributeType(String attributeType) {
        this.attributeType = attributeType;
        //Create attribute in Japan
        setAttributeTypeJapan(EnumAttributeType.getTypeValueByKey(this.attributeType));
    }
    /**
     * @return the attributeLengthLimit
     */
    public String getAttributeLengthLimit() {
        return attributeLengthLimit;
    }
    /**
     * @param attributeLengthLimit the attributeLengthLimit to set
     */
    public void setAttributeLengthLimit(String attributeLengthLimit) {
        this.attributeLengthLimit = attributeLengthLimit;
    }
    /**
     * @return the attributeRegexp
     */
    public String getAttributeRegexp() {
        return attributeRegexp;
    }
    /**
     * @param attributeRegexp the attributeRegexp to set
     */
    public void setAttributeRegexp(String attributeRegexp) {
        this.attributeRegexp = attributeRegexp;
    }
    /**
     * @return the attributeSystemLockFlg
     */
    public String getAttributeSystemLockFlg() {
        return attributeSystemLockFlg;
    }
    /**
     * @param attributeSystemLockFlg the attributeSystemLockFlg to set
     */
    public void setAttributeSystemLockFlg(String attributeSystemLockFlg) {
        this.attributeSystemLockFlg = attributeSystemLockFlg;
    }
    /**
     * @return the attributeShowFlg
     */
    public String getAttributeShowFlg() {
        return attributeShowFlg;
    }
    /**
     * @param attributeShowFlg the attributeShowFlg to set
     */
    public void setAttributeShowFlg(String attributeShowFlg) {
        this.attributeShowFlg = attributeShowFlg;
    }
    /**
     * @return the attributeDelFlg
     */
    public String getAttributeDelFlg() {
        return attributeDelFlg;
    }
    /**
     * @param attributeDelFlg the attributeDelFlg to set
     */
    public void setAttributeDelFlg(String attributeDelFlg) {
        this.attributeDelFlg = attributeDelFlg;
    }
    /**
     * @return the createOn
     */
    public String getCreateOn() {
        return createOn;
    }
    /**
     * @param createOn the createOn to set
     */
    public void setCreateOn(String createOn) {
        this.createOn = createOn;
    }
    /**
     * @return the createUserId
     */
    public String getCreateUserId() {
        return createUserId;
    }
    /**
     * @param createUserId the createUserId to set
     */
    public void setCreateUserId(String createUserId) {
        this.createUserId = createUserId;
    }
    /**
     * @return the updatedOn
     */
    public String getUpdatedOn() {
        return updatedOn;
    }
    /**
     * @param updatedOn the updatedOn to set
     */
    public void setUpdatedOn(String updatedOn) {
        this.updatedOn = updatedOn;
    }
    /**
     * @return the updatedUserId
     */
    public String getUpdatedUserId() {
        return updatedUserId;
    }
    /**
     * @param updatedUserId the updatedUserId to set
     */
    public void setUpdatedUserId(String updatedUserId) {
        this.updatedUserId = updatedUserId;
    }
    /**
     * @return the updatedOnFrom
     */
    public String getUpdatedOnFrom() {
        return updatedOnFrom;
    }
    /**
     * @param updatedOnFrom the updatedOnFrom to set
     */
    public void setUpdatedOnFrom(String updatedOnFrom) {
        this.updatedOnFrom = updatedOnFrom;
    }
    /**
     * @return the updatedOnTo
     */
    public String getUpdatedOnTo() {
        return updatedOnTo;
    }
    /**
     * @param updatedOnTo the updatedOnTo to set
     */
    public void setUpdatedOnTo(String updatedOnTo) {
        this.updatedOnTo = updatedOnTo;
    }
    /**
     * @return the loginUserId
     */
    public String getLoginUserId() {
        return loginUserId;
    }
    /**
     * @param loginUserId the loginUserId to set
     */
    public void setLoginUserId(String loginUserId) {
        this.loginUserId = loginUserId;
    }
    /**
     * @return the sortField
     */
    public String getSortField() {
        return sortField;
    }
    /**
     * @param sortField the sortField to set
     */
    public void setSortField(String sortField) {
        if ("attributeTypeJapan".equals(sortField)) {
            this.sortField = "attributeType";
        } else {
            this.sortField = sortField;
        }
    }
    /**
     * @return the sortDir
     */
    public String getSortDir() {
        return sortDir;
    }
    /**
     * @param sortDir the sortDir to set
     */
    public void setSortDir(String sortDir) {
        this.sortDir = sortDir;
    }
    /**
     * @return the attributeValues
     */
    public String getAttributeValues() {
        return attributeValues;
    }
    /**
     * @param attributeValues the attributeValues to set
     */
    public void setAttributeValues(String attributeValues) {
        this.attributeValues = attributeValues;
    }
    /**
     * @return the attributeDisplay
     */
    public String getAttributeDisplay() {
        return attributeDisplay;
    }
    /**
     * @param attributeDisplay the attributeDisplay to set
     */
    public void setAttributeDisplay(String attributeDisplay) {
        this.attributeDisplay = attributeDisplay;
    }
    /**
     * @return the categoryAttributeRequiredFlg
     */
    public String getCategoryAttributeRequiredFlg() {
        return categoryAttributeRequiredFlg;
    }
    /**
     * @param categoryAttributeRequiredFlg the categoryAttributeRequiredFlg to set
     */
    public void setCategoryAttributeRequiredFlg(String categoryAttributeRequiredFlg) {
        this.categoryAttributeRequiredFlg = categoryAttributeRequiredFlg;
    }
    /**
     * @return the jSon of attribute
     */
    public String getAttributeJSon() {
        return Utils.parseObjectToJson(this);
    }
    /**
     * @return the attrOfCategory
     */
    public Boolean getAttrOfCategory() {
        return attrOfCategory;
    }
    /**
     * @param attrOfCategory the attrOfCategory to set
     */
    public void setAttrOfCategory(Boolean attrOfCategory) {
        this.attrOfCategory = attrOfCategory;
    }
    /**
     * @return the attributeGroupCode
     */
    public String getAttributeGroupCode() {
        return attributeGroupCode;
    }
    /**
     * @param attributeGroupCode the attributeGroupCode to set
     */
    public void setAttributeGroupCode(String attributeGroupCode) {
        this.attributeGroupCode = attributeGroupCode;
    }
	/**
	 * @return the idRow
	 */
	public String getIdRow() {
		return idRow;
	}
	/**
	 * @param idRow the idRow to set
	 */
	public void setIdRow(String idRow) {
		this.idRow = idRow;
	}
	/**
	 * @return the crudFlg
	 */
	public String getCrudFlg() {
		return crudFlg;
	}
	/**
	 * @param crudFlg the crudFlg to set
	 */
	public void setCrudFlg(String crudFlg) {
		this.crudFlg = crudFlg;
	}
	/**
	 * @return the productId
	 */
	public Long getProductId() {
		return productId;
	}
	/**
	 * @param productId the productId to set
	 */
	public void setProductId(Long productId) {
		this.productId = productId;
	}
	/**
	 * @return the attributeFlagValues
	 */
	public Integer getAttributeFlagValues() {
		return attributeFlagValues;
	}
	/**
	 * @param attributeFlagValues the attributeFlagValues to set
	 */
	public void setAttributeFlagValues(Integer attributeFlagValues) {
		this.attributeFlagValues = attributeFlagValues;
	}
	/**
	 * @return the attributeSort
	 */
	public Integer getAttributeSort() {
		return attributeSort;
	}
	/**
	 * @param attributeSort the attributeSort to set
	 */
	public void setAttributeSort(Integer attributeSort) {
		this.attributeSort = attributeSort;
	}
	/**
	 * @return the attributeFloatDisplay
	 */
	public String getAttributeFloatDisplay() {
		return attributeFloatDisplay;
	}
	/**
	 * @param attributeFloatDisplay the attributeFloatDisplay to set
	 */
	public void setAttributeFloatDisplay(String attributeFloatDisplay) {
		this.attributeFloatDisplay = attributeFloatDisplay;
	}
	/**
	 * @return the attributeFloatValues
	 */
	public float getAttributeFloatValues() {
		return attributeFloatValues;
	}
	/**
	 * @param attributeFloatValues the attributeFloatValues to set
	 */
	public void setAttributeFloatValues(float attributeFloatValues) {
		this.attributeFloatValues = attributeFloatValues;
	}
	/**
	 * @return the attributeGroupDisplay
	 */
	public String getAttributeGroupDisplay() {
		return attributeGroupDisplay;
	}
	/**
	 * @param attributeGroupDisplay the attributeGroupDisplay to set
	 */
	public void setAttributeGroupDisplay(String attributeGroupDisplay) {
		this.attributeGroupDisplay = attributeGroupDisplay;
	}
	/**
	 * @return the attributeIntegerDisplay
	 */
	public String getAttributeIntegerDisplay() {
		return attributeIntegerDisplay;
	}
	/**
	 * @param attributeIntegerDisplay the attributeIntegerDisplay to set
	 */
	public void setAttributeIntegerDisplay(String attributeIntegerDisplay) {
		this.attributeIntegerDisplay = attributeIntegerDisplay;
	}
	/**
	 * @return the attributeIntegerValues
	 */
	public Integer getAttributeIntegerValues() {
		return attributeIntegerValues;
	}
	/**
	 * @param attributeIntegerValues the attributeIntegerValues to set
	 */
	public void setAttributeIntegerValues(Integer attributeIntegerValues) {
		this.attributeIntegerValues = attributeIntegerValues;
	}
	/**
	 * @return the attributeStringValues
	 */
	public String getAttributeStringValues() {
		return attributeStringValues;
	}
	/**
	 * @param attributeStringValues the attributeStringValues to set
	 */
	public void setAttributeStringValues(String attributeStringValues) {
		this.attributeStringValues = attributeStringValues;
	}
    /**
     * @return the syouhinSysCode
     */
    public Long getSyouhinSysCode() {
        return syouhinSysCode;
    }
    /**
     * @param syouhinSysCode the syouhinSysCode to set
     */
    public void setSyouhinSysCode(Long syouhinSysCode) {
        this.syouhinSysCode = syouhinSysCode;
    }

}
