/************************************************************************
 * File Name EntTblBrandConditionRule.java
 * Author hoang.ho
 * Version 1.0.0
 * Date Created 2014/02/18
 * Date Updated 2014/02/18
 * Description �store value of EntTblBrandConditionRule.
 ***********************************************************************/
package net.webike.RcProductFactory.entity;

/**
 * @author luong.dai
 *
 */
public class EntTblBrandConditionRule {
    private Integer conditionRuleBrandCode;
    private Integer conditionRuleSort;
    private Integer conditionRuleCategoryCode;
    private Integer conditionRuleProperFrom;
    private Integer conditionRuleProperTo;
    private Long productId;
    private Integer minConditionRuleSort;
    private Integer brandMarumeCode = 1;
    private Integer conditionRuleProductProperSellingFlg = -1;
    private Integer conditionRuleProductOrderProductFlg = -1;
    private Integer conditionRuleProductNonReturnableFlg = -1;
    private Integer conditionRuleProductAmbiguousImageFlg = -1;
    /**
     * @return the conditionRuleBrandCode
     */
    public Integer getConditionRuleBrandCode() {
        return conditionRuleBrandCode;
    }
    /**
     * @param conditionRuleBrandCode the conditionRuleBrandCode to set
     */
    public void setConditionRuleBrandCode(Integer conditionRuleBrandCode) {
        this.conditionRuleBrandCode = conditionRuleBrandCode;
    }
    /**
     * @return the conditionRuleSort
     */
    public Integer getConditionRuleSort() {
        return conditionRuleSort;
    }
    /**
     * @param conditionRuleSort the conditionRuleSort to set
     */
    public void setConditionRuleSort(Integer conditionRuleSort) {
        this.conditionRuleSort = conditionRuleSort;
    }
    /**
     * @return the conditionRuleProperFrom
     */
    public Integer getConditionRuleProperFrom() {
        return conditionRuleProperFrom;
    }
    /**
     * @param conditionRuleProperFrom the conditionRuleProperFrom to set
     */
    public void setConditionRuleProperFrom(Integer conditionRuleProperFrom) {
        this.conditionRuleProperFrom = conditionRuleProperFrom;
    }
    /**
     * @return the conditionRuleProperTo
     */
    public Integer getConditionRuleProperTo() {
        return conditionRuleProperTo;
    }
    /**
     * @param conditionRuleProperTo the conditionRuleProperTo to set
     */
    public void setConditionRuleProperTo(Integer conditionRuleProperTo) {
        this.conditionRuleProperTo = conditionRuleProperTo;
    }
    /**
     * @return the productId
     */
    public Long getProductId() {
        return productId;
    }
    /**
     * @param productId the productId to set
     */
    public void setProductId(Long productId) {
        this.productId = productId;
    }
    /**
     * @return the conditionRuleCategoryCode
     */
    public Integer getConditionRuleCategoryCode() {
        return conditionRuleCategoryCode;
    }
    /**
     * @param conditionRuleCategoryCode the conditionRuleCategoryCode to set
     */
    public void setConditionRuleCategoryCode(Integer conditionRuleCategoryCode) {
        this.conditionRuleCategoryCode = conditionRuleCategoryCode;
    }
    /**
     * @return the minConditionRuleSort
     */
    public Integer getMinConditionRuleSort() {
        return minConditionRuleSort;
    }
    /**
     * @param minConditionRuleSort the minConditionRuleSort to set
     */
    public void setMinConditionRuleSort(Integer minConditionRuleSort) {
        this.minConditionRuleSort = minConditionRuleSort;
    }
    /**
     * @return the brandMarumeCode
     */
    public Integer getBrandMarumeCode() {
        return brandMarumeCode;
    }
    /**
     * @param brandMarumeCode the brandMarumeCode to set
     */
    public void setBrandMarumeCode(Integer brandMarumeCode) {
        this.brandMarumeCode = brandMarumeCode;
    }
    /**
     * @return the conditionRuleProductProperSellingFlg
     */
    public Integer getConditionRuleProductProperSellingFlg() {
        return conditionRuleProductProperSellingFlg;
    }
    /**
     * @param conditionRuleProductProperSellingFlg the conditionRuleProductProperSellingFlg to set
     */
    public void setConditionRuleProductProperSellingFlg(Integer conditionRuleProductProperSellingFlg) {
        this.conditionRuleProductProperSellingFlg = conditionRuleProductProperSellingFlg;
    }
    /**
     * @return the conditionRuleProductOrderProductFlg
     */
    public Integer getConditionRuleProductOrderProductFlg() {
        return conditionRuleProductOrderProductFlg;
    }
    /**
     * @param conditionRuleProductOrderProductFlg the conditionRuleProductOrderProductFlg to set
     */
    public void setConditionRuleProductOrderProductFlg(Integer conditionRuleProductOrderProductFlg) {
        this.conditionRuleProductOrderProductFlg = conditionRuleProductOrderProductFlg;
    }
    /**
     * @return the conditionRuleProductNonReturnableFlg
     */
    public Integer getConditionRuleProductNonReturnableFlg() {
        return conditionRuleProductNonReturnableFlg;
    }
    /**
     * @param conditionRuleProductNonReturnableFlg the conditionRuleProductNonReturnableFlg to set
     */
    public void setConditionRuleProductNonReturnableFlg(Integer conditionRuleProductNonReturnableFlg) {
        this.conditionRuleProductNonReturnableFlg = conditionRuleProductNonReturnableFlg;
    }
    /**
     * @return the conditionRuleProductAmbiguousImageFlg
     */
    public Integer getConditionRuleProductAmbiguousImageFlg() {
        return conditionRuleProductAmbiguousImageFlg;
    }
    /**
     * @param conditionRuleProductAmbiguousImageFlg the conditionRuleProductAmbiguousImageFlg to set
     */
    public void setConditionRuleProductAmbiguousImageFlg(Integer conditionRuleProductAmbiguousImageFlg) {
        this.conditionRuleProductAmbiguousImageFlg = conditionRuleProductAmbiguousImageFlg;
    }
}
