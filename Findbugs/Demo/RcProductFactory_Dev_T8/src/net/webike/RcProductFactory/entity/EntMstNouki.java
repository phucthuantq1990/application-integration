/************************************************************************
 * file name	： EntMstNouki.java
 * author		： Le.Dinh
 * version		： 1.0.0
 * date created	： 2014/02/12
 * date updated	： 2014/02/12
 * description	： Entity EntMstNouki.
 *
 ***********************************************************************/
package net.webike.RcProductFactory.entity;

/**
 * EntMstNouki.
 */
public class EntMstNouki {
	private String  noukiCode;
	private String  noukiName1;
	/**
	 * @return the noukiCode
	 */
	public String getNoukiCode() {
		return noukiCode;
	}
	/**
	 * @param noukiCode the noukiCode to set
	 */
	public void setNoukiCode(String noukiCode) {
		this.noukiCode = noukiCode;
	}
	/**
	 * @return the noukiName1
	 */
	public String getNoukiName1() {
		return noukiName1;
	}
	/**
	 * @param noukiName1 the noukiName1 to set
	 */
	public void setNoukiName1(String noukiName1) {
		this.noukiName1 = noukiName1;
	}

}
