/************************************************************************
 * file name	： EntMstLinkReason.java
 * author		： Nguyen.Chuong
 * version		： 1.0.0
 * date created	： 2014/02/17
 * date updated	： 2014/02/17
 * description	： Entity EntMstLinkReason to map with rc_syouhin.mst_link_reason.
 *
 ***********************************************************************/
package net.webike.RcProductFactory.entity;

import java.util.Date;

/**
 * EntMstLinkReason.
 */
public class EntMstLinkReason {
	private Integer linkReasonCode;

	private String linkReasonName = "";

	private String linkReasonSort = "";

	private Date createdOn;

	private String createdUserId = "";

	private Date updatedOn;

	private String updatedUserId = "";

	private String rowId = "";

	private String crudFlg = "";

	/**
	 * @return the linkReasonCode
	 */
	public Integer getLinkReasonCode() {
		return linkReasonCode;
	}

	/**
	 * @param linkReasonCode the linkReasonCode to set
	 */
	public void setLinkReasonCode(Integer linkReasonCode) {
		this.linkReasonCode = linkReasonCode;
	}

	/**
	 * @return the linkReasonName
	 */
	public String getLinkReasonName() {
		return linkReasonName;
	}

	/**
	 * @param linkReasonName the linkReasonName to set
	 */
	public void setLinkReasonName(String linkReasonName) {
		this.linkReasonName = linkReasonName;
	}

	/**
	 * @return the linkReasonSort
	 */
	public String getLinkReasonSort() {
		return linkReasonSort;
	}

	/**
	 * @param linkReasonSort the linkReasonSort to set
	 */
	public void setLinkReasonSort(String linkReasonSort) {
		this.linkReasonSort = linkReasonSort;
	}

	/**
	 * @return the createdOn
	 */
	public Date getCreatedOn() {
		if (createdOn != null) {
            return (Date) createdOn.clone();
        }
        return null;
	}

	/**
	 * @param createdOn the createdOn to set
	 */
	public void setCreatedOn(Date createdOn) {
		if (createdOn != null) {
			this.createdOn = (Date) createdOn.clone();
        } else {
        	this.createdOn = null;
        }
	}

	/**
	 * @return the createdUserId
	 */
	public String getCreatedUserId() {
		return createdUserId;
	}

	/**
	 * @param createdUserId the createdUserId to set
	 */
	public void setCreatedUserId(String createdUserId) {
		this.createdUserId = createdUserId;
	}

	/**
	 * @return the updatedOn
	 */
	public Date getUpdatedOn() {
		if (updatedOn != null) {
            return (Date) updatedOn.clone();
        }
        return null;
	}

	/**
	 * @param updatedOn the updatedOn to set
	 */
	public void setUpdatedOn(Date updatedOn) {
		if (updatedOn != null) {
            this.updatedOn = (Date) updatedOn.clone();
        } else {
            this.updatedOn = null;
        }
	}

	/**
	 * @return the updatedUserId
	 */
	public String getUpdatedUserId() {
		return updatedUserId;
	}

	/**
	 * @param updatedUserId the updatedUserId to set
	 */
	public void setUpdatedUserId(String updatedUserId) {
		this.updatedUserId = updatedUserId;
	}

	/**
	 * @return the rowId
	 */
	public String getRowId() {
		return rowId;
	}

	/**
	 * @param rowId the rowId to set
	 */
	public void setRowId(String rowId) {
		this.rowId = rowId;
	}

	/**
	 * @return the crudFlg
	 */
	public String getCrudFlg() {
		return crudFlg;
	}

	/**
	 * @param crudFlg the crudFlg to set
	 */
	public void setCrudFlg(String crudFlg) {
		this.crudFlg = crudFlg;
	}

}
