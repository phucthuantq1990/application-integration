/************************************************************************
 * file name	： EntTblFactoryProductLink.java
 * author		： Tran.Thanh
 * version		： 1.0.0
 * date created	： 18 Feb 2014
 * date updated	： 18 Feb 2014
 * description	： Entity for Table tbl_factory_product_link.
 *
 ***********************************************************************/
package net.webike.RcProductFactory.entity;

import java.sql.Timestamp;

/**
 * Entity for Table tbl_factory_product_link.
 */
public class EntTblFactoryProductLink {
	private Long productId;

	private Integer productLinkSort;

	private Integer productLinkReasonCode;

	private String productLinkTitle = "";

	private String productLinkUrl = "";

	private Timestamp updatedOn;

	private String updatedUserId = "";

	private String idRow = "";

	private String crudFlg = "";

	private String linkReasonName = "";

	private Long syouhinSysCode;

	/**
	 * @return the productId
	 */
	public Long getProductId() {
		return productId;
	}

	/**
	 * @param productId the productId to set
	 */
	public void setProductId(Long productId) {
		this.productId = productId;
	}

	/**
	 * @return the productLinkSort
	 */
	public Integer getProductLinkSort() {
		return productLinkSort;
	}

	/**
	 * @param productLinkSort the productLinkSort to set
	 */
	public void setProductLinkSort(Integer productLinkSort) {
		this.productLinkSort = productLinkSort;
	}

	/**
	 * @return the productLinkReasonCode
	 */
	public Integer getProductLinkReasonCode() {
		return productLinkReasonCode;
	}

	/**
	 * @param productLinkReasonCode the productLinkReasonCode to set
	 */
	public void setProductLinkReasonCode(Integer productLinkReasonCode) {
		this.productLinkReasonCode = productLinkReasonCode;
	}

	/**
	 * @return the productLinkTitle
	 */
	public String getProductLinkTitle() {
		return productLinkTitle;
	}

	/**
	 * @param productLinkTitle the productLinkTitle to set
	 */
	public void setProductLinkTitle(String productLinkTitle) {
		this.productLinkTitle = productLinkTitle;
	}

	/**
	 * @return the productLinkUrl
	 */
	public String getProductLinkUrl() {
		return productLinkUrl;
	}

	/**
	 * @param productLinkUrl the productLinkUrl to set
	 */
	public void setProductLinkUrl(String productLinkUrl) {
		this.productLinkUrl = productLinkUrl;
	}

	/**
	 * @return the updatedOn
	 */
	public Timestamp getUpdatedOn() {
		if (updatedOn != null) {
            return (Timestamp) updatedOn.clone();
        }
        return null;
	}

	/**
	 * @param updatedOn the updatedOn to set
	 */
	public void setUpdatedOn(Timestamp updatedOn) {
		if (updatedOn != null) {
			this.updatedOn = (Timestamp) updatedOn.clone();
        } else {
        	this.updatedOn = null;
        }
	}

	/**
	 * @return the updatedUserId
	 */
	public String getUpdatedUserId() {
		return updatedUserId;
	}

	/**
	 * @param updatedUserId the updatedUserId to set
	 */
	public void setUpdatedUserId(String updatedUserId) {
		this.updatedUserId = updatedUserId;
	}

	/**
	 * @return the idRow
	 */
	public String getIdRow() {
		return idRow;
	}

	/**
	 * @param idRow the idRow to set
	 */
	public void setIdRow(String idRow) {
		this.idRow = idRow;
	}

	/**
	 * @return the crudFlg
	 */
	public String getCrudFlg() {
		return crudFlg;
	}

	/**
	 * @param crudFlg the crudFlg to set
	 */
	public void setCrudFlg(String crudFlg) {
		this.crudFlg = crudFlg;
	}

	/**
	 * @return the linkReasonName
	 */
	public String getLinkReasonName() {
		return linkReasonName;
	}

	/**
	 * @param linkReasonName the linkReasonName to set
	 */
	public void setLinkReasonName(String linkReasonName) {
		this.linkReasonName = linkReasonName;
	}

    /**
     * @return the syouhinSysCode
     */
    public Long getSyouhinSysCode() {
        return syouhinSysCode;
    }

    /**
     * @param syouhinSysCode the syouhinSysCode to set
     */
    public void setSyouhinSysCode(Long syouhinSysCode) {
        this.syouhinSysCode = syouhinSysCode;
    }

}
