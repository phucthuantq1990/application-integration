
package net.webike.RcProductFactory.entity;


/**
 * Entity of EntTblFactoryProductAttributeInteger.
 * @author Doan Chuong
 * Date Created �F 2014/03/05
 */
public class EntTblFactoryProductAttributeInteger {
    private Long productId;
    private Integer attributeCode;
    private String attributeIntegerDisplay;
    private Integer attributeIntegerValues;
    private Integer attributeIntegerSort;
    private String updatedOn;
    private String updatedUserId;
    /**
     * @return the productId
     */
    public Long getProductId() {
        return productId;
    }
    /**
     * @param productId the productId to set
     */
    public void setProductId(Long productId) {
        this.productId = productId;
    }
    /**
     * @return the attributeCode
     */
    public Integer getAttributeCode() {
        return attributeCode;
    }
    /**
     * @param attributeCode the attributeCode to set
     */
    public void setAttributeCode(Integer attributeCode) {
        this.attributeCode = attributeCode;
    }
    /**
     * @return the attributeIntegerDisplay
     */
    public String getAttributeIntegerDisplay() {
        return attributeIntegerDisplay;
    }
    /**
     * @param attributeIntegerDisplay the attributeIntegerDisplay to set
     */
    public void setAttributeIntegerDisplay(String attributeIntegerDisplay) {
        this.attributeIntegerDisplay = attributeIntegerDisplay;
    }
    /**
     * @return the attributeIntegerValues
     */
    public Integer getAttributeIntegerValues() {
        return attributeIntegerValues;
    }
    /**
     * @param attributeIntegerValues the attributeIntegerValues to set
     */
    public void setAttributeIntegerValues(Integer attributeIntegerValues) {
        this.attributeIntegerValues = attributeIntegerValues;
    }
    /**
     * @return the attributeIntegerSort
     */
    public Integer getAttributeIntegerSort() {
        return attributeIntegerSort;
    }
    /**
     * @param attributeIntegerSort the attributeIntegerSort to set
     */
    public void setAttributeIntegerSort(Integer attributeIntegerSort) {
        this.attributeIntegerSort = attributeIntegerSort;
    }
    /**
     * @return the updatedOn
     */
    public String getUpdatedOn() {
        return updatedOn;
    }
    /**
     * @param updatedOn the updatedOn to set
     */
    public void setUpdatedOn(String updatedOn) {
        this.updatedOn = updatedOn;
    }
    /**
     * @return the updatedUserId
     */
    public String getUpdatedUserId() {
        return updatedUserId;
    }
    /**
     * @param updatedUserId the updatedUserId to set
     */
    public void setUpdatedUserId(String updatedUserId) {
        this.updatedUserId = updatedUserId;
    }
}
