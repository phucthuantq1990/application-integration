package net.webike.RcProductFactory.exception;

import java.util.Date;

/**
 * This exception happens when user modifies database after another one updates previously.
 * @author nguyen.hieu
 *
 */
public class ExclusionException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	private Date updatedDate;
	private String updatedUserId;
	private long matterNo;
	private String matterName;

	//BOE #7208 Tran.Thanh : message when not deleted any product
	private String messageNoProduct;
	//EOE #7208 Tran.Thanh : message when not deleted any product

	/**
	 * Construct exclusion exception.
	 * @param matterNo int
	 * @param updatedUserId String
	 * @param matterName String
	 * @param updatedDate Date
	 */
	public ExclusionException(final long matterNo, final String matterName, final String updatedUserId, final Date updatedDate) {
		this.matterNo = matterNo;
		this.matterName = matterName;
		this.updatedUserId = updatedUserId;
        if (updatedDate != null) {
            this.updatedDate = (Date) updatedDate.clone();
        } else {
            this.updatedDate = null;
        }
	}

	//BOE #7208 Tran.Thanh : message when not deleted any product
	/**
	 * Construct exclusion exception with error message.
	 * @param messageNoProduct messageNoProduct
	 */
	public ExclusionException(final String messageNoProduct) {
		this.messageNoProduct = messageNoProduct;
	}
	//BOE #7208 Tran.Thanh : message when not deleted any product

	public long getMatterNo() {
		return matterNo;
	}

	/************************************************************************
	 * <b>Description:</b><br>
	 *  get UpdatedDate.
	 *
	 * @author		Nguyen.Chuong
	 * @date		Jan 29, 2014
	 * @return		Date
	 ************************************************************************/
	public Date getUpdatedDate() {
        if (updatedDate != null) {
            return (Date) updatedDate.clone();
        }
        return null;
	}

	public String getUpdatedUserId() {
		return updatedUserId;
	}

	public String getMatterName() {
		return matterName;
	}

	//BOE #7208 Tran.Thanh : message when not deleted any product
	/**
	 * @return the messageNoProduct
	 */
	public String getMessageNoProduct() {
		return messageNoProduct;
	}

	/**
	 * @param messageNoProduct the messageNoProduct to set
	 */
	public void setMessageNoProduct(String messageNoProduct) {
		this.messageNoProduct = messageNoProduct;
	}
	//BOE #7208 Tran.Thanh : message when not deleted any product

}
