package net.webike.RcProductFactory.action;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * Class test CommonAction.
 */
public class CommonActionTest {

	private CommonAction commonAction;

	/**----------------------------------------------------------------------------------.
	 * @method		setUp.
	 * @description method called before start test
	 * @author		Tran.Thanh
	 * @date		12 Sep 2013
	 * @throws      Exception    void
	 *----------------------------------------------------------------------------------*/
	@Before
	public void setUp() throws Exception {
		commonAction = new CommonAction();
	}

	/**----------------------------------------------------------------------------------.
	 * @method		tearDown.
	 * @description method called after test
	 * @author		Tran.Thanh
	 * @date		12 Sep 2013
	 * @throws      Exception    void
	 *----------------------------------------------------------------------------------*/
	@After
	public void tearDown() throws Exception {
	}

	/**----------------------------------------------------------------------------------.
	 * @method		testConvertDateToString.
     * @description Test function ConvertDateToString check output is correct.
	 * @author		Tran.Thanh
	 * @date		13 Sep 2013		void
	 *----------------------------------------------------------------------------------*/
	@Test
	public void testConvertDateToString() {
		Date d = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
    	String dateString = formatter.format(d);
        //String result = commonAction.convertDateToString(d);
		//assertEquals(dateString, result);
        //assertFalse(result == "2013/09/11");
	}


	/**----------------------------------------------------------------------------------.
	 * @method		testConvertDateToString_inputNull.
	 * @description Test fucntion ConvertDateToString when input Date is null
	 * @author		Tran.Thanh
	 * @date		13 Sep 2013		void
	 *----------------------------------------------------------------------------------*/
	@Test
	public void testConvertDateToStringInputNull() {
		//String result2 = commonAction.convertDateToString(null);
		//assertEquals("", result2);
		//assertFalse(result2 ==  "0000/00/01");
	}

}
