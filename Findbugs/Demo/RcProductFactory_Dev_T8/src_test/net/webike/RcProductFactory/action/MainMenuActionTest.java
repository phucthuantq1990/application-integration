package net.webike.RcProductFactory.action;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * Class test MainMenuAction.
 */
public class MainMenuActionTest {

	private MainMenuAction mainMenuAction;

	/**----------------------------------------------------------------------------------.
	 * @method		setUp.
	 * @description method called before start test
	 * @author		Tran.Thanh
	 * @date		12 Sep 2013
	 * @throws      Exception    void
	 *----------------------------------------------------------------------------------*/
	@Before
	public void setUp() throws Exception {
		mainMenuAction = new MainMenuAction();
	}

	/**----------------------------------------------------------------------------------.
	 * @method		tearDown.
	 * @description method called after test
	 * @author		Tran.Thanh
	 * @date		12 Sep 2013
	 * @throws      Exception    void
	 *----------------------------------------------------------------------------------*/
	@After
	public void tearDown() throws Exception {
	}

	/**----------------------------------------------------------------------------------.
	 * @method		testExecute
	 * @description test MainMenuAction execute return seccess
	 * @author		Tran.Thanh
	 * @date		13 Sep 2013		void
	 *----------------------------------------------------------------------------------*/
	@Test
	public void testExecute() {
		String s = "success";
		String success = mainMenuAction.execute();
		assertEquals(s, success);
	}

	/**----------------------------------------------------------------------------------.
	 * @method		testExecute
	 * @description test MainMenuAction execute return IsNotNull
	 * @author		Tran.Thanh
	 * @date		13 Sep 2013		void
	 *----------------------------------------------------------------------------------*/
	@Test
	public void testExecuteIsNotNull() {
		String result = mainMenuAction.execute();
		assertNotNull(result);
	}

}
