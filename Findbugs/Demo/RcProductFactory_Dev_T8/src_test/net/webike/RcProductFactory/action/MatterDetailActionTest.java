//package net.webike.RcProductFactory.action;
//
//import static org.junit.Assert.*;
//import static org.mockito.Mockito.when;
//
//import java.util.Date;
//
//import javax.servlet.http.HttpServletRequest;
//
//import net.webike.RcProductFactory.action.form.CommonForm;
//import net.webike.RcProductFactory.action.form.MatterDetailActionForm;
//import net.webike.RcProductFactory.entity.EntMstFactoryMatter;
//import net.webike.RcProductFactory.service.MatterService;
//
//import org.apache.log4j.Logger;
//import org.junit.After;
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.mockito.InjectMocks;
//import org.mockito.Mock;
//import org.mockito.MockitoAnnotations;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.test.context.ContextConfiguration;
//import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;
//import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
//
///**
// * jUnit test MatterDetailActionTest.
// */
//@RunWith(SpringJUnit4ClassRunner.class)
//@ContextConfiguration(locations = {"classpath:applicationContext-test.xml",
//                                   "classpath:dataSource-test.xml" })
//public class MatterDetailActionTest extends AbstractTransactionalJUnit4SpringContextTests {
//
//    @InjectMocks
//    private MatterDetailAction matterDetailAction = new MatterDetailAction();
//    @Mock
//    private HttpServletRequest request;
//    @Mock
//    private CommonForm commonForm;
//    @Mock
//    private MatterDetailActionForm matterDetailActionForm;
//
//    @Autowired
//    private MatterService matterService;
//
//    private static Logger log = Logger.getLogger(JSonActionTest.class);
//    /************************************************************************
//     * <b>Description:</b><br>
//     *  init mock object for this matterDetailAction action.
//     *
//     * @author		Nguyen.Chuong
//     * @date		Sep 27, 2013
//     * @throws      Exception exception
//     ************************************************************************/
//    @Before
//    public void setUp() throws Exception {
//        MockitoAnnotations.initMocks(this);
//    }
//
//    /************************************************************************
//     * <b>Description:</b><br>
//     *  do after test finish.
//     *
//     * @author		Nguyen.Chuong
//     * @date		Sep 27, 2013
//     * @throws      Exception		void
//     ************************************************************************/
//    @After
//    public void tearDown() throws Exception {
//    }
//
//    /************************************************************************
//     * <b>Description:</b><br>
//     *  test excecute function with no userId login. Expected "error" result.
//     *
//     * @author		Nguyen.Chuong
//     * @date		Sep 27, 2013
//     ************************************************************************/
//    @Test
//    public void testExecuteNoUserId() {
//        String user = "";
//        matterDetailAction.setMatterService(matterService);
//        matterDetailAction.setCommonForm(commonForm);
//        when(commonForm.getLoginId()).thenReturn(user);
//        String result = matterDetailAction.execute();
//        assertTrue("error" == result);
//    }
//
//    /************************************************************************
//     * <b>Description:</b><br>
//     *  test excecute function with no param matterNo. Expected "error" result.
//     *
//     * @author		Nguyen.Chuong
//     * @date		Sep 27, 2013
//     ************************************************************************/
//    @Test
//    public void testExecuteNoMatterNo() {
//        String user = "chuongId";
//        String matterNoNumber = "28abc";
//
//        matterDetailAction.setMatterService(matterService);
//        matterDetailAction.setCommonForm(commonForm);
//
//        when(commonForm.getLoginId()).thenReturn(user);
//        when(request.getParameter("matterNo")).thenReturn(matterNoNumber);
//
//        String result = matterDetailAction.execute();
//        assertTrue("error" == result);
//    }
//
//    /************************************************************************
//     * <b>Description:</b><br>
//     *  test excecute function with no entMatter get form DB. Expected "error" result.
//     *
//     * @author		Nguyen.Chuong
//     * @date		Sep 27, 2013
//     ************************************************************************/
//    @Test
//    public void testExecuteGetNullEnt() {
//        String user = "chuongId";
//        String matterNoNumber = "280000";
//        matterDetailAction.setMatterService(matterService);
//        matterDetailAction.setCommonForm(commonForm);
//
//        when(commonForm.getLoginId()).thenReturn(user);
//        when(request.getParameter("matterNo")).thenReturn(matterNoNumber);
//
//        String result = matterDetailAction.execute();
//        assertTrue("error" == result);
//    }
//
//    /************************************************************************
//     * <b>Description:</b><br>
//     *  test excecute function: login as operator but change . Expected "error" result.
//     *
//     * @author		Nguyen.Chuong
//     * @date		Sep 27, 2013
//     ************************************************************************/
//    @Test
//    public void testExecuteIdNotEqualmattChangeId() {
//        String user;
//        String matterNoNumber = "1";
//        EntMstFactoryMatter entMatter = matterService.selectDetailMatterByMatterNo(matterNoNumber);
//        user = entMatter.getMatterChargeUserId() + "T";
//
//        matterDetailAction.setMatterService(matterService);
//        matterDetailAction.setCommonForm(commonForm);
//
//        when(commonForm.getLoginId()).thenReturn(user);
//        when(request.getParameter("matterNo")).thenReturn(matterNoNumber);
//        when(commonForm.getLoginAuthority()).thenReturn("0");
//
//        String result = matterDetailAction.execute();
//        assertTrue("error" == result);
//    }
//
//    @Test
//    public void testDeleteMatter() {
//        String user = "chuongId";
//        String matterNoNumber = "2";
////        EntMstFactoryMatter entMatterOld = matterService.selectDetailMatterByMatterNo(matterNoNumber);
////        String oldUserName = entMatterOld.getKousinUserId();
////        Date oldDate = entMatterOld.getKousinDate();
//
//        matterDetailAction.setMatterService(matterService);
//        matterDetailAction.setCommonForm(commonForm);
//
//        when(commonForm.getLoginId()).thenReturn(user);
//        when(request.getParameter("matterNo")).thenReturn(matterNoNumber);
//
//        String result = matterDetailAction.deleteMatter();
//        EntMstFactoryMatter entMatter = matterService.selectDetailMatterByMatterNo(matterNoNumber);
//        assertTrue(null == entMatter);
//        assertTrue("deleted" == result);
////
////        entMatterOld.setKousinUserId(oldUserName);
////        entMatterOld.setKousinDate(oldDate);
////        matterService.updateDetailMatter(entMatterOld);
//    }
//
//    @Test
//    public void testUpdateDetailMatterNoUser() {
//        String user = "";
//        matterDetailAction.setMatterService(matterService);
//        matterDetailAction.setCommonForm(commonForm);
//        when(commonForm.getLoginId()).thenReturn(user);
//        String result = matterDetailAction.updateDetailMatter();
//        assertTrue("error" == result);
//    }
//
//    @Test
//    public void testUpdateDetailMatterMttNoIsNotInter() {
//        String user = "chuongId";
//        String matterNoNumber = "28abc";
//
//        matterDetailAction.setMatterService(matterService);
//        matterDetailAction.setCommonForm(commonForm);
//
//        when(commonForm.getLoginId()).thenReturn(user);
//        when(request.getParameter("matterNo")).thenReturn(matterNoNumber);
//
//        String result = matterDetailAction.updateDetailMatter();
//        assertTrue("error" == result);
//    }
//
//    @Test
//    public void testUpdateDetail() {
//        String user = "chuongId";
//        String matterNoNumber = "3";
//        EntMstFactoryMatter entMatterOld = matterService.selectDetailMatterByMatterNo(matterNoNumber);
//
//        matterDetailAction.setMatterService(matterService);
//        matterDetailAction.setCommonForm(commonForm);
//
//        when(commonForm.getLoginId()).thenReturn(user);
//        when(request.getParameter("matterNo")).thenReturn(matterNoNumber);
//        when(request.getParameter("mName")).thenReturn(entMatterOld.getMatterName());
//        when(request.getParameter("status")).thenReturn(entMatterOld.getMatterStatusCode());
//        when(request.getParameter("matterChargeUserId")).thenReturn(user);
//        when(request.getParameter("mMessage")).thenReturn(entMatterOld.getMatterMessage());
//
//
//        String result = matterDetailAction.updateDetailMatter();
//        EntMstFactoryMatter entMatter = matterService.selectDetailMatterByMatterNo(matterNoNumber);
//        int compareDate = entMatterOld.getKousinDate().compareTo(entMatter.getKousinDate());
//        assertTrue(0 != compareDate);
//        assertTrue(entMatterOld.getKousinUserId().equals(entMatter.getKousinUserId()));
//        assertTrue("updated" == result);
//
//        matterService.updateDetailMatter(entMatterOld);
//    }
//
//    /**
//     * @return the log
//     */
//    public static Logger getLog() {
//        return log;
//    }
//
//    /**
//     * @param log the log to set
//     */
//    public static void setLog(Logger log) {
//        MatterDetailActionTest.log = log;
//    }
//
//    /**
//     * @return the matterDetailActionForm
//     */
//    public MatterDetailActionForm getMatterDetailActionForm() {
//        return matterDetailActionForm;
//    }
//
//    /**
//     * @param matterDetailActionForm the matterDetailActionForm to set
//     */
//    public void setMatterDetailActionForm(MatterDetailActionForm matterDetailActionForm) {
//        this.matterDetailActionForm = matterDetailActionForm;
//    }
//
//}
