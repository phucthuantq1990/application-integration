package net.webike.RcProductFactory.action;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import net.webike.RcProductFactory.entity.EntMstFactoryMatter;
import net.webike.RcProductFactory.service.MatterService;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * jUnit test jSonAction.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath*:applicationContext-test.xml",
                                   "classpath*:dataSource-test.xml" })
public class JSonActionTest {
    @InjectMocks
    private JSonAction jSonAction = new JSonAction();

    @Mock
    private HttpServletRequest request;

    @Autowired
    private MatterService matterService;

    private static Logger log = Logger.getLogger(JSonActionTest.class);

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @After
    public void tearDown() throws Exception {
    }

    /************************************************************************
     * <b>Description:</b><br>
     *  Test function mainMenuLoadJson get data follow status and user login.
     *
     * @author		Nguyen.Chuong
     * @date		Sep 23, 2013		void
     ************************************************************************/
    @Test
    public void testMainMenuLoadJson() {
        String status = "00";
        String user = "chuongId";
        jSonAction.setMatterService(matterService);
        when(request.getParameter("status")).thenReturn(status);
        when(request.getParameter("user")).thenReturn(user);
        List<EntMstFactoryMatter> list = matterService.selectListMatterByStatusAndUser(status, user);
        String result = jSonAction.mainMenuLoadJson();
        assertNotNull("failure - list EntMstFactoryMatter is null", list);
        assertTrue("action excecute success", "list" == result);
    }

    /**
     * @return the matterService
     */
    public MatterService getMatterService() {
        return matterService;
    }

    /**
     * @param matterService the matterService to set
     */
    public void setMatterService(MatterService matterService) {
        this.matterService = matterService;
    }

    /**
     * @return the log
     */
    public static Logger getLog() {
        return log;
    }

    /**
     * @param log the log to set
     */
    public static void setLog(Logger log) {
        JSonActionTest.log = log;
    }
}
