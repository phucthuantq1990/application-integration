/************************************************************************
 * file name	： customUserDetailsService.java
 * author		： Tran.Thanh
 * version		： 1.0.0
 * date created	： 23 Dec 2013
 * date updated	： 23 Dec 2013
 * description	： Replace your description for this class here
 *
 ***********************************************************************/
package net.webike.RcProductFactory.service;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

import net.webike.RcProductFactory.entity.EntMstFactoryUserTmp;
import net.webike.RcProductFactory.entity.EntTblFactorySetting;
import net.webike.RcProductFactory.util.Constant;
import net.webike.RcProductFactory.util.RedmineUtils;
import net.webike.RcProductFactory.util.Utils;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

/**
 * Class custom spring security.
 */
public class CustomUserDetailsService implements UserDetailsService {

	@Autowired
	private UserTmpService userTmpService;
	@Autowired
	private SettingService settingService;

	@Override
	public UserDetails loadUserByUsername(String loginId) {
		UserDetails user = null;
		List<String> listRoleAuth;
		List<EntTblFactorySetting> listEntTblFactorySetting = settingService.selectListFactorySetting();
		String projectId  = settingService.selectSettingValueBySettingCode(Constant.REDMINE_PRODUCTFACTORY_PJ_ID);

		try {
			ServletRequestAttributes requestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
			String passWordRQ =  requestAttributes.getRequest().getParameter("j_password");
			String passWordMD5 =  Utils.convertStringToMd5(passWordRQ);

			EntMstFactoryUserTmp entUserTmp;
			entUserTmp = userTmpService.selectUserTmpById(loginId);
			// If EntMstFactoryUserTmp exits in database  => login PF
			String urlRedmineInDb = settingService.selectSettingValueBySettingCode("REDMINE_URL");
			String redmineUserUrl = urlRedmineInDb + Constant.REDMINE_CURRENT_USER_URL;
			String redmineHostName = settingService.selectSettingValueBySettingCode("REDMINE_HOST_NAME");
			if (entUserTmp != null) {
				if (StringUtils.isNotEmpty(entUserTmp.getUserPasswd())) {
					com.taskadapter.redmineapi.bean.User userRedmine = null;
					HttpsURLConnection httpsCon = RedmineUtils.getResultHttpsConWithApiKey(
														entUserTmp.getUserRedmineApiKey(),
														redmineUserUrl,
														redmineHostName);

					if (httpsCon.getResponseCode() == Constant.HTTPSCON_RESPONSECODE) {
						userRedmine = RedmineUtils.getJsonUserRedmine(httpsCon);
						List<String> listRoleUser = RedmineUtils.getListRolebyProjectId(
																projectId,
																userRedmine);
						// get listRoleAuth
						listRoleAuth = Utils.checkRoleOfCurrentuser(listRoleUser, listEntTblFactorySetting);
						user = new User(loginId, entUserTmp.getUserPasswd(), true, true, true, true, getAuthorities(listRoleAuth));
					} else {
						user = new User(loginId, entUserTmp.getUserPasswd(), true, true, true, true, getAuthorities(null));
					}
				}
			// Else EntMstFactoryUserTmp not exits in database => call Redmine Api
			} else {
				com.taskadapter.redmineapi.bean.User userRedmine = null;
				HttpsURLConnection httpsCon = RedmineUtils.getResultHttpsConWithUser(
															 loginId,
															 passWordRQ,
															 redmineUserUrl,
															 redmineHostName);

				if (httpsCon.getResponseCode() == Constant.HTTPSCON_RESPONSECODE) {
					userRedmine = RedmineUtils.getJsonUserRedmine(httpsCon);
					// initUserToInsertDB
					entUserTmp = this.initUserToInsertDB(userRedmine, requestAttributes, passWordMD5);
					// insertNewUserTmp
					 boolean reInsert =	userTmpService.insertNewUserTmp(entUserTmp);
					 if (reInsert) {
						// get list Role
						List<String> listRoleUser = RedmineUtils.getListRolebyProjectId(
																projectId,
																userRedmine);
						// get listRoleAuth
						listRoleAuth = Utils.checkRoleOfCurrentuser(listRoleUser, listEntTblFactorySetting);
						user = new User(loginId, passWordMD5, true, true, true, true, getAuthorities(listRoleAuth));
					 }
				}
			}
		} catch (RuntimeException e) {
		    throw e;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return user;
	}

	/**
	 * Retrieves the correct ROLE type depending on the access level, where access level is an Integer.
	 * Basically, this interprets the access value whether it's for a regular user or admin.
	 *
	 * @param listRoleAuth List<String>
	 * @return collection of granted authorities
	 */
	 public Collection<GrantedAuthority> getAuthorities(List<String> listRoleAuth) {

			List<GrantedAuthority> authList = new ArrayList<GrantedAuthority>();
			if (listRoleAuth == null) {
				return authList;
			}
			for (String roleAuth : listRoleAuth) {
				authList.add(new SimpleGrantedAuthority(roleAuth));
			}

			return authList;
	  }

	 /************************************************************************
	 * <b>Description:</b><br>
	 *  initUserToInsertDB.
	 *
	 * @author		Le.Dinh
	 * @date		10 Jan 2014
	 * @param 		userRedmine	User
	 * @param 		requestAttributes	ServletRequestAttributes
	 * @param 		passWordMD5	String
	 * @return		EntMstFactoryUserTmp
	 * @throws 		NoSuchAlgorithmException	NoSuchAlgorithmException
	 ************************************************************************/
	private EntMstFactoryUserTmp initUserToInsertDB(com.taskadapter.redmineapi.bean.User userRedmine
			 										 , ServletRequestAttributes requestAttributes
			 										 , String passWordMD5) throws NoSuchAlgorithmException {

		 	EntMstFactoryUserTmp entUserTmpInsert = new EntMstFactoryUserTmp();
		 	entUserTmpInsert.setUserId(userRedmine.getLogin());
			entUserTmpInsert.setUserPasswd(passWordMD5);
			entUserTmpInsert.setUserLastName(userRedmine.getLastName());
			entUserTmpInsert.setUserFirstName(userRedmine.getFirstName());
			entUserTmpInsert.setUserEmail(userRedmine.getMail());
			entUserTmpInsert.setUserDelFlg("0");
			entUserTmpInsert.setUserRedmineApiKey(userRedmine.getApiKey());
			entUserTmpInsert.setCreatedUserId(userRedmine.getLogin());
			entUserTmpInsert.setUpdateUserId(userRedmine.getLogin());

			return entUserTmpInsert;
	 }

}
