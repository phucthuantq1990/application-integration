/************************************************************************
 * File Name	： TblFactoryCalibrationRequestIndividualMapper.java
 * Author		： Dau.Phuong
 * Version		： 1.0.0
 * Date Created	： 2014/07/16
 * Date Updated	： 2014/07/16
 * Description	： Contain method to get data from database .
 *
 ***********************************************************************/
package net.webike.RcProductFactory.mapper;

import java.sql.SQLException;
import java.util.List;

import net.webike.RcProductFactory.entity.EntTblFactoryCalibrationRequestIndividual;

import org.apache.ibatis.annotations.Param;

/**
 * Interface TblFactoryCalibrationRequestIndividualMapper.
 */
public interface TblFactoryCalibrationRequestIndividualMapper {
    /************************************************************************
     * <b>Description:</b><br>
     *  select data calibration request individual from rc_product_factory.tbl_factory_calibration_request_individual.
     *
     * @author		Dau.Phuong
     * @date		July 16, 2014
     * @param       ent            EntTblFactoryCalibrationRequestIndividual
     * @param       matterNo       matter number
     * @return		List<EntTblFactoryCalibrationRequestIndividual>
     ************************************************************************/
    List<EntTblFactoryCalibrationRequestIndividual> selectCalibrationRequestIndividual(@Param("entTblFactoryCalibrationRequestIndividual")EntTblFactoryCalibrationRequestIndividual ent, @Param("matterNo")Long matterNo);

    /************************************************************************
     * <b>Description:</b><br>
     *  select calibration type from mst_factory_calibration_type.
     *
     * @author      Dau.Phuong
     * @date        July 16, 2014
     * @return      List<EntTblFactoryCalibrationRequestIndividual>
     ************************************************************************/
    List<EntTblFactoryCalibrationRequestIndividual> selectCalibrationType();

    /************************************************************************
     * <b>Description:</b><br>
     *  update status check when preview product.
     * @author      Dau.Phuong
     * @date        July 18, 2014
     * @param       entTblFactoryCalibrationRequestIndividual           entTblFactoryCalibrationRequestIndividual
     ************************************************************************/
    void changeCorrespondRequest(@Param("entTblFactoryCalibrationRequestIndividual")EntTblFactoryCalibrationRequestIndividual entTblFactoryCalibrationRequestIndividual); 

    /************************************************************************
     * <b>Description:</b><br>
     *  Update content of request.
     *
     * @author      Dau.Phuong
     * @date        July 18, 2014
     * @param       entTblFactoryCalibrationRequestIndividual           entTblFactoryCalibrationRequestIndividual
     ************************************************************************/
    void updateContentRequest(@Param("entTblFactoryCalibrationRequestIndividual")EntTblFactoryCalibrationRequestIndividual entTblFactoryCalibrationRequestIndividual);

    /************************************************************************
     * <b>Description:</b><br>
     * delete  calibration request individual
     *
     * @author      Luong.Tuong
     * @date        July 21, 2014
     * @param       ent EntTblFactoryCalibrationRequestIndividual
     * @throws      SQLException        SQLException
     ************************************************************************/
    void deleteCalibrationRequestIndividual(@Param("entTblFactoryCalibrationRequestIndividual")EntTblFactoryCalibrationRequestIndividual ent) throws SQLException;

    /************************************************************************
     * <b>Description:</b><br>
     *  insert new calibration request individal into DB rc_product_factory.tbl_factory_calibration_request_individual.
     *
     * @author      Nguyen.Chuong
     * @date        Jul 21, 2014
     * @param       ent EntTblFactoryCalibrationRequestWhole content data.
     * @return      1 => insert success, 0 => fail
     ************************************************************************/
    Integer insertNewCalibrationRequestIndividual(EntTblFactoryCalibrationRequestIndividual ent);

    /************************************************************************
     * <b>Description:</b><br>
     *  select max individual code in DB.
     *
     * @author      Nguyen.Chuong
     * @date        Jul 21, 2014
     * @return      max individual code in DB.
     ************************************************************************/
    Long selectMaxIndividualCode();
}
