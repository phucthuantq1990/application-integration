<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN"
"http://mybatis.org/dtd/mybatis-3-mapper.dtd">

<mapper namespace="net.webike.RcProductFactory.mapper.TblFactoryProductModelMapper">
    <resultMap id="factoryProductModelResultMap" type="net.webike.RcProductFactory.entity.EntTblFactoryProductFitModel">
        <result column="product_id"                             property="productId" />
        <result column="fit_model_sort"                         property="fitModelSort" />
        <result column="fit_model_maker"                        property="fitModelMaker" />
        <result column="fit_model_maker_no_fit_flg"             property="fitModelMakerNoFitFlg" />
        <result column="fit_model_model"                        property="fitModelModel" />
        <result column="fit_model_model_no_fit_flg"             property="fitModelModelNoFitFlg" />
        <result column="fit_model_style"                        property="fitModelStyle" />
        <result column="fit_model_del_flg"                      property="fitModelDelFlg" />
        <result column="updated_date"                           property="updateDate" />
        <result column="updated_user_id"                        property="updateUserId" />
    </resultMap>

    <!-- BOE [#] Nguyen.Chuong 2014/01/23: add query to get list fit model in fitModelCheck page.-->
    <resultMap id="listFitModelResultMap" type="net.webike.RcProductFactory.entity.EntTblFactoryProductFitModel">
        <result column="productId"                  property="productId"/>
        <result column="fitModelSort"               property="fitModelSort"/>
        <result column="fitModelMaker"              property="fitModelMaker"/>
        <result column="fitModelMakerNoFitFlg"      property="fitModelMakerNoFitFlg"/>
        <result column="fitModelModel"              property="fitModelModel" />
        <result column="fitModelModelNoFitFlg"      property="fitModelModelNoFitFlg"/>
        <result column="fitModelStyle"              property="fitModelStyle" />
        <result column="fitModelDelFlg"             property="fitModelDelFlg"/>
        <result column="updateUserId"               property="updateUserId" />
        <result column="updateDate"                 property="updateDate" />
        <result column="id_row" 					property="rowId" />
        <result column="maker_logo" 				property="makerLogo" />
    </resultMap>
    <select id="selectListFitModelOfProduct" parameterType="net.webike.RcProductFactory.entity.EntTblFactoryProductFitModel" resultMap="listFitModelResultMap">
        SELECT fitModel.product_id
             , fitModel.fit_model_sort                 AS fitModelSort
             , fitModel.fit_model_maker                AS fitModelMaker
             , fitModel.fit_model_maker_no_fit_flg     AS fitModelMakerNoFitFlg
             , fitModel.fit_model_model                AS fitModelModel
             , fitModel.fit_model_model_no_fit_flg     AS fitModelModelNoFitFlg
             , IFNULL(fitModel.fit_model_style, "")    AS fitModelStyle
             , fitModel.updated_date                   AS updateDate
             , fitModel.updated_user_id                AS updateUserId
             , CONCAT(fitModel.product_id, fitModel.fit_model_sort)    AS id_row
        FROM rc_product_factory.tbl_factory_product_fit_model AS fitModel
        <trim prefix="WHERE" prefixOverrides="AND">
            <if test="null != productId and '' != productId">
                AND fitModel.product_id = #{productId}
            </if>
            <if test="null != fitModelMaker and '' != fitModelMaker">
                AND fitModel.fit_model_maker LIKE CONCAT('%', #{fitModelMaker}, '%')
            </if>
            <if test="null != fitModelModel and '' != fitModelModel">
                AND fitModel.fit_model_model LIKE CONCAT('%', #{fitModelModel}, '%')
            </if>
            <if test="null != fitModelStyle and '' != fitModelStyle">
                AND fitModel.fit_model_style LIKE CONCAT('%', #{fitModelStyle}, '%')
            </if>
        </trim>
        ORDER BY ${sortField} ${sortDir};
    </select>
    <!-- EOE [#] Nguyen.Chuong 2014/01/23  -->

    <delete id="deleteFitModel" parameterType="java.lang.Long">
      DELETE FROM rc_product_factory.tbl_factory_product_fit_model
      WHERE
        product_id IN
        <foreach item="item" index="index" collection="list" open="(" separator="," close=")">
          #{item}
        </foreach>
    </delete>

    <select id="selectListFitModelWithMaxModelSort" parameterType="java.lang.Long" resultMap="factoryProductModelResultMap">
      <!-- BOE #7331 Luong.Dai 2014/07/03 Fix bug cannot add fit model for product have no fit model -->
      <!-- SELECT product_id                AS productId
             , MAX(fit_model_sort)     AS fitModelSort
      FROM rc_product_factory.tbl_factory_product_fit_model
      WHERE
        product_id IN
        <foreach item="item" index="index" collection="list" open="(" separator="," close=")">
          #{item}
        </foreach>
      GROUP BY product_id -->
    SELECT 	product.product_id                												AS productId
    		, IF (MAX(fit_model.fit_model_sort) IS NULL, -1, MAX(fit_model.fit_model_sort))	AS fitModelSort
	FROM
		rc_product_factory.tbl_factory_product						AS product
		LEFT JOIN rc_product_factory.tbl_factory_product_fit_model	AS fit_model
			ON product.product_id = fit_model.product_id
	WHERE
    	product.product_id IN
    	<foreach item="item" collection="list" open="(" separator="," close=")">
          #{item}
        </foreach>
	GROUP BY product.product_id;
    <!-- BOE #7331 Luong.Dai 2014/07/03 Fix bug cannot add fit model for product have no fit model -->
    </select>

    <insert id="insertToFitModel" parameterType="net.webike.RcProductFactory.entity.EntTblFactoryProductFitModel">
      INSERT INTO rc_product_factory.tbl_factory_product_fit_model
      ( product_id
      , fit_model_sort
      , fit_model_maker
      , fit_model_maker_no_fit_flg
      , fit_model_model
      , fit_model_model_no_fit_flg
      , fit_model_style
      , updated_user_id)
      VALUES
      ( #{productId}
      , #{fitModelSort}
      , #{fitModelMaker}
      , #{fitModelMakerNoFitFlg}
      , #{fitModelModel}
      , #{fitModelModelNoFitFlg}
      , #{fitModelStyle}
      , #{updateUserId})
    </insert>
    
    <insert id="insertFitModelMultiRow" parameterType="java.util.Map">
      INSERT INTO rc_product_factory.tbl_factory_product_fit_model
      ( product_id
      , fit_model_sort
      , fit_model_maker
      , fit_model_maker_no_fit_flg
      , fit_model_model
      , fit_model_model_no_fit_flg
      , fit_model_style
      , updated_user_id)
      VALUES
        <foreach item="item" collection="listFitModelToInsert" separator=",">
           (#{item.productId}
	      , #{item.fitModelSort}
	      , #{item.fitModelMaker}
	      , #{item.fitModelMakerNoFitFlg}
	      , #{item.fitModelModel}
	      , #{item.fitModelModelNoFitFlg}
	      , #{item.fitModelStyle}
	      , #{item.updateUserId})
        </foreach>
    </insert>
    
    <select id="selectListFitModelByProductId" parameterType="java.lang.Long" resultMap="factoryProductModelResultMap">
      SELECT fit_model_maker
           , fit_model_model
           , fit_model_style
      FROM rc_product_factory.tbl_factory_product_fit_model
      WHERE product_id = #{productId}
    </select>

    <!-- BOE 02/25/2014 Long Vu -->
    <insert id="insertNewFactoryProductFitModel" parameterType="net.webike.RcProductFactory.entity.EntProduct">
        INSERT INTO rc_product_factory.tbl_factory_product_fit_model
         (  product_id
          , fit_model_sort
          , fit_model_maker
          , fit_model_maker_no_fit_flg
          , fit_model_model
          , fit_model_model_no_fit_flg
          , fit_model_style
          , updated_date
          , updated_user_id
         )
         SELECT
            #{productId}
          , des.syouhin_fit_model_code
          , des.maker
          , 0
          , des.model
          , 0
          , des.style
          , NOW()
          , #{updatedUserId}
         FROM rc_syouhin.tbl_syouhin_fit_model AS des
         WHERE des.syouhin_sys_code = #{syouhinSysCode}
    </insert>
    <!-- EOE 02/25/2014 Long Vu -->
    <!-- BOE 2013/03/03 hoang.ho -->
    <delete id="deleteFactoryProductFitModel" parameterType="java.util.Map">
        DELETE FROM
		rc_product_factory.tbl_factory_product_fit_model
		WHERE product_id = #{entMstProduct.productId}
		AND fit_model_maker = #{entMstProduct.fitModelMaker}
		AND fit_model_model = #{entMstProduct.fitModelModel}
	   <if test="null == entMstProduct.fitModelStyle || '' == entMstProduct.fitModelStyle">
          AND (fit_model_style is null || fit_model_style = #{entMstProduct.fitModelStyle})
       </if>
       <if test="null != entMstProduct.fitModelStyle and '' != entMstProduct.fitModelStyle">
            AND fit_model_style = #{entMstProduct.fitModelStyle}
       </if>
    </delete>
    <select id="getProductFitModelErrorState" parameterType="java.util.Map" resultType="java.lang.Integer">
	    SELECT COUNT(product_id) FROM rc_product_factory.tbl_factory_product_fit_model fit
	    WHERE fit.product_id = #{productId}
	    AND (fit.fit_model_maker = 1
	    OR fit.fit_model_model = 1
	    OR fit.fit_model_style = 1)
    </select>
    <insert id="insertMultipleFitModel" parameterType="java.util.Map">
        INSERT INTO rc_product_factory.tbl_factory_product_fit_model(
             product_id
           , fit_model_sort
           , fit_model_maker
           , fit_model_maker_no_fit_flg
           , fit_model_model
           , fit_model_model_no_fit_flg
           , fit_model_style
           , updated_date
           , updated_user_id
        ) VALUES
        <foreach item="item" collection="fitModelList" separator=",">
            (
                #{item.productId}
              , #{item.fitModelSort}
              , #{item.fitModelMaker}
              <if test="item.fitModelMakerNoFitFlg == true">
                , 0
              </if>
              <if test="item.fitModelMakerNoFitFlg == false">
                , 1
              </if>
              , #{item.fitModelModel}
              <if test="item.fitModelModelNoFitFlg == true">
                , 0
              </if>
              <if test="item.fitModelModelNoFitFlg == false">
                , 1
              </if>
              , #{item.fitModelStyle}
              , NOW()
              , #{loginUserId}
            )
        </foreach>
    </insert>
    <delete id="deleteMultipleFitModelByProductsId" parameterType="java.util.Map">
        DELETE FROM rc_product_factory.tbl_factory_product_fit_model
        WHERE product_id IN (${deletingProductsStr})
    </delete>
    <!-- EOE 2013/03/03 hoang.ho -->

    <select id="selectSyouhinFitModelBySyouhinSysCodeList2" parameterType="java.lang.String" resultType="net.webike.RcProductFactory.entity.EntTblTmpCSV">
        SELECT
            syouhin_sys_code AS productSyouhinSysCode
            , maker AS fitModelMaker
            , model AS fitModelModel
            , style AS fitModelStyle
        FROM
            rc_syouhin.tbl_syouhin_fit_model
        WHERE
            syouhin_sys_code IN
            <foreach item="item" index="index" collection="list" open="(" separator="," close=")">
              #{item}
            </foreach>
        ORDER BY syouhin_sys_code
    </select>

	<!-- BOE #7203 Luong.Dai 2014/06/13 select list fit model of product with maker logo -->
	<select id="selectListFitModelWithMakerLogo"
		parameterType="net.webike.RcProductFactory.entity.EntTblFactoryProductFitModel"
		resultMap="listFitModelResultMap">
        SELECT fitModel.product_id
             , fitModel.fit_model_maker                	AS fitModelMaker
             , fitModel.fit_model_model                	AS fitModelModel
             , fitModel.fit_model_style                 AS fitModelStyle
             , logo.maker_logo							AS makerLogo
        FROM rc_product_factory.tbl_factory_product_fit_model 			AS fitModel
        	LEFT JOIN rc_product_factory.tbl_factory_maker_logo			AS logo
        		ON fitModel.fit_model_maker = logo.maker_name
        WHERE fitModel.product_id = #{productId}
        ORDER BY ${sortField} ${sortDir};
    </select>
    <!-- EOE #7203 Luong.Dai 2014/06/13 select list fit model of product with maker logo -->
    <!-- BOE @rcv! Luong.Dai 2014/08/22 select list product has fit model same data with entity model -->
    <select id="selectListProductIdByFitModelData" parameterType="java.util.Map" resultType="java.lang.Long">
        SELECT  product_id
        FROM 
            rc_product_factory.tbl_factory_product_fit_model
        WHERE 
            product_id IN
            <foreach item="product" collection="lstProduct" open="(" separator="," close=")">
                #{product.productId}
            </foreach>
            AND fit_model_maker = #{model.fitModelMaker} 
            AND fit_model_model = #{model.fitModelModel}
            AND fit_model_style = #{model.fitModelStyle}
        GROUP BY
            product_id
        LIMIT 0, 1000;
    </select>
    <!-- EOE @rcv! Luong.Dai 2014/08/22 select list product has fit model same data with entity model -->
    <!-- BOE @rcv!Luong.Dai 2014/08/22 insert multi fit model to DB by list ent product -->
    <insert id="insertMultiFitModel" parameterType="java.util.Map">
      INSERT INTO rc_product_factory.tbl_factory_product_fit_model
      ( product_id
      , fit_model_sort
      , fit_model_maker
      , fit_model_maker_no_fit_flg
      , fit_model_model
      , fit_model_model_no_fit_flg
      , fit_model_style
      , updated_user_id)
      VALUES
      <foreach item="product" collection="lstProduct" separator=",">
                ( #{product.productId}
			      , #{product.modelSort}
			      , #{model.fitModelMaker}
			      , #{model.fitModelMakerNoFitFlg}
			      , #{model.fitModelModel}
			      , #{model.fitModelModelNoFitFlg}
			      , #{model.fitModelStyle}
			      , #{updateUserId})
      </foreach>
    </insert>
    <!-- BOE @rcv!Luong.Dai 2014/08/22 insert multi fit model to DB by list ent product -->
</mapper>
