/************************************************************************
 * File Name    ： MstAttributeMapper.java
 * Author       ： hoang.ho
 * Version      ： 1.0.0
 * Date Created ： 2014/02/15
 * Date Updated ： 2014/02/15
 * Description  ： Contain method to get data from Mst_attribute database .
 *
 ***********************************************************************/
package net.webike.RcProductFactory.mapper;

import java.util.List;
import java.util.Map;

import net.webike.RcProductFactory.entity.EntMstAttribute;
import net.webike.RcProductFactory.entity.EntTblFactoryProductAttributeFlag;
import net.webike.RcProductFactory.entity.EntTblFactoryProductAttributeFloat;
import net.webike.RcProductFactory.entity.EntTblFactoryProductAttributeGroup;
import net.webike.RcProductFactory.entity.EntTblFactoryProductAttributeInteger;
import net.webike.RcProductFactory.entity.EntTblFactoryProductAttributeString;
import net.webike.RcProductFactory.entity.EntTblFactoryProductNew;
import net.webike.RcProductFactory.entity.EntTblTmpCSV;

import org.apache.ibatis.annotations.Param;

/**
 * Contain method to get data from Mst_attribute database .
 */
public interface MstAttributeMapper {
    /************************************************************************
     * <b>Description:</b><br>
     *  search and get list attribute by attrName.
     *
     * @author		Luong.Dai
     * @date		Jan 8, 2014
     * @param attrName String
     * @return		List<EntMstAttribute>
     ************************************************************************/
    List<EntMstAttribute> selectListAttribute(String attrName);

    /************************************************************************
     * <b>Description:</b><br>
     *  search and get list attribute by filter at attributeManage page.
     *
     * @author		Nguyen.Chuong
     * @date		Jan 14, 2014
     * @param       limit row per page
     * @param       offset start rows
     * @param       entMstAttribute filter value
     * @return		List<EntMstAttribute>
     ************************************************************************/
    List<EntMstAttribute> selectListAttributeWithFilter(@Param("limit")int limit
                                                      , @Param("offset")int offset
                                                      , @Param("entMstAttribute")EntMstAttribute entMstAttribute);

    /************************************************************************
     * <b>Description:</b><br>
     *  count record when get data from DB.
     *
     * @author		Nguyen.Chuong
     * @date		Jan 16, 2014
     * @param       entMstAttribute entity contain filter
     * @return		int
     ************************************************************************/
    int selectTotalRecordOfFilter(@Param("entMstAttribute") EntMstAttribute entMstAttribute);

    /************************************************************************
     * <b>Description:</b><br>
     *  Select attribute by code.
     *
     * @author		Luong.Dai
     * @date		Jan 15, 2014
     * @param attributeCode Integer
     * @return		EntMstAttribute
     ************************************************************************/
    EntMstAttribute selectAttributeByCode(Integer attributeCode);

    /************************************************************************
     * <b>Description:</b><br>
     * select attribute by name of code but different the given code.
     *
     * @author      Long Vu
     * @date        Jan 14, 2014
     * @param       attribute attribute
     * @return      EntMstAttribute
     ************************************************************************/
    EntMstAttribute selectAttributeWithouCode(EntMstAttribute attribute);

    /************************************************************************
     * <b>Description:</b><br>
     * select attribute by name.
     *
     * @author      Long Vu
     * @date        Jan 14, 2014
     * @param       attributeName attribute name
     * @return      EntMstAttribute
     ************************************************************************/
    EntMstAttribute selectAttributeByName(String attributeName);

    /************************************************************************
     * <b>Description:</b><br>
     *  Insert new attribute to DB.
     *
     * @author		Luong.Dai
     * @date		Jan 16, 2014
     * @param attribute EntMstAttribute
     * @param map Map<String, Integer>
     * @return		int
     ************************************************************************/
    int insertNewAttribute(@Param("attribute") EntMstAttribute attribute, @Param("map") Map<String, Integer> map);

    /************************************************************************
     * <b>Description:</b><br>
     * udpate data to attribute table.
     *
     * @author      Long Vu
     * @date        Jan 14, 2014
     * @param       attribute object to update.
     * @return      int.
     ************************************************************************/
    int updateAttributeByCode(EntMstAttribute attribute);

    /************************************************************************
     * <b>Description:</b><br>
     *  Select list attribute by categoryCode.
     *
     * @author		Luong.Dai
     * @date		Feb 12, 2014
     * @param       product EntTblFactoryProductNew
     * @return		List<EntMstAttribute>
     ************************************************************************/
    List<EntMstAttribute> selectAttributeByCategoryCode(EntTblFactoryProductNew product);

    /************************************************************************
     * <b>Description:</b><br>
     *  When attribute_type = Flag, get attribute_flag_values.
     *
     * @author		Luong.Dai
     * @date		Feb 12, 2014
     * @param       product EntTblFactoryProductNew
     * @return		EntMstAttribute
     ************************************************************************/
    EntMstAttribute selectAttributeFlagValues(EntTblFactoryProductNew product);

    /************************************************************************
     * <b>Description:</b><br>
     *  When attribute_type = Float, get attribute_float_values, attribute_float_display.
     *
     * @author		Luong.Dai
     * @date		Feb 12, 2014
     * @param       product EntTblFactoryProductNew
     * @return		EntMstAttribute
     ************************************************************************/
    EntMstAttribute selectAttributeFloatValues(EntTblFactoryProductNew product);

    /************************************************************************
     * <b>Description:</b><br>
     *  When attribute_type = Group, get attribute_group_name, attribute_group_display.
     *
     * @author		Luong.Dai
     * @date		Feb 12, 2014
     * @param       product EntTblFactoryProductNew
     * @return		EntMstAttribute
     ************************************************************************/
    EntMstAttribute selectAttributeGroupValues(EntTblFactoryProductNew product);

    /************************************************************************
     * <b>Description:</b><br>
     *  When attribute_type = Integer, get attribute_integer_values, attribute_integer_display.
     *
     * @author		Luong.Dai
     * @date		Feb 12, 2014
     * @param       product EntTblFactoryProductNew
     * @return		EntMstAttribute
     ************************************************************************/
    EntMstAttribute selectAttributeIntegerValues(EntTblFactoryProductNew product);

    /************************************************************************
     * <b>Description:</b><br>
     *  When attribute_type = String, get attribute_string_values.
     *
     * @author		Luong.Dai
     * @date		Feb 12, 2014
     * @param       product EntTblFactoryProductNew
     * @return		EntMstAttribute
     ************************************************************************/
    EntMstAttribute selectAttributeStringValues(EntTblFactoryProductNew product);

    /************************************************************************
     * <b>Description:</b><br>
     *  Select all list attribute with del_flg = 0.
     *
     * @author		Luong.Dai
     * @date		Feb 13, 2014
     * @return		List<EntMstAttribute>
     ************************************************************************/
    List<EntMstAttribute> selectAllListAttribute();

    /************************************************************************
     * <b>Description:</b><br>
     *  select all list attribute by productId at table tbl_factory_product_attribute_group.
     *
     * @author		Luong.Dai
     * @date		Feb 14, 2014
     * @param       productId Long
     * @return		List<EntMstAttribute>
     ************************************************************************/
    List<EntMstAttribute> selectAllListAttributeByAttributeGroup(Long productId);

    /************************************************************************
     * <b>Description:</b><br>
     *  select all list attribute by productId at table tbl_factory_product_attribute_flag.
     *
     * @author      Luong.Dai
     * @date        Feb 14, 2014
     * @param       productId Long
     * @return      List<EntMstAttribute>
     ************************************************************************/
    List<EntMstAttribute> selectAllListAttributeByAttributeFlag(Long productId);

    /************************************************************************
     * <b>Description:</b><br>
     *  select all list attribute by productId at table tbl_factory_product_attribute_integer.
     *
     * @author      Luong.Dai
     * @date        Feb 14, 2014
     * @param       productId Long
     * @return      List<EntMstAttribute>
     ************************************************************************/
    List<EntMstAttribute> selectAllListAttributeByAttributeInteger(Long productId);

    /************************************************************************
     * <b>Description:</b><br>
     *  select all list attribute by productId at table tbl_factory_product_attribute_string.
     *
     * @author      Luong.Dai
     * @date        Feb 14, 2014
     * @param       productId Long
     * @return      List<EntMstAttribute>
     ************************************************************************/
    List<EntMstAttribute> selectAllListAttributeByAttributeString(Long productId);

    /************************************************************************
     * <b>Description:</b><br>
     *  select all list attribute by productId at table tbl_factory_product_attribute_float.
     *
     * @author      Luong.Dai
     * @date        Feb 14, 2014
     * @param       productId Long
     * @return      List<EntMstAttribute>
     ************************************************************************/
    List<EntMstAttribute> selectAllListAttributeByAttributeFloat(Long productId);

    /**
     * Get attribute list.
     * @author		nguyen.hieu
     * @date		2014-02-17
     * @return		Attribute list.
     */
    List<EntMstAttribute> selectAttributeList();

    /************************************************************************
     * <b>Description:</b><br>
     *  Select attributes by list of code.
     *
     * @author      Doan.Chuong
     * @date        Feb 28, 2014
     * @param       attributeCodeList List<String>
     * @return      EntMstAttribute
     ************************************************************************/
    List<EntMstAttribute> selectAttributeByCodes(List<String> attributeCodeList);

    /************************************************************************
     * <b>Description:</b><br>
     *  Delete attribute flag by product id and attribute code.
     *
     * @author      Doan.Chuong
     * @date        March 5, 2014
     * @param       productId long
     * @param       attributeCode int
     ************************************************************************/
    void deleteAttributeFlagByProductIdAndAttributeCode(@Param("productId") long productId, @Param("attributeCode") int attributeCode);

    /************************************************************************
     * <b>Description:</b><br>
     *  Delete attribute float by product id and attribute code.
     *
     * @author      Doan.Chuong
     * @date        March 5, 2014
     * @param       productId long
     * @param       attributeCode int
     ************************************************************************/
    void deleteAttributeFloatByProductIdAndAttributeCode(@Param("productId") long productId, @Param("attributeCode") int attributeCode);

    /************************************************************************
     * <b>Description:</b><br>
     *  Delete attribute group by product id and attribute code.
     *
     * @author      Doan.Chuong
     * @date        March 5, 2014
     * @param       productId long
     * @param       attributeCode int
     ************************************************************************/
    void deleteAttributeGroupByProductIdAndAttributeCode(@Param("productId") long productId, @Param("attributeCode") int attributeCode);

    /************************************************************************
     * <b>Description:</b><br>
     *  Delete attribute integer by product id and attribute code.
     *
     * @author      Doan.Chuong
     * @date        March 5, 2014
     * @param       productId long
     * @param       attributeCode int
     ************************************************************************/
    void deleteAttributeIntegerByProductIdAndAttributeCode(@Param("productId") long productId, @Param("attributeCode") int attributeCode);

    /************************************************************************
     * <b>Description:</b><br>
     *  Delete attribute string by product id and attribute code.
     *
     * @author      Doan.Chuong
     * @date        March 5, 2014
     * @param       productId long
     * @param       attributeCode int
     ************************************************************************/
    void deleteAttributeStringByProductIdAndAttributeCode(@Param("productId") long productId, @Param("attributeCode") int attributeCode);

    /************************************************************************
     * <b>Description:</b><br>
     *  Delete attribute flag by product id.
     *
     * @author      Doan.Chuong
     * @date        March 5, 2014
     * @param       productId long
     ************************************************************************/
    void deleteAttributeFlagByProductId(long productId);

    /************************************************************************
     * <b>Description:</b><br>
     *  Delete attribute float by product id.
     *
     * @author      Doan.Chuong
     * @date        March 5, 2014
     * @param       productId long
     ************************************************************************/
    void deleteAttributeFloatByProductId(long productId);

    /************************************************************************
     * <b>Description:</b><br>
     *  Delete attribute group by product id.
     *
     * @author      Doan.Chuong
     * @date        March 5, 2014
     * @param       productId long
     ************************************************************************/
    void deleteAttributeGroupByProductId(long productId);

    /************************************************************************
     * <b>Description:</b><br>
     *  Delete attribute integer by product id.
     *
     * @author      Doan.Chuong
     * @date        March 5, 2014
     * @param       productId long
     ************************************************************************/
    void deleteAttributeIntegerByProductId(long productId);

    /************************************************************************
     * <b>Description:</b><br>
     *  Delete attribute string by product id.
     *
     * @author      Doan.Chuong
     * @date        March 5, 2014
     * @param       productId long
     ************************************************************************/
    void deleteAttributeStringByProductId(long productId);

    /************************************************************************
     * <b>Description:</b><br>
     *  insertFactoryProductAttributeFlag.
     *
     * @author      Doan.Chuong
     * @date        05 March 2014
     * @param       ent EntTblFactoryProductAttributeFlag
     * @return      int
     ************************************************************************/
    int insertFactoryProductAttributeFlag(EntTblFactoryProductAttributeFlag ent);

    /************************************************************************
     * <b>Description:</b><br>
     *  insertFactoryProductAttributeFloat.
     *
     * @author      Doan.Chuong
     * @date        05 March 2014
     * @param       ent EntTblFactoryProductAttributeFloat
     * @return      int
     ************************************************************************/
    int insertFactoryProductAttributeFloat(EntTblFactoryProductAttributeFloat ent);

    /************************************************************************
     * <b>Description:</b><br>
     *  insertFactoryProductAttributeGroup.
     *
     * @author      Doan.Chuong
     * @date        05 March 2014
     * @param       ent EntTblFactoryProductAttributeGroup
     * @return      int
     ************************************************************************/
    int insertFactoryProductAttributeGroup(EntTblFactoryProductAttributeGroup ent);

    /************************************************************************
     * <b>Description:</b><br>
     *  insertFactoryProductAttributeInteger.
     *
     * @author      Doan.Chuong
     * @date        05 March 2014
     * @param       ent EntTblFactoryProductAttributeInteger
     * @return      int
     ************************************************************************/
    int insertFactoryProductAttributeInteger(EntTblFactoryProductAttributeInteger ent);

    /************************************************************************
     * <b>Description:</b><br>
     *  insertFactoryProductAttributeString.
     *
     * @author      Doan.Chuong
     * @date        05 March 2014
     * @param       ent EntTblFactoryProductAttributeString
     * @return      int
     ************************************************************************/
    int insertFactoryProductAttributeString(EntTblFactoryProductAttributeString ent);

    /**
     * deleteFactoryProductAttributeFlagByProductIds.
     * @param deletingProductsStr store products id with format: 1,2,3,4,5
     * @author      Doan.Chuong
     * @date        2014/03/05
     * @return      number error records.
     */
    int deleteFactoryProductAttributeFlagByProductIds(@Param("deletingProductsStr")String deletingProductsStr);

    /**
     * deleteFactoryProductAttributeFloatByProductIds.
     * @param deletingProductsStr store products id with format: 1,2,3,4,5
     * @author      Doan.Chuong
     * @date        2014/03/05
     * @return      number error records.
     */
    int deleteFactoryProductAttributeFloatByProductIds(@Param("deletingProductsStr")String deletingProductsStr);

    /**
     * deleteFactoryProductAttributeGroupByProductIds.
     * @param deletingProductsStr store products id with format: 1,2,3,4,5
     * @author      Doan.Chuong
     * @date        2014/03/05
     * @return      number error records.
     */
    int deleteFactoryProductAttributeGroupByProductIds(@Param("deletingProductsStr")String deletingProductsStr);

    /**
     * deleteFactoryProductAttributeIntegerByProductIds.
     * @param deletingProductsStr store products id with format: 1,2,3,4,5
     * @author      Doan.Chuong
     * @date        2014/03/05
     * @return      number error records.
     */
    int deleteFactoryProductAttributeIntegerByProductIds(@Param("deletingProductsStr")String deletingProductsStr);

    /**
     * deleteFactoryProductAttributeStringByProductIds.
     * @param deletingProductsStr store products id with format: 1,2,3,4,5
     * @author      Doan.Chuong
     * @date        2014/03/05
     * @return      number error records.
     */
    int deleteFactoryProductAttributeStringByProductIds(@Param("deletingProductsStr")String deletingProductsStr);

    /**
     * insertMultipleAttributeFlag.
     * @param       list info
     * @param       loginUserId loginUserId
     * @author      Doan.Chuong
     * @date        2014/03/05
     * @return      number error records.
     */
    int insertMultipleAttributeFlag(@Param("list")List<EntTblTmpCSV> list, @Param("loginUserId")String loginUserId);

    /**
     * insertMultipleAttributeFloat.
     * @param       list info
     * @param       loginUserId loginUserId
     * @author      Doan.Chuong
     * @date        2014/03/05
     * @return      number error records.
     */
    int insertMultipleAttributeFloat(@Param("list")List<EntTblTmpCSV> list, @Param("loginUserId")String loginUserId);

    /**
     * insertMultipleAttributeGroup.
     * @param       list info
     * @param       loginUserId loginUserId
     * @author      Doan.Chuong
     * @date        2014/03/05
     * @return      number error records.
     */
    int insertMultipleAttributeGroup(@Param("list")List<EntTblTmpCSV> list, @Param("loginUserId")String loginUserId);

    /**
     * insertMultipleAttributeInteger.
     * @param       list info
     * @param       loginUserId loginUserId
     * @author      Doan.Chuong
     * @date        2014/03/05
     * @return      number error records.
     */
    int insertMultipleAttributeInteger(@Param("list")List<EntTblTmpCSV> list, @Param("loginUserId")String loginUserId);

    /**
     * insertMultipleAttributeString.
     * @param       list info
     * @param       loginUserId loginUserId
     * @author      Doan.Chuong
     * @date        2014/03/05
     * @return      number error records.
     */
    int insertMultipleAttributeString(@Param("list")List<EntTblTmpCSV> list, @Param("loginUserId")String loginUserId);

    /**
     * selectAttributeFlagBySyouhinSysCodeList.
     * @param       syouhinSysCodeList syouhinSysCodeList
     * @author      Doan.Chuong
     * @date        2014/03/12
     * @return      List<EntTblTmpCSV>
     */
    List<EntTblTmpCSV> selectAttributeFlagBySyouhinSysCodeList(List<String> syouhinSysCodeList);

    /**
     * selectAttributeFloatBySyouhinSysCodeList.
     * @param       syouhinSysCodeList syouhinSysCodeList
     * @author      Doan.Chuong
     * @date        2014/03/12
     * @return      List<EntTblTmpCSV>
     */
    List<EntTblTmpCSV> selectAttributeFloatBySyouhinSysCodeList(List<String> syouhinSysCodeList);

    /**
     * selectAttributeGroupBySyouhinSysCodeList.
     * @param       syouhinSysCodeList syouhinSysCodeList
     * @author      Doan.Chuong
     * @date        2014/03/12
     * @return      List<EntTblTmpCSV>
     */
    List<EntTblTmpCSV> selectAttributeGroupBySyouhinSysCodeList(List<String> syouhinSysCodeList);

    /**
     * selectAttributeIntegerBySyouhinSysCodeList.
     * @param       syouhinSysCodeList syouhinSysCodeList
     * @author      Doan.Chuong
     * @date        2014/03/12
     * @return      List<EntTblTmpCSV>
     */
    List<EntTblTmpCSV> selectAttributeIntegerBySyouhinSysCodeList(List<String> syouhinSysCodeList);

    /**
     * selectAttributeStringBySyouhinSysCodeList.
     * @param       syouhinSysCodeList syouhinSysCodeList
     * @author      Doan.Chuong
     * @date        2014/03/12
     * @return      List<EntTblTmpCSV>
     */
    List<EntTblTmpCSV> selectAttributeStringBySyouhinSysCodeList(List<String> syouhinSysCodeList);
}
