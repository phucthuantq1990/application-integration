<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN"
"http://mybatis.org/dtd/mybatis-3-mapper.dtd">

<mapper namespace="net.webike.RcProductFactory.mapper.TblFactoryProductGeneralMapper">
	<resultMap id="productFactoryProductGeneralResultMap" type="net.webike.RcProductFactory.entity.EntTblFactoryProductGeneral">
          <result column="product_id"            			property="productId"/>
          <result column="product_supplier_release_date"  	property="productSupplierReleaseDate"/>
          <result column="product_supplier_status_code"  	property="productSupplierStatusCode"/>
          <result column="product_group_code"               property="productGroupCode"/>
          <result column="product_matter_no"                property="productMatterNo"/>
          <result column="product_del_flg"                  property="productDelFlg"/>
          <result column="product_brand_code"  				property="productBrandCode"/>
          <result column="product_webike_code_flg"  		property="productWebikeCodeFlg"/>
          <result column="product_name"  					property="productName"/>
          <result column="product_code"  					property="productCode"/>
          <result column="product_ean_code"  				property="productEanCode"/>
          <result column="product_group_name"  				property="productGroupname"/>
          <result column="product_category_code"  			property="productCategoryCode"/>
          <result column="description_remarks"  			property="descriptionRemarks"/>
          <result column="category_name"                    property="productCategoryName"/>
          <result column="product_syouhin_sys_code"         property="productSyouhinSysCode"/>
          <result column="supplier_price_price"             property="supplierPricePrice"/>
          <result column="product_proper_price"             property="productProperPrice"/>
    </resultMap>
	<select id="selectProductFactoryProductGeneralByProductid" parameterType="java.lang.Long" resultMap="productFactoryProductGeneralResultMap">
		SELECT
		  pg.product_id
		  ,pg.product_supplier_release_date
		  ,pg.product_supplier_status_code
		  ,pg.product_brand_code
		  ,pg.product_webike_code_flg
		  ,pg.product_name
		  ,pg.product_code
		  ,pg.product_group_code
		  ,pg.product_ean_code
		  ,tfpd.description_summary AS productDescriptionSummary
		  ,tfpd.description_caution AS productDescriptionCaution
		  ,mb.name AS productBrandName
		  ,tfpc.product_open_price_flg	AS productOpenPriceFlg
		  ,tfpc.product_proper_selling_flg AS productProperSellingFlg
		  ,tfpc.product_order_product_flg	AS productOrderProductFlg
		  ,tfpc.product_no_returnable_flg	AS productNoReturnableFlg
		  ,tfpc.product_ambiguous_image_flg AS productAmbiguousImageFlg
		  <!-- BOE by Luong.Dai 2014/03/25 add properties -->
		  ,pg.product_group_name
		  ,pg.product_del_flg
		  ,pg.product_category_code
		  ,tfpd.description_remarks
		  <!-- EOE by Luong.Dai 2014/03/25 add properties -->
		  <!-- BOE by Tran.Thanh 2014/06/13 add category name -->
		  , mc.category_name
		  , fp.product_syouhin_sys_code
		  , sp.supplier_price_price
		  , pg.product_proper_price
		  <!-- EOE by Tran.Thanh 2014/06/13 add category name -->
      FROM
		  rc_product_factory.tbl_factory_product_general AS pg
		  	<!-- BOE by Luong.Dai 2014/03/07 change JOIN to LEFT JOIN -->
      		LEFT JOIN rc_syouhin.mst_brand AS mb ON pg.product_brand_code = mb.brand_code
      		LEFT JOIN rc_product_factory.tbl_factory_product_condition AS tfpc ON pg.product_id = tfpc.product_id
      		LEFT JOIN rc_product_factory.tbl_factory_product_description AS tfpd ON pg.product_id = tfpd.product_id
		  	<!-- EOE by Luong.Dai 2014/03/07 change JOIN to LEFT JOIN -->
		  	<!-- BOE by Tran.Thanh 2014/06/13 add category name -->
		  	LEFT JOIN rc_syouhin.mst_category AS mc ON pg.product_category_code = mc.category_code
		  	LEFT JOIN rc_product_factory.tbl_factory_product AS fp ON pg.product_id = fp.product_id
		  	LEFT JOIN rc_product_factory.tbl_factory_product_supplier_price AS sp ON pg.product_id = sp.product_id
		  	<!-- EOE by Tran.Thanh 2014/06/13 add category name -->
	  WHERE
		  pg.product_id = #{productId}
	</select>
	<!-- BOE of Le.Dinh for productEdit at 02/12/2014 -->
	<update id="updateProductFactoryProductGeneral" parameterType="net.webike.RcProductFactory.entity.EntTblFactoryProductGeneral">
		UPDATE rc_product_factory.tbl_factory_product_general
		SET
		   product_brand_code 				= #{productBrandCode}
		  ,product_category_code 			= #{productCategoryCode}
		  ,product_name 					= #{productName}
		  <!-- BOE By Luong.Dai 2014/03/07 check null data
		  ,product_code 					= #{productCode}
		  ,product_webike_code_flg 			= #{productWebikeCodeFlg}
		  ,product_proper_price 			= #{productProperPrice}
		  ,product_ean_code 				= #{productEanCode}
		  EOE By Luong.Dai -->
		  <!-- BOE By Luong.Dai 2014/03/07 -->
		  <if test="null != productCode and '' != productCode">
		  		,product_code 					= #{productCode}
		  </if>
		  ,product_webike_code_flg 			= #{productWebikeCodeFlg}
		  <if test="null != productProperPrice and -1 != productProperPrice">
		  		,product_proper_price 			= #{productProperPrice}
		  </if>
		  <if test="null != productEanCode and '' != productEanCode">
		  		,product_ean_code 				= #{productEanCode}
		  </if>
		  <!-- EOE By Luong.Dai -->
		  ,product_group_code 				= #{productGroupCode}
		  ,product_supplier_release_date 	= #{productSupplierReleaseDate}
		  ,product_supplier_status_code 	= #{productSupplierStatusCode}
		  ,product_del_flg 					= #{productDelFlg}
		  ,updated_on 						= NOW()
		  ,updated_user_id 					= #{updatedUserId}
		WHERE product_id 					= #{productId}
	</update>
	<!-- EOE of Le.Dinh for productEdit at 02/12/2014 -->

	<!-- BOE of Luong.Dai for productEdit at 02/12/2014 -->
	<!--
        @description: Select category_code of product by productId.
        @author: Luong Dai
        @since: 2014/02/12
        -->
    <select id="selectCategoryCodeByProductId" parameterType="java.lang.String" resultType="java.lang.Integer">
        SELECT
            product_gen.product_category_code
        FROM
            rc_product_factory.tbl_factory_product_general AS product_gen
        WHERE
            product_gen.product_id = #{productId};
    </select>
    <!--
        @description: Select product general value by productID.
        @author: Luong Dai
        @since: 2014/02/12
        -->
    <select id="selectProductGeneralValueByProductId" parameterType="java.lang.String" resultMap="productFactoryProductGeneralResultMap">
        SELECT
			    product.product_id                          AS product_id
			    , product_general.product_brand_code        AS product_brand_code
			    , product_general.product_group_code        AS product_group_code
			    , product.product_matter_no                 AS product_matter_no
			    , product_general.product_group_code        AS product_group_code
		FROM
		    rc_product_factory.tbl_factory_product AS product
		    JOIN rc_product_factory.tbl_factory_product_general AS product_general
		        ON product.product_id = product_general.product_id
		WHERE product.product_id = #{productId};
    </select>

    <!-- Update product_code, product_proper_price and product_ean_code -->
    <update id="updateProductCodeProperPriceAndEANCodeByProductId"
        parameterType="net.webike.RcProductFactory.entity.EntTblFactoryProductNew">
        UPDATE
            rc_product_factory.tbl_factory_product_general
        SET
        	updated_on                      = NOW()
            , updated_user_id                 = #{updatedUserId}
            <if test="null != productCode">
            	, product_code                      = #{productCode}
            </if>
            , product_proper_price            = #{productProperPrice}
           	<if test="null != productEanCode">
           		, product_ean_code                = #{productEanCode}
           	</if>
        WHERE
            product_id                        = #{productId}
    </update>
    <!-- End of Update product_code, product_proper_price and product_ean_code -->
    <!-- Insert product general of group product -->
    <insert id="insertNewFactoryProductGeneralOfGroupProduct"
    	parameterType="net.webike.RcProductFactory.entity.EntTblFactoryProductGeneral">
      INSERT INTO
      	rc_product_factory.tbl_factory_product_general
       	(
       	product_id
        , product_brand_code
        , product_code
        , product_proper_price
        , product_ean_code
        , product_group_code
        , updated_on
        , updated_user_id
       )
       VALUES (
         #{productId}
		,#{productBrandCode}
		,#{productCode}
		,#{productProperPrice}
		,#{productEanCode}
		,#{productGroupCode}
		,NOW()
		,#{updatedUserId}
       )
    </insert>
    <!-- End of Insert product general of group product -->

	<!-- Select updatedOn of productGeneral by productId -->
	<select id="selectUpdatedOnOfProductByProductId" parameterType="java.lang.Long" resultType="java.util.Date">
	SELECT
		updated_on
	FROM
		rc_product_factory.tbl_factory_product_general
	WHERE
		product_id = #{productId};
	</select>
	<!-- End of Select updatedOn of productGeneral by productId -->
    <!-- BOE of Luong.Dai for productEdit at 02/12/2014 -->

    <!-- BOE 2014/02/25 Long Vu -->
    <insert id="insertNewFactoryProductGeneral" parameterType="net.webike.RcProductFactory.entity.EntMstProduct">
      INSERT INTO rc_product_factory.tbl_factory_product_general
       (  product_id
        , product_brand_code
        , product_category_code
        , product_name
        , product_code
        , product_webike_code_flg
        , product_proper_price
        , product_ean_code
        , product_group_code
        , product_group_name
        , product_supplier_release_date
        , product_supplier_status_code
        , product_del_flg
        , updated_on
        , updated_user_id
       )
       SELECT
          #{productId}
        , mst.product_brand_code
        , mst.product_category_code
        , mst.product_name
        , mst.product_code
        , mst.product_webike_code_flg
        , mst.product_proper_price
        , mst.product_ean_code
        , mst.product_group_code
        , mst.product_group_name
        , mst.product_supplier_release_date
        , mst.product_supplier_status_code
        , mst.product_del_flg
        , NOW()
        , #{updatedUserId}
       FROM rc_syouhin.mst_product AS mst
       WHERE mst.syouhin_sys_code = #{syouhinSysCode}
    </insert>

    <insert id="insertMultipleNewFactoryProductGeneral" parameterType="map">
      INSERT INTO rc_product_factory.tbl_factory_product_general
       (  product_id
        , product_brand_code
        , product_category_code
        , product_name
        , product_code
        , product_webike_code_flg
        , product_proper_price
        , product_ean_code
        , product_group_code
        , product_group_name
        , product_supplier_release_date
        , product_supplier_status_code
        , product_del_flg
        , updated_on
        , updated_user_id
       )
       SELECT
        @productId:=@productId+1 AS product_id
        , mst1.*
        FROM
        (SELECT
          mst.product_brand_code
        , mst.product_category_code
        , mst.product_name
        , mst.product_code
        , mst.product_webike_code_flg
        , mst.product_proper_price
        , mst.product_ean_code
        , mst.product_group_code
        , mst.product_group_name
        , mst.product_supplier_release_date
        , mst.product_supplier_status_code
        , mst.product_del_flg
        , NOW() AS updated_on
        , #{updatedUserId} AS updated_user_id
       FROM rc_syouhin.mst_product AS mst
       WHERE mst.syouhin_sys_code IN
       <foreach item="item" index="index" collection="list" open="(" separator="," close=")">
          #{item}
        </foreach>
       ) mst1, (SELECT @productId:=#{productId}) tmp
    </insert>

    <!-- EOE 2014/03/23 Le.Dinh -->
    <insert id="insertNewFactoryProductGeneralByProductGeneral" parameterType="net.webike.RcProductFactory.entity.EntTblFactoryProductGeneral">
      INSERT INTO rc_product_factory.tbl_factory_product_general
       (  product_id
        , product_brand_code
        , product_category_code
        , product_name
        , product_code
        , product_webike_code_flg
        , product_proper_price
        , product_ean_code
        , product_group_code
        , product_group_name
        , product_supplier_release_date
        , product_supplier_status_code
        , product_del_flg
        , updated_on
        , updated_user_id
       )
       VALUES (
         #{productId}
		,#{productBrandCode}
		,#{productCategoryCode}
		,#{productName}
		,#{productCode}
		,#{productWebikeCodeFlg}
		,#{productProperPrice}
		,#{productEanCode}
		,#{productGroupCode}
		,#{productGroupname}
		,#{productSupplierReleaseDate}
		,#{productSupplierStatusCode}
		,#{productDelFlg}
		,NOW()
		,#{updatedUserId}
       )
    </insert>
    <!-- EOE 2014/03/23 Le.Dinh -->

    <!-- BOE 2014/02/25 Tran Thanh -->
    <update id="updateProductGeneralByProductId" parameterType="java.util.Map">
        UPDATE  rc_product_factory.tbl_factory_product_general as general
        SET     product_name = #{productName}
                , updated_on = NOW()
                , updated_user_id = #{updatedUserId}
        WHERE   general.product_id = #{productId};
    </update>
    <!-- EOE 2014/02/25 Tran Thanh -->

    <!-- BOE 2014/03/20 by Luong.Dai update productGroupCode -->
    <update id="updateProductGroupCodeByProductId" parameterType="net.webike.RcProductFactory.entity.EntTblFactoryProductGeneral">
        UPDATE  rc_product_factory.tbl_factory_product_general as general
        SET     product_group_code 		= #{productGroupCode}
                , updated_on 			= NOW()
                , updated_user_id 		= #{updatedUserId}
        WHERE   general.product_id 		= #{productId};
    </update>
    <!-- EOE 2014/03/20 by Luong.Dai update productGroupCode -->

    <!-- BOE #7363 2014/04/22 Tran.Thanh : add query update table  -->
    <update id="updateProductGeneralPopupBlock15" parameterType="map">
        UPDATE  rc_product_factory.tbl_factory_product_general as general
        SET
            updated_on = NOW()
          , updated_user_id = #{entTblFactoryProductGeneral.updatedUserId}

            <!-- Start ProductName -->
            <if test="null != entTblFactoryProductGeneral.productName and '' != entTblFactoryProductGeneral.productName">
                <if test="null != entTblFactoryProductGeneral.replaceModeProductName
                            and entTblFactoryProductGeneral.replaceModeProductName == 0">
                    ,product_name = #{entTblFactoryProductGeneral.productName}
	            </if>
                <if test="null != entTblFactoryProductGeneral.replaceModeProductName
                            and entTblFactoryProductGeneral.replaceModeProductName == 1">
                    ,product_name = CONCAT(#{entTblFactoryProductGeneral.productName}, product_name)
                </if>
                <if test="null != entTblFactoryProductGeneral.replaceModeProductName
                            and entTblFactoryProductGeneral.replaceModeProductName == 2">
                    ,product_name = CONCAT(product_name, #{entTblFactoryProductGeneral.productName})
                </if>
                <if test="null != entTblFactoryProductGeneral.replaceModeProductName
                            and entTblFactoryProductGeneral.replaceModeProductName == 3">
                    ,product_name = replace(product_name, #{entTblFactoryProductGeneral.productNameFind}, #{entTblFactoryProductGeneral.productName})
                </if>
            </if>
            <!-- End ProductName -->

            <!-- Start ProductCategoryCode -->
            <if test="null != entTblFactoryProductGeneral.productCategoryCode">
                ,product_category_code = #{entTblFactoryProductGeneral.productCategoryCode}
            </if>
            <!-- End ProductCategoryCode -->

            <!-- Start ProductBrandCode -->
            <if test="null != entTblFactoryProductGeneral.productBrandCode">
                ,product_brand_code = #{entTblFactoryProductGeneral.productBrandCode}
            </if>
            <!-- End ProductBrandCode -->

            <!-- Start ProductEanCode -->
            <if test="null != entTblFactoryProductGeneral.productEanCode and '' != entTblFactoryProductGeneral.productEanCode">
                <if test="null != entTblFactoryProductGeneral.replaceModeProductEanCode
                            and entTblFactoryProductGeneral.replaceModeProductEanCode == 0">
                    ,product_ean_code = #{entTblFactoryProductGeneral.productEanCode}
                </if>
                <if test="null != entTblFactoryProductGeneral.replaceModeProductEanCode
                            and entTblFactoryProductGeneral.replaceModeProductEanCode == 1">
                    ,product_ean_code = CONCAT(#{entTblFactoryProductGeneral.productEanCode}, product_ean_code)
                </if>
                <if test="null != entTblFactoryProductGeneral.replaceModeProductEanCode
                            and entTblFactoryProductGeneral.replaceModeProductEanCode == 2">
                    ,product_ean_code = CONCAT(product_ean_code, #{entTblFactoryProductGeneral.productEanCode})
                </if>
                <if test="null != entTblFactoryProductGeneral.replaceModeProductEanCode
                            and entTblFactoryProductGeneral.replaceModeProductEanCode == 3">
                    ,product_ean_code = replace(product_ean_code, #{entTblFactoryProductGeneral.productEanCodeFind}, #{entTblFactoryProductGeneral.productEanCode})
                </if>
            </if>
            <!-- End ProductEanCode -->

            <!-- Start ProductProperPrice -->
            <if test="null != entTblFactoryProductGeneral.productProperPriceStr">
                <if test="null != entTblFactoryProductGeneral.replaceModeProductProperPrice
                            and entTblFactoryProductGeneral.replaceModeProductProperPrice == 0">
                    ,product_proper_price = #{entTblFactoryProductGeneral.productProperPriceStr}
                </if>
                <if test="null != entTblFactoryProductGeneral.replaceModeProductProperPrice
                            and entTblFactoryProductGeneral.replaceModeProductProperPrice == 1">
                    ,product_proper_price = CONCAT(#{entTblFactoryProductGeneral.productProperPriceStr}, product_proper_price)
                </if>
                <if test="null != entTblFactoryProductGeneral.replaceModeProductProperPrice
                            and entTblFactoryProductGeneral.replaceModeProductProperPrice == 2">
                    ,product_proper_price = CONCAT(product_proper_price, #{entTblFactoryProductGeneral.productProperPriceStr})
                </if>
                <if test="null != entTblFactoryProductGeneral.replaceModeProductProperPrice
                            and entTblFactoryProductGeneral.replaceModeProductProperPrice == 3">
                    ,product_proper_price = replace(product_proper_price,
                                                    #{entTblFactoryProductGeneral.productProperPriceFind},
                                                    #{entTblFactoryProductGeneral.productProperPriceStr})
                </if>
            </if>
            <!-- End ProductProperPrice -->

            <!-- Start productWebikeCodeFlg -->
            <if test="null != entTblFactoryProductGeneral.productWebikeCodeFlg and entTblFactoryProductGeneral.productWebikeCodeFlg == 1">
                    ,product_webike_code_flg = 1
                    <!-- BOE #7363 Tran.Thanh : if product_code is exits, not update -->
<!--                     ,product_code = CONCAT("W-",product_brand_code,"-P",product_id) -->
                    ,product_code = IF (product_code IS NULL or product_code = '', CONCAT("W-",product_brand_code,"-P",product_id), product_code)
                    <!-- BOE #7363 Tran.Thanh : if product_code is exits, not update -->
            </if>
            <if test="null != entTblFactoryProductGeneral.productWebikeCodeFlg
                        and entTblFactoryProductGeneral.productWebikeCodeFlg == 0">
                    ,product_webike_code_flg = 0
                    <if test="null != entTblFactoryProductGeneral.replaceModeProductCode
	                            and entTblFactoryProductGeneral.replaceModeProductCode == 0">
	                    ,product_code  = #{entTblFactoryProductGeneral.productCode}
	                </if>
	                <if test="null != entTblFactoryProductGeneral.replaceModeProductCode
	                            and entTblFactoryProductGeneral.replaceModeProductCode == 1">
	                    ,product_code  = CONCAT(#{entTblFactoryProductGeneral.productCode}, product_code )
	                </if>
	                <if test="null != entTblFactoryProductGeneral.replaceModeProductCode
	                            and entTblFactoryProductGeneral.replaceModeProductCode == 2">
	                    ,product_code  = CONCAT(product_code , #{entTblFactoryProductGeneral.productCode})
	                </if>
	                <if test="null != entTblFactoryProductGeneral.replaceModeProductCode
	                            and entTblFactoryProductGeneral.replaceModeProductCode == 3">
	                    ,product_code  = replace(product_code ,
	                                                    #{entTblFactoryProductGeneral.productCodeFind},
	                                                    #{entTblFactoryProductGeneral.productCode})
	                </if>
            </if>
            <!-- End productWebikeCodeFlg -->

            <!-- Start ProductSupplierReleaseDate -->
            <if test="null != entTblFactoryProductGeneral.productSupplierReleaseDate
                    and '' != entTblFactoryProductGeneral.productSupplierReleaseDate">
                <if test="null != entTblFactoryProductGeneral.replaceModeProductReleaseDate
                            and entTblFactoryProductGeneral.replaceModeProductReleaseDate == 0">
                    ,product_supplier_release_date = #{entTblFactoryProductGeneral.productSupplierReleaseDate}
                </if>
                <if test="null != entTblFactoryProductGeneral.replaceModeProductReleaseDate
                            and entTblFactoryProductGeneral.replaceModeProductReleaseDate == 1">
                    ,product_supplier_release_date = CONCAT(#{entTblFactoryProductGeneral.productSupplierReleaseDate}, product_supplier_release_date)
                </if>
                <if test="null != entTblFactoryProductGeneral.replaceModeProductReleaseDate
                            and entTblFactoryProductGeneral.replaceModeProductReleaseDate == 2">
                    ,product_supplier_release_date = CONCAT(product_supplier_release_date, #{entTblFactoryProductGeneral.productSupplierReleaseDate})
                </if>
                <if test="null != entTblFactoryProductGeneral.replaceModeProductReleaseDate
                            and entTblFactoryProductGeneral.replaceModeProductReleaseDate == 3">
                    ,product_supplier_release_date = replace(product_supplier_release_date,
                                                    #{entTblFactoryProductGeneral.productSupplierReleaseDateFind},
                                                    #{entTblFactoryProductGeneral.productSupplierReleaseDate})
                </if>
            </if>
            <!-- End ProductSupplierReleaseDate -->
        WHERE
            general.product_id IN
            <foreach item="item" index="index" collection="list" open="(" separator="," close=")">
              #{item}
            </foreach>
    </update>
    <!-- EOE 2014/02/25 Tran Thanh -->
    <!-- BOE #7208 Luong.Dai Delete product general at 2014/05/12 -->
    <delete id="deleteProductGeneralByListProductId" parameterType="java.lang.Long">
	DELETE FROM rc_product_factory.tbl_factory_product_general
	WHERE product_id IN
		<foreach item="item" index="index" collection="list" open="(" separator="," close=")">
			#{item}
		</foreach>
    </delete>
    <!-- BOE #7208 Luong.Dai Delete product general at 2014/05/12 -->
    <!-- BOE #7206 Luong.Dai add list product general -->
    <insert id="insertListNewFactoryProductGeneralByProductGeneral" parameterType="net.webike.RcProductFactory.entity.EntTblFactoryProductGeneral">
      	INSERT INTO rc_product_factory.tbl_factory_product_general
      	(
      		product_id
       		, product_brand_code
        	, product_category_code
        	, product_name
        	, product_code
        	, product_webike_code_flg
        	, product_proper_price
        	, product_ean_code
        	, product_group_code
        	, product_group_name
        	, product_supplier_release_date
        	, product_supplier_status_code
        	, product_del_flg
        	, updated_on
        	, updated_user_id
       )
       VALUES
       <foreach item="item" collection="lstProductGeneral" separator=",">
       (
         	#{item.productId}
			, #{item.productBrandCode}
			, #{item.productCategoryCode}
			, #{item.productName}
			, #{item.productCode}
			, #{item.productWebikeCodeFlg}
			, #{item.productProperPrice}
			, #{item.productEanCode}
			, #{item.productGroupCode}
			, #{item.productGroupname}
			, #{item.productSupplierReleaseDate}
			, #{item.productSupplierStatusCode}
			, #{item.productDelFlg}
			, NOW()
			, #{item.updatedUserId}
       )
       </foreach>
    </insert>
    <!-- EOE #7206 Luong.Dai add list product general -->
    <select id="selectListGeneralByMatterNo"
            parameterType="java.util.Map"
            resultMap="productFactoryProductGeneralResultMap">
        SELECT
            product_general.product_group_code
            , product_general.product_brand_code
        FROM rc_product_factory.tbl_factory_product AS product
        LEFT JOIN rc_product_factory.tbl_factory_product_general  AS product_general
          ON product.product_id = product_general.product_id
        WHERE
            product.product_matter_no = #{matterNo}
            AND product_general.product_group_code > 0
        GROUP BY
             product_general.product_group_code
             , product_general.product_brand_code
        ORDER BY product_general.product_id ASC;
    </select>
	<!-- BOE #7203 Luong.Dai 2014/06/16 Select prodcut proper price and supplier price of product -->
    <select id="selectProductPriceByProductId"
    	parameterType="java.lang.String"
    	resultMap="productFactoryProductGeneralResultMap">
        SELECT
			pg.product_id					AS product_id
			, sp.supplier_price_price		AS supplier_price_price
			, pg.product_proper_price		AS product_proper_price
			, pg.product_category_code		AS product_category_code
		FROM
			rc_product_factory.tbl_factory_product_general 					AS pg
			LEFT JOIN rc_product_factory.tbl_factory_product				AS fp
				ON pg.product_id = fp.product_id
			LEFT JOIN rc_product_factory.tbl_factory_product_supplier_price	AS sp
				ON pg.product_id = sp.product_id
		WHERE
			pg.product_id = #{productId}
    </select>
    <!-- EOE #7203 Luong.Dai 2014/06/16 Select prodcut proper price and supplier price of product -->
</mapper>
