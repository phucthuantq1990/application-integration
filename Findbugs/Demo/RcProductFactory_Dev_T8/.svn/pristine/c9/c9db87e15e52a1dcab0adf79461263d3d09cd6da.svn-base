/************************************************************************
 * File Name    ： EntMstAttributeGroup.java
 * Author       ： Long Vu
 * Version      ： 1.0.0
 * Date Created ： 2014/01/15
 * Date Updated ： 2014/01/15
 * Description  ： Entity of MstAttributeGroup.
 ***********************************************************************/
package net.webike.RcProductFactory.entity;

import java.util.Date;

/**
 * Entity of MstAttributeGroup.
 * @author Long Vu
 * Date Created ： 2014/01/15
 */
public class EntMstAttributeGroup {
    private Integer attributeGroupCode;
    private Integer attributeCode;
    private String attributeGroupName;
    private Integer attributeSort;
    private Date createdOn;
    private String createdUserId;
    private Date updatedOn;
    private String updatedUserId;
    private int delFlg;
    private String loginUserId;
    /**
     * @return the attributeGroupCode
     */
    public Integer getAttributeGroupCode() {
        return attributeGroupCode;
    }
    /**
     * @param attributeGroupCode the attributeGroupCode to set
     */
    public void setAttributeGroupCode(Integer attributeGroupCode) {
        this.attributeGroupCode = attributeGroupCode;
    }
    /**
     * @return the attributeCode
     */
    public Integer getAttributeCode() {
        return attributeCode;
    }
    /**
     * @param attributeCode the attributeCode to set
     */
    public void setAttributeCode(Integer attributeCode) {
        this.attributeCode = attributeCode;
    }
    /**
     * @return the attributeGroupName
     */
    public String getAttributeGroupName() {
        return attributeGroupName;
    }
    /**
     * @param attributeGroupName the attributeGroupName to set
     */
    public void setAttributeGroupName(String attributeGroupName) {
        this.attributeGroupName = attributeGroupName;
    }
    /**
     * @return the attributeSort
     */
    public Integer getAttributeSort() {
        return attributeSort;
    }
    /**
     * @param attributeSort the attributeSort to set
     */
    public void setAttributeSort(Integer attributeSort) {
        this.attributeSort = attributeSort;
    }
    /**
     * @return the createdOn
     */
    public Date getCreatedOn() {
        if (createdOn != null) {
            return (Date) createdOn.clone();
        }

        return null;
    }
    /**
     * @param createdOn the createdOn to set
     */
    public void setCreatedOn(Date createdOn) {
        if (createdOn != null) {
            this.createdOn = (Date) createdOn.clone();
        } else {
            this.createdOn = null;
        }
    }
    /**
     * @return the createdUserId
     */
    public String getCreatedUserId() {
        return createdUserId;
    }
    /**
     * @param createdUserId the createdUserId to set
     */
    public void setCreatedUserId(String createdUserId) {
        this.createdUserId = createdUserId;
    }
    /**
     * @return the updatedOn
     */
    public Date getUpdatedOn() {
        if (updatedOn != null) {
            return (Date) updatedOn.clone();
        }

        return null;
    }
    /**
     * @param updatedOn the updatedOn to set
     */
    public void setUpdatedOn(Date updatedOn) {
        if (updatedOn != null) {
            this.updatedOn = (Date) updatedOn.clone();
        } else {
            this.updatedOn = null;
        }
    }
    /**
     * @return the updatedUserId
     */
    public String getUpdatedUserId() {
        return updatedUserId;
    }
    /**
     * @param updatedUserId the updatedUserId to set
     */
    public void setUpdatedUserId(String updatedUserId) {
        this.updatedUserId = updatedUserId;
    }
    /**
     * @return the delFlg
     */
    public int getDelFlg() {
        return delFlg;
    }
    /**
     * @param delFlg the delFlg to set
     */
    public void setDelFlg(int delFlg) {
        this.delFlg = delFlg;
    }
    /**
     * @return the loginUserId
     */
    public String getLoginUserId() {
        return loginUserId;
    }
    /**
     * @param loginUserId the loginUserId to set
     */
    public void setLoginUserId(String loginUserId) {
        this.loginUserId = loginUserId;
    }
}
