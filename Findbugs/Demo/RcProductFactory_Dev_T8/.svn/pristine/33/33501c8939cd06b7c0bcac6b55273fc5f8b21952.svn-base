/************************************************************************
 * File Name    �F Utils.java
 * Author       �F Nguyen.Chuong
 * Version      �F 1.0.0
 * Date Created �F 2013/10/24
 * Date Updated �F 2013/10/24
 * Description  �F Common function of productFactory.
 *
 ***********************************************************************/
package net.webike.RcProductFactory.util;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.imageio.ImageIO;
import javax.net.ssl.HttpsURLConnection;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import net.webike.RcProductFactory.entity.EntTblFactorySetting;
import net.webike.RcProductFactory.entity.ImageUploadKenDo;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.w3c.tidy.Tidy;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.opensymphony.xwork2.ActionSupport;
import com.taskadapter.redmineapi.RedmineException;
import com.taskadapter.redmineapi.bean.User;

/**
 *  Common function of productFactory.
 */
public final class Utils {
	// get INSTANCE class
	private static final Utils INSTANCE = new Utils();
	public static Utils getInstance() {
		return INSTANCE;
	}
	private static final String CURRENT_CLASS = "Utils";
	private final Logger logger = Logger.getLogger(this.getClass());
	private static final int VALIDSIZE_THUMB = 140;
	private static final String CONTENTHEADER = "HEAD";
    /**---------------------------------------------------------------------------
     * private constructor.
     */
    private Utils() { };

    /************************************************************************
     * <b>Description:</b><br>
     *  get text from properties file for java file.
     *
     * @author      Nguyen.Chuong
     * @date        2013/10/02
     * @param       key String
     * @return      String
     ************************************************************************/
    public static String getProp(String key) {
        ActionSupport as = new ActionSupport();
        String ret = as.getText(key);
        if (StringUtils.isEmpty(ret)) {
            ret = "";
        }
        return ret;
    }

    /************************************************************************
     * <b>Description:</b><br>
     *  Convert date default format to String format:yyyy/MM/dd .
     *
     * @author      Tran.Thanh
     * @date        24 Sep 2013
     * @param       date Date input
     * @return      String
     ************************************************************************/
    public static String convertDateToString(Date date) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
        if (date != null) {
            String dateString = formatter.format(date);
            return dateString;
        }
        return "";
    }
    /************************************************************************
     * <b>Description:</b><br>
     *  getUpdatedOnToString yyyy/MM/dd H:m:s.
     *
     * @author		Le.Dinh
     * @date		2014/01/23
     * @param 		date	Date
     * @return		String
     ************************************************************************/
    public static String formatDateFull(Date date) {
    	 SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd H:m:s");
        if (date != null) {
            String dateString = formatter.format(date);
            return dateString;
        }
        return "";
    }

    /************************************************************************
     * <b>Description:</b><br>
     *  Format date by partern.
     *      ex: partern : yyyyMMddHHmmss => 20140227111057
     *
     * @author      hoang.ho
     * @since       2014/02/27
     * @param       date    Date
     * @param       formatPartern format partern
     * @return      String
     ************************************************************************/
    public static String formatDate(Date date, String formatPartern) {
         SimpleDateFormat formatter = new SimpleDateFormat(formatPartern);
        if (date != null) {
            String dateString = formatter.format(date);
            return dateString;
        }
        return "";
    }

    /************************************************************************
     * <b>Description:</b><br>
     *  convert String to Md5.
     *
     * @author      Tran.Thanh
     * @date        7 Oct 2013
     * @param       data String
     * @return      String(32)
     * @throws NoSuchAlgorithmException try catch.
     ************************************************************************/
    public static String convertStringToMd5(String data) throws NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance("MD5");
        md.update(data.getBytes(Charset.forName("UTF-8")));

        byte[] byteData = md.digest();

        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < byteData.length; i++) {
            sb.append(Integer.toString((byteData[i] & Constant.OXFF) + Constant.OX100, Constant.HEX).substring(1));
        }
        return sb.toString();
    }

    /************************************************************************
     * <b>Description:</b><br>
     *  Format String:
     *  - Trim to remove first and last space of string
     *  - Remove all 2 adjacent space inner.
     *
     * @author		Nguyen.Chuong
     * @date		Nov 19, 2013
     * @param       value String to format.
     * @return		formated String
     ************************************************************************/
    public static String standardizedString(String value) {
        String result = value;
        if (StringUtils.isNotEmpty(value)) {
            //Trim space in first and last.
            result = result.trim();
            //Replace 2 space innner.
            result = result.replaceAll("\\s+", " ");
        }
        return result;
    }

	/************************************************************************
	 * <b>Description:</b><br>
	 *  checkRoleOfCurrentuser.
	 *
	 * @author		Le.Dinh
	 * @date		2014/01/13
	 * @param 		listRoleRedmine	List<Integer>
	 * @param 		lisEntTblFactorySettings	List<EntTblFactorySetting>
	 * @return		listRoleSpring
	 ************************************************************************/
	public static List<String> checkRoleOfCurrentuser(List<String> listRoleRedmine, List<EntTblFactorySetting> lisEntTblFactorySettings) {
		List<String> listRoleSpring  = new ArrayList<String>();
		for (EntTblFactorySetting entTblFactorySetting : lisEntTblFactorySettings) {
			if (listRoleRedmine.contains(entTblFactorySetting.getSettingValue())) {
				listRoleSpring.add(entTblFactorySetting.getSettingCode());
			}
		}
		return listRoleSpring;
	}
    /************************************************************************
     * <b>Description:</b><br>
     *  selectRedmineApiKey.
     *
     * @author		Le.Dinh
     * @date		2014/01/14
     * @param 		userId	String
     * @param 		userPasswd	String
     * @param 		fullUrl	String
     * @param 		rootUrl	String
     * @return		String
     * @throws 		IOException	IOException
     ************************************************************************/
    public static String selectRedmineApiKey(String userId, String userPasswd, String fullUrl, String rootUrl) throws IOException {
    	// get API key from redmine API
    	String apiAccesKey = "";
    	User userRedmine = null;
    	try {
			HttpsURLConnection httpsCon = RedmineUtils.getResultHttpsConWithUser(
																userId,
																userPasswd,
																fullUrl,
																rootUrl);

			if (httpsCon.getResponseCode() == Constant.HTTPSCON_RESPONSECODE) {
				userRedmine = RedmineUtils.getJsonUserRedmine(httpsCon);
			}
		} catch (RedmineException e) {
            e.printStackTrace();
		}
    	if (userRedmine != null) {
    		apiAccesKey = userRedmine.getApiKey();
    	}
        return apiAccesKey;
    }

    /************************************************************************
     * <b>Description:</b><br>
     *  validate getRealPath.
     *
     * @author      Le.Dinh
     * @date        2014/01/02
     * @param       request HttpServletRequest
     * @return      String
     ************************************************************************/
    public static String getRealPath(HttpServletRequest request) {
		ServletContext servletContext = request.getSession().getServletContext();
		String filePath = servletContext.getRealPath("/");
    	return filePath;
    }

    /************************************************************************
     * <b>Description:</b><br>
     *  remove Image Not Use In TMP folder.
     *
     * @author      Le.Dinh
     * @date        2014/01/08
     * @param       folder File
     * @param       allFileOfUser String
     * @param       currentFileUser String
     * @return       boolean
     ************************************************************************/
    public boolean removeImageNotUseInTMP(File folder, String allFileOfUser, String currentFileUser) {
    	if (!folder.exists()) {
    		return false;
    	}
		try {
			File[] fList = folder.listFiles();
			if (fList != null && fList.length > 1) {
				for (int i = 0; i < fList.length; i++) {
					File file = fList[i];
					if (file.getName().contains(allFileOfUser)
				    	&& !file.getName().contains(currentFileUser)
				    	) {
				    	FileUtils.forceDelete(file);
				    }
				}
			}
		} catch (IOException e) {
			logger.error(CURRENT_CLASS + " An error when detele list file is not used");
			logger.error(e.toString());
		}
		return true;
    }
    /**
     * Parse Object to Json String.
     * @param obj object will be parse string json.
     * @return String Json
     * @author hoang.ho
     * @since 2014/01/23
     */
    public static String parseObjectToJson(Object obj) {
        if (null == obj) {
            return "";
        }
        Gson gson = new Gson();
        return gson.toJson(obj);
    }

    /**
     * Cut string by length.
     * @param value string need cut
     * @param cutLength length for cut.
     * @return String value after cut.
     * @author      hoang.ho
     * @since       2014/02/19
     */
    public static String cutStringByLength(String value, int cutLength) {
        String result = "";
        // check null or empty
        if (!StringUtils.isEmpty(value)) {
            // cut brand-name at character = 50.
            int length = value.length();
            int subLength = length;
            if (length > cutLength) {
                subLength = cutLength;
            }
            result = value.substring(0, subLength);
        }
        return result;
    }

    /************************************************************************
     * <b>Description:</b><br>
     *  Format normal date to return Japan week type part.
     *  Ex: if date is 2014/02/20 then 2014�N 2��  ���{.
     *  <br>Day: <br>
     *  - Day 1-> 10 : ��{ <br>
     *  - Day 11-> 20 : ���{ <br>
     *  - Day 21-> 31 : ���{
     *
     * @author		Nguyen.Chuong
     * @date		Feb 20, 2014
     * @param       normalDate date
     * @return		formatedDate String
     ************************************************************************/
    public static String formatDateToJapanWeekPartDate(Date normalDate) {
        String formatedDate = "";
        if (null != normalDate) {
            Calendar cal = Calendar.getInstance();
            cal.setTime(normalDate);
            //Get year
            formatedDate += cal.get(Calendar.YEAR) + getProp("year") + " ";

            //Get month
            formatedDate += cal.get(Calendar.MONTH) + 1 + getProp("month") + " ";

            //Get day
            int day = cal.get(Calendar.DAY_OF_MONTH);
            //If day is between 1 and 10 then day is "��{"
            if (day <= Constant.FIRST_WEEK_PART_END_DAY) {
                formatedDate += getProp("firstWeekPart");
                return formatedDate;
            } else if (day <= Constant.MIDDLE_WEEK_PART_END_DAY) {
                //If day is between 11 and 20 then day is "���{"
                formatedDate += getProp("middleWeekPart");
                return formatedDate;
            }
            //If day is between 21 and 31 then day is "���{"
            formatedDate += getProp("lastWeekPart");
        }
        return formatedDate;
    };

	/************************************************************************
	 * <b>Description:</b><br>
	 *  parseJsonStringToList.
	 *
	 * @author		Le.Dinh
	 * @date		24 Feb 2014
	 * @param 		jsonStr	String
	 * @param 		type	Type
	 * @return		List
	 ************************************************************************/
	@SuppressWarnings("rawtypes")
	public static List parseJsonStringToList(String jsonStr, Type type) {
		 Gson gson = new Gson();
		 List target = gson.fromJson(jsonStr, type);
		 return target;
	}

	/************************************************************************
	 * <b>Description:</b><br>
	 *  parseJsonStringToListWithDateFormat.
	 *
	 * @author		Tran.Thanh
	 * @date		25 Feb 2014
	 * @param 		jsonStr jsonStr
	 * @param 		type type
	 * @return		List
	 ************************************************************************/
	@SuppressWarnings("rawtypes")
	public static List parseJsonStringToListWithDateFormat(String jsonStr, Type type) {
		Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss").create();
		 List target = gson.fromJson(jsonStr, type);
		 return target;
	}

	/************************************************************************
	 * <b>Description:</b><br>
	 *  Check and parse String to Long object.
	 *
	 * @author		Nguyen.Chuong
	 * @date		Mar 4, 2014
	 * @param       stringTmp String
	 * @return		Long object
	 ************************************************************************/
	public static Long parseStringToLongObject(String stringTmp) {
	    Long longTmp = null;
	    if (StringUtils.isNotEmpty(stringTmp)) {
	        stringTmp = stringTmp.trim();
            if (StringUtils.isNotEmpty(stringTmp)) {
                longTmp = Long.parseLong(stringTmp);
                return longTmp;
            }
        }
	    return null;
	}

	/************************************************************************
	 * <b>Description:</b><br>
	 *  readImageDescriptionSentence.
	 * @author		Le.Dinh
	 * @date		11 Mar 2014
	 * @param 		request	HttpServletRequest
	 * @param 		folderImage	String
	 * @return		List<ImageUploadKenDo>
	 * @throws 		IOException	IOException
	 ************************************************************************/
	public static List<ImageUploadKenDo> readImageDescriptionSentence(HttpServletRequest request, String folderImage) throws IOException {
		// get real path
		String filePath = Utils.getRealPath(request);
		ImageUploadKenDo imageUpload;
		List<ImageUploadKenDo> listImageUploadKenDo = new ArrayList<ImageUploadKenDo>();

		// check exist folder images/tmp_image
		File folderTmp = new File(filePath + folderImage);
		if (!folderTmp.exists()) {
			FileUtils.forceMkdir(folderTmp);
		}

		File folder = new File(filePath + folderImage);
		File[] fList = folder.listFiles();
		if (fList != null && fList.length > 0) {
			for (int i = 0; i < fList.length; i++) {
				File fileRead = fList[i];

				imageUpload = new ImageUploadKenDo();
				imageUpload.setName(fileRead.getName());
				imageUpload.setType("f");
				imageUpload.setSize(fileRead.length());
				imageUpload.setFilePatch(folderImage + "/" + fileRead.getName());

				listImageUploadKenDo.add(imageUpload);
			}
		}
		return	listImageUploadKenDo;
	}

	/************************************************************************
	 * <b>Description:</b><br>
	 *  upLoadImageDescriptionSentence.
	 * @author		Le.Dinh
	 * @date		11 Mar 2014
	 * @param 		request	HttpServletRequest
	 * @param 		folderImage	String
	 * @param 		fileFileName	fileFileName
	 * @param 		file	File
	 * @param 		maxSize Max size can upload
	 * @return		ImageUploadKenDo
	 * @throws 		IOException	IOException
	 ************************************************************************/
	public static ImageUploadKenDo upLoadImageDescriptionSentence(
													HttpServletRequest request,
													String folderImage,
													String fileFileName,
													File file,
													int maxSize) throws IOException {
		// get real path
		String filePath = Utils.getRealPath(request);
		ImageUploadKenDo imageUpload = new ImageUploadKenDo();

		// Validate maximum size
		long totalForderImageSize = 0;
		File folder = new File(filePath + folderImage);
		//File[] fList = folder.listFiles();
		for (File fileRead : folder.listFiles()) {
	        if (fileRead.isFile()) {
	        	totalForderImageSize += fileRead.length();
	        }
	    }

		long limitSizeImage = (totalForderImageSize + file.length());

		if (limitSizeImage <= maxSize) {
			File fileToCreate = new File(filePath + folderImage, fileFileName);
			FileUtils.copyFile(file, fileToCreate);
			imageUpload.setName(fileFileName);
			imageUpload.setType("f");
			imageUpload.setSize(file.length());
			imageUpload.setFilePatch(folderImage + "/" + fileFileName);
		} else {
			imageUpload.setName("error");
			imageUpload.setType("f");
			imageUpload.setSize(0);
		}

		return imageUpload;
	}
	/************************************************************************
	 * <b>Description:</b><br>
	 *  deleteImageDescriptionSentence.
	 * @author		Le.Dinh
	 * @date		11 Mar 2014
	 * @param 		request	HttpServletRequest
	 * @param 		fileNameToDelete	fileNameToDelete
	 * @param 		folderImage	String
	 * @return		boolean
	 * @throws 		IOException	IOException
	 ************************************************************************/
	public static boolean deleteImageDescriptionSentence(HttpServletRequest request, String folderImage, String fileNameToDelete) throws IOException {
		// get real path
		String filePath = Utils.getRealPath(request);
		File fileToDelete = new File(filePath + folderImage, fileNameToDelete);
		if (fileToDelete.exists()) {
			FileUtils.forceDelete(fileToDelete);
		}
		return true;
	}

	/************************************************************************
	 * <b>Description:</b><br>
	 *  rename File name tmp.
	 *
	 * @author		Tran.Thanh
	 * @date		Aug 25, 2014
	 * @param 		request base path
	 * @param 		folderImage folderImage
	 * @param 		imageNameTmp fileName tmp
	 * @param 		imageName imageName
	 * @return		ImageUploadKenDo true if rename success
	 * @throws IOException		boolean
	 ************************************************************************/
	public static ImageUploadKenDo renameFileUpload(HttpServletRequest request,
			String folderImage, String imageNameTmp, String imageName) throws IOException {
		ImageUploadKenDo imageUpload = new ImageUploadKenDo();

		String filePath = Utils.getRealPath(request);
		File fileTmp = new File(filePath, imageNameTmp);

		//String imageName2 = URLDecoder.decode(imageName, Constant.UTF_8_ENCODE);
		File fileNew = new File(filePath + folderImage, imageName);

		if (fileTmp.renameTo(fileNew)) {
			imageUpload.setFilePatch("./" + folderImage + "/" +  imageName);
			return imageUpload;
		}
		return null;
	}

	/************************************************************************
	 * <b>Description:</b><br>
	 *  Find and replace one quote symbol (") to two symbol.
	 *
	 * @author		Nguyen.Chuong
	 * @date		Apr 3, 2014
	 * @param      textInput String
	 * @return		String
	 ************************************************************************/
	public static String dobuleQuoteChar(String textInput) {
        String textReturn = "";
        if (StringUtils.isNotEmpty(textInput)) {
            textReturn = textInput.replaceAll("\"", "\"\"");
        }
        return textReturn;
    }

	/************************************************************************
	 * <b>Description:</b><br>
	 *  Remove char 65279: char start of file.
	 *
	 * @author		Luong.Dai
	 * @date		May 9, 2014
	 * @param 		s		First line of file
	 * @return		String
	 ************************************************************************/
	public static String removeCharNoBreakSpace(String s) {
		if (StringUtils.isNotBlank(s)
        	&& s.charAt(0) == Constant.ZERO_WIDTH_NO_BREAK_SPACE) {
        	s = s.substring(1);
        }
		return s;
	}

    /**
     * Not content special charater of japan.
     * @author nguyen.hieu
     * @date 2014-03-10
     * @param str Input string.
     * @return If text doesn't contain symbol.
     */
    public static boolean symbolCheck(String str) {
        if (str != null && !str.isEmpty()) {
            try {
            	/* BOE @rcv! Luong.Dai 2014/09/10: replace none special char. Chars can't detect when change encode */
            	//Remove none special char
            	String tmp = str.replaceAll(Constant.REGEX_NONE_SPECIAL_CHAR, "");
                /*String utf8Str = EncodingUtils.sjisToUtf8(str);
                String sjisStr = EncodingUtils.utf8ToSjis(utf8Str);
                return str.equals(sjisStr);*/
            	String utf8Str = EncodingUtils.sjisToUtf8(tmp);
                String sjisStr = EncodingUtils.utf8ToSjis(utf8Str);
                return tmp.equals(sjisStr);
                /* EOE @rcv! Luong.Dai 2014/09/10: replace none special char. Chars can't detect when change encode */
            } catch (UnsupportedEncodingException e) {
                return false;
            }
        }
        return true;
    }

    /************************************************************************
     * <b>Description:</b><br>
     *  Create error message when import CSV.
     *  - If oldMess empty => add [Error] before NewMess.
     *  - If oldMess not empty(content one other mess) => add [/] before new mess and add new NewMess to oldMess.
     * @author		Nguyen.Chuong
     * @date		Jul 9, 2014
     * @param       inputMess String: current message.
     * @param       newMess String: message which want to add.
     * @return		String message which is content old and new message.
     ************************************************************************/
    public static String createErrorCSVMessage(String inputMess, String newMess) {
        String returnString;
        //Check messge content [Error].
        //If messge not content [Error] => new messge
        //=> add [Error] before messge then add message.
        if (inputMess.indexOf(Constant.PREFIX_ERROR_MESSAGE) < 0) {
            returnString = Constant.PREFIX_ERROR_MESSAGE + newMess;
            return returnString;
        }
        //If message content [Error] then messge content more then one other messge.
        //=> add "/" + new message.
        returnString = inputMess + Constant.SEPARATOR_ERROR_MESSAGE + newMess;
        return returnString;
    }

    /************************************************************************
     * <b>Description:</b><br>
     *  validate content space 1byte or 2 byte.
     * - True: string content space 1byte or 2 byte.
     * - False: Strig not content space 1byte or 2 byte.
     * @author		Nguyen.Chuong
     * @date		Jul 10, 2014
     * @param       input String to validate.
     * @return		boolean
     ************************************************************************/
    public static boolean isStrContentSpace(String input) {
        if (input.indexOf(" ") > 0 || input.indexOf("�@") > 0) {
            return true;
        }
        return false;
    }

    /************************************************************************
     * <b>Description:</b><br>
     * Check that String is content 2 byte character.
     * @author      Nguyen.Chuong
     * @date        2014/07/15
     * @param   str String to check.
     * @return      boolean
     * True: when string content 2 byte character. EX: isContent2ByteCharacter("���{nation") => true
     * 		: has exception when convert string to byte array
     * False: when string not content 2 byte character. EX: isContent2ByteCharacter("nation") => false
     ************************************************************************/
    public static boolean isContent2ByteCharacter(String str) {
        try {
        	//Convert all input to String
            str = str + "";
            //Normal length of string.
            int strLength = str.length();
            //Length of list byte convert from String.
            int strByteLength = str.getBytes(Constant.UTF_8_ENCODE).length;
            //If both length is equal => not content 2 byte character.
            if (strByteLength == strLength) {
                return false;
            }
        } catch (UnsupportedEncodingException e) {
        	//Cannot convert string to byte array
        	e.printStackTrace();
        }
      //If strByteLength > strLength => content 2 byte character.
        return true;
    }

    // BOE @rcv!Luong.Tuong 2014/07/15 #9955 : Validate String image
    /**
     * Validate thumb nail size
     * @author Luong.Tuong
     * @date 2014-07-15
     * @param thumbnailPath Input string.
     * @return If size 140X140 return true(valid), else return false(invalid)
     */
    public boolean validateThumbnailSize(String thumbnailPath) {
        boolean status = false;
        if (StringUtils.isNotEmpty(thumbnailPath)) {
            try {
            	URL url = new URL(thumbnailPath);
            	URLConnection conn = url.openConnection();
            	InputStream in = conn.getInputStream();
            	BufferedImage  image = ImageIO.read(in);
            	int width = image.getWidth();
            	int height = image.getHeight();
            	if (width == VALIDSIZE_THUMB && height == VALIDSIZE_THUMB) {
            	    status =  true;
            	} else {
            	    status = false;
            	}
            } catch (IOException e) {
                logger.error(CURRENT_CLASS + " An error when get image ");
                logger.error(e.toString());
                status = false;
            }
        }
        return status;
    }
    /**
     * Check exists image with path.
     * @author Luong.Tuong
     * @date 2014-07-15
     * @param imageFileName Input Path.
     * @return If exists file return true, else return false
     */
    public boolean isExistsImage(String imageFileName) {
    	boolean status = false;
    	String contentType = "";
    	if (StringUtils.isNotEmpty(imageFileName)) {
            HttpURLConnection conn = null;
            try {
                URL url = new URL(imageFileName);
                conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod(CONTENTHEADER);
                contentType = conn.getContentType();
            } catch (IOException e) {
                    logger.error(CURRENT_CLASS + " An error when get image");
                    logger.error(e.toString());
                    status = false;
            }
        }
        if (contentType.contains(Constant.IMAGE)) {
            status = true;
        } else {
            status = false;
        }
        return status;
    }
    // EOE @rcv!Luong.Tuong 2014/07/15 #9955 : Validate String image

    /************************************************************************
     * <b>Description:</b><br>
     * validate html string.
     *
     * @author      Tran.Toan
     * @date        Jul 11, 2014
     * @param       source html string
     * @return      boolean
     ************************************************************************/
    public static boolean isValidHtml(String source) {
        if (StringUtils.isNotEmpty(source)) {
            source = source.replaceAll("&", "&amp;");
            source = "<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n"
                    + "<html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=Shift-JIS\"><title></title></head><body>"
                    + source + "</body></html>";
            Tidy tidy = new Tidy();
            ByteArrayOutputStream os = new ByteArrayOutputStream();
            try {
                //Hide log output
//                tidy.setQuiet(true);
                tidy.parse(new ByteArrayInputStream(source.getBytes("shift-jis")), os);
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
                return false;
            }
            return !((tidy.getParseErrors() > 0) || (tidy.getParseWarnings() > 0));
        }
        return true;
    }
}
