/**
 * Class store condition for sql mapper MstProduct.xml.
 * @author ho.hoang
 * @since 2014/03/21
 */
package net.webike.RcProductFactory.entity;

import org.apache.commons.lang.StringUtils;

/**
 * Store condition.
 * @author ho.hoang
 *
 */
public class MstProductSqlCondition {
    /**
     * Create product sql query condition to determine left-inner join table.
     * @param entMstProduct entMstProduct
     */
    public MstProductSqlCondition(final EntMstProduct entMstProduct) {
        this.entMstProduct = entMstProduct;
    }

    /** check group buy or not.*/
    private boolean groupByFlg;

    private EntMstProduct entMstProduct;
    /** inner join table rc_syouhin.tbl_syouhin_siire.*/
    private boolean tblSyouhinSiireFlg;

    /** inner join table rc_syouhin.tbl_product_description.*/
    private boolean tblProductDescriptionFlg;

    /** inner join table rc_syouhin.tbl_syouhin_fit_model.*/
    private boolean tblSyouhinFitModelFlg;

    /** inner join table rc_syouhin.tbl_syouhin_image.*/
    private boolean tblSyouhinImageFlg;

    /** inner join table rc_syouhin.tbl_product_attribute_***.*/
    private boolean tblProductAttributeFlg;

    /** inner join table rc_syouhin.tbl_product_attribute_**ALL*.*/
    private boolean tblProductAttributeAllFlg;

    /** inner join table rc_syouhin.tbl_product_link.*/
    private boolean tblProductLinkFlg;

    /** inner join table rc_syouhin.tbl_product_video.*/
    private boolean tblProductVideoFlg;

    /** inner join table rc_syouhin.tbl_product_condition.*/
    private boolean tblProductConditionFlg;

    /** inner join table rc_syouhin.tbl_sikiri_now.*/
    private boolean tblSikiriNowFlg;

    /** inner join table rc_syouhin.tbl_syouhin_select.*/
    private boolean tblSyouhinSelectFlg;

    /** inner join table rc_syouhin.mst_brand.*/
    private boolean mstBrandFlg;
    /** inner join table rc_syouhin.mst_product_supplier_status.*/
    private boolean mstProductSupplierStatusFlg;

    /** inner join table rc_syouhin.mst_product_status.*/
    private boolean mstProductStatusCodeFlg;

    /** inner join table rc_syouhin.mst_product_status.*/
    private boolean mstFactoryUserFlg;

    /**
     * @return the tblSyouhinSiireFlg
     */
    public boolean isTblSyouhinSiireFlg() {
        tblSyouhinSiireFlg = false;
        String sirreCode = entMstProduct.getSiireCode();
        String noukiCode = entMstProduct.getNoukiCode();
        if (null != sirreCode
            || (noukiCode != null && StringUtils.isNotBlank(noukiCode))
            || "siireName1".equals(entMstProduct.getSortField())
            || "siireName2".equals(entMstProduct.getSortField())) {
            tblSyouhinSiireFlg = true;
        }
        return tblSyouhinSiireFlg;
    }

    /**
     * @return the tblProductDescriptionFlg
     */
    public boolean isTblProductDescriptionFlg() {
        tblProductDescriptionFlg = false;
        String remarks = entMstProduct.getDescriptionRemarks();
        String summary = entMstProduct.getDescriptionSummary();
        String sentence = entMstProduct.getDescriptionSentence();
        String caution = entMstProduct.getDescriptionCaution();
        if ((remarks != null && StringUtils.isNotBlank(remarks))
            || (summary != null && StringUtils.isNotBlank(summary))
            || (sentence != null && StringUtils.isNotBlank(sentence))
            || (caution != null && StringUtils.isNotBlank(caution))
            || entMstProduct.isNoteWhether()
            || entMstProduct.isSummaryWhether()
            || entMstProduct.isExplanationWhether()
            || entMstProduct.isCautionWhether()
            || "descriptionSummary".equals(entMstProduct.getSortField())) {
            tblProductDescriptionFlg = true;
        }
        return tblProductDescriptionFlg;
    }

    /**
     * @return the tblSyouhinFitModelFlg
     */
    public boolean isTblSyouhinFitModelFlg() {
        tblSyouhinFitModelFlg = false;
        String model = entMstProduct.getModel();
        String style = entMstProduct.getStyle();
        if ((model != null && StringUtils.isNotBlank(model))
            || (style != null && StringUtils.isNotBlank(style))
            || entMstProduct.isSupportedBikeWhether()
            || entMstProduct.isBikeModelWhether()) {
            tblSyouhinFitModelFlg = true;
        }
        return tblSyouhinFitModelFlg;
    }

    /**
     * @return the tblSyouhinSelectFlg
     */
    public boolean isTblSyouhinSelectFlg() {
        tblSyouhinSelectFlg = false;
        Integer selectCode = entMstProduct.getSelectCode();
        String hyoujiName = entMstProduct.getHyoujiName();
        if (selectCode != null
        || (hyoujiName != null && StringUtils.isNotBlank(hyoujiName))
        || entMstProduct.isOptionWhether()) {
            tblSyouhinSelectFlg = true;
        }
        return tblSyouhinSelectFlg;
    }

    /**
     * @return the tblSyouhinImageFlg
     */
    public boolean isTblSyouhinImageFlg() {
        tblSyouhinImageFlg = false;
        String thumbnail = entMstProduct.getThumbnail();
        String image = entMstProduct.getImage();
        if ((thumbnail != null && StringUtils.isNotBlank(thumbnail))
            || (image != null && StringUtils.isNotBlank(image))
            || entMstProduct.isThumbnailImagePathWhether()
            || entMstProduct.isDetailedImagePathWhether()) {
            tblSyouhinImageFlg = true;
        }
        return tblSyouhinImageFlg;
    }

    /**
     * @return the tblProductAttributeFlg
     */
    public boolean isTblProductAttributeFlg() {
        tblProductAttributeFlg = false;
        Integer attributeCode = entMstProduct.getAttributeCode();
        String attributeDisplay = entMstProduct.getAttributeDisplayName();
        String attributeValue = entMstProduct.getAttributeManagementValue();
        if ((attributeCode != null)
            || (attributeDisplay != null && StringUtils.isNotBlank(attributeDisplay))
            || (attributeValue != null && StringUtils.isNotBlank(attributeValue))
            || entMstProduct.isAttributeDisplayNameWhether()
            || entMstProduct.isAttributeManagementValueWhether()) {
            tblProductAttributeFlg = true;
        }
        return tblProductAttributeFlg;
    }

    /**
     * @return the tblProductAttributeAllFlg
     */
    public boolean isTblProductAttributeAllFlg() {
        tblProductAttributeAllFlg = false;
        Integer attributeCode = entMstProduct.getAttributeCode();
        String attributeDisplay = entMstProduct.getAttributeDisplayName();
        String attributeValue = entMstProduct.getAttributeManagementValue();
        if (attributeCode != null) {
            if ((attributeDisplay == null || StringUtils.isBlank(attributeDisplay))
                && (attributeValue == null || StringUtils.isBlank(attributeValue))) {
                tblProductAttributeAllFlg = true;
            }
        }
        return tblProductAttributeAllFlg;
    }

    /**
     * @return the tblProductLinkFlg
     */
    public boolean isTblProductLinkFlg() {
        tblProductLinkFlg = false;
        String linkTitle = entMstProduct.getLinkTitle();
        if ((linkTitle != null && StringUtils.isNotBlank(linkTitle))
            || entMstProduct.isLinkTitleWhether()) {
            tblProductLinkFlg = true;
        }
        return tblProductLinkFlg;
    }

    /**
     * @return the tblProductVideoFlg
     */
    public boolean isTblProductVideoFlg() {
        tblProductVideoFlg = false;
        String videoTitle = entMstProduct.getVideoTitle();
        if ((videoTitle != null && StringUtils.isNotBlank(videoTitle))
            || entMstProduct.isVideoTitleWhether()) {
            tblProductVideoFlg = true;
        }
        return tblProductVideoFlg;
    }

    /**
     * @return the tblProductConditionFlg
     */
    public boolean isTblProductConditionFlg() {
        tblProductConditionFlg = false;
        if (entMstProduct.isProductOpenPriceFlg()
            || entMstProduct.isProductProperSellingFlg()
            || entMstProduct.isProductOrderProductFlg()
            || entMstProduct.isProductNoReturnableFlg()
            || entMstProduct.isProductAmbigousImageFlg()) {
                tblProductConditionFlg = true;
        }
        return tblProductConditionFlg;
    }

    /**
     * @return the isTblSikiriNowFlg
     */
    public boolean isTblSikiriNowFlg() {
        tblSikiriNowFlg = false;
        Integer formPrice = entMstProduct.getFromPartition();
        Integer toPrice = entMstProduct.getToPartition();
        if (formPrice != null || toPrice != null
            || "rate".equals(entMstProduct.getSortField())) {
            tblSikiriNowFlg = true;
        }
        return tblSikiriNowFlg;
    }

    /**
     * @return the mstBrandFlg
     */
    public boolean isMstBrandFlg() {
        mstBrandFlg = false;
        Integer brandCode = entMstProduct.getBrandCode();
        if (brandCode != null
            || "brandName".equals(entMstProduct.getSortField())) {
            mstBrandFlg = true;
        }
        return mstBrandFlg;
    }

    /**
     * @return the mstProductSupplierStatusFlg
     */
    public boolean isMstProductSupplierStatusFlg() {
        mstProductSupplierStatusFlg = false;
        String supplierStatusName = entMstProduct.getSupplierStatusName();
        if ((supplierStatusName != null && StringUtils.isNotBlank(supplierStatusName))
            || "supplierStatusName".equals(entMstProduct.getSortField())) {
            mstProductSupplierStatusFlg = true;
        }
        return mstProductSupplierStatusFlg;
    }

    /**
     * @return the mstProductStatusCodeFlg
     */
    public boolean isMstProductStatusCodeFlg() {
        mstProductStatusCodeFlg = false;
        String statusName = entMstProduct.getProductStatusName();
        if ((statusName != null && StringUtils.isNotBlank(statusName))
            || "productStatusName".equals(entMstProduct.getSortField())) {
            mstProductStatusCodeFlg = true;
        }
        return mstProductStatusCodeFlg;
    }

    /**
     * @return the mstFactoryUserFlg
     */
    public boolean isMstFactoryUserFlg() {
        mstFactoryUserFlg = false;
        String fullName = entMstProduct.getFullName();
        if ((fullName != null && StringUtils.isNotBlank(fullName))
            || "fullName".equals(entMstProduct.getSortField())) {
            mstFactoryUserFlg = true;
        }
        return mstFactoryUserFlg;
    }

    /**
     * @return the groupByFlg
     */
    public boolean isGroupByFlg() {
        groupByFlg = false;
        if (isTblSyouhinSiireFlg()
            || isTblSyouhinFitModelFlg()
            || isTblSyouhinImageFlg()
            || isTblProductAttributeFlg()
            || isTblProductAttributeAllFlg()
            || isTblProductLinkFlg()
            || isTblProductVideoFlg()
            || isTblSyouhinSelectFlg()
            || isMstProductSupplierStatusFlg()) {
            groupByFlg = true;
        }
        return groupByFlg;
    }
}
