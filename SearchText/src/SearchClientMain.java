import java.io.File;
import java.io.IOException;
import java.util.ArrayList;


public class SearchClientMain {

	public static void main(String[] args) {
		ReadFile readFile = new ReadFile();
		SearchText searchText = new SearchText();
		String tokenFilePath = "D:\\laixe\\client.txt";
		String fileCollecitonPath = "D:\\laixe\\mapping.txt";
		try {
			ArrayList<String> tokenSearchCollection = readFile.readFile1(new File(tokenFilePath));
			ArrayList<String> fileCollection = readFile.readFile1(new File(fileCollecitonPath));
			
			for (String tokeBilling : tokenSearchCollection) {
				for (String mappingFilePath : fileCollection) {
					searchText.search(tokeBilling, mappingFilePath);
				}
			}
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
