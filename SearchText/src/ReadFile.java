import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;


public class ReadFile {	
	
	public ArrayList<String> readFile1(File fin) throws IOException {
		FileInputStream fis = new FileInputStream(fin);
		ArrayList<String> arrayList = new ArrayList<String>();
		//Construct BufferedReader from InputStreamReader
		BufferedReader br = new BufferedReader(new InputStreamReader(fis));
	 
		String line = null;
		while ((line = br.readLine()) != null) {
			arrayList.add(line);
		}
	 
		br.close();
		return arrayList;
	}

}
