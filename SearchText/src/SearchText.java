import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;


public class SearchText {
	
	ArrayList<String> retArray = new ArrayList<String>();

public void search (String inputSearch, String filePath)
{
	
    double count = 0,countBuffer=0,countLine=0;
    String lineNumber = "";
//    String filePath = "D:\\ExceedJ\\workspace\\pA_GIT\\exceedj-businesslayer\\exceedj-base\\config\\hibernate-mappings\\PolicyTab.queries.hbm.xml";
    BufferedReader br;
//    String inputSearch = "POLICY_TAB";
    String line = "";

    try {
        br = new BufferedReader(new FileReader(filePath));
        try {
            while((line = br.readLine()) != null)
            {
                countLine++;
                //System.out.println(line);
                String[] words = line.split(" ");

                for (String word : words) {
                  if (word.equals(inputSearch)) {
                    count++;
                    countBuffer++;
                  }
                }

                if(countBuffer > 0)
                {
                    countBuffer = 0;
                    lineNumber += countLine + ",";
                }

            }
            br.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    } catch (FileNotFoundException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
    }
    if(count > 0 && !isExits(filePath))
    {
//    System.out.println(filePath + "--Times found at--" + count);
    	retArray.add(filePath);
    	System.out.println(filePath);
    }
    
//    for (String fileBillingPath : retArray) {
//    	System.out.println(fileBillingPath);
//	}
}

private boolean isExits (String filePath)
{
	boolean ret = false;
	for (String element : retArray) {
		if(element.equalsIgnoreCase(filePath))
			{
			ret = true;
			break;
			}
	}
	
	return ret;
	
}
}